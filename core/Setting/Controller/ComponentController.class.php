<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * Main controller for Setting
 *
 * @author Michael Ritter <michael.ritter@comvation.com>
 * @package cloudrexx
 * @subpackage core_setting
 */

namespace Cx\Core\Setting\Controller;

/**
 * Main controller for Setting
 *
 * @author Michael Ritter <michael.ritter@comvation.com>
 * @package cloudrexx
 * @subpackage core_setting
 */
class ComponentController extends \Cx\Core\Core\Model\Entity\SystemComponentController implements \Cx\Core\Event\Model\Entity\EventListener {

    /**
     * {@inheritdoc}
     */
    public function getControllerClasses() {
        return array('SettingJson');
    }

    /**
     * {@inheritdoc}
     */
    public function getControllersAccessableByJson() {
        return array('SettingJsonController');
    }

    /**
     * Returns a list of command mode commands provided by this component
     * @return array List of command names
     */
    public function getCommandsForCommandMode() {
        return array(
            'Setting' => new \Cx\Core_Modules\Access\Model\Entity\Permission(
                array(),
                array('cli'),
                false
            ),
        );
    }

    /**
     * Returns the description for a command provided by this component
     * @param string $command The name of the command to fetch the description from
     * @param boolean $short Wheter to return short or long description
     * @return string Command description
     */
    public function getCommandDescription($command, $short = false) {
        if ($command != 'Setting') {
            return '';
        }
        if ($short) {
            return 'Allows to show, add, edit and remove settings';
        }
        return 'Setting [user] list <component> -group=<group> [-engine=<engine>] [-repository=<repository>]
Setting [user] get <component> -group=<group> [-engine=<engine>] [-repository=<repository>] [-user=<userId>] <name>
Setting [user] set <component> -group=<group> [-engine=<engine>] [-repository=<repository>] [-user=<userId>] <name> <value>
Setting [user] add <component> -group=<group> [-engine=<engine>] [-repository=<repository>] [--dry-run] <name> <value> <ord> <type> <values>
Setting [user] alter <component> -group=<group> [-engine=<engine>] [-repository=<repository>] <name> <new_type> <new_values> [<new_group> [<new_ord>]]
Setting [user] delete <component> -group=<group> [-engine=<engine>] [-repository=<repository>] <name>';
    }

    /**
     * Execute one of the commands listed in getCommandsForCommandMode()
     * @see getCommandsForCommandMode()
     * @param string $command Name of command to execute
     * @param array $arguments List of arguments for the command
     * @param array  $dataArguments (optional) List of data arguments for the command
     * @return void
     */
    public function executeCommand($command, $arguments, $dataArguments = array()) {
        switch ($command) {
            case 'Setting':
                if (count($arguments) < 2) {
                    echo 'Not enough arguments';
                    return;
                }
                $subCommand = array_shift($arguments);
                $userBased = $subCommand == 'user';
                if ($userBased) {
                    if (count($arguments) < 2) {
                        echo 'Not enough arguments';
                        return;
                    }
                    $subCommand = array_shift($arguments);
                }
                $component = array_shift($arguments);
                $group = null;
                if (isset($arguments['-group'])) {
                    $group = $arguments['-group'];
                    unset($arguments['-group']);
                }
                $engine = 'Database';
                if (isset($arguments['-engine'])) {
                    $engine = $arguments['-engine'];
                    unset($arguments['-engine']);
                }
                $repository = null;
                if (isset($arguments['-repository'])) {
                    $repository = $arguments['-repository'];
                    unset($arguments['-repository']);
                }
                $flush = true;
                if (($flushIdx = array_search('--dry-run', $arguments)) !== false) {
                    $flush = false;
                    unset($arguments[$flushIdx]);
                }
                $userId = 0;
                if (isset($arguments['-user'])) {
                    $userId = (int) $arguments['-user'];
                    if ($userId <= 0) {
                        echo 'Invalid user ID';
                        return;
                    }
                    unset($arguments['-user']);
                }
                if ($userBased) {
                    if ($repository || $engine != 'Database') {
                        echo 'User-based settings are always stored in DB';
                        return;
                    }
                    if (!$group) {
                        echo 'Group is mandatory for user-based settings';
                        return;
                    }
                    switch ($subCommand) {
                        case 'list':
                            $data = $this->getUserBasedSettings(
                                $this->getComponent($component),
                                $group
                            );
                            foreach ($data as $index=>&$entity) {
                                $entity = $entity->toArray();
                            }
                            break;
                        case 'add':
                            if (count($arguments) != 5) {
                                echo 'Wrong number of arguments';
                                return;
                            }
                            $name = array_shift($arguments);
                            $value = array_shift($arguments);
                            $ord = array_shift($arguments);
                            $type = array_shift($arguments);
                            $values = array_shift($arguments);
                            $this->createUserSetting(
                                $this->getComponent($component),
                                $group,
                                $name,
                                $type,
                                $value,
                                $values,
                                $ord
                            );
                            echo 'Success';
                            return;
                            break;
                        case 'alter':
                            if (count($arguments) < 3) {
                                echo 'Not enough arguments';
                                return;
                            }
                            $name = array_shift($arguments);
                            $type = array_shift($arguments);
                            $values = array_shift($arguments);
                            $newGroup = '';
                            $ord = 0;
                            if (count($arguments)) {
                                $newGroup = array_shift($arguments);
                            }
                            if (count($arguments)) {
                                $ord = array_shift($arguments);
                            }
                            $this->alterUserSetting(
                                $this->getComponent($component),
                                $group,
                                $name,
                                $type,
                                $values,
                                $newGroup,
                                $ord
                            );
                            echo 'Success';
                            return;
                            break;
                        case 'delete':
                            if (count($arguments) != 1) {
                                echo 'Wrong number of arguments';
                                return;
                            }
                            $name = array_shift($arguments);
                            $this->removeUserSetting(
                                $this->getComponent($component),
                                $group,
                                $name
                            );
                            echo 'Success';
                            return;
                            break;
                        case 'get':
                            if (!$userId) {
                                echo 'Argument -user is required';
                                return;
                            }
                            if (count($arguments) != 1) {
                                echo 'Wrong number of arguments';
                                return;
                            }
                            $name = array_shift($arguments);
                            $data = $this->getSettingUserValue(
                                $this->getComponent($component),
                                $group,
                                $name,
                                $userId
                            );
                            break;
                        case 'set':
                            if (!$userId) {
                                echo 'Argument -user is required';
                                return;
                            }
                            if (count($arguments) != 2) {
                                echo 'Wrong number of arguments';
                                return;
                            }
                            $name = array_shift($arguments);
                            $value = array_shift($arguments);
                            $this->setSettingUserValue(
                                $this->getComponent($component),
                                $group,
                                $name,
                                $userId,
                                $value
                            );
                            echo 'Success';
                            return;
                            break;
                        default:
                            echo 'Illegal syntax';
                            return;
                    }
                } else {
                    \Cx\Core\Setting\Controller\Setting::init(
                        $component,
                        $group,
                        $engine,
                        $repository
                    );
                    switch ($subCommand) {
                        case 'list':
                            $data = \Cx\Core\Setting\Controller\Setting::getArray(
                                $component,
                                $group
                            );
                            break;
                        case 'get':
                            if (!count($arguments)) {
                                echo 'Not enough arguments';
                                return;
                            }
                            $data = \Cx\Core\Setting\Controller\Setting::getValue(
                                array_shift($arguments),
                                $component
                            );
                            break;
                        case 'set':
                            if (count($arguments) < 2) {
                                echo 'Not enough arguments';
                                return;
                            }
                            $name = array_shift($arguments);
                            \Cx\Core\Setting\Controller\Setting::set(
                                $name,
                                array_shift($arguments)
                            );
                            $data = \Cx\Core\Setting\Controller\Setting::update(
                                $name
                            );
                            break;
                        case 'add':
                            if (count($arguments) < 5) {
                                echo 'Not enough arguments';
                                return;
                            }
                            $data = \Cx\Core\Setting\Controller\Setting::add(
                                array_shift($arguments),
                                array_shift($arguments),
                                array_shift($arguments),
                                array_shift($arguments),
                                array_shift($arguments),
                                $group,
                                $flush
                            );
                            break;
                        case 'alter':
                            if (count($arguments) < 3) {
                                echo 'Not enough arguments';
                                return;
                            }
                            $name = array_shift($arguments);
                            $type = array_shift($arguments);
                            $values = array_shift($arguments);
                            $newGroup = '';
                            $ord = 0;
                            if (count($arguments)) {
                                $newGroup = array_shift($arguments);
                            }
                            if (count($arguments)) {
                                $ord = array_shift($arguments);
                            }
                            $data = \Cx\Core\Setting\Controller\Setting::alter(
                                $name,
                                $type,
                                $values,
                                $newGroup,
                                $ord
                            );
                            break;
                        case 'delete':
                            if (!count($arguments)) {
                                echo 'Not enough arguments';
                                return;
                            }
                            $data = \Cx\Core\Setting\Controller\Setting::delete(
                                array_shift($arguments),
                                $group
                            );
                            break;
                        default:
                            echo 'Illegal syntax';
                            return;
                    }
                }
                $hydrator = $this->getComponent('DataAccess')->getController(
                    'CliOutput'
                );
                echo $hydrator->parse(
                    array(
                        'status' => 'success',
                        'data' => $data,
                    )
                );
                break;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function registerEventListeners() {
        $evm = $this->cx->getEvents();
        $evm->addModelListener(
            \Doctrine\ORM\Events::postRemove,
            'User',
            $this
        );
        $evm->addModelListener(
            \Doctrine\ORM\Events::onFlush,
            $this->getNamespace() . '\\Model\\Entity\\Setting',
            $this
        );
    }

    /**
     * {@inheritdoc}
     */
    public function onEvent($eventName, array $eventArgs) {
        switch ($eventName) {
            case \Doctrine\ORM\Events::postRemove:
                $userId = current($eventArgs)->getEntity()->getId();
                $em = $this->cx->getDb()->getEntityManager();
                $settingValueRepo = $em->getRepository(
                    $this->getNamespace() . '\\Model\\Entity\\SettingUserValue'
                );
                foreach (
                    $settingValueRepo->findBy(
                        array(
                            'userId' => $userId,
                        )
                    ) as $settingValue
                ) {
                    $em->remove($settingValue);
                }
                $em->flush();
                break;
            case \Doctrine\ORM\Events::onFlush:
                // This whole case is a work-around for CLX-5109
                $em = current($eventArgs)->getEntityManager();
                $uow = $em->getUnitOfWork();
                $changedEntities = array();
                foreach (
                    array(
                        'EntityInsertions',
                        'EntityUpdates',
                        'EntityDeletions',
                        'CollectionDeletions',
                        'CollectionUpdates',
                    ) as $method
                ) {
                    $method = 'getScheduled' . $method;
                    foreach ($uow->$method() as $entity) {
                        if (get_class($entity) != $this->getNamespace() . '\\Model\\Entity\\Setting') {
                            continue;
                        }
                        if (in_array($entity, $changedEntities)) {
                            continue;
                        }
                        $changedEntities[] = $entity;
                    }
                }
                foreach ($changedEntities as $entity) {
                    $entity->validate();
                }
                break;
        }
    }

    /**
     * Creates a new user-specific setting
     *
     * $name has to be unique per $groupName and $component, but accross non-
     * user-specific settings as well.
     * @param \Cx\Core\Core\Model\Entity\SystemComponentController $component Responsible component for this setting
     * @param string $groupName Component specific group name
     * @param string $name Component specific name of the setting
     * @param string $type One of the \Cx\Core\Setting\Controller\Setting::TYPE_* constants
     * @param string $default Default value for this setting
     * @param string $validValues String containing valid values for the type
     * @param int $ord Integer to order this setting by
     * @param bool $flush Whether to flush the new setting to the repository.
     * @throws \Exception If the combination of $component, $groupName and $name is not unique
     */
    public function createUserSetting(
        \Cx\Core\Core\Model\Entity\SystemComponentController $component,
        string $groupName,
        string $name,
        string $type,
        string $default,
        string $validValues,
        int $ord,
        bool $flush = true
    ): void {
        $em = $this->cx->getDb()->getEntityManager();
        $repo = $em->getRepository(
            $this->getNamespace() . '\\Model\\Entity\\Setting'
        );
        if (
            $repo->findOneBy(
                array(
                    'section' => $component->getName(),
                    'group' => $groupName,
                    'name' => $name,
                )
            )
        ) {
            throw new \Exception('Setting already exists');
        }
        $entity = new \Cx\Core\Setting\Model\Entity\Setting(
            $component->getName(),
            $groupName,
            $name,
            $type,
            $default,
            $validValues,
            $ord
        );
        $em->persist($entity);
        if (!$flush) {
            return;
        }
        $em->flush();
    }

    /**
     * Alter an existing user-specific setting
     *
     * @param string $name The name of an existing user-specific setting
     * @param string $groupName Component specific group name
     * @param string $type One of the \Cx\Core\Setting\Controller\Setting::TYPE_* constants
     * @param string $validValues The type specific values
     * @param string $newGroupName The group to use. If not set or set to an
     *     empty string, the existing group will be left untouched.
     * @param int $ord Integer to order this setting by. If not set or
     *     set to `0`, the existing ordinal value will be left untouched.

     * @throws \Exception If no existing setting is found
     * @throws \Exception If the combination of $component, $newGroupName and $name is not unique
     */
    public function alterUserSetting(
        \Cx\Core\Core\Model\Entity\SystemComponentController $component,
        string $groupName,
        string $name,
        string $type,
        string $validValues = '',
        string $newGroupName = '',
        int $ord = 0
    ): void {
        $em = $this->cx->getDb()->getEntityManager();
        $repo = $em->getRepository(
            $this->getNamespace() . '\\Model\\Entity\\Setting'
        );
        $entity = $repo->findOneBy(
            array(
                'section' => $component->getName(),
                'group' => $groupName,
                'name' => $name,
            )
        );
        if (!$entity) {
            throw new \Exception('Setting not found');
        }
        if (
            $newGroupName !== ''
            && $repo->findOneBy(
                array(
                    'section' => $component->getName(),
                    'group' => $newGroupName,
                    'name' => $name,
                )
            )
        ) {
            throw new \Exception('Setting already exists');
        }
        if ($ord !== 0) {
            $entity->setOrd($ord);
        }
        $entity->setType($type);
        $entity->setValues($validValues);
        if ($newGroupName !== '') {
            $entity->setGroup($newGroupName);
        }
        $em->flush();
    }

    /**
     * Lists user-specific settings
     * @param ?\Cx\Core\Core\Model\Entity\SystemComponentController $component Component to filter by, defaults to NULL
     * @param string $groupName Group name to filter by, defaults to empty string
     * @param string $name Setting name to filter by, defaults to empty string
     * @return array List of \Cx\Core\Setting\Model\Entity\Setting entities
     */
    public function getUserBasedSettings(
        ?\Cx\Core\Core\Model\Entity\SystemComponentController $component = null,
        string $groupName = '',
        string $name = ''
    ): array {
        $repo = $this->cx->getDb()->getEntityManager()->getRepository(
            $this->getNamespace() . '\\Model\\Entity\\Setting'
        );
        $conditions = array('userBased' => true);
        if ($component) {
            $conditions['section'] = $component->getName();
        }
        if (!empty($groupName)) {
            $conditions['group'] = $groupName;
        }
        if (!empty($name)) {
            $conditions['name'] = $name;
        }
        return $repo->findBy($conditions);
    }

    /**
     * Removes a user-specific setting
     * @param \Cx\Core\Core\Model\Entity\SystemComponentController $component Responsible component for this setting
     * @param string $groupName Component specific group name
     * @param string $name Component specific name of the setting
     * @throws \Exception If no matching setting is found
     */
    public function removeUserSetting(
        \Cx\Core\Core\Model\Entity\SystemComponentController $component,
        string $groupName,
        string $name
    ): void {
        $em = $this->cx->getDb()->getEntityManager();
        $repo = $em->getRepository(
            $this->getNamespace() . '\\Model\\Entity\\Setting'
        );
        $entity = $repo->findOneBy(array(
            'userBased' => true,
            'section' => $component->getName(),
            'group' => $groupName,
            'name' => $name,
        ));
        if (!$entity) {
            throw new \Exception('Setting not found');
        }
        // Need to cascade manually as setting table does not support relations
        // We do this with a query rather than via $em->remove() for performance
        // We would need to flush the cache after executing the query, but
        // SettingUserValue entities are only loaded from DB if the corresponding
        // Setting entity exists which is dropped just below.
        $qb = $em->createQueryBuilder();
        $qb->delete(
            $this->getNamespace() . '\\Model\\Entity\\SettingUserValue',
            'suv'
        )->where($qb->expr()->eq('suv.setting', $entity));
        $qb->getQuery()->execute();
        $em->remove($entity);
        $em->flush();
    }

    /**
     * Drops all user settings for a given component (if any)
     * @param \Cx\Core\Core\Model\Entity\SystemComponentController $component Responsible component for this setting
     * @throws \Exception On error
     */
    public function removeComponentUserSettings(
        \Cx\Core\Core\Model\Entity\SystemComponentController $component
    ): void {
        $settings = $this->getUserBasedSettings($component);
        $ids = array();
        foreach ($settings as $setting) {
            $ids[] = $setting->getId();
        }
        // Need to cascade manually as setting table does not support relations
        // We do this with a query rather than via $em->remove() for performance
        // We would need to flush the cache after executing the query, but
        $em = $this->cx->getDb()->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->delete(
            $this->getNamespace() . '\\Model\\Entity\\SettingUserValue',
            'suv'
        )->where(
            $qb->expr()->in('suv.setting', ':ids')
        )->setParameter('ids', $ids);
        $qb->getQuery()->execute();
        $qb = $em->createQueryBuilder();
        $qb->delete(
            $this->getNamespace() . '\\Model\\Entity\\Setting',
            's'
        )->where(
            $qb->expr()->in('s.id', ':ids')
        )->setParameter('ids', $ids);
        $qb->getQuery()->execute();
    }

    /**
     * Gets the value of a user-specific setting
     * @param \Cx\Core\Core\Model\Entity\SystemComponentController $component Responsible component for this setting
     * @param string $groupName Component specific group name
     * @param string $name Component specific name of the setting
     * @param int $userId ID of the user to get the value for
     * @return string Value of the user-specific setting
     * @throws \Exception If no matching setting is found
     */
    public function getSettingUserValue(
        \Cx\Core\Core\Model\Entity\SystemComponentController $component,
        string $groupName,
        string $name,
        int $userId
    ): string {
        $em = $this->cx->getDb()->getEntityManager();
        $settingRepo = $em->getRepository(
            $this->getNamespace() . '\\Model\\Entity\\Setting'
        );
        $settingValueRepo = $em->getRepository(
            $this->getNamespace() . '\\Model\\Entity\\SettingUserValue'
        );
        $setting = $settingRepo->findOneBy(array(
            'userBased' => true,
            'section' => $component->getName(),
            'group' => $groupName,
            'name' => $name,
        ));
        if (!$setting) {
            throw new \Exception('Setting not found');
        }
        $userValue = $settingValueRepo->findOneBy(array(
            'setting' => $setting,
            'userId' => $userId,
        ));
        if (!$userValue) {
            return $setting->getValue();
        }
        return $userValue->getValue();
    }

    /**
     * Sets the value of a user-specific setting
     * @param \Cx\Core\Core\Model\Entity\SystemComponentController $component Responsible component for this setting
     * @param string $groupName Component specific group name
     * @param string $name Component specific name of the setting
     * @param int $userId ID of the user to set the value for
     * @param string $value Value to set
     * @throws \Exception If no matching setting is found
     */
    public function setSettingUserValue(
        \Cx\Core\Core\Model\Entity\SystemComponentController $component,
        string $groupName,
        string $name,
        int $userId,
        string $value
    ): void {
        $em = $this->cx->getDb()->getEntityManager();
        $settingRepo = $em->getRepository(
            $this->getNamespace() . '\\Model\\Entity\\Setting'
        );
        $settingValueRepo = $em->getRepository(
            $this->getNamespace() . '\\Model\\Entity\\SettingUserValue'
        );
        $setting = $settingRepo->findOneBy(array(
            'userBased' => true,
            'section' => $component->getName(),
            'group' => $groupName,
            'name' => $name,
        ));
        if (!$setting) {
            throw new \Exception('Setting not found');
        }
        $userValue = $settingValueRepo->findOneBy(array(
            'setting' => $setting,
            'userId' => $userId,
        ));
        if (!$userValue) {
            $userValue = new \Cx\Core\Setting\Model\Entity\SettingUserValue(
                $setting,
                $userId
            );
            $em->persist($userValue);
        }
        $userValue->setValue($value);
        $em->flush();
    }
}
