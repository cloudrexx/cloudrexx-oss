<?php declare(strict_types=1);

/**
 * Cloudrexx
 *
 * @link      https://www.cloudrexx.com
 * @copyright Cloudrexx AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

namespace Cx\Core\Setting\Model\Entity;

/**
 * @copyright   Cloudrexx AG
 * @author      Michael Ritter <michael.ritter@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  core_setting
 * Setting
 */
class Setting extends \Cx\Model\Base\EntityBase {
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $section = '';

    /**
     * @var string
     */
    protected $name = '';

    /**
     * @var string
     */
    protected $group = '';

    /**
     * @var boolean
     */
    protected $userBased = false;

    /**
     * @var string
     */
    protected $type = 'text';

    /**
     * @var string
     */
    protected $value;

    /**
     * @var string
     */
    protected $values;

    /**
     * @var integer
     */
    protected $ord = 0;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    protected $userValues;

    /**
     * Constructor
     */
    public function __construct(
        string $section = '',
        string $group = '',
        string $name = '',
        string $type = 'text',
        string $value = '',
        string $values = '',
        int $ord = 0
    ) {
        foreach (array('section', 'name', 'group', 'type', 'value', 'values', 'ord') as $property) {
            $setter = 'set' . ucfirst($property);
            $this->$setter($$property);
        }
        $this->userBased = true;
        $this->userValues = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set section
     *
     * @param string $section
     * @return Setting
     */
    public function setSection($section) {
        $this->section = $section;
    }

    /**
     * Get section
     *
     * @return string 
     */
    public function getSection() {
        return $this->section;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Setting
     */
    public function setName($name) {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set group
     *
     * @param string $group
     * @return Setting
     */
    public function setGroup($group) {
        $this->group = $group;
    }

    /**
     * Get group
     *
     * @return string 
     */
    public function getGroup() {
        return $this->group;
    }

    /**
     * Set userBased
     *
     * @param boolean $userBased
     * @return Setting
     */
    public function setUserBased($userBased) {
        if (!$userBased) {
            throw new \Exception(
                'Non-user-based settings are not implemented yet using the Doctrine model'
            );
        }
    }

    /**
     * Get userBased
     *
     * @return boolean 
     */
    public function getUserBased() {
        return true;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Setting
     */
    public function setType($type) {
        $this->type = $type;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType() {
        return $this->type;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return Setting
     */
    public function setValue($value) {
        $this->value = $value;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue() {
        return $this->value;
    }

    /**
     * Set values
     *
     * @param string $values
     * @return Setting
     */
    public function setValues($values) {
        $this->values = $values;
    }

    /**
     * Get values
     *
     * @return string 
     */
    public function getValues() {
        return $this->values;
    }

    /**
     * Set ord
     *
     * @param integer $ord
     * @return Setting
     */
    public function setOrd($ord) {
        $this->ord = $ord;
    }

    /**
     * Get ord
     *
     * @return integer 
     */
    public function getOrd() {
        return $this->ord;
    }

    /**
     * Add userValues
     *
     * @param \Cx\Core\Setting\Model\Entity\SettingUserValue $userValues
     * @return Setting
     */
    public function addUserValue(\Cx\Core\Setting\Model\Entity\SettingUserValue $userValues) {
        $this->userValues[] = $userValues;
    }

    /**
     * Remove userValues
     *
     * @param \Cx\Core\Setting\Model\Entity\SettingUserValue $userValues
     */
    public function removeUserValue(\Cx\Core\Setting\Model\Entity\SettingUserValue $userValues) {
        $this->userValues->removeElement($userValues);
    }

    /**
     * Get userValues
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUserValues() {
        return $this->userValues;
    }

    /**
     * {@inheritdoc}
     */
    public function initializeValidators() {
        $supportedTypes = array(
            \Cx\Core\Setting\Controller\Setting::TYPE_DROPDOWN,
            \Cx\Core\Setting\Controller\Setting::TYPE_DROPDOWN_MULTISELECT,
            \Cx\Core\Setting\Controller\Setting::TYPE_CHECKBOX,
            \Cx\Core\Setting\Controller\Setting::TYPE_WYSIWYG,
            \Cx\Core\Setting\Controller\Setting::TYPE_TEXT,
            \Cx\Core\Setting\Controller\Setting::TYPE_TEXTAREA,
            \Cx\Core\Setting\Controller\Setting::TYPE_FILECONTENT,
            \Cx\Core\Setting\Controller\Setting::TYPE_PASSWORD,
        );
        $this->validators = array(
            'id' => new \CxValidateInteger(),
            'section' => new \CxValidateComponentName(),
            'name' => new \CxValidateRegexp(array('pattern' => '/^[a-zA-Z0-9 _-]+/')),
            'group' => new \CxValidateRegexp(array('pattern' => '/^[a-zA-Z0-9 _-]+/')),
            'value' => $this->getValidatorForValuesField(),
            'values' => new \CxValidateRegexp(
                array(
                    'pattern' => '/^(\{src:([a-z0-9_\\\:]+)\(\)\}|[a-z0-9:,]+)$/i',
                )
            ),
            'type' => new \CxValidateRegexp(
                array(
                    'pattern' => '/^(' . implode(
                        '|', array_map('preg_quote', $supportedTypes)
                    ) . ')$/',
                )
            ),
            'ord' => new \CxValidateInteger(),
        );
    }

    /**
     * Returns the validator for the type of this setting
     * @todo Add support for currently unsupported types
     * @throws \Exception If the field type is not (yet) supported
     * @return \CxValidate Validate instance
     */
    protected function getValidatorForValuesField(): \CxValidate {
        $values = implode('|', array_map('preg_quote', array_keys($this->getValuesAsArray())));
        switch ($this->getType()) {
            case \Cx\Core\Setting\Controller\Setting::TYPE_DROPDOWN:
                return new \CxValidateRegexp(array('pattern' => '/^(' . $values . ')$/'));
                break;
            case \Cx\Core\Setting\Controller\Setting::TYPE_DROPDOWN_MULTISELECT:
                return new \CxValidateRegexp(array('pattern' => '/^(' . $values . ')+$/'));
                break;
            case \Cx\Core\Setting\Controller\Setting::TYPE_CHECKBOX:
                return new \CxValidateRegexp(array('pattern' => '/^(0|1)$/'));
                break;
            case \Cx\Core\Setting\Controller\Setting::TYPE_WYSIWYG:
            case \Cx\Core\Setting\Controller\Setting::TYPE_TEXT:
            case \Cx\Core\Setting\Controller\Setting::TYPE_TEXTAREA:
            case \Cx\Core\Setting\Controller\Setting::TYPE_FILECONTENT:
            case \Cx\Core\Setting\Controller\Setting::TYPE_PASSWORD:
                return new \CxValidateString(array());
                break;
            case \Cx\Core\Setting\Controller\Setting::TYPE_DROPDOWN_USER_CUSTOM_ATTRIBUTE:
            case \Cx\Core\Setting\Controller\Setting::TYPE_DROPDOWN_USERGROUP:
            case \Cx\Core\Setting\Controller\Setting::TYPE_FILEUPLOAD:
            case \Cx\Core\Setting\Controller\Setting::TYPE_EMAIL:
            case \Cx\Core\Setting\Controller\Setting::TYPE_BUTTON:
            case \Cx\Core\Setting\Controller\Setting::TYPE_CHECKBOXGROUP:
            case \Cx\Core\Setting\Controller\Setting::TYPE_RADIO:
            case \Cx\Core\Setting\Controller\Setting::TYPE_DATE:
            case \Cx\Core\Setting\Controller\Setting::TYPE_DATETIME:
            case \Cx\Core\Setting\Controller\Setting::TYPE_IMAGE:
                throw new \Exception('Type not supported yet');
                break;
        }
        throw new \Exception('Unsupported type');
    }

    /**
     * Returns $this->values as a parsed array
     *
     * $this->values is one of the following:
     * 1. A callback which returns an array
     * 2. A comma separated list
     * 3. A single value
     * Whether it's 1/2 or 3 can only be determined using the type
     * To check whether it's 1 or 2 we can use preg_match()
     * @todo Add support for currently unsupported types
     * @return array List of valid values
     */
    protected function getValuesAsArray(): array {
        $isArrayType = in_array(
            $this->getType(),
            array(
                \Cx\Core\Setting\Controller\Setting::TYPE_DROPDOWN,
                \Cx\Core\Setting\Controller\Setting::TYPE_DROPDOWN_MULTISELECT,
            )
        );
        if (!$isArrayType) {
            return array($this->getValues());
        }
        $matches = array();
        $values = $this->getValues();
        if (
            preg_match(
                '/^\{src:([a-z0-9_\\\:]+)\(\)\}$/i',
                $values,
                $matches
            )
        ) {
            $values = call_user_func($matches[1]);
            if (is_array($values)) {
                return $values;
            }
        }
        $returnedValues = array();
        foreach (explode(',', $values) as $value) {
            $splitValue = explode(':', $value);
            if (count($splitValue) == 2) {
                $returnedValues[$splitValue[0]] = $splitValue[1];
            } else {
                $returnedValues[] = $value;
            }
        }
        return $returnedValues;
    }

    /**
     * Returns the data of this entity as an array
     * @todo: This should be solved in EntityBase
     * @return array Simple key=>value array of all fields of this entity
     */
    public function toArray(): array {
        return array(
            'id' => $this->getId(),
            'section' => $this->getSection(),
            'group' => $this->getGroup(),
            'name' => $this->getName(),
            'userBased' => $this->getUserBased(),
            'type' => $this->getType(),
            'value' => $this->getValue(),
            'values' => $this->getValues(),
            'ord' => $this->getOrd(),
        );
    }
}
