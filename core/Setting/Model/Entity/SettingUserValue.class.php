<?php declare(strict_types=1);

/**
 * Cloudrexx
 *
 * @link      https://www.cloudrexx.com
 * @copyright Cloudrexx AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

namespace Cx\Core\Setting\Model\Entity;

/**
 * @copyright   Cloudrexx AG
 * @author      Michael Ritter <michael.ritter@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  core_setting
 * Setting
 */
class SettingUserValue extends \Cx\Model\Base\EntityBase {
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var integer
     */
    protected $userId;

    /**
     * @var string
     */
    protected $value;

    /**
     * @var \Cx\Core\Setting\Model\Entity\Setting
     */
    protected $setting;

    public function __construct(?Setting $setting = null, int $userId = 0, string $value = '') {
        foreach (array('setting', 'userId', 'value') as $property) {
            if ($$property) {
                $setter = 'set' . ucfirst($property);
                $this->$setter($$property);
            }
        }
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     * @return SettingUserValue
     */
    public function setUserId($userId) {
        $this->userId = $userId;
    }

    /**
     * Get userId
     *
     * @return integer 
     */
    public function getUserId() {
        return $this->userId;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return SettingUserValue
     */
    public function setValue($value) {
        $this->value = $value;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue() {
        return $this->value;
    }

    /**
     * Set setting
     *
     * @param \Cx\Core\Setting\Model\Entity\Setting $setting
     * @return SettingUserValue
     */
    public function setSetting(\Cx\Core\Setting\Model\Entity\Setting $setting = null) {
        $this->setting = $setting;
    }

    /**
     * Get setting
     *
     * @return \Cx\Core\Setting\Model\Entity\Setting 
     */
    public function getSetting() {
        return $this->setting;
    }
}
