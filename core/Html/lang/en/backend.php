<?php

/**
 * Cloudrexx
 *
 * @link      https://www.cloudrexx.com
 * @copyright Cloudrexx AG
 *
 * This file is part of Cloudrexx.
 *
 * Cloudrexx is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3 of the License.
 *
 * Cloudrexx is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * @copyright   Cloudrexx AG
 * @author      Cloudrexx AG <info@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  module_html
 */

global $_ARRAYLANG;

$_ARRAYLANG['TXT_CORE_HTML_UPDATE_SORT_ORDER_FAILED'] = 'Failed to store the sort order.';
$_ARRAYLANG['TXT_CORE_HTML_ENTITY_NOT_FOUND_ERROR'] = 'The entity <strong>%s</strong> is not found in the component <strong>%s</strong>.';
$_ARRAYLANG['TXT_CORE_HTML_GETTER_SETTER_NOT_FOUND_ERROR'] = 'The entity <strong>%s</strong> has no setter and getter for <strong>%s</strong> or <strong>%s</strong>.';
$_ARRAYLANG['TXT_CORE_HTML_SORTING_NO_ENTITY_FOUND_ERROR'] = 'No entries found for the sorting process.';
$_ARRAYLANG['TXT_CORE_HTML_STATUS_NO_ENTITY_FOUND_ERROR'] = 'No entry found to update.';
$_ARRAYLANG['TXT_CORE_HTML_PLEASE_CHOOSE'] = 'Please choose...';
$_ARRAYLANG['TXT_CORE_HTML_CANT_UPDATE_STATUS'] = 'Status could not be updated.';
$_ARRAYLANG['TXT_CORE_HTML_EXPORT'] = 'Export to CSV with current filter';
$_ARRAYLANG['TXT_CORE_HTML_NEW_BUTTON_MOVED'] = 'Use this to add new entries.';
$_ARRAYLANG['TXT_CORE_HTML_ENTRIES'] = 'Entries';

// Twin Select
$_ARRAYLANG['TXT_CORE_HTML_MULTI_SELECT_ADD_ENTRY'] = 'Assign';
$_ARRAYLANG['TXT_CORE_HTML_MULTI_SELECT_REMOVE_ENTRY'] = 'Remove';
$_ARRAYLANG['TXT_CORE_HTML_MULTI_SELECT_SELECT_ALL'] = 'Select all';
$_ARRAYLANG['TXT_CORE_HTML_MULTI_SELECT_DESELECT_ALL'] = 'Remove all';
