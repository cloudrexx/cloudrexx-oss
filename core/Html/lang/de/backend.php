<?php

/**
 * Cloudrexx
 *
 * @link      https://www.cloudrexx.com
 * @copyright Cloudrexx AG
 *
 * This file is part of Cloudrexx.
 *
 * Cloudrexx is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3 of the License.
 *
 * Cloudrexx is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * @copyright   Cloudrexx AG
 * @author      Cloudrexx AG <info@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  module_html
 */

global $_ARRAYLANG;

$_ARRAYLANG['TXT_CORE_HTML_UPDATE_SORT_ORDER_FAILED'] = 'Das System konnte die Sortierung nicht speichern.';
$_ARRAYLANG['TXT_CORE_HTML_ENTITY_NOT_FOUND_ERROR'] = 'Die Entity <strong>%s</strong> wurde in der Komponente <strong>%s</strong> nicht gefunden.';
$_ARRAYLANG['TXT_CORE_HTML_GETTER_SETTER_NOT_FOUND_ERROR'] = 'Die Entity <strong>%s</strong> hat keine Getter und Setter für die Eigenschaften <strong>%s</strong> oder <strong>%s</strong>';
$_ARRAYLANG['TXT_CORE_HTML_SORTING_NO_ENTITY_FOUND_ERROR'] = 'Keine Einträge zum Sortieren gefunden.';
$_ARRAYLANG['TXT_CORE_HTML_STATUS_NO_ENTITY_FOUND_ERROR'] = 'Kein Eintrag zum Aktualisieren gefunden.';
$_ARRAYLANG['TXT_CORE_HTML_PLEASE_CHOOSE'] = 'Bitte wählen...';
$_ARRAYLANG['TXT_CORE_HTML_CANT_UPDATE_STATUS'] = 'Status konnte nicht aktualisiert werden.';
$_ARRAYLANG['TXT_CORE_HTML_EXPORT'] = 'Mit aktuellem Filter zu CSV exportieren';
$_ARRAYLANG['TXT_CORE_HTML_NEW_BUTTON_MOVED'] = 'Mit dieser Funktion können Sie neue Einträge hinzufügen.';
$_ARRAYLANG['TXT_CORE_HTML_ENTRIES'] = 'Einträge';

// Multi Select Element
$_ARRAYLANG['TXT_CORE_HTML_MULTI_SELECT_ADD_ENTRY'] = 'Zuweisen';
$_ARRAYLANG['TXT_CORE_HTML_MULTI_SELECT_REMOVE_ENTRY'] = 'Entfernen';
$_ARRAYLANG['TXT_CORE_HTML_MULTI_SELECT_SELECT_ALL'] = 'Alle auswählen';
$_ARRAYLANG['TXT_CORE_HTML_MULTI_SELECT_DESELECT_ALL'] = 'Alle entfernen';
