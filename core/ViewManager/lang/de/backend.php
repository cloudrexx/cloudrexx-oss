<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * @copyright   CLOUDREXX CMS - CLOUDREXX AG
 * @author      Cloudrexx Development Team <info@cloudrexx.com>
 * @access      public
 * @package     cloudrexx
 * @subpackage  core_viewmanager
 */
global $_ARRAYLANG;
$_ARRAYLANG['TXT_DESIGN_MANAGEMENT'] = "Layout";
$_ARRAYLANG['TXT_THEME_UNABLE_TO_CREATE'] = ' konnte nicht erstellt werden';
$_ARRAYLANG['TXT_VIEWMANAGER_OVERVIEW'] = 'Übersicht';
$_ARRAYLANG['TXT_VIEWMANAGER_TEMPLATE_EDITOR'] = 'Theme Manager';
$_ARRAYLANG['TXT_DESIGN_SETTINGS'] = 'Einstellungen';
$_ARRAYLANG['TXT_DESIGN_FILES_ADMINISTRATION'] = 'Dateiverwaltung';
$_ARRAYLANG['TXT_CORE_PLACEHOLDERS'] = 'Platzhalter';
$_ARRAYLANG['TXT_THEME_IMPORT_EXPORT'] = 'Schnittstellen';
$_ARRAYLANG['TXT_CHOOSE_TEMPLATE_GROUP'] = 'Theme Gruppe auswählen';
$_ARRAYLANG['TXT_SELECT_FILE'] = 'Datei auswählen';
$_ARRAYLANG['TXT_DESIGN_FOLDER'] = 'Theme Ordner ';
$_ARRAYLANG['TXT_TEMPLATE_FILES'] = 'Vorlagen Dateien';
$_ARRAYLANG['TXT_FILE_EDITOR'] = 'Dateieditor';
$_ARRAYLANG['TXT_UPLOAD_FILES'] = 'Dateien Hochladen';
$_ARRAYLANG['TXT_CREATE_FILE'] = 'Datei erstellen';
$_ARRAYLANG['TXT_DELETE'] = 'Löschen';
$_ARRAYLANG['TXT_SAVE'] = 'Speichern';
$_ARRAYLANG['TXT_RESET'] = 'Zurücksetzen';
$_ARRAYLANG['TXT_SELECT_ALL'] = 'Alles markieren';
$_ARRAYLANG['TXT_MANAGE_FILES'] = 'Dateien verwalten';
$_ARRAYLANG['TXT_SELECT_THEME'] = 'Theme auswählen';
$_ARRAYLANG['TXT_THEME_NAME'] = 'Theme-Name';
$_ARRAYLANG['TXT_MODE'] = 'Modus';
$_ARRAYLANG['TXT_SETTINGS_MODFIY'] = 'Bearbeiten';
$_ARRAYLANG['TXT_CREATE'] = 'Erstellen';
$_ARRAYLANG['TXT_THEME_NOT_COMPONENT'] = 'Das Theme %s wurde noch nicht auf die neue Version migriert. Dadurch können Sie einige Funktionen nicht verwenden.';
$_ARRAYLANG['TXT_THEME_THEMES'] = 'Themes';
$_ARRAYLANG['TXT_THEME_PREVIEW'] = 'Vorschau';
$_ARRAYLANG['TXT_THEMES_CREATE'] = 'Theme hinzufügen';
$_ARRAYLANG['TXT_THEME_IMPORT'] = 'Theme importieren';
$_ARRAYLANG['TXT_THEME_DO_IMPORT'] = 'Importieren';
$_ARRAYLANG['TXT_THEME_IMPORT_THEME'] = 'Theme importieren';
$_ARRAYLANG['TXT_THEME_EXPORT'] = 'Export';
$_ARRAYLANG['TXT_THEME_PREVIEW_NEW_WINDOW'] = 'Vorschau in neuem Fenster';
$_ARRAYLANG['TXT_THEME_DIRECTORY_NAME'] = 'Verzeichnisname';
$_ARRAYLANG['TXT_THEME_SEND_ARCHIVE'] = 'Archiv senden';
$_ARRAYLANG['TXT_THEME_IMPORT_ARCHIVE'] = 'Archiv importieren';
$_ARRAYLANG['TXT_THEME_LOCAL_FILE'] = 'Theme hochladen (ZIP-Archiv)';
$_ARRAYLANG['TXT_THEME_SPECIFY_URL'] = 'Direkt von einer Website herunterladen (via URL)';
$_ARRAYLANG['TXT_THEME_CONFIRM_DELETE'] = 'Theme %s wirklich löschen?';
$_ARRAYLANG['TXT_FUNCTIONS'] = 'Funktionen';
$_ARRAYLANG['TXT_NAME'] = 'Name';
$_ARRAYLANG['TXT_THEME_SHOW_PRINT_THEME'] = 'Druckansicht auflisten';
$_ARRAYLANG['TXT_THEME_NO_URL_SPECIFIED'] = 'Keine URL angegeben';
$_ARRAYLANG['TXT_THEME_NO_FILE_SPECIFIED'] = 'Keine Datei angegeben';
$_ARRAYLANG['TXT_THEME_DETAILS'] = 'Details';
$_ARRAYLANG['TXT_THEME_IMPORT_INFO'] = 'Importieren';
$_ARRAYLANG['TXT_THEME_IMPORT_INFO_BODY'] = 'Geben Sie oben die Quelle des Themes an. Es kann eine lokale ZIP-Datei hochgeladen werden oder es kann eine https://-Adresse zu einer externen ZIP-Datei angegeben werden';
$_ARRAYLANG['TXT_SKINS_PREVIEW'] = 'Vorschau';
$_ARRAYLANG['TXT_THEME_PREVIEW_IMAGE'] = 'Vorschaubild';
$_ARRAYLANG['TXT_THEME_PREVIEW_URL'] = 'Vorschau URL';
$_ARRAYLANG['TXT_THEME_INFORMATION'] = 'Informationen';
$_ARRAYLANG['TXT_LANGUAGES'] = 'Sprachen';
$_ARRAYLANG['TXT_AUTHOR'] = 'Autor';
$_ARRAYLANG['TXT_VERSION'] = 'Version';
$_ARRAYLANG['TXT_DESCRIPTION'] = 'Beschreibung';
$_ARRAYLANG['TXT_CORE_CM_ACTION_PUBLISH'] = 'Aktivieren';
$_ARRAYLANG['TXT_ACTIVATE_DESIGN'] = 'Übersicht Vorlagen';
$_ARRAYLANG['TXT_THEME_IMPORT_WRONG_MIMETYPE'] = 'Falscher Medien-Typ oder keine Datei';
$_ARRAYLANG['TXT_COULD_NOT_UPLOAD_FILE'] = 'Die Datei konnte nicht hochgeladen werden';
$_ARRAYLANG['TXT_THEME_ARCHIVE_WRONG_STRUCTURE'] = 'Falsche Archivstruktur';
$_ARRAYLANG['TXT_THEME_FOLDER_ALREADY_EXISTS'] = 'Dieser Theme Ordner existiert bereits';
$_ARRAYLANG['TXT_THEME_FOLDER_DELETE_FIRST'] = 'Bitte löschen Sie zuerst das alte Theme';
$_ARRAYLANG['TXT_THEME_ARCHIVE_ERROR'] = 'Archivfehler';
$_ARRAYLANG['TXT_THEME_SUCCESSFULLY_IMPORTED'] = 'wurde erfolgreich importiert';
$_ARRAYLANG['TXT_THEME_HTTP_CONNECTION_FAILED'] = 'Konnte keine HTTPS Verbindung herstellen';
$_ARRAYLANG['TXT_THEME_FOLDER_DOES_NOT_EXISTS'] = 'Verzeichnis existiert nicht';
$_ARRAYLANG['TXT_DATA_RECORD_UPDATED_SUCCESSFUL'] = 'Die Daten wurden erfolgreich aktualisiert';
$_ARRAYLANG['TXT_DATABASE_QUERY_ERROR'] = 'Fehler beim Ausführen der Datenbankabfrage!';
$_ARRAYLANG['TXT_OVERVIEW'] = 'Übersicht';
$_ARRAYLANG['TXT_ID'] = 'ID';
$_ARRAYLANG['TXT_LANGUAGE'] = 'Sprache';
$_ARRAYLANG['TXT_ACTIVE_TEMPLATE'] = 'Standard';
$_ARRAYLANG['TXT_ACTIVE_PDF_TEMPLATE'] = 'PDF';
$_ARRAYLANG['TXT_ACTIVE_PRINT_TEMPLATE'] = 'Druck';
$_ARRAYLANG['TXT_THEME_ACTIVATE_INFO'] = 'Aktivierte Sprachen';
$_ARRAYLANG['TXT_ACTIVE_MOBILE_TEMPLATE'] = 'Mobile';
$_ARRAYLANG['TXT_APP'] = 'App';
$_ARRAYLANG['TXT_DESIGN_VARIABLES_LIST'] = 'Platzhalterverzeichnis';
$_ARRAYLANG['TXT_STANDARD_TEMPLATE_STRUCTURE'] = 'Beispiel Theme System';
$_ARRAYLANG['TXT_FRONTEND_EDITING_LOGIN_FRONTEND'] = 'Frontend-Editing';
$_ARRAYLANG['TXT_STARTPAGE'] = 'Startseite';
$_ARRAYLANG['TXT_STANDARD_PAGES'] = 'Standardseiten';
$_ARRAYLANG['TXT_REPLACEMENT_LIST'] = 'Platzhalter';
$_ARRAYLANG['TXT_FILES'] = 'Dateien';
$_ARRAYLANG['TXT_CONTENTS'] = 'Inhalte';
$_ARRAYLANG['TXT_DESIGN_REPLACEMENTS_DIR'] = 'Platzhalterverzeichnis';
$_ARRAYLANG['TXT_PLACEHOLDER_DIRECTORY_DESCRIPTION'] = 'Eine detaillierte Platzhalter-Liste finden Sie im <a href="https://doc.cloudrexx.com" target="_blank">Platzhalterverzeichnis</a>.';
$_ARRAYLANG['TXT_CHANNELS'] = 'Ansichten';
$_ARRAYLANG['TXT_MODULE_URLS'] = 'Verlinkungen Grundanwendungen';
$_ARRAYLANG['TXT_CONTACT'] = 'Kontakt';
$_ARRAYLANG['TXT_NEW_DIRECTORY'] = 'Neues Verzeichnis';
$_ARRAYLANG['TXT_EXISTING_DIR_NAME'] = 'oder bestehendes Verzeichnis verwenden';
$_ARRAYLANG['TXT_DIR_NAME'] = 'Neuer Verzeichnisname';
$_ARRAYLANG['TXT_DB_NAME'] = 'Anzeigename';
$_ARRAYLANG['TXT_SELECT_DIR'] = 'Verzeichnis wählen';
$_ARRAYLANG['TXT_FROM_TEMPLATE'] = 'Vorlage kopieren?';
$_ARRAYLANG['TXT_STATUS_CHECK_INPUTS'] = 'Bitte Eingaben prüfen.';
$_ARRAYLANG['TXT_STATUS_SUCCESSFULLY_CREATE'] = 'Erfolgreich erstellt.';
$_ARRAYLANG['TXT_MSG_ERROR_NEW_DIR'] = 'Fehler: Neuer Ordner kann nicht erstellt werden!';
$_ARRAYLANG['TXT_STATUS_SUCCESSFULLY_DELETE'] = 'Erfolgreich gelöscht.';
$_ARRAYLANG['TXT_STATUS_FILE_CURRENTLY_OPEN'] = 'Datei ist bereits geöffnet.';
$_ARRAYLANG['TXT_STATUS_CANNOT_DELETE'] = 'Kann nicht gelöscht werden.';
$_ARRAYLANG['TXT_THEME_LIBRARY_AUTOMATICALLY_ADJUSTED'] = 'Die Einstellung der Bibliothek(en) %s wurde aufgrund von Abhängigkeiten zu anderen Bibliotheken automatisch angepasst.';
$_ARRAYLANG['TXT_COULD_NOT_WRITE_TO_FILE'] = 'Konnte nicht in Datei schreiben';
$_ARRAYLANG['TXT_TEMPLATE_USED_LIBRARIES'] = 'Verwendete Bibliotheken';
$_ARRAYLANG['TXT_THEME_LIBRARY_NOT_USED'] = 'nicht verwenden';
$_ARRAYLANG['TXT_DEFAULT'] = 'Standard';
$_ARRAYLANG['TXT_THEME_PRINT'] = 'Druckansicht';
$_ARRAYLANG['TXT_THEME_PDF'] = 'PDF-Ansicht';
$_ARRAYLANG['TXT_APP_VIEW'] = 'App-Ansicht';
$_ARRAYLANG['TXT_DESIGN_LAYOUT'] = 'Layout';
$_ARRAYLANG['TXT_DESIGN_APPLICATION_TEMPLATE'] = 'Anwendungsvorlagen';
$_ARRAYLANG['TXT_DESIGN_CONTENT_TEMPLATE'] = 'Inhaltsvorlagen';
$_ARRAYLANG['TXT_DESIGN_HOME_TEMPLATE'] = 'Vorlagen für Startseite';
$_ARRAYLANG['TXT_STATUS_CANNOT_OPEN'] = 'Die Dateien können nicht geöffnet werden.';
$_ARRAYLANG['TXT_CHOOSE_DESIGN'] = 'Theme wählen';
$_ARRAYLANG['TXT_UNABLE_TO_CREATE_FILE'] = 'Kann die Datei %s nicht erstellen!';
$_ARRAYLANG['TXT_STATUS_CANNOT_WRITE'] = 'Datei nicht beschreibbar.';
$_ARRAYLANG['TXT_THEME_ACTIVATION_LABEL'] = 'Wählen Sie die gewünschten Sprachen für die Aktivierung aus:';
$_ARRAYLANG['TXT_THEME_ADD_NEW'] = "Neues Theme hinzufügen";
$_ARRAYLANG['TXT_THEME_FOLDER_NAME_NOT_ALLOWED'] = 'Der Verzeichnisname %s ist ungültig. Bitte wählen Sie einen anderen Namen.';
$_ARRAYLANG['TXT_THEME_FILE_RENAME_SUCCESS'] = 'Datei erfolgreich umbenannt.';
$_ARRAYLANG['TXT_THEME_FOLDER_RENAME_SUCCESS'] = 'Verzeichnis erfolgreich umbenannt.';
$_ARRAYLANG['TXT_THEME_FILE_DELETE_SUCCESS'] = 'Datei %s ist erfolgreich gelöscht.';
$_ARRAYLANG['TXT_THEME_FOLDER_DELETE_SUCCESS'] = 'Verzeichnis %s erfolgreich gelöscht.';
$_ARRAYLANG['TXT_THEME_FILE_CREATE_SUCCESS'] = 'Die Datei %s wurde erfolgreich erstellt.';
$_ARRAYLANG['TXT_THEME_FOLDER_CREATE_SUCCESS'] = 'Das Verzeichnis %s wurde erfolgreich erstellt.';
$_ARRAYLANG['TXT_THEME_ACTIVATE'] = "Aktivieren";
$_ARRAYLANG['TXT_THEME_ACTIVATE_THEME'] = "Theme aktivieren";
$_ARRAYLANG['TXT_THEME_CANCEL'] = "Abbrechen";
$_ARRAYLANG['TXT_THEMES_LIBRARIES'] = "Bibliotheken";
$_ARRAYLANG['TXT_THEME_EDIT_FILE'] = "Datei";
$_ARRAYLANG['TXT_THEME_NEW_FILE'] = "Datei erstellen";
$_ARRAYLANG['TXT_THEME_NEW_FOLDER'] = "Verzeichnis erstellen";
$_ARRAYLANG['TXT_THEME_NEW_THEME'] = 'Theme erstellen';
$_ARRAYLANG['TXT_THEME_CREATE_NEW_FILE'] = "Neue Datei erstellen";
$_ARRAYLANG['TXT_THEME_CREATE_NEW_FOLDER'] = "Neues Verzeichnis erstellen";
$_ARRAYLANG['TXT_THEME_CREATE'] = "Erstellen";
$_ARRAYLANG['TXT_THEME_FILE_NAME'] = "Dateiname";
$_ARRAYLANG['TXT_THEME_RENAME_FILE_OPERATION'] = "Datei umbenennen";
$_ARRAYLANG['TXT_THEME_RENAME_FOLDER_OPERATION'] = "Verzeichnis umbenennen";
$_ARRAYLANG['TXT_THEME_RENAME'] = "Umbenennen";
$_ARRAYLANG['TXT_THEME_REMOVE'] = 'Löschen';
$_ARRAYLANG['TXT_THEME_RESET'] = 'Originalzustand wiederherstellen';
$_ARRAYLANG['TXT_THEME_OPERATION_FAILED_FOR_EMPTY_NAME'] = 'Bitte wählen Sie einen gültigen Namen.';
$_ARRAYLANG['TXT_THEME_OPERATION_FAILED_FOR_VIRTUAL_FOLDER'] = 'Der ausgewählte Name ist geschützt. Bitte wählen Sie einen anderen Namen.';
$_ARRAYLANG['TXT_THEME_OPERATION_FAILED_FOR_RENAME_VIRTUAL_FOLDER'] = 'Der ausgewählte Name ist geschützt. Bitte wählen Sie einen anderen Namen.';
$_ARRAYLANG['TXT_THEME_OPERATION_FAILED_FOR_FILE_ALREADY_EXITS'] = 'Eine Datei mit dem Namen %s besteht bereits. Bitte wählen Sie einen anderen Namen.';
$_ARRAYLANG['TXT_THEME_OPERATION_FAILED_FOR_FILE_NOT_EXITS'] = 'Die Datei oder das Verzeichnis %s ist nicht mehr verfügbar.';
$_ARRAYLANG['TXT_THEME_FILE_EXTENSION_NOT_ALLOWED'] = "Die Datei verwendet eine nicht unterstützte Dateierweiterung. Bitte wählen Sie eine andere Dateierweiterung.";
$_ARRAYLANG['TXT_THEME_RENAME_FAILED'] = "Umbenennung fehlgeschlagen. Bitte versuchen Sie es erneut.";
$_ARRAYLANG['TXT_THEME_DELETE_FAILED'] = "Löschung fehlgeschlagen. Bitte versuchen Sie es erneut.";
$_ARRAYLANG['TXT_THEME_NEWFILE_FAILED'] = "Erstellung fehlgeschlagen. Bitte versuchen Sie es erneut.";
$_ARRAYLANG['TXT_THEME_CONFIRM_DELETE_FILE'] = "Bitte bestätigen Sie die unwiderrufliche Löschung der Datei %s";
$_ARRAYLANG['TXT_THEME_CONFIRM_DELETE_FOLDER'] = "Bitte bestätigen Sie die unwiderrufliche Löschung des Verzeichnisses %s";
$_ARRAYLANG['TXT_THEME_CONFIRM_RESET_FILE'] = "Bitte bestätigen Sie die Wiederherstellung des originalen Zustandes der ausgewählten Datei";
$_ARRAYLANG['TXT_THEME_CONFIRM_RESET_FOLDER'] = "Bitte bestätigen Sie die Wiederherstellung des originalen Zustandes des ausgewählten Verzeichnisses";
$_ARRAYLANG['TXT_THEME_FILESYSTEM'] = 'Aus dem Verzeichnis %s importieren';
$_ARRAYLANG['TXT_VIEWMANAGER_THEME_SELECTION_TXT'] = 'Wählen Sie die gewünschte Quelle aus, von der Sie ein neues Theme importieren möchten.';
$_ARRAYLANG['TXT_VIEWMANAGER_THEME'] = 'Theme';
$_ARRAYLANG['TXT_VIEWMANAGER_SOURCE'] = 'Quelle';
$_ARRAYLANG['TXT_THEME_DELETE_CUSTOM_ACTIVE_ALERT'] = "Das ausgewählte Theme wird zurzeit für die Darstellung individueller Inhaltsseiten verwendet. Falls Sie dieses Theme löschen, werden die betroffenen Inhaltsseiten anschliessend wieder mit dem hinterlegten Standard-Theme angezeigt.<br><br>Wichtig: Dieser Vorgang kann nicht rückgängig gemacht werden!<br><br>Bitte bestätigen Sie den Vorgang.";
$_ARRAYLANG['TXT_THEME_DELETE_ALERT'] = "Dieser Vorgang wird das ausgewählte Theme löschen.<br><br>Wichtig: Dieser Vorgang kann nicht rückgängig gemacht werden!<br><br>Bitte bestätigen Sie den Vorgang.";
$_ARRAYLANG['TXT_THEME_DELETE_DIALOG_TITLE'] = "Ausgewähltes Theme löschen";
$_ARRAYLANG['TXT_THEME_DELETE_CURRENT_ACTIVE_ALERT'] = "Dieses Theme kann nicht gelöscht werden, da es zurzeit verwendet wird.";
$_ARRAYLANG['TXT_THEME_FILE_RESET_SUCCESS'] = 'Die Datei wurde erfolgreich zurückgesetzt.';
$_ARRAYLANG['TXT_THEME_FOLDER_RESET_SUCCESS'] = 'Das Verzeichnis wurde erfolgreich zurückgesetzt.';
$_ARRAYLANG['TXT_THEME_RESET_FAILED'] = "Die Rücksetzung ist fehlgeschlagen. Bitte versuchen Sie es erneut.";
$_ARRAYLANG['TXT_THEME_FULLSCREEN'] = 'Vollbild';
$_ARRAYLANG['TXT_THEME_EXIT_FULLSCREEN'] = 'Vollbild Modus verlassen';
$_ARRAYLANG['TXT_THEME_TEMPLATEEDITOR_USABLE'] = 'Editor';
$_ARRAYLANG['TXT_THEME_TEMPLATEEDITOR_UNUSABLE'] = 'Code only';
$_ARRAYLANG['TXT_THEME_TEMPLATEEDITOR_EDIT'] = 'Anpassen';
$_ARRAYLANG['TXT_THEME_ERROR_IN_INSERT_THEME'] = 'Das Theme konnte nicht in der Datenbank gespeichert werden.';
$_ARRAYLANG['TXT_UNABLE_TO_CONVERT_THEME_TO_COMPONENT'] = 'Das Theme konnte nicht korrekt eingelesen werden. Stellen Sie sicher, dass auf das Verzeichnis im Dateisystem Schreibzugriff besteht.';
$_ARRAYLANG['TXT_CORE_VIEWMANAGER_LOADING'] = "Laden...";
$_ARRAYLANG['TXT_VIEWMANAGER_INVALID_FILE_ENCODING_MSG'] = "Achtung: Die Datei %s enthält ungültige Zeichen (Code Unit Sequenzen). Die ungültigen Zeichen wurden mit einem Unicode Ersatz Zeichen (U+FFFD) ersetzt.";
$_ARRAYLANG['TXT_VIEWMANAGER_CONFIRM_INVALID_FILE_ENCODING'] = "Mit dem Speichervorgang gehen die ungültigen Zeichen verloren. Wollen Sie die Datei trotzdem speichern?";
$_ARRAYLANG['TXT_CORE_VIEWMANAGER_THEME_TOO_MANY_FILES_WARNING'] = 'Das Theme %s enthält sehr viele Dateien. Zur Verbesserung der Performance Ihrer Website sollte das Theme optimiert werden.';
