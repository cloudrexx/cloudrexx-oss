<?php declare(strict_types=1);

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * @copyright   Cloudrexx AG
 * @author      Michael Ritter <michael.ritter@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  core_mediasource
 */

namespace Cx\Core\MediaSource\Model\Event;

/**
 * Class IndexerSearchResultEventListener
 *
 * Lists results from indexer in global search results
 * WARNING: This is a dirty hack!! Every component should return it's own results
 * @todo Drop this code!
 * @deprecated  Each component should return it's own indexer results instead!
 * @copyright   Cloudrexx AG
 * @author      Michael Ritter <michael.ritter@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  core_mediasource
 */
class IndexerSearchResultEventListener extends \Cx\Core\Event\Model\Entity\DefaultEventListener {

    protected function areIndexerResultsTurnedOn() {
        return \Cx\Core\Setting\Controller\Setting::getValue(
            'CLX4188_IncludeIndexedFilesInGlobalSearch',
            'Config'
        ) == 'on';
    }

    public function SearchFindContent($search) {
        if (!$this->areIndexerResultsTurnedOn()) {
            return;
        }

        // fetch search results
        $result = new \Cx\Core_Modules\Listing\Model\Entity\DataSet(
            $this->getResultForSearchComponent(
                $search
            )
        );

        // attach results to search component
        $search->appendResult($result);
    }

    protected function getResultForSearchComponent(
        \Cx\Core_Modules\Search\Controller\Search $search
    ) {
        $searchResult = array();
        $em = $this->cx->getDb()->getEntityManager();
        $qb = $em->createQueryBuilder();
        $query = $qb->select(
            'ie'
        )->from(
            'Cx\Core\MediaSource\Model\Entity\IndexerEntry', 'ie'
        )->where(
            $qb->expr()->like('ie.path', ':searchterm')
        )->orWhere(
            $qb->expr()->like(
                'ie.content', ':searchterm'
            )
        )->setParameters(
            array('searchterm' => '%'.$search->getTerm().'%')
        )->setFirstResult(
            null
        )->setMaxResults(
            null
        )->getQuery();
        $matches = $qb->getQuery()->getResult();
        foreach ($matches as $match) {
            $fileInformation['Score'] = 100;
            $fileInformation['Title'] = ucfirst(
                basename(
                    $match->getPath()
                )
            );
            $fileInformation['Content'] = $search->parseContentForResultDescription(
                $match->getContent()
            );
            $fileInformation['Link'] = '/api/IndexDownload?path=' . urlencode($match->getPath());
            $fileInformation['Component'] = 'MediaSource';
            array_push($searchResult, $fileInformation);
        }
        return $searchResult;
    }
}
