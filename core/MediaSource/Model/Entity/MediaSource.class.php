<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * @copyright   Cloudrexx AG
 * @author      Robin Glauser <robin.glauser@comvation.com>
 * @package     cloudrexx
 * @subpackage  coremodule_mediabrowser
 */

namespace Cx\Core\MediaSource\Model\Entity;

use Cx\Core\DataSource\Model\Entity\DataSource;

/**
 * MediaSource Exception
 * @copyright   Cloudrexx AG
 * @author      Reto Kohli <reto.kohli@comvation.com>
 * @package     cloudrexx
 * @subpackage  core_mediasource
 */
class MediaSourceException extends \Exception
{
    /**
     * Supported status codes
     */
    const STATUS_200 = 200;
    const STATUS_400 = 400;
    const STATUS_403 = 403;
    const STATUS_404 = 404;

    /**
     * Text for known status
     */
    const STATUS_TEXT = [
        200 => 'Success',
        400 => 'Bad Request',
        403 => 'Forbidden',
        404 => 'Not Found',
    ];

    /**
     * Construct a MediaSourceException
     *
     * Used to pass the status back to the caller.
     * Applies the default code or message for 400 Bad Request
     * if either is empty or invalid.
     * @param   string      $message
     * @param   int         $code
     * @param   \Throwable  $previous
     * @return  \MediaSourceException
     */
    public function __construct(
        string $message = '',
        int $code = 0,
        \Throwable $previous = null
    ) {
        if (!$code || !array_key_exists($code, static::STATUS_TEXT)) {
            $code = static::STATUS_400;
        }
        if (!$message) {
            $message = static::STATUS_TEXT[$code];
        }
        parent::__construct($message, $code, $previous);
    }

}

/**
 * Class MediaSource
 *
 * @copyright   Cloudrexx AG
 * @author      Robin Glauser <robin.glauser@comvation.com>
 * @package     cloudrexx
 * @subpackage  coremodule_mediabrowser
 */
class MediaSource extends DataSource {

    /**
     * List of operations supported by this DataSource
     * @var array List of operations
     */
    protected $supportedOperations = array();

    /**
     * Name of the mediatype e.g. files, shop, media1
     * @var string
     */
    protected $name;

    /**
     * @var int
     */
    protected $position;

    /**
     * Human readable name
     * @var string
     */
    protected $humanName;

    /**
     * Array with the web and normal path to the directory.
     *
     * e.g:
     * array(
     *      $this->cx->getWebsiteImagesContentPath(),
     *      $this->cx->getWebsiteImagesContentWebPath(),
     * )
     *
     * @var array
     */
    protected $directory = array();

    /**
     * Array with access ids to use with \Permission::checkAccess($id, 'static', true)
     *
     * Accepts various formats. Most basic is as follows:
     * <code>array(
     *     <id>[,<id>,...]
     * )</code>
     *
     * This will set the access as follows:
     * - for both, read and write access, the user must have access to every
     *   listed id (as specified by <id>).
     *
     * Use the following format instead, to change the requirement of having
     * access to all listed IDs, but instead only one of the listed IDs:
     * <code>array(
     * 	   'any' => array(
     *         <id>[,<id>,...]
     *     ),
     * )</code>
     *
     * To set different read and write access permissions, do specify as
     * follows:
     * <code>array(
     *     'read' => array(
     *         <id>[,<id>,...]
     *     ),
     *     'write' => array(
     *         <id>[,<id>,...]
     *     ),
     * )</code>
     *
     * Again, to make the user only require having access to one of the listed
     * IDs, do specify as follows:
     * <code>array(
     *     'read' => array(
     *         'any' => array(
     *             <id>[,<id>,...]
     *         ),
     *     ),
     *     'write' => array(
     *         'any' => array(
     *             <id>[,<id>,...]
     *         ),
     *     ),
     * )</code>
     *
     * @var array
     */
    protected $accessIds = array();

    /**
     * @var FileSystem
     */
    protected $fileSystem;

    /**
     * @var bool if indexer is activated
     */
    protected $isIndexingActivated;

    /**
     * @var \Cx\Core\Core\Model\Entity\SystemComponentController $systemComponentController
     */
    protected $systemComponentController;


    public function __construct($name,$humanName, $directory, $accessIds = array(), $position = '',FileSystem $fileSystem = null, \Cx\Core\Core\Model\Entity\SystemComponentController $systemComponentController = null, $isIndexingActivated = true) {
        $this->fileSystem = $fileSystem ? $fileSystem : LocalFileSystem::createFromPath($directory[0]);
        $this->name      = $name;
        $this->position  = $position;
        $this->humanName = $humanName;
        $this->directory = $directory;
        $this->accessIds = $accessIds;
        $this->setIndexingActivated($isIndexingActivated);

        // Sets provided SystemComponentController
        $this->systemComponentController = $systemComponentController;
        if (!$this->systemComponentController) {
            // Searches a SystemComponentController intelligently by RegEx on backtrace stack frame
            $traces = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 2);
            $trace = end($traces);
            if (empty($trace['class'])) {
                throw new MediaBrowserException('No SystemComponentController for ' . __CLASS__ . ' can be found');
            }
            $matches = array();
            preg_match(
                '/Cx\\\\(?:Core|Core_Modules|Modules)\\\\([^\\\\]*)\\\\/',
                $trace['class'],
                $matches
            );
            $this->systemComponentController = $this->getComponent($matches[1]);
        }
    }

    /**
     * Define if indexer is activated
     *
     * @param $activated
     *
     * @return void
     */
    public function setIndexingActivated($activated)
    {
        $this->isIndexingActivated = $activated;
    }

    /**
     * Get information if indexer is activated
     *
     * @return bool
     */
    public function isIndexingActivated()
    {
        return $this->isIndexingActivated;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return array
     */
    public function getDirectory()
    {
        return $this->directory;
    }


    /**
     * @return array
     */
    public function getAccessIds()
    {
        return $this->accessIds;
    }

    /**
     * @param array $accessIds See DocBlock of {link $this->accessIds}
     */
    public function setAccessIds($accessIds)
    {
        $this->accessIds = $accessIds;
    }

    /**
     * @param   string  $scope  Set to 'read' or 'write' to check for specific
     *                          access permissions.
     * @return bool
     */
    public function checkAccess($scope = 'read'){
        // fetch set access IDs on MediaSource
        if (
            !empty($scope) &&
            isset($this->accessIds[$scope])
        ) {
            $accessIds = $this->accessIds[$scope];
        } else {
            $accessIds = $this->accessIds;
        }

        // check if the user must have permission to all access IDs
        $requiresAll = true;
        $modifier = 'all';
        if (is_array(current($accessIds))) {
            $modifier = current(array_keys($accessIds));
            $accessIds = current($accessIds);
        }
        if ($modifier == 'any') {
            $requiresAll = false;
        }

        // finally, perform access check
        foreach ($accessIds as $id){
            // in case the user requires having access to only one access ID,
            // then we can stop now
            if (
                \Permission::checkAccess($id, 'static', true) &&
                !$requiresAll
            ) {
                return true;
            }

            // in case the user requires having access to all specified access
            // IDs, then we have to abort here in case the user is missing
            // access to any IDs
            if (
                !\Permission::checkAccess($id, 'static', true) &&
                $requiresAll
            ) {
                return false;
            }
        }

        // in case the user only requires access to one access ID, but this
        // line of code is reached, then this means that the user did not
        // have access to any of the specified access IDs. Therefore, the
        // user does not have access permission to this MediaSource.
        if (!$requiresAll) {
            return false;
        }

        return true;
    }

    /**
     * @return string
     */
    public function getHumanName()
    {
        return $this->humanName;
    }

    /**
     * @param string $humanName
     */
    public function setHumanName($humanName)
    {
        $this->humanName = $humanName;
    }

    /**
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param int $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * @return FileSystem
     */
    public function getFileSystem() {
        return $this->fileSystem;
    }

    /**
     * @return \Cx\Core\Core\Model\Entity\SystemComponentController
     */
    public function getSystemComponentController() {
        return $this->systemComponentController;
    }

    /**
     * Returns a list of field names this DataSource consists of
     * @return array List of field names
     */
    public function listFields() {
        throw new \Exception('Not yet implemented');
    }

    /**
     * @inheritdoc
     */
    public function getIdentifierFieldNames() {
        return array('filename');
    }

    /**
     * @inheritdoc
     */
    public function get(
        $elementId = array(),
        $filter = array(),
        $order = array(),
        $limit = 0,
        $offset = 0,
        &$count = 0,
        $fieldList = array()
    ) {
        throw new \Exception('Not yet implemented');

        // The following code is beta. We need to define what MediaSource
        // returns: Binary file data or Metadata/file lists or both
        /*$fileList = $this->getMediaSource()->getFileSystem()->getFileList('');
        if (count($elementId) && $fileList[current($elementId)]) {
            return array(current($elementId) => $fileList[current($elementId)]);
        }
        return $fileList;*/
    }

    /**
     * Returns the real instance of this MediaSource
     *
     * DataSources are loaded from DB, MediaSources are loaded via event hooks,
     * MediaSource is a DataSource  -->  MediaSources cannot be loaded from
     * DB yet. As soon as this is possible this can be removed.
     * @return MediaSource Real instance of this MediaSource
     */
    protected function getMediaSource() {
        // force access
        try {
            $mediaSource = $this->cx->getMediaSourceManager()->getMediaType(
                $this->getIdentifier()
            );
        } catch (\Cx\Core\MediaSource\Model\Entity\MediaSourceManagerException $e) {
            $mediaSource = $this->cx->getMediaSourceManager()->getLockedMediaType(
                $this->getIdentifier()
            );
        }
        if (!$mediaSource) {
            throw new \Exception('MediaSource not found');
        }
        return $mediaSource;
    }

    /**
     * Adds a new entry to this DataSource
     * @todo Chunked upload is untested and will most likely not work
     * @param array $data Field=>value-type array. Not all fields may be required.
     * @throws \Exception If something did not go as planned
     */
    public function add($data) {
        $mediaSource = $this->getMediaSource();
        // $data['path'] is not the file system path to the file, but a
        // combination of MediaSource identifier and a file system path.
        // Therefore using $this->getIdentifier() is intended and correct.
        // See JsonUploader::upload()
        $data['path'] = $this->getIdentifier() . '/';
        $jd = new \Cx\Core\Json\JsonData();
        $res = $jd->data(
            'Uploader',
            'upload',
            array('get' => '', 'post' => $data, 'mediaSource' => $mediaSource)
        );
        if ($res['status'] != 'success' || $res['data']['OK'] !== 1) {
            throw new \Exception('Upload failed: ' . $res['message']);
        }
        return true;
    }

    /**
     * Updates an existing entry of this DataSource
     * @param array $elementId field=>value-type condition array identifying an entry
     * @param array $data Field=>value-type array. Not all fields are required.
     * @throws \Exception If something did not go as planned
     */
    public function update($elementId, $data) {
        $this->remove($elementId);
        return $this->add($data);
    }

    /**
     * Drops an entry from this DataSource
     * @param array $elementId field=>value-type condition array identifying an entry
     * @throws \Exception If something did not go as planned
     */
    public function remove($elementId) {
        $mediaSource = $this->getMediaSource();
        $fs = $mediaSource->getFileSystem();
        $filename = '/' . implode('/', $elementId);
        $file = $fs->getFileFromPath($filename);
        if (!$file) {
            throw new \Exception('File "' . $filename . '" not found!');
        }
        return $fs->removeFile($file);
    }

    /**
     * Get all matches from search term.
     *
     * @param $searchterm string term to search
     * @param $path       string path to search in
     * @param $limit      int|null (optional) Limits the max number of results
     * @param $offset     int|null (optional) Offset if $limit is used
     * @param $count      int (optional, reference) Total number of results
     * @todo This currently only works for indexers that use the default
     *          index table. To allow other ways of storage a storage driver
     *          should be introduced. Indexers using the same storage driver
     *          could then still be queried at once.
     * @throws \Cx\Core\Core\Model\Entity\SystemComponentException
     * @return array Search results as specified by Search component
     */
    public function getFileSystemMatches($searchterm, $path, $limit = null, $offset = null, &$count = 0)
    {
        if (!$this->isIndexingActivated()) {
            return array();
        }
        $searchLength = \Cx\Core\Setting\Controller\Setting::getValue(
            'searchDescriptionLength'
        );
        $fullPath = $this->getDirectory()[0] . $path;
        $fileList = array();
        $searchResult = array();

        $componentName = '';
        if (!empty($this->getSystemComponentController())) {
            $componentName = $this->getSystemComponentController()->getName();
        }
        $em = $this->cx->getDb()->getEntityManager();
        $qb = $em->createQueryBuilder();
        $query = $qb->select(
            'count(ie.id)'
        )->from(
            'Cx\Core\MediaSource\Model\Entity\IndexerEntry', 'ie'
        )->where(
            $qb->expr()->like('ie.path', ':path')
        )->andWhere(
            $qb->expr()->like(
                'ie.content', ':searchterm'
            )
        )->setParameters(
            array('path' => $path . '%', 'searchterm' => '%'.$searchterm.'%')
        )->setFirstResult(
            null
        )->setMaxResults(
            null
        )->getQuery();
        $count = $query->getSingleScalarResult();
        if ($limit === 0) {
            return array();
        }
        if ($limit === null) {
            $limit = 10;
        }

        $qb->select(
            'ie'
        );
        $qb->setFirstResult($offset);
        $qb->setMaxResults($limit);
        $matches = $qb->getQuery()->getResult();
        foreach ($matches as $match) {
            $content = substr(
                $match->getContent(), 0, $searchLength
            ) .'...';
            $fileInformation['Score'] = 100;
            $fileInformation['Title'] = ucfirst(
                str_replace($fullPath . '/', '', $match->getPath())
            );
            $fileInformation['Content'] = $content;
            $fileInformation['Link'] = $match->getPath();
            $fileInformation['Component'] = $componentName;
            array_push($searchResult, $fileInformation);
        }
        return $searchResult;
    }

    /**
     * Returns an array with all file paths of all files in this directory,
     * including files located in subdirectories.
     *
     * @param $fileList array  all files and directories
     * @param $file    \Cx\Core\MediaSource\Model\Entity\LocalFile file to check
     * @param $result   array  existing result
     *
     * @return array with all files as
     *               \Cx\Core\MediaSource\Model\Entity\LocalFile
     */
    protected function getAllFilesAsObjects($fileList, $file, $result)
    {
        foreach ($fileList as $fileEntryKey => $fileListEntry) {
            $newFile = new \Cx\Core\MediaSource\Model\Entity\LocalFile(
                $file->__toString() . $fileEntryKey,
                $this->getFileSystem()
            );
            if ($this->getFileSystem()->isDirectory($newFile)) {
                $result = $this->getAllFilesAsObjects(
                    $fileListEntry, $newFile, $result
                );
            } else if ($this->getFileSystem()->isFile($newFile)) {
                array_push($result, $newFile);
            }
        }
        return $result;
    }
}
