<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * @copyright   Cloudrexx AG
 * @author      Robin Glauser <robin.glauser@comvation.com>
 * @package     cloudrexx
 * @subpackage  core_mediasource
 */

namespace Cx\Core\MediaSource\Controller;

use Cx\Core\Core\Model\Entity\SystemComponentController;

/**
 * Class ComponentController
 *
 * @copyright   Cloudrexx AG
 * @author      Robin Glauser <robin.glauser@comvation.com>
 * @package     cloudrexx
 * @subpackage  core_mediasource
 */
class ComponentController extends SystemComponentController {

    /**
     * Include all registered indexers
     */
    protected $indexers = array();

    /**
     * All file events. The indexer reacts to the events of a file
     *
     * @var array
     */
    protected $fileEvents = array('Remove', 'Add', 'Update');

    /**
     * @var \Cx\Core\MediaSource\Model\Event\IndexerEventListener Event listener instance
     */
    protected $indexerEventListener;

    /**
     * Register your events here
     *
     * Do not do anything else here than list statements like
     * $this->cx->getEvents()->addEvent($eventName);
     */
    public function registerEvents()
    {
        $eventHandlerInstance = $this->cx->getEvents();
        $eventHandlerInstance->addEvent('mediasource.load');
        foreach ($this->fileEvents as $fileEvent) {
            $eventHandlerInstance->addEvent(
                'MediaSource.File:' . $fileEvent
            );
        }
    }

    /**
     * Register your event listeners here
     *
     * USE CAREFULLY, DO NOT DO ANYTHING COSTLY HERE!
     * CALCULATE YOUR STUFF AS LATE AS POSSIBLE.
     * Keep in mind, that you can also register your events later.
     * Do not do anything else here than initializing your event listeners and
     * list statements like
     * $this->cx->getEvents()->addEventListener($eventName, $listener);
     */
    public function registerEventListeners()
    {
        $eventHandlerInstance = $this->cx->getEvents();
        $this->indexerEventListener = new \Cx\Core\MediaSource\Model\Event\IndexerEventListener($this->cx);

        foreach ($this->fileEvents as $fileEvent) {
            $eventHandlerInstance->addEventListener(
                'MediaSource.File:' . $fileEvent,
                $this->indexerEventListener
            );
        }
        $this->cx->getEvents()->addEventListener(
            'SearchFindContent',
            new \Cx\Core\MediaSource\Model\Event\IndexerSearchResultEventListener(
                $this->cx
            )
        );
    }

    public function getControllerClasses() {
        // Return an empty array here to let the component handler know that there
        // does not exist a backend, nor a frontend controller of this component.
        return array();
    }

    /**
     * @inheritDoc
     */
    public function getCommandsForCommandMode() {
        return array('Indexer', 'IndexDownload');
    }

    /**
     * @inheritDoc
     */
    public function getCommandDescription($command, $short = false) {
        $desc = 'Allows interaction with indexers';
        if ($short) {
            return $desc;
        }
        $desc .= '. Usage:
./cx Indexer index <absoluteFileName>';
        return $desc;
    }

    /**
     * @inheritDoc
     */
    public function executeCommand($command, $arguments, $dataArguments = array()) {
        switch ($command) {
            case 'Indexer':
                switch (current($arguments)) {
                    case 'index':
                        if (!isset($arguments[1])) {
                            echo 'No file path supplied' . PHP_EOL;
                            die();
                        }
                        array_shift($arguments);
                        $this->indexerEventListener->index(array(
                            'path' => current($arguments),
                            'oldPath' => '',
                        ));
                        break;
                }
                break;
            case 'IndexDownload':
                if (
                    \Cx\Core\Setting\Controller\Setting::getValue(
                        'CLX4188_IncludeIndexedFilesInGlobalSearch',
                        'Config'
                    ) != 'on'
                ) {
                    return;
                }
                // @TODO: This is highly insecure and might not work for larger files
                if (!isset($arguments['path'])) {
                    echo 'No path specified';
                    return;
                }
                $path = $arguments['path'];
                $em = $this->cx->getDb()->getEntityManager();
                $indexerRepo = $em->getRepository('Cx\Core\MediaSource\Model\Entity\IndexerEntry');
                $results = $indexerRepo->findBy(array('path' => $path));
                if (count($results) != 1) {
                    // For security reasons we do not advertise a file exists in this case
                    echo 'No path specified';
                    return;
                }
                // avoid injection
                $path = current($results)->getPath();
                $objFile = new \Cx\Lib\FileSystem\File(
                    $this->cx->getWebsiteDocumentRootPath() . $path
                );
                $data = $objFile->getData();
                $filename = basename($objFile->getAbsoluteFilePath());
                $filetype = mime_content_type($objFile->getAbsoluteFilePath());

                $objHTTPDownload = new \HTTP_Download();
                $objHTTPDownload->setFile($objFile->getAbsoluteFilePath());
                $objHTTPDownload->setContentDisposition(
                    HTTP_DOWNLOAD_ATTACHMENT,
                    str_replace('"', '\"', $filename)
                );
                $objHTTPDownload->setContentType($filetype);
                $objHTTPDownload->send();

                header('Content-type: ' . $filetype);
                header('Content-Disposition: attachment; filename="' . $filename . '"');
                echo $data;
                break;
        }
    }

    /**
     * Register a new indexer.
     *
     * @param $indexer \Cx\Core\MediaSource\Model\Entity\Indexer indexer
     *
     * @throws  \Cx\Core\MediaSource\Model\Entity\IndexerException if an index
     *          already exists with this file extension
     * @return void
     */
    public function registerIndexer($indexer)
    {
        global $_ARRAYLANG;

        $extensions = $indexer->getExtensions();
        foreach ($extensions as $extension) {
            if (!empty($this->indexers[$extension])) {
                throw new \Cx\Core\MediaSource\Model\Entity\IndexerException(
                    $_ARRAYLANG['TXT_INDEXER_ALREADY_EXISTS']
                );
            }
            $this->indexers[$extension] = $indexer;
        }
    }

    /**
     * List all indexer
     *
     * @return array
     */
    public function listIndexers()
    {
        return $this->indexers;
    }

    /**
     * Get indexer by file extension
     *
     * @param $extension string file extension of indexer
     *
     * @return \Cx\Core\MediaSource\Model\Entity\Indexer
     */
    public function getIndexer($extension)
    {
        if (!isset($this->indexers[$extension])) {
            return null;
        }
        return $this->indexers[$extension];
    }
}
