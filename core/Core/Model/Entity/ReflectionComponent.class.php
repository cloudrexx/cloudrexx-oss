<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * Represents an abstraction of a component
 *
 * @copyright   Cloudrexx AG
 * @author      Michael Ritter <michael.ritter@comvation.com>
 * @package     cloudrexx
 * @subpackage  core_core
 * @version     3.1.0
 */

namespace Cx\Core\Core\Model\Entity;

/**
 * ReflectionComponentException
 *
 * @copyright   Cloudrexx AG
 * @author      Michael Ritter <michael.ritter@comvation.com>
 * @package     cloudrexx
 * @subpackage  core_core
 * @version     3.1.0
 */
class ReflectionComponentException extends \Exception {}

/**
 * Represents an abstraction of a component
 *
 * @copyright   Cloudrexx AG
 * @author      Michael Ritter <michael.ritter@comvation.com>
 * @package     cloudrexx
 * @subpackage  core_core
 * @version     3.1.0
 */
class ReflectionComponent {
    /**
     * List of all available component types
     * @todo Wouldn't it be better to move this to Component class?
     * @var array List of component types
     */
    protected static $componentTypes = array('core', 'core_module', 'module', 'lib');

    /**
     * @var \Cx\Core\Core\Controller\Cx $cx Current instance of Cx
     */
    protected \Cx\Core\Core\Controller\Cx $cx;

    /**
     * Name of the component this instance is an abstraction of
     * @var string Component name
     */
    protected $componentName = null;

    /**
     * Type of the component this instance is an abstraction of
     * @var string Component type
     */
    protected $componentType = null;

    /**
     * Fully qualified filename for/of the package file
     * @var string ZIP package filename
     */
    protected $packageFile = null;

    /**
     * Database object
     *
     * @var object
     */
    protected $db = null;

    /**
     * Two different ways to instanciate this are supported:
     * 1. Supply an instance of \Cx\Core\Core\Model\Entity\Component
     * 2. Supply a install package zip filename
     * 3. Supply a component name and type
     * @param mixed $arg1 Either an instance of \Cx\Core\Core\Model\Entity\Component or the name of a component
     * @param string|null $arg2 (only if a component name was supplied as $arg1) Component type (one of core_module, module, core, lib)
     * @throws ReflectionComponentException
     * @throws \BadMethodCallException
     */
    public function __construct($arg1, $arg2 = null) {
        $this->cx = \Cx\Core\Core\Controller\Cx::instanciate();
        $this->db = $this->cx->getDb()->getAdoDb();

        if (is_a($arg1, 'Cx\Core\Core\Model\Entity\SystemComponent')) {
            $this->componentName = $arg1->getName();
            $this->componentType = $arg1->getType();
            return;
        }
        $arg1Parts = explode('.', $arg1);
        if (file_exists($arg1) && end($arg1Parts) == 'zip') {
            // clean up tmp dir
            \Cx\Lib\FileSystem\FileSystem::delete_folder(ASCMS_APP_CACHE_FOLDER, true, true);

            // Uncompress package using PCLZip
            $file = new \PclZip($arg1);
            $list = $file->extract(PCLZIP_OPT_PATH, ASCMS_APP_CACHE_FOLDER);

            // Check for meta.yml, if none: throw Exception
            if (!file_exists(ASCMS_APP_CACHE_FOLDER . '/meta.yml')) {
                throw new ReflectionComponentException('This ain\'t no package file: "' . $arg1 . '"');
            }

            // Read meta info
            $metaTypes = array('core'=>'core', 'core_module'=>'system', 'module'=>'application', 'lib'=>'other');
            $yaml = new \Symfony\Component\Yaml\Yaml();
            $content = file_get_contents(ASCMS_APP_CACHE_FOLDER . '/meta.yml');
            $meta = $yaml->parse($content);
            $type = array_key_exists($meta['DlcInfo']['type'], $metaTypes) ? $meta['DlcInfo']['type'] : 'lib';

            // initialize ReflectionComponent
            $this->packageFile = $arg1;
            $this->componentName = $meta['DlcInfo']['name'];
            $this->componentType = $type;
            return;
        } else if (is_string($arg1) && $arg2 && in_array($arg2, self::$componentTypes)) {
            $this->componentName = $arg1;
            $this->componentType = $arg2;

            // look for the valid component name or legacy
            if (!$this->isValidComponentName($this->componentName) && !$this->isValid()) {
                throw new \BadMethodCallException("Provided component name \"{$this->componentName}\" is invalid. Component name must be written in CamelCase notation.");
            }

            return;
        }
        throw new \BadMethodCallException('Pass a component or zip package filename or specify a component name and type');
    }

    /**
     * Check if the provided string is a valid component name
     * @param  string component name
     * @return boolean True if sring $name is a valid component name
     */
    public function isValidComponentName($name) {
        return preg_match('/^([A-Z][a-z0-9]*)+$/', $name);
    }

    /**
     * Returns the components name
     * @return string Component name
     */
    public function getName() {
        return $this->componentName;
    }

    /**
     * Returns the components type
     * @return string Component type
     */
    public function getType() {
        return $this->componentType;
    }

    /**
     * Tells wheter this component is customized or not
     * @return boolean True if customized (and customizings are active)
     */
    protected function isCustomized() {
        $basepath = ASCMS_DOCUMENT_ROOT . SystemComponent::getPathForType($this->componentType);
        $componentPath = $basepath . '/' . $this->componentName;
        $classLoader = $this->cx->getClassLoader();
        return $classLoader->getFilePath($componentPath) != $componentPath;
    }

    /**
     * Returns wheter this component exists or not in the system
     * Note : It not depends the component type
     *
     * @param boolean $allowCustomizing (optional) Set to false if you want to ignore customizings
     * @return boolean True if it exists, false otherwise
     */
    public function exists($allowCustomizing = true) {
        foreach (self::$componentTypes as $componentType) {
            $basepath      = ASCMS_DOCUMENT_ROOT . \Cx\Core\Core\Model\Entity\SystemComponent::getPathForType($componentType);
            $componentPath = $basepath . '/' . $this->componentName;

            if (!$allowCustomizing) {
                if (file_exists($componentPath)) {
                    return true;
                }
            }
            $classLoader = $this->cx->getClassLoader();
            if ($classLoader->getFilePath($componentPath)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns wheter this component installed or not
     *
     * @return boolean True if it exists, false otherwise
     */
    public function isInstalled() {
        $query = '
            SELECT
                `id`
            FROM
                `' . DBPREFIX . 'component`
            WHERE
                `name` = \'' . $this->componentName . '\'
        ';
        $result = $this->cx->getDb()->getAdoDb()->query($query);
        if ($result && $result->RecordCount()) {
            return true;
        }

        $query = '
            SELECT
                `id`
            FROM
                `' . DBPREFIX . 'modules`
            WHERE
                `name` = \'' . $this->componentName . '\'
        ';
        $result = $this->cx->getDb()->getAdoDb()->query($query);

        if ($result && $result->RecordCount()) {
            return true;
        }

        return false;
    }

    /**
     * Returns wheter this component is valid or not. A valid component will work as expected
     * @return boolean True if valid, false otherwise
     */
    public function isValid() {
        // file system
        if (!$this->exists()) {
            return false;
        }

        // DB: entry in components or modules
        // DB: entry in backend areas
        // DB: existing page if necessary

        // what else?

        return true;
    }

    /**
     * Tells wheter this is a legacy component or not
     * @return boolean True if its a legacy one, false otherwise
     */
    public function isLegacy() {
        if (!$this->exists()) {
            return false;
        }
        if (file_exists($this->getDirectory() . '/Controller/')) {
            return false;
        }
        return true;
    }

    /**
     * Returns the absolute path to this component's location in the file system
     * @param boolean $allowCustomizing (optional) Set to false if you want to ignore customizings
     * @param boolean $forceCustomized (optional) If true, the directory in customizing folder is returned, default false
     * @return string Path for this component
     */
    public function getDirectory($allowCustomizing = true, $forceCustomized = false) {
        $docRoot = ASCMS_DOCUMENT_ROOT;
        if ($forceCustomized) {
            $allowCustomizing = false;
            $docRoot = ASCMS_CUSTOMIZING_PATH;
        }
        $basepath = $docRoot.SystemComponent::getPathForType($this->componentType);
        $componentPath = $basepath . '/' . $this->componentName;
        if (!$allowCustomizing) {
            return $componentPath;
        }
        $classLoader = $this->cx->getClassLoader();
        return $classLoader->getFilePath($componentPath);
    }

    /**
     * Return namespace of the component
     *
     * @return string
     */
    public function getNameSpace() {
        return SystemComponent::getBaseNamespaceForType($this->componentType) . '\\' . $this->componentName;
    }

    /**
     * Installs this component from a zip file (if available)
     * @todo This should not do any FS action as these should be done by GIT or
     *          `cx env component`. Instead, this should do initializations like
     *          settings, DB tables, entries in modules and component tables, ...
     * @todo Uninstall should do the opposite of what's described above
     * @todo DB stuff (structure and data)
     * @todo check dependency versions
     * @todo activate templates
     */
    public function install() {
        // Check (not already installed (different version), all dependencies installed)
        if (!$this->packageFile) {
            throw new SystemComponentException('Package file not available');
        }
        if (!file_exists(ASCMS_APP_CACHE_FOLDER . '/meta.yml')) {
            throw new ReflectionComponentException('Invalid package file');
        }
        if ($this->exists()) {
            throw new SystemComponentException('Component is already installed');
        }

        $websitePath = $this->cx->getWebsiteDocumentRootPath();

        // Read meta file
        $yaml = new \Symfony\Component\Yaml\Yaml();
        $content = file_get_contents(ASCMS_APP_CACHE_FOLDER . '/meta.yml');
        $meta = $yaml->parse($content);

        // Check dependencies
        echo "Checking  dependencies ... ";
        foreach ($meta['DlcInfo']['dependencies'] as $dependencyInfo) {
            $dependency = new static($dependencyInfo['name'], $dependencyInfo['type']);
            if (!$dependency->exists()) {
                throw new SystemComponentException('Dependency "' . $dependency->getName() . '" not met');
            }
        }
        echo "Done \n";

        // Copy ZIP contents
        echo "Copying files to installation ... ";
        $componentPath = \Cx\Core\Core\Model\Entity\SystemComponent::getPathForType(
            $this->componentType
        ) . '/' . $this->componentName;
        $srcPath = ASCMS_APP_CACHE_FOLDER . '/DLC_FILES' . $componentPath;
        $dstPath = $websitePath . $componentPath;

        $filesystem = new \Cx\Lib\FileSystem\FileSystem();
        $filesystem->copy_folder(
            $srcPath,
            $dstPath,
            true,
            true
        );
        echo "Done \n";

        // Activate (if type is system or application)
        // TODO: templates need to be activated too!
        if ($this->componentType != 'core' && $this->componentType != 'core_module' && $this->componentType != 'module') {
            return;
        }

        // Copy ZIP contents (also copy meta.yml into component folder if type is system or application)
        try {
            $objFile = new \Cx\Lib\FileSystem\PrivilegedFile(ASCMS_APP_CACHE_FOLDER . '/meta.yml');
            $objFile->copy($this->getDirectory(false) . '/meta.yml');
        } catch (\Cx\Lib\FileSystem\FileSystemException $e) {
            \DBG::msg($e->getMessage());
        }

        // clean-up
        \Cx\Lib\FileSystem\FileSystem::delete_folder(ASCMS_APP_CACHE_FOLDER, true, true);

        // Activate this component
        echo "Activating component ... ";
        $this->activate();
        echo "Done \n";
    }

    /**
     * Import table's from the yml files
     */
    protected function createTablesFromYaml()
    {
        $em  = $this->cx->getDb()->getEntityManager();
        $ymlDirectory = $this->getDirectory(false).'/Model/Yaml';
        $this->cx->getDb()->addSchemaFileDirectories([$ymlDirectory]);
        $classes = array();
        foreach (glob($ymlDirectory.'/*.yml') as $yml) {
            $ymlArray  = \Symfony\Component\Yaml\Yaml::parse($yml);
            $classes[] = $em->getClassMetadata(key($ymlArray));
        }
        if (!$classes) {
            return;
        }
        $scm = new \Doctrine\ORM\Tools\SchemaTool($em);
        $scm->createSchema($classes);
    }

    /**
     * Imports table structure to database from sql dump file
     */
    protected function importStructureFromSql()
    {
        $sqlDump = $this->cx->getCodeBaseDocumentRootPath() . SystemComponent::getPathForType($this->componentType) . '/' . $this->componentName . '/Data/Structure.sql';
        if (!file_exists($sqlDump)) {
            return;
        }
        $fp = @fopen ($sqlDump, "r");
        if ($fp !== false) {
            $sqlQuery = '';
            while (!feof($fp)) {
                $buffer = fgets($fp);
                if ((substr($buffer,0,1) != "#") && (substr($buffer,0,2) != "--")) {
                    $sqlQuery .= $buffer;
                    if (preg_match("/;[ \t\r\n]*$/", $buffer)) {
                        $sqlQuery = preg_replace('#contrexx_#', DBPREFIX, $sqlQuery, 1);
                        $result = $this->db->Execute($sqlQuery);
                        if ($result === false) {
                            throw new SystemComponentException($sqlQuery .' ('. $this->db->ErrorMsg() .')');
                        }
                        $sqlQuery = '';
                    }
                }
            }
        } else {
            throw new SystemComponentException('File not found : '. $sqlDump);
        }
    }

    /**
     * import component data's to database from sql dump file
     */
    protected function importDataFromSql(int $moduleId)
    {
        if (!$moduleId) {
            throw new SystemComponentException('Module ID must be set before Data.sql can be imported');
        }
        $sqlDump = $this->cx->getCodeBaseDocumentRootPath() . SystemComponent::getPathForType($this->componentType) . '/' . $this->componentName . '/Data/Data.sql';
        if (!file_exists($sqlDump)) {
            return;
        }

        $pattern = '/\s+INTO\s+`?([a-z\d_]+)`?/i';

        $fp = @fopen ($sqlDump, "r");
        if ($fp !== false) {
            $sqlQuery = '';
            $backendAreasCleaned = false;
            $moduleRepositoryCleaned = false;
            $backendAreaMap = [];
            $moduleRepositoryMap = [];
            while (!feof($fp)) {
                $buffer = fgets($fp);
                if ((substr($buffer,0,1) != "#") && (substr($buffer,0,2) != "--")) {
                    $sqlQuery .= $buffer;
                    if (preg_match("/;[ \t\r\n]*$/", $buffer)) {
                        $sqlQuery = preg_replace('#contrexx_#', DBPREFIX, $sqlQuery, 1);
                        $matches = null;
                        preg_match($pattern, $sqlQuery , $matches , 0);
                        $table = isset($matches[1]) ? $matches[1] : '';

                        switch ($table) {
                            case DBPREFIX.'modules':
                                $this->db->Execute('DELETE FROM `' . DBPREFIX . 'modules` WHERE `id` = ' . $moduleId);
                                $data = $this->getColumnsAndDataFromSql($sqlQuery);
                                $replacements = array(
                                    'id' => $moduleId,
                                    'is_required' => (int) ($this->componentType === 'core'),
                                    'is_core' => (int) ($this->componentType === 'core'
                                        || $this->componentType === 'core_module'),
                                );
                                $sqlQuery = $this->repalceDataInQuery($table, $data, $replacements);
                                break;
                            case DBPREFIX.'component':
                                $this->db->Execute('DELETE FROM `' . DBPREFIX . 'component` WHERE `id` = ' . $moduleId);
                                $data = $this->getColumnsAndDataFromSql($sqlQuery);
                                $replacements = array(
                                    'id' => $moduleId,
                                    'type' => $this->componentType,
                                );
                                $sqlQuery = $this->repalceDataInQuery($table, $data, $replacements);
                                break;
                            case DBPREFIX.'backend_areas':
                                if (!$backendAreasCleaned) {
                                    $this->db->Execute('DELETE FROM `' . DBPREFIX . 'backend_areas` WHERE `module_id` = ' . $moduleId);
                                    $backendAreasCleaned = true;
                                }
                                $data = $this->getColumnsAndDataFromSql($sqlQuery);
                                $areaId = $this->getNextCustomBackendAreaId();
                                $backendAreaMap[$data['area_id']] = $areaId;
                                $replacements = array(
                                    'area_id' => $areaId,
                                    'module_id' => $moduleId,
                                    'parent_area_id' => $backendAreaMap[$data['parent_area_id']] ?? $data['parent_area_id'],
                                );
                                $sqlQuery = $this->repalceDataInQuery($table, $data, $replacements);
                                break;
                            case DBPREFIX . 'module_repository':
                                if (!$moduleRepositoryCleaned) {
                                    $this->db->Execute('DELETE FROM `' . DBPREFIX . 'module_repository` WHERE `moduleid` = ' . $moduleId);
                                    $moduleRepositoryCleaned = true;
                                }
                                $data = $this->getColumnsAndDataFromSql($sqlQuery);
                                $replacements = array(
                                    'id' => NULL,
                                    'moduleid' => $moduleId,
                                    'parid' => $moduleRepositoryMap[$data['parid']] ?? 0,
                                );
                                $sqlQuery = $this->repalceDataInQuery($table, $data, $replacements);
                                $result = $this->db->Execute($sqlQuery);
                                if ($result === false) {
                                    throw new SystemComponentException($sqlQuery .' ('. $this->db->ErrorMsg() .')');
                                }
                                $moduleRepositoryMap[$data['id']] = $this->db->insert_id();
                                $sqlQuery = '';
                                continue 2;
                                break;
                            default:
                                break;
                        }

                        $result = $this->db->Execute($sqlQuery);
                        if ($result === false) {
                            throw new SystemComponentException($sqlQuery .' ('. $this->db->ErrorMsg() .')');
                        }
                        $sqlQuery = '';
                    }
                }
            }
        }
    }

    /**
     * replace data in the existing data by the given replacements
     * and return the sql query
     *
     * @param string $table        Table name
     * @param array  $columns      Columns array
     * @param array  $data         Data array
     * @param array  $replacements Replacement data array
     */
    protected function repalceDataInQuery($table, $data, $replacements)
    {
        $data = array_intersect_key($replacements + $data, $data);

        $sql  = 'INSERT INTO `'.$table.'` ';
        $sql .= "SET \n";

        $firstCol = true;
        foreach($data as $column => $data) {
            $value = is_null($data) ? "NULL" : (is_string($data) ? "'$data'" : $data);

            $sql .= '    '.($firstCol ? '' : ',') ."`$column` = $value\n";
            $firstCol = false;
        }

        return $sql;
    }

    /**
     * parse the mysql query and return the columns and data from the given query.
     *
     * @param string $sqlQuery Mysql query
     *
     * @return array
     */
    public function getColumnsAndDataFromSql($sqlQuery)
    {
        $columnAndData = null;
        preg_match_all('/\((.+?)\)/', $sqlQuery, $columnAndData);
        $columnsString = $columnAndData[1][0];
        $dataString    = $columnAndData[1][1];

        $columns = null;
        preg_match_all('/`([^`]+)`/', $columnsString, $columns);
        $data = null;
        preg_match_all('/(?:^|,\s*)([\'"])?(\d+|(?:[^\'"]|(?<=\\\\)\1)+|)/', $dataString, $data);

        return array_combine($columns[1], $data[2]);
    }

    /**
     * Get the component related tables
     *
     * @return array  component related tables
     */
    protected function getComponentTables() {
        global $_DBCONFIG;

        // load tables
        $tblSyntax = DBPREFIX . $this->componentType . '_' . strtolower($this->componentName);
        $objResult = $this->db->query('SHOW TABLES LIKE "'. $tblSyntax .'_%"');

        $componentTables = array();
        while (!$objResult->EOF) {
            $componentTables[] = $objResult->fields['Tables_in_'. $_DBCONFIG['database'] .' ('. $tblSyntax .'_%)'];
            $objResult->MoveNext();
        }

        return $componentTables;
    }

    /**
     * Creates the settings specified in the Component.yml
     *
     * In order for this to work the version needs to be >= 2.1.
     * @throws \Exception On error
     */
    protected function createSettings(): void {
        if (!file_exists($this->getDirectory() . '/Component.yml')) {
            return;
        }
        $component = $this->cx->getComponent($this->getName());
        if (!$component) {
            return;
        }
        $version = $component->getConfigValue('version');
        if ($version < 2.1) {
            return;
        }
        try {
            $data = $component->getConfigValues('componentOptions/Setting');
        } catch (\Exception $e) {
            return;
        }
        if (!count($data)) {
            return;
        }
        $requireEarly = false;
        foreach ($data as $group=>$settings) {
            foreach ($settings as $setting=>$settingConfig) {
                if (
                    empty($settingConfig['name']) ||
                    !isset($settingConfig['value']) ||
                    !isset($settingConfig['ord']) ||
                    !isset($settingConfig['type']) ||
                    !isset($settingConfig['values'])
                ) {
                    throw new \Exception('Mandatory settings key missing!');
                }
                if (
                    !$requireEarly &&
                    isset($settingConfig['flags']) &&
                    in_array('preInit', $settingConfig['flags'])
                ) {
                    $requireEarly = true;
                }
            }
        }
        // As setting currently does not allow mixing the engine within the same
        // section we need to switch the engine for all settings of this component.
        \Cx\Core\Setting\Controller\Setting::init(
            $this->getName(),
            null,
            $requireEarly ? 'FileSystem' : 'Database'
        );
        foreach ($data as $group=>$settings) {
            foreach ($settings as $setting=>$settingConfig) {
                if (
                    isset($settingConfig['flags']) &&
                    in_array('user', $settingConfig['flags'])
                ) {
                    if (!$this->cx->getComponent($this->getName())) {
                        throw new \Exception('Could not add user setting as component is not present');
                    }
                    if (
                        count(
                            $this->cx->getComponent('Setting')->getUserBasedSettings(
                                $this->cx->getComponent($this->getName()),
                                '',
                                $settingConfig['name']
                            )
                        )
                    ) {
                        continue;
                    }
                    $this->cx->getComponent('Setting')->createUserSetting(
                        $this->cx->getComponent($this->getName()),
                        $group,
                        $settingConfig['name'],
                        $settingConfig['type'],
                        $settingConfig['value'],
                        $settingConfig['values'],
                        $settingConfig['ord']
                    );
                    continue;
                }
                if (\Cx\Core\Setting\Controller\Setting::isDefined($settingConfig['name'])) {
                    continue;
                }
                if (
                    !\Cx\Core\Setting\Controller\Setting::add(
                        $settingConfig['name'],
                        $settingConfig['value'],
                        $settingConfig['ord'],
                        $settingConfig['type'],
                        $settingConfig['values'],
                        $group
                    )
                ) {
                    throw new \Exception('Adding setting failed!');
                }
            }
        }
    }

    /**
     * Removes all settings of this component
     * @throws \Exception On error
     */
    protected function removeSettings(): void {
        if ($this->cx->getComponent($this->getName())) {
            // Note: User setting depends on the component being present
            $this->cx->getComponent('Setting')->removeComponentUserSettings(
                $this->cx->getComponent($this->getName())
            );
        }
        foreach (array('Database', 'FileSystem', 'Yaml') as $engine) {
            \Cx\Core\Setting\Controller\Setting::init(
                $this->getName(),
                null,
                $engine
            );
            if (
                !\Cx\Core\Setting\Controller\Setting::getSectionEngine()->deleteModule()
            ) {
                throw new \Exception('Could not remove settings from engine ' . $engine);
            }
        }
    }

    /**
     * Removes this component
     *
     * This might not work perfectly for legacy components, since there could
     * be files outside the component's directory!
     * Be sure there is no other component relying on this one!
     * @todo    Apparently unused; drop?
     */
    public function remove() {
        // remove from db
        $this->removeFromDb();

        // if there are no files, quit
        if (!$this->exists()) {
            return;
        }

        // remove from fs
        \Cx\Lib\FileSystem\FileSystem::delete_folder($this->getDirectory(), true, true);
    }

    /**
     * Activate this component
     *
     * @TODO: This should only do install() (see todo there) and the same as ModuleManager does
     * Adds all missing database records, updates where necessary.
     * Recreates all proxies on success.
     * Fails if the component folder doesn't exist.
     * When adding custom components, new module and access IDs are created.
     * Note that most of the methods called within contain "custom" in their
     * names.  These also apply to standard components, but only do standard
     * stuff, and return standard values for them.
     * Standard component IDs are defined by the installer, and should be
     * preserved whenever possible.
     * @return  void
     * @throws  \Cx\Core\Core\Controller\ComponentException     on failure
     * @todo    Backend navigation entry from meta.yml (rxqcmv1)
     * @todo    Pages from meta.yml (rxqcmv1)
     */
    public function activate()
    {
        if (!$this->exists()) {
            throw new \Cx\Core\Core\Controller\ComponentException(
                'No folder found'
                . ' for component "' . $this->componentType
                . '/' . $this->componentName . '"'
            );
        }
        echo "Importing component data (structure & data) ... ";
        if (!file_exists($this->getDirectory(false)."/Model/Yaml")) {
            $this->importStructureFromSql();
        } else {
            $this->createTablesFromYaml();
        }
        $moduleId = $this->addCustomModule();
        $this->addCustomComponent($moduleId);
        $this->addCustomBackendArea($moduleId);
        $this->importDataFromSql($moduleId);
        $this->createSettings();
        echo "Done \n";
        if ($this->componentType !== 'module') {
            return;
        }
        $this->addCustomPages($moduleId);
        $workbenchComponent = new self('Workbench', 'core_module');
        if (!$workbenchComponent->exists()) {
            return; // The code below MUST NOT run on production
        }
        $em = $this->cx->getDb()->getEntityManager();
        $proxyFolderPath = $em->getConfiguration()->getProxyDir();
        $metadata = $em->getMetadataFactory()->getAllMetadata();
        $em->getProxyFactory()->generateProxyClasses(
            $metadata, $proxyFolderPath
        );
    }

    /**
     * Add or update a module table record, and return the module ID
     *
     * Existing entries will only have their status and flags reset to defaults.
     * @return  int
     * @throws  \Cx\Core\Core\Controller\ComponentException     on failure
     */
    protected function addCustomModule()
    {
        $databaseConnection = $this->cx->getDb()->getAdoDb();
        $moduleId = $this->getNextCustomModuleId();
        $distributor = 'Cloudrexx AG';
        $workbenchComponent = new self('Workbench', 'core_module');
        if ($workbenchComponent->exists()) {
            $workbench = new \Cx\Core_Modules\Workbench\Controller\Workbench();
            $distributor = $workbench->getConfigEntry('distributor');
        }
        $description = 'TXT_' . strtoupper($this->componentType)
            . '_' . strtoupper($this->componentName) . '_DESCRIPTION';
        $isRequired = (int) ($this->componentType === 'core');
        $isCore = (int) ($this->componentType === 'core'
            || $this->componentType === 'core_module');
        $result = $databaseConnection->query('
            INSERT INTO `' . DBPREFIX . 'modules` (
                `id`, `name`, `distributor`, `description_variable`,
                `status`, `is_required`, `is_core`,
                `is_active`, `is_licensed`
            ) VALUES (
                ?, ?, ?, ?,
                ?, ?, ?,
                ?, ?
            )
            ON DUPLICATE KEY UPDATE
                `status`=?, `is_required`=?, `is_core`=?,
                `is_active`=?, `is_licensed`=?',
            [
                $moduleId, $this->componentName, $distributor, $description,
                'y', $isRequired, $isCore,
                1, 1,
                'y', $isRequired, $isCore,
                1, 1
            ]
        );
        if (!$result) {
            throw new \Cx\Core\Core\Controller\ComponentException(
                'Failed to add or update module table entry'
                . ' for component "' . $this->componentType
                . '/' . $this->componentName . '"'
            );
        }
        return $moduleId;
    }

    /**
     * Return the next available ID for a custom module
     *
     * For existing modules, returns their current ID.
     * This may be taken from the modules table, or, if not present there,
     * from the backend areas table.
     * If neither can be found, returns the next available ID greater than
     * or equal to 900.
     * @return  int
     * @throws  \Cx\Core\Core\Controller\ComponentException     on failure
     */
    protected function getNextCustomModuleId()
    {
        $databaseConnection = $this->cx->getDb()->getAdoDb();
        $result = $databaseConnection->query('
            SELECT `id`
            FROM `' . DBPREFIX . 'modules`
            WHERE `name`=?',
            [$this->componentName]
        );
        if (!$result) {
            throw new \Cx\Core\Core\Controller\ComponentException(
                'Failed to query current module ID'
                . ' for component "' . $this->componentType
                . '/' . $this->componentName . '"'
            );
        }
        if (!$result->EOF) {
            return $result->fields['id'];
        }
        $moduleId = $this->getModuleIdFromBackendArea();
        if ($moduleId) {
            return $moduleId;
        }
        $moduleIdPrevious = 899;
        $result = $databaseConnection->query('
            SELECT MAX(`id`) `id`
            FROM `' . DBPREFIX . 'modules`
            WHERE `id`>?',
            [$moduleIdPrevious]
        );
        if (!$result) {
            throw new \Cx\Core\Core\Controller\ComponentException(
                'Failed to query available module ID'
                . ' for component "' . $this->componentType
                . '/' . $this->componentName . '"'
            );
        }
        if (!$result->EOF && $result->fields['id']) {
            $moduleIdPrevious = $result->fields['id'];
        }
        return $moduleIdPrevious + 1;
    }

    /**
     * Return the module ID for this component from the backend areas table
     *
     * Returns null if there's no such entry.
     * Mind that {@see deactivate()} won't remove that record, thus allowing
     * to (re-)activate components using the same module ID.
     * Otherwise, standard components would be installed using a custom ID
     * (900 and up).
     * @return  int|null
     * @throws  \Cx\Core\Core\Controller\ComponentException     on failure
     */
    protected function getModuleIdFromBackendArea()
    {
        $databaseConnection = $this->cx->getDb()->getAdoDb();
        $result = $databaseConnection->query('
            SELECT `module_id`
            FROM `' . DBPREFIX . 'backend_areas`
            WHERE `uri` LIKE CONCAT("%cmd=", ?, "&%")
            OR `uri` LIKE CONCAT("%cmd=", ?)',
            [
                $this->componentName,
                $this->componentName
            ]
        );
        if (!$result) {
            throw new \Cx\Core\Core\Controller\ComponentException(
                'Failed to query current module ID from backend area'
                . ' for component "' . $this->componentType
                . '/' . $this->componentName . '"'
            );
        }
        if ($result->EOF) {
            return null;
        }
        return $result->fields['module_id'];
    }

    /**
     * Add a custom component table entry
     *
     * This is a noop for legacy components.
     * Doesn't touch existing records.
     * Note:  As the component ID column is AUTO_INCREMENT, and there's no
     * setId() method available on SystemComponent, the ID is forcibly
     * updated to the $moduleId after creation.
     * That SHOULD not be an issue, since there are at most as many records
     * for components as in the module table (legacy).
     * However, any orphan records will inevitably lead to a conflict
     * at some point.
     * @param   int         $moduleId
     * @return  void
     * @throws  \Cx\Core\Core\Controller\ComponentException     on failure
     */
    protected function addCustomComponent($moduleId)
    {
        if ($this->isLegacy()) {
            return;
        }
        $em = $this->cx->getDb()->getEntityManager();
        $componentRepo = $em->getRepository(
            'Cx\\Core\\Core\\Model\\Entity\\SystemComponent'
        );
        if ($componentRepo->findOneBy(array(
            'name' => $this->componentName,
            'type' => $this->componentType,
        ))) {
            return;
        }
        $component = new SystemComponent();
        $component->setName($this->componentName);
        $component->setType($this->componentType);
        $em->persist($component);
        $em->flush();
        try {
            $em->createQueryBuilder()
                ->update('Cx\\Core\\Core\\Model\\Entity\\SystemComponent', 'c')
                ->set('c.id', $moduleId)
                ->where('c.type=:type')
                ->andWhere('c.name=:name')
                ->getQuery()->execute([
                    'type' => $this->componentType,
                    'name' => $this->componentName
                ]);
        } catch (\Exception $e) {
            throw new \Cx\Core\Core\Controller\ComponentException(
                'Failed to update ID'
                . ' for component "' . $this->componentType
                . '/' . $this->componentName . '"'
            );
        }
    }

    /**
     * Add a backend area table record for any component
     *
     * Searches for the component by its "uri" column.
     * For existing records, updates the module ID only.
     * Adds default values for new ones.
     * @param   int     $moduleId
     * @throws  \Cx\Core\Core\Controller\ComponentException     on failure
     */
    protected function addCustomBackendArea($moduleId)
    {
        $databaseConnection = $this->cx->getDb()->getAdoDb();
        $result = $databaseConnection->query('
            SELECT `area_id`
            FROM `' . DBPREFIX . 'backend_areas`
            WHERE `uri` LIKE CONCAT("%cmd=", ?, "&%")
            OR `uri` LIKE CONCAT("%cmd=", ?)',
            [
                $this->componentName,
                $this->componentName
            ]
        );
        if (!$result) {
            throw new \Cx\Core\Core\Controller\ComponentException(
                'Failed to query current backend area'
                . ' for component "' . $this->componentType
                . '/' . $this->componentName . '"'
            );
        }
        if (!$result->EOF) {
            $result = $databaseConnection->query('
                UPDATE `'.DBPREFIX.'backend_areas`
                SET `module_id`=?
                WHERE `area_id`=?',
                [
                    $moduleId,
                    $result->fields['area_id']
                ]
            );
            return;
        }
        $parentAreaId = 0;
        if ($this->componentType === 'module') {
            $parentAreaId = 2;
        }
        $ordinal = $this->getNextCustomOrdinal($parentAreaId);
        $accessId = $this->getNextCustomAccessId();
        $areaName = 'TXT_' . strtoupper($this->componentType)
            . '_' . strtoupper($this->componentName);
        $uri = 'index.php?cmd=' . $this->componentName;
        $databaseConnection->query('
            INSERT INTO `'.DBPREFIX.'backend_areas` (
                `parent_area_id`, `type`, `scope`, `area_name`,
                `is_active`, `uri`, `target`,
                `module_id`, `order_id`, `access_id`
            ) VALUES (
                ?, ?, ?, ?,
                ?, ?, ?,
                ?, ?, ?
            )',
            [
                $parentAreaId, 'navigation', 'backend', $areaName,
                (int) ($parentAreaId === 2), $uri, '_self',
                $moduleId, $ordinal, $accessId
            ]
        );
    }

    /**
     * Return the next available Access ID for a custom component
     *
     * Returns an integer greater than or equal to 900.
     * @return  int
     * @throws  \Cx\Core\Core\Controller\ComponentException     on failure
     */
    protected function getNextCustomAccessId()
    {
        $databaseConnection = $this->cx->getDb()->getAdoDb();
        $accessIdPrevious = 899;
        $result = $databaseConnection->query('
            SELECT MAX(`access_id`) `access_id`
            FROM `' . DBPREFIX . 'backend_areas`
            WHERE `access_id`>?',
            [$accessIdPrevious]
        );
        if (!$result) {
            throw new \Cx\Core\Core\Controller\ComponentException(
                'Failed to query backend area access ID'
                . ' for component "' . $this->componentType
                . '/' . $this->componentName . '"'
            );
        }
        if (!$result->EOF && $result->fields['access_id']) {
            $accessIdPrevious = $result->fields['access_id'];
        }
        return $accessIdPrevious + 1;
    }

    /**
     * Return the next ordinal for the given parent backend area ID
     *
     * Increases the previously highest ordinal by one, or returns 1 if none.
     * @param   int     $parentAreaId
     * @return  int
     * @throws  \Cx\Core\Core\Controller\ComponentException     on failure
     */
    protected function getNextCustomOrdinal($parentAreaId)
    {
        $databaseConnection = $this->cx->getDb()->getAdoDb();
        $ordinalPrevious = 0;
        $result = $databaseConnection->query('
            SELECT MAX(`order_id`) `order_id`
            FROM `' . DBPREFIX . 'backend_areas`
            WHERE `parent_area_id`=?',
            [$parentAreaId]
        );
        if (!$result) {
            throw new \Cx\Core\Core\Controller\ComponentException(
                'Failed to query backend area ordinal'
                . ' for component "' . $this->componentType
                . '/' . $this->componentName . '"'
            );
        }
        if (!$result->EOF && $result->fields['order_id']) {
            $ordinalPrevious = $result->fields['order_id'];
        }
        return $ordinalPrevious + 1;
    }

    /**
     * Return the next available backend area ID for a custom component
     *
     * Returns an integer greater than or equal to 900.
     * @return  int
     * @throws  \Cx\Core\Core\Controller\ComponentException     on failure
     */
    protected function getNextCustomBackendAreaId() {
        $db = $this->cx->getDb()->getAdoDb();
        $areaIdPrevious = 899;
        $result = $db->query('
            SELECT MAX(`area_id`) `area_id`
            FROM `' . DBPREFIX . 'backend_areas`
            WHERE `area_id`>?',
            [$areaIdPrevious]
        );
        if (!$result) {
            throw new \Cx\Core\Core\Controller\ComponentException(
                'Failed to query backend area ID'
                . ' for component "' . $this->componentType
                . '/' . $this->componentName . '"'
            );
        }
        if (!$result->EOF && $result->fields['area_id']) {
            $areaIdPrevious = $result->fields['area_id'];
        }
        return $areaIdPrevious + 1;
    }

    /**
     * Add default Pages for a component
     *
     * Only applies to modules; for other types, this is a noop.
     * Does nothing if any page exists for the module already.
     * If no content is found in the module repository, creates a
     * default application page.
     * @see     loadPagesFromModuleRepository()
     * @param   int     $moduleId
     * @return  void
     */
    protected function addCustomPages($moduleId)
    {
        if ($this->componentType !== 'module') {
            return;
        }
        $em = $this->cx->getDb()->getEntityManager();
        $pageRepo = $em->getRepository(
            '\Cx\Core\ContentManager\Model\Entity\Page'
        );
        if ($pageRepo->findBy(array(
            'module' => $this->componentName,
            'type' => \Cx\Core\ContentManager\Model\Entity\Page::TYPE_APPLICATION,
        ))) {
            return;
        }
        if ($this->loadPagesFromModuleRepository($moduleId)) {
            return;
        }
        $nodeRepo = $em->getRepository(
            '\\Cx\\Core\\ContentManager\\Model\\Entity\\Node'
        );
        $newnode = new \Cx\Core\ContentManager\Model\Entity\Node();
        $newnode->setParent($nodeRepo->getRoot());
        $em->persist($newnode);
        $em->flush();
        $nodeRepo->moveDown($newnode, true);
        foreach (\FWLanguage::getActiveFrontendLanguages() as $lang) {
            $type = \Cx\Core\ContentManager\Model\Entity\Page::TYPE_FALLBACK;
            if ($lang['is_default'] === 'true' || !$lang['fallback']) {
                $type = \Cx\Core\ContentManager\Model\Entity\Page::TYPE_APPLICATION;
            }
            $page = $pageRepo->createPage(
                $newnode,
                $lang['id'],
                $this->componentName,
                $type,
                $this->componentName,
                '',
                false,
                '{APPLICATION_DATA}'
            );
            $em->persist($page);
        }
        $em->flush();
    }

    /**
     * Creates application Pages from the module repository
     *
     * Returns false if there are no pages in the repository for that module,
     * true otherwise.
     * @param   int     $moduleId
     * @return  bool
     * @throws  \Cx\Core\Core\Controller\ComponentException     on failure
     */
    protected function loadPagesFromModuleRepository($moduleId)
    {
        $em = $this->cx->getDb()->getEntityManager();
        $nodeRepo = $em->getRepository('\\Cx\\Core\\ContentManager\\Model\\Entity\\Node');
        $pageRepo = $em->getRepository('\\Cx\\Core\\ContentManager\\Model\\Entity\\Page');
        $objResult = $this->cx->getDb()->getAdoDb()->query('
            SELECT `id`, `moduleid`, `content`, `title`,
                `cmd`, `expertmode`, `parid`, `displaystatus`,
                `username`, `displayorder`
            FROM `' . DBPREFIX . 'module_repository`
            WHERE `moduleid`=' . $moduleId . '
            ORDER BY `parid` ASC'
        );
        if (!$objResult) {
            throw new \Cx\Core\Core\Controller\ComponentException(
                'Failed to query module repository'
                . ' for component "' . $this->componentType
                . '/' . $this->componentName . '"'
            );
        }
        if ($objResult->EOF) {
            return false;
        }
        $paridarray = array();
        while (!$objResult->EOF) {
            $root = false;
            if (isset($paridarray[$objResult->fields['parid']])) {
                $parcat = $paridarray[$objResult->fields['parid']];
            } else {
                $root = true;
                $parcat = $nodeRepo->getRoot();
            }
            $newnode = new \Cx\Core\ContentManager\Model\Entity\Node();
            $newnode->setParent($parcat);
            $em->persist($newnode);
            $em->flush();
            $nodeRepo->moveDown($newnode, true);
            $paridarray[$objResult->fields['id']] = $newnode;
            foreach (\FWLanguage::getActiveFrontendLanguages() as $lang) {
                if ($lang['is_default'] === 'true' || !$lang['fallback']) {
                    $page = $pageRepo->createPage(
                        $newnode,
                        $lang['id'],
                        $objResult->fields['title'],
                        \Cx\Core\ContentManager\Model\Entity\Page::TYPE_APPLICATION,
                        $this->componentName,
                        $objResult->fields['cmd'],
                        !$root && $objResult->fields['displaystatus'],
                        $objResult->fields['content']
                    );
                } else {
                    $page = $pageRepo->createPage(
                        $newnode,
                        $lang['id'],
                        $objResult->fields['title'],
                        \Cx\Core\ContentManager\Model\Entity\Page::TYPE_FALLBACK,
                        $this->componentName,
                        $objResult->fields['cmd'],
                        !$root && $objResult->fields['displaystatus'],
                        ''
                    );
                }
                $em->persist($page);
            }
            $em->flush();
            $objResult->MoveNext();
        }
        return true;
    }

    /**
     * Deactivate this component
     *
     * @TODO: This should only do the same as ModuleManager does
     * Removes Pages, as well as the component and module table entries.
     * Doesn't touch the backend area with the access IDs.
     */
    public function deactivate()
    {
        $this->removeSettings();
        $adoDb = $this->cx->getDb()->getAdoDb();
        $adoDb->execute('
            DELETE FROM `' . DBPREFIX . 'core_text`
            WHERE `section`=?',
            [$this->componentName]
        );
        $adoDb->execute('
            DELETE FROM `' . DBPREFIX . 'core_mail_template`
            WHERE `section`=?',
            [$this->componentName]
        );
        $adoDb->execute('
            DELETE FROM `' . DBPREFIX . 'translations`
            WHERE `object_class` LIKE ?',
            [addslashes($this->getNameSpace() . '\\%')]
        );
        $adoDb->execute('
            DELETE FROM `' . DBPREFIX . 'backend_areas`
            WHERE `module_id`= (SELECT `id` FROM `' . DBPREFIX . 'modules` WHERE `name` = ?)',
            [$this->componentName]
        );
        $adoDb->execute('
            DELETE FROM `' . DBPREFIX . 'modules`
            WHERE `name`=?',
            [$this->componentName]
        );
        $em = $this->cx->getDb()->getEntityManager();
        $pageRepo = $em->getRepository(
            'Cx\\Core\\ContentManager\\Model\\Entity\\Page'
        );
        $pages = $pageRepo->findBy(array(
            'module' => $this->componentName,
        ));
        foreach ($pages as $page) {
            $em->remove($page);
        }
        $componentRepo = $em->getRepository(
            'Cx\\Core\\Core\\Model\\Entity\\SystemComponent'
        );
        $component = $componentRepo->findOneBy(
            array(
                'name' => $this->getName(),
                'type' => $this->getType(),
            )
        );
        if ($component) {
            $em->remove($component->getSystemComponent());
        }
        $em->flush();
    }

    /**
     * This completely removes this component from DB
     * @todo Test removing components tables (including doctrine schema)
     * @todo This contains duplicate code with $this->deactivate()
     * @internal    Currently unused in this class; see {@see remove()}
     *              The only current usage is in Workbench's
     *              ReflectionComponent::internalRelocate()
     */
    protected function removeFromDb() {
        // pages, settings, etc.
        $this->deactivate();

        // module tables (LIKE DBPREFIX . strtolower($moduleName)%)
        $query = '
            SHOW TABLES
            LIKE
                \'' . DBPREFIX . 'module_' . strtolower($this->componentName) . '%\'
        ';
        $result = $adoDb->execute($query);
        while (!$result->EOF) {
            $query = '
                DROP TABLE
                    `' . current($result->fields) . '`
            ';
            $adoDb->execute($query);

            $result->MoveNext();
        }
    }
}
