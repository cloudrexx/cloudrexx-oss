<?php declare(strict_types=1);

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2023
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * Admin CP navigation
 *
 * @copyright   CLOUDREXX CMS - CLOUDREXX AG
 * @author      Cloudrexx Development Team <info@cloudrexx.com>
 * @version     2.0.0
 * @package     cloudrexx
 * @subpackage  core
 */
namespace Cx\Core\Core\Model\Entity;

/**
 * Class for the Admin CP navigation
 *
 * @copyright   CLOUDREXX CMS - CLOUDREXX AG
 * @author      Cloudrexx Development Team <info@cloudrexx.com>
 * @access      public
 * @version     2.0.0
 * @package     cloudrexx
 * @subpackage  core
 * @deprecated  This will be replaced by using the same mechanism as in frontend
 */
class AdminNavigation extends \Cx\Model\Base\EntityBase {
    protected \Cx\Core\Model\CustomAdodbPdo $database;

    /**
     * @var array List of translated group names indexed by ID
     */
    protected array $menuGroups = array();

    /**
     * @var array Flat list of all (sub / non-group) menu items indexed by ID
     *
     * Items are an array with the following structure:
     *  0 => <parentId>
     *  1 => <translatedName>
     *  2 => <targetUrl>
     *  3 => <targetWindow>
     *  4 => <moduleName>
     *  5 => <orderId>
     *  6 => <accessId>
     *  7 => <alternateCmds> (optional)
     */
    protected array $menuItems = array();

    /**
     * Creates a new instance of admin navigation
     * @param \Cx\Core\Model\CustomAdodbPdo $database Database connection to use
     */
    public function __construct(\Cx\Core\Model\CustomAdodbPdo $database) {
        $this->database = $database;
        $this->init();
    }

    /**
     * Merges hardcoded legacy entries with modern ones into $this->menuItems and $this->menuGroups
     *
     * Depends on the following external resources:
     * - FWUser
     * - Init object
     */
    protected function init(): void {
        global $_CORELANG, $_ARRAYLANG;

        // 1. define core component order
        $baseComponentsOrder = array(
            'Home',
            'ContentManager',
            'Contact',
            'Media',
            'Stats',
            'Core', // TODO: Find a better way to load administration-things
        );

        // 2. init legacy entries
        // Define legacy entries (these need to be removed later on)
        $this->menuGroups = array(
            'Home' => array(
                'label' => 'TXT_HOME',
                'icon' => 'fa-building fa-solid',
                'iconInactive' => 'fa-building fa-regular',
                'order' => 1,
                'accessId' => 0,
                'base' => true,
            ),
            'ContentManager' => array(
                'label' => 'TXT_CONTENT_MANAGEMENT',
                'icon' => 'fa-clone fa-solid',
                'iconInactive' => 'fa-clone fa-regular',
                'order' => 2,
                'accessId' => 1,
                'base' => true,
            ),
            'Contact' => array(
                'label' => 'TXT_CONTACTS',
                'icon' => 'fa-file-lines fa-solid',
                'iconInactive' => 'fa-file-lines fa-regular',
                'order' => 3,
                'accessId' => 84,
                'base' => true,
            ),
            'MediaMenu' => array(
                'label' => 'TXT_CORE_MEDIA',
                'icon' => 'fa-folder-closed fa-solid',
                'iconInactive' => 'fa-folder-closed fa-regular',
                'order' => 4,
                'accessId' => 162,
                'base' => true,
            ),
            'Stats' => array(
                'label' => 'TXT_CORE_STATS',
                'icon' => 'fa-chart-bar fa-solid',
                'iconInactive' => 'fa-chart-bar fa-regular',
                'order' => 5,
                'accessId' => 163,
                'base' => true,
            ),
            'Core' => array(
                'label' => 'TXT_ADMINISTRATION',
                'icon' => 'fa-screwdriver-wrench fa-solid',
                'iconInactive' => 'fa-wrench fa-solid',
                'order' => 5,
                'accessId' => 3,
                'base' => true,
            ),
            'Shop' => array(
                'label' => 'TXT_SHOP',
                'icon' => 'fa-credit-card fa-solid',
                'iconInactive' => 'fa-credit-card fa-regular',
                'order' => 0,
                'accessId' => 4,
            ),
            'Newsletter' => array(
                'label' => 'TXT_CORE_EMAIL_MARKETING',
                'icon' => 'fa-envelope fa-solid',
                'iconInactive' => 'fa-envelope fa-regular',
                'order' => 0,
                'accessId' => 152,
            ),
            'Crm' => array(
                'label' => 'TXT_CRM',
                'icon' => 'fa-address-book fa-solid',
                'iconInactive' => 'fa-address-book fa-regular',
                'order' => 0,
                'accessId' => 194,
            ),
            'MediaDir' => array(
                'label' => 'TXT_MEDIADIR_MODULE',
                'icon' => 'fa-object-group fa-solid',
                'iconInactive' => 'fa-object-group fa-regular',
                'order' => 0,
                'accessId' => 153,
            ),
            'Calendar' => array(
                'label' => 'TXT_CALENDAR',
                'icon' => 'fa-calendar-check fa-solid',
                'iconInactive' => 'fa-calendar-check fa-regular',
                'order' => 0,
                'accessId' => 16,
            ),
        );
        $this->menuItems = array(
            array(
                'Home',
                'TXT_DASHBOARD',
                (string) \Cx\Core\Routing\Url::fromBackend('Home'),
                '_self',
                'Home',
                1,
                0,
            ),
            array(
                'ContentManager',
                'TXT_CONTENT_MANAGER',
                (string) \Cx\Core\Routing\Url::fromBackend('ContentManager'),
                '_self',
                'ContentManager',
                2,
                6,
            ),
            array(
                'ContentManager',
                'TXT_CONTENT_HISTORY',
                (string) \Cx\Core\Routing\Url::fromBackend('ContentWorkflow'),
                '_self',
                'ContentWorkflow',
                3,
                75,
            ),
            array(
                'ContentManager',
                'TXT_BLOCK_SYSTEM',
                (string) \Cx\Core\Routing\Url::fromBackend('Block'),
                '_self',
                'Block',
                4,
                76,
            ),
            array(
                'Contact',
                'TXT_CONTACT_CONTACT_FORMS',
                (string) \Cx\Core\Routing\Url::fromBackend('Contact'),
                '_self',
                'Contact',
                1,
                84,
            ),
            array(
                'Contact',
                'TXT_CONTACT_SETTINGS',
                (string) \Cx\Core\Routing\Url::fromBackend('Contact', 'settings'),
                '_self',
                'Contact',
                2,
                85,
            ),
            array(
                'MediaMenu',
                'TXT_IMAGE_ADMINISTRATION',
                (string) \Cx\Core\Routing\Url::fromBackend('Media', '', 0, array('archive' => 'content')),
                '_self',
                'Media',
                1,
                32,
            ),
            array(
                'MediaMenu',
                'TXT_MEDIA_ARCHIVES',
                (string) \Cx\Core\Routing\Url::fromBackend('Media', '', 0, array('archive' => 'archive1')),
                '_self',
                'Media',
                2,
                7,
            ),
            array(
                'MediaMenu',
                'TXT_FILESHARING_MODULE',
                (string) \Cx\Core\Routing\Url::fromBackend('Media', '', 0, array('archive' => 'FileSharing')),
                '_self',
                'FileSharing',
                3,
                8,
            ),
            array(
                'MediaMenu',
                'TXT_DOWNLOADS',
                (string) \Cx\Core\Routing\Url::fromBackend('Downloads'),
                '_self',
                'Downloads',
                4,
                141,
            ),
            array(
                'Stats',
                'TXT_CORE_VISITOR_DETAILS',
                (string) \Cx\Core\Routing\Url::fromBackend('Stats', 'visitors'),
                '_self',
                'Stats',
                1,
                166,
            ),
            array(
                'Stats',
                'TXT_CORE_VISITORS_AND_PAGE_VIEWS',
                (string) \Cx\Core\Routing\Url::fromBackend('Stats', 'requests'),
                '_self',
                'Stats',
                2,
                164,
            ),
            array(
                'Stats',
                'TXT_CORE_REFERER',
                (string) \Cx\Core\Routing\Url::fromBackend('Stats', 'referer'),
                '_self',
                'Stats',
                3,
                167,
            ),
            array(
                'Stats',
                'TXT_CORE_SEARCH_ENGINES',
                (string) \Cx\Core\Routing\Url::fromBackend('Stats', 'spiders'),
                '_self',
                'Stats',
                4,
                168,
            ),
            array(
                'Stats',
                'TXT_CORE_SEARCH_TERMS',
                (string) \Cx\Core\Routing\Url::fromBackend('Stats', 'search'),
                '_self',
                'Stats',
                5,
                169,
            ),
            array(
                'Stats',
                'TXT_STATS_SETTINGS',
                (string) \Cx\Core\Routing\Url::fromBackend('Stats', 'settings'),
                '_self',
                'Stats',
                6,
                170,
            ),
            array(
                'Core',
                'TXT_SYSTEM_SETTINGS',
                (string) \Cx\Core\Routing\Url::fromBackend('Config'),
                '_self',
                'Config',
                1,
                17,
            ),
            array(
                'Core',
                'TXT_NET_MANAGER',
                (string) \Cx\Core\Routing\Url::fromBackend('NetManager'),
                '_self',
                'NetManager',
                2,
                196,
            ),
            array(
                'Core',
                'TXT_CORE_MODULE_PRIVACY',
                (string) \Cx\Core\Routing\Url::fromBackend('Privacy'),
                '_self',
                'Privacy',
                2,
                207,
            ),
            array(
                'Core',
                'TXT_USER_ADMINISTRATION',
                (string) \Cx\Core\Routing\Url::fromBackend('Access'),
                '_self',
                'Access',
                3,
                18,
            ),
            array(
                'Core',
                'TXT_DESIGN_MANAGEMENT',
                (string) \Cx\Core\Routing\Url::fromBackend('ViewManager'),
                '_self',
                'ViewManager',
                4,
                21,
            ),
            array(
                'Core',
                'TXT_CORE_LOCALE',
                (string) \Cx\Core\Routing\Url::fromBackend('Locale'),
                '_self',
                'Locale',
                5,
                22,
            ),
            array(
                'Core',
                'TXT_MODULE_MANAGER',
                (string) \Cx\Core\Routing\Url::fromBackend('ComponentManager'),
                '_self',
                'ComponentManager',
                6,
                23,
            ),
            array(
                'Core',
                'TXT_ALIAS_ADMINISTRATION',
                (string) \Cx\Core\Routing\Url::fromBackend('Alias'),
                '_self',
                'Alias',
                7,
                115,
            ),
            array(
                'Core',
                'TXT_CORE_MODULE_GEOIP',
                (string) \Cx\Core\Routing\Url::fromBackend('GeoIp'),
                '_self',
                'GeoIp',
                14,
                201,
            ),
            array(
                'Core',
                'TXT_CORE_ROUTING',
                (string) \Cx\Core\Routing\Url::fromBackend('Routing'),
                '_self',
                'Routing',
                14,
                200,
            ),
            // shop
            array(
                'Shop',
                'TXT_MODULE_SHOP_ACT_ORDERS',
                (string) \Cx\Core\Routing\Url::fromBackend('Shop'),
                '_self',
                'Shop',
                1,
                13,
            ),
            array(
                'Shop',
                'TXT_MODULE_SHOP_ACT_CATEGORIES',
                (string) \Cx\Core\Routing\Url::fromBackend('Shop', 'categories'),
                '_self',
                'Shop',
                1,
                13,
            ),
            array(
                'Shop',
                'TXT_MODULE_SHOP_ACT_PRODUCTS',
                (string) \Cx\Core\Routing\Url::fromBackend('Shop', 'products'),
                '_self',
                'Shop',
                1,
                13,
            ),
            array(
                'Shop',
                'TXT_MODULE_SHOP_ACT_MANUFACTURER',
                (string) \Cx\Core\Routing\Url::fromBackend('Shop', 'manufacturer'),
                '_self',
                'Shop',
                1,
                13,
            ),
            array(
                'Shop',
                'TXT_MODULE_SHOP_ACT_CUSTOMERS',
                (string) \Cx\Core\Routing\Url::fromBackend('Shop', 'customers'),
                '_self',
                'Shop',
                1,
                13,
                array('customerdetails', 'neweditcustomer'),
            ),
            array(
                'Shop',
                'TXT_MODULE_SHOP_ACT_STATISTICS',
                (string) \Cx\Core\Routing\Url::fromBackend('Shop', 'statistics'),
                '_self',
                'Shop',
                1,
                13,
            ),
            array(
                'Shop',
                'TXT_MODULE_SHOP_ACT_IMPORT',
                (string) \Cx\Core\Routing\Url::fromBackend('Shop', 'import'),
                '_self',
                'Shop',
                1,
                13,
            ),
            array(
                'Shop',
                'TXT_MODULE_SHOP_ACT_SETTINGS',
                (string) \Cx\Core\Routing\Url::fromBackend('Shop', 'settings'),
                '_self',
                'Shop',
                1,
                13,
            ),
            // newsletter
            array(
                'Newsletter',
                'TXT_NEWSLETTER_OVERVIEW',
                (string) \Cx\Core\Routing\Url::fromBackend('Newsletter', 'mails'),
                '_self',
                'Newsletter',
                1,
                171,
                array('news'),
            ),
            array(
                'Newsletter',
                'TXT_NEWSLETTER_LISTS',
                (string) \Cx\Core\Routing\Url::fromBackend('Newsletter', 'lists'),
                '_self',
                'Newsletter',
                2,
                172,
            ),
            array(
                'Newsletter',
                'TXT_NEWSLETTER_RECIPIENTS',
                (string) \Cx\Core\Routing\Url::fromBackend('Newsletter', 'users'),
                '_self',
                'Newsletter',
                3,
                174,
            ),
            array(
                'Newsletter',
                'TXT_SETTINGS',
                (string) \Cx\Core\Routing\Url::fromBackend('Newsletter', 'Settings'),
                '_self',
                'Newsletter',
                5,
                176,
            ),
            // crm
            array(
                'Crm',
                'TXT_CRM_CUSTOMER',
                (string) \Cx\Core\Routing\Url::fromBackend('Crm', 'customers'),
                '_self',
                'Crm',
                1,
                194,
            ),
            array(
                'Crm',
                'TXT_CRM_TASKS',
                (string) \Cx\Core\Routing\Url::fromBackend('Crm', 'task'),
                '_self',
                'Crm',
                2,
                194,
            ),
            array(
                'Crm',
                'TXT_CRM_OPPORTUNITY',
                (string) \Cx\Core\Routing\Url::fromBackend('Crm', 'deals'),
                '_self',
                'Crm',
                3,
                194,
            ),
            array(
                'Crm',
                'TXT_CRM_SETTINGS',
                (string) \Cx\Core\Routing\Url::fromBackend('Crm', 'settings'),
                '_self',
                'Crm',
                4,
                195,
            ),
            // mediadir
            array(
                'MediaDir',
                'TXT_MEDIADIR_OVERVIEW',
                (string) \Cx\Core\Routing\Url::fromBackend('MediaDir'),
                '_self',
                'MediaDir',
                1,
                153,
            ),
            array(
                'MediaDir',
                'TXT_MEDIADIR_MANAGE_ENTRIES',
                (string) \Cx\Core\Routing\Url::fromBackend('MediaDir', 'entries'),
                '_self',
                'MediaDir',
                1,
                153,
                array('modify_entry'),
            ),
            array(
                'MediaDir',
                'TXT_MEDIADIR_INTERFACES',
                (string) \Cx\Core\Routing\Url::fromBackend('MediaDir', 'interfaces'),
                '_self',
                'MediaDir',
                1,
                158,
            ),
            array(
                'MediaDir',
                'TXT_SETTINGS',
                (string) \Cx\Core\Routing\Url::fromBackend('MediaDir', 'settings'),
                '_self',
                'MediaDir',
                1,
                159,
            ),
            // calendar
            array(
                'Calendar',
                'TXT_CALENDAR_MENU_OVERVIEW',
                (string) \Cx\Core\Routing\Url::fromBackend('Calendar'),
                '_self',
                'Calendar',
                1,
                16,
            ),
            array(
                'Calendar',
                'TXT_CALENDAR_CATEGORIES',
                (string) \Cx\Core\Routing\Url::fromBackend('Calendar', 'categories'),
                '_self',
                'Calendar',
                1,
                165,
            ),
            array(
                'Calendar',
                'TXT_CALENDAR_MENU_SETTINGS',
                (string) \Cx\Core\Routing\Url::fromBackend('Calendar', 'settings'),
                '_self',
                'Calendar',
                1,
                181,
            ),
        );
        $predefinedComponents = array();
        foreach ($this->menuItems as $menuItem) {
            if (in_array($menuItem[4], $predefinedComponents)) {
                continue;
            }
            $predefinedComponents[] = $menuItem[4];
        }
        $skippedLegacyComponents = array(
            'Media1',
            'Media2',
            'Media3',
            'Media4',
            'Wysiwyg',
            'DataAccess',
            'DataSource',
            'Sync2',
            'Model',
            'Country',
        );

        // 3. load non-legacy entries (including 3rd-party)
        // for each component:
        $componentRepo = $this->cx->getDb()->getEntityManager()->getRepository(
            'Cx\Core\Core\Model\Entity\SystemComponent'
        );
        $objFWUser = \FWUser::getFWUserObject();
        foreach ($componentRepo->findAll() as $component) {
            if (!$component->getController('Backend')) {
                continue;
            }
            if (!$this->moduleExists($component->getName())) {
                // This should never happen, as component should not be loaded in this case.
                // But it does as components are loaded if they are installed and licensed,
                // but ModuleChecker only considers it existing if a page exists as well.
                continue;
            }
            if (in_array($component->getName(), $predefinedComponents)) {
                continue;
            }
            if (in_array($component->getName(), $skippedLegacyComponents)) {
                continue;
            }
            $key = count($this->menuGroups);
            $icon = 'fa-gears fa-solid';
            $iconInactive = 'fa-gear fa-solid';
            try {
                $icon = $component->getConfigValue('meta/icons/default');
                $iconInactive = $component->getConfigValue('meta/icons/inactive');
            } catch (\Exception $e) {}
            $this->menuGroups[$component->getName()] = array(
                'label' => 'TXT_' . strtoupper($component->getType()) . '_' . strtoupper($component->getName()),
                'icon' => $icon,
                'iconInactive' => $iconInactive,
                'order' => 0, // sort alphabetically
                'accessId' => 0,
            );
            // TODO: Load icon from component (if set)
            $i = 1;
            foreach ($component->getController('Backend')->getParsedCommands() as $key=>$command) {
                if ($command['permission'] && !$command['permission']->hasAccess()) {
                    continue;
                }
                $this->menuItems[] = array(
                    $component->getName(),
                    $command['langKey'],
                    (string) \Cx\Core\Routing\Url::fromBackend($component->getName(), (string) $key),
                    '_self',
                    $component->getName(),
                    $i,
                    0,
                );
                $i++;
            }
        }
        // 4. Check permissions and resolve titles
        $objInit = \Env::get('init');
        foreach ($this->menuGroups as $componentName=>$groupItem) {
            if (
                !$objFWUser->objUser->getAdminStatus() &&
                $groupItem['accessId'] !== 0 &&
                !in_array($groupItem['accessId'], $objFWUser->objUser->getStaticPermissionIds())
            ) {
                unset($this->menuGroups[$componentName]);
                continue;
            }

            if ($componentName != 'MediaMenu' && !$this->moduleExists($componentName)) {
                unset($this->menuGroups[$componentName]);
                continue;
            }

            if (
                $componentName != 'MediaMenu' &&
                !in_array(
                    $componentName,
                    array_merge(array('Core'), \Env::get('cx')->getLicense()->getLegalComponentsList())
                )
            ) {
                unset($this->menuGroups[$componentName]);
                continue;
            }

            $translatedLabel = $groupItem['label'];
            if (isset($_CORELANG[$translatedLabel])) {
                $translatedLabel = $_CORELANG[$translatedLabel];
            } else {
                // load language file
                $componentLanguageData = $objInit->getComponentSpecificLanguageData(
                    $componentName,
                    false,
                    $objInit->backendLangId
                );
                if (isset($componentLanguageData[$translatedLabel])) {
                    $translatedLabel = $componentLanguageData[$translatedLabel];
                } else {
                    $translatedLabel = preg_replace('/.*MODULE_/', '', $translatedLabel);
                }
            }
            $this->menuGroups[$componentName]['label'] = $translatedLabel;
        }
        foreach ($this->menuItems as $id=>$menuItem) {
            if (
                !$objFWUser->objUser->getAdminStatus() &&
                $menuItem[6] !== 0 &&
                !in_array($menuItem[6], $objFWUser->objUser->getStaticPermissionIds())
            ) {
                unset($this->menuItems[$id]);
                continue;
            }

            if (!$this->moduleExists($menuItem[4])) {
                unset($this->menuItems[$id]);
                continue;
            }

            $translatedLabel = $menuItem[1];
            if (isset($_CORELANG[$translatedLabel])) {
                $translatedLabel = $_CORELANG[$translatedLabel];
            } else {
                // load language file
                $componentLanguageData = $objInit->getComponentSpecificLanguageData(
                    $menuItem[4],
                    false,
                    $objInit->backendLangId
                );
                if (isset($componentLanguageData[$translatedLabel])) {
                    $translatedLabel = $componentLanguageData[$translatedLabel];
                } else {
                    $translatedLabel = preg_replace('/.*_ACT_/', '', $translatedLabel);
                }
            }
            $this->menuItems[$id][1] = $translatedLabel;
        }
    }

    /**
     * Creates the administration navigation
     *
     * @param \HTML_Template_Sigma $template Template to parse admin nav into
     * @param string $activeCmd Current CMD
     * Considers the users' rights and only shows what they're
     * allowed to see
     * @global array  $_CORELANG
     * @global array  $_CONFIG
     */
    public function getAdminNavbar(
        \HTML_Template_Sigma $template,
        string $activeCmd,
        string $activeAct,
        string $activeTpl
    ): void {
        global $_CORELANG, $_CONFIG;

        $template->addBlockfile('NAVIGATION_OUTPUT', 'navigation_output', 'BackendNavigation.html');

        reset($this->menuItems);

        if (empty($activeTpl)) {
            $actParts = explode('/', $activeAct);
            if (count($actParts) > 1) {
                $activeAct = $actParts[0];
                $activeTpl = $actParts[1];
            }
        }

        // Find the currently active menu entry
        $possibleMatches = array();
        foreach ($this->menuItems as $menuItemId=>$menuItem) {
            $matches = array();
            if (
                !preg_match(
                    '#^/cadmin/([^/?]+)(/([^/?]+)|())((/[^/?]+)|())#',
                    $menuItem[2],
                    $matches
                )
            ) {
                // This case is for external links. Internal links should never get here.
                continue;
            }
            if ($matches[1] != $activeCmd) {
                continue;
            }
            $possibleMatches[$menuItemId] = 1;
            if (
                $matches[3] != $activeAct &&
                (!isset($menuItem[7]) || !in_array($activeAct, $menuItem[7]))
            ) {
                continue;
            }
            $possibleMatches[$menuItemId]++;
            if ($matches[5] != $activeTpl) {
                continue;
            }
            $possibleMatches[$menuItemId]++;
        }
        arsort($possibleMatches);
        $activeMenuId = key($possibleMatches);

        uasort(
            $this->menuGroups,
            function($a, $b) {
                // if $a AND $b have sort order > 0, use sort order to sort
                if ($a['order'] > 0 && $b['order'] > 0) {
                    return $a['order'] - $b['order'];

                    // if $a AND $b have 0 as sort order, sort them alphabetically
                } else if ($a['order'] == 0 && $b['order'] == 0) {
                    return strcasecmp($a['label'], $b['label']);

                    // if $a XOR $b have 0 as sort order, sort them after anything else
                } else {
                    return (($a['order'] == 0) * 2) - 1;
                }
            }
        );

        $groupSpacerInserted = false;
        $objFWUser = \FWUser::getFWUserObject();
        foreach ($this->menuGroups as $group_id => $group_data) {
            // Module group menu and module check!
            $navigation = '';

            //used to remember items in current menu group
            $arrMatchingItems = array();
            //used to remember captions of the current menu group
            //later used to perform array_multisort
            $arrMatchingItemCaptions = array();

            //(1/3) find entries of current menu group
            foreach ($this->menuItems as $link_key=>$link_data) {
                // checks if the links are childs of this area ID
                if ($link_data[0] == $group_id) {
                    $arrMatchingItems[$link_key] = $link_data;
                    $arrMatchingItemCaptions[$link_key] = $link_data[1];
                }
            }
            uasort(
                $arrMatchingItems,
                function($a, $b) {
                    // if $a AND $b have sort order > 0, use sort order to sort
                    if ($a[5] > 0 && $b[5] > 0) {
                        return $a[5] - $b[5];

                    // if $a AND $b have 0 as sort order, sort them alphabetically
                    } else if ($a[5] == 0 && $b[5] == 0) {
                        return strcasecmp($a[1], $b[1]);

                    // if $a XOR $b have 0 as sort order, sort them after anything else
                    } else {
                        return (($a[5] == 0) * 2) - 1;
                    }
                }
            );

            //(3/3) display a nice ordered menu.
            $subentryActive = false;
            $hasSubentries = false;
            foreach ($arrMatchingItems as $link_key=>$link_data) {
                $cssClass = 'inactive';
                if ($link_key == $activeMenuId) {
                    $cssClass = 'active';
                }

                // active exceptions for media and content module
                // TODO: This list should not exist and should be solved differently!
                // TODO: See CLX-5246
                if (preg_match('/^\/cadmin\/([^\/?]+)(?:.*[&?]archive\=([^&#]+))?/', $link_data[2], $arrMatch)) {
                    if ($arrMatch[1] == 'Media' && !empty($arrMatch[2])) {
                        $activeArchive = $_GET['archive'] ?? '';
                        $cssClass = 'inactive';
                        switch ($activeArchive) {
                            case 'archive1':
                            case 'archive2':
                            case 'archive3':
                            case 'archive4':
                                $activeArchive = 'archive1';
                                break;
                            case 'FileSharing':
                                break;
                            default:
                                $activeArchive = 'content';
                        }
                        if ($activeCmd == 'Media' && $activeArchive == $arrMatch[2]) {
                            $cssClass = 'active';
                        }
                    }
                }
                // TODO: See CLX-5249
                if ($link_data[6] == 5) {
                    if (!\Permission::checkAccess(127, 'static', true)) {
                        continue;
                    }
                }

                $hasSubentries = true;
                if ($cssClass == 'active') {
                    $subentryActive = true;
                }
                $template->setVariable(array(
                    'CSS_CLASS' => trim($cssClass),
                    'TARGET_URL' => strip_tags($link_data[2]),
                    'LINK_TITLE' => htmlentities($link_data[1], ENT_QUOTES, CONTREXX_CHARSET),
                    'TARGET_WINDOW' => $link_data[3],
                    'TARGET_NAME' => htmlentities($link_data[1], ENT_QUOTES, CONTREXX_CHARSET),
                ));
                $template->parse('navigationRow');
            }
            if ($hasSubentries) {
                $groupIcon = '';
                $groupIconInactive = '';
                $groupSpacer = '';
                if (isset($this->menuGroups[$group_id])) {
                    $groupIcon = $this->menuGroups[$group_id]['icon'];
                    $groupIconInactive = $this->menuGroups[$group_id]['iconInactive'];
                    if (
                        !$groupSpacerInserted
                        && !($this->menuGroups[$group_id]['base'] ?? false)
                    ) {
                        $groupSpacer = 'navigation_spacer';
                        $groupSpacerInserted = true;
                    }
                }
                $template->setVariable(array(
                    'NAVIGATION_GROUP_NAME' => contrexx_raw2xhtml($group_data['label']),
                    'NAVIGATION_GROUP_ID' => $group_id,
                    'NAVIGATION_GROUP_ICON' => $groupIcon,
                    'NAVIGATION_GROUP_ICON_INACTIVE' => $groupIconInactive,
                    'NAVIGATION_GROUP_CLASS' => ($subentryActive ? 'active' : 'inactive'),
                    'NAVIGATION_GROUP_SPACER' => $groupSpacer,
                ));
                $template->parse('navigationGroup');
            }
        }

        $template->setVariable(array(
            'TXT_SEARCH'                    => $_CORELANG['TXT_SEARCH'],
            'TXT_HOME_LINKNAME'             => $_CORELANG['TXT_HOME'],
            'TXT_DASHBOARD_LINKNAME'        => $_CORELANG['TXT_DASHBOARD'],
            'NAVIGATION_DASHBOARD_CLASS'    => empty($_GET['cmd']) ? 'active' : 'inactive',
        ));
        $template->parse('navigation_output');
    }

    /**
     * Tests whether the component with the given name is installed
     *
     * @return bool True if component is installed or component name is empty
     */
    protected function moduleExists(string $component): bool {
        if (empty($component)) {
            return true;
        }

        if (
            $component == 'Core' ||
            (
                $component != 'News' &&
                contrexx_isCoreModule($component)
            )
        ) {
            return true;
        } else {
            return contrexx_isModuleInstalled($component);
        }
    }

    /**
     * Tells whether a given component has its own level 1 navigation point
     *
     * @param string $componentName Name of the component name to check
     * @return bool True If $componentName has its own level 1 navigation point
     */
    public function isComponentItsOwnNavPoint(string $componentName): bool {
        $mainPoint = '';
        // find component name in 2nd level
        foreach ($this->menuItems as $values) {
            if ($values[4] != $componentName) {
                continue;
            }
            $mainPoint = $values[0];
            break;
        }
        if ($mainPoint != $componentName) {
            return false;
        }
        // find all entries in same main point
        // return if all are of the same component
        foreach ($this->menuItems as $values) {
            if ($values[0] != $mainPoint) {
                continue;
            }
            if ($values[4] != $componentName) {
                return false;
            }
        }
        return true;
    }
}
