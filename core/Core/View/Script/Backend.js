document.addEventListener("DOMContentLoaded", function() {
    const caption = document.querySelector("table.adminlist caption");
    if (caption && caption.textContent.trim() == "") {
        caption.style.height = "0";
        caption.style.padding = "0";
    }
});
cx.ready(function() {
    for (const el of document.querySelectorAll("[data-copyable]")) {
        const wrapper = document.createElement("div");
        wrapper.classList.add("copy-item");
        el.parentNode.insertBefore(wrapper, el);
        wrapper.appendChild(el);

        const copyLink = document.createElement("a");
        copyLink.href = "#";
        copyLink.innerHTML = "<i class='fa-regular fa-copy'></i>";
        wrapper.appendChild(copyLink);

        const tooltip = document.createElement("span");
        tooltip.classList.add("tooltip-message");
        tooltip.innerHTML = cx.variables.get("TXT_COPY_TO_CLIPBOARD_DONE", "Core/lang");
        wrapper.appendChild(tooltip);

        cx.ui.tooltip(copyLink);
        cx.jQuery(copyLink).tooltip().hide();
        cx.jQuery(copyLink).unbind("mouseover");
        cx.jQuery(copyLink).unbind("mouseleave");

        // Note: title can only be set after initializing tooltip as the title
        // is otherwise used as tooltip text.
        copyLink.title = cx.variables.get("TXT_COPY_TO_CLIPBOARD", "Core/lang");

        copyLink.addEventListener(
            "click",
            function(e) {
                e.preventDefault();
                const tmpInput = document.createElement("input");
                tmpInput.type = "text";
                let value = "";
                if (el.value != undefined) {
                    value = el.value;
                } else {
                    value = el.innerHTML;
                }
                tmpInput.value = value;
                document.body.appendChild(tmpInput);
                tmpInput.select();
                document.execCommand("Copy");
                document.body.removeChild(tmpInput);

                cx.jQuery(copyLink).tooltip().show();
                setTimeout(
                    function() {
                        cx.jQuery(copyLink).tooltip().hide();
                    },
                    1000
                );
            }
        );
    }
});
