// TODO: This could should be moved to the paging component once it exists
document.addEventListener("DOMContentLoaded", function() {
    // save a list of all elements that exist at page load
    const staticEls = Array.from(document.querySelectorAll("div.paging input"));

    let timer;
    function debounce(callback) {
        clearTimeout(timer);
        timer = setTimeout(callback, 500);
    }

    document.addEventListener("change", function(e) {
        const el = e.target;
        if (!el.matches("div.paging input")) {
            return;
        }
        if (
            parseInt(el.value) > parseInt(el.getAttribute("max")) ||
            parseInt(el.value) < parseInt(el.getAttribute("min"))
        ) {
            return;
        }
        debounce(function() {
            const pageOffset = el.dataset.entriesPerPage * (el.value - 1);
            let url = el.dataset.url.replace("%25d", pageOffset);
            url = url + "&csrf=" + cx.variables.get('csrf', 'contrexx');
            if (staticEls.find(element => element === el)) {
                document.location = url;
            } else {
                cx.trigger("jumpToPage", "paging", { el: el, offset: pageOffset });
            }
        });
    });
});
