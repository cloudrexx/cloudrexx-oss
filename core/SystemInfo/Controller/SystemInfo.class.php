<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * Settings
 * @copyright   CLOUDREXX CMS - CLOUDREXX AG
 * @author        Cloudrexx Development Team <info@cloudrexx.com>
 * @version        1.0.0
 * @package     cloudrexx
 * @subpackage  core_systeminfo
 * @todo        Edit PHP DocBlocks!
 */

namespace Cx\Core\SystemInfo\Controller;

/**
 * Settings
 * @copyright   CLOUDREXX CMS - CLOUDREXX AG
 * @author        Cloudrexx Development Team <info@cloudrexx.com>
 * @access        public
 * @version        1.0.0
 * @package     cloudrexx
 * @subpackage  core_systeminfo
 */
class SystemInfo
{
    var $pageTitle="";
    var $statusMessage="";

    private $act = '';

    /**
     * Constructor
     * @access public
     */
    function __construct() {}

    public function setNavigation()
    {
        global $objTemplate, $_ARRAYLANG;

        $objTemplate->setVariable('CONTENT_NAVIGATION', '
            <a href="index.php?cmd=SystemInfo" class="'.($this->act == '' ? 'active' : '').'">'.$_ARRAYLANG['TXT_OVERVIEW'].'</a>
            <a href="index.php?cmd=SystemInfo&amp;act=phpinfo" class="'.($this->act == 'phpinfo' ? 'active' : '').'">'.$_ARRAYLANG['TXT_PHP_INFO'].'</a>
        ');
    }

    /**
     * Sets the requested content
     *
     * @global  array   Core language
     * @global  mixed   Template
     * @return  void
     */
    function getPage() {
        global $_ARRAYLANG, $objTemplate;

        if(!isset($_GET['act'])) {
            $_GET['act']="";
        }

        switch($_GET['act']) {
            case "phpinfo":
                $objTemplate->addBlockfile('ADMIN_CONTENT', 'server_phpinfo', 'server_phpinfo.html');
                $this->pageTitle = $_ARRAYLANG['TXT_PHP_INFO'];
                $this->showPHPInfo();
            break;

            default:
                $objTemplate->addBlockfile('ADMIN_CONTENT', 'server_settings', 'server_settings.html');
                $this->pageTitle = $_ARRAYLANG['TXT_OVERVIEW'];
                $this->showServerInfo();
            break;
        }

        $objTemplate->setVariable(array(
            'CONTENT_TITLE'    => $this->pageTitle,
            'CONTENT_STATUS_MESSAGE'    => $this->statusMessage,
        ));

        $this->act = $_REQUEST['act'];
        $this->setNavigation();
    }


    /**
     * Set the server information
     */
    function showServerInfo() {
        global $_CONFIG, $objTemplate;

        $objTemplate->setVariable(array(
            'ADMIN_CMS_NAME'           => htmlentities($_CONFIG['coreCmsName'], ENT_QUOTES, CONTREXX_CHARSET),
            'ADMIN_CMS_VERSION'        => str_replace(' Service Pack 0', '', preg_replace('#^(\d+\.\d+)\.(\d+)$#', '$1 Service Pack $2', $_CONFIG['coreCmsVersion'])),
            'MYADMIN_DB_VERSION'       => \Env::get('cx')->getDb()->getPdoConnection()->getAttribute(\PDO::ATTR_SERVER_VERSION),
            'MYADMIN_PHP_VERSION'      => @phpversion(),
            'MYADMIN_WEBSERVER'        => $this->getServerSoftware(),
            'MYADMIN_PHP_BUILDON'      => @php_uname(),
            'MYADMIN_PHP_INTERFACE'    => @php_sapi_name(),
            'MYADMIN_ZEND_VERSION'     => @zend_version()
        ));
    }


    /**
     * Sets information about the PHP version in use
     * @return void
     */
    function showPHPInfo() {
        global $objTemplate;
        ob_start();
        phpinfo(INFO_GENERAL | INFO_CONFIGURATION | INFO_MODULES);
        $phpinfo = ob_get_contents();
        ob_end_clean();
        preg_match_all('#<body[^>]*>(.*)</body>#siU', $phpinfo, $output);
        $output = preg_replace('#<table#', '<table class="adminlist"', $output[1][0]);
        $output = preg_replace('#(\w),(\w)#', '\1, \2', $output);
        //$output = preg_replace('#border="0" cellpadding="3" width="600"#', 'border="0" cellspacing="0" cellpadding="4" width="600"', $output);
        $output = preg_replace('#<hr />#', '', $output);
        $objTemplate->setVariable('SERVER_PHPINFO',$output);
    }


    /**
     * Sets information about the server
     * @return string  Server information
     */
    function getServerSoftware()
    {
        if (isset($_SERVER['SERVER_SOFTWARE'])) {
            return $_SERVER['SERVER_SOFTWARE'];
        } else if (($sf = getenv('SERVER_SOFTWARE'))) {
            return $sf;
        } else {
            return 'n/a';
        }
    }
}
