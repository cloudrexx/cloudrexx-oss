<?php
declare(strict_types=1);

/**
 * Cloudrexx
 *
 * @link      https://www.cloudrexx.com
 * @copyright Cloudrexx AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

namespace Cx\Core\PageTree;

/**
 * Handler to generate XML sitemaps
 *
 * Instances of this class correspond to a single XML sitemap file. Whereas the
 * static method {@see XmlSitemapPageTree::write()} does generate all XML
 * sitemap files.
 *
 * @copyright   Cloudrexx AG
 * @author      Michael Ritter <michael.ritter@cloudrexx.com>
 * @author      Thomas Wirz <thomas.wirz@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  core_pagetree
 */
class XmlSitemapPageTree extends PageTree {

    /**
     * @var \Cx\Lib\FileSystem\File
     */
    protected $file;

    /**
     * @var string
     */
    protected $localeCode = '';

    private static $strFileName = 'sitemap.xml';
    private static $strFileNameWithLang = 'sitemap_%s.xml';

    /**
     * Override the constructor from the PageTree
     * @see Cx\Core\PageTree::__construct()
     * @param type $entityManager
     * @param type $license
     * @param type $maxDepth
     * @param type $rootNode
     * @param type $lang
     * @param type $currentPage
     * @param type $skipInvisible
     * @param type $considerLogin
     * @param boolean $skipInactive           Skip inactive pages
     * @param boolean $considerSeoEnabledOnly Consider seo enabled pages alone
     */
    public function __construct($entityManager, $license, $maxDepth = 0, $rootNode = null,
                                $lang = null, $currentPage = null, $skipInvisible = false,
                                $considerLogin = false,
                                $skipInactive = true,
                                $considerSeoEnabledOnly = true
    ) {
        parent::__construct($entityManager, $license, $maxDepth, $rootNode, $lang,
                            $currentPage, $skipInvisible, $considerLogin, $skipInactive, $considerSeoEnabledOnly);
    }

    /**
     * Writes the XML-Sitemap in all langs (if activated in config)
     * @global type $_CORELANG
     * @return boolean True on success (including deactivated), false otherwise
     */
    public static function write() {
        global $_CORELANG;
        static::cleanUp();
        $cx = \Cx\Core\Core\Controller\Cx::instanciate();
        $useSitemaps = $cx->getConfig()->getBaseConfigString('xmlSitemapStatus') == 'on';
        if (!$useSitemaps) {
            return true;
        }
        $arrActiveLanguages = array();
        foreach (\FWLanguage::getLanguageArray() as $arrLanguage) {
            if ($arrLanguage['frontend'] == 1) {
                $arrActiveLanguages[$arrLanguage['id']] = $arrLanguage['lang'];
            }
        }

        $sitemaps = [];
        foreach ($arrActiveLanguages as $langId => $langCode) {
            $langSitemap = new static(
                $cx->getDb()->getEntityManager(),
                $cx->getLicense(),
                0,
                null,
                $langId
            );
            if (!$langSitemap->writeXML()) {
                \Message::warning(
                    sprintf(
                        $_CORELANG['TXT_CORE_XML_SITEMAP_NOT_WRITABLE'],
                        sprintf(
                            static::$strFileNameWithLang,
                            $langCode
                        )
                    )
                );
                continue;
            }
            $sitemaps[] = $langSitemap;
        }
        if (!count($sitemaps)) {
            return false;
        }

        $sitemapIndexFile = $cx->getWebsiteDocumentRootPath() . '/' . static::$strFileName;
        $useSitemapIndex = $cx->getConfig()->getBaseConfigString('useVirtualLanguageDirectories') == 'on';
        if (
            !$useSitemapIndex
            || count($sitemaps) === 1
        ) {
            $sitemap = current($sitemaps);
            try {
                $sitemap->getFile()->copy(
                    $sitemapIndexFile,
                    true
                );
                return true;
            } catch (\Cx\Lib\FileSystem\FileSystemException $e) {
                \DBG::msg($e->getMessage());
                \Message::warning(
                    sprintf(
                        $_CORELANG['TXT_CORE_XML_SITEMAP_NOT_WRITABLE'],
                        static::$strFileName
                    )
                );
                return false;
            }
        }

        $sitemapIndex = [
            '<?xml version="1.0" encoding="UTF-8"?>',
            '<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'
        ];
        foreach ($sitemaps as $sitemap) {
            $sitemapIndex[] = '<sitemap>';
            $sitemapIndex[] = '<loc>' . $sitemap->getUrl()->toString() . '</loc>';
            // According to https://www.sitemaps.org/protocol.html#sitemapIndex_lastmod:
            // Identifies the time that the corresponding Sitemap file was
            // modified. It does not correspond to the time that any of the
            // pages listed in that Sitemap were changed. The value for the
            // lastmod tag should be in W3C Datetime format.
            $sitemapIndex[] = '<lastmod>' . date(DATE_ATOM) . '</lastmod>';
            $sitemapIndex[] = '</sitemap>';
        }
        $sitemapIndex[] = '</sitemapindex>';
        try {
            $objFile = new \Cx\Lib\FileSystem\PrivilegedFile($sitemapIndexFile);
            $objFile->touch();
            $objFile->write(implode("\n", $sitemapIndex));
            return true;
        } catch (\Cx\Lib\FileSystem\FileSystemException $e) {
            \DBG::log($e->getMessage());
            return false;
        }
    }

    /**
     * Drop XML sitemaps from filesystem that are no longer in use.
     */
    protected static function cleanUp(): void {
        $cx = \Cx\Core\Core\Controller\Cx::instanciate();
        $useSitemaps = $cx->getConfig()->getBaseConfigString('xmlSitemapStatus') == 'on';
        $docRoot = $cx->getWebsiteDocumentRootPath() . '/';
        $sitemaps = glob(
            $docRoot . 'sitemap{,_[a-zA-Z-]*}.xml',
            GLOB_NOSORT|GLOB_BRACE
        );
        if (!$sitemaps) {
            return;
        }
        $localeData = $cx->getComponent('Locale')->getLocaleData();
        foreach ($sitemaps as $sitemap) {
            preg_match(
                '/^' . preg_quote($docRoot, '/') . 'sitemap(?:_([a-zA-Z-]+))?\.xml$/',
                $sitemap,
                $match
            );
            $localeCode = $match[1] ?? '';
            if (
                $useSitemaps
                && (
                    !$localeCode
                    || isset($localeData['Hashtables']['IdByCode'][$localeCode])
                )
            ) {
                continue;
            }
            try {
                $objFile = new \Cx\Lib\FileSystem\PrivilegedFile($sitemap);
                $objFile->delete();
            } catch (\Cx\Lib\FileSystem\FileSystemException $e) {
                \DBG::msg($e->getMessage());
            }
        }
    }

    /**
     * Renders the PageTree element
     * @param type $title
     * @param type $level
     * @param type $hasChilds
     * @param type $lang
     * @param type $path
     * @param type $current
     * @param type $page
     * @return type
     */
    protected function renderElement($title, $level, $hasChilds, $lang, $path, $current, $page) {
        // TODO: setting the proper protocol should be done in Url::fromPage()
        $protocol = '';
        $config = \Env::get('config');
        if ($config['forceProtocolFrontend'] != 'none') {
            $protocol = $config['forceProtocolFrontend'];
        }

        // Skip redirects, symlinks and fallbacks to redirects and symlinks
        $resolvedPage = $page;
        while ($resolvedPage->getType() == \Cx\Core\ContentManager\Model\Entity\Page::TYPE_FALLBACK) {
            $resolvedPage = $resolvedPage->getFallback();
            // there can be fallback pages without an existing target
            if (!$resolvedPage) {
                return '';
            }
        }
        if (
            $resolvedPage->getType() == \Cx\Core\ContentManager\Model\Entity\Page::TYPE_REDIRECT ||
            $resolvedPage->getType() == \Cx\Core\ContentManager\Model\Entity\Page::TYPE_SYMLINK
        ) {
            return '';
        }

        $xml = "\t" . '<url>' .
                "\n\t\t" . '<loc>' . \Cx\Core\Routing\Url::fromPage($page, array(), $protocol)->toString() . '</loc>' .
                "\n\t\t" . '<lastmod>' . $this->getLastModificationDate($page) . '</lastmod>' .
                "\n\t\t" . '<changefreq>' . $this->getChangingFrequency($page) . '</changefreq>' .
                "\n\t\t" . '<priority>0.5</priority>';
        $alternates = [];
        foreach ($page->getNode()->getPages() as $alternate) {
            if (!$alternate->isActive()) {
                continue;
            }
            if (!$alternate->getMetarobots()) {
                continue;
            }
            if (
                in_array(
                    $alternate->getType(),
                    [
                        \Cx\Core\ContentManager\Model\Entity\Page::TYPE_REDIRECT,
                        \Cx\Core\ContentManager\Model\Entity\Page::TYPE_SYMLINK,
                    ]
                )
            ) {
                continue;
            }
            $alternates[$alternate->getLang()] = $alternate;
        }
        if (count($alternates)) {
            foreach ($alternates as $alternate) {
                $xml .= "\n\t\t" . '<xhtml:link rel="alternate" hreflang="'
                    . \FWLanguage::getLanguageCodeById($alternate->getLang())
                    . '" href="' . \Cx\Core\Routing\Url::fromPage(
                        $alternate,
                        array(),
                        $protocol
                    )->toString()
                    . '"/>';
            }
        }
        if (isset($alternates[\FWLanguage::getDefaultLangId()])) {
            $xDefault = $alternates[\FWLanguage::getDefaultLangId()];
            $xml .= "\n\t\t" . '<xhtml:link rel="alternate" hreflang="x-default" href="'
                . \Cx\Core\Routing\Url::fromPage(
                $xDefault,
                array(),
                $protocol
                )->toString()
                . '"/>';
        }
        $xml .= "\n\t" . '</url>' . "\n";
        return $xml;
    }

    public function preRenderLevel($level, $lang, $parentNode) {}

    public function postRenderLevel($level, $lang, $parentNode) {}

    /**
     * PageTree override (unused)
     * @param type $level
     * @param type $hasChilds
     * @param type $lang
     * @param type $page
     * @return string
     */
    protected function postRenderElement($level, $hasChilds, $lang, $page) {
        return '';
    }

    /**
     * Renders the head of the PageTree
     * @param type $lang
     * @return type
     */
    protected function renderHeader($lang) {
        return '<?xml version="1.0" encoding="UTF-8"?>' . "\n" .
                '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xhtml="http://www.w3.org/1999/xhtml">' . "\n";
    }

    /**
     * Renders the foot of the PageTree
     * @param type $lang
     * @return string
     */
    protected function renderFooter($lang) {
        return '</urlset>';
    }

    /**
     * PageTree override (unused)
     * @param type $lang
     */
    protected function preRender($lang) {
    }

    /**
     * PageTree override (unused)
     * @param type $lang
     */
    protected function postRender($lang) {
    }


    /**
     * Write sitemap-file
     *
     * @global     object
     * @global     array
     * @param   array An array containing the language ID's of which languages should be included in the sitemap.
     * @param   string  The two letter language code of the selected language used as the virtual language path
     */
    protected function writeXML() {
        $localeCode = \FWLanguage::getLanguageCodeById($this->lang);
        if (!$localeCode) {
            return false;
        }
        $this->localeCode = $localeCode;
        $filename = sprintf(
            static::$strFileNameWithLang,
            $localeCode
        );
        $xml = $this->render();
        try {
            $cx = \Cx\Core\Core\Controller\Cx::instanciate();
            $this->file = new \Cx\Lib\FileSystem\PrivilegedFile(
                $cx->getWebsiteDocumentRootPath() . '/' . $filename
            );
            $this->file->touch();
            $this->file->write($xml);
        } catch (\Cx\Lib\FileSystem\FileSystemException $e) {
            \DBG::log($e->getMessage());
            return false;
        }

        return true;
    }

    /**
     * Return file instance of XML sitemap file
     * @return \Cx\Lib\FileSystem\File  Value of {@see static::$file}
     */
    public function getFile(): \Cx\Lib\FileSystem\File {
        return $this->file;
    }

    /**
     * Return locale code of XML sitemap file
     * @return string  Value of {@see static::$localeCode}
     */
    public function getLocaleCode(): string {
        return $this->localeCode;
    }

    /**
     * Return URL of this XML sitemap's location
     * @return \Cx\Core\Routing\Url URL of this XML-sitemap
     */
    public function getUrl(): \Cx\Core\Routing\Url {
        $url = \Cx\Core\Routing\Url::fromDocumentRoot();
        $url->setPath(static::$strFileName);
        $url->setLangDir($this->getLocaleCode());
        $url->isFrontend(true);
        return $url;
    }

    /**
     * Creates the modification-date of a page as a string which can be processed by google. The method uses
     * for module-pages the current date, for normale pages the date of last modification.
     *
     * @param        integer        $intModule: value of the module-field in the database
     * @param        string        $strCmd: value of the cmd-field in the database
     * @param        integer        $intTimestamp: last update of the page as a timestamp
     * @return        string        A date string which can be understood by google
     */
    protected function getLastModificationDate($page) {
        $date = $page->getLastModificationDateTime();
        return $date->format('Y-m-d');
    }

    /**
     * Returns the changing-frequency of the page depending on the database values. If the page is a module
     * page, the frequency is set to 'hourly', for normal pages to 'weekly'.
     *
     * @param        integer        $intModule: value of the module-field in the database
     * @param        string        $strCmd: value of the cmd-field in the database
     * @return        string        true, if the page is a module page. Otherwise false.
     */
    protected function getChangingFrequency($page) {
        return $page->getChangeFrequency();
    }

    protected function init() {

    }

    protected function preRenderElement($level, $hasChilds, $lang, $page) {

    }

    protected function getFullNavigation() {
        return true;
    }
}
