<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * This is the english language file for backend mode.
 * This file is included by Cloudrexx and all entries are set as placeholder
 * values for backend ACT template by SystemComponentBackendController
 *
 * @copyright   CLOUDREXX CMS - Cloudrexx AG Thun
 * @author      Project Team SS4U <info@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  core_netmanager
 */

global $_ARRAYLANG;

// Let's start with module info:
$_ARRAYLANG['TXT_CORE_NETMANAGER'] = 'Domains';
$_ARRAYLANG['TXT_NETMANAGER_MODULE_DESCRIPTION'] = 'Verwaltung der mit der Website verknüpften Domains.';

// Here come the ACTs:
$_ARRAYLANG['TXT_CORE_NETMANAGER_ACT_DEFAULT'] = 'Übersicht';
$_ARRAYLANG['TXT_CORE_NETMANAGER_ACT_TRACKEDSENDERDOMAIN'] = 'Absender-Verifikation';
$_ARRAYLANG['TXT_CORE_NETMANAGER_OVERVIEW_ENTITY'] = 'Domain';
$_ARRAYLANG['TXT_CORE_NETMANAGER_VERIFICATION_ENTITY'] = 'Absender-Verifikation';

$_ARRAYLANG['spfTitle'] = 'SPF';
$_ARRAYLANG['dmarcTitle'] = 'DMARC';
$_ARRAYLANG['dkimTitle'] = 'DKIM';
$_ARRAYLANG['dkimDomainkey'] = 'Selektor';
$_ARRAYLANG['dkimPublicKey'] = 'Wert';
$_ARRAYLANG['dkimKeyGenerator'] = 'Key Generator';
$_ARRAYLANG['status'] = 'Status';
$_ARRAYLANG['usages'] = 'Verwendet in';
$_ARRAYLANG['cachedAt'] = 'Letzte Aktualisierung';

$_ARRAYLANG['TXT_CORE_NETMANAGER_DOMAIN'] = 'Domain';
$_ARRAYLANG['TXT_CORE_NETMANAGER_DOMAIN_ADD_NAME'] = 'Domainname';
$_ARRAYLANG['TXT_CORE_NETMANAGER_USAGES_NONE'] = '-';

$_ARRAYLANG['TXT_CORE_NETMANAGER_VALIDATE'] = 'Verifizieren?';
$_ARRAYLANG['TXT_CORE_NETMANAGER_STATE_INVALID'] = 'Nicht verifiziert';
$_ARRAYLANG['TXT_CORE_NETMANAGER_STATE_VALID'] = 'Verifiziert';
$_ARRAYLANG['TXT_CORE_NETMANAGER_INVALID_ADDRESS'] = 'Ungültige Adresse';

$_ARRAYLANG['TXT_CORE_NETMANAGER_BLACKLISTED'] = 'Nicht verfügbar';
$_ARRAYLANG['TXT_CORE_NETMANAGER_BLACKLISTED_TOOLTIP'] = 'Diese Domain kann nicht als Absender-Domain für den E-Mail-Versand verwendet werden.';

$_ARRAYLANG['TXT_CORE_NETMANAGER_STATE'] = 'Status';
$_ARRAYLANG['TXT_CORE_NETMANAGER_RECORD'] = 'Abfrageresultat';
$_ARRAYLANG['TXT_CORE_NETMANAGER_RECORD_PROPOSAL'] = 'Vorgeschlagener DNS-Eintrag';
$_ARRAYLANG['TXT_CORE_NETMANAGER_RECORD_PROPOSAL_WARNING_SPF'] = 'Senden Sie E-Mails über weitere Dienste wie z.B. Google Workspace oder Microsoft 365 mit dieser Domain, so müssen Sie deren Server beim SPF-Eintrag ebenfalls hinzufügen. Wenden Sie sich in diesem Fall an uns oder an den Betreiber des Dienstes für weitere Unterstützung.';
$_ARRAYLANG['TXT_CORE_NETMANAGER_RECORD_PROPOSAL_WARNING_DMARC'] = 'Der vorgeschlagene DNS-Eintrag erfüllt die minimalen Anforderungen für DMARC. Um vom vollen Schutz von DMARC zu profitieren, muss der DNS-Eintrag individuell eingerichtet werden. Wenden Sie sich in diesem Fall an uns oder an den Betreiber des Dienstes für weitere Unterstützung.';
$_ARRAYLANG['TXT_CORE_NETMANAGER_RECORD_PROPOSAL_TARGET'] = 'Ziel';
$_ARRAYLANG['TXT_CORE_NETMANAGER_RECORD_PROPOSAL_TARGET_EMPTY'] = 'Leerlassen';
$_ARRAYLANG['TXT_CORE_NETMANAGER_RECORD_PROPOSAL_TYPE'] = 'Typ';
$_ARRAYLANG['TXT_CORE_NETMANAGER_RECORD_PROPOSAL_VALUE'] = 'Wert';
$_ARRAYLANG['TXT_CORE_NETMANAGER_RECORD_PROPOSAL_INTRO'] = 'Bitte hinterlegen Sie untenstehende(n) Wert(e) im DNS von <code>%1$s</code>. Eine Anleitung finden Sie unter <a target="_blank" href="%2$s">%3$s</a>';
$_ARRAYLANG['TXT_CORE_NETMANAGER_RECORD_PROPOSAL_EXTEND'] = 'Bitte ergänzen Sie den untenstehenden Wert im bestehenden SPF-Eintrag von <code>%1$s</code>. Eine Anleitung finden Sie unter <a target="_blank" href="%2$s">%3$s</a>.';
$_ARRAYLANG['TXT_CORE_NETMANAGER_RECORD_LAST_UPDATE'] = 'Letzte Aktualisierung erfolgte am %s';

$_ARRAYLANG['TXT_CORE_NETMANAGER_MANUAL'] = 'Anleitung';
$_ARRAYLANG['TXT_CORE_NETMANAGER_NO_RESULT'] = 'Keine Einträge vorhanden';

$_ARRAYLANG['TXT_CORE_NETMANAGER_REFRESH_LONG'] = 'Aus DNS-Zone abfragen';
$_ARRAYLANG['TXT_CORE_NETMANAGER_REFRESH_SHORT'] = 'Aktualisieren';
$_ARRAYLANG['TXT_CORE_NETMANAGER_BACK'] = 'Zurück';

$_ARRAYLANG['TXT_CORE_NETMANAGER_SENDER_AUTHENTICATION_SETTINGS'] = 'Absender-Authentifizierungseinstellungen';
$_ARRAYLANG['TXT_CORE_NETMANAGER_DOMAIN_NOT_COMPLIANT'] = 'Die Domain %1$s wurde nicht verifiziert oder authentifiziert. Bitte überprüfen Sie die %2$s, um sicherzustellen, dass Sie berechtigt sind, E-Mails von dieser Domain zu senden.';
$_ARRAYLANG['TXT_CORE_NETMANAGER_SENDER_AUTHENTICATION_SETTINGS_NOTE'] = 'Die folgenden Angaben beziehen sich auf den E-Mail-Versand von der Website. Die Absender-Verifikation für den E-Mail-Versand via Mail-Service muss separat eingerichtet werden. Siehe <a href="https://cloudrexx.atlassian.net/wiki/spaces/HELP/pages/592347137" target="_blank">Anleitung in der Anwendungshilfe</a>.';
