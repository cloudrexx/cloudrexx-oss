<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * This is the english language file for backend mode.
 * This file is included by Cloudrexx and all entries are set as placeholder
 * values for backend ACT template by SystemComponentBackendController
 *
 * @copyright   CLOUDREXX CMS - Cloudrexx AG Thun
 * @author      Project Team SS4U <info@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  core_netmanager
 */

global $_ARRAYLANG;

// Let's start with module info:
$_ARRAYLANG['TXT_CORE_NETMANAGER'] = 'Domains';
$_ARRAYLANG['TXT_CORE_NETMANAGER_DESCRIPTION'] = 'This module shows the all Domain Alias';

// Here come the ACTs:
$_ARRAYLANG['TXT_CORE_NETMANAGER_ACT_DEFAULT'] = 'Overview';
$_ARRAYLANG['TXT_CORE_NETMANAGER_ACT_TRACKEDSENDERDOMAIN'] = 'Sender verification';
$_ARRAYLANG['TXT_CORE_NETMANAGER_OVERVIEW_ENTITY'] = 'Domain';
$_ARRAYLANG['TXT_CORE_NETMANAGER_VERIFICATION_ENTITY'] = 'Sender verification';

$_ARRAYLANG['spfTitle'] = 'SPF';
$_ARRAYLANG['dmarcTitle'] = 'DMARC';
$_ARRAYLANG['dkimTitle'] = 'DKIM';
$_ARRAYLANG['dkimDomainkey'] = 'Selector';
$_ARRAYLANG['dkimPublicKey'] = 'Value';
$_ARRAYLANG['dkimKeyGenerator'] = 'Key generator';
$_ARRAYLANG['status'] = 'State';
$_ARRAYLANG['usages'] = 'Used in';
$_ARRAYLANG['cachedAt'] = 'Last update';

$_ARRAYLANG['TXT_CORE_NETMANAGER_DOMAIN'] = 'Domain';
$_ARRAYLANG['TXT_CORE_NETMANAGER_DOMAIN_ADD_NAME'] = 'Domain name';
$_ARRAYLANG['TXT_CORE_NETMANAGER_USAGES_NONE'] = '-';

$_ARRAYLANG['TXT_CORE_NETMANAGER_VALIDATE'] = 'Verify?';
$_ARRAYLANG['TXT_CORE_NETMANAGER_STATE_INVALID'] = 'Not verified';
$_ARRAYLANG['TXT_CORE_NETMANAGER_STATE_VALID'] = 'Verified';
$_ARRAYLANG['TXT_CORE_NETMANAGER_INVALID_ADDRESS'] = 'Invalid address';

$_ARRAYLANG['TXT_CORE_NETMANAGER_BLACKLISTED'] = 'Nicht available';
$_ARRAYLANG['TXT_CORE_NETMANAGER_BLACKLISTED_TOOLTIP'] = 'This domain is not available as a sender domain.';

$_ARRAYLANG['TXT_CORE_NETMANAGER_STATE'] = 'State';
$_ARRAYLANG['TXT_CORE_NETMANAGER_RECORD'] = 'Query result';
$_ARRAYLANG['TXT_CORE_NETMANAGER_RECORD_PROPOSAL'] = 'Proposed DNS record';
$_ARRAYLANG['TXT_CORE_NETMANAGER_RECORD_PROPOSAL_WARNING_SPF'] = 'If you also send mails from other services like for example Google Workspace or Microsoft 365 using this domain you need to add their servers to the SPF record. For support in this matter please contact us or refer to the respective service directly.';
$_ARRAYLANG['TXT_CORE_NETMANAGER_RECORD_PROPOSAL_WARNING_DMARC'] = 'The proposed DNS record meets the minimal requirements for DMARC. To benefit from the full protection DMARC offers, the DNS record must be configured individually. In this case, please contact us or the service operator for further assistance.';
$_ARRAYLANG['TXT_CORE_NETMANAGER_RECORD_PROPOSAL_TARGET'] = 'Target';
$_ARRAYLANG['TXT_CORE_NETMANAGER_RECORD_PROPOSAL_TARGET_EMPTY'] = 'Leave empty';
$_ARRAYLANG['TXT_CORE_NETMANAGER_RECORD_PROPOSAL_TYPE'] = 'Type';
$_ARRAYLANG['TXT_CORE_NETMANAGER_RECORD_PROPOSAL_VALUE'] = 'Value';
$_ARRAYLANG['TXT_CORE_NETMANAGER_RECORD_PROPOSAL_INTRO'] = 'Please add the following record(s) on the DNS of <code>%1$s</code>. A manual can be found on <a target="_blank" href="%2$s">%3$s</a>';
$_ARRAYLANG['TXT_CORE_NETMANAGER_RECORD_PROPOSAL_EXTEND'] = 'Please extend the SPF record of <code>%1$s</code> by the value seen below. A manual can be found on <a target="_blank" href="%2$s">%3$s</a>.';
$_ARRAYLANG['TXT_CORE_NETMANAGER_RECORD_LAST_UPDATE'] = 'Record was last updated at %s';

$_ARRAYLANG['TXT_CORE_NETMANAGER_MANUAL'] = 'Manual';
$_ARRAYLANG['TXT_CORE_NETMANAGER_NO_RESULT'] = 'No record found';

$_ARRAYLANG['TXT_CORE_NETMANAGER_REFRESH_LONG'] = 'Re-fetch from DNS-zone';
$_ARRAYLANG['TXT_CORE_NETMANAGER_REFRESH_SHORT'] = 'Update';
$_ARRAYLANG['TXT_CORE_NETMANAGER_BACK'] = 'Back';

$_ARRAYLANG['TXT_CORE_NETMANAGER_SENDER_AUTHENTICATION_SETTINGS'] = 'sender authentication settings';
$_ARRAYLANG['TXT_CORE_NETMANAGER_DOMAIN_NOT_COMPLIANT'] = 'The domain %1$s has not been verified or authenticated. Please check the %2$s to ensure you have permission to send emails from that domain.';
$_ARRAYLANG['TXT_CORE_NETMANAGER_SENDER_AUTHENTICATION_SETTINGS_NOTE'] = 'The following information relates to sending emails from the website. Sender verification for sending emails via the mail service must be set up separately. See the <a href="https://cloudrexx.atlassian.net/wiki/spaces/HELP/pages/592347137" target="_blank">instructions in the application help</a>.';
