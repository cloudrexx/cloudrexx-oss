<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * Main controller for Net Manager
 *
 * @copyright   Cloudrexx AG
 * @author      Project Team SS4U <info@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  core_netmanager
 */

namespace Cx\Core\NetManager\Controller;

/**
 * Main controller for Net Manager
 *
 * @copyright   Cloudrexx AG
 * @author      Project Team SS4U <info@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  core_netmanager
 */
class ComponentController extends \Cx\Core\Core\Model\Entity\SystemComponentController implements \Cx\Core\Json\JsonAdapter {

    /**
     * @var array $trackedSenderDomainNames Cached list of names of tracked sender domains
     */
    protected array $trackedSenderDomainNames = array();

    /**
     * List of domains that are prohibitted from using as sender domains
     *
     * @var array
     */
    protected const BLACKLISTED_DOMAINS = [
        '10minmail.de',
        '10minutenmail.xyz',
        'bluewin.ch',
        'bossmail.de',
        'candymail.de',
        'cloudrexx.at',
        'cloudrexx.ch',
        'cloudrexx.com',
        'cloudrexx.de',
        'contrexx.com',
        'doublemail.de',
        'dsgvo.party',
        'example.com',
        'example.org',
        'existiert.net',
        'fake-box.com',
        'fliegender.fish',
        'fn.de',
        'freenet.de',
        'freenetmail.de',
        'fukaru.com',
        'funnymail.de',
        'gmail.com',
        'gmx.at',
        'gmx.biz',
        'gmx.ch',
        'gmx.com',
        'gmx.de',
        'gmx.eu',
        'gmx.info',
        'gmx.net',
        'gmx.org',
        'gmx.tm',
        'hispeed.ch',
        'hotmail.com',
        'jaga.email',
        'ji5.de',
        'ji6.de',
        'ji7.de',
        'justmail.de',
        'knickerbockerban.de',
        'lambsauce.de',
        'live.com',
        'magenta.de',
        'magspam.net',
        'mail.de',
        'mdz.email',
        'muell.icu',
        'muell.io',
        'muell.monster',
        'muell.xyz',
        'muellemail.com',
        'muellmail.com',
        'oida.icu',
        'opentrash.com',
        'outlook.com',
        'papierkorb.me',
        'puppetmail.de',
        'ramenmail.de',
        're-gister.com',
        'spam.care',
        'spoofmail.de',
        'spoofmail.es',
        'spoofmail.fr',
        'sudern.de',
        't-online.de',
        'tonne.to',
        'trash-mail.com',
        'trash-me.com',
        'ultra.fyi',
        'web.de',
        'wegwerfemail.de',
        'yahoo.com',
        'you-spam.com',
    ];

    /**
     * @inheritDoc
     */
    public function getCommandsForCommandMode() {
        return array(
            $this->getName() => new \Cx\Core_Modules\Access\Model\Entity\Permission(
                array(),
                array('cli'),
                false
            ),
        );
    }

    /**
     * @inheritDoc
     */
    public function getCommandDescription($command, $short = false) {
        if ($command != $this->getName()) {
            return '';
        }
        if ($short) {
            return 'Allows access to the sender domain validation';
        }
        return $this->getName() . " revalidate
\tRevalidates all tracked sender domains";
    }

    /**
     * @inheritDoc
     */
    public function executeCommand($command, $arguments, $dataArguments = array()) {
        switch ($command) {
            case $this->getName():
                if (count($arguments) < 1) {
                    throw new \Exception('Not enough arguments');
                }
                $operation = array_shift($arguments);
                switch ($operation) {
                    case 'revalidate':
                        $this->getTrackedSenderDomains([], true);
                        echo 'Done ' . PHP_EOL;
                        break;
                }
                break;
        }
    }

    /**
     * Get the Controller classes
     *
     * @return array name of the controller classes
     */
    public function getControllerClasses() {
        // Return an empty array here to let the component handler know that there
        // does not exist a backend, nor a frontend controller of this component.
        return array('Backend');
    }

    /**
     * @inheritDoc
     */
    public function getControllersAccessableByJson() {
        return array(
            'ComponentController'
        );
    }

    /**
     * Returns the internal name used as identifier for this adapter
     * @return String Name of this adapter
     */
    public function getName() {
        return parent::getName();
    }

    /**
     * @inheritDoc
     */
    public function getAccessableMethods() {
        return array(
            'refreshDnsRecords',
            'addTrackedSenderDomain',
            'removeTrackedSenderDomain',
        );
    }

    /**
     * @inheritDoc
     */
    public function getDefaultPermissions() {
        return new \Cx\Core_Modules\Access\Model\Entity\Permission(
            ['https', 'http'],
            ['get'],
            true,
            [],
            [196]
        );
    }

    /**
     * @inheritDoc
     */
    public function getMessagesAsString() {
        return '';
    }

    /**
     * @inheritDoc
     */
    public function registerEvents() {
        $this->cx->getEvents()->addEvent('NetManager:getSenderDomains');
    }

    /**
     * @inheritDoc
     */
    public function registerEventListeners() {
        $this->cx->getEvents()->addEventListener(
            'Cache.Cachefile:outdateIndex',
            new \Cx\Core\NetManager\Model\Event\CacheTtlUpdateEventListener($this->cx)
        );
    }

    /**
     * Loads the list of tracked sender domains
     *
     * Regarding caching options:
     * Set $domainsToRefreshDnsOf to a list of domains you want to update DNS
     * data of. Keep in mind, that this may take some time as uncached DNS is
     * quite slow.
     * $forceReload should not be necessary and is only there for debugging
     * purposes. Therefore this is deprecated.
     * @todo This method does not work correctly if Cache component is not available
     * @param array $domainsToRefreshDnsOf (optional) List of domain names to ignore DNS cache for
     * @param bool $forceReload (deprecated, optional) If set to true, list of domains is not loaded from cache
     * @return \Cx\Core_Modules\Listing\Model\Entity\DataSet List of tracked sender domains
     */
    public function getTrackedSenderDomains(
        array $domainsToRefreshDnsOf = array(),
        bool $forceReload = false
    ): \Cx\Core_Modules\Listing\Model\Entity\DataSet {
        $cache = $this->getComponent('Cache');
        $fromCache = false;
        $result = array();
        if (!count($this->trackedSenderDomainNames)) {
            $this->loadTrackedSenderDomainNames();
            $usedDomainsWithoutUsages = $this->cx->getEvents()->triggerEvent(
                'NetManager:getSenderDomains',
                array(array('usages' => false))
            );
            // drop first level of array (component name)
            $usedDomainsWithoutUsages = call_user_func_array('array_merge', array_values($usedDomainsWithoutUsages));
            $this->trackedSenderDomainNames += array_keys($usedDomainsWithoutUsages);
        }
        $domainsToRefresh = array();
        if (!$forceReload && $cache) {
            try {
                $response = $cache->getCachefileData(
                    $this,
                    'TrackedSenderDomains'
                );
                // Ensure domain list is up to date
                foreach ($this->trackedSenderDomainNames as $domainName) {
                    if ($response->entryExists($domainName)) {
                        continue;
                    }
                    $domainsToRefresh[] = $domainName;
                }
                // Now do the opposite. This should not happen, but if it does
                // it leads to inconsistant data. Synchronizing this makes
                // related caching problems obvious.
                foreach ($response as $domainName=>$domainInfo) {
                    if (in_array($domainName, $this->trackedSenderDomainNames)) {
                        continue;
                    }
                    \DBG::log('Tracked sender domain "' . $domainName . '" is in cache but not in settings!');
                    $this->trackedSenderDomainNames[] = $domainName;
                    $domainsToRefresh[] = $domainName;
                }
                // If DNS of an untracked domain should be refreshed, add it
                foreach ($domainsToRefreshDnsOf as $domainName) {
                    if (in_array($domainName, $this->trackedSenderDomainNames)) {
                        continue;
                    }
                    $this->trackedSenderDomainNames[] = $domainName;
                    $domainsToRefresh[] = $domainName;
                }
                if (!count($domainsToRefresh) && !count($domainsToRefreshDnsOf)) {
                    return $response;
                } else {
                    $result = $response->toArray(false);
                    $fromCache = true;
                }
            } catch (\Exception $e) {}
        }
        $domainsToUpdateCacheOf = array();
        if (!count($result) || count($domainsToRefresh)) {
            $usedDomainsWithUsages = $this->cx->getEvents()->triggerEvent(
                'NetManager:getSenderDomains',
                array(array('usages' => true))
            );
            foreach ($domainsToRefresh as $domainName) {
                $result[$domainName]['usages'] = [];
            }
            // drop first level of array (component name)
            // Note that we cannot use the same shorthand as this would overwrite
            // usages.
            foreach ($usedDomainsWithUsages as $componentName=>$usedDomains) {
                foreach ($usedDomains as $usedDomainName=>$usedDomainInfo) {
                    if (!isset($result[$usedDomainName])) {
                        $result[$usedDomainName] = $usedDomainInfo;
                        $result[$usedDomainName]['usages'] = array();
                        $result[$usedDomainName]['usages'][$componentName] = $usedDomainInfo['usages'];
                        continue;
                    }
                    $result[$usedDomainName]['strict'] = $result[$usedDomainName]['strict'] || $usedDomainInfo['strict'];
                    $result[$usedDomainName]['usages'][$componentName] = $usedDomainInfo['usages'];
                }
            }

            // Add tracked domains to used domains
            foreach ($this->trackedSenderDomainNames as $domainName) {
                if (isset($result[$domainName])) {
                    continue;
                }
                $domainsToUpdateCacheOf[] = $domainName;
                $result[$domainName] = array(
                    'strict' => false,
                    'usages' => array()
                );
            }
            // Update list of tracked domains
            foreach ($result as $domainName=>$info) {
                if (in_array($domainName, $this->trackedSenderDomainNames)) {
                    continue;
                }
                $domainsToUpdateCacheOf[] = $domainName;
                $this->trackedSenderDomainNames[] = $domainName;
            }
            \Cx\Core\Setting\Controller\Setting::init($this->getName());
            \Cx\Core\Setting\Controller\Setting::set(
                'trackedSenderDomains',
                implode(',', $this->trackedSenderDomainNames)
            );
            \Cx\Core\Setting\Controller\Setting::update('trackedSenderDomains');

            // cleanup data
            foreach ($result as $domainName=>&$info) {
                $info['name'] = $domainName;
                if (count($info['usages'])) {
                    $info['virtual'] = true;
                }
            }
        }
        // load/update DNS if necessary
        foreach ($result as $domainName=>&$domain) {
            // if list is loaded from cache, this only needs to be done if
            // either the DNS info should be refreshed or this domain was
            // not cached before.
            if (
                $fromCache &&
                !in_array($domainName, $domainsToRefreshDnsOf) &&
                !empty($domain['spf'])
            ) {
                continue;
            }
            $domain = array_merge(
                $domain,
                $this->loadMailComplicanceDetails(
                    $domainName,
                    in_array($domainName, $domainsToRefreshDnsOf)
                )
            );
            $domainsToUpdateCacheOf[] = $domainName;
        }
        // update cache dates
        $cacheResult = array();
        $domainsToUpdateCacheOf = array_unique($domainsToUpdateCacheOf);
        if (count($domainsToUpdateCacheOf)) {
            foreach ($domainsToUpdateCacheOf as $domainName) {
                $this->addCacheMetaDataToEntry($result[$domainName]);
                $cacheResult[$domainName] = $result[$domainName];
            }
        }
        // update cache
        if ($cache && count($cacheResult)) {
            $cache->addCachefileRecords(
                $this,
                'TrackedSenderDomains',
                array('name'),
                $cacheResult
            );
        }
        // Create dataset
        $dataSet = new \Cx\Core_Modules\Listing\Model\Entity\DataSet($result);
        $dataSet->setIdentifier('TrackedSenderDomain');
        return $dataSet;
    }

    /**
     * Load tracked sender domain names
     *
     * This loads the list of domains being used as sender domains from the
     * setting trackedSenderDomains and puts it into
     * {@see $this->trackedSenderDomainNames}.
     */
    protected function loadTrackedSenderDomainNames(): void {
        if (count($this->trackedSenderDomainNames)) {
            return;
        }
        \Cx\Core\Setting\Controller\Setting::init($this->getName());
        $this->trackedSenderDomainNames = explode(
            ',',
            \Cx\Core\Setting\Controller\Setting::getValue(
                'trackedSenderDomains',
                $this->getName()
            )
        );
        // explode converts NULL to array('')
        if (count($this->trackedSenderDomainNames) == 1 && current($this->trackedSenderDomainNames) == '') {
            $this->trackedSenderDomainNames = array();
        }
    }

    /**
     * Adds 'validUntil' and 'cachedAt' indexes to $entry which are then used
     * by {@see \Cx\Core_Modules\Cache\Controller\ComponentController::getCachefileData()}
     * to determine its expiration.
     *
     * @param array $entry Entry to add cache meta data to.
     */
    public function addCacheMetaDataToEntry(array &$entry): void {
        $entry['validUntil'] = new \DateTime(
            '+' . $entry['shortestTtl'] . ' seconds'
        );
        $entry['cachedAt'] = new \DateTime();
    }

    /**
     * Gets the compliance info for a given domain
     *
     * @param string $domainName Name of domain to get info for
     * @return array List of domain info
     */
    public function getDomainMailComplianceDetails(string $domainName): array {
        $domainName = \Cx\Core\Net\Controller\ComponentController::convertIdnToAsciiFormat(
            $domainName
        );
        $cache = $this->getComponent('Cache');
        if ($cache) {
            try {
                $response = $cache->getCachefileRecord(
                    $this,
                    'TrackedSenderDomains',
                    array(
                        'name' => $domainName,
                    )
                );
                return $response;
            } catch (\Exception $e) {}
        }
        // do not load via getTrackedSenderDomains() as this would at it to the
        // list of tracked domains and potentially takes too long.
        $details = $this->loadMailComplicanceDetails(
            $domainName
        );
        $this->loadTrackedSenderDomainNames();
        if (
            $cache
            && in_array($domainName, $this->trackedSenderDomainNames)
        ) {
            $details['name'] = $domainName;
            $this->addCacheMetaDataToEntry($details);
            $cache->addCachefileRecords(
                $this,
                'TrackedSenderDomains',
                array('name'),
                [$domainName => $details]
            );
        }
        return $details;
    }

    /**
     * Loads the mail compliance details for a domain
     *
     * Note: While getDomainMailComplianceDetails() loads all info of a tracked
     * sender domain this only returns the uncached mail compliance details.
     *
     * @param string $domainName Name of the domain to get info for
     * @param bool $skipCache (optional) If set to true DNS cache is not used
     */
    public function loadMailComplicanceDetails(
        string $domainName,
        bool $skipCache = false
    ): array {
        $url = \Cx\Core\Html\Controller\ViewGenerator::getVgEditUrl(
            0,
            $domainName,
            \Cx\Core\Routing\Url::fromBackend(
                $this->getName(),
                'TrackedSenderDomain'
            )
        );
        $data = array(
            'editUrl' => (string) $url,
            'shortestTtl' => PHP_INT_MAX,
        );
        if ($this->isBlacklistedDomain($domainName)) {
            $nonComplient = array(
                'hasRecord' => false,
                'records' => [],
                'validUntil' => PHP_INT_MAX,
                'isValid' => false,
            );
            $data['spf'] = $nonComplient;
            $data['dkim'] = $nonComplient;
            $data['dmarc'] = $nonComplient;
            $data['compliant'] = false;
            return $data;
        }
        $allCompliant = true;
        $features = array(
            'spf' => array(''),
            'dmarc' => array('_dmarc.'),
            'dkim' => array(
                'test.cloudrexx1._domainkey.',
                'test.cloudrexx2._domainkey.',
                'test.cloudrexx3._domainkey.',
            ),
        );
        foreach ($features as $feature=>$checkDomains) {
            $records = array();
            $recordTtl = PHP_INT_MAX;
            foreach ($checkDomains as $checkDomainPrefix) {
                try {
                    $recordResult = $this->getComponent('Net')->getDnsResponse(
                        $checkDomainPrefix . $domainName,
                        $feature == 'dkim' ? 'CNAME' : 'TXT',
                        true,
                        false,
                        $skipCache
                    );
                    foreach ($recordResult as $result) {
                        if (
                            $feature != 'dkim' &&
                            !preg_match('/' . $feature . '/i', $result['value'])
                        ) {
                            continue;
                        }
                        $records[] = $result['value'];
                        if ($result['ttl'] < $recordTtl) {
                            $recordTtl = $result['ttl'];
                        }
                    }
                } catch (\Exception $e) {}
            }
            $data[$feature] = array(
                'hasRecord' => !empty($records),
                'records' => $records,
                'validUntil' => $recordTtl,
                'isValid' => false,
            );
            if ($recordTtl < $data['shortestTtl']) {
                $data['shortestTtl'] = $recordTtl;
            }
            $valid = $data[$feature]['hasRecord'];
            switch ($feature) {
                case 'spf':
                    if ($valid) {
                        $valid = $this->isSpfValid($domainName, current($data[$feature]['records']));
                    }
                    break;
                case 'dmarc':
                    if ($valid) {
                        $valid = $this->isDmarcValid(current($data[$feature]['records']));
                    }
                    break;
                case 'dkim':
                    $valid = $this->isDkimValid(
                        $data[$feature]['records']
                    );
                    break;
            }
            $data[$feature]['isValid'] = $valid;
            $allCompliant = $allCompliant && $valid;
        }
        $data['compliant'] = $allCompliant;
        return $data;
    }

    /**
     * Refreshes the DNS and mail compliance info for the given method
     *
     * @param array Get param "domain" needs to be set
     */
    public function refreshDnsRecords(array $params): void {
        if (!isset($params['get']['domain'])) {
            throw new \Exception('Required param "domain" not set');
        }
        $domainName = $params['get']['domain'];
        $domainName = \Cx\Core\Net\Controller\ComponentController::convertIdnToAsciiFormat(
            $domainName
        );
        // Log current state ahead of refresh until the sender validation issue
        // has been identified. See CLX-5697
        \DBG::log('Debug sender validation ahead of refresh:');
        \DBG::dump($domainName);
        \DBG::dump(file_get_contents(
            $this->cx->getWebsiteCachePath() . '/Net/DNS.yml'
        ));
        \DBG::dump(file_get_contents(
            $this->cx->getWebsiteCachePath() . '/NetManager/TrackedSenderDomains.yml'
        ));
        $domains = $this->getTrackedSenderDomains(array($domainName));
    }

    /**
     * JSON Adapter to remove a domain from the list of tracked sender domains
     *
     * @param array $params GET param "domain" needs to be set to a tracked domain
     * @throws \Exception If GET param "domain" is not set to a tracked domain
     */
    public function removeTrackedSenderDomain(array $params): void {
        if (!isset($params['get']['domain'])) {
            throw new \Exception('Required param "domain" not set');
        }
        $this->addOrRemoveTrackedSenderDomain($params['get']['domain'], false);
    }

    /**
     * JSON Adapter to add a domain to the list of tracked sender domains
     *
     * @param array $params GET param "domain" needs to be set to an untracked domain
     * @throws \Exception If GET param "domain" is not set to an untracked domain
     * @return array Array with the key "url" set to the detail page URL for the new domain
     */
    public function addTrackedSenderDomain(array $params): array {
        if (!isset($params['get']['domain'])) {
            throw new \Exception('Required param "domain" not set');
        }
        $this->addOrRemoveTrackedSenderDomain($params['get']['domain'], true);
        return array(
            'url' => (string) \Cx\Core\Html\Controller\ViewGenerator::getVgEditUrl(
                0,
                \Cx\Core\Net\Controller\ComponentController::convertIdnToAsciiFormat(
                    $params['get']['domain']
                ),
                \Cx\Core\Routing\Url::fromBackend(
                    $this->getName(),
                    'TrackedSenderDomain'
                )
            ),
        );
    }

    /**
     * Adds or removes a domain from the list of tracked sender domains
     *
     * @param string $domainName Name of the domain to add/remove
     * @param bool $add Set to true to add, false to remove
     * @throws \Exception If operation cannot be executed successfully
     */
    protected function addOrRemoveTrackedSenderDomain(string $domainName, bool $add): void {
        $domainName = \Cx\Core\Net\Controller\ComponentController::convertIdnToAsciiFormat(
            $domainName
        );
        if (!filter_var($domainName, FILTER_VALIDATE_DOMAIN, FILTER_FLAG_HOSTNAME)) {
            throw new \Exception('Invalid domain name');
        }
        $senderDomains = $this->getTrackedSenderDomains();
        if ($add) {
            if (in_array($domainName, $this->trackedSenderDomainNames)) {
                throw new \Exception('Domain is already registered');
            }
            $this->getTrackedSenderDomains(array($domainName));
        } else {
            if (!$senderDomains->entryExists($domainName)) {
                throw new \Exception('No such domain in tracked sender domain list');
            }
            $domainInfo = $senderDomains->getEntry($domainName);
            if (count($domainInfo['usages'])) {
                // TODO: Maybe reload usages to ensure we do not have a caching problem
                throw new \Exception('Domain cannot be removed as it is in use');
            }
            // We need to drop the domain from $this->trackedSenderDomainNames
            // in case $this->getTrackedSenderDomains() is called in the same
            // request. We also need to drop it from settings and cache.
            unset($this->trackedSenderDomainNames[array_search($domainName, $this->trackedSenderDomainNames)]);
            \Cx\Core\Setting\Controller\Setting::init($this->getName());
            \Cx\Core\Setting\Controller\Setting::set(
                'trackedSenderDomains',
                implode(',', $this->trackedSenderDomainNames)
            );
            \Cx\Core\Setting\Controller\Setting::update('trackedSenderDomains');
            $cache = $this->getComponent('Cache');
            if ($cache) {
                try {
                    $cache->dropCachefileRecord(
                        $this,
                        'TrackedSenderDomains',
                        array('name' => $domainName)
                    );
                } catch (\Exception $e) {}
            }
        }
    }

    /**
     * @todo Validate actual record $spf instead of currently published DNS record
     */
    protected function isSpfValid(string $domain, string $spf): bool {
        $hostIp = $this->getComponent('Net')->getPublicIpAddress();
        if (empty($hostIp)) {
            trigger_error(
                sprintf(
                    'SPF sender validation on domain %s not possible as no public IP-address is set!',
                    $domain
                ),
                E_USER_WARNING
            );
            \DBG::debug('Set public IP using: cx Setting set Net publicIpAddress <ipv4>');
            return false;
        }
        $command = sprintf(
            'spfquery -ip %1$s -sender %2$s',
            escapeshellarg($hostIp),
            escapeshellarg($domain)
        );
        #$output = [];
        #$code = 0;
        if (exec($command, $output, $code) === false) {
            \DBG::debug('Command execution failed: ' . $command);
            return false;
        }
        if (
            $code === 2
            && $output[0] === 'pass'
        ) {
            return true;
        }
        \DBG::dump($output, DBG_DEBUG);
        return false;
    }

    /**
     * Validates a DKIM DNS record against a DKIM private key.
     *
     * @param string $dnsRecord The DKIM DNS record.
     * @param string $privateKey The private DKIM key.
     * @return bool True if the DNS record matches the private key, false otherwise.
     */
    protected function isDkimValid(array $dnsRecords): bool {
        if (count($dnsRecords) != 3) {
            return false;
        }
        asort($dnsRecords);
        for ($i = 1; $i <= 3; $i++) {
            if ($dnsRecords[$i - 1] != 'cloudrexx' . $i . '.dkim.cloudrexx.com') {
                return false;
            }
        }
        return true;
    }

    protected function isDmarcValid(string $dmarc): bool {
        if (strpos($dmarc, 'v=DMARC1;') !== 0) {
            return false;
        }
        $tags = explode(';', $dmarc);
        foreach ($tags as $tag) {
            if (strpos($tag, '=') === false) {
                continue;
            }
            list($key, $value) = explode('=', $tag, 2);
            if ($key === 'p' && !in_array($value, ['none', 'quarantine', 'reject'], true)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Check if a domain is on the blacklist
     *
     * A domain that is on the blacklist must not be used as sender domain
     *
     * @param string $domain Domain to check against blacklist
     * @return bool Whether the domain is blacklisted or not
     */
    public function isBlacklistedDomain(string $domain): bool {
        // note: as this method gets called during the send process of an email,
        // we have to ensure that we're not breaking any deprecated calls to
        // Setting::getValue() which are missing the component as second
        // argument. Therefore we have to call Setting::init with
        // $setAsCurrentEngineType set to `false`
        \Cx\Core\Setting\Controller\Setting::init(
            $this->getName(),
            null,
            'Database',
            null,
            \Cx\Core\Setting\Controller\Setting::NOT_POPULATE,
            false
        );
        if (
            \Cx\Core\Setting\Controller\Setting::getValue(
                'ignoreDomainBlacklist',
                $this->getName()
            ) === 'on'
        ) {
            return false;
        }
        $blacklistedDomainList = join(
            '|',
            array_map('preg_quote', static::BLACKLISTED_DOMAINS)
        );
        return preg_match(
            '/(?:^|.*\.)(' . $blacklistedDomainList . ')$/i',
            $domain
        );
    }
}
