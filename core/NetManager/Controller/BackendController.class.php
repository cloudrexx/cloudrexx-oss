<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * Specific BackendController for this Component. Use this to easily create a backend view
 *
 * @copyright   Cloudrexx AG
 * @author      Project Team SS4U <info@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  core_netmanager
 */

namespace Cx\Core\NetManager\Controller;

/**
 * Specific BackendController for this Component. Use this to easily create a backend view
 *
 * @copyright   Cloudrexx AG
 * @author      Project Team SS4U <info@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  core_netmanager
 */
class BackendController extends \Cx\Core\Core\Model\Entity\SystemComponentBackendController {

    /**
     * @var string
     */
    const PROPOSED_SPF_RECORD = 'v=spf1 include:spf.cloudrexx.com';

    /**
     * @var string
     */
    const PROPOSED_SPF_RECORD_MECHANISM = 'include:spf.cloudrexx.com';

    /**
     * @var string
     */
    const PROPOSED_DMARC_RECORD = 'v=DMARC1; p=none;';

    protected const INTRO_LINKS = array(
        'spf' => 'https://cloudrexx.atlassian.net/wiki/spaces/HELP/pages/468811905',
        'dkim' => 'https://cloudrexx.atlassian.net/wiki/spaces/HELP/pages/696188930',
        'dmarc' => 'https://cloudrexx.atlassian.net/wiki/spaces/HELP/pages/468779777',
    );
    protected const INTRO_LINK_TITLES = array(
        'spf' => 'Wie erstelle ich einen SPF-Eintrag?',
        'dkim' => 'Wie richte ich DKIM ein?',
        'dmarc' => 'Wie richte ich DMARC ein?',
    );

    /**
     * @var \Cx\Core\Html\Sigma Template to parse current view into
     */
    protected \Cx\Core\Html\Sigma $template;

    /**
     * Returns a list of available commands (?act=XY)
     * @return array List of acts
     */
    public function getCommands() {
        $this->defaultPermission = new \Cx\Core_Modules\Access\Model\Entity\Permission(
            ['https', 'http'],
            ['get', 'post'],
            true,
            [],
            [196]
        );
        return array('TrackedSenderDomain');
    }

    /**
     * Use this to parse your backend page
     *
     * You will get the template located in /View/Template/{CMD}.html
     * You can access Cx class using $this->cx
     * To show messages, use \Message class
     * @param \Cx\Core\Html\Sigma $template Template for current CMD
     * @param array $cmd CMD separated by slashes
     * @param boolean $isSingle Wether edit view or not
     */
    public function parsePage(\Cx\Core\Html\Sigma $template, array $cmd, &$isSingle = false) {
        $entityClassName = '';
        $this->template = $template;
        switch (current($cmd)) {
            case '':
                $entityClassName = 'Cx\Core\Net\Model\Entity\Domain';
                break;
            case 'TrackedSenderDomain':
                global $_ARRAYLANG;
                \Message::information(
                    $_ARRAYLANG['TXT_CORE_NETMANAGER_SENDER_AUTHENTICATION_SETTINGS_NOTE']
                );
                $cxjs = \ContrexxJavascript::getInstance();
                $cxjs->setVariable(
                    array(
                        'TXT_CORE_RECORD_DELETE_CONFIRM' => $_ARRAYLANG['TXT_CORE_RECORD_DELETE_CONFIRM'],
                        'TXT_ADD' => $_ARRAYLANG['TXT_ADD'],
                        'TXT_CANCEL' => $_ARRAYLANG['TXT_CANCEL'],
                        'TXT_CORE_NETMANAGER_DOMAIN_ADD_NAME' => $_ARRAYLANG['TXT_CORE_NETMANAGER_DOMAIN_ADD_NAME'],
                    ),
                    $this->getName() . '/lang'
                );
                $entityClassName = 'TrackedSenderDomain';
                break;
        }
        $this->parseEntityClassPage($this->template, $entityClassName, current($cmd), array(), $isSingle);
    }

    protected function getViewGeneratorParseObjectForEntityClass($entityClassName) {
        if ($entityClassName != 'TrackedSenderDomain') {
            return parent::getViewGeneratorParseObjectForEntityClass($entityClassName);
        }
        $trackedSenderDomains = $this->getTrackedSenderDomains([], true);
        if ($this->cx->getRequest()->hasParam('editid')) {
            $requestedDomain = \Cx\Core\Html\Controller\ViewGenerator::getParam(
                0,
                $this->cx->getRequest()->getParam('editid')
            );
            $requestedDomain = \Cx\Core\Net\Controller\ComponentController::convertIdnToAsciiFormat(
                $requestedDomain
            );
            if ($this->isBlacklistedDomain($requestedDomain)) {
                $this->cx->getRequest()->getUrl()->setParam('editid', null);
                // note: as ViewGenerator does not use $this->cx->getRequest()->getUrl()
                // we also have to unset the legacy URL modifiers
                unset($_GET['editid']);
                unset($_POST['editid']);
                unset($_REQUEST['editid']);
            }
            if (!$trackedSenderDomains->entryExists($requestedDomain)) {
                $trackedSenderDomains = $this->getTrackedSenderDomains(array($requestedDomain));
            }
        }
        return $trackedSenderDomains;
    }

    /**
     * Returns all entities of this component which can have an auto-generated view
     *
     * @access protected
     * @return array
     */
    protected function getEntityClassesWithView() {
        // at the moment the view is only used for domain overview
        return array(
            'Cx\Core\Net\Model\Entity\Domain',
            'TrackedSenderDomain',
        );
    }

    /**
     * This function returns the ViewGeneration options for a given entityClass
     *
     * @access protected
     * @global $_ARRAYLANG
     * @param $entityClassName contains the FQCN from entity
     * @param $dataSetIdentifier if $entityClassName is DataSet, this is used for better partition
     * @return array with options
     */
    protected function getViewGeneratorOptions($entityClassName, $dataSetIdentifier = '') {
        global $_ARRAYLANG;

        $classNameParts = explode('\\', $entityClassName);
        $classIdentifier = end($classNameParts);

        $langVarName = 'TXT_' . strtoupper($this->getType() . '_' . $this->getName() . '_ACT_' . $classIdentifier);
        $header = '';
        if (isset($_ARRAYLANG[$langVarName])) {
            $header = $_ARRAYLANG[$langVarName];
        }
        switch ($entityClassName) {
            case 'Cx\Core\Net\Model\Entity\Domain':
                return array(
                    'header'    => '',
                    'entityName'    => $_ARRAYLANG['TXT_CORE_NETMANAGER_OVERVIEW_ENTITY'],
                    'fields' => array(
                        'name'  => array(
                            'header' => $_ARRAYLANG['TXT_NAME'],
                            'validValues' => $entityClassName::NAME_VALIDATION_REGEXP,
                            'table' => array(
                                'parse' => function($value) {
                                    static $mainDomainName;
                                    if (empty($mainDomainName)) {
                                        $domainRepository = new \Cx\Core\Net\Model\Repository\DomainRepository();
                                        $mainDomainName = $domainRepository->getMainDomain()->getName();
                                    }
                                    $domainName = contrexx_raw2xhtml(\Cx\Core\Net\Controller\ComponentController::convertIdnToUtf8Format($value));
                                    if($domainName!=contrexx_raw2xhtml($value)) {
                                        $domainName.= ' (' .  contrexx_raw2xhtml($value) . ')';
                                    }
                                    $mainDomainIcon = '';
                                    if ($value == $mainDomainName) {
                                        $langData = \Env::get('init')->getComponentSpecificLanguageData('Config', false);
                                        $mainDomainIcon = '<div class="oklabel">' . $langData['TXT_CORE_CONFIG_MAINDOMAINID'] . '</div>';
                                    }
                                    return $domainName.$mainDomainIcon;
                                },
                            ),
                            'formfield' => function($fieldname, $fieldtype, $fieldlength, $fieldvalue, $fieldoptions) {
                                return \Cx\Core\Net\Controller\ComponentController::convertIdnToUtf8Format($fieldvalue);
                            },
                        ),
                        'id'    => array(
                            'showOverview' => false,
                        ),
                    ),
                    'functions' => array(
                        'add'           => true,
                        'edit'          => false,
                        'allowEdit'    => true,
                        'delete'        => false,
                        'allowDelete'   => true,
                        'sorting'       => false,
                        'paging'        => false,
                        'filtering'     => false,
                        'actions'       => function($rowData, $rowId) {
                            global $_CORELANG;
                            static $mainDomainName;
                            if (empty($mainDomainName)) {
                                $domainRepository = new \Cx\Core\Net\Model\Repository\DomainRepository();
                                $mainDomainName = $domainRepository->getMainDomain()->getName();
                            }

                            preg_match_all('/\d+/', $rowId, $ids, 0, 0);

                            // hostname's ID is 0
                            if (!$ids[0][1]) {
                                return '';
                            }

                            $actionIcons = '';
                            $csrfParams = \Cx\Core\Csrf\Controller\Csrf::param();
                            if ($mainDomainName !== $rowData['name']) {
                                $actionIcons = '<a '
                                    . 'href="' . \Env::get('cx')->getWebsiteBackendPath() . '/?cmd=NetManager&amp;editid=' . $rowId . '"'
                                    . 'class="edit" title="Edit entry">'
                                    . '</a>';
                                $actionIcons .= '<a
                                    onclick=" if(confirm(\''.$_CORELANG['TXT_CORE_RECORD_DELETE_CONFIRM'].'\'))'
                                    . 'window.location.replace(\'' . \Env::get('cx')->getWebsiteBackendPath()
                                    . '/?cmd=NetManager&amp;deleteid=' . (empty($ids[0][1])?0:$ids[0][1])
                                    . '&amp;vg_increment_number=' . (empty($ids[0][0])?0:$ids[0][0])
                                    . '&amp;' . $csrfParams . '\');" href="javascript:void(0);"'
                                    . 'class="delete"'
                                    . 'title="Delete entry">
                                    </a>';
                            }
                            return $actionIcons;
                        }
                    )
                );
            case '':
                return array(
                    'header' => '',
                    'entityName' => $_ARRAYLANG['TXT_CORE_NETMANAGER_VERIFICATION_ENTITY'],
                    'fields' => array(
                        'status' => array(
                            'custom' => true,
                            'table' => array(
                                'parse' => function($value, $rowData) {
                                    global $_ARRAYLANG;

                                    $statusLabels = new \Cx\Core\Html\Model\Entity\HtmlElement('div');
                                    $statusLabels->addClass('status-labels');
                                    if ($this->isBlacklistedDomain($rowData['name'])) {
                                        $label = new \Cx\Core\Html\Model\Entity\HtmlElement('div');
                                        $label->addClass('label');
                                        $label->addClass('tooltip-trigger');
                                        $label->addClass('alertlabel');
                                        $label->addChild(
                                            new \Cx\Core\Html\Model\Entity\TextElement(
                                                $_ARRAYLANG['TXT_CORE_NETMANAGER_BLACKLISTED']
                                            )
                                        );
                                        $statusLabels->addChild($label);
                                        $tooltip = new \Cx\Core\Html\Model\Entity\HtmlElement('span');
                                        $tooltip->addClass('tooltip-message');
                                        $tooltip->addChild(
                                            new \Cx\Core\Html\Model\Entity\TextElement(
                                                $_ARRAYLANG['TXT_CORE_NETMANAGER_BLACKLISTED_TOOLTIP']
                                            )
                                        );
                                        $statusLabels->addChild($tooltip);
                                        return (string) $statusLabels;
                                    }
                                    foreach (array('spf', 'dkim', 'dmarc') as $feature) {
                                        $label = $this->showDnsState($rowData['name'], $feature, strtoupper($feature));
                                        $label->addClass('tooltip-trigger');
                                        $statusLabels->addChild($label);
                                        $tooltip = new \Cx\Core\Html\Model\Entity\HtmlElement('span');
                                        $tooltip->addClass('tooltip-message');
                                        $tooltip->addChild(
                                            new \Cx\Core\Html\Model\Entity\TextElement(
                                                $_ARRAYLANG['TXT_CORE_NETMANAGER_RECORD'] . ': ' . (
                                                    !empty($rowData[$feature]['records']) ?
                                                    '<code>' . implode('</code>, <code>', $rowData[$feature]['records']) . '</code>' :
                                                    $_ARRAYLANG['TXT_CORE_NETMANAGER_NO_RESULT']
                                                )
                                            )
                                        );
                                        $statusLabels->addChild($tooltip);
                                    }
                                    return (string) $statusLabels;
                                },
                            ),
                            'showDetail' => false,
                        ),
                        'spfTitle' => array(
                            'custom' => true,
                            'showOverview' => false,
                            'formfield' => function($fieldname, $fieldtype, $fieldlength, $fieldvalue, $fieldoptions, $entityId) {
                                return new \Cx\Core\Html\Model\Entity\TextElement('');
                            },
                        ),
                        'spfState' => array(
                            'custom' => true,
                            'header' => 'TXT_CORE_NETMANAGER_RECORD',
                            'showOverview' => false,
                            'tooltip' => function($fieldname, $entityData) {
                                global $_ARRAYLANG;

                                return sprintf(
                                    $_ARRAYLANG['TXT_CORE_NETMANAGER_RECORD_LAST_UPDATE'],
                                    $entityData['cachedAt']->format(ASCMS_DATE_FORMAT_DATETIME)
                                );
                            },
                            'formfield' => function($fieldname, $fieldtype, $fieldlength, $fieldvalue, $fieldoptions, $entityId) {
                                $div = new \Cx\Core\Html\Model\Entity\HtmlElement('div');
                                $div->addChild($this->showDnsState($entityId, 'spf'));
                                $div->addChild($this->showDnsRecord($entityId, 'spf'));
                                return $div;
                            },
                        ),
                        'spfProposal' => array(
                            'custom' => true,
                            'header' => 'TXT_CORE_NETMANAGER_RECORD_PROPOSAL',
                            'showOverview' => false,
                            'formfield' => function($fieldname, $fieldtype, $fieldlength, $fieldvalue, $fieldoptions, $entityId) {
                                global $_ARRAYLANG;

                                $domain = $this->getTrackedSenderDomains()->getEntry($entityId);
                                if (!empty($domain['spfRecord'])) {
                                    $div = new \Cx\Core\Html\Model\Entity\HtmlElement('div');
                                    $span = new \Cx\Core\Html\Model\Entity\HtmlElement('div');
                                    $span->addChild(
                                        new \Cx\Core\Html\Model\Entity\TextElement(
                                            sprintf(
                                                $_ARRAYLANG['TXT_CORE_NETMANAGER_RECORD_PROPOSAL_EXTEND'],
                                                $domain->getName(),
                                                static::INTRO_LINKS['spf'],
                                                static::INTRO_LINK_TITLES['spf']
                                            )
                                        )
                                    );
                                    $div->addChild($span);
                                    $code = new \Cx\Core\Html\Model\Entity\HtmlElement('code');
                                    $code->addChild(
                                        new \Cx\Core\Html\Model\Entity\TextElement(
                                            static::PROPOSED_SPF_RECORD_MECHANISM
                                        )
                                    );
                                    $div->addChild($code);
                                    return $div;
                                }
                                return $this->showDnsProposal($entityId, 'spf');
                            },
                        ),
                        'dkimTitle' => array(
                            'custom' => true,
                            'showOverview' => false,
                            'formfield' => function($fieldname, $fieldtype, $fieldlength, $fieldvalue, $fieldoptions, $entityId) {
                                return new \Cx\Core\Html\Model\Entity\TextElement('');
                            },
                        ),
                        'dkimState' => array(
                            'custom' => true,
                            'header' => 'TXT_CORE_NETMANAGER_RECORD',
                            'showOverview' => false,
                            'tooltip' => function($fieldname, $entityData) {
                                global $_ARRAYLANG;

                                return sprintf(
                                    $_ARRAYLANG['TXT_CORE_NETMANAGER_RECORD_LAST_UPDATE'],
                                    $entityData['cachedAt']->format(ASCMS_DATE_FORMAT_DATETIME)
                                );
                            },
                            'formfield' => function($fieldname, $fieldtype, $fieldlength, $fieldvalue, $fieldoptions, $entityId) {
                                $div = new \Cx\Core\Html\Model\Entity\HtmlElement('div');
                                $div->addChild($this->showDnsState($entityId, 'dkim'));
                                $div->addChild($this->showDnsRecord($entityId, 'dkim'));
                                return $div;
                            },
                        ),
                        'dkimProposal' => array(
                            'custom' => true,
                            'header' => 'TXT_CORE_NETMANAGER_RECORD_PROPOSAL',
                            'showOverview' => false,
                            'formfield' => function($fieldname, $fieldtype, $fieldlength, $fieldvalue, $fieldoptions, $entityId) {
                                return $this->showDnsProposal($entityId, 'dkim');
                            },
                        ),
                        'dmarcTitle' => array(
                            'custom' => true,
                            'showOverview' => false,
                            'formfield' => function($fieldname, $fieldtype, $fieldlength, $fieldvalue, $fieldoptions, $entityId) {
                                return new \Cx\Core\Html\Model\Entity\TextElement('');
                            },
                        ),
                        'dmarcState' => array(
                            'custom' => true,
                            'header' => 'TXT_CORE_NETMANAGER_RECORD',
                            'showOverview' => false,
                            'tooltip' => function($fieldname, $entityData) {
                                global $_ARRAYLANG;

                                return sprintf(
                                    $_ARRAYLANG['TXT_CORE_NETMANAGER_RECORD_LAST_UPDATE'],
                                    $entityData['cachedAt']->format(ASCMS_DATE_FORMAT_DATETIME)
                                );
                            },
                            'formfield' => function($fieldname, $fieldtype, $fieldlength, $fieldvalue, $fieldoptions, $entityId) {
                                $div = new \Cx\Core\Html\Model\Entity\HtmlElement('div');
                                $div->addChild($this->showDnsState($entityId, 'dmarc'));
                                $div->addChild($this->showDnsRecord($entityId, 'dmarc'));
                                return $div;
                            },
                        ),
                        'dmarcProposal' => array(
                            'custom' => true,
                            'header' => 'TXT_CORE_NETMANAGER_RECORD_PROPOSAL',
                            'showOverview' => false,
                            'formfield' => function($fieldname, $fieldtype, $fieldlength, $fieldvalue, $fieldoptions, $entityId) {
                                return $this->showDnsProposal($entityId, 'dmarc');
                            },
                        ),
                        'name' => array(
                            'header' => $_ARRAYLANG['TXT_CORE_NETMANAGER_DOMAIN'],
                            'type' => 'hidden',
                            'table' => array(
                                'parse' => function($value) {
                                    $domainName = contrexx_raw2xhtml(\Cx\Core\Net\Controller\ComponentController::convertIdnToUtf8Format($value));
                                    if ($domainName != contrexx_raw2xhtml($value)) {
                                        $domainName .= ' (' .  contrexx_raw2xhtml($value) . ')';
                                    }
                                    return $domainName;
                                },
                            ),
                        ),
                        'usages' => array(
                            'showDetail' => false,
                            'table' => array(
                                'parse' => function($value, $rowData) {
                                    global $_ARRAYLANG;

                                    if (!count($value)) {
                                        $el = new \Cx\Core\Html\Model\Entity\HtmlElement('i');
                                        $el->addChild(
                                            new \Cx\Core\Html\Model\Entity\TextElement(
                                                $_ARRAYLANG['TXT_CORE_NETMANAGER_USAGES_NONE']
                                            )
                                        );
                                        return $el;
                                    }
                                    $links = array();
                                    foreach ($value as $componentName=>$componentLinks) {
                                        foreach ($componentLinks as $link) {
                                            $url = \Cx\Core\Routing\Url::fromMagic($link['editurl']);
                                            $link['editurl'] = (string) $url;
                                            $span = new \Cx\Core\Html\Model\Entity\HtmlElement('span');
                                            $span->addChild(
                                                new \Cx\Core\Html\Model\Entity\TextElement(
                                                    $link['title']
                                                )
                                            );
                                            $img = new \Cx\Core\Html\Model\Entity\HtmlElement('i');
                                            $img->setAttribute('class', 'fa-solid fa-arrow-up-right-from-square');
                                            $el = new \Cx\Core\Html\Model\Entity\HtmlElement('a');
                                            $el->setAttribute('href', $link['editurl']);
                                            $el->setAttribute('target', '_blank');
                                            $el->addChild($img);
                                            $span->addChild($el);
                                            $links[] = $span;
                                        }
                                    }
                                    if (count($links) == 1) {
                                        return current($links);
                                    }
                                    $div = new \Cx\Core\Html\Model\Entity\HtmlElement('div');
                                    $collapseCheckbox = new \Cx\Core\Html\Model\Entity\HtmlElement('input');
                                    $collapseCheckbox->addClass('usages-collapse');
                                    $collapseCheckbox->setAttribute('type', 'checkbox');
                                    $collapseCheckbox->setAttribute('id', 'usages-collapse-' . $rowData['name']);
                                    $div->addChild($collapseCheckbox);
                                    $collapseLabel = new \Cx\Core\Html\Model\Entity\HtmlElement('label');
                                    $collapseLabel->setAttribute('for', 'usages-collapse-' . $rowData['name']);
                                    $div->addChild($collapseLabel);
                                    $collapsible = new \Cx\Core\Html\Model\Entity\HtmlElement('div');
                                    $collapsible->setAttribute('data-depends', 'usages-collapse-' . $rowData['name']);
                                    $div->addChild(current($links));
                                    for ($i = 1; $i < count($links); $i++) {
                                        $collapsible->addChild($links[$i]);
                                        $collapsible->addChild(new \Cx\Core\Html\Model\Entity\HtmlElement('br'));
                                    }
                                    $div->addChild($collapsible);
                                    return $div;
                                },
                            ),
                        ),
                        'cachedAt' => array(
                            'showDetail' => false,
                            'table' => array(
                                'parse' => function($value, $rowData) {
                                    if ($this->isBlacklistedDomain($rowData['name'])) {
                                        return '-';
                                    }
                                    return $value->format(ASCMS_DATE_FORMAT);
                                }
                            ),
                        ),
                        'spf' => array(
                            'showOverview' => false,
                            'showDetail' => false,
                        ),
                        'dkim' => array(
                            'showOverview' => false,
                            'showDetail' => false,
                        ),
                        'dmarc' => array(
                            'showOverview' => false,
                            'showDetail' => false,
                        ),
                        'strict' => array(
                            'showOverview' => false,
                            'showDetail' => false,
                        ),
                        'editUrl' => array(
                            'showOverview' => false,
                            'showDetail' => false,
                        ),
                        'compliant' => array(
                            'showOverview' => false,
                            'showDetail' => false,
                        ),
                        'shortestTtl' => array(
                            'showOverview' => false,
                            'showDetail' => false,
                        ),
                        'validUntil' => array(
                            'showOverview' => false,
                            'showDetail' => false,
                        ),
                    ),
                    'order' => array(
                        'overview' => array(
                            'name',
                            'status',
                            'usages',
                            'cachedAt',
                        ),
                        'form' => array(
                            'name',
                            'spfTitle',
                            'spfState',
                            'spfProposal',
                            'dkimTitle',
                            'dkimState',
                            'dkimDomainkey',
                            'dkimPublicKey',
                            'dkimKeyGenerator',
                            'dkimProposal',
                            'dmarcTitle',
                            'dmarcState',
                            'dmarcProposal',
                        ),
                    ),
                    'functions' => array(
                        'delete'        => true,
                        'sorting'       => false,
                        'paging'        => false,
                        'filtering'     => false,
                        'formButtons'   => false,
                        'order'         => ['name' => \SORT_ASC],
                        'actions' => function($rowData, $editId) {
                            global $_ARRAYLANG;
                            if ($this->isBlacklistedDomain($rowData['name'])) {
                                return '';
                            }
                            $editId = current(\Cx\Core\Html\Controller\ViewGenerator::getParam(0, $editId));
                            $editUrl = \Cx\Core\Html\Controller\ViewGenerator::getVgEditUrl(0, $editId);
                            return '<a href="#" data-action="refresh-dns-record" data-id="' . $editId . '" title="' . $_ARRAYLANG['TXT_CORE_NETMANAGER_REFRESH_LONG'] . '">
                                <i class="fa-solid fa-arrows-rotate"></i>
                            </a>
                            <a href="' . $editUrl . '" class="edit" title="' . $_ARRAYLANG['TXT_CORE_RECORD_SHOW_TITLE'] . '"></a>';
                        },
                        'onclick' => array(
                            'delete' => 'deleteTrackedSenderDomain',
                        ),
                        'additionalOverallFunctions' => function($vgId) {
                            global $_CORELANG;

                            return array(
                                array(
                                    'icon' => 'fa-solid fa-add',
                                    'label' => $_CORELANG['TXT_ADD'],
                                    'link' => 'javascript:addTrackedSenderDomain();',
                                ),
                            );
                        },
                    ),
                    'preRenderDetail' => function($vg, $formGenerator, $entityId) {
                        $this->template->touchBlock('edit_buttons');
                        $this->template->setVariable('ENTITY_ID', $entityId);
                    },
                );
                break;
            default:
                return array(
                    'header' => $header,
                    'functions' => array(
                        'add'       => true,
                        'edit'      => true,
                        'delete'    => true,
                        'sorting'   => true,
                        'paging'    => true,
                        'filtering' => false,
                    ),
                );
                break;
        }
    }

    protected function showDnsState($entityId, $feature, string $text = ''): \Cx\Core\Html\Model\Entity\HtmlElement {
        global $_ARRAYLANG;

        $status = $this->getComponent('NetManager')->getDomainMailComplianceDetails($entityId);
        $label = new \Cx\Core\Html\Model\Entity\HtmlElement('div');
        $label->addClass('label');
        $labelStatus = 'infolabel';
        $langVar = 'INVALID';
        if ($status[$feature]['isValid']) {
            $labelStatus = 'oklabel';
            $langVar = 'VALID';
        } else if (count($status[$feature]['records'])) {
            $labelStatus = 'alertlabel';
        }
        // if any of the following conditions match the feature should
        // be shown "red" instead of "white":
        // - if we have usages and check is strict
        // - if we have usages and we're parsing SPF and DKIM is not valid
        // - if we have usages and we're parsing DKIM and SPF is not valid
        if (
            $labelStatus == 'infolabel' &&
            count($status['usages']) &&
            (
                $status['strict'] ||
                (
                    $feature == 'spf' &&
                    !$status['dkim']['isValid']
                ) || (
                    $feature == 'dkim' &&
                    !$status['spf']['isValid']
                )
            )
        ) {
            $labelStatus = 'alertlabel';
        }
        $label->addClass($labelStatus);
        $label->addChild(
            new \Cx\Core\Html\Model\Entity\TextElement(
                !empty($text) ? $text : $_ARRAYLANG['TXT_CORE_NETMANAGER_STATE_' . $langVar]
            )
        );
        return $label;
    }

    protected function showDnsRecord($entityId, $feature): \Cx\Core\Html\Model\Entity\HtmlElement {
        global $_ARRAYLANG;

        $div = new \Cx\Core\Html\Model\Entity\HtmlElement('div');

        $status = $this->getComponent('NetManager')->getDomainMailComplianceDetails($entityId);
        if (!count($status) || empty($status[$feature]['records'])) {
            $div->addChild(new \Cx\Core\Html\Model\Entity\TextElement(
                $_ARRAYLANG['TXT_CORE_NETMANAGER_NO_RESULT']
            ));
        } else {
            $box = new \Cx\Core\Html\Model\Entity\HtmlElement('pre');
            foreach ($status[$feature]['records'] as $record) {
                $code = new \Cx\Core\Html\Model\Entity\HtmlElement('code');
                $code->addChild(new \Cx\Core\Html\Model\Entity\TextElement(
                    $record
                ));
                $box->addChild($code);
            }
            $div->addChild($box);
        }
        return $div;
    }

    protected function showDnsProposal($domainName, $feature): \Cx\Core\Html\Model\Entity\HtmlElement {
        global $_ARRAYLANG;

        $isCompliant = false;
        $complianceDetails = $this->getDomainMailComplianceDetails($domainName);
        $isCompliant = $complianceDetails[$feature]['isValid'];

        $div = new \Cx\Core\Html\Model\Entity\HtmlElement('div');
        if ($isCompliant) {
            $div->addClass('hidden');
            return $div;
        }

        $introText = new \Cx\Core\Html\Model\Entity\HtmlElement('span');
        $introText->addChild(
            new \Cx\Core\Html\Model\Entity\TextElement(
                sprintf(
                    $_ARRAYLANG['TXT_CORE_NETMANAGER_RECORD_PROPOSAL_INTRO'],
                    \Cx\Core\Net\Controller\ComponentController::convertIdnToUtf8Format($domainName),
                    static::INTRO_LINKS[$feature],
                    static::INTRO_LINK_TITLES[$feature]
                )
            )
        );
        $div->addChild($introText);

        switch ($feature) {
            case 'spf':
                $div->addChild($this->getRecordTable('', 'TXT', static::PROPOSED_SPF_RECORD));
                break;
            case 'dmarc':
                $div->addChild($this->getRecordTable('_dmarc', 'TXT', static::PROPOSED_DMARC_RECORD));
                break;
            case 'dkim':
                $div->addChild($this->getRecordTable('*.cloudrexx1._domainkey', 'CNAME', 'cloudrexx1.dkim.cloudrexx.com'));
                $div->addChild($this->getRecordTable('*.cloudrexx2._domainkey', 'CNAME', 'cloudrexx2.dkim.cloudrexx.com'));
                $div->addChild($this->getRecordTable('*.cloudrexx3._domainkey', 'CNAME', 'cloudrexx3.dkim.cloudrexx.com'));
                break;
        }

        if (!empty($_ARRAYLANG['TXT_CORE_NETMANAGER_RECORD_PROPOSAL_WARNING_' . strtoupper($feature)])) {
            $warningNote = new \Cx\Core\Html\Model\Entity\HtmlElement('div');
            $warningNote->addClass('warningbox');
            $warningNote->addChild(
                new \Cx\Core\Html\Model\Entity\TextElement(
                    $_ARRAYLANG['TXT_CORE_NETMANAGER_RECORD_PROPOSAL_WARNING_' . strtoupper($feature)]
                )
            );
            $div->addChild($warningNote);
        }
        return $div;
    }

    protected function getRecordTable(string $target, string $type, string $value): \Cx\Core\Html\Model\Entity\HtmlElement {
        global $_ARRAYLANG;

        $recordTable = new \Cx\Core\Html\Model\Entity\HtmlElement('table');
        $recordTable->addClass('adminlist');
        $recordTable->addClass('splitview');
        foreach (array('target', 'type', 'value') as $rowName) {
            $row = new \Cx\Core\Html\Model\Entity\HtmlElement('tr');
            $col1 = new \Cx\Core\Html\Model\Entity\HtmlElement('th');
            $col1->addChild(
                new \Cx\Core\Html\Model\Entity\TextElement(
                    $_ARRAYLANG['TXT_CORE_NETMANAGER_RECORD_PROPOSAL_' . strtoupper($rowName)],
                )
            );
            $col2 = new \Cx\Core\Html\Model\Entity\HtmlElement('td');
            $col2Content = null;
            switch ($rowName) {
                case 'target':
                    if (empty($target)) {
                        $col2Content = new \Cx\Core\Html\Model\Entity\TextElement(
                            $_ARRAYLANG['TXT_CORE_NETMANAGER_RECORD_PROPOSAL_TARGET_EMPTY']
                        );
                    } else {
                        $col2Content = new \Cx\Core\Html\Model\Entity\HtmlElement('code');
                        $col2Content->addClass('dns-target');
                        $col2Content->setAttribute('data-copyable');
                        $col2Content->addChild(new \Cx\Core\Html\Model\Entity\TextElement(
                            $target
                        ));
                    }
                    break;
                case 'type':
                    $col2Content = new \Cx\Core\Html\Model\Entity\HtmlElement('code');
                    $col2Content->addChild(new \Cx\Core\Html\Model\Entity\TextElement(
                        $type
                    ));
                    break;
                case 'value':
                    $col2Content = new \Cx\Core\Html\Model\Entity\HtmlElement('code');
                    $col2Content->setAttribute('data-copyable');
                    $col2Content->addChild(
                        new \Cx\Core\Html\Model\Entity\TextElement(
                            $value
                        )
                    );
                    break;
            }
            $col2->addChild($col2Content);
            $row->addChild($col1);
            $row->addChild($col2);
            $recordTable->addChild($row);
        }
        return $recordTable;
    }
}
