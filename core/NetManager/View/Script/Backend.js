function deleteTrackedSenderDomain(deleteUrl) {
    const domain = deleteUrl.split("?", 2)[1].split("deleteid=", 2)[1].split("&")[0];
    if (confirm(cx.variables.get("TXT_CORE_RECORD_DELETE_CONFIRM", "NetManager/lang"))) {
        cx.ajax(
            "NetManager",
            "removeTrackedSenderDomain",
            {
                "showMessage": true,
                "data": {
                    "domain": domain
                },
                "postSuccess": function() {
                    document.location.reload();
                },
                "postError": function() {
                    // This should not happen!
                }
            }
        );
    }
}
function addTrackedSenderDomain() {
    const dialog = cx.ui.dialog({
        title: cx.variables.get("TXT_ADD", "NetManager/lang"),
        content: "<label for='addDomainName'>" + cx.variables.get("TXT_CORE_NETMANAGER_DOMAIN_ADD_NAME", "NetManager/lang") + ":</label> <input type='text' id='addDomainName' />",
        modal: true,
        buttons: [
            {
                text: cx.variables.get("TXT_CANCEL", "NetManager/lang"),
                click: function() {
                    cx.jQuery(this).dialog("close");
                },
            },
            {
                text: cx.variables.get("TXT_ADD", "NetManager/lang"),
                click: function() {
                    const valueElement = document.getElementById("addDomainName");
                    const value = valueElement.value;
                    valueElement.disabled = true;
                    valueElement.classList.remove("error");
                    cx.ajax(
                        "NetManager",
                        "addTrackedSenderDomain",
                        {
                            "showMessage": true,
                            "data": {
                                "domain": value,
                            },
                            "postSuccess": function(data) {
                                document.location.reload();
                                dialog.getElement().remove()
                            },
                            "postError": function(data, statusCodeMsg, errorMsg) {
                                if (errorMsg == "Invalid domain name") {
                                    valueElement.disabled = false;
                                    valueElement.classList.add("error");
                                    valueElement.focus();
                                    // Note: There seems to be some timing problem
                                    // which requires to call removeAll() twice...
                                    cx.ui.messages.removeAll();
                                    cx.ui.messages.removeAll();
                                    return;
                                }
                                dialog.close();
                                dialog.getElement().remove()
                            }
                        }
                    );
                },
            },
        ],
    });
}

cx.ready(function() {
    document.querySelectorAll(
        "[data-action=refresh-dns-record]"
    ).forEach(
        function(e) {
            e.addEventListener(
                "click",
                function(e) {
                    e.preventDefault();
                    cx.ajax(
                        "NetManager",
                        "refreshDnsRecords",
                        {
                            "showMessage": true,
                            "data": {
                                "domain": this.dataset.id
                            },
                            "postSuccess": function(json) {
                                location.reload();
                            }
                        }
                    );
                }
            );
        }
    );
    const editNameField = document.querySelector("form[action*=TrackedSenderDomain] input[name=name]");
    if (!editNameField) {
        return;
    }
    const legend = document.querySelector("#form-0 fieldset legend");
    legend.innerHTML += ": \"" + editNameField.value + "\"";
});
