<?php declare(strict_types=1);

/**
 * Cloudrexx
 *
 * @link      https://www.cloudrexx.com
 * @copyright Cloudrexx AG
 *
 * This file is part of Cloudrexx.
 *
 * Cloudrexx is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3 of the License.
 *
 * Cloudrexx is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

namespace Cx\Core\NetManager\Model\Event;

/**
 * @copyright   Cloudrexx AG
 * @author      Michael Ritter <michael.ritter@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  core
 */
class CacheTtlUpdateEventListener extends \Cx\Core\Event\Model\Entity\DefaultEventListener {

    /**
     * Updates the TTL of cached domain validations
     *
     * As long as we can't be 100% sure that a validation is no longer valid we
     * keep the cache entry to avoid problems when having to resolve DNS on the
     * file during mail send.
     * @todo This currently always keeps all entries. We need to find a way to drop/refresh them
     * @param array $args
     * @return array An array with the new TTL for the entry as key `validUntil`
     *     or an empty array to drop it
     */
    public function cacheCachefileOutdateIndex($args): array {
        $component = $args['component'];
        if ($component != $this->getSystemComponentController()) {
            return [];
        }
        $cacheFile = $args['cacheFile'];
        $index = $args['index'];
        $entry = $cacheFile->getEntry($index);

        // Quick'n'dirty: The only Cachefile this component currently uses is 
        // the domain validation cache. For now we simply move the TTL forward
        // by a day in case the revalidation fails.
        // We never need to drop a cache entry in this Cachefile unless the user
        // manually removes a domain from the validation. In all other cases we
        // should instead update it.
        $fetchedData = $this->getSystemComponentController()->loadMailComplicanceDetails(
            $entry['name']
        );
        if ($this->verifyCompliance($entry, $fetchedData)) {
            $this->getSystemComponentController()->addCacheMetaDataToEntry($fetchedData);
            $entry = array_merge(
                $entry, $fetchedData
            );
            $cacheFile->set($index, $entry);
            return $entry;
        }
        trigger_error(
            sprintf(
                'Previous sender validation lost on domain %s!',
                $entry['name']
            ),
            E_USER_WARNING
        );
        \DBG::dump([
            'entry' => $entry,
            'fetchedData' => $fetchedData,
        ]);
        $validDate = new \DateTime();
        return ['validUntil' => $validDate->modify('+1day')];
    }

    /**
     * Checks if sender validation of `$cachedData` is covered by `$updatedData`
     *
     * @param array $cachedData Cached sender validation data for a specific
     *     sender domain.
     * @param array $updatedData Updated sender validation data for the same
     *     sender domain as `$cachedData`.
     * @return bool `true` if `$updatedData` has a sender validation equal or
     *     higher than `$cachedData`. Otherwise `false`.
     */
    protected function verifyCompliance(
        array $cachedData,
        array $updatedData
    ): bool {
        foreach (['spf', 'dmarc', 'dkim'] as $feature) {
            if (
                $cachedData[$feature]['isValid']
                && !$updatedData[$feature]['isValid']
            ) {
                return false;
            }
        }
        if (
            $cachedData['compliant']
            && !$updatedData['compliant']
        ) {
            return false;
        }
        return true;
    }
}
