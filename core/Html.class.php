<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * HTML element helpers
 *
 * Provides some commonly used HTML elements
 * @copyright   CLOUDREXX CMS - CLOUDREXX AG
 * @author      Reto Kohli <reto.kohli@comvation.com>
 * @deprecated  This class is deprecated in favor of using Sigma or \Cx\Core\Html\Model\Entity\...
 * @version     3.0.0
 * @package     cloudrexx
 * @subpackage  core
 */

/**
 * HTML class
 *
 * Provides some commonly used HTML elements
 * @copyright   CLOUDREXX CMS - CLOUDREXX AG
 * @author      Reto Kohli <reto.kohli@comvation.com>
 * @deprecated  This class is deprecated in favor of using Sigma or \Cx\Core\Html\Model\Entity\...
 * @version     3.0.0
 * @package     cloudrexx
 * @subpackage  core
 */
class Html
{
    /**
     * Class constants defining the names of various (LED) status
     *
     * See {@see Html::getLed()}
     */
    const STATUS_RED = 'red';
    const STATUS_YELLOW = 'yellow';
    const STATUS_GREEN = 'green';

    /**
     * Some basic and often used (and frequently misspelt) HTML attributes
     *
     * Note the leading space that allows you to add the placeholder right after
     * the preceeding attribute without wasting whitespace when it's unused
     */
    const ATTRIBUTE_CHECKED = ' checked="checked"';
    const ATTRIBUTE_SELECTED = ' selected="selected"';
    const ATTRIBUTE_DISABLED = ' disabled="disabled"';
    const ATTRIBUTE_READONLY = ' readonly="readonly"';
    const ATTRIBUTE_MULTIPLE = ' multiple="multiple"';
    // more...?

    /**
     * Some basic and frequently used (and often misspelt) CSS properties
     */
    const CSS_DISPLAY_NONE = 'display:none;';
    const CSS_DISPLAY_INLINE = 'display:inline;';
    const CSS_DISPLAY_BLOCK = 'display:block;';
    // more...?

    /**
     * Icon used on the link for removing an HTML element
     */
    const ICON_ELEMENT_REMOVE = '/core/Core/View/Media/icons/delete.gif';

    /**
     * Icon used on the link for adding an HTML element
     * @todo    Find a better icon for this
     */
    const ICON_ELEMENT_ADD = '/core/Core/View/Media/icons/check.gif';

    /**
     * Icon used for omitted functions
     */
    const ICON_FUNCTION_BLANK = '/core/Core/View/Media/icons/pixel.gif';

    /**
     * Icon used on the link for viewing any entry
     */
    const ICON_FUNCTION_VIEW = '/core/Core/View/Media/icons/viewmag.png';

    /**
     * Icon used on the link for deleting any entry
     */
    const ICON_FUNCTION_DELETE = '/core/Core/View/Media/icons/delete.gif';

    /**
     * Icon used on the link for copying any entry
     */
    const ICON_FUNCTION_COPY = '/core/Core/View/Media/icons/copy.gif';

    /**
     * Icon used on the link for editing any entry
     */
    const ICON_FUNCTION_EDIT = '/core/Core/View/Media/icons/edit.gif';

    /**
     * Icon used on the link for removing an image
     */
    const ICON_FUNCTION_CLEAR_IMAGE = '/core/Core/View/Media/icons/delete.gif';

    /**
     * Icon used on the link for marking as not deleted
     */
    const ICON_FUNCTION_MARK_UNDELETED = '/core/Core/View/Media/icons/restore.gif';

    /**
     * Icon used on the link for marking as deleted
     */
    const ICON_FUNCTION_MARK_DELETED = '/core/Core/View/Media/icons/empty.gif';

    /**
     * Icon used on the link for marking as special
     */
    const ICON_FUNCTION_SPECIAL_ON = '/core/Core/View/Media/icons/special_on.png';

    /**
     * Icon used on the link for marking as not special
     */
    const ICON_FUNCTION_SPECIAL_OFF = '/core/Core/View/Media/icons/special_off.png';

    /**
     * Icon used on the link for downloading a PDF document
     */
    const ICON_FUNCTION_DOWNLOAD_PDF = '/core/Core/View/Media/icons/pdf.gif';

    /**
     * Icon used for red status
     */
    const ICON_STATUS_RED = '/core/Core/View/Media/icons/status_red.gif';

    /**
     * Icon used for yellow status
     */
    const ICON_STATUS_YELLOW = '/core/Core/View/Media/icons/status_yellow.gif';

    /**
     * Icon used for green status
     */
    const ICON_STATUS_GREEN = '/core/Core/View/Media/icons/status_green.gif';

    /**
     * Icon used for the checked status
     */
    const ICON_STATUS_CHECKED = '/core/Core/View/Media/icons/check.gif';

    /**
     * Icon used for the unchecked status
     */
    const ICON_STATUS_UNCHECKED = '/core/Core/View/Media/icons/pixel.gif';

    /**
     * Icon used for Comments (with tooltip containing the text)
     */
    const ICON_COMMENT = '/core/Core/View/Media/icons/comment.gif';

    /**
     * Icon used for omitted icons (for aligning/formatting)
     */
    const ICON_BLANK = '/core/Core/View/Media/icons/blank.gif';

    /**
     * Index counter for all form elements
     *
     * Incremented and added to the tabindex attribute for each element
     * in the order they are created
     * @var   integer
     */
    private static $index_tab = 0;

    /**
     * Returns HTML code for a form
     *
     * If the $id parameter is false, the id attribute is not set.
     * If it's empty (but not false), the name is used instead.
     * @param   string    $name         The element name
     * @param   string    $action       The action URI
     * @param   string    $content      The form content
     * @param   string    $id           The optional element id
     * @param   string    $method       The optional request method.
     *                                  Defaults to 'post'
     * @param   string    $attribute    Additional optional attributes
     * @return  string                  The HTML code for the element
     * @author  Reto Kohli <reto.kohli@comvation.com>
     */
    static function getForm(
        $name, $action, $content, $id=false, $method='post', $attribute=''
    ) {
//echo("Html::getForm(): action ".contrexx_raw2xhtml($action)."<br />");
        return
            '<form name="'.$name.'"'.
            ($id === false ? '' : ' id="'.($id ? $id : $name).'"').
            'action="'.$action.'"'.
            'method="'.($method == 'post' ? 'post' : 'get').'"'.
            ($attribute ? ' '.$attribute : '').
            ">\n".$content."</form>\n";
    }


    /**
     * Returns HTML code for a text imput field
     *
     * If the $id parameter is false, the id attribute is not set.
     * If it's empty (but not false), the name is used instead.
     * If the custom attributes parameter $attribute is empty, and
     * is_numeric($value) evaluates to true, the text is right aligned
     * within the input element.
     * @param   string    $name         The element name
     * @param   string    $value        The element value, defaults to the
     *                                  empty string
     * @param   string    $id           The optional element id, defaults to
     *                                  false for none
     * @param   string    $attribute    Additional optional attributes
     * @return  string                  The HTML code for the element
     * @author  Reto Kohli <reto.kohli@comvation.com>
     */
    static function getInputText($name, $value='', $id=false, $attribute='')
    {
        return
            '<input type="text" name="'.$name.'"'.
            ($id === false ? '' : ' id="'.($id ? $id : $name).'"').
            ' value="'.contrexx_raw2xhtml($value).'"'.
// TODO: Add this exeption to other elements
            (preg_match('/\btabindex\b/', $attribute)
              ? '' : ' tabindex="'.++self::$index_tab.'"').
            ($attribute
              ? " $attribute"
              : (is_numeric($value)
                  ? ' style="text-align: right;"'
                  : '')).
            " />\n";
    }


    /**
     * Returns HTML code for a number imput field
     *
     * Note: This requires an HTML5 capable browser.
     * If the $id parameter is false, the id attribute is not set.
     * If it's empty (but not false), the name is used instead.
     * Use the $attribute parameter to specify additional attributes, like
     * min, max, or step.
     * @param   string    $name         The element name
     * @param   string    $value        The element value, defaults to the
     *                                  empty string
     * @param   string    $id           The optional element id, defaults to
     *                                  false for none
     * @param   string    $attribute    Additional optional attributes
     * @return  string                  The HTML code for the element
     * @author  Reto Kohli <reto.kohli@comvation.com>
     */
    static function getInputNumber($name, $value='', $id=false, $attribute='')
    {
        return
            '<input type="number" name="'.$name.'"'.
            ($id === false ? '' : ' id="'.($id ? $id : $name).'"').
            ' value="'.contrexx_raw2xhtml($value).'"'.
// TODO: Add this exeption to other elements
            (preg_match('/\btabindex\b/', $attribute)
              ? '' : ' tabindex="'.++self::$index_tab.'"').
            ($attribute
              ? " $attribute"
              : '').
//              (is_numeric($value)
//                  ? ' style="text-align: right;"'
//                  : '')).
            " />\n";
    }


    /**
     * Returns HTML code for a range imput field
     *
     * Note: This requires an HTML5 capable browser.
     * If the $id parameter is false, the id attribute is not set.
     * If it's empty (but not false), the name is used instead.
     * Use the $attribute parameter to specify additional attributes, like
     * min, max, or step.
     * @param   string    $name         The element name
     * @param   string    $value        The element value, defaults to the
     *                                  empty string
     * @param   string    $id           The optional element id, defaults to
     *                                  false for none
     * @param   string    $attribute    Additional optional attributes
     * @return  string                  The HTML code for the element
     * @author  Reto Kohli <reto.kohli@comvation.com>
     */
    static function getInputRange($name, $value='', $id=false, $attribute='')
    {
        return
            '<input type="range" name="'.$name.'"'.
            ($id === false ? '' : ' id="'.($id ? $id : $name).'"').
            ' value="'.contrexx_raw2xhtml($value).'"'.
// TODO: Add this exeption to other elements
            (preg_match('/\btabindex\b/', $attribute)
              ? '' : ' tabindex="'.++self::$index_tab.'"').
            ($attribute
              ? " $attribute"
              : '').
//              (is_numeric($value)
//                  ? ' style="text-align: right;"'
//                  : '')).
            " />\n";
    }


    /**
     * Returns HTML code for a password text imput field
     *
     * The $name parameter is used for both the element name and id attributes.
     * @param   string    $name         The element name
     * @param   string    $value        The element value
     * @param   string    $attribute    Additional optional attributes
     * @return  string                  The HTML code for the element
     * @author  Reto Kohli <reto.kohli@comvation.com>
     */
    static function getInputPassword($name, $value, $attribute='')
    {
        return
            '<input type="password" name="'.$name.'" id="'.$name.'"'.
            ' value="'.contrexx_raw2xhtml($value).'"'.
            ' tabindex="'.++self::$index_tab.'"'.
            ($attribute ? ' '.$attribute : '').
            " />\n";
    }


    /**
     * Returns HTML code for a file upload input field
     *
     * If the $id parameter is false, the id attribute is not set.
     * If it's empty (but not false), the name is used instead.
     * @param   string    $name         The element name
     * @param   string    $id           The optional element id.  You can
     *                                  only get the target path and delete
     *                                  functionality if this is not false.
     * @param   string    $maxlength    The optional maximum accepted size
     * @param   string    $mimetype     The optional accepted MIME type
     * @param   string    $attribute    Additional optional attributes
     * @param   boolean   $visible      If true, the input element is set
     *                                  visible.  Defaults to true
     * @param   string    $path         Optional path.  If not empty, and the
     *                                  $id is non-empty,
     *                                  the file path is shown
     *                                  on top of the upload element, with
     *                                  a clickable delete icon to the right.
     *                                  Defaults to false
     * @return  string                  The HTML code for the element
     * @author  Reto Kohli <reto.kohli@comvation.com>
     */
    static function getInputFileupload(
        $name, $id=false, $maxlength='', $mimetype='', $attribute='',
        $visible=true, $path=false
    ) {
//        global $_CORELANG;

        $input = '';
        if ($path) {
            $id_path = '';
            if ($id !== false) {
                $id = ($id ? $id : $name);
                $id_path = 'path-'.$id;
                $id_div = 'div-'.$id;
                $input =
                    '<div id="'.$id_div.'">'.
                    '<a href="'.urlencode(ASCMS_PATH_OFFSET.'/'.$path).'">'.
                    $path.'</a>&nbsp;
                    <a href="javascript:void(0);" onclick="javascript:document.getElementById(\''.$id_path.'\').value=\'\';'.
                            'document.getElementById(\''.$id_div.'\').style.display=\'none\';'.
                    '">
                      <i class="fa-regular fa-trash-can" title="{TXT_CORE_HTML_DELETE}"></i>
                    </a>
                    </div>';
            }
            $input .= self::getHidden($name, $path, $id_path);
        }
        $id = ($id === false ? '' : ' id="'.($id ? $id : $name).'"');
        return
            $input.
            '<input type="file" name="'.$name.'"'.($id ? $id : '').
            ' tabindex="'.++self::$index_tab.'"'.
            ($maxlength ? ' maxlength="'.$maxlength.'"' : '').
            ($mimetype ? ' accept="'.$mimetype.'"' : '').
            ($attribute ? ' '.$attribute : '').
            ($visible ? '' : ' style="display: none;"').
            " />\n";
    }


    /**
     * Returns HTML code for a text area
     *
     * The $name parameter is used for both the element name and id attributes.
     * @param   string    $name         The element name
     * @param   string    $value        The element value
     * @param   string    $cols         The optional number of columns
     * @param   string    $rows         The optional number of rows
     * @param   string    $attribute    Additional optional attributes
     * @return  string                  The HTML code for the element
     * @author  Reto Kohli <reto.kohli@comvation.com>
     */
    static function getTextarea(
        $name, $value, $cols='', $rows='', $attribute=''
    ) {
        return
            '<textarea name="'.$name.'" id="'.$name.'"'.
            ' tabindex="'.++self::$index_tab.'"'.
            ($cols ? ' cols="'.$cols.'"' : '').
            ($rows ? ' rows="'.$rows.'"' : '').
            ($attribute ? ' '.$attribute : '').
            '>'.contrexx_raw2xhtml($value).
            "</textarea>\n";
    }


    /**
     * Returns HTML code for a hidden imput field
     *
     * If the $id parameter is false, the id attribute is not set.
     * If it's empty (but not false), the name is used instead.
     * @todo    Maybe the optional attributes will never be used
     *          and can be removed?
     * @param   string    $name         The element name
     * @param   string    $value        The element value
     * @param   string    $id           The element id, if non-empty
     * @param   string    $attribute    Additional optional attributes
     * @return  string                  The HTML code for the element
     * @author  Reto Kohli <reto.kohli@comvation.com>
     */
    static function getHidden($name, $value, $id=false, $attribute='')
    {
        $id = ($id === false ? '' : ' id="'.($id ? $id : $name).'"');
//DBG::log("Html::getHidden($name, $value, $id, $attribute): Fixed id $id");
        return
            '<input type="hidden" name="'.$name.'"'.
            $id.
            ' value="'.htmlspecialchars($value, ENT_COMPAT).'"'.
            ($attribute ? ' '.$attribute : '')." />\n";
    }

    /**
     * Returns HTML code for a button
     *
     * If the $id parameter is false, the id attribute is not set.
     * If it's empty (but not false), the name is used instead.
     * @param   string    $name         The element name
     * @param   string    $value        The element value
     * @param   string    $type         The button type, defaults to 'submit'
     * @param   string    $id           The element id, if non-empty
     * @param   string    $attribute    Additional optional attributes
     * @param   string    $label        The optional label text
     * @param   string    $label_attribute  The optional label attributes
     * @return  string                  The HTML code for the element
     * @author  Reto Kohli <reto.kohli@comvation.com>
     */
    static function getInputButton(
        $name, $value, $type='submit', $id=false, $attribute='',
        $label='', $label_attribute=''
    ) {
        if (   $type != 'submit'
            && $type != 'reset'
            && $type != 'button') $type = 'submit';
        $id = ($id === false ? '' : ($id ? $id : $name));
        return
            '<input type="'.$type.'" name="'.$name.'"'.
            ($id ? ' id="'.$id.'"' : '').
            ' tabindex="'.++self::$index_tab.'"'.
            ' value="'.contrexx_raw2xhtml($value).'"'.
            ($attribute ? ' '.$attribute : '')." />\n".
            ($label
              ? self::getLabel($id, $label, $label_attribute) : '');
    }


    /**
     * Returns HTML code for a dropdown menu
     *
     * If the name is empty, the empty string is returned.
     * If the $id parameter is false, the id attribute is not set.
     * If it's empty (but not false), the name is used instead.
     * @param   string    $name         The element name
     * @param   array     $arrOptions   The options array
     * @param   string    $selected     The optional preselected option key
     * @param   string    $id           The optional element id
     * @param   string    $onchange     The optional onchange event script
     * @param   string    $attribute    Additional optional attributes
     * @return  string                  The HTML code for the element
     * @author  Reto Kohli <reto.kohli@comvation.com>
     */
    static function getSelect(
        $name, $arrOptions=array(), $selected='', $id=false, $onchange='', $attribute=''
    ) {
//DBG::log("getSelect(name $name, options ".var_export($arrOptions, true).", selected $selected, id $id, onchange $onchange, attribute $attribute): Entered<br />");
        return self::getSelectCustom($name,
            self::getOptions($arrOptions, $selected), $id,
            $onchange, $attribute);
    }


    /**
     * Returns HTML code for a custom dropdown menu
     *
     * Similar to {@see getSelect()}, but takes a preproduced string
     * for its options instead of an array and selected key.
     * If the name is empty, the empty string is returned.
     * If the $id parameter is false, the id attribute is not set.
     * If it's empty (but not false), the name is used instead.
     * @param   string    $name         The element name
     * @param   string    $strOptions   The options string
     * @param   string    $id           The optional element id
     * @param   string    $onchange     The optional onchange event script
     * @param   string    $attribute    Additional optional attributes
     * @return  string                  The HTML code for the element
     * @author  Reto Kohli <reto.kohli@comvation.com>
     */
    static function getSelectCustom(
        $name, $strOptions='', $id=false, $onchange='', $attribute=''
    ) {
        if (empty($name)) {
            return '';
        }
        $menu =
            '<select name="'.$name.'"'.
            ($id === false ? '' : ' id="'.($id ? $id : $name).'"').
// TODO: Add this exeption to other elements
            (preg_match('/\btabindex\b/', $attribute)
              ? '' : ' tabindex="'.++self::$index_tab.'"').
            ($onchange ? ' onchange="'.$onchange.'"' : '').
            ($attribute ? ' '.$attribute : '').
            ">\n".$strOptions."</select>\n";
//echo("getSelectCustom(): made menu: ".contrexx_raw2xhtml($menu)."<br />");
        return $menu;
    }


    /**
     * Returns HTML code for selection options
     *
     * The optional $selected parameter may be an array, in which case all
     * IDs found in the array keys are selected.  The arrays' values are
     * ignored.
     * @param   array   $arrOptions The options array
     * @param   mixed   $selected   The optional preselected option key
     *                              or array of keys
     * @param   string  $attribute  Additional optional attributes
     * @return  string              The menu options HTML code
     * @author  Reto Kohli <reto.kohli@comvation.com>
     */
    static function getOptions($arrOptions, $selected='', $attribute=null)
    {
        $options = '';
        foreach ($arrOptions as $key => $value) {
            $options .=
                '<option value="' . $key . '"'
                . (
                    (
                        is_array($selected) &&
                        array_key_exists($key, $selected)
                    ) || 
                    (
                        !is_array($selected) &&
                        "$selected" === "$key"
                    )
                    ? Html::ATTRIBUTE_SELECTED : ''
                )
                . ($attribute ? ' ' . $attribute : '') . '>'
                . ($value != '' ? contrexx_raw2xhtml($value) : '&nbsp;')
                . "</option>\n";
        }
        return $options;
    }


    /**
     * Returns HTML code for a radio button group
     *
     * If the name is empty, the empty string is returned.
     * The $name parameter is both used for the name and id parameter
     * in the element.  For the id, a dash and an additional index are
     * appended, like '$name-$index'.  That index is increased accordingly
     * on each call to this method.  Mind that thus, it *MUST* be unique on
     * your page.
     * The $arrOptions array must contain the value-text pairs in the order
     * to be added.  The values are used in the radio button, and the text
     * for the label appended.
     * @param   string    $name         The element name
     * @param   array     $arrOptions   The options array
     * @param   string    $checked      The optional preselected option key
     * @param   string    $onchange     The optional onchange event script
     * @param   string    $attributeRadio    Additional optional attributes
     *                                  for the radio button elements
     * @param   string    $attributeLabel    Additional optional attributes
     *                                  for the labels
     * @return  string                  The HTML code for the element
     * @author  Reto Kohli <reto.kohli@comvation.com>
     */
    static function getRadioGroup(
        $name, $arrOptions, $checked='', $onchange='',
        $attributeRadio='', $attributeLabel=''
    ) {
        static $index = array();

//echo("getRadioGroup($name, $arrOptions, $checked, $onchange, $attributeRadio, $attributeLabel): Entered<br />");

        if (empty($name)) return '';
        // Remove any bracketed construct from the end of the name
        $name_stripped = preg_replace('/\[.*$/', '', $name);
        $radiogroup = '';
        foreach ($arrOptions as $value => $text) {
            $index[$name_stripped] = (empty($index[$name_stripped])
                ? 1 : ++$index[$name_stripped]);
            $id = $name_stripped.'-'.$index[$name_stripped];
            $radiogroup .=
                self::getRadio(
                    $name, $value, $id, ($value == $checked),
                    $onchange, $attributeRadio
                ).
                self::getLabel(
                    $id,
                    $text,
                    $attributeLabel
                );
        }
//echo("getRadioGroup(): Made ".contrexx_raw2xhtml($radiogroup)."<br />");
        return $radiogroup;
    }


    /**
     * Returns HTML code for a radio button
     *
     * If the name is empty, the empty string is returned.
     * If the $id parameter is false, the id attribute is not set.
     * If it's empty (but not false), the name is used instead.
     * @param   string    $name         The element name
     * @param   array     $value        The element value
     * @param   string    $id           The optional element id
     * @param   boolean   $checked     If true, the radio button is
     *                                  preselected.  Defaults to false
     * @param   string    $onchange     The optional onchange event script
     * @param   string    $attribute    Additional optional attributes
     * @return  string                  The HTML code for the element
     * @author  Reto Kohli <reto.kohli@comvation.com>
     */
    static function getRadio(
        $name, $value, $id=false, $checked=false, $onchange='', $attribute='')
    {

//echo("getRadio($name, $value, $id, $checked, $onchange, $attribute): Entered<br />");

        if (empty($name)) return '';
        return
            '<input type="radio" name="'.$name.
            '" value="'.contrexx_raw2xhtml($value).'"'.
            ($id === false ? '' : ' id="'.($id ? $id : $name).'"').
            ($checked ? ' checked="checked"' : '').
// TODO: Add this exeption to other elements
            (preg_match('/\btabindex\b/', $attribute)
              ? '' : ' tabindex="'.++self::$index_tab.'"').
            ($onchange ? ' onchange="'.$onchange.'"' : '').
            ($attribute ? ' '.$attribute : '').
            " />\n";
    }


    /**
     * Returns HTML code for a checkbox group
     *
     * If the name is empty, the empty string is returned.
     * The $name parameter is both used for the name and id parameter
     * in the element.  For the id, a dash and an additional index are
     * appended, like '$name-$index'.  That index is increased accordingly
     * on each call to this method.  Mind that thus, it *MUST* be unique on
     * your page.
     * The $arrOptions array must contain the key-value pairs in the order
     * to be added.  The keys are used to index the name attribute in the
     * checkboxes, the value is put into the value attribute.
     * The $arrLabel should use the same keys, its values are appended
     * as label text to the respective checkboxes, if present.
     * The $arrChecked array may contain the values to be preselected
     * as array values.  It's keys are ignored.
     * @param   string    $name         The element name
     * @param   array     $arrOptions   The options array
     * @param   array     $arrLabel     The optional label text array
     * @param   array     $arrChecked   The optional preselected option keys
     * @param   string    $id           The optional element id
     * @param   string    $onchange     The optional onchange event script
     * @param   string    $separator    The optional separator between
     *                                  checkboxes
     * @param   string    $attributeRadio    Additional optional attributes
     *                                  for the checkbox elements
     * @param   string    $attributeLabel    Additional optional attributes
     *                                  for the labels
     * @return  string                  The HTML code for the elements
     * @author  Reto Kohli <reto.kohli@comvation.com>
     */
    static function getCheckboxGroup(
        $name, $arrOptions, $arrLabel=null, $arrChecked=null, $id=false,
        $onchange='', $separator='',
        $attributeCheckbox='', $attributeLabel=''
    ) {
        static $index = array();

//echo("getCheckboxGroup($name, ".var_export($arrOptions, true).", ".var_export($arrLabel, true).", ".var_export($arrChecked, true).", $onchange, $attributeCheckbox, $attributeLabel): Entered<br />");

        if (empty($name)) return '';
        // Remove any bracketed construct from the end of the name
        $name_stripped = preg_replace('/\[.*$/', '', $name);
        if (!is_array($arrLabel)) $arrLabel = array();
        if (!is_array($arrChecked)) $arrChecked = array();
        if (empty($id) && $id !== false) $id = $name_stripped;
        $checkboxgroup = '';
        foreach ($arrOptions as $key => $value) {
            if (empty($index[$name_stripped])) $index[$name_stripped] = 0;
            $id_local = ($id ? $id.'-'.++$index[$name_stripped] : false);
            $checkboxgroup .=
                ($checkboxgroup ? $separator : '').
                self::getCheckbox(
                    $name.'['.$key.']', $key, $id_local,
                    in_array($key, $arrChecked),
                    $onchange, $attributeCheckbox
                ).
                self::getLabel(
                    $id_local,
                    $arrLabel[$key],
                    $attributeLabel
                );
        }
        return $checkboxgroup;
    }


    /**
     * Returns HTML code for a checkbox
     *
     * If the name is empty, the empty string is returned.
     * The $value is contrexx_raw2xhtml()d to prevent side effects.
     * If the $id parameter is false, the id attribute is not set.
     * If it's empty (but not false), the name is used instead.
     * @param   string    $name         The element name
     * @param   string    $value        The element value, defaults to 1 (one)
     * @param   string    $id           The optional element id
     * @param   boolean   $checked      If true, the checkbox is checked
     * @param   string    $onchange     The optional onchange event script
     * @param   string    $attribute    Additional optional attributes
     * @return  string                  The HTML code for the element
     * @author  Reto Kohli <reto.kohli@comvation.com>
     */
    static function getCheckbox(
        $name, $value=1, $id=false, $checked=false, $onchange='', $attribute=''
    ) {

//echo("getCheckbox($name, $value, $id, $checked, $onchange, $attribute): Entered<br />");

        if (empty($name)) return '';
        return
            '<input type="checkbox" name="'.$name.'"'.
            ' value="'.contrexx_raw2xhtml($value).'"'.
            ($id === false ? '' : ' id="'.($id ? $id : $name).'"').
            ($checked ? ' checked="checked"' : '').
            ' tabindex="'.++self::$index_tab.'"'.
            ($onchange ? ' onchange="'.$onchange.'"' : '').
            ($attribute ? ' '.$attribute : '').
            " />\n";
    }


    /**
     * Wraps the content in a label
     *
     * The $text is contrexx_raw2xhtml()d to prevent side effects.
     * Mind that the $for parameter must match the id attribute of the
     * contained element in $content.
     * @param   string    $for          The for attribute of the label
     * @param   string    $text         The text of the label
     * @param   string    $attribute    Additional optional attributes
     * @return  string                  The HTML code for the label with
     *                                  the text
     * @author  Reto Kohli <reto.kohli@comvation.com>
     */
    static function getLabel($for, $text, $attribute='')
    {
        global $_ARRAYLANG;

        $text = isset($_ARRAYLANG[$text]) ? $_ARRAYLANG[$text] : contrexx_raw2xhtml($text);

        return
            '<label for="'.$for.'"'.
            ($attribute ? ' '.$attribute : '').
            '>'. $text ."</label>\n";
    }

    /**
     * Returns an image tag for the given Image path
     *
     * This adds alt, width, and height attributes with the values returned
     * by {@see ImageManager}.
     * If the $attribute parameter contains one of the alt, width, or height
     * attributes (or corresponding style information), these will override
     * the data from the Image object.
     * @param   Image     $objImage     The Image object
     * @param   string    $attribute    Additional optional attributes
     * @return  string                  The HTML code for the image tag
     * @author  Reto Kohli <reto.kohli@comvation.com>
     */
    static function getImageByPath($image_path, $attribute = '') {
        File::path_relative_to_root($image_path);
        $image_path = preg_replace('/\.thumb$/', '', $image_path);
        if ($image_path == 'images/modules/hotelcard/no_image.gif') {
            $image_path = '';
        } else {
            $image_path = strip_tags($image_path);
        }
        $attribute = trim($attribute);
        $alt = '';
        if (!preg_match('/alt\=/i', $attribute)) {
            $alt = ' alt="' . $image_path . '"';
        }
        if ($attribute != '') {
            $attribute = ' ' . $attribute;
        }
        return '<img src="' . contrexx_raw2encodedUrl($image_path) . '"' . $alt . $attribute . ' />';
    }

    /**
     * Returns a datepicker element
     *
     * Uses and activates jQueryUI
     * (see {@see DateTimeTools::addDatepickerJs()}).
     * If the $id parameter is empty, uses the $name parameter value to
     * create an id attribute.
     * The ID attribute created for the element is returned in the $id
     * parameter.
     * The options array may contain any keys that are valid datepicker
     * options, and their respective values.
     * See http://jqueryui.com/demos/datepicker/#options for details.
     * As of writing this (20101125), valid options are:
     *  altField
     *  altFormat
     *  appendText
     *  autoSize
     *  buttonImage
     *  buttonImageOnly
     *  buttonText
     *  calculateWeek
     *  changeMonth
     *  changeYear
     *  closeText
     *  constrainInput
     *  currentText
     *  dateFormat
     *  dayNames
     *  dayNamesMin
     *  dayNamesShort
     *  defaultDate
     *  disabled
     *  duration
     *  firstDay
     *  gotoCurrent
     *  hideIfNoPrevNext
     *  isRTL
     *  maxDate
     *  minDate
     *  monthNames
     *  monthNamesShort
     *  navigationAsDateFormat
     *  nextText
     *  numberOfMonths
     *  prevText
     *  selectOtherMonths
     *  shortYearCutoff
     *  showAnim
     *  showButtonPanel
     *  showCurrentAtPos
     *  showMonthAfterYear
     *  showOn
     *  showOptions
     *  showOtherMonths
     *  showWeek
     *  stepMonths
     *  weekHeader
     *  yearRange
     *  yearSuffix
     * Note that you *MUST* specify boolean option values as true or false,
     * *NOT* as string literals "true" or "false".  The latter will not work.
     * @link    http://jqueryui.com/demos/datepicker/
     * @link    http://jqueryui.com/demos/datepicker/#options
     * @param   string    $name       The element name
     * @param   array     $options    The optional datepicker options,
     *                                including the value (defaultDate)
     * @param   string    $attribute  The optional attributes
     * @param   string    $id         The optional ID, by reference.
     * @return  string                The datepicker element HTML code
     * @internal  Ignore the code analyzer warning about $id
     */
    static function getDatepicker($name, $options=null, $attribute=null, &$id=null)
    {
        static $index = 0;

        DateTimeTools::addDatepickerJs();
        if (empty($id)) $id = ($name ? $name : 'datepicker-'.++$index);
// TODO: Add sensible defaults for mandatory options *only*
//if (!isset($options['defaultDate'])) $options['defaultDate'] = '+1';
//if (!isset($options['minDate'])) $options['minDate'] = '+1d';
//if (!isset($options['maxDate'])) $options['maxDate'] = '+1y';
//if (!isset($options['gotoCurrent'])) $options['gotoCurrent'] = 'true';
        $date = '';
        $strOptions = '';
        foreach ($options as $option => $value) {
//DBG::log("Html::getDatePicker(): Option $option => value $value");
            $strOptions .=
            ($strOptions ? ', ' : '').
            $option.': '.
            (is_numeric($value)
              ? $value
              : (is_bool($value)
                  ? ($value ? 'true' : 'false')
                  : '"'.$value.'"'));
            if ($option == 'defaultDate') {
                $date = $value;
            }
        }
//DBG::log("Html::getDatePicker(): Options: $strOptions");
        JS::registerCode('
cx.jQuery(function() {
  cx.jQuery("#'.$id.'").datepicker({'.$strOptions.'});
});
');
        return self::getInputText($name, $date, $id, $attribute);
    }

    /**
     * Returns a datetimepicker element
     *
     * @staticvar integer $index integer value
     * @param     string $name textbox name for datetimepicker
     * @param     array  $options datetimepicker options Ex.dateFormat
     * @param     string $attribute
     * @param     string $id To initialize the datatime picker
     *
     * @return    string The datetimepicker element HTML code
     */
    static function getDatetimepicker($name, $options = null, $attribute = null, &$id = null) {
        static $index = 0;
        DateTimeTools::addDatepickerJs();
        if (empty($id))
            $id = ($name ? $name : 'datetimepicker-' . ++$index);
        $date = '';
        $strOptions = '';
        foreach ($options as $option => $value) {
            $strOptions .=
                    ($strOptions ? ', ' : '') .
                    $option . ': ' .
                    (is_numeric($value) ? $value : (is_bool($value) ? ($value ? 'true' : 'false') : '"' . $value . '"'));
            if ($option == 'defaultDate') {
                $date = $value;
            }
        }
        JS::registerCode('
cx.jQuery(function() {
  cx.jQuery("#' . $id . '").datetimepicker({' . $strOptions . '});
});
');
        return self::getInputText($name, $date, $id, $attribute);
    }

    /**
     * Returns HMTL code for displaying two adjacent menus
     *
     * Options can be moved between the menus.
     * The index "onsubmit" is added to the options array; insert this
     * string into the onsubmit attribute of your form in order to have
     * all options selected before submitting the form.
     * @param   array   $options    The options array, by reference
     */
    static function getTwinmenu(&$options=null)
    {
        global $_CORELANG;

        if (empty($options['label_left']))
            $options['label_left'] =
                $_CORELANG['TXT_CORE_HTML_TWINMENU_DEFAULT_LABEL_LEFT'];
        if (empty($options['name_left']))
            $options['name_left'] = 'left_'.++self::$index_tab;
        if (empty($options['options_left']))
            $options['options_left'] = array();
        if (empty($options['selected_left']))
            $options['selected_left'] = null;
        if (empty($options['id_left']))
            $options['id_left'] = $options['name_left'];
        if (empty($options['onchange_left']))
            $options['onchange_left'] = '';
        if (empty($options['attribute_left']))
            $options['attribute_left'] = '';
        if (!preg_match('/\b(?:size|height)\b/i', $options['attribute_left'])) {
            $options['attribute_left'] .= ' size="10"';
        }
        if (!preg_match('/\b(?:width)\b/i', $options['attribute_left'])) {
            $options['attribute_left'] .= ' style="width: 290px;"';
        }
        if (!preg_match('/\b(?:multiple)\b/i', $options['attribute_left'])) {
            $options['attribute_left'] .= Html::ATTRIBUTE_MULTIPLE;
        }

        if (empty($options['label_right']))
            $options['label_right'] =
                $_CORELANG['TXT_CORE_HTML_TWINMENU_DEFAULT_LABEL_RIGHT'];
        if (empty($options['name_right']))
            $options['name_right'] = 'right_'.++self::$index_tab;
        if (empty($options['options_right']))
            $options['options_right'] = array();
        if (empty($options['selected_right']))
            $options['selected_right'] = null;
        if (empty($options['id_right']))
            $options['id_right'] = $options['name_right'];
        if (empty($options['onchange_right']))
            $options['onchange_right'] = '';
        if (empty($options['attribute_right']))
            $options['attribute_right'] = '';
        if (!preg_match('/\b(?:size|height)\b/i', $options['attribute_right'])) {
            $options['attribute_right'] .= ' size="10"';
        }
        if (!preg_match('/\b(?:width)\b/i', $options['attribute_right'])) {
            $options['attribute_right'] .= ' style="width: 290px;"';
        }
        if (!preg_match('/\b(?:multiple)\b/i', $options['attribute_right'])) {
            $options['attribute_right'] .= Html::ATTRIBUTE_MULTIPLE;
        }

        if (empty($options['label_button_move_right']))
            $options['label_button_move_right'] =
                $_CORELANG['TXT_CORE_HTML_TWINMENU_DEFAULT_LABEL_BUTTON_MOVE_RIGHT'];
        if (empty($options['label_button_move_left']))
            $options['label_button_move_left'] =
                $_CORELANG['TXT_CORE_HTML_TWINMENU_DEFAULT_LABEL_BUTTON_MOVE_LEFT'];

        if (empty($options['name_button_move_right']))
            $options['name_button_move_right'] = 'move_right_'.++self::$index_tab;
        if (empty($options['name_button_move_left']))
            $options['name_button_move_left'] = 'move_left_'.++self::$index_tab;

        JS::registerCode(self::getJavascript_Twinmenu());
        if (empty($options['onsubmit'])) {
            $options['onsubmit'] = '';
        }
        $options['onsubmit'] =
            'select_options(document.getElementById(\''.$options['id_left'].'\'),true);'.
            'select_options(document.getElementById(\''.$options['id_right'].'\'),true);';
        return
            '<div style="float:left;width:300px;">'."\n".
// TODO: Add an option to set the separator or to reposition the labels
            $options['label_left'].'<br />'."\n".
            self::getSelect($options['name_left'].'[]', $options['options_left'],
                $options['selected_left'], $options['id_left'],
                $options['onchange_left'], $options['attribute_left']).
            '<br />'."\n".
            self::getInputButton('select_all_left',
                $_CORELANG['TXT_CORE_HTML_SELECT_ALL'], 'button', false,
                'onclick="select_options(this.form.'.$options['name_left'].',true)"'
            ).
            self::getInputButton('deselect_all_left',
                $_CORELANG['TXT_CORE_HTML_DESELECT_ALL'], 'button', false,
                'onclick="select_options(this.form.'.$options['name_left'].',false)"'
            ).
            '</div>'."\n".
            '<div style="float:left;width:40px;">'."\n".
            '<br />'."\n".
            self::getInputButton($options['name_button_move_right'],
                $options['label_button_move_right'], 'button', false,
                'onclick="move_options('.
//                  'this.form.elements[\''.$options['name_left'].'\'],'.
//                  'this.form.elements[\''.$options['name_right'].'\'],'.
                  'this.form.'.$options['name_left'].','.
                  'this.form.'.$options['name_right'].','.
                  $options['name_button_move_right'].','.
                  $options['name_button_move_left'].');"'
            ).
            '<br />'."\n".
            self::getInputButton($options['name_button_move_left'],
                $options['label_button_move_left'], 'button', false,
                'style="margin-top:2px;" '.
                'onclick="move_options('.
//                  'this.form.elements[\''.$options['name_right'].'\'],'.
//                  'this.form.elements[\''.$options['name_left'].'\'],'.
                  'this.form.'.$options['name_right'].','.
                  'this.form.'.$options['name_left'].','.
                  $options['name_button_move_left'].','.
                  $options['name_button_move_right'].');"'
            ).
            "\n".
            '</div>'."\n".
            '<div style="float:left;width:300px;">'.
            $options['label_right'].'<br />'."\n".
            self::getSelect($options['name_right'].'[]', $options['options_right'],
                $options['selected_right'], $options['id_right'],
                $options['onchange_right'], $options['attribute_right']).
            '<br />'."\n".
            self::getInputButton('select_all_right',
                $_CORELANG['TXT_CORE_HTML_SELECT_ALL'], 'button', false,
                'onclick="select_options(this.form.'.$options['name_right'].',true)"'
            ).
            self::getInputButton('deselect_all_right',
                $_CORELANG['TXT_CORE_HTML_DESELECT_ALL'], 'button', false,
                'onclick="select_options(this.form.'.$options['name_right'].',false)"'
            ).
            '</div>'."\n";
    }


    /**
     * Returns the Javascript code required by the Twinmenu
     *
     * This method is called by {@see getTwinmenu()} and *SHOULD NOT* be
     * used otherwise.
     * @return  string          The Javascript code
     * @static
     * @access  private
     */
    private static function getJavascript_Twinmenu()
    {
        return '
function move_options(from, dest, add, remove)
{
  if (from.selectedIndex < 0) {
    if (from.options[0] != null)
      from.options[0].selected = true;
    from.focus();
    return false;
  } else {
    for (var i = 0; i<from.length; ++i) {
      if (from.options[i].selected) {
        dest.options[dest.length] = new Option(from.options[i].text, from.options[i].value, false, false);
      }
    }
    for (var i = from.length-1; i>=0; --i) {
      if (from.options[i].selected) {
        from.options[i] = null;
      }
    }
  }
  flagMasterChanged = 1;
  // Enable or disable the buttons
  if (from.options.length > 0) {
    add.disabled = 0;
  } else {
    add.disabled = 1;
  }
  if (dest.options.length > 0) {
    remove.disabled = 0;
  } else {
    remove.disabled = 1;
  }
}

function select_options(element, on_or_off) {
//alert("Name: "+name);
//  element = this.form.elements[name];
//alert("Element: "+element);
  if (element) {
    for (var i = 0; i < element.length; ++i) {
      element.options[i].selected = on_or_off;
    }
  }
}
';
    }


    /**
     * Returns HMTL code for displaying a group of Text fields
     * @param   array   $options    The options array, by reference
     */
    static function getInputTextGroup(&$options=null)
    {
        global $_CORELANG;

        if (empty($options['delete']))
            $options['delete'] = false;
        if (empty($options['add']))
            $options['add'] = false;
        if (empty($options['name']))
            $options['name'] = 'input_text_group_';
        if (empty($options['id']))
            $options['id'] = $options['name'];
        if (empty($options['values']))
            $options['values'] = array();
        // Styling
        if (empty($options['style_input']))
            $options['style_input'] =
                'width:220px;';
        if (empty($options['style_delete']))
            $options['style_delete'] =
                'border:0px;width:17px;height:17px;background-image:url('.
                ASCMS_PATH_OFFSET.
                '/core/Core/View/Media/icons/delete.gif);background-repeat:no-repeat;';
        if (empty($options['style_add']))
            $options['style_add'] = '';
        // Local copy, as this is modified in the subelements below
        $index_tab = ++self::$index_tab;
        $return =
            '<div id="itg_container_'.$index_tab.'"'.
            ' style="clear:both;width:auto;">'."\n";
        $index_max = 0;
        foreach ($options['values'] as $index => $value) {

            $return .=
                '<div>'."\n".
                self::getInputText(
                    $options['name'].'[]', $value,
                    $options['id'].'_'.$index,
                    'style="'.$options['style_input'].'"').
//                "\n".
                ($options['delete']
                  ? self::getInputButton('itg_button_delete_'.$index,
                      '', 'button', false,
                      'onclick="this.parentNode.parentNode.removeChild(this.parentNode);"'.
//                      'alt="'.$_CORELANG['TXT_CORE_HTML_DELETE'].'"'.
                      ' style="'.$options['style_delete'].'"').
                    "\n"
                  : '').
                '</div>'."\n";
            if ($index > $index_max) $index_max = $index;
        }
        $return .=
            '</div>'."\n";

        if ($options['add']) {
DBG::log("Html::getInputTextGroup(): Add");
            $return .=
                '<div style="clear:left;">'."\n".
                self::getInputButton('itg_button_add_'.$index_tab,
                    $_CORELANG['TXT_CORE_HTML_ADD'], 'button', false,
                    'onclick="itg_add_'.$index_tab.'(\'itg_container_'.$index_tab.'\');"'.
//                    ' alt="'.$_CORELANG['TXT_CORE_HTML_ADD'].'"'.
                    ' style="'.$options['style_add'].'"'
                      ).
                "\n".
                '</div>'."\n";
        }
        // This *MUST* be instantiated individually for each group,
        // as the styles and maximum index will vary!
        JS::registerCode('
itg_index_max_'.$index_tab.' = '.$index_max.';
function itg_add_'.$index_tab.'(container_id) {
  d=document.createElement("div");
//  d.style="float:left;";

  i=document.createElement("input");
  i.name="'.$options['name'].'[]";
  i.type="text";
  i.value="";
  i.id="'.$options['id'].'_"+itg_index_max_'.$index_tab.';
  i.setAttribute("style", "'.$options['style_input'].'");

  d.appendChild(i);

  d.appendChild(document.createTextNode("\n"));'.

  ($options['delete']
    ? '
  b=document.createElement("input");
  b.name="itg_button_delete_"+itg_index_max_'.$index_tab.';
  b.type="button";
//  b.alt="'.$_CORELANG['TXT_CORE_HTML_DELETE'].'";
// No see (but work):
  b.onclick=function(){this.parentNode.parentNode.removeChild(this.parentNode);}
// No work, no see:
//  b.onclick="this.parentNode.parentNode.removeChild(this.parentNode);";
  b.setAttribute("style", "'.$options['style_delete'].'");
  d.appendChild(b);'
  : '').'

  c=document.getElementById(container_id);
  c.appendChild(d);
}
');


        return $return;
    }


    /**
     * Builds a raty element
     *
     * This element lets you display a customizable star rating.
     * Uses and activates jQuery.
     * The selector will be passed to jQuery to select the element(s) to
     * which the rating is applied (some <div> or similar will do, but that
     * element must exist).
     * The options array may contain any keys that are valid raty
     * options, and their respective values.
     * See {@see http://plugins.jquery.com/project/raty} for details.
     * As of writing this (20110228), valid options and their respective
     * defaults are:
     *  cancelHint:   'cancel this rating!'
     *  cancelOff:    'cancel-off.png'
     *  cancelOn:     'cancel-on.png'
     *  cancelPlace:  'left'
     *  click:        null
     *  half:         false
     *  hintList:     ['bad', 'poor', 'regular', 'good', 'gorgeous']
     *  iconRange:    []
     *  noRatedMsg:   'not rated yet'
     *  number:       5
     *  path:         'img/'
     *  readOnly:     false
     *  scoreName:    'score'
     *  showCancel:   false
     *  starHalf:     'star-half.png'
     *  starOff:      'star-off.png'
     *  starOn:       'star-on.png'
     *  start:        0
     * Note that you *MUST* specify boolean option values as true or false,
     * *NOT* as string literals "true" or "false".  The latter will not work.
     * @link    http://plugins.jquery.com/project/raty
     * @param   string    $selector   The element selector
     * @param   array     $options    The optional raty options
     * @return  boolean               True on success, false otherwise
     */
    static function makeRating($selector, $options=null)
    {
// TODO: This loop is always the same for all jQuery stuff; create a method
        $strOptions = '';
        foreach ($options as $option => $value) {
//DBG::log("Html::makeRating(): Option $option => value $value");
            $strOptions .=
                "\n    ".$option.': '.
                (is_numeric($value)
                  ? $value
                  : (is_bool($value)
                      ? ($value ? 'true' : 'false')
                      : (   preg_match('/^[\[\{].*[\]\{]$/', $value) // array or object
                         || preg_match('/^\(?function/', $value)     // function
                          ? $value
                          :'"'.$value.'"'))). // plain string, unquoted!
                ",";
        }
//DBG::log("Html::makeRating($selector, [options]): Options: $strOptions");
        JS::activate('raty');
        JS::registerCode('
cx.jQuery(document).ready(function($) {
  $("'.$selector.'").raty({'.$strOptions."\n  });\n});");
        return true;
    }


    /**
     * Returns HTML code to represent some status with a colored LED image
     *
     * Colors currently available include green, yellow, and red.
     * For unknown colors, the empty string is returned.
     * The $alt parameter value is added as the images' alt attribute value,
     * if non-empty.
     * The $action parameter may include URI parameters to be inserted in the
     * href attribute of a link, which is added if $action is non-empty.
     * @param   string    $color      The LED color
     * @param   string    $alt        The optional alt attribute for the image
     * @param   string    $action     The optional action URI parameters
     * @return  string                The LED HTML code on success, the
     *                                empty string otherwise
     */
    static function getLed($status='', $alt='', $action=''): string {
        $path = '';
        switch ($status) {
          case 'green':
            $path = self::ICON_STATUS_GREEN;
            break;
          case 'yellow':
            $path = self::ICON_STATUS_YELLOW;
            break;
          case 'red':
            $path = self::ICON_STATUS_RED;
            break;
        }
        if (empty($path)) {
            return '';
        }
        $led_html = self::getImageByPath(
            $path,
            'height="13 width="13" border="0"' . ($alt ? ' alt="'.$alt.'" title="'.$alt.'"' : '')
        );
        if ($action) {
            $uri = self::getRelativeUri_entities();
            self::replaceUriParameter($uri, $action);
            $led_html = '<a href="'.$uri.'">'.$led_html.'</a>';
        }
        return $led_html;
    }


    /**
     * Returns HTML code for either a checked or unchecked icon
     * for indicating yes/no status
     *
     * For the time being, the unchecked status is represented by
     * an empty space, aka pixel.gif
     * @param   boolean   $status     If true, the checked box is returned,
     *                                the unchecked otherwise
     * @return  string                The HTML code with the checkbox icon
     * @todo    There should be an unchecked icon other than "pixel.gif"
     */
    static function getCheckmark($status=''): string {
        return self::getImageByPath(
            $status ? self::ICON_STATUS_CHECKED : self::ICON_STATUS_UNCHECKED,
            'border="0" height="16" width="16"'
        );
    }


    /**
     * Returns HTML code for a link
     *
     * If either $uri or $text is empty, returns the empty string.
     * @param   string    $uri          The target address
     * @param   array     $text         The link text
     * @param   string    $target       The optional target window
     * @param   string    $attribute    Additional optional attributes
     * @return  string                  The HTML code for the element
     * @author  Reto Kohli <reto.kohli@comvation.com>
     */
    static function getLink($uri, $text, $target=null, $attribute=null)
    {
//DBG::log("getLink($uri, $text, $target, $attribute): Entered");
        if ($uri == '' || $text == '') return '';
        return
            '<a href="'.$uri.'"'.
            ' tabindex="'.++self::$index_tab.'"'.
            ($target ? ' target="'.$target.'"' : '').
            ($attribute ? ' '.$attribute : '').
            '>'.$text.'</a>';
    }

    /**
     * Returns the page URI with all special characters substituted
     * by their corresponding HTML entities
     *
     * The URI contains neither the host nor the directory,
     * but only the script file name and query string.
     * //including the current CSRF parameter.
     * This is ready for use in any href or action attribute.
     * Apply html_entity_decode() before using it with javascript.
     * @see     getRelativeUri()
     * @return  string                  The URI with entities
     */
    static function getRelativeUri_entities()
    {
        return contrexx_raw2xhtml(self::getRelativeUri());
    }


    /**
     * Returns the page URI
     *
     * The URI contains neither the host nor the directory,
     * but only the script file name and query string.
     * //including the current CSRF parameter.
     * This is ready for use with javascript.
     * Apply contrexx_raw2xhtml() before using it in any href or action attribute.
     * @see     getRelativeUri_entities()
     * @return  string                  The URI
     */
    static function getRelativeUri()
    {
        // returns the relative uri from url request object
        $cx = \Cx\Core\Core\Controller\Cx::instanciate();
        return (string) clone $cx->getRequest()->getUrl();
    }


    /**
     * Remove the parameter and its value from the URI string,
     * by reference
     *
     * If the parameter cannot be found, the URI is left unchanged.
     * Note that this expects and produces URIs in the form as returned by
     * {@see getRelativeUri_entities()}.  It operates on a URI without
     * entities, too, but the result will probably not be correct.
     * @param   string    $uri              The URI, by reference
     * @param   string    $parameter_name   The name of the parameter
     * @return  string                      The former parameter value,
     *                                      or the empty string
     */
    static function stripUriParam(&$uri, $parameter_name)
    {
        $match = array();
//DBG::log("Html::stripUriParam(".contrexx_raw2xhtml($uri).", ".contrexx_raw2xhtml($parameter_name)."): Entered");

        // Match the parameter *WITH* equal sign and value (possibly empty)
        $uri = preg_match_replace(
            '/(?<=\?|\&amp;|\&(?!amp;))'.
            preg_quote($parameter_name, '/').'\=([^&]*)'.
            '(?:\&amp;|\&(?!amp;)|$)/',
            '', $uri, $match
        );
        // Match the parameter *WITHOUT* equal sign and value
        $uri = preg_match_replace(
            '/(?<=\?|\&amp;|\&(?!amp;))'.
            preg_quote($parameter_name, '/').
            '(?:\&amp;|\&(?!amp;)|$)/',
            '$1', $uri
        );
//echo("Html::stripUriParam(".contrexx_raw2xhtml($uri).", ".contrexx_raw2xhtml($parameter_name)."): regex ".contrexx_raw2xhtml($re)."<br />");
        // Remove trailing '?', '&', or '&amp;'.
        // At least one of those will be left over when the last parameter
        // was removed from the URI!
        $uri = preg_replace('/(?:\?|\&amp;|\&(?!amp;))*$/', '', $uri);
//echo("Html::stripUriParam(".contrexx_raw2xhtml($uri).", ".contrexx_raw2xhtml($parameter_name)."): stripped $count times ".var_export($match, true)."<br />");
        if (empty($match[1])) return '';
        return $match[1];
    }


    /**
     * Replaces the URI parameters given in the URI, by reference
     *
     * Parameters whose names are present in the URI already are replaced
     * with the new values from the $parameter string.
     * Parameters from $parameter that are not already present in the URI
     * are appended.
     * The replaced/added parameters are separated by '&amp;'.
     * Note that this expects and produces URIs in the form as returned by
     * {@see getRelativeUri_entities()}.  It operates on a URI without
     * entities, too, but the result will probably not be correct.
     * @param   string    $uri        The full URI, by reference
     * @param   string    $parameter  The parameters to be replaced or added
     * @return  void
     */
    static function replaceUriParameter(&$uri, $parameter)
    {
//DBG::log("Html::replaceUriParameter($uri, $parameter): Entered");
        $match = array();
        // Remove script name and leading question mark, if any
        if (preg_match('/^.*\?(.+)$/', $parameter, $match)) {
//        if (preg_match('/^(.*)\?(.+)$/', $parameter, $match)) {
//            $bogus_index = $match[1];
            $parameter = $match[1];
//DBG::log("Html::replaceUriParameter(): Split parameter in bogus index /$bogus_index/ and parameter /".contrexx_raw2xhtml($parameter)."/");
        }
        $arrParts = preg_split('/\&(?:amp;)?/', $parameter, -1, PREG_SPLIT_NO_EMPTY);

//DBG::log("Html::replaceUriParameter(): parts: ".var_export($arrParts, true));
        foreach ($arrParts as $parameter) {
//DBG::log("Html::replaceUriParameter(): processing parameter ".contrexx_raw2xhtml($parameter));

            if (!preg_match('/^([^=]+)\=?(.*)$/', $parameter, $match)) {
//DBG::log("Html::replaceUriParameter(): skipped illegal parameter ".contrexx_raw2xhtml($parameter));
                continue;
            }
            self::stripUriParam($uri, $match[1]);
//            $old = self::stripUriParam($uri, $match[1]);
//DBG::log("Html::replaceUriParameter(): stripped to $uri, removed $old");
            $uri .=
                (preg_match('/\?/', $uri) ? '&amp;' : '?').
                $parameter;
//DBG::log("Html::replaceUriParameter(): added to $uri");
        }
//        $uri = ($index ? $index.'?' : '&amp;').$uri;
//DBG::log("Html::replaceUriParameter($uri, $parameter): Exiting");
//die();
    }


    /**
     * Shortens text and appends an optional link to "more...".
     * Mind: the returned string is contrexx_raw2xhtml()d for the current charset!
     * @param   string    $text         The text
     * @param   integer   $max_length   The maximum length
     * @param   string    $more_uri     The optional URI
     * @param   string    $more_uri     The optional target window
     * @return  string                  The shortened text, with the
     *                                  optional link appended
     */
    static function shortenText(
        $text, $max_length=30, $more_uri=null, $more_target=null
    ) {
        global $_CORELANG;

        $text = preg_replace('/(?:\r|\n|\s\s+)/s', ' ', $text);
        if (strlen($text) > $max_length) {
            $text = preg_replace(
                '/^(.{0,'.$max_length.'})(?:\s|$).+$/is',
                '$1',
                $text
            );
// TODO: I should probably wrap this in contrexx_raw2xhtml()
            $text = sprintf(
                $_CORELANG['TXT_CORE_HTML_ETC'],
                contrexx_raw2xhtml($text));
        }
        return
            $text.
            ($more_uri
                ? '&nbsp;<a href="'.$more_uri.'"'.
                  ($more_target ? ' target="'.$more_target.'"' : '').
                  '>'.$_CORELANG['TXT_CORE_HTML_MORE'].'</a>'
                : ''
            );
    }

    /**
     * Generates code for ContentManager style language state icons
     *
     * For $languageStates you may supply an array in one of these to forms:
     *
     * $languageStates = array(
     *      {language id} => 'active','inactive','inexistent',
     * )
     *
     * $languageStates = array(
     *      {language id} => array(
     *          'active' => {bool},
     *          'page' => {page id or object},
     *      ),
     * )
     *
     * The latter will be resolved to the first form. The two forms can be mixed.
     *
     * For $link, supply a hyperlink, that may contain %1$d and %2$s which will be
     * replaced with the language ID and code.
     *
     * @param   array   $languageStates Language states to get icons for
     * @param   string  $link           Hyperlink for language icons
     * @return  string                  The HTML code for the elements
     */
    public static function getLanguageIcons(&$languageStates, $link) {
        // resolve second to first form
        foreach ($languageStates as $langId=>$state) {
            if (is_array($state)) {
                if (is_object($state['page'])) {
                    $languageStates[$langId] = $state['active'] ? 'active' : 'inactive';
                } else {
                    $em = \Env::get('cx')->getDb()->getEntityManager();
                    $pageRepo = $em->getRepository('Cx\Core\ContentManager\Model\Entity\Page');
                    $page = $pageRepo->findOneById($state['page']);
                    if (!$page) {
                        $languageStates[$langId] = 'inexistent';
                    } else {
                        $languageStates[$langId] = $state['active'] ? 'active' : 'inactive';
                    }
                }
            }
        }

        // parse icons
        $content = '<div class="language-icons">';
        foreach (\FWLanguage::getActiveFrontendLanguages() as $language) {
            if (isset($languageStates[$language['id']])) {
                $state = $languageStates[$language['id']];
            } else {
                $state = 'inactive';
            }
            $parsedLink = sprintf($link, $language['id'], $language['lang']);
            $content .= self::getLanguageIcon($language['id'], $state, $parsedLink, strtoupper($language['lang']));
        }
        return $content . '</div>';
    }

    /**
     * Returns a single language icon
     * @param   int     $languageId     Language ID
     * @param   string  $state          One of active,inactive,inexistent
     * @param   string  $languageLabel  (optional) Label for the icon, default is uppercase language code
     * @return  string                  The HTML code for the elements
     */
    public static function getLanguageIcon($languageId, $state, $link, $languageLabel = '') {
        if (empty($languageLabel)) {
            $languageLabel = strtoupper(\FWLanguage::getLanguageCodeById($languageId));
        }
        return '<div class="language-icon ' . \FWLanguage::getLanguageCodeById($languageId) . ' ' . $state . '">
            <a href="' . $link . '">
                ' . $languageLabel . '
            </a>
        </div>';
    }
}
