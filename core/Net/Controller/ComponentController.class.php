<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * Main controller for Net
 *
 * @copyright   Cloudrexx AG
 * @author      Project Team SS4U <info@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  core_net
 */

namespace Cx\Core\Net\Controller;

/**
 * Main controller for Net
 *
 * @copyright   Cloudrexx AG
 * @author      Project Team SS4U <info@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  core_net
 */
class ComponentController extends \Cx\Core\Core\Model\Entity\SystemComponentController {
    /**
     * Holds an instanciated copy of the DomainRepository
     *
     * @var \Cx\Core\Net\Model\Repository\DomainRepository
     */
    protected $domainRepo = null;

    /**
     * Public IP-address of this instance
     *
     * @var string
     */
    protected $publicIpAddress = '';

    public function getControllerClasses() {
        // Return an empty array here to let the component handler know that there
        // does not exist a backend, nor a frontend controller of this component.
        return array();
    }

    /**
     * {@inheritdoc}
     */
    public function getCommandsForCommandMode() {
        return array(
            'Net' => new \Cx\Core_Modules\Access\Model\Entity\Permission(
                array(),
                array('cli'),
                false
            ),
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getCommandDescription($command, $short = false) {
        switch ($command) {
            case $this->getName():
                if ($short) {
                    return 'Allows (un-)mapping of domains';
                }
                return $this->getCommandDescription($command, true) . '

    Usage:
        ./cx ' . $this->getName() . ' list [--idn]
        ./cx ' . $this->getName() . ' [un]map DOMAIN [--set-as-main]
        ./cx ' . $this->getName() . ' dns flush';
                break;

            default:
                return '';
        }
    }

    /**
     * {@inheritdoc}
     *
     * @todo Net model operations should be dropped and instead be provided
     *     through the data api.
     */
    public function executeCommand($command, $arguments, $dataArguments = array()) {
        switch ($command) {
            case $this->getName():
                if (count($arguments) < 1) {
                    throw new \Exception('Not enough arguments');
                }
                $operation = array_shift($arguments);
                if ($operation === 'dns') {
                    if (empty($arguments[0])) {
                        throw new \Exception('Not enough arguments');
                    }
                    if ($arguments[0] !== 'flush') {
                        throw new \Exception(
                            'Unknown operation: ' . $operation . ' ' . $arguments[0]
                        );
                    }
                    $this->getComponent('Cache')->dropCachefile($this, 'DNS');
                    echo 'Done ' . PHP_EOL;
                    break;
                }
                $domainRepo = $this->cx->getDb()->getEntityManager()->getRepository(
                    \Cx\Core\Net\Model\Entity\Domain::class
                );
                if ($operation === 'list') {
                    $asIdn = false;
                    if (
                        !empty($arguments[0])
                        && $arguments[0] === '--idn'
                    ) {
                        $asIdn = true;
                    }
                    $domains = $domainRepo->findAll();
                    foreach ($domains as $domain) {
                        if ($asIdn) {
                            echo $domain->getName();
                        } else {
                            echo \Cx\Core\Net\Controller\ComponentController::convertIdnToUtf8Format(
                                $domain->getName()
                            );
                        }
                        if ($domain->getId() === $this->cx->getConfig()->getBaseConfig('mainDomainId')) {
                            echo ' [main domain]';
                        }
                        echo PHP_EOL;
                    }
                    break;
                }
                if (!$arguments) {
                    throw new \Exception('Not enough arguments');
                }
                $domainNameFromInput = array_shift($arguments);
                if (!preg_match(\Cx\Core\Net\Model\Entity\Domain::NAME_VALIDATION_REGEXP, $domainNameFromInput)) {
                    throw new \Exception('Invalid domain name: ' . $domainNameFromInput);
                }
                $domainName = \Cx\Core\Net\Controller\ComponentController::convertIdnToAsciiFormat(
                    $domainNameFromInput
                );
                if (empty($domainName)) {
                    throw new \Exception('Invalid domain name: ' . $domainNameFromInput);
                }
                switch ($operation) {
                    case 'map':
                        $domain = new \Cx\Core\Net\Model\Entity\Domain($domainName);
                        $domainRepo->add($domain);
                        $domainRepo->flush();
                        if (
                            !$arguments
                            || $arguments[0] !== '--set-as-main'
                        ) {
                            break;
                        }
                        \Cx\Core\Setting\Controller\Setting::set('mainDomainId', $domain->getId());
                        if (\Cx\Core\Setting\Controller\Setting::update('mainDomainId')) {
                            echo $domainName . ' set as new main domain' . PHP_EOL;
                        }
                        break;

                    case 'unmap':
                        $domain = $domainRepo->findOneBy(['name' => $domainName]);
                        if (!$domain) {
                            throw new \Exception('Unknown domain ' . $domainName);
                        }
                        if ($domain->getId() === $this->cx->getConfig()->getBaseConfig('mainDomainId')) {
                            \Cx\Core\Setting\Controller\Setting::set('mainDomainId', 0);
                            if (!\Cx\Core\Setting\Controller\Setting::update('mainDomainId')) {
                                throw new \Exception(
                                    'Failed to reset main domain. Removal aborted.'
                                );
                            }
                        }
                        $domainRepo->remove($domain);
                        $domainRepo->flush();
                        break;

                    default:
                        throw new \Exception('Unknown operation: ' . $operation);
                }
                echo 'Done ' . PHP_EOL;
                break;
        }
    }

    public function preInit(\Cx\Core\Core\Controller\Cx $cx) {
        global $_CONFIG;
        $this->domainRepo = new \Cx\Core\Net\Model\Repository\DomainRepository();
        $_CONFIG['domainUrl'] = $this->domainRepo->getMainDomain()->getName();
        \Env::set('config', $_CONFIG);
    }

    /**
     * {@inheritdoc}
     */
    public function postResolve(\Cx\Core\ContentManager\Model\Entity\Page $page) {
        if ($this->cx->getMode() != \Cx\Core\Core\Controller\Cx::MODE_BACKEND) {
            return;
        }
        $domainRepo = $this->cx->getComponent('Net')->getDomainRepository();
        $objTemplate = $this->cx->getTemplate();
        $objTemplate->setVariable(
            'MAIN_DOMAIN',
            $domainRepo->getMainDomain()->getName()
        );
    }

    /**
     * Get repository of domains
     *
     * @return  \Cx\Core\Net\Model\Repository\DomainRepository The domain
     *              repository of this website.
     */
    public function getDomainRepository() {
        return $this->domainRepo;
    }

    /**
     * Get the hostname of the website. This is usually the name of the
     * virtual host the website is running on.
     *
     * @return  string The hostname of the website
     */
    public function getHostname() {
        return $this->domainRepo->findOneBy(array('id' => 0))->getName();
    }

    /**
     * Convert idn to ascii Format
     *
     * @todo This method should do validation and throw an error if $name is not valid
     * @param string $name
     *
     * @return string
     */
    public static function convertIdnToAsciiFormat($name) {
        if (empty($name)) {
            return;
        }

        if (!function_exists('idn_to_ascii')) {
            \DBG::msg('Idn is not supported in this system.');
        } else {
            // Test if UTS #46 (http://unicode.org/reports/tr46/) is available.
            // Important: PHP7.2 has deprecated any other use than UTS #46.
            // Therefore after PHP7.2, Cloudrexx requires ICU 4.6 or newer
            // as minimum system requirement
            if (defined('INTL_IDNA_VARIANT_UTS46')) {
                $ascii = idn_to_ascii($name, IDNA_DEFAULT, INTL_IDNA_VARIANT_UTS46);
            } else {
                $ascii = idn_to_ascii($name);
            }

            // check if conversion was successful
            if (!empty($ascii)) {
                // in case the INTL extension is misconfigured on
                // the server, then the return value of idn_to_ascii()
                // will be empty. in that case let's return the
                // original domain's name
                $name = $ascii;
            }
        }

        return $name;
    }

    /**
     * Convert idn to utf8 format
     *
     * @param string $name
     *
     * @return string
     */
    public static function convertIdnToUtf8Format($name) {
        if (empty($name)) {
            return;
        }

        if (!function_exists('idn_to_utf8')) {
            \DBG::msg('Idn is not supported in this system.');
        } else {
            // Test if UTS #46 (http://unicode.org/reports/tr46/) is available.
            // Important: PHP7.2 has deprecated any other use than UTS #46.
            // Therefore after PHP7.2, Cloudrexx requires ICU 4.6 or newer
            // as minimum system requirement
            if (defined('INTL_IDNA_VARIANT_UTS46')) {
                $utf8 = idn_to_utf8($name, IDNA_DEFAULT, INTL_IDNA_VARIANT_UTS46);
            } else {
                $utf8 = idn_to_utf8($name);
            }

            // check if conversion was successful
            if (!empty($utf8)) {
                // in case the INTL extension is misconfigured on
                // the server, then the return value of idn_to_utf8()
                // will be empty. in that case let's return the
                // original domain's name
                $name = $utf8;
            }
        }

        return $name;
    }

    /**
     * Get Host by IP address
     *
     * @param string $ip IP address
     *
     * @return string
     */
    public function getHostByAddr($ip)
    {
        $dnsHostnameLookup = \Cx\Core\Setting\Controller\Setting::getValue(
            'dnsHostnameLookup',
            'Config'
        );
        if ($dnsHostnameLookup != 'on') {
            return $ip;
        }

        return gethostbyaddr($ip);
    }

    /**
     * Gets the response for a DNS query which might be authoritative, if possible from cache
     *
     * A single record has the following keys:
     * array(
     *     'domain' => $domain,
     *     'type' => $type,
     *     'authoritative' => $authoritative,
     *     'ttl' => <ttl>,
     *     'value' => <value>,
     *     'cachedAt' => <cachedAt>,
     *     'validUntil' => <validUntil>,
     * )
     * <ttl> is the actual TTL of the record for authoritative queries, otherwise
     * it's the TTL of the queried DNS server.
     * <value> is the value of the record as returned by DNS.
     * <cachedAt> is only set if the result comes from cache and contains a
     * \DateTime object with the time the cache was created.
     * <validUntil> is only set if the result comes from cache and contains a
     * \DateTime object with the time the cache becomes invalid.
     *
     * Additional fields may be set per record type.
     *
     * This is the caching abstraction layer of DNS resolving.
     * See {@see executeDnsQueryAuthoritative} and {@see executeDnsQuery}.
     *
     * @todo Implement all record types
     * @param string $domain Domain name to query (e.g. "example.org")
     * @param string $type Result type (e.g. "A" or "TXT")
     * @param bool $authoritative (optional) If set to true an authoritative result is returned
     * @param bool $forceAuthoritative (optional) If set to true along with
     *     $authoritative, then an exception will be thrown if nameservers
     *     could not be resolved for authoritative results.
     * @param bool $forceCacheRenewal (optional) If set to true result is neverrloaded from cache
     * @throws \Exception If $authoritative and $forceAuthoritative are both
     *     set to `true`, then if nameservers could not be resolved for
     *     authoritative results
     * @throws \Exception If a $type is requested that is not yet implemented
     * @return array List of matching records. See description.
     */
    public function getDnsResponse(string $domain, string $type, bool $authoritative = false, bool $forceAuthoritative = false, bool $forceCacheRenewal = false): array {
        $cache = $this->getComponent('Cache');
        if ($cache && !$forceCacheRenewal) {
            $response = null;
            try {
                $response = $cache->getCachefileRecord(
                    $this,
                    'DNS',
                    array(
                        'domain' => $domain,
                        'type' => $type,
                        'authoritative' => $authoritative,
                    )
                );
            } catch (\Exception $e) {}
            if ($response) {
                if (isset($response['exceptionClass']) && isset($response['exceptionMessage'])) {
                    \DBG::dumpExtendedLog(DBG_ALL ^ DBG_DB);
                    throw new $response['exceptionClass']($response['exceptionMessage']);
                }
                if (!count($response) || empty($response['records'])) {
                    return array();
                }
                $baseRecordData = $response;
                unset($baseRecordData['records']);
                $ret = array();
                foreach ($response['records'] as $record) {
                    $ret[] = $baseRecordData + $record;
                }
                return $ret;
            }
        }
        $additionalUsedRecords = array();
        $shortestTtl = PHP_INT_MAX;
        $exception = null;
        try {
            $result = $this->executeDnsQueryAuthoritative(
                $domain,
                $type,
                $authoritative,
                $forceAuthoritative,
                $additionalUsedRecords
            );
            foreach (($result + $additionalUsedRecords) as $record) {
                if ($record['ttl'] < $shortestTtl) {
                    $shortestTtl = $record['ttl'];
                }
            }
        } catch (\Exception $e) {
            $exception = $e;
            $result = array();
        }
        if ($cache) {
            $cacheResult = array(
                'domain' => $domain,
                'type' => $type,
                'authoritative' => $authoritative,
            );
            if (!count($result)) {
                $shortestTtl = 86400;
                try {
                    $soaResult = $this->executeDnsQueryAuthoritative(
                        $domain,
                        'SOA',
                        true,
                        $forceAuthoritative
                    );
                    if (count($soaResult)) {
                        $shortestTtl = current($soaResult)['ttl'];
                        if (current($soaResult)['minimum'] < $shortestTtl) {
                            $shortestTtl = current($soaResult)['minimum'];
                        }
                    }
                } catch (\Exception $e) {}
            }
            if ($exception) {
                $cacheResult += array(
                    'exceptionClass' => get_class($exception),
                    'exceptionMessage' => $exception->getMessage(),
                );
            } else {
                $cacheResult += array(
                    'records' => array_map(
                        function($record) {
                            unset($record['domain']);
                            unset($record['type']);
                            unset($record['authorative']);
                            return $record;
                        },
                        $result
                    ),
                );
            }
            $cacheResult['validUntil'] = new \DateTime('+' . $shortestTtl . ' seconds');
            $cache->addCachefileRecords(
                $this,
                'DNS',
                array('domain', 'type', 'authoritative'),
                array($cacheResult)
            );
        }
        if ($exception) {
            throw $exception;
        }
        return $result;
    }

    /**
     * Gets the response for a DNS query which might be authoritative
     *
     * A single record has the following keys:
     * array(
     *     'domain' => $domain,
     *     'type' => $type,
     *     'authoritative' => $authoritative,
     *     'ttl' => <ttl>,
     *     'value' => <value>,
     * )
     * <ttl> is the actual TTL of the record for authoritative queries, otherwise
     * it's the TTL of the queried DNS server.
     * <value> is the value of the record as returned by DNS.
     *
     * Additional fields may be set per record type.
     *
     * This is the recursion abstraction layer of DNS resolving.
     * See {@see getDnsResponse} and {@see executeDnsQuery}.
     *
     * @todo Implement all record types
     * @param string $domain Domain name to query (e.g. "example.org")
     * @param string $type Result type (e.g. "A" or "TXT")
     * @param bool $authoritative (optional) If set to true an authoritative result is returned
     * @param bool $forceAuthoritative (optional) If set to true along with
     *     $authoritative, then an exception will be thrown if nameservers
     *     could not be resolved for authoritative results.
     * @param array $additionalUsedRecords (optional) Reference which will be filled with additional records that were used during resolving
     * @throws \Exception If $authoritative and $forceAuthoritative are both
     *     set to `true`, then if nameservers could not be resolved for
     *     authoritative results
     * @throws \Exception If a $type is requested that is not yet implemented
     * @return array List of matching records. See description.
     */
    protected function executeDnsQueryAuthoritative(string $domain, string $type, bool $authoritative = false, bool $forceAuthoritative = false, array &$additionalUsedRecords = array()): array {
        $additionalUsedRecords = array();
        $nameservers = array();
        if ($authoritative) {
            $lookupDomain = $domain;
            do {
                foreach ($this->executeDnsQuery($lookupDomain, 'NS') as $ns) {
                    $nsResponse = $this->executeDnsQuery($ns['value'], 'A');
                    if (!count($nsResponse)) {
                        continue;
                    }
                    $nameservers[] = current($nsResponse)['value'];
                    $additionalUsedRecords[] = $ns;
                }
                $lookupDomain = substr($lookupDomain, strpos($lookupDomain, '.') + 1);
            } while (
                empty($nameservers) && substr_count($lookupDomain, '.')
            );
            if (!count($nameservers) && $forceAuthoritative) {
                \DBG::dumpExtendedLog(DBG_ALL ^ DBG_DB);
                throw new \Exception('Could not resolve nameservers for ' . $domain);
            }
        }
        $result = $this->executeDnsQuery($domain, $type, $nameservers);
        foreach ($result as &$record) {
            $record['authoritative'] = $authoritative;
        }
        return $result;
    }

    /**
     * Gets the response for a DNS query
     *
     * A single record has the following keys:
     * array(
     *     'domain' => $domain,
     *     'type' => $type,
     *     'ttl' => <ttl>,
     *     'value' => <value>,
     * )
     * <ttl> is the actual TTL of the record for authoritative queries, otherwise
     * it's the TTL of the queried DNS server.
     * <value> is the value of the record as returned by DNS.
     *
     * Additional fields may be set per record type.
     *
     * This is the library abstraction layer of DNS resolving.
     * See {@see executeDnsQuery} and {@see executeDnsQueryAuthoritative}.
     *
     * @todo Implement all record types
     * @param string $domain Domain name to query (e.g. "example.org")
     * @param string $type Result type (e.g. "A" or "TXT")
     * @param array $nameservers (optional) List of IPs as string to use as nameservers instead of default
     * @throws \Exception If a $type is requested that is not yet implemented
     * @return array List of matching records. See description.
     */
    protected function executeDnsQuery(string $domain, string $type, array $nameservers = array()) {
        if (!count($nameservers)) {
            $nameservers = array($this->cx->getConfig()->getBaseConfig('dnsServer'));
        }
        if (!empty($nameservers)) {
            $options = array(
                'nameservers' => $nameservers,
            );
        }
        $dnsResolver = new \Net_DNS2_Resolver($options);
        $dnsResolver->use_tcp = true; // TODO: ?
        try {
            \DBG::debug('Performing uncached DNS query: "' . $domain . ' IN ' . $type . ' using nameserver(s):');
            \DBG::dump($nameservers, DBG_DEBUG);
            $rawResult = $dnsResolver->query($domain, $type);
        } catch (\Net_DNS2_Exception $e) {
            \DBG::dump($e->getMessage(), DBG_DEBUG);
            \DBG::dump('Raw response from ' . $e->getResponse()->answer_from . ':', DBG_DEBUG);
            \DBG::dump((string)$e->getResponse(), DBG_DEBUG);
            \DBG::dumpExtendedLog(DBG_ALL ^ DBG_DB);
            return array();
        }
        $result = array();
        foreach ($rawResult->answer as $record) {
            if ($record->type !== $type) {
                continue;
            }
            $value = '';
            $recordResult = array(
                'domain' => $domain,
                'type' => $type,
                'ttl' => $record->ttl,
            );
            $additionalValues = array();
            switch ($record->type) {
                case 'NS':
                    $value = $record->nsdname;
                    break;
                case 'TXT':
                    $value = implode("\n", $record->text);
                    break;
                case 'A':
                    $value = $record->address;
                    break;
                case 'SOA':
                    $additionalValues = array(
                        'mname' => $record->mname,
                        'rname' => $record->rname,
                        'serial' => $record->serial,
                        'refresh' => $record->refresh,
                        'retry' => $record->retry,
                        'expire' => $record->expire,
                        'minimum' => $record->minimum,
                    );
                    $value = implode(' ', $additionalValues);
                    break;
                case 'CNAME':
                    $value = $record->cname;
                    break;
                default:
                    throw new \Exception('Handling for DNS type "' . $type . '" is not yet implemented');
                    break;
            }
            $recordResult['value'] = $value;
            $recordResult += $additionalValues;
            $result[] = $recordResult;
        }
        \DBG::debug('Fetched record:');
        \DBG::dump($result, DBG_DEBUG);
        return $result;
    }

    /**
     * @inheritDoc
     */
    public function postInit(\Cx\Core\Core\Controller\Cx $cx) {
        $this->initPublicIpAddress();
    }

    /**
     * Initializes the public IP-address of this instance
     */
    protected function initPublicIpAddress(): void {
        \Cx\Core\Setting\Controller\Setting::init($this->getName());
        $publicIpAddress = \Cx\Core\Setting\Controller\Setting::getValue(
            'publicIpAddress',
             $this->getName()
        );
        if (!$publicIpAddress) {
            return;
        }
        $this->setPublicIpAddress($publicIpAddress);
    }

    /**
     * Get the public IP-address of this instance
     *
     * @return string Public IP-address
     */
    public function getPublicIpAddress(): string {
        return $this->publicIpAddress;
    }

    /**
     * Set the public IP-address of this instance
     *
     * @param string $ipAddress Public IP-address
     */
    public function setPublicIpAddress(string $ipAddress): void {
        $this->publicIpAddress = $ipAddress;
    }
}
