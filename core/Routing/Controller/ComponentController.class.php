<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * Main controller for Routing
 *
 * @copyright   Cloudrexx AG
 * @author      Project Team SS4U <info@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  core_routing
 */

namespace Cx\Core\Routing\Controller;

/**
 * Main controller for Routing
 *
 * @copyright   Cloudrexx AG
 * @author      Project Team SS4U <info@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  core_routing
 */
class ComponentController extends \Cx\Core\Core\Model\Entity\SystemComponentController {

    /**
     * Note: As development environments (cx env config --mode=development) have
     * pm.max_children (of php-fpm) set to 5 by default (through env var
     * CLX_FPM_CONFIG_PM_MAX_CHILDREN) a value greater than 3 would result in
     * breaching pm.max_children and will cause an request dead-lock.
     * @var int Max amount of nested internal redirects.
     */
    protected const INTERNAL_REDIRECT_RECURSION_LIMIT = 3;

    public function getControllerClasses() {
        // Return an empty array here to let the component handler know that there
        // does not exist a backend, nor a frontend controller of this component.
        return array('Backend', 'EsiWidget');
    }

    /**
     * {@inheritdoc}
     */
    public function getControllersAccessableByJson() {
        return array(
            'EsiWidgetController',
        );
    }

    /**
     * {@inheritdoc}
     */
    public function postInit(\Cx\Core\Core\Controller\Cx $cx)
    {
        $widgetController = $this->getComponent('Widget');
        foreach (
            array(
                'PAGE_ID',
                'PAGE_NODE_ID',
            ) as $widgetName
        ) {
            $widgetController->registerWidget(
                new \Cx\Core_Modules\Widget\Model\Entity\EsiWidget(
                    $this,
                    $widgetName
                )
            );
        }
    }

    /**
     * @param \Cx\Core\Routing\Url $url URL instance of the current request.
     * @throws \Cx\Core\Core\Controller\InstanceException When a RewriteRule
     *     has machted.
     * @todo Implement protocol and domain adjustments as rewrite rules. See CLX-5700.
     */
    public function resolveCustomRedirects(\Cx\Core\Routing\Url $url): void {
        if ($this->cx->getMode() != \Cx\Core\Core\Controller\Cx::MODE_FRONTEND) {
            return;
        }

        // apply any existing rewrite rules
        $em = $this->cx->getDb()->getEntityManager();
        $rewriteRuleRepo = $em->getRepository($this->getNamespace() . '\\Model\\Entity\\RewriteRule');
        $rewriteRules = $rewriteRuleRepo->findBy(
            array('active' => true),
            array('orderNo' => 'asc')
        );
        $continue = true;
        $originalUrl = clone $url;
        foreach ($rewriteRules as $rewriteRule) {
            try {
                $url = $rewriteRule->resolve($url, $continue);
            } catch (\Exception $e) {
                \DBG::msg('RewriteRule error for rewrite rule #'. $rewriteRule->getId());
                \DBG::msg($e->getMessage());
                // This is thrown if the regex of the rule is not valid
            }
            if (!$continue) {
                break;
            }
        }

        // abort in case no rewriting has been done
        if ($originalUrl->toString() == $url->toString()) {
            return;
        }

        // detect infinite loops like /foo(/bar)+
        $iterationPoint = strrpos(
            $url->getPath(),
            $originalUrl->getPath()
        );
        // if $iterationPoint is neither 'false' nor '0', then
        // this means that $originalUrl->getPath() is found in
        // $url->getPath() and is a potential infinite loop
        if ($iterationPoint) {
            $redundancy = substr($url->getPath(), 0, $iterationPoint);
            if (substr_count($url->getPath(), $redundancy) > 2) {
                \DBG::msg('Potential infinite loop detected');
                \DBG::msg('Abort resolving');
                \header($_SERVER['SERVER_PROTOCOL'] . ' 502 Bad Gateway');
                // remove CSRF token
                output_reset_rewrite_vars();
                throw new \Cx\Core\Core\Controller\InstanceException();
            }
        }

        // execute external (301/302) redirection (and cache it)
        if (
            $rewriteRule->getRewriteStatusCode() !=
            \Cx\Core\Routing\Model\Entity\RewriteRule::REDIRECTION_TYPE_INTERN
        ) {
            if ($url->isInternal()) {
                // note: this will end request processing by throwing a
                // \Cx\Core\Core\Controller\InstanceException
                $this->cx->adjustRequest(
                    $url,
                    $rewriteRule->getRewriteStatusCode()
                );
            }
            \DBG::log('Rewrite request (using HTTP ' . $rewriteRule->getRewriteStatusCode() . ') to: ' . $url->toString());
            $this->cx->getResponse()->setCode($rewriteRule->getRewriteStatusCode());
            $this->cx->getResponse()->setHeader('Location', $url->toString());
            $this->cx->getResponse()->setParsedContent('');

            // TODO: drop call to http_response_code() along with the manual hack
            // on $headers once CLX-5687 has been implemented.
            http_response_code($this->cx->getResponse()->getCode());
            $headers = $this->cx->getResponse()->getHeaders();
            $headers[
                \Cx\Core_Modules\Cache\Controller\Cache::HTTP_STATUS_CODE_HEADER
            ] = $this->cx->getResponse()->getCode();
            $this->getComponent('Cache')->writeCacheFileForRequest(
                null,
                $headers,
                $this->cx->getResponse()->getParsedContent()
            );
            // this will throw \Cx\Core\Core\Controller\InstanceException
            $this->cx->getResponse()->send();
        }

        // process internal sub-request
        try {
            if ($this->cx->getRequest()->getHttpRequestMethod() != 'get') {
                // throws InstanceException
                $this->handleInternalRedirectError(
                    'Internal rewrite rule #' .  $rewriteRule->getId() .  ' triggered on non-GET request.',
                    $this->cx->getRequest()
                );
            }
            if (!$url->isInternal()) {
                // throws InstanceException
                $this->handleInternalRedirectError(
                    'Internal rewrite rule #' .  $rewriteRule->getId() .  ' did not point to internal target.',
                    $this->cx->getRequest()
                );
            }
            $recursionLevel = 0;
            $requestHeaders = $this->cx->getRequest()->getHeaders();
            if (isset($requestHeaders['X_CLX_REDIRECT'])) {
                $recursionLevel = (int) $requestHeaders['X_CLX_REDIRECT'];
                $recursionLevel++;
                if ($recursionLevel > static::INTERNAL_REDIRECT_RECURSION_LIMIT) {
                    // throws InstanceException
                    $this->handleInternalRedirectError(
                        'Internal rewrite rule #' .  $rewriteRule->getId() .  ' reached recursion depth limit of ' . static::INTERNAL_REDIRECT_RECURSION_LIMIT . '.',
                        $this->cx->getRequest()
                    );
                }
            }
            \DBG::log('Fetching content from ' . str_replace(' ', '%20', $url->toString()));
            $request = new \HTTP_Request2(str_replace(' ', '%20', $url->toString()), \HTTP_Request2::METHOD_GET);
            $request->setHeader('x-clx-redirect', $recursionLevel);
            $request->setConfig(array(
                // disable ssl peer verification
                'ssl_verify_host' => false,
                'ssl_verify_peer' => false,
                // follow HTTP redirect
                'follow_redirects' => true,
                // resend original request to new location
                'strict_redirects' => true,
            ));
            $response = $request->send();
            $content = $response->getBody();
            foreach ($response->getHeader() as $key=>$value) {
                if (in_array($key, array(
                    'content-encoding',
                    'transfer-encoding',
                ))) {
                    continue;
                }
                $this->cx->getResponse()->setHeader($key, $value);
            }
            $this->cx->getResponse()->setCode($response->getStatus());
            $this->cx->getResponse()->setParsedContent($content);

            // TODO: drop call to http_response_code() along with the manual hack
            // on $headers once CLX-5687 has been implemented.
            http_response_code($this->cx->getResponse()->getCode());
            $headers = $this->cx->getResponse()->getHeaders();
            $headers[
                \Cx\Core_Modules\Cache\Controller\Cache::HTTP_STATUS_CODE_HEADER
            ] = $response->getStatus();
            $this->getComponent('Cache')->writeCacheFileForRequest(
                null,
                $headers,
                $this->cx->getResponse()->getParsedContent()
            );
            // this will throw \Cx\Core\Core\Controller\InstanceException
            $this->cx->getResponse()->send();
        } catch (\HTTP_Request2_Exception $e) {
            \DBG::dump($e);
        }
    }

    /**
     * Adds SysLog entry and sends adequate response on internal redirect error
     *
     * @param string $msg Message for the SysLog entry
     * @param \Cx\Core\Routing\Model\Entity\Request $request Original request to add to the SysLog
     * @throws \Cx\Core\Core\Controller\InstanceException Ends this request
     */
    protected function handleInternalRedirectError(
        string $msg,
        \Cx\Core\Routing\Model\Entity\Request $request
    ): void {
        $this->cx->getEvents()->triggerEvent(
            'SysLog/Add',
            array(
                'severity' => 'WARNING',
                'message' => 'Internal redirect error',
                'data' => $msg . PHP_EOL . 'Request was ' . strtoupper(
                    $request->getHttpRequestMethod()
                ) . ' ' . $request->getUrl()->toString(),
            )
        );
        $response = $this->cx->getResponse();
        $response->setCode(502);
        $response->setParsedContent('');
        // TODO: drop call to http_response_code() along with the manual hack
        // on $headers once CLX-5687 has been implemented.
        http_response_code($response->getCode());
        // remove CSRF token
        output_reset_rewrite_vars();
        $response->send();
    }

    /**
     * Do something after resolving is done
     *
     * USE CAREFULLY, DO NOT DO ANYTHING COSTLY HERE!
     * CALCULATE YOUR STUFF AS LATE AS POSSIBLE
     * @param \Cx\Core\ContentManager\Model\Entity\Page $page       The resolved page
     */
    public function postResolve(\Cx\Core\ContentManager\Model\Entity\Page $page)
    {
        // TODO: The registration of widgets must not be done here.
        //       Instead the registration must be done in postInit hook.

        // Initialize value of FinalStringWidget CANONICAL_LINK as empty
        // string for the case when the requested page does not have a
        // canonical-link.
        $link = '';

        // NOTE: code has been moved to preFinalize()
        return;

        // fetch canonical-link
        $headers = \Env::get('Resolver')->getHeaders();
        if (
            isset($headers['Link']) &&
            preg_match('/^<([^>]+)>;\s+rel="canonical"/', $headers['Link'], $matches)
        ) {
            $canonicalLink = $matches[1];
            
            $link = new \Cx\Core\Html\Model\Entity\HtmlElement('link');
            $link->setAttribute('rel', 'canonical');
            $link->setAttribute('href', $canonicalLink);
        }

        // TODO: Once each componet will have implemented a proper resolve hook
        //       the CANONICAL_LINK widget shall be converted into an EsiWidget.
        $this->getComponent('Widget')->registerWidget(
            new \Cx\Core_Modules\Widget\Model\Entity\FinalStringWidget(
                $this,
                'CANONICAL_LINK',
                (string) $link
            )
        );
    }

    /**
     * Do something before main template gets parsed
     *
     * @param \Cx\Core\Html\Sigma                       $template   The main template
     */
    public function preFinalize(\Cx\Core\Html\Sigma $template) {
        // TODO: note: the registration of the CANONICAL_LINK widget must be moved
        //       to the preInit() hook. Currently, it has to be located in the preFinalize()
        //       hook to make it work. The reason is, that Response::header('Link') is set
        //       in adjustResponse() hook.

        // Initialize value of FinalStringWidget CANONICAL_LINK as empty
        // string for the case when the requested page does not have a
        // canonical-link.
        $link = '';

        // fetch canonical-link
        try {
            // TODO: CLX-2483 rollout: remove 
            $response = $this->cx->getResponse();
            $headers = $response->getHeaders();
            $linkHeader = '';
            if (isset($headers['Link'])) {
                $linkHeader = $headers['Link'];
            } else {
                $headers = \Env::get('Resolver')->getHeaders();
                if (isset($headers['Link'])) {
                    $linkHeader = $headers['Link'];
                }
            }

            if (!preg_match('/^<([^>]+)>;\s+rel="canonical"/', $linkHeader, $matches)) {
                throw new \Exception('no canonical-link header set');
            }

            $canonicalLink = $matches[1];
            $link = new \Cx\Core\Html\Model\Entity\HtmlElement('link');
            $link->setAttribute('rel', 'canonical');
            $link->setAttribute('href', $canonicalLink);
            // TODO: CLX-2483 rollout: add
            //$link = $this->getComponent('ContentManager')->fetchAlreadySetCanonicalLink($this->cx->getResponse());
            // TODO: CLX-2483 rollout: DONE
        } catch (\Exception $e) {
            \DBG::msg($e->getMessage());
            // no Link header set -> page doesn't have a canonical-link
        }

        // TODO: Once each component will have implemented a proper resolve hook
        //       the CANONICAL_LINK widget shall be converted into an EsiWidget.
        $this->getComponent('Widget')->registerWidget(
            new \Cx\Core_Modules\Widget\Model\Entity\FinalStringWidget(
                $this,
                'CANONICAL_LINK',
                (string) $link
            )
        );
    }
}
