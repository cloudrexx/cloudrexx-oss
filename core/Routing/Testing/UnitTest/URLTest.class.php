<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * ResolverTest
 *
 * @copyright   CLOUDREXX CMS - CLOUDREXX AG
 * @author      Cloudrexx Development Team <info@cloudrexx.com>
 * @author      SS4U <ss4u.comvation@gmail.com>
 * @version     1.0.0
 * @package     cloudrexx
 * @subpackage  core_resolver
 */

namespace Cx\Core\Routing\Testing\UnitTest;
use Cx\Core\Routing\Url as Url;

/**
 * ResolverTest
 *
 * @copyright   CLOUDREXX CMS - CLOUDREXX AG
 * @author      Cloudrexx Development Team <info@cloudrexx.com>
 * @author      SS4U <ss4u.comvation@gmail.com>
 * @version     1.0.0
 * @package     cloudrexx
 * @subpackage  core_resolver
 */
class URLTest extends \Cx\Core\Test\Model\Entity\DoctrineTestCase {
    const PORT_FRONTEND_HTTP = 80;
    const PORT_BACKEND_HTTP = 8080;
    const PORT_FRONTEND_HTTPS = 443;
    const PORT_BACKEND_HTTPS = 8443;

    protected $defaultPorts = [];
    protected $systemPorts = [];
    protected $hostname = '';

    /**
     * Prevent the default behavior of starting a new transaction
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        \Cx\Core\Setting\Controller\Setting::init(
            'Config',
            null,
            'Yaml',
            null,
            \Cx\Core\Setting\Controller\Setting::REPOPULATE
        );
        $this->defaultPorts = [
            'http' => [
                'frontend' => static::PORT_FRONTEND_HTTP,
                'backend' => static::PORT_FRONTEND_HTTPS,
            ],
            'https' => [
                'frontend' => static::PORT_BACKEND_HTTP,
                'backend' => static::PORT_BACKEND_HTTPS,
            ],
        ];
        \Cx\Core\Setting\Controller\Setting::set(
            'portFrontendHTTP',
            $this->defaultPorts['http']['frontend']
        );
        \Cx\Core\Setting\Controller\Setting::set(
            'portFrontendHTTPS',
            $this->defaultPorts['https']['frontend']
        );
        \Cx\Core\Setting\Controller\Setting::set(
            'portBackendHTTP',
            $this->defaultPorts['http']['backend']
        );
        \Cx\Core\Setting\Controller\Setting::set(
            'portBackendHTTPS',
            $this->defaultPorts['https']['backend']
        );
        \Cx\Core\Setting\Controller\Setting::set(
            'forceProtocolFrontend',
            'none',
        );
        \Cx\Core\Setting\Controller\Setting::set(
            'forceProtocolBackend',
            'none',
        );
        \Cx\Core\Setting\Controller\Setting::set(
            'useVirtualLanguageDirectories',
            'on'
        );

        $this->systemPorts = [
            'http' => getservbyname('http', 'tcp'),
            'https' => getservbyname('https', 'tcp'),
        ];

        $cx = \Cx\Core\Core\Controller\Cx::instanciate();
        $this->hostname = $cx->getComponent('Net')->getHostname();

        # add just one locale 'en'
        $language = self::$em->getRepository(
            \Cx\Core\Locale\Model\Entity\Language::class
        )->findOneBy(['iso1' => 'en']);
        $newLocale = new \Cx\Core\Locale\Model\Entity\Locale();
        $newLocale->setIso1($language);
        $newLocale->setLabel('English');
        $newLocale->setOrderNo(1);
        $newLocale->setSourceLanguage($language);
        self::$em->persist($newLocale);
        self::$em->flush();
        # remove all existing locales
        do {
            $didCleanup = false;
            $locales = self::$em->getRepository(
                \Cx\Core\Locale\Model\Entity\Locale::class
            )->findAll();
            foreach ($locales as $locale) {
                if ($locale === $newLocale) {
                    continue;
                }
                if (
                    self::$em->getRepository(
                        \Cx\Core\Locale\Model\Entity\Locale::class
                    )->findOneBy(['fallback' => $locale])
                ) {
                    continue;
                }
                self::$em->remove($locale);
                self::$em->flush();
                $didCleanup = true;
            }
        } while ($didCleanup);
        # flush locale to legacy locale wrapper
        \FWLanguage::init();
    }

    /**
     * Prevent the default behavior of terminating the transaction
     * @return void
     */
    public function tearDown(): void
    {
        \Cx\Core\Setting\Controller\Setting::init(
            'Config',
            null,
            'Yaml',
            null,
            \Cx\Core\Setting\Controller\Setting::REPOPULATE
        );
        parent::tearDown();
    }

    public function testDomainAndPath() {
        $url = new Url('http://' . $this->hostname . '/');
        $this->assertEquals($this->hostname, $url->getDomain());
        $this->assertEquals('', $url->getPath());
        $this->assertEquals('', $url->getPathPart());
        $this->assertEquals(true, $url->isInternal());
        $this->assertEquals(true, $url->isFrontend());
        $this->assertEquals(false, $url->isBackend());

        $url = new Url('http://' . $this->hostname . '/Test');
        $this->assertEquals($this->hostname, $url->getDomain());
        $this->assertEquals('Test', $url->getPath());
        $this->assertEquals('/Test', $url->getPathPart());
        $this->assertEquals(true, $url->isInternal());
        $this->assertEquals(true, $url->isFrontend());
        $this->assertEquals(false, $url->isBackend());

        $url = new Url('http://' . $this->hostname . '/Second/Test/?a=asfd');
        $this->assertEquals($this->hostname, $url->getDomain());
        $this->assertEquals('Second/Test/?a=asfd', $url->getPath());
        $this->assertEquals('/Second/Test/', $url->getPathPart());
        $this->assertEquals(true, $url->isInternal());
        $this->assertEquals(true, $url->isFrontend());
        $this->assertEquals(false, $url->isBackend());

        $url = new Url('http://' . $this->hostname . '/cadmin');
        $this->assertEquals($this->hostname, $url->getDomain());
        $this->assertEquals('cadmin', $url->getPath());
        $this->assertEquals('/cadmin', $url->getPathPart());
        $this->assertEquals(true, $url->isInternal());
        $this->assertEquals(false, $url->isFrontend());
        $this->assertEquals(true, $url->isBackend());

        $url = new Url('http://' . $this->hostname . '/cadmin/Home');
        $this->assertEquals($this->hostname, $url->getDomain());
        $this->assertEquals('cadmin/Home', $url->getPath());
        $this->assertEquals('/cadmin/Home', $url->getPathPart());
        $this->assertEquals(true, $url->isInternal());
        $this->assertEquals(false, $url->isFrontend());
        $this->assertEquals(true, $url->isBackend());

        $url = new Url('http://' . $this->hostname . '/api');
        $this->assertEquals($this->hostname, $url->getDomain());
        $this->assertEquals('api', $url->getPath());
        $this->assertEquals('/api', $url->getPathPart());
        $this->assertEquals(true, $url->isInternal());
        $this->assertEquals(false, $url->isFrontend());
        $this->assertEquals(true, $url->isBackend());

        $url = new Url('http://' . $this->hostname . '/cadmin/Home?foo=bar');
        $this->assertEquals($this->hostname, $url->getDomain());
        $this->assertEquals('cadmin/Home?foo=bar', $url->getPath());
        $this->assertEquals('/cadmin/Home', $url->getPathPart());
        $this->assertEquals(true, $url->isInternal());
        $this->assertEquals(false, $url->isFrontend());
        $this->assertEquals(true, $url->isBackend());

        $url = new Url('http://invalid.example.org/');
        $this->assertEquals('invalid.example.org', $url->getDomain());
        $this->assertEquals('', $url->getPath());
        $this->assertEquals('', $url->getPathPart());
        $this->assertEquals(false, $url->isInternal());
        $this->assertEquals(false, $url->isFrontend());
        $this->assertEquals(false, $url->isBackend());

        $url = new Url('http://invalid.example.org/cadmin/');
        $this->assertEquals('invalid.example.org', $url->getDomain());
        $this->assertEquals('cadmin/', $url->getPath());
        $this->assertEquals('/cadmin/', $url->getPathPart());
        $this->assertEquals(false, $url->isInternal());
        $this->assertEquals(false, $url->isFrontend());
        $this->assertEquals(false, $url->isBackend());

        $this->assertEquals(false, $url->isRouted());
    }

    public function testSuggestions() {
        $url = new Url('http://' . $this->hostname . '/Test');
        $this->assertEquals('Test', $url->getSuggestedTargetPath());
        $this->assertEquals('', $url->getSuggestedParams());

        $url = new Url('http://' . $this->hostname . '/Test?foo=bar');
        $this->assertEquals('Test', $url->getSuggestedTargetPath());
        $this->assertEquals('?foo=bar', $url->getSuggestedParams());
    }

    public function testFallbackToSystemPorts() {
        $protocol = 'http';
        $mode = 'frontend';
        $url = new Url($protocol . '://' . $this->hostname);
        $this->assertEquals($protocol, $url->getProtocol());
        $this->assertEquals(true, $url->isFrontend());
        $this->assertEquals(false, $url->isBackend());
        $this->assertEquals($this->systemPorts[$protocol], $url->getPort());

        $protocol = 'https';
        $mode = 'frontend';
        $url = new Url($protocol . '://' . $this->hostname);
        $this->assertEquals($protocol, $url->getProtocol());
        $this->assertEquals(true, $url->isFrontend());
        $this->assertEquals(false, $url->isBackend());
        $this->assertEquals($this->systemPorts[$protocol], $url->getPort());

        $protocol = 'http';
        $mode = 'backend';
        $url = new Url($protocol . '://' . $this->hostname . '/cadmin');
        $this->assertEquals($protocol, $url->getProtocol());
        $this->assertEquals(false, $url->isFrontend());
        $this->assertEquals(true, $url->isBackend());
        $this->assertEquals($this->systemPorts[$protocol], $url->getPort());

        $protocol = 'https';
        $mode = 'backend';
        $url = new Url($protocol . '://' . $this->hostname . '/cadmin');
        $this->assertEquals($protocol, $url->getProtocol());
        $this->assertEquals(false, $url->isFrontend());
        $this->assertEquals(true, $url->isBackend());
        $this->assertEquals($this->systemPorts[$protocol], $url->getPort());

        $protocol = 'http';
        $mode = 'backend';
        $url = new Url($protocol . '://' . $this->hostname . '/api');
        $this->assertEquals($protocol, $url->getProtocol());
        $this->assertEquals(false, $url->isFrontend());
        $this->assertEquals(true, $url->isBackend());
        $this->assertEquals($this->systemPorts[$protocol], $url->getPort());

        $protocol = 'https';
        $mode = 'backend';
        $url = new Url($protocol . '://' . $this->hostname . '/api');
        $this->assertEquals($protocol, $url->getProtocol());
        $this->assertEquals(false, $url->isFrontend());
        $this->assertEquals(true, $url->isBackend());
        $this->assertEquals($this->systemPorts[$protocol], $url->getPort());
    }

    public function testSetDefaultPorts() {
        $protocol = 'http';
        $mode = 'frontend';
        $url = new Url($protocol . '://' . $this->hostname, true);
        $this->assertEquals(true, $url->isInternal());
        $this->assertEquals($protocol, $url->getProtocol());
        $this->assertEquals(true, $url->isFrontend());
        $this->assertEquals(false, $url->isBackend());
        $this->assertEquals($this->defaultPorts[$protocol][$mode], $url->getPort());

        $protocol = 'https';
        $mode = 'frontend';
        $url = new Url($protocol . '://' . $this->hostname, true);
        $this->assertEquals($protocol, $url->getProtocol());
        $this->assertEquals(true, $url->isFrontend());
        $this->assertEquals(false, $url->isBackend());
        $this->assertEquals($this->defaultPorts[$protocol][$mode], $url->getPort());

        $protocol = 'http';
        $mode = 'backend';
        $url = new Url($protocol . '://' . $this->hostname . '/cadmin', true);
        $this->assertEquals($protocol, $url->getProtocol());
        $this->assertEquals(false, $url->isFrontend());
        $this->assertEquals(true, $url->isBackend());
        $this->assertEquals($this->defaultPorts[$protocol][$mode], $url->getPort());

        $protocol = 'https';
        $mode = 'backend';
        $url = new Url($protocol . '://' . $this->hostname . '/cadmin', true);
        $this->assertEquals($protocol, $url->getProtocol());
        $this->assertEquals(false, $url->isFrontend());
        $this->assertEquals(true, $url->isBackend());
        $this->assertEquals($this->defaultPorts[$protocol][$mode], $url->getPort());

        $protocol = 'http';
        $mode = 'backend';
        $url = new Url($protocol . '://' . $this->hostname . '/api', true);
        $this->assertEquals($protocol, $url->getProtocol());
        $this->assertEquals(false, $url->isFrontend());
        $this->assertEquals(true, $url->isBackend());
        $this->assertEquals($this->defaultPorts[$protocol][$mode], $url->getPort());

        $protocol = 'https';
        $mode = 'backend';
        $url = new Url($protocol . '://' . $this->hostname . '/api', true);
        $this->assertEquals($protocol, $url->getProtocol());
        $this->assertEquals(false, $url->isFrontend());
        $this->assertEquals(true, $url->isBackend());
        $this->assertEquals($this->defaultPorts[$protocol][$mode], $url->getPort());
    }

    public function testReplacePortsByDefaultPorts() {
        $protocol = 'http';
        $mode = 'frontend';
        $url = new Url($protocol . '://' . $this->hostname . ':' . ($this->defaultPorts[$protocol][$mode] + 1), true);
        $this->assertEquals($protocol, $url->getProtocol());
        $this->assertEquals(true, $url->isFrontend());
        $this->assertEquals(false, $url->isBackend());
        $this->assertEquals($this->defaultPorts[$protocol][$mode], $url->getPort());

        $protocol = 'https';
        $mode = 'frontend';
        $url = new Url($protocol . '://' . $this->hostname . ':' . ($this->defaultPorts[$protocol][$mode] + 1), true);
        $this->assertEquals($protocol, $url->getProtocol());
        $this->assertEquals(true, $url->isFrontend());
        $this->assertEquals(false, $url->isBackend());
        $this->assertEquals($this->defaultPorts[$protocol][$mode], $url->getPort());

        $protocol = 'http';
        $mode = 'backend';
        $url = new Url($protocol . '://' . $this->hostname . ':' . ($this->defaultPorts[$protocol][$mode] + 1) . '/cadmin', true);
        $this->assertEquals($protocol, $url->getProtocol());
        $this->assertEquals(false, $url->isFrontend());
        $this->assertEquals(true, $url->isBackend());
        $this->assertEquals($this->defaultPorts[$protocol][$mode], $url->getPort());

        $protocol = 'https';
        $mode = 'backend';
        $url = new Url($protocol . '://' . $this->hostname . ':' . ($this->defaultPorts[$protocol][$mode] + 1) . '/cadmin', true);
        $this->assertEquals($protocol, $url->getProtocol());
        $this->assertEquals(false, $url->isFrontend());
        $this->assertEquals(true, $url->isBackend());
        $this->assertEquals($this->defaultPorts[$protocol][$mode], $url->getPort());

        $protocol = 'http';
        $mode = 'backend';
        $url = new Url($protocol . '://' . $this->hostname . ':' . ($this->defaultPorts[$protocol][$mode] + 1) . '/api', true);
        $this->assertEquals($protocol, $url->getProtocol());
        $this->assertEquals(false, $url->isFrontend());
        $this->assertEquals(true, $url->isBackend());
        $this->assertEquals($this->defaultPorts[$protocol][$mode], $url->getPort());

        $protocol = 'https';
        $mode = 'backend';
        $url = new Url($protocol . '://' . $this->hostname . ':' . ($this->defaultPorts[$protocol][$mode] + 1) . '/api', true);
        $this->assertEquals($protocol, $url->getProtocol());
        $this->assertEquals(false, $url->isFrontend());
        $this->assertEquals(true, $url->isBackend());
        $this->assertEquals($this->defaultPorts[$protocol][$mode], $url->getPort());
    }

    public function testSetPortsBasedOnForcedFrontendProtocol() {
        $protocol = 'http';
        \Cx\Core\Setting\Controller\Setting::set('forceProtocolFrontend', $protocol);


        $mode = 'frontend';
        $url = new Url($protocol . '://' . $this->hostname . ':' . ($this->defaultPorts[$protocol][$mode] + 1), true);
        $this->assertEquals($protocol, $url->getProtocol());
        $this->assertEquals(true, $url->isFrontend());
        $this->assertEquals(false, $url->isBackend());
        $this->assertEquals($this->defaultPorts[$protocol][$mode], $url->getPort());

        $url = new Url($protocol . '://' . $this->hostname . ':' . ($this->defaultPorts[$protocol][$mode] + 1));
        $this->assertEquals($protocol, $url->getProtocol());
        $this->assertEquals(true, $url->isFrontend());
        $this->assertEquals(false, $url->isBackend());
        $this->assertEquals($this->systemPorts[$protocol], $url->getPort());

        $url = new Url('https://' . $this->hostname . ':' . ($this->defaultPorts[$protocol][$mode] + 1), true);
        $this->assertEquals($protocol, $url->getProtocol());
        $this->assertEquals(true, $url->isFrontend());
        $this->assertEquals(false, $url->isBackend());
        $this->assertEquals($this->defaultPorts[$protocol][$mode], $url->getPort());

        $url = new Url('https://' . $this->hostname . ':' . ($this->defaultPorts[$protocol][$mode] + 1));
        $this->assertEquals($protocol, $url->getProtocol());
        $this->assertEquals(true, $url->isFrontend());
        $this->assertEquals(false, $url->isBackend());
        $this->assertEquals($this->systemPorts[$protocol], $url->getPort());

        $protocol = 'https';
        \Cx\Core\Setting\Controller\Setting::set('forceProtocolFrontend', $protocol);
        $url = new Url('http://' . $this->hostname . ':' . ($this->defaultPorts[$protocol][$mode] + 1), true);
        $this->assertEquals($protocol, $url->getProtocol());
        $this->assertEquals(true, $url->isFrontend());
        $this->assertEquals(false, $url->isBackend());
        $this->assertEquals($this->defaultPorts[$protocol][$mode], $url->getPort());

        $url = new Url('http://' . $this->hostname . ':' . ($this->defaultPorts[$protocol][$mode] + 1));
        $this->assertEquals($protocol, $url->getProtocol());
        $this->assertEquals(true, $url->isFrontend());
        $this->assertEquals(false, $url->isBackend());
        $this->assertEquals($this->systemPorts[$protocol], $url->getPort());

        $url = new Url($protocol . '://' . $this->hostname . ':' . ($this->defaultPorts[$protocol][$mode] + 1), true);
        $this->assertEquals($protocol, $url->getProtocol());
        $this->assertEquals(true, $url->isFrontend());
        $this->assertEquals(false, $url->isBackend());
        $this->assertEquals($this->defaultPorts[$protocol][$mode], $url->getPort());

        $url = new Url($protocol . '://' . $this->hostname . ':' . ($this->defaultPorts[$protocol][$mode] + 1));
        $this->assertEquals($protocol, $url->getProtocol());
        $this->assertEquals(true, $url->isFrontend());
        $this->assertEquals(false, $url->isBackend());
        $this->assertEquals($this->systemPorts[$protocol], $url->getPort());
    }

    /**
     * Test handling of file: protocol
     *          Expected: 'file:///var/www/html'
     *          Actual:   'file://cloudrexx-bit.lvh.me/var/www/html'
     */
    public function testFileUrls() {
        $testResult = 'file://' . getcwd();

        $url = Url::fromRequest();
        $this->assertEquals($testResult, $url->toString());
        $this->assertEquals(false, $url->isFrontend());
        $this->assertEquals(false, $url->isBackend());
        $this->assertEquals(false, $url->isInternal());

        $url = Url::fromMagic($testResult);
        $this->assertEquals($testResult, $url->toString());
        $this->assertEquals(false, $url->isFrontend());
        $this->assertEquals(false, $url->isBackend());
        $this->assertEquals(false, $url->isInternal());
    }

    public function testLanguageTagInUrls() {
        $tests = [
            /**
             * external URLs
             */
            [
                'url' => 'https://externalurl.example.org/foo/bar',
                'use_lang' => false,
                'lang' => '',
                'path' => 'foo/bar',
            ],
            [
                'url' => 'https://externalurl.example.org/foo/bar',
                'use_lang' => true,
                'lang' => '',
                'path' => 'foo/bar',
            ],
            [
                'url' => 'https://externalurl.example.org/en/foo/bar',
                'use_lang' => false,
                'lang' => '',
                'path' => 'en/foo/bar',
            ],
            [
                'url' => 'https://externalurl.example.org/en/foo/bar',
                'use_lang' => true,
                'lang' => '',
                'path' => 'en/foo/bar',
            ],
            /**
             *  internal URLs
             */
            // frontend
            [
                'url' => 'https://' . $this->hostname . '/en/foo/bar',
                'use_lang' => false,
                'lang' => '',
                'path' => 'en/foo/bar',
            ],
            [
                'url' => 'https://' . $this->hostname . '/en/foo/bar',
                'use_lang' => true,
                'lang' => 'en',
                'path' => 'foo/bar',
            ],
            [
                'url' => 'https://' . $this->hostname . '/foo/bar',
                'use_lang' => false,
                'lang' => '',
                'path' => 'foo/bar',
            ],
            [
                'url' => 'https://' . $this->hostname . '/foo/bar',
                'use_lang' => true,
                'lang' => '',
                'path' => 'foo/bar',
            ],
            // backend / api
            [
                'url' => 'https://' . $this->hostname . '/cadmin',
                'use_lang' => false,
                'lang' => '',
                'path' => 'cadmin',
            ],
            [
                'url' => 'https://' . $this->hostname . '/cadmin',
                'use_lang' => true,
                'lang' => '',
                'path' => 'cadmin',
            ],
            [
                'url' => 'https://' . $this->hostname . '/api',
                'use_lang' => false,
                'lang' => '',
                'path' => 'api',
            ],
            [
                'url' => 'https://' . $this->hostname . '/api',
                'use_lang' => true,
                'lang' => '',
                'path' => 'api',
            ],
        ];
        foreach ($tests as $test) {
            \Cx\Core\Setting\Controller\Setting::set(
                'useVirtualLanguageDirectories',
                $test['use_lang'] ? 'on' : 'off'
            );
            $url = Url::fromMagic($test['url']);
            \DBG::dump(\Cx\Core\Setting\Controller\Setting::getValue(
                'useVirtualLanguageDirectories'
            ));
            \DBG::dump($test);
            \DBG::dump($url);
            $this->assertEquals($test['url'], $url->toString());
            $this->assertEquals($test['lang'], $url->getLangDir(true));
            $this->assertEquals($test['path'], $url->getPath());
        }
    }
}
