<?php
declare(strict_types=1);
/**
 * Cloudrexx
 *
 * @link      https://www.cloudrexx.com
 * @copyright Cloudrexx AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

// Act Variables
$_ARRAYLANG['TXT_CORE_ROUTING'] = 'Redirections';
$_ARRAYLANG['TXT_CORE_ROUTING_ACT_DEFAULT'] = 'Aperçu';
$_ARRAYLANG['TXT_CORE_ROUTING_ACT_REDIRECT'] = 'Redirections';

$_ARRAYLANG['TXT_CORE_ROUTING_ACT_REWRITERULE'] = 'Règles de transfert';
$_ARRAYLANG['id'] = 'ID';
$_ARRAYLANG['active'] = 'Actif';
$_ARRAYLANG['regularExpression'] = 'Expression régulière';
$_ARRAYLANG['TXT_CORE_ROUTING_REGULAR_EXPRESSION_TOOLTIP'] = '<a href="https://fr.wikipedia.org/wiki/Expression_r%C3%A9guli%C3%A8re" target="_blank">Expression régulière</a> utilisée pour réécrire l"adresse de la requête vers l"adresse de destination. Exemple: <b>#/shop\.example\.com/#/example.com/de/Shop/#</b>.';
$_ARRAYLANG['orderNo'] = 'Numéro de tri';
$_ARRAYLANG['rewriteStatusCode'] = 'Code d"état HTTP de la redirection';
$_ARRAYLANG['TXT_CORE_ROUTING_REWRITE_STATUS_CODE_TOOLTIP'] = 'Choisissez 302 si l"adresse de la demande est toujours valide, sinon 301. <a href="https://de.wikipedia.org/wiki/HTTP-Statuscode#3xx_.E2.80.93_Umleitung" target="_blank">Plus d informations</a><br />Sélectionnez "Interne" si vous souhaitez conserver l"adresse de la requête dans la barre d"adresse du navigateur.';
$_ARRAYLANG['continueOnMatch'] = 'Continuer après avoir touché';
$_ARRAYLANG['TXT_CORE_ROUTING_CONTINUE_ON_MATCH_TOOLTIP'] = 'Si cette règle s"applique, les règles suivantes (le cas échéant) doivent-elles quand même être modifiées?';

$_ARRAYLANG['TXT_CORE_ROUTING_TITLE'] = 'Règles de transfert';
$_ARRAYLANG['TXT_CORE_ROUTING_INTRODUCTION'] = 'Cette fonction permet de rediriger les requêtes vers le site Internet de manière globale. Par exemple, il est possible de rediriger les requêtes d"un autre domaine vers une sous-page. Les options de configuration individuelles sont expliquées ci-dessous:<br><br>Les règles consistent en une <a href="https://fr.wikipedia.org/wiki/Expression_r%C3%A9guli%C3%A8re" target="_blank">Expression régulière </a>. Ceci est utilisé pour une simple recherche/remplacement sur l"URL de la demande. Les règles individuelles sont traitées les unes après les autres, triées par numéro de tri. Si l"une des règles s"applique et que "Continuer après hit" est réglé sur "Non", aucune autre règle n"est traitée. Une fois que toutes les règles ont été traitées et qu"au moins une a été appliquée, la redirection est effectuée avec le code d"état HTTP de la dernière règle appliquée. Pour plus d"informations, consultez la <a href="https://support.cloudrexx.com/5000673701" target="_blank">Base de connaissances</a>.';
$_ARRAYLANG['TXT_CORE_ROUTING_ADD_RULE'] = 'Ajouter une règle de transfert';
