<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * Resolver
 *
 * @copyright   CLOUDREXX CMS - CLOUDREXX AG
 * @author      CLOUDREXX Development Team <info@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  core_routing
 */

namespace Cx\Core\Routing;

/**
 * ResolverException
 *
 * @copyright   CLOUDREXX CMS - CLOUDREXX AG
 * @author      CLOUDREXX Development Team <info@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  core_routing
 */
class ResolverException extends \Exception {};

/**
 * Takes an URL and tries to find the Page.
 *
 * @copyright   CLOUDREXX CMS - CLOUDREXX AG
 * @author      CLOUDREXX Development Team <info@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  core_routing
 */
class Resolver {
    protected $em = null;
    protected $url = null;

    /**
     * @var Cx\Core\Routing\Url
     */
    protected $originalUrl = null;

    /**
     * language id.
     * @var integer
     */
    protected $lang = null;

    /**
     * the page we found.
     * @var Cx\Core\ContentManager\Model\Entity\Page
     */
    protected $page = null;

    /**
     * Will be set to the preview page from the Content Manager.
     *
     * @var \Cx\Core\ContentManager\Model\Entity\Page
     */
    protected $previewPage = null;

    /**
     * Will be set to `true` by {@see static::resolvePage()} if the current
     * request is on a page preview from Content Manager.
     *
     * @var boolean
     */
    protected bool $isPreviewPageRequested = false;

    /**
     * Aliaspage if we resolve an alias
     * @var Cx\Core\ContentManager\Model\Entity\Page
     */
    protected $aliaspage = null;

    /**
     * @var Cx\Core\ContentManager\Model\Entity\Page
     */
    protected $urlPage = null;

    /**
     * Doctrine Cx\Core\ContentManager\Model\Repository\PageRepository
     */
    protected $pageRepo = null;

    /**
     * Doctrine NodeRepository
     */
    protected $nodeRepo = null;

    /**
     * Remembers if we've come across a redirection while resolving the URL.
     * This allow to properly redirect.
     * @var boolean
     */
    protected $isRedirection = false;

    /**
     * Maps language ids to fallback language ids.
     * @var array ($languageId => $fallbackLanguageId)
     */
    protected $fallbackLanguages = null;

    /**
     * Contains the resolved module name (if any, empty string if none)
     * @var String
     */
    protected $section = '';

    /**
     * Contains the resolved module command (if any, empty string if none)
     * @var String
     */
    protected $command = '';

    /**
     * Remembers if it's a page preview.
     * @var boolean
     */
    protected $pagePreview = false;

    /**
     * Contains the history id to revert the page to an older version.
     * @var int
     */
    protected $historyId = 0;

    /**
     * Holds the data of the page to preview from the Content Manager
     * @var array
     */
    protected $sessionPage = array();

    protected $forceInternalRedirection;
    protected $logRepo;
    protected $pathOffset;
    protected $path;

    /**
     * List of headers set by resolver
     * @var array
     */
    protected $headers = array();

    /**
     * Passed path after the resolved page's path
     * @var string
     */
    protected $additionalPath = '';

    /**
     * List of pages that have been resolved
     *
     * This is used to detect infinite internal redirection loops
     *
     * @var array
     */
    protected $resolveStack = array();

    /**
     * @param Url $url the url to resolve
     * @param integer $lang the language Id
     * @param $entityManager
     * @param string $pathOffset ASCMS_PATH_OFFSET
     * @param array $fallbackLanguages (languageId => fallbackLanguageId)
     * @param boolean $forceInternalRedirection does not redirect for internal redirections if set to true.
     *                this is used mainly for testing currently.
     *                IMPORTANT: Do insert new parameters before this one if you need to and correct the tests.
     */
    public function __construct($url, $lang, $entityManager, $pathOffset, $fallbackLanguages, $forceInternalRedirection=false) {
        $this->init($url, $lang, $entityManager, $pathOffset, $fallbackLanguages, $forceInternalRedirection);
    }


    /**
     * @param Url $url the url to resolve
     * @param integer $lang the language Id
     * @param $entityManager
     * @param string $pathOffset ASCMS_PATH_OFFSET
     * @param array $fallbackLanguages (languageId => fallbackLanguageId)
     * @param boolean $forceInternalRedirection does not redirect for internal redirections if set to true.
     *                this is used mainly for testing currently.
     *                IMPORTANT: Do insert new parameters before this one if you need to and correct the tests.
     */
    public function init($url, $lang, $entityManager, $pathOffset, $fallbackLanguages, $forceInternalRedirection=false) {
        $this->url = $url;
        $this->originalUrl = clone $url;
        $this->em = $entityManager;
        $this->lang = $lang;
        $this->pathOffset = $pathOffset;
        $this->pageRepo = $this->em->getRepository('Cx\Core\ContentManager\Model\Entity\Page');
        $this->nodeRepo = $this->em->getRepository('Cx\Core\ContentManager\Model\Entity\Node');
        $this->logRepo  = $this->em->getRepository('Cx\Core\Model\Model\Entity\LogEntry');
        $this->forceInternalRedirection = $forceInternalRedirection;
        $this->fallbackLanguages = $fallbackLanguages;
        $this->pagePreview = !empty($_GET['pagePreview']);
        $this->historyId = !empty($_GET['history']) ? $_GET['history'] : 0;

        // load preview page from session (if requested)
        $cx = \Cx\Core\Core\Controller\Cx::instanciate();
        $session = $cx->getComponent('Session');
        $session->getSession($this->pagePreview);
        if (
            $session->isInitialized()
            && $this->pagePreview
        ) {
            $this->sessionPage = !empty($_SESSION['page']) ? $_SESSION['page']->toArray() : array();
        }
    }

    public function resolve() {
        // $this->resolveAlias() also sets $this->page
        $this->aliaspage = $this->resolveAlias();

        $this->lang = \Env::get('init')->getFallbackFrontendLangId();

        if ($this->aliaspage != null) {
            $targetLang = $this->aliaspage->getTargetLangId();
            if ($targetLang) {
                $this->lang = $targetLang;
            }
            $this->aliaspage = clone $this->aliaspage;
            $this->aliaspage->setVirtual(true);
        } else {
            // if the current URL points to a file:
            // In case the URL is a file (ends with a file-ending) we can end the request
            // as if this file existed it would have been served by apache.
            // TODO: The check for the virtual lang dir is wrong as url->getLangDir(true)
            //          will return true even if no lang dir is present in the request URL.
            //          We need to check for lang dir as the file may be part of an
            //          "additional path".
            if (
                empty($this->url->getLangDir(true)) &&
                preg_match('/^[^?]*\.[a-z0-9]{2,4}$/', $this->url->toString())
            ) {
                if (
                    \Cx\Core\Core\Controller\Cx::instanciate()
                        ->getComponent('Session')
                        ->isInitialized()
                ) {
                    \Cx\Core\Core\Controller\Cx::instanciate()
                        ->getComponent('Session')->getSession()->discard();
                }
                global $url;
                $_GET['id'] = 404;
                try {
                    $this->url = \Cx\Core\Routing\Url::fromModuleAndCmd('Error', '', \FWLanguage::getDefaultLangId(), array(), '', false);
                } catch (UrlException $e) {
                    \DBG::msg($e->getMessage());
                    $cx = \Cx\Core\Core\Controller\Cx::instanciate();
                    $response = $cx->getResponse();
                    if ($response) {
                        $response->setCode(502);
                    }
                    http_response_code(502);
                    // remove CSRF token
                    output_reset_rewrite_vars();
                    throw new \Cx\Core\Core\Controller\InstanceException();
                }
                $url = $this->url;
            }

            //try to find the language in the url
            $extractedLanguage = \FWLanguage::getLanguageIdByCode($this->url->getLangDir());
            $activeLanguages = \FWLanguage::getActiveFrontendLanguages();
            if (!$extractedLanguage) {
                $this->redirectToCorrectLanguageDir();
            }
            if (!in_array($extractedLanguage, array_keys($activeLanguages))) {
                $this->lang = \FWLanguage::getDefaultLangId();
                $this->redirectToCorrectLanguageDir();
            }
            //only set langid according to url if the user has not explicitly requested a language change.
            if (!isset($_REQUEST['setLang'])) {
                $this->lang = $extractedLanguage;

            //the user wants to change the language, but we're still inside the wrong language directory.
            } else if($this->lang != $extractedLanguage) {
                $this->redirectToCorrectLanguageDir();
            }
        }

        if ($this->lang) {
            // used for LinkGenerator
            $langCode = \FWLanguage::getLanguageCodeById($this->lang);
            if (!$langCode) {
                // This happens if there's an alias to a page in an inactive
                // language. Redirect to default lang's error page.
                $this->lang = \FWLanguage::getDefaultLangId();
                global $url;
                $_GET['id'] = 404;
                try {
                    $this->url = \Cx\Core\Routing\Url::fromModuleAndCmd('Error', '', \FWLanguage::getDefaultLangId(), array(), '', false);
                    $_REQUEST['section'] = 'Error';
                    $_REQUEST['cmd'] = '';
                } catch (UrlException $e) {
                    \DBG::msg($e->getMessage());
                    \header($_SERVER['SERVER_PROTOCOL'] . ' 502 Bad Gateway');
                    // remove CSRF token
                    output_reset_rewrite_vars();
                    throw new \Cx\Core\Core\Controller\InstanceException();
                }
                $url = $this->url;
                $langCode = \FWLanguage::getLanguageCodeById($this->lang);
            }
            // TODO: This should not be done here!
            if (!defined('FRONTEND_LANG_ID')) {
                define('FRONTEND_LANG_ID', $this->lang);
            }
            $cx = \Cx\Core\Core\Controller\Cx::instanciate();
            $cx->getDb()->getTranslationListener()->setTranslatableLocale(
                $langCode
            );
            // used to load template file
            \Env::get('init')->setFrontendLangId($this->lang);
        }

        global $section, $command, $history, $url, $_CORELANG,
                $page, $pageId, $themesPages,
                $page_template,
                $now, $start, $end;

        $section = '';
        if (
            isset($_REQUEST['section']) &&
            !is_array($_REQUEST['section']) &&
            preg_match('/^[a-z0-9]+$/i', $_REQUEST['section'])
        ) {
            $section = $_REQUEST['section'];
        } else {
            // we must unset $_REQUEST['section'] here in case it contains an
            // invalid value which would otherwise confuse {@see $this->resolvePage()}
            unset($_REQUEST['section']);
        }
        $command = '';
        if (
            isset($_REQUEST['cmd']) &&
            !is_array($_REQUEST['cmd']) &&
            preg_match(
                '/^([-a-z0-9_]+|\[\[' .
                    \Cx\Core\Routing\NodePlaceholder::NODE_URL_PCRE .
                    '\]\])$/ix',
                $_REQUEST['cmd']
            )
        ) {
            $command = $_REQUEST['cmd'];
        }
        $history = isset($_REQUEST['history']) ? intval($_REQUEST['history']) : 0;


        // Initialize page meta
        $page = null;
        $pageAccessId = 0;
        $page_protected = $pageId = $themesPages =
        $page_template = null;

        // TODO: history (empty($history) ? )
        $cx = \Cx\Core\Core\Controller\Cx::instanciate();
        $session = $cx->getComponent('Session');
        if ($this->pagePreview) {
            // resume existing session, but don't
            // initialize a new session
            $session->getSession(false);
        }
        $this->init($url, $this->lang, \Env::get('em'), ASCMS_INSTANCE_OFFSET.\Env::get('virtualLanguageDirectory'), \FWLanguage::getFallbackLanguageArray());
        try {
            $this->resolvePage();
            $page = $this->getPage();
            // TODO: should this check (for type 'application') moved to \Cx\Core\ContentManager\Model\Entity\Page::getCmd()|getModule() ?
            // only set $section and $command if the requested page is an application
            $command = $this->getCmd();
            $section = $this->getSection();
        } catch (\Cx\Core\Routing\ResolverException $e) {
            \DBG::debug($e->getMessage());
            try {
                $this->legacyResolve($url, $section, $command);
                $page = $this->getPage();
                $command = $this->getCmd();
                $section = $this->getSection();
            } catch(\Cx\Core\Routing\ResolverException $e) {
                \DBG::debug($e->getMessage());
                // legacy resolving also failed.
                // provoke a 404
                $page = null;
            }
        }

        if(!$page || !$page->isActive()) {
            //fallback for inexistant error page
            if ($section == 'Error') {
                // If the error module is not installed, show this
                die($_CORELANG['TXT_THIS_MODULE_DOESNT_EXISTS']);
            } else {
                //page not found, redirect to error page.
                // hotfix: resolve to error page
                return $this->resolveToErrorPage();
                // hotfix: resolve to error page
            }
        }

        // TODO: question: what do we need this for? I think there is no need for this (had been added in r15026)
        //legacy: re-populate cmd and section into $_GET
        $_GET['cmd'] = $command;
        $_GET['section'] = $section;
        // END of TODO question

        //check whether the page is active
        $now = new \DateTime('now');
        $start = $page->getStart();
        $end = $page->getEnd();

        $pageId = $page->getId();

        //access: frontend access id for default requests
        $pageAccessId = $page->getFrontendAccessId();
        //revert the page if a history param has been given
        if($history) {
            //access: backend access id for history requests
            $pageAccessId = $page->getBackendAccessId();
            $logRepo = \Env::get('em')->getRepository('Cx\Core\Model\Model\Entity\LogEntry');
            try {
                $logRepo->revert($page, $history);
            }
            catch(\Gedmo\Exception\UnexpectedValueException $e) {
            }

            $logRepo->revert($page, $history);
        }
        /*
        //404 for inactive pages
        if(($start > $now && $start != null) || ($now > $end && $end != null)) {
            if ($section == 'Error') {
                // If the error module is not installed, show this
                die($_CORELANG['TXT_THIS_MODULE_DOESNT_EXISTS']);
            }
            \Cx\Core\Csrf\Controller\Csrf::header('Location: index.php?section=Error&id=404');
            exit;
        }*/


        \Env::get('init')->setCustomizedTheme($page->getSkin(), $page->getCustomContent(), $page->getUseSkinForAllChannels());

        $themesPages = \Env::get('init')->getTemplates($page);

        //replace the {NODE_<ID>_<LANG>}- placeholders
        \LinkGenerator::parseTemplate($themesPages);

        //TODO: analyze those, take action.
        //$page_protected = $objResult->fields['protected'];
        $page_protected = $page->isFrontendProtected();

        //$page_access_id = $objResult->fields['frontend_access_id'];
        $page_template  = $themesPages['content'];

        // Authentification for protected pages
        $this->checkPageFrontendProtection($page, $history);

        //TODO: history

        // TODO: refactor system to be able to remove this backward compatibility
        // Backwards compatibility for code pre Contrexx 3.0 (update)
        $_GET['cmd']     = $_POST['cmd']     = $_REQUEST['cmd']     = $command;
        $_GET['section'] = $_POST['section'] = $_REQUEST['section'] = $section;
        // the system should directly use $this->url->getParamArray() instead of using the super globals
        // TODO: This was disabled during codebase sync. Re-enable. Commit ID a32378e
        /*$qsArr = $this->url->getParamArray();
        foreach ($qsArr as $qsParam => $qsArgument) {
            $_GET[$qsParam] = $_REQUEST[$qsParam] = $qsArgument;
        }*/

        // Start page or default page for no section
        if ($section == 'Home') {
            if (!\Env::get('init')->hasCustomContent()){
                $page_template = $themesPages['home'];
            } else {
                $page_template = $themesPages['content'];
            }
        }

        // this is the case for standalone and backend requests
        if (!$this->page) {
            return null;
        }

        $this->page = clone $this->page;
        $this->page->setVirtual();

        // check for further URL parts to resolve
        if (
            $this->page->getType() == \Cx\Core\ContentManager\Model\Entity\Page::TYPE_APPLICATION
        ) {
            // does this work for fallback(/aliases)?
            $this->additionalPath = substr('/' . $this->originalUrl->getSuggestedTargetPath(), strlen($this->page->getPath()));
            $componentController = $this->em->getRepository('Cx\Core\Core\Model\Entity\SystemComponent')->findOneBy(array('name'=>$this->page->getModule()));
            if ($componentController) {
                $parts = array();
                if (!empty($this->additionalPath)) {
                    $parts = explode('/', substr($this->additionalPath, 1));
                }
                $componentController->resolve($parts, $this->page);
            }
        }
        $canonicalPage = $this->page;
        try {
            if (
                $this->urlPage &&
                $this->urlPage->getType() == \Cx\Core\ContentManager\Model\Entity\Page::TYPE_SYMLINK
            ) {
                $canonicalPage = $this->pageRepo->getTargetPage($this->urlPage);
            }
        } catch (\Exception $e) {
            \DBG::log(__METHOD__ . ': '. $e->getMessage());
        }

        // set canonical page only in case it hasn't been set already
        $linkHeader = preg_grep('/^Link:.*canonical["\']$/', headers_list());
        if ($linkHeader) {
            $linkHeader = current($linkHeader);
            $linkHeaderParts = explode(':', $linkHeader, 2);
            $link = trim($linkHeaderParts[1]);
            $this->headers['Link'] = $link;
        }

        // don't set canonical page when replying with an application page
        // since we can't know which application pages share the same content.
        // TODO: this should be handled by the affected components themself
        if (
            $canonicalPage->getType() == \Cx\Core\ContentManager\Model\Entity\Page::TYPE_APPLICATION &&
            !in_array($canonicalPage->getModule(), array('Home', 'Sitemap', 'Agb', 'Imprint', 'Privacy', 'Ids'))
        ) {
            return $this->page;
        }

        if (!$linkHeader) {
            $link = '<' . \Cx\Core\Routing\Url::fromPage($canonicalPage)->toString() . '>; rel="canonical"';
            header('Link: ' . $link);
            $this->headers['Link'] = $link;
        }

        return $this->page;
    }

    /**
     * Checks for alias request
     * @return Page or null
     */
    public function resolveAlias() {
        // This is our alias, if any
        $path = $this->url->getSuggestedTargetPath();
        $this->path = $path;

        //(I) see what the model has for us, aliases only.
        $result = $this->pageRepo->getPagesAtPath(
            $path,
            null,
            \Cx\Core\ContentManager\Model\Repository\PageRepository::SEARCH_MODE_ALIAS_ONLY
        );

        //(II) sort out errors
        if(!$result) {
            // no alias
            return null;
        }

        if(!$result['page']) {
            // no alias
            return null;
        }
        if (count($result['page']) != 1) {
            throw new ResolverException('Unable to match a single page for this alias (tried path ' . $path . ').');
        }
        $page = current($result['page']);
        if (!$page->isActive()) {
            throw new ResolverException('Alias found, but it is not active.');
        }

        $langDir = $this->url->getLangDir(true);

        // TODO: There are cases where an alias exists for a page on the first level with
        //          the same slug. These get healed by the IF below but should be fixed
        //          in model.
        // TODO: FRONTEND_LANG_ID should not be used here. It is only set if we're within an
        //          internal redirect to error (resolveToErrorPage()). Therefore we can
        //          simply use null and drop variable $frontendLang.
        $frontendLang = defined('FRONTEND_LANG_ID') ? FRONTEND_LANG_ID : null;

        // In case the request does contain a virtual language directory,
        // we have to ensure that there does not exist a page by the requested path.
        // Pages do have a higher precidence than aliases, if a virtual language directory
        // is set.
        // If the request does not contain a virtual language directory, but
        // the request does contain a slash (/), then the request does most
        // likely point to a page, as an alias can not contain a slash
        // character. In such case, we also have to check if the request might
        // match an actual page.
        if (
            ($langDir || strpos($path, '/') !== false)
            && $this->pageRepo->getPagesAtPath(
                $langDir . '/' . $path, $frontendLang,
                \Cx\Core\ContentManager\Model\Repository\PageRepository::SEARCH_MODE_PAGES_ONLY
            )
        ) {
            return null;
        }

        $this->page = $page;

        $params = $this->url->getParamArray();
        if (
            (isset($params['external']) && $params['external'] == 'permanent') ||
            ($this->page->isTargetInternal() && preg_match('/[?&]external=permanent/', $this->page->getTarget()))
        ) {
            if ($this->page->isTargetInternal()) {
                if (isset($params['external']) && $params['external'] == 'permanent') {
                    unset($params['external']);
                }
                if (trim($this->page->getTargetQueryString()) != '') {
                    $params = array_merge($params, explode('&', $this->page->getTargetQueryString()));
                }
                try {
                    $target = \Cx\Core\Routing\Url::fromNodeId($this->page->getTargetNodeId(), $this->page->getTargetLangId(), $params);
                } catch (UrlException $e) {
                    \DBG::log($e->getMessage());
                    return null;
                }
            } else {
                $target = $this->page->getTarget();
            }
            $this->headers['Location'] = $target;
            $emptyString = '';
            \Env::set('Resolver', $this);
            \Env::set('Page', $this->page);
            // need to set this before Cache::postFinalize()
            http_response_code(301);
            \Env::get('cx')->getComponent('Cache')->postFinalize($emptyString);
            header('Location: ' . $target, true, 301);
            if (!$this->page->isTargetInternal()) {
                header('Connection: close');
            }
            // HOTFIX until CLX-2720 & CLX-2725 have been implemented
            $cx = \Cx\Core\Core\Controller\Cx::instanciate();
            $cx->getResponse()->setCode(301);
            \DBG::writeFinishLine($cx, false);
            // END HOTFIX
            exit;
        }

        // If an alias like /de/alias1 was resolved, redirect to /alias1
        if (!empty($langDir)) {
            $correctUrl = \Cx\Core\Routing\Url::fromPage($this->page);
            // get additinal path parts (like /de/alias1/additional/path)
            $splitQuery = explode('?', $this->url->getPath(), 2);
            $pathParts = explode('/', $splitQuery[0]);
            unset($pathParts[0]);
            if (count($pathParts)) {
                $additionalPath = '/' . implode('/', $pathParts);
                $correctUrl->setPath($correctUrl->getPath() . $additionalPath);
            }
            // set query params (like /de/alias1?foo=bar)
            $correctUrl->setParams($this->url->getParamArray());
            $this->headers['Location'] = $correctUrl->toString();
            $emptyString = '';
            \Env::set('Resolver', $this);
            \Env::set('Page', $this->page);
            // need to set this before Cache::postFinalize()
            http_response_code(301);
            \Env::get('cx')->getComponent('Cache')->postFinalize($emptyString);
            header('Location: ' . $correctUrl->toString(), true, 301);
            // HOTFIX until CLX-2720 & CLX-2725 have been implemented
            $cx = \Cx\Core\Core\Controller\Cx::instanciate();
            $cx->getResponse()->setCode(301);
            \DBG::writeFinishLine($cx, false);
            // END HOTFIX
            exit();
        }
        return $this->page;
    }

    public function redirectToCorrectLanguageDir() {
        $cx = \Cx\Core\Core\Controller\Cx::instanciate();
        try {
            $this->url->setLangDir(\FWLanguage::getLanguageCodeById($this->lang));
        } catch (UrlException $e) {
            \DBG::log($e->getMessage());
            \DBG::log('Is the domain mapped? ' . $this->url->toString());
            $response = $cx->getResponse();
            if ($response) {
                $response->setCode(502);
            }
            http_response_code(502);
            // remove CSRF token
            output_reset_rewrite_vars();
            throw new \Cx\Core\Core\Controller\InstanceException();
        }
        $cx->adjustRequest($this->url, 302);
    }

    /**
     * Does the resolving work, extends $this->url with targetPath and params.
     */
    public function resolvePage($internal = false) {
        // Abort here in case we're handling a legacy request.
        // The legacy request will be handled by $this->legacyResolve().
        // Important: We must check for $internal == FALSE here, to abort the resolving process
        //            only when we're resolving the initial (original) request.
        //            Internal resolving requests might be called by $this->legacyResolve()
        //            and shall therefore be processed.
        if (!$internal && isset($_REQUEST['section'])) {
            throw new ResolverException('Legacy request');
        }

        $path = $this->url->getSuggestedTargetPath();

        if (!$this->page || $internal) {
            $page = null;
            $previewPage = null;
            if (
                !$internal
                && $this->pagePreview
                && !empty($this->sessionPage)
                && (
                    !empty($this->sessionPage['pageId'])
                    || !empty($this->sessionPage['nodeId'])
                    || !empty($this->sessionPage['parentNodeId'])
                )
            ) {
                $previewPage = $this->getPreviewPage();
            }
            if (
                $previewPage
                && ltrim($previewPage->getPath(), '/') == $path
            ) {
                $this->isPreviewPageRequested = true;
                $page = $previewPage;
                $result = [
                    'matchedPath' => $previewPage->getPath(),
                    'unmatchedPath' => '',
                ];
            } else {
                // we must reset $this->sessionPage as otherwise the preview
                // of non-published pages does not work
                $this->sessionPage = [];
            }
            if (!$page) {
                //(I) see what the model has for us
                $langDirPath = '';
                if (\Cx\Core\Routing\Url::isVirtualLanguageDirsActive()) {
                    $langDirPath = $this->url->getLangDir() . '/';
                }
                $mode = \Cx\Core\ContentManager\Model\Repository\PageRepository::SEARCH_MODE_PAGES_ONLY;
                $result = $this->pageRepo->getPagesAtPath(
                    $langDirPath . $path, $this->lang,
                    $mode
                );
                //(II) sort out errors
                if (!$result) {
                    throw new ResolverException(
                        'No page for language directory "' . $langDirPath
                        . '" and path "' . $path . '"'
                    );
                }
                if (empty($result['page']) || empty(current($result['page']))) {
                    throw new ResolverException(
                        'No page for language directory "' . $langDirPath
                        . '" and path "' . $path . '"'
                    );
                }
                $page = current($result['page']);
            }
            if ($this->pagePreview) {
                if (empty($this->sessionPage)) {
                    if (\Permission::checkAccess(6, 'static', true)) {
                        $page->setActive(true);
                        $page->setDisplay(true);
                        if (($page->getEditingStatus() == 'hasDraft') || (($page->getEditingStatus() == 'hasDraftWaiting'))) {
                            $logEntries = $this->logRepo->getLogEntries($page);
                            $this->logRepo->revert($page, $logEntries[1]->getVersion());
                        }
                    }
                }
            }

            if (!$page->isActive()) {
                throw new ResolverException('Found page is inactive');
            }

            // if user has no rights to see this page, we redirect to login
            $this->checkPageFrontendProtection($page);

            // If an older revision was requested, revert to that in-place:
            if (!empty($this->historyId) && \Permission::checkAccess(6, 'static', true)) {
                $this->logRepo->revert($page, $this->historyId);
            }

            //(III) extend our url object with matched path / params
            $this->url->setTargetPath($result['matchedPath'].$result['unmatchedPath']);
            $this->url->setParams($this->url->getSuggestedParams() . $this->url->getSuggestedAnchor());

            $this->page = $page;
        }
        if (!$this->urlPage) {
            $this->urlPage = clone $this->page;
        }
        /*
          the page we found could be a redirection.
          in this case, the URL object is overwritten with the target details and
          resolving starts over again.
         */
        $target = $this->page->getTarget();
        $isRedirection = $this->page->getType() == \Cx\Core\ContentManager\Model\Entity\Page::TYPE_REDIRECT;
        $isAlias = $this->page->getType() == \Cx\Core\ContentManager\Model\Entity\Page::TYPE_ALIAS;

        //handles alias redirections internal / disables external redirection
        $this->forceInternalRedirection = $this->forceInternalRedirection || $isAlias;

        if($target && ($isRedirection || $isAlias)) {
            // Check if page is a internal redirection and if so handle it
            if($this->page->isTargetInternal()) {
//TODO: add check for endless/circular redirection (a -> b -> a -> b ... and more complex)
                $nId = $this->page->getTargetNodeId();
                $lId = $this->page->getTargetLangId();
                $module = $this->page->getTargetModule();
                $cmd = $this->page->getTargetCmd();
                $qs = $this->page->getTargetQueryString();

                $langId = $lId ? $lId : $this->lang;

                // try to find the redirection target page
                if ($nId) {
                    $targetPage = $this->pageRepo->findOneBy(array('node' => $nId, 'lang' => $langId));

                    // revert to default language if we could not retrieve the specified langauge by the redirection.
                    // so lets try to load the redirection of the current language
                    if(!$targetPage) {
                        if($langId != 0) { //make sure we weren't already retrieving the default language
                            $targetPage = $this->pageRepo->findOneBy(array('node' => $nId, 'lang' => $this->lang));
                            $langId = $this->lang;
                        }
                    }
                } else {
                    $targetPage = $this->pageRepo->findOneByModuleCmdLang($module, $cmd, $langId);

                    // in case we were unable to find the requested page, this could mean that we are
                    // trying to retrieve a module page that uses a string with an ID (STRING_ID) as CMD.
                    // therefore, lets try to find the module by using the string in $cmd and INT in $langId as CMD.
                    // in case $langId is really the requested CMD then we will have to set the
                    // resolved language back to our original language $this->lang.
                    if (!$targetPage) {
                        $targetPage = $this->pageRepo->findOneBymoduleCmdLang($module, $cmd.'_'.$langId, $this->lang);
                        if ($targetPage) {
                            $langId = $this->lang;
                        }
                    }

                    // try to retrieve a module page that uses only an ID as CMD.
                    // lets try to find the module by using the INT in $langId as CMD.
                    // in case $langId is really the requested CMD then we will have to set the
                    // resolved language back to our original language $this->lang.
                    if (!$targetPage) {
                        $targetPage = $this->pageRepo->findOneByModuleCmdLang($module, $langId, $this->lang);
                        $langId = $this->lang;
                    }

                    // revert to default language if we could not retrieve the specified langauge by the redirection.
                    // so lets try to load the redirection of the current language
                    if (!$targetPage) {
                        if ($langId != 0) { //make sure we weren't already retrieving the default language
                            $targetPage = $this->pageRepo->findOneByModuleCmdLang($module, $cmd, $this->lang);
                            $langId = $this->lang;
                        }
                    }
                }

                //check whether we have an active page now
                if (!$targetPage || !$targetPage->isActive()) {
                    $this->page = null;
                    return;
                }

                // Ensure the resolved target page has not yet been resolved
                // before. If it has however, then that means we are in an
                // infinite redirect loop and must abort the resolving process.
                if (in_array($targetPage, $this->resolveStack)) {
                    \DBG::msg(__METHOD__ . ': internal infinite redirection');
                    $cx = \Cx\Core\Core\Controller\Cx::instanciate();
                    $response = $cx->getResponse();
                    if ($response) {
                        $response->setCode(502);
                    }
                    http_response_code(502);
                    // remove CSRF token
                    output_reset_rewrite_vars();
                    throw new \Cx\Core\Core\Controller\InstanceException();
                } else {
                    $this->resolveStack[] = $targetPage;
                }

                // the redirection page is located within a different language.
                // therefore, we must set $this->lang to the target's language of the redirection.
                // this is required because we will next try to resolve the redirection target
                if ($langId != $this->lang) {
                    $this->lang = $langId;
                    $this->url->setLangDir(\FWLanguage::getLanguageCodeById($langId));
                    $this->pathOffset = ASCMS_INSTANCE_OFFSET;
                }

                $targetPath = substr($targetPage->getPath(), 1);

                $this->url->setTargetPath($targetPath.$qs);
                $this->url->setPath($targetPath.$qs);
                $this->isRedirection = true;
                $this->resolvePage(true);

                if (empty($this->page)) {
                    \DBG::msg(__METHOD__ . ': target page of internal redirection not found/published');
                    // hotfix: resolve to error page
                    return $this->resolveToErrorPage();
                    // endhotfix: resolve to error page
                }
            } else { //external target - redirect via HTTP redirect
                if (\FWValidator::isUri($target)) {
                    $this->headers['Location'] = $target;
                    $emptyString = '';
                    \Env::set('Resolver', $this);
                    \Env::set('Page', $this->page);
                    // need to set this before Cache::postFinalize()
                    http_response_code(301);
                    \Env::get('cx')->getComponent('Cache')->postFinalize($emptyString);
                    header('Location: ' . $target, true, 301);
                    header('Connection: close');
                    // HOTFIX until CLX-2720 & CLX-2725 have been implemented
                    $cx = \Cx\Core\Core\Controller\Cx::instanciate();
                    $cx->getResponse()->setCode(301);
                    \DBG::writeFinishLine($cx, false);
                    // END HOTFIX
                    exit;
                } else {
                    if ($target[0] == '/') {
                        $target = substr($target, 1);
                    }
                    $langDir = '';
                    if (!file_exists(ASCMS_INSTANCE_PATH . ASCMS_INSTANCE_OFFSET . '/' . $target)) {
                        $langCode = '';
                        if (\Cx\Core\Routing\Url::isVirtualLanguageDirsActive()) {
                            $langCode = \FWLanguage::getLanguageCodeById($this->lang);
                        }
                        if (!empty($langCode)) {
                            $langDir = '/' . $langCode;
                        }
                    }

                    $target = ASCMS_INSTANCE_OFFSET . $langDir . '/' . $target;
                    $this->headers['Location'] = $target;
                    $emptyString = '';
                    \Env::set('Resolver', $this);
                    \Env::set('Page', $this->page);
                    // need to set this before Cache::postFinalize()
                    http_response_code(301);
                    \Env::get('cx')->getComponent('Cache')->postFinalize($emptyString);
                    header('Location: ' . $target, true, 301);
                    // HOTFIX until CLX-2720 & CLX-2725 have been implemented
                    $cx = \Cx\Core\Core\Controller\Cx::instanciate();
                    $cx->getResponse()->setCode(301);
                    \DBG::writeFinishLine($cx, false);
                    // END HOTFIX
                    exit;
                }
            }
        }

        //if we followed one or more redirections, the user shall be redirected.
        if ($this->isRedirection && !$this->forceInternalRedirection) {
            $params = $this->url->getSuggestedParams();
            $target = $this->page->getURL($this->pathOffset, array());
            $target->setParams($params);
            $this->headers['Location'] = $target->toString() . $this->url->getSuggestedAnchor();
            $emptyString = '';
            \Env::set('Resolver', $this);
            \Env::set('Page', $this->page);
            // need to set this before Cache::postFinalize()
            http_response_code(301);
            \Env::get('cx')->getComponent('Cache')->postFinalize($emptyString);
            header('Location: ' . $target->toString() . $this->url->getSuggestedAnchor(), true, 301);
            // HOTFIX until CLX-2720 & CLX-2725 have been implemented
            $cx = \Cx\Core\Core\Controller\Cx::instanciate();
            $cx->getResponse()->setCode(301);
            \DBG::writeFinishLine($cx, false);
            // END HOTFIX
            exit;
        }

        // in case the requested page is of type fallback, we will now handle/load this page
        $this->handleFallbackContent($this->page, !$internal);

        // set legacy <section> and <cmd> in case the requested page is an application
        if ($this->page->getType() == \Cx\Core\ContentManager\Model\Entity\Page::TYPE_APPLICATION
                || $this->page->getType() == \Cx\Core\ContentManager\Model\Entity\Page::TYPE_FALLBACK) {
            $this->command = $this->page->getCmd();
            $this->section = $this->page->getModule();
        }
    }

    public function legacyResolve($url, $section, $command)
    {
        $objFWUser = \FWUser::getFWUserObject();

        /*
          The Resolver couldn't find a page.
          We're looking at one of the following situations, which are treated in the listed order:
           a) Request for the 'home' page
           b) Legacy request with section / cmd
           c) Request for inexistant page
          We try to locate a module page via cmd and section (if provided).
          If that doesn't work, an error is shown.
        */

        // a: 'home' page
        $urlPointsToHome =    $url->getSuggestedTargetPath() == 'index.php'
                           || $url->getSuggestedTargetPath() == '';
        //    user probably tried requesting the home-page
        if(!$section && $urlPointsToHome) {
            $section = 'Home';
        }
        $this->setSection($section, $command);

        // b(, a): fallback if section and cmd are specified
        if ($section) {
            if ($section == 'logout') {
                $cx = \Cx\Core\Core\Controller\Cx::instanciate();
                $cx->getComponent('Session')->getSession();
                if ($objFWUser->objUser->login()) {
                    $objFWUser->logout();
                }
                // note: this is obsolete for signed-in users, as
                // {@see \FWUser::logout()} does already redirect
                // to the Login page.
                \Cx\Core\Csrf\Controller\Csrf::redirect(
                    \Cx\Core\Routing\Url::fromModuleAndCmd('Login')
                );
            }

            $pageRepo = \Env::get('em')->getRepository('Cx\Core\ContentManager\Model\Entity\Page');
            // If the database uses a case insensitive collation, $section needn't be the exact component name to find a page
            $this->page = $pageRepo->findOneByModuleCmdLang($section, $command, FRONTEND_LANG_ID);

            //fallback content
            if (!$this->page) {
                return;
            }

            // make legacy-requests case insensitive works only if database-collation is case insensitive as well
            $this->setSection($this->page->getModule(), $this->page->getCmd());

            $this->checkPageFrontendProtection($this->page);

            $this->handleFallbackContent($this->page);
        }

        // c: inexistant page gets catched below.
    }

    /**
     * Returns the preview page built from the session page array.
     * @param bool $init If set to `true`, then the preview page will be
     *     initialized.
     * @throws ResolverException In case `$init` is set to `false`, but not preview-page
     *     is set.
     * @return Cx\Core\ContentManager\Model\Entity\Page $page
     */
    public function getPreviewPage(bool $init = true): \Cx\Core\ContentManager\Model\Entity\Page {

        if ($this->previewPage) {
            return $this->previewPage;
        }
        if (!$init) {
            throw new ResolverException('No preview page initialized!');
        }
        $data = $this->sessionPage;

        // load existing page
        $node = null;
        $parentNode = $this->nodeRepo->getRoot();
        if (
            !empty($data['pageId'])
            && $data['pageId'] != 'new'
        ) {
            $this->previewPage = $this->pageRepo->findOneById($data['pageId']);
        }
        // load non-translated page from within the tree
        if (!$this->previewPage && !empty($data['nodeId'])) {
            $node = $this->nodeRepo->find($data['nodeId']);
            if ($node) {
                $this->previewPage = $node->translatePage(
                    true,
                    \FWLanguage::getLanguageIdByCode($data['lang']),
                    false
                );
            }
        }
        // load non-persistent page inserted within the tree
        if (!$this->previewPage && !empty($data['parentNodeId'])) {
            $parentNode = $this->nodeRepo->find($data['parentNodeId']);
            if ($parentNode) {
                $parentPage = $parentNode->getPage(\FWLanguage::getLanguageIdByCode($data['lang']));
                if ($parentPage) {
                    $parentNode->translatePage(
                        false,
                        \FWLanguage::getLanguageIdByCode($data['lang']),
                        false
                    );
                }
            }
        }
        // create virtual node if non-existant
        if (!$node) {
            $node = new \Cx\Core\ContentManager\Model\Entity\Node();
            $node->setParent($parentNode);
            $node->setLvl($parentNode->getLvl() + 1);
            $node->setVirtual(true);
            $this->nodeRepo->getRoot()->addChildren($node);
        }
        // load non-persistent page at root-level
        if (!$this->previewPage) {
            $this->previewPage = new \Cx\Core\ContentManager\Model\Entity\Page();
            // set ID of page to 0 to mark it as preview page
            $this->previewPage->setId(0);
            $node->addPage($this->previewPage);
            $this->previewPage->setNode($node);
            $this->pageRepo->addVirtualPage($this->previewPage);
        }

        unset($data['pageId']);
        unset($data['nodeId']);
        unset($data['parentNodeId']);
        $this->previewPage->setLang(\FWLanguage::getLanguageIdByCode($data['lang']));
        unset($data['lang']);
        $this->previewPage->updateFromArray($data);
        $this->previewPage->setUpdatedAtToNow();
        $this->previewPage->setActive(true);
        $this->previewPage->setVirtual(true);
        $this->previewPage->validate();

        return $this->previewPage;
    }

    /**
     * Check if the preview page has been requested.
     *
     * @return boolean Whether the current request is made to the preview page
     *     of the Content Manager.
     */
    public function isPreviewPageRequested(): bool {
        return $this->isPreviewPageRequested;
    }

    /**
     * Checks whether $page is of type 'fallback'. Loads fallback content if yes.
     * @param Cx\Core\ContentManager\Model\Doctrine $page
     * @param boolean $requestedPage Set to TRUE (default) if the $page passed by $page is the first resolved page (actual requested page)
     * @throws ResolverException
     */
    public function handleFallbackContent($page, $requestedPage = true) {
        //handle untranslated pages - replace them by the right language version.

        // Important: We must reset the modified $page object here.
        // Otherwise the EntityManager holds false information about the page.
        // I.e. type might be 'application' instead of 'fallback'
        // See bug-report #1536
        // Note: Except for non-persistent pages from pagePreview
        if ($page->getId()) {
            $page = $this->pageRepo->findOneById($page->getId());
        }

        if (
            $page->getType() == \Cx\Core\ContentManager\Model\Entity\Page::TYPE_FALLBACK ||
            $page->getType() == \Cx\Core\ContentManager\Model\Entity\Page::TYPE_SYMLINK
        ) {
            // in case the first resolved page (= original requested page) is a fallback page
            // we must check here if this very page is active.
            // If we miss this check, we would only check if the referenced fallback page is active!
            if ($requestedPage && !$page->isActive()) {
                return;
            }

            // if this page is protected, we do not follow fallback
            $this->checkPageFrontendProtection($page);

            try {
                if ($page->getType() == \Cx\Core\ContentManager\Model\Entity\Page::TYPE_SYMLINK) {
                    $fallbackPage = $this->pageRepo->getTargetPage($page);
                } else {
                    $fallbackPage = $this->getFallbackPage($page);
                }
            } catch (\Exception $e) {
                \DBG::msg(__METHOD__ . ': '. $e->getMessage());
                //page not found, redirect to error page.
                if ($this->section == 'Error') {
                    \DBG::msg(
                        __METHOD__ . ': Internal infinite redirection'
                        . ' due to missing target of Error fallback-page'
                    );
                    $cx = \Cx\Core\Core\Controller\Cx::instanciate();
                    $response = $cx->getResponse();
                    if ($response) {
                        $response->setCode(502);
                    }
                    http_response_code(502);
                    // remove CSRF token
                    output_reset_rewrite_vars();
                    throw new \Cx\Core\Core\Controller\InstanceException();
                }
                // hotfix: resolve to error page
                return $this->resolveToErrorPage();
                // end hotfix: resolve to error page
            }

            // due that the fallback is located within a different language
            // we must set $this->lang to the fallback's language.
            // this is required because we will next try to resolve the page
            // that is referenced by the fallback page.
            // we have to remember the requested lang for the case when the
            // page referenced by the fallback page redirects to an invalid target.
            $requestLang = $this->lang;
            $this->lang = $fallbackPage->getLang();
            $this->url->setLangDir(\FWLanguage::getLanguageCodeById($this->lang));
            $this->url->setSuggestedTargetPath(substr($fallbackPage->getPath(), 1));

            // now lets resolve the page that is referenced by our fallback page
            $this->resolvePage(true);

            // in case the referenced page redirects to an invalid target,
            // we'll have to resolve to the error page.
            if (!$this->page) {
                $this->lang = $requestLang;
                return $this->resolveToErrorPage();
            }
            // fallback pages use theme options of their target page, symlink use their own
            $page->setContentOf(
                $this->page,
                $page->getType() == \Cx\Core\ContentManager\Model\Entity\Page::TYPE_FALLBACK,
                $page->getType() == \Cx\Core\ContentManager\Model\Entity\Page::TYPE_SYMLINK
            );

            // Important: We must assigne a copy of $page to $this->path here.
            // Otherwise, the virtual fallback page ($this->page) will also
            // be reset, when we reset (see next command) the original
            // requested page $page.
            $this->page = clone $page;

            // Due to the fallback-resolving, the virtual language directory
            // is currently set to the fallback language. Therefore we must set
            // it back to the language of the original request. Same also applies
            // to $this->lang, which was used to resolv the fallback page(s).
            $this->url->setLangDir(\FWLanguage::getLanguageCodeById($page->getLang()));
            $this->lang = $page->getLang();
        }
    }

    public function getFallbackPage($page) {
        $fallbackPage = null;
        if (isset($this->fallbackLanguages[$page->getLang()])) {
            $langId = $this->fallbackLanguages[$page->getLang()];
            $fallbackPage = $page->getNode()->getPage($langId);
        }
        if (!$fallbackPage) {
            throw new ResolverException('Followed fallback page, but couldn\'t find content of fallback Language');
        }
        return $fallbackPage;
    }

    /**
     * Checks if this page can be displayed in frontend, redirects to login of not
     * @param \Cx\Core\ContentManager\Model\Entity\Page $page Page to check
     * @param int $history (optional) Revision of page to use, 0 means current, default 0
     */
    public function checkPageFrontendProtection($page, $history = 0) {
        $page_protected = $page->isFrontendProtected();
        $pageAccessId = $page->getFrontendAccessId();
        if ($history) {
            $pageAccessId = $page->getBackendAccessId();
        }

        // login pages are unprotected by design
        $checkLogin = array($page);
        while (count($checkLogin)) {
            $currentPage = array_pop($checkLogin);
            if ($currentPage->getType() == \Cx\Core\ContentManager\Model\Entity\Page::TYPE_FALLBACK) {
                try {
                    array_push($checkLogin, $this->getFallbackPage($currentPage));
                } catch (ResolverException $e) {}
            }
            if ($currentPage->getModule() == 'Login') {
                return;
            }
        }

        // Authentification for protected pages
        if (($page_protected || $history) &&
            (
               !isset($_REQUEST['section']) ||
                $_REQUEST['section'] != 'Login'
            )
        ) {
            if (\FWUser::getFWUserObject()->objUser->login()) {
                if ($page_protected) {
                    if (!\Permission::checkAccess($pageAccessId, 'dynamic', true)) {
                        $link=base64_encode(\Env::get('cx')->getRequest()->getUrl()->toString());
                        \Cx\Core\Csrf\Controller\Csrf::header('Location: '.\Cx\Core\Routing\Url::fromModuleAndCmd('Login', 'noaccess', '', array('redirect' => $link)));
                        exit;
                    }
                }
                if ($history && !\Permission::checkAccess(78, 'static', true)) {
                    $link=base64_encode(\Env::get('cx')->getRequest()->getUrl()->toString());
                    \Cx\Core\Csrf\Controller\Csrf::header('Location: '.\Cx\Core\Routing\Url::fromModuleAndCmd('Login', 'noaccess', '', array('redirect' => $link)));
                    exit;
                }
            } else {
                if (isset($_GET['redirect'])) {
                    $link = $_GET['redirect'];
                } else {
                    $cx = \Cx\Core\Core\Controller\Cx::instanciate();
                    if ($this->aliaspage) {
                        $link = \Cx\Core\Routing\Url::fromPage(
                            $this->aliaspage,
                            $cx->getRequest()->getUrl()->getParamArray()
                        )->toString();
                    } else {
                        $link = \Env::get('cx')->getRequest()->getUrl()->toString();
                    }
                    $link = base64_encode($link);
                }
                \Cx\Core\Csrf\Controller\Csrf::header(
                    'Location: '. \Cx\Core\Routing\Url::fromModuleAndCmd(
                        'Login',
                        '',
                        '',
                        array('redirect' => $link)
                    )
                );
                exit;
            }
        }
    }

    public function getPage() {
        return $this->page;
    }

    public function getURL() {
        return $this->url;
    }

    /**
     * Returns the resolved module name (if any, empty string if none)
     * @return String Module name
     */
    public function getSection() {
        return $this->section;
    }

    /**
     * Returns the resolved module command (if any, empty string if none)
     * @return String Module command
     */
    public function getCmd() {
        return $this->command;
    }

    /**
     * Sets the value of the resolved module name and command
     * This should not be called from any (core_)module!
     * For legacy requests only!
     *
     * @param String $section Module name
     * @param String $cmd Module command
     * @todo Remove this method as soon as legacy request are no longer possible
     */
    public function setSection($section, $command = '') {
        $this->section = $section;
        $this->command = $command;
    }

    /**
     * Returns the headers set by this class
     * @return array key=>value style array
     */
    public function getHeaders() {
        $response = \Cx\Core\Core\Controller\Cx::instanciate()->getResponse();
        if ($response->getExpirationDate()) {
            $this->headers['Expires'] = $response->getExpirationDate()->format('r');
        }
        return $this->headers;
    }

    /**
     * Returns the passed path additional to the resolved page's path
     * @return string Offset path without leading slash
     */
    public function getAdditionalPath() {
        return substr($this->additionalPath, 1);
    }

    /**
     * Resolve the current request the Error application page
     *
     * In case there does not exists a published application page of component
     * Error, then an InstanceException with HTTP status code 502 Bad Gateway
     * is thrown.
     * @throws  \Cx\Core\Core\Controller\InstanceException
     * @return  \Cx\Core\ContentManager\Model\Entity\Page   The resolved Error
     *                                                      application page
     */
    protected function resolveToErrorPage() {
        if (
            \Cx\Core\Core\Controller\Cx::instanciate()
                ->getComponent('Session')
                ->isInitialized()
        ) {
            \Cx\Core\Core\Controller\Cx::instanciate()
                ->getComponent('Session')->getSession()->discard();
        }

        // $url needs to be global as some legacy code (outside of Routing)
        // requires it
        global $url;

        // as the resolving is partially dodgy, we have to overwrite
        // the following set of variables to ensure the resolving to the
        // Error application page works as expected in any use case
        $_GET=array();
        $_GET['id'] = 404;
        $_REQUEST=array();
        $this->setSection('Error', '');
        $this->page = null;

        // resolve to Error application page
        try {
            $page = $this->pageRepo->findOneByModuleCmdLang('Error', '', $this->lang);
            if (!$page || !$page->isActive()) {
                throw new UrlException('No active error page');
            }
            $this->url = \Cx\Core\Routing\Url::fromModuleAndCmd('Error', '', $this->lang, array(), '', false);
        } catch (UrlException $e) {
            \DBG::msg($e->getMessage());
            $cx = \Cx\Core\Core\Controller\Cx::instanciate();
            $response = $cx->getResponse();
            if ($response) {
                $response->setCode(502);
            }
            http_response_code(502);
            // remove CSRF token
            output_reset_rewrite_vars();
            throw new \Cx\Core\Core\Controller\InstanceException();
        }
        $url = $this->url;
        return $this->resolve();
    }
}
