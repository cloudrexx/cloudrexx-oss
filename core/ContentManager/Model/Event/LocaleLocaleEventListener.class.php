<?php
declare(strict_types=1);

/**
 * Cloudrexx
 *
 * @link      https://www.cloudrexx.com
 * @copyright Cloudrexx AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

namespace Cx\Core\ContentManager\Model\Event;

/**
 * @copyright   Cloudrexx AG
 * @author      Thomas Wirz <thomas.wirz@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  core_contentmanager
 */
class LocaleLocaleEventListener extends \Cx\Core\Event\Model\Entity\DefaultEventListener {
    protected function prePersist($eventArgs) {
        $this->validateLocale($eventArgs->getEntity());
    }

    protected function preUpdate($eventArgs) {
        $this->validateLocale($eventArgs->getObject());
    }

    /**
     * Verifies that the locale $locale does not conflict with any existing alias
     * or page slug.
     *
     * @param  \Cx\Core\Locale\Model\Enitty\Locale $locale Locale to validate
     * throws \Cx\Model\Base\ValidationException In case the locale conflicts
     *     with a page/alias
     */
    protected function validateLocale(\Cx\Core\Locale\Model\Entity\Locale $locale): void {
        $localeTag = $locale->getIso1()->getIso1();
        if ($locale->getCountry()) {
            $localeTag .= '-' . $locale->getCountry()->getAlpha2();
        }
        $criteria = [
            'slug' => $localeTag
        ];
        if (\Cx\Core\Routing\Url::isVirtualLanguageDirsActive()) {
            $criteria['type'] = \Cx\Core\ContentManager\Model\Entity\Page::TYPE_ALIAS;
        }
        // note: the lookup is case insensitive
        $pages = $this->cx->getDb()->getEntityManager()->getRepository(
            \Cx\Core\ContentManager\Model\Entity\Page::class
        )->findBy($criteria, null, null, null, true);
        if (!$pages) {
            return;
        }
        $rootNode = $this->cx->getDb()->getEntityManager()->getRepository(
            \Cx\Core\ContentManager\Model\Entity\Node::class
        )->getRoot();
        foreach ($pages as $page) {
            if ($page->getNode()->getParent() != $rootNode) {
                continue;
            }
            throw $this->getValidationException($page);
        }
    }

    /**
     * Generate a user-friendly exception based on the type of $page which
     * is conflicting the the locale from {@see static::validateLocale()}.
     *
     * @param   \Cx\Core\ContentManager\Model\Entity\Page $page Page 
     * @return \Cx\Model\Base\ValidationException Exception containing the
     *     custom user-friendly message.
     */
    protected function getValidationException(
        \Cx\Core\ContentManager\Model\Entity\Page $page
    ): \Cx\Model\Base\ValidationException {
        $_ARRAYLANG = \Env::get('init')->getComponentSpecificLanguageData('Core', false);
        $targetPage = null;
        if (
            $page->getType() == \Cx\Core\ContentManager\Model\Entity\Page::TYPE_ALIAS
            && $page->isTargetInternal()
        ) {
            $targetPage = $this->cx->getDb()->getEntityManager()->getRepository(
                \Cx\Core\ContentManager\Model\Entity\Page::class
            )->getTargetPage($page);
            $url = \Cx\Core\Routing\Url::fromBackend(
                'ContentManager',
                '',
                0,
                [
                    'page' => $targetPage->getId(),
                    'tab' => 'navigation',
                ]
            );
            $url->setAnchor('navigation');
        }
        if (
            $page->getType() == \Cx\Core\ContentManager\Model\Entity\Page::TYPE_ALIAS
            && !$targetPage
        ) {
            $url = \Cx\Core\Routing\Url::fromBackend(
                'Alias',
                'entries',
                0,
                ['term' => $page->getSlug()]
            );
            return new \Cx\Model\Base\ValidationException([
                'slug' => [
                    sprintf(
                        $_ARRAYLANG['TXT_CONTENTMANAGER_LOCALE_CONFLICTS_WITH_ALIAS'],
                        $page->getSlug(),
                        $this->getAnchorTag($url, $page->getTitle())
                    )
                ]
            ]);
        }
        if (!$targetPage) {
            $targetPage = $page;
        }
        $url = \Cx\Core\Routing\Url::fromBackend(
            'ContentManager',
            '',
            0,
            [
                'page' => $targetPage->getId(),
                'tab' => 'navigation',
            ]
        );
        return new \Cx\Model\Base\ValidationException([
            'slug' => [
                sprintf(
                    $_ARRAYLANG['TXT_CONTENTMANAGER_LOCALE_CONFLICTS_WITH_PAGE'],
                    $page->getSlug(),
                    $this->getAnchorTag($url, $targetPage->getTitle())
                )
            ]
        ]);
    }

    /**
     * Generate a HTML-a-tag for the text $title with target $url.
     *
     * @param \Cx\Core\Routing\Url $url Url to use a a-href attribute
     * @param   string $title Text to use as text-child-node of a-tag
     * @return  \Cx\Core\Html\Model\Entity\HtmlElement The generated HTML-a-tag
     */
    protected function getAnchorTag(
        \Cx\Core\Routing\Url $url,
        string $title
    ): \Cx\Core\Html\Model\Entity\HtmlElement {
        $tag = new \Cx\Core\Html\Model\Entity\HtmlElement('a');
        $tag->setAttribute('href', $url);
        $tag->addChild(
            new \Cx\Core\Html\Model\Entity\TextElement($title)
        );
        return $tag;
    }
}

