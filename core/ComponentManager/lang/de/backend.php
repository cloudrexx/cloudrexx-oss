<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * @copyright   CLOUDREXX CMS - CLOUDREXX AG
 * @author      Cloudrexx Development Team <info@cloudrexx.com>
 * @access      public
 * @package     cloudrexx
 * @subpackage  core_componentmanager
 */
global $_ARRAYLANG;
$_ARRAYLANG['TXT_MODULE_MANAGER'] = 'Übersicht';
$_ARRAYLANG['TXT_MODULE_ACTIVATION'] = 'Anwendung aktivierung';
$_ARRAYLANG['TXT_NAME'] = 'Name';
$_ARRAYLANG['TXT_CONFIRM_DELETE_DATA'] = 'Datensatz wirklich löschen?';
$_ARRAYLANG['TXT_ACTION_IS_IRREVERSIBLE'] = 'Diese Aktion kann nicht rückgängig gemacht werden!';
$_ARRAYLANG['TXT_DESCRIPTION'] = 'Beschreibung';
$_ARRAYLANG['TXT_STATUS'] = 'Status';
$_ARRAYLANG['TXT_INSTALL_MODULE'] = 'Installieren';
$_ARRAYLANG['TXT_PROVIDE_MODULE'] = 'Bereitstellen';
$_ARRAYLANG['TXT_REMOVE_MODULE'] = 'Entfernen';
$_ARRAYLANG['TXT_ACCEPT_CHANGES'] = 'Speichern';
$_ARRAYLANG['TXT_APPLICATION'] = 'Anwendung';
$_ARRAYLANG['TXT_MODULE_ACTIVATED_SUCCESSFULLY'] = 'Anwendung(en) erfolgreich aktiviert';
$_ARRAYLANG['TXT_MODULE_DEACTIVATED_SUCCESSFULLY'] = 'Anwendung(en) erfolgreich deaktiviert';
$_ARRAYLANG['TXT_REQUIRED'] = 'Erforderlich';
$_ARRAYLANG['TXT_LICENSE_NOT_LICENSED'] = 'Nicht lizenziert';
$_ARRAYLANG['TXT_OPTIONAL'] = 'Optional';
$_ARRAYLANG['TXT_MODULES_INSTALLED_SUCCESFULL'] = 'Anwendung(en) %s erfolgreich installiert.';
$_ARRAYLANG['TXT_MODULES_REMOVED_SUCCESSFUL'] = 'Anwendung(en) %s erfolgreich entfernt.';
$_ARRAYLANG['TXT_DATABASE_QUERY_ERROR'] = 'Fehler beim Ausführen der Datenbankabfrage!';
