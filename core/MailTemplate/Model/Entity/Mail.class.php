<?php
declare(strict_types=1);

/**
 * Cloudrexx
 *
 * @link      https://www.cloudrexx.com
 * @copyright Cloudrexx AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

namespace Cx\Core\MailTemplate\Model\Entity;

/**
 * Wrapper class for \PHPMailer\PHPMailer\PHPMailer
 *
 * @copyright   Cloudrexx AG
 * @author      Project Team SS4U <info@cloudrexx.com>
 * @author      Thomas Wirz <thomas.wirz@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  core_mailtemplate
 */
class Mail extends \PHPMailer\PHPMailer\PHPMailer {

    /**
     * If set to TRUE, then the send process is not actually executed.
     * However the method {@see static::send()} still returns TRUE as if the
     * send process was successful.
     *
     * @var boolean
     */
    protected $softFail = false;

    /**
     * Whether the sender domain (from {@see static::$From}) needs to be
     * verified by both, SPF and DKIM and also be in alignement with DMARC.
     *
     * @var boolean
     */
    protected bool $requiresFullSenderAuthentication = false;

    /**
     * Whether the sender domain (from {@see static::$From}) needs to be
     * verified at all
     *
     * @var boolean
     */
    protected bool $ignoreSenderValidation = false;

    /**
     * ID of the SMTP server ({@see \SmtpSettings::getSmtpAccounts()}) selected
     * for sending e-mails.
     *
     * @var int
     */
    protected int $selectedSmtpServerId = 0;

    /**
     * IP of this request (as initialized by {@see static::initClientIp()}).
     * Will be used for logging.
     *
     * @var string
     */
    protected string $clientIp = '';

    public function __construct($exceptions = null) {
        parent::__construct($exceptions);

        // disable user-friendly error messages
        // this will cause PHPMailer to yield error-codes that can be
        // processed as done by NewsletterManager::SendEmail()
        $this->language = ['clx_dummy_entry_to_prevent_loading_lang_data' => ''];

        // enable debugging only if DBG_DEBUG is set
        $this->Debugoutput = function($str, $level) {
            \DBG::debug($str);
        };
        if (\DBG::getMode() & \DBG_DEBUG) {
            $this->SMTPDebug = \PHPMailer\PHPMailer\SMTP::DEBUG_LOWLEVEL;
        }

        // set charset to be used for emails
        $this->CharSet = CONTREXX_CHARSET;

        // use email validation algorithm of cloudrexx
        // to validate email addresses
        static::$validator = function ($address) {
            return \FWValidator::isEmail($address);
        };

        $this->XMailer = ' ';

        // force using SMTP instead of php's native mail() function
        // see https://github.com/PHPMailer/PHPMailer/discussions/3022
        $this->isSMTP();

        $cx = \Cx\Core\Core\Controller\Cx::instanciate();
        try {
            $this->selectSmtpAccount(
                $cx->getConfig()->getBaseConfig('coreSmtpServer')
            );
        } catch (\Exception $e) {
            \DBG::debug($e->getMessage());
        }

        $this->initClientIp();
    }

    /**
     * Select the SMTP server ({@see \SmtpSettings::getSmtpAccounts()}) to use
     * for sending e-mails.
     *
     * @param int $id ID of SMTP server
     * @throws \Exception If selection failed.
     */
    public function selectSmtpAccount(int $id): void {
        // reset to default and abort in case no custom SMTP server is set
        if (empty($id)) {
            $reflector = new \ReflectionClass($this);
            $defaultProperties = $reflector->getDefaultProperties();
            foreach (['Host', 'Port', 'SMTPAuth', 'Username', 'Password'] as $prop) {
                $this->$prop = $defaultProperties[$prop];
            }
            $this->selectedSmtpServerId = $id;
            return;
        }

        // abort in case custom SMTP server is non-existant
        $arrSmtp = \SmtpSettings::getSmtpAccount($id);
        if (!$arrSmtp) {
            throw new \Exception('Invalid SMTP account by ID ' . $id);
        }
        $this->selectedSmtpServerId = $id;

        // set custom SMTP server
        $this->isSMTP();
        $this->Host = $arrSmtp['hostname'];
        $this->Port = $arrSmtp['port'];

        // abort in case no SMTP authentication is set
        if (empty($arrSmtp['username'])) {
            return;
        }

        // set SMTP authentication
        $this->SMTPAuth = true;
        $this->Username = $arrSmtp['username'];
        $this->Password = $arrSmtp['password'];
    }

    /**
     * Get the name (label) of the currently selected SMTP server
     * used for sending e-mails.
     *
     * @return string Name of selected SMTP server
     */
    public function getSmtpName(): string {
        $smtpAccounts = \SmtpSettings::getSmtpAccounts();
        return $smtpAccounts[$this->selectedSmtpServerId]['name'];
    }

    /**
     * @inheritDoc
     */
    protected function addAnAddress($kind, $address, $name = '') {
        \DBG::log('MailTemplate (' . $this->clientIp . '): add address (' . $kind . '): ' . $name . ' <' . $address . '>');
        $cx = \Cx\Core\Core\Controller\Cx::instanciate();
        $cx->getEvents()->triggerEvent(
            'MailTemplate.Mail:addAddress',
            [
                [
                    'kind' => $kind,
                    'address' => $address,
                    'name' => $name,
                    'mail' => $this,
                ],
            ]
        );
        parent::addAnAddress($kind, $address, $name);
    }

    /**
     * @inheritDoc
     */
    public function preSend() {
        if (!$this->verifySender()) {
            \DBG::log('MailTemplate (' . $this->clientIp . '): send aborted (sender domain not verified)');
            return false;
        }
        $this->parseEsiFunctions();
        $this->makeImagesAbsoluteInHtmlBody();
        $this->parseNodePlaceholders();
        try {
            $this->signWithDkim();
        } catch (\Exception $e) {
            \DBG::debug($e->getMessage());
            if ($this->requiresDkimSigning()) {
                \DBG::log('MailTemplate (' . $this->clientIp . '): send aborted (sender domain requires DKIM)');
                return false;
            }
        }
        $status = parent::preSend();
        if ($status === false) {
            \DBG::log('MailTemplate (' . $this->clientIp . '): init failed');
            \DBG::debug($this->ErrorInfo);
        }
        \DBG::log('MailTemplate (' . $this->clientIp . '): ' . $this->getLastMessageID());
        return $status;
    }

    /**
     * @inheritDoc
     */
    public function postSend() {
        \DBG::log(
            sprintf(
                'MailTemplate (%s): from "%s <%s>": %s',
                $this->clientIp,
                $this->FromName,
                $this->From,
                $this->Subject
            )
        );
        // emulate successful send process, but do not actually execute it
        // to let spammers believe its all working fine
        if ($this->softFail) {
            \DBG::log('MailTemplate (' . $this->clientIp . '): send aborted (soft fail)');
            return true;
        }
        $this->exceptions = true;
        try {
            $status = parent::postSend();
        } catch (\Exception $e) {
            $status = false;
            \DBG::debug($e->getMessage());
        }
        $this->exceptions = false;
        if ($status !== false) {
            \DBG::log(
                sprintf(
                    'MailTemplate (%s): sent by %s user',
                    $this->clientIp,
                    \FWUser::getFWUserObject()->objUser->isLoggedIn() ? 'authenticated' : 'anonymous'
                )
            );
        } else {
            \DBG::log('MailTemplate (' . $this->clientIp . '): send failed');
            \DBG::debug($this->ErrorInfo);
        }
        return $status;
    }

    /*
     * Validates $path to be a local media resource
     * {@inheritDoc}
     */
    public function addAttachment(
        $path,
        $name = '',
        $encoding = self::ENCODING_BASE64,
        $type = '',
        $disposition = 'attachment'
    ) {
        if (!$this->isAllowedAttachment($path)) {
            \DBG::log(__METHOD__ . ' (' . $this->clientIp . '): not allowed: ' . $path);
            return false;
        }
        return parent::addAttachment($path, $name, $encoding, $type, $disposition);
    }

    /*
     * Validates $path to be a local media resource
     * {@inheritDoc}
     */
    public function addEmbeddedImage(
        $path,
        $cid,
        $name = '',
        $encoding = self::ENCODING_BASE64,
        $type = '',
        $disposition = 'inline'
    ) {
        if (!$this->isAllowedAttachment($path)) {
            \DBG::log(__METHOD__ . ' (' . $this->clientIp . '): not allowed: ' . $path);
            return false;
        }
        return parent::addEmbeddedImage($path, $cid, $name, $encoding, $type, $disposition);
    }

    /**
     * Verify if $path is a valid attachment
     *
     * A valid attachment is a file located in a non-privileged filesystem location.
     * See {@see \Cx\Lib\FileSystem\FileSystemFile::$privileged}.
     *
     * @return boolean  True if $path is a file located in one of the
     * allowed locations.
     */
    protected function isAllowedAttachment($path) {
        try {
            new \Cx\Lib\FileSystem\File($path);
            return true;
        } catch (\Cx\Lib\FileSystem\FileSystemException $e) {
            return false;
        }
    }

    /**
     * Enable {@see static::$softFail}
     */
    public function softFail() {
        $this->softFail = true;
    }

    /**
     * Sets if full sender authentication is required.
     *
     * Full sender authentication implies that the sender domain
     * (from {@see static:$From}) must be verified by SPF, DKIM and in
     * alignement with DMARC. If `$requires` set to `false`, then only one of
     * SPF or DKIM must be verified and DMARC alignmement is not required.
     *
     * @param bool $requires Set to `true` to require full sender authentication
     */
    public function requiresFullSenderAuthentication(bool $requires = true): void {
        $this->requiresFullSenderAuthentication = $requires;
    }

    /**
     * Check if DKIM signing is required for this instance.
     *
     * @return bool `true` if DKIM signing is required.
     */
    protected function requiresDkimSigning(): bool {
        if ($this->ignoreSenderValidation) {
            return false;
        }
        return $this->selectedSmtpServerId
            || $this->requiresFullSenderAuthentication;
    }

    /**
     * Sets if sender authentication is required.
     *
     * Sender authentication implies that the sender domain
     * (from {@see static:$From}) must be verified by SPF or DKIM.
     * If `$ignore` is set to `false`, then no sender validation is performed.
     * WARNING: Setting `$ignore` to `true` is highly discouraged!
     *
     * @param bool $ignore Set to `true` to ignore sender authentication.
     *    This will also set {@see static::$requiresFullSenderAuthentication} to `false`.
     */
    public function ignoreSenderValidation(bool $ignore = true): void {
        $this->ignoreSenderValidation = $ignore;
        if ($ignore) {
            $this->requiresFullSenderAuthentication = false;
        }
    }

    /**
     * Verify if the domain used by {@see static::From} and
     * {@see static::Sender} is properly been authenticated
     *
     * @return bool Returns `true` if sender domain is authenticated.
     *     Otherwise `false`.
     */
    protected function verifySender(): bool {
        if ($this->ignoreSenderValidation) {
            return true;
        }
        $_ARRAYLANG = \Env::get('init')->getComponentSpecificLanguageData('MailTemplate', false);

        $parts = explode('@', $this->From);
        $domains = [array_pop($parts)];
        $parts = explode('@', $this->Sender);
        $domains[] = array_pop($parts);
        $domains = array_unique($domains);

        $cx = \Cx\Core\Core\Controller\Cx::instanciate();
        $mailTemplate = $cx->getComponent('MailTemplate');
        $netManager = $cx->getComponent('NetManager');
        foreach ($domains as $domainName) {
            try {
                if (empty($domainName)) {
                    return false;
                }
                if (
                    $mailTemplate->isDomainMailCompliant(
                        $domainName,
                        $this->selectedSmtpServerId,
                        $this->requiresFullSenderAuthentication
                    )
                ) {
                    continue;
                }
                $domain = $netManager->getDomainMailComplianceDetails($domainName);
                // Log current state until the sender validation issue
                // has been identified. See CLX-5697
                \DBG::dump($domain);
                \DBG::dump($this->selectedSmtpServerId);
                \DBG::dump($this->requiresFullSenderAuthentication);
                \DBG::dump(file_get_contents(
                    $cx->getWebsiteCachePath() . '/Net/DNS.yml'
                ));
                \DBG::dump(file_get_contents(
                    $cx->getWebsiteCachePath() . '/NetManager/TrackedSenderDomains.yml'
                ));
                $link = new \Cx\Core\Html\Model\Entity\HtmlElement('a');
                $link->setAttribute('href', $domain['editUrl']);
                $link->addChild(
                    new \Cx\Core\Html\Model\Entity\TextElement(
                        $_ARRAYLANG['TXT_CORE_MAILTEMPLATE_SENDER_AUTHENTICATION_SETTINGS']
                    )
                );
                $this->ErrorInfo = sprintf(
                    $_ARRAYLANG['TXT_CORE_MAILTEMPLATE_DOMAIN_NOT_COMPLIANT'],
                    $domainName,
                    $link
                );
                return false;
            } catch (\Exception $e) {
                \DBG::debug($e->getMessage());
                return false;
            }
        }
        return true;
    }

    /**
     * Parse esi functions in body
     */
    protected function parseEsiFunctions(): void {
        $cx = \Cx\Core\Core\Controller\Cx::instanciate();
        $this->Body = $cx->getComponent('Cache')->parseEsiVars(
            $this->Body,
            \Cx\Core_Modules\Cache\Controller\CacheLib::PARSE_ESI_FUNCTIONS
        );
        $this->AltBody = $cx->getComponent('Cache')->parseEsiVars(
            $this->AltBody,
            \Cx\Core_Modules\Cache\Controller\CacheLib::PARSE_ESI_FUNCTIONS
        );
    }

    /**
     * Converts `src` attributes of `img` HTML tags in the HTML-body {@see static::$Body}
     * into absolute URLs.
     */
    protected function makeImagesAbsoluteInHtmlBody() {
        $images = array();
        preg_match_all(
            '/src=("[^"]*"|\'[^\']*\')/',
            $this->Body,
            $images,
            PREG_PATTERN_ORDER
        );
        foreach ($images[1] as $match) {
            $src = trim(
                $match,
                '"\''
            );
            if (
                !empty($src[0])
                && $src[0] === '/'
            ) {
                $src = \Cx\Core\Routing\Url::fromMagic(
                    $src
                );
            }
            $url = new \Cx\Core\Routing\Url(
                $src,
                true
            );
            if ($url->isInternal()) {
                $absoluteSrc = $url->toString();
            } else {
                $absoluteSrc = $src;
            }
            $this->Body = preg_replace(
                '/(["\'])' . preg_quote((string)$src, '/') . '\1/',
                '\1' . contrexx_raw2encodedUrl($absoluteSrc) . '\1',
                $this->Body
            );
        }
    }

    /**
     * Replaces any {@see \Cx\Core\Routing\NodePlaceholder} by its
     * target URL in the HTML-body ({@see static::Body}).
     */
    protected function parseNodePlaceholders(): void {
        $this->Body = preg_replace(
            '/\[\[(NODE_[A-Z0-9]+(?:_[A-Z0-9_-]+)?)\]\]/',
            '{\\1}',
            $this->Body
        );
        \LinkGenerator::parseTemplate($this->Body, true);
    }

    /**
     * Signs the mail with DKIM in case DKIM has been set up for the domain
     * of {@see static::$From}.
     *
     * @throw \Exception In case DKIM signing fails.
     */
    protected function signWithDkim(): void {
        $parts = explode('@', $this->From);
        $domain = array_pop($parts);

        $cx = \Cx\Core\Core\Controller\Cx::instanciate();
        $info = $cx->getComponent('NetManager')->getDomainMailComplianceDetails($domain);
        if (!$info['dkim']['isValid']) {
            throw new \Exception('DKIM-setup is not valid for sender domain ' . $domain);
        }
        $dkimData = $this->getDkimKey();
        $this->DKIM_domain = $domain;
        $this->DKIM_private_string = $dkimData['key'];
        $this->DKIM_selector = $dkimData['selector'];
        $this->DKIM_extraHeaders = ['List-Unsubscribe', 'List-Unsubscribe-Post'];
        $this->DKIM_copyHeaderFields = (bool) $cx->getComponent('Workbench');
    }

    /**
     * Get the current active DKIM key and selector to use for signing
     *
     * @return array Array with two keys, 'selector' and 'key'.
     * @throws \Exception In case no active DKIM key can be loaded
     */
    protected function getDkimKey(): array{
        $cx = \Cx\Core\Core\Controller\Cx::instanciate();
        $files = glob(
            $cx->getCodeBaseCorePath() . '/MailTemplate/Data/DkimKeys/*.cloudrexx[123].private.pem',
            \GLOB_NOSORT
        );
        if (!$files) {
            throw new \Exception('No DKIM key found');
        }
        sort($files, \SORT_NUMERIC);
        $currentKey = array_pop($files);
        if (!preg_match('#/([0-9-]+\.cloudrexx[1-3])\.private\.pem$#', $currentKey, $match)) {
            throw new \Exception('Invalid selector format: '. $currentKey);
        }
        try {
            $file = new \Cx\Lib\FileSystem\PrivilegedFile($currentKey);
            $key = $file->getData();
            return [
                'selector' => $match[1],
                'key' => $key,
            ];
        } catch (\Cx\Lib\FileSystem\FileSystemException $e) {
            \DBG::msg($e->getMessage());
            throw new \Exception('Unable to read DKIM key from ' . $currentKey);
        }
    }

    /**
     * Determine the IP (using {@see \Cx\Core\Routing\Model\Entity\Request::getClientIp()})
     * of this request and cache it in {@see static::$clientIp}.
     * If request is a CLI-call, then {@see static::$clientIp} will be set to
     * 'cli'. If the request was made using an invalid IP (see
     * {@see \Cx\Core\Routing\Model\Entity\Request::getClientIp()}), then
     * {@see static::$clientIp} will be set to 'unknown'.
     */
    protected function initClientIp(): void {
        try {
            $this->clientIp = \Cx\Core\Routing\Model\Entity\Request::getClientIp();
            return;
        } catch (\Exception $e) {
            $cx = \Cx\Core\Core\Controller\Cx::instanciate();
            if ($cx->isCliCall()) {
                $this->clientIp = 'cli';
                return;
            }
        }
        $this->clientIp = 'unknown';
    }
}
