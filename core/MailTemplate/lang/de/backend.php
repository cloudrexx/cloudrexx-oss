<?php
declare(strict_types=1);

/**
 * Cloudrexx
 *
 * @link      https://www.cloudrexx.com
 * @copyright Cloudrexx AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * @copyright   Cloudrexx AG
 * @author      Thomas Wirz <thomas.wirz@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  core_mailtemplate
 */
$_ARRAYLANG['TXT_CORE_MAILTEMPLATE_SENDER_AUTHENTICATION_SETTINGS'] = 'Absender-Authentifizierungseinstellungen';
$_ARRAYLANG['TXT_CORE_MAILTEMPLATE_DOMAIN_NOT_COMPLIANT'] = 'Die Domain %1$s wurde nicht verifiziert oder authentifiziert. Bitte überprüfen Sie die %2$s, um sicherzustellen, dass Sie berechtigt sind, E-Mails von dieser Domain zu senden.';

$_ARRAYLANG['TXT_CORE_MAILTEMPLATE_VALIDATE'] = 'Verifizieren?';
$_ARRAYLANG['TXT_CORE_MAILTEMPLATE_STATE_INVALID'] = 'Nicht verifiziert';
$_ARRAYLANG['TXT_CORE_MAILTEMPLATE_STATE_VALID'] = 'Verifiziert';
$_ARRAYLANG['TXT_CORE_MAILTEMPLATE_INVALID_ADDRESS'] = 'Ungültige Adresse';
