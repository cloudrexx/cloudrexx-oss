<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * Main controller for MailTemplate
 *
 * @copyright   Cloudrexx AG
 * @author      Thomas Wirz <thomas.wirz@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  core_mailtemplate
 */

namespace Cx\Core\MailTemplate\Controller;

/**
 * Main controller for Routing
 *
 * @copyright   Cloudrexx AG
 * @author      Thomas Wirz <thomas.wirz@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  core_mailtemplate
 */
class ComponentController extends \Cx\Core\Core\Model\Entity\SystemComponentController implements \Cx\Core\Json\JsonAdapter {
    public function getControllerClasses() {
        // Return an empty array here to let the component handler know that there
        // does not exist a backend, nor a frontend controller of this component.
        return [];
    }

    /**
     * @inheritDoc
     */
    public function getControllersAccessableByJson() {
        return array(
            'ComponentController'
        );
    }

    /**
     * Returns the internal name used as identifier for this adapter
     * @return String Name of this adapter
     */
    public function getName() {
        return parent::getName();
    }

    /**
     * @inheritDoc
     */
    public function getAccessableMethods() {
        return array(
            'isAddressAuthenticated',
        );
    }

    /**
     * @inheritDoc
     */
    public function getMessagesAsString() {
        return '';
    }

    /**
     * @inheritDoc
     */
    public function getDefaultPermissions() {
        return new \Cx\Core_Modules\Access\Model\Entity\Permission();
    }

    /**
     * {@inheritdoc}
     */
    public function registerEvents() {
        $this->cx->getEvents()->addEvent(
            $this->getName() . '.Mail:addAddress'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function postInit(\Cx\Core\Core\Controller\Cx $cx) {
        $smtpServer = getenv('CLX_SMTP_MTA_HOST');
        if ($smtpServer === false) {
            return;
        }
        putenv('SMTPSERVER=' . $smtpServer);
    }

    /**
     * @inheritDoc
     */
    public function preFinalize(\Cx\Core\Html\Sigma $template) {
        if (!$this->cx->isBackendMode()) {
            return;
        }
        \JS::registerJS($this->getDirectory(false, true) . '/View/Script/SenderAddressValidation.js');
        \JS::registerCSS($this->getDirectory(false, true) . '/View/Style/SenderAddressValidation.css');
        $_ARRAYLANG = \Env::get('init')->getComponentSpecificLanguageData($this->getName(), false);
        \ContrexxJavascript::getInstance()->setVariable(
            [
                'TXT_CORE_MAILTEMPLATE_VALIDATE' => $_ARRAYLANG['TXT_CORE_MAILTEMPLATE_VALIDATE'],
                'TXT_CORE_MAILTEMPLATE_STATE_VALID' => $_ARRAYLANG['TXT_CORE_MAILTEMPLATE_STATE_VALID'],
                'TXT_CORE_MAILTEMPLATE_STATE_INVALID' => $_ARRAYLANG['TXT_CORE_MAILTEMPLATE_STATE_INVALID'],
                'TXT_CORE_MAILTEMPLATE_INVALID_ADDRESS' => $_ARRAYLANG['TXT_CORE_MAILTEMPLATE_INVALID_ADDRESS'],
            ],
            $this->getName() . '/lang'
        );
    }

    public function isDomainMailCompliant(string $domainName, int $smtpServerId = -1, bool $strict = true): bool {
        $requireDkimOnly = false;
        if ($smtpServerId < 0) {
            $smtpServerId = $this->cx->getConfig()->getBaseConfig('coreSmtpServer');
        }
        if ($smtpServerId) {
            $requireDkimOnly = true;
        }
        $domain = $this->getComponent('NetManager')->getDomainMailComplianceDetails($domainName);
        if ($requireDkimOnly && $strict) {
            return $domain['dkim']['isValid'] && $domain['dmarc']['isValid'];
        }
        if ($strict) {
            return $domain['compliant'];
        }
        if ($requireDkimOnly) {
            return $domain['dkim']['isValid'];
        }
        return $domain['spf']['isValid'] || $domain['dkim']['isValid'];
    }

    /**
     * Checkes whether a mail address is of a compliant domain
     *
     * @param array Get param "address" needs to be set to a valid e-mail address
     * @return array Array with indexes "compliant" (bool) and "editUrl" (string) set
     */
    public function isAddressAuthenticated(array $params): array {
        $address = $params['get']['address'] ?? '';
        if (!\FWValidator::isEmail($address)) {
            throw new \Exception('Invalid address');
        }
        $addressParts = explode('@', $address);
        $domain = array_pop($addressParts);
        $validation = $params['get']['validation'] ?? '';
        $strict = $validation === 'strict';
        $smtpServerId = (int) ($params['get']['smtpServerId'] ?? -1);
        $complianceDetails = $this->getComponent('NetManager')->getDomainMailComplianceDetails($domain);
        $result = [
            'compliant' => $this->isDomainMailCompliant($domain, $smtpServerId, $strict),
        ];
        if (\Permission::checkAccess(196, 'static', true)) {
            $result['editUrl'] = $complianceDetails['editUrl'];
        }
        return $result;
    }

    /**
     * Check if there is a incompliant sender domain in use
     *
     * @return bool `true` if there is at least one acitve sender domain
     *     present that is not in compliance with sender guidelines.
     */
    public function hasIncompliantSenderDomains(): bool {
        $forceReload = false;
        $iteration = 0;
        while ($iteration++ < 2) {
            $domains = $this->getComponent('NetManager')->getTrackedSenderDomains([], $forceReload);
            foreach ($domains as $senderDomain) {
                // 'usages' is not set in case the sender validation cache has
                // been generated during a mail sending process.
                // therefore we have to reload the sender domain validation to
                // load all usages.
                if (!isset($senderDomain['usages'])) {
                    $forceReload = true;
                    continue 2;
                }
                if (!count($senderDomain['usages'])) {
                    continue;
                }
                if (
                    !$this->isDomainMailCompliant(
                        $senderDomain['name'],
                        -1,
                        $senderDomain['strict']
                    )
                ) {
                    return true;
                }
            }
            break;
        }
        return false;
    }
}
