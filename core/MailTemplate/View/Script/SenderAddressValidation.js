function debounce(fn, delay) {
    var timer = null;
    return function () {
        var context = this, args = arguments;
        clearTimeout(timer);
        timer = setTimeout(function () {
            fn.apply(context, args);
        }, delay);
    };
}
cx.ready(function(){
    // TODO: This should be moved to cx.ui.dialog directly
    let currentFocusElement = null;
    window.addEventListener("focusin", function(e) {
        if (e.target.classList.contains("cxDialog")) {
            return;
        }
        currentFocusElement = e.target;
    });
    document.querySelectorAll(
        'input.senderAddress'
    ).forEach((senderAddress) => {
        const senderAddressBox = document.createElement('div');
        senderAddress.parentNode.appendChild(senderAddressBox);
        senderAddressBox.appendChild(senderAddress);
        const validateAddress = function(input) {
            if (!input.dataset.state) {
                input.dataset.state = 'notValidated';
            }
            let statusDiv = input.parentNode.querySelector('div.label');
            let statusText = input.parentNode.querySelector('span');
            let statusLink = input.parentNode.querySelector('a');
            let inputValue = input.value;
            if (inputValue === '' && input.getAttribute('placeholder') !== null) {
                inputValue = input.getAttribute('placeholder');
            }
            if (inputValue === '') {
                if (statusDiv) {
                    statusDiv.remove();
                }
                if (statusLink) {
                    statusLink.remove();
                }
                if (statusText) {
                    statusText.remove();
                }
                return;
            }
            const handleResponse = function(responseDomain, data, state) {
                if (responseDomain != inputValue) {
                    // This reponse is for an outdated value, discard
                    return;
                }
                // Note: Even if state did not change we might need to update edit link
                if (!statusDiv) {
                    statusDiv = document.createElement('div');
                    statusDiv.classList.add('label');
                    input.parentNode.appendChild(statusDiv);
                }
                switch (state) {
                    case 'error':
                        input.dataset.state = 'error';
                        statusDiv.classList.remove('oklabel');
                        statusDiv.classList.add('alertlabel');
                        statusDiv.textContent = cx.variables.get('TXT_CORE_MAILTEMPLATE_INVALID_ADDRESS', 'MailTemplate/lang');
                        if (statusLink) {
                            statusLink.remove();
                        }
                        if (statusText) {
                            statusText.remove();
                        }
                        break;
                    case 'valid':
                        input.dataset.state = 'valid';
                        statusDiv.classList.remove('alertlabel');
                        statusDiv.classList.add('oklabel');
                        statusDiv.textContent = cx.variables.get('TXT_CORE_MAILTEMPLATE_STATE_VALID', 'MailTemplate/lang');
                        if (statusLink) {
                            statusLink.remove();
                        }
                        if (statusText) {
                            statusText.remove();
                        }
                        break;
                    case 'invalid':
                        input.dataset.state = 'invalid';
                        statusDiv.classList.remove('oklabel');
                        statusDiv.classList.add('alertlabel');
                        statusDiv.textContent = cx.variables.get('TXT_CORE_MAILTEMPLATE_STATE_INVALID', 'MailTemplate/lang');
                        if (!data.editUrl) {
                            if (statusLink) {
                                statusLink.remove();
                            }
                            break;
                        }
                        if (!statusLink) {
                            statusText = document.createElement('span');
                            statusText.innerHTML = cx.variables.get('TXT_CORE_MAILTEMPLATE_VALIDATE', 'MailTemplate/lang');
                            input.parentNode.appendChild(statusText);
                            statusLink = document.createElement('a');
                            statusLink.setAttribute('target', '_blank');
                            input.parentNode.appendChild(statusLink);
                            const statusLinkImg = document.createElement('i');
                            statusLinkImg.setAttribute('class', 'fa-solid fa-arrow-up-right-from-square')
                            statusLink.appendChild(statusLinkImg);
                        }
                        statusLink.setAttribute('href', data.editUrl + '&csrf=' + cx.variables.get('csrf', 'contrexx'));
                        break;
                }
            }
            input.dataset.address = inputValue;
            cx.ajax(
                "MailTemplate",
                "isAddressAuthenticated",
                {
                    data: {
                        address: inputValue,
                        validation: input.dataset.senderValidation,
                        smtpServerId: input.dataset.smtpServerId
                    },
                    beforeSend: function() {
                        cx.ui.messages.showLoad();
                        if (currentFocusElement) {
                            currentFocusElement.focus();
                        }
                    },
                    showMessage: true,
                    postError: function() {
                        // there is some timing issue which requires us to call this twice
                        cx.ui.messages.removeAll();
                        cx.ui.messages.removeAll();
                        handleResponse(this.data.address, {}, "error");
                        if (currentFocusElement) {
                            currentFocusElement.focus();
                        }
                    },
                    postSuccess: function(json) {
                        let state = "invalid";
                        if (json.data.compliant) {
                            state = 'valid';
                        }
                        handleResponse(this.data.address, json.data, state);
                        if (currentFocusElement) {
                            currentFocusElement.focus();
                        }
                    },
                }
            );
        }
        senderAddress.addEventListener(
            "input",
            debounce(
                function(e) {
                    validateAddress(e.target);
                },
                250
            )
        );
        validateAddress(senderAddress);
    });
});
