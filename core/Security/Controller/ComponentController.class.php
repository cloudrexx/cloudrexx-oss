<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * Main controller for Security
 *
 * @copyright   CLOUDREXX CMS - CLOUDREXX AG
 * @author      Gerben van der Lubbe <spoofedexistence@gmail.com>
 * @author      Ivan Schmid <ivan.schmid@comvation.com>
 * @package     cloudrexx
 * @subpackage  core_security
 */

namespace Cx\Core\Security\Controller;

/**
 * Main controller for Security
 *
 * The security class checks for possible attacks to the server
 * and supports a few functions to make everything more secure.
 * @copyright   CLOUDREXX CMS - CLOUDREXX AG
 * @author      Gerben van der Lubbe <spoofedexistence@gmail.com>
 * @author      Ivan Schmid <ivan.schmid@comvation.com>
 * @package     cloudrexx
 * @subpackage  core_security
 */
class ComponentController extends \Cx\Core\Core\Model\Entity\SystemComponentController {

    /**
     * Title of the active page
     * @var boolean
     */
    public $reportingMode = false;

    /**
     * $_SERVER variable indexes used by Cloudrexx
     *
     * @var array
     */
    public $criticalServerVars = array (
        'DOCUMENT_ROOT',
        'HTTPS',
        'HTTP_ACCEPT_LANGUAGE',
        'HTTP_CLIENT_IP',
        'HTTP_HOST',
        'HTTP_REFERER',
        'HTTP_USER_AGENT',
        'HTTP_VIA',
        'HTTP_X_FORWARDED_FOR',
        'PHP_SELF',
        'QUERY_STRING',
        'REMOTE_ADDR',
        'REQUEST_URI',
        'SCRIPT_FILENAME',
        'SCRIPT_NAME',
        'SCRIPT_URI',
        'SERVER_ADDR',
        'SERVER_NAME',
        'SERVER_PORT',
        'SERVER_PROTOCOL',
        'SERVER_SOFTWARE',
        'argv',
    );

    /**
     * API command name for reporting CSP violations
     *
     * @param string
     */
    protected const API_COMMAND_REPORT_CSP = 'ReportCsp';

    /**
     * Host of CLX resources
     *
     * @param string
     */
    protected const CLX_MEDIA_CDN = 'https://media.cloudrexx.com/';

    /**
     * Whether the (header for the) Content Security Policy has been set for
     * the current request.
     *
     * @var bool
     */
    protected bool $isCspSet = false;

    /**
     * Defines mappings of Content Security Policy (CSP) directives to specific HTML tags and their children.
     * This associative array is structured to facilitate the dynamic application of CSP rules based on HTML content structure.
     *
     * Each key at the first level represents a CSP directive, such as 'frame-src' or 'media-src'. The values associated
     * with these keys are arrays that detail the HTML elements (tags) to which the CSP directive applies.
     *
     * At the second level, the array either lists HTML tag names directly under the directive (e.g., 'frame', 'iframe' for 'frame-src')
     * or specifies parent tags that have further nested child tags defined in a third level. This nested structure allows
     * for specifying CSP rules more granularly for complex HTML elements like media elements, which can contain child tags.
     *
     * For instance, under 'media-src', the parent tags ('audio', 'video') each have their own arrays listing child tags
     * ('source', 'track') that should also adhere to the CSP rules defined for the parent.
     *
     * Structure:
     * [
     *     'directive-name' => [
     *         'tag-name',                           // Direct mapping for simple tags
     *         'parent-tag' => ['child-tag', ...]    // Parent-child mapping for complex tags
     *     ],
     *     ...
     * ]
     *
     * This approach ensures that the CSP rules are applied precisely where needed, enhancing security while maintaining flexibility.
     *
     * @var array
     */
    protected array $cspSourceTags = [
        'frame-src' => [
            'frame',
            'iframe',
        ],
        'media-src' => [
            'audio' => [
                'source',
                'track',
            ],
            'video' => [
                'source',
                'track',
            ],
        ]
    ];

    /**
     * An array that stores CSP compliant sources extracted from content
     * {@see static::fetchTrustedCspSources()} based on the defined CSP tags
     * {@see static::$cspSourceTags} or manualy set through
     * {@see static::registerTrustedCspSource()}.
     * Each source is of the form <protocol>://<domain>.
     *
     * @var array
     */
    protected array $cspSources = [];

    /**
     * Stores unique identifiers for each CSP source tag that has been processed.
     * These identifiers are used to verify and validate the trusted sources.
     *
     * @var array
     */
    protected array $cspSourceIds = [];

    /**
     * Used as internal index for each trusted CSP source
     *
     * @var int
     */
    protected static int $cspSourceIndex = 0;

    /**
     * Indicates whether 'unsafe-inline' should be allowed for script sources in CSP.
     * This can be a security risk and should be enabled only when absolutely necessary.
     *
     * @var bool
     */
    protected bool $useCspScriptSrcUnsafeInline = false;

    /**
     * Indicates whether data URLs should be allowed for image sources in CSP.
     * Enabling this allows images to be sourced directly from data URLs.
     *
     * @var bool
     */
    protected bool $useCspImgSrcData = false;

    /**
     * Indicates whether 'self' should be allowed for worker sources in CSP.
     * By default, no worker sources are allowed.
     *
     * @var bool
     */
    protected bool $useCspWorkerSrcSelf = false;

    public function getControllerClasses() {
        // Return an empty array here to let the component handler know that there
        // does not exist a backend, nor a frontend controller of this component.
        return array();
    }

    /**
     * @inheritDoc
     */
    public function getCommandsForCommandMode() {
        return [
            static::API_COMMAND_REPORT_CSP => new \Cx\Core_Modules\Access\Model\Entity\Permission(
                ['https'],
                ['post'],
                false
            ),
        ];
    }

    /**
     * @inheritDoc
     */
    public function getCommandDescription($command, $short = false) {
        switch ($command) {
            case static::API_COMMAND_REPORT_CSP:
                return 'Process Content Security Policy violation';

            default:
                return '';
        }
    }

    /**
     * @inheritDoc
     */
    public function executeCommand($command, $arguments, $dataArguments = array()) {
        switch ($command) {
            case static::API_COMMAND_REPORT_CSP:
                $this->ReportCsp($dataArguments);
                break;
        }
    }

    /**
     * Do something before resolving is done
     *
     * @param \Cx\Core\Routing\Url                      $request    The URL object for this request
     */
    public function preResolve(\Cx\Core\Routing\Url $request) {
        $this->setHSTS();
        $this->blockMimeSniffing();
        $this->scanRequest();
    }

    /**
     * @inheritDoc
     */
    public function postFinalize(&$endcode) {
        $this->setContentSecurityPolicy($endcode);
    }

    /**
     * Configure HTTP header Strict-Transport-Security
     */
    protected function setHSTS() {
        // we must not set HSTS if we're serving over HTTP.
        // see https://www.rfc-editor.org/rfc/rfc6797#section-8.1
        if (ASCMS_PROTOCOL !== 'https') {
            return;
        }

        // force HSTS for one non-leap year, if HTTPS is beeing forced in both,
        // front- and backend
        $forceProtocolBackend = \Cx\Core\Setting\Controller\Setting::getValue(
            'forceProtocolBackend',
            'Config'
        );
        $forceProtocolFrontend = \Cx\Core\Setting\Controller\Setting::getValue(
            'forceProtocolFrontend',
            'Config'
        );
        if (
            $forceProtocolBackend !== 'https'
            || $forceProtocolFrontend !== 'https'
        ) {
            $this->cx->getResponse()->setHeader(
                'Strict-Transport-Security',
                'max-age=0'
            );
            return;
        }
        $hstsIncludeSubDomains = '';
        if (
            \Cx\Core\Setting\Controller\Setting::getValue(
                'hstsIncludeSubDomains',
                'Config'
            ) === 'on'
        ) {
            $hstsIncludeSubDomains = '; includeSubDomains';
        }
        $hstsPreload = '';
        if (
            \Cx\Core\Setting\Controller\Setting::getValue(
                'hstsIncludeSubDomains',
                'Config'
            ) === 'on'
            && \Cx\Core\Setting\Controller\Setting::getValue(
                'hstsPreload',
                'Config'
            ) === 'on'
        ) {
            $hstsPreload = '; preload';
        }
        $this->cx->getResponse()->setHeader(
            'Strict-Transport-Security',
            'max-age=31536000' . $hstsIncludeSubDomains . $hstsPreload
        );
    }

    /**
     * Configure HTTP header Content-Security-Policy
     *
     * @param string $content HTML response to be sent back to the client
     */
    protected function setContentSecurityPolicy(?string &$content) {
        if ($this->isCspSet) {
            return;
        }
        $this->setCspReporting();
        $this->setUpgradeInsecureRequests();
        $this->configureCspFrameAncestors();
        $this->configureCspBaseUri();
        $this->fetchTrustedCspSources($content);
        $this->configureCspDefaultSrc();
        $this->configureCspChildSrc();
        $this->configureCspConnectSrc();
        $this->configureCspFontSrc();
        $this->configureCspFrameSrc();
        $this->configureCspImgSrc();
        $this->configureCspManifestSrc();
        $this->configureCspMediaSrc();
        $this->configureCspObjectSrc();
        $this->configureCspScriptSrc($content);
        $this->configureCspStyleSrc($content);
        $this->configureCspWorkerSrc();
        $this->isCspSet = true;
    }

    /**
     * Set Content-Security-Policy upgrade-insecure-requests if
     * HTTPS is enforced.
     */
    protected function setUpgradeInsecureRequests(): void {
        if ($this->getSupportedProtocols() !== 'https') {
            return;
        }
        if (
            \Cx\Core\Setting\Controller\Setting::getValue(
                'upgradeInsecureRequests',
                'Config'
            ) !== 'on'
        ) {
            return;
        }
        $this->cx->getResponse()->setContentSecurityPolicyDirective(
            'upgrade-insecure-requests',
            []
        );
    }

    /**
     * Returns the value of the setting option forceProtocolFrontend if we're
     * in frontend or otherwise the value of option forceProtocolBackend.
     *
     * @return string One of 'none', 'http' or 'https'
     */
    protected function getSupportedProtocols(): string {
        if ($this->cx->isFrontendMode()) {
            $option = 'forceProtocolFrontend';
        } else {
            $option = 'forceProtocolBackend';
        }
        return $this->cx->getConfig()->getBaseConfig($option);
    }

    /**
     * Configure frame-ancestors directive of the
     * Content-Security-Policy header.
     *
     * This is used to allow or prevent embedding this website.
     * Embedding of the backend is never allowed.
     */
    protected function configureCspFrameAncestors() {
        // check if we should allow embedding in frontend.
        $frontendDenyIframing = \Cx\Core\Setting\Controller\Setting::getValue(
            'frontendDenyIframing',
            'Config'
        );
        if (
               $this->cx->isFrontendMode()
            && $frontendDenyIframing === 'off'
        ) {
            return;
        }

        // now prevent embedding
        if ($this->cx->isFrontendMode()) {
            $protocolOption = 'forceProtocolFrontend';
        } else {
            $protocolOption = 'forceProtocolBackend';
        }
        $forceProtocol = \Cx\Core\Setting\Controller\Setting::getValue(
            $protocolOption,
            'Config'
        );
        $domain = $this->cx->getRequest()->getUrl()->getDomain();
        $sources = array();
        switch ($forceProtocol) {
            case 'http':
                $sources[] = 'http://' . $domain;
                break;

            case 'https':
                $sources[] = 'https://' . $domain;
                break;

            case 'none':
            default:
                $sources[] = 'http://' . $domain;
                $sources[] = 'https://' . $domain;
                break;
        }
        $this->cx->getResponse()->setContentSecurityPolicyDirective(
            'frame-ancestors',
            $sources
        );

        // compatability for older browsers that do not
        // support Content-Security-Policy
        $this->cx->getResponse()->setHeader('X-Frame-Options', 'SAMEORIGIN');
    }

    /**
     * Configure base-uri directive of the
     * Content-Security-Policy header.
     */
    protected function configureCspBaseUri(): void {
        $this->cx->getResponse()->setContentSecurityPolicyDirective(
            'base-uri',
            ['self']
        );
    }

    /**
     * Enable Content Security Policy violation reporting to the client
     */
    protected function setCspReporting(): void {
        $endpointName = 'main-endpoint';
        $endpointUrl = \Cx\Core\Routing\Url::fromApi(
            static::API_COMMAND_REPORT_CSP, []
        );
        // note: the specification only accepts secure endpoints
        // see https://www.w3.org/TR/reporting-1/#header
        $endpointUrl->setProtocol('https');

        // support Firefox
        $this->cx->getResponse()->setContentSecurityPolicyDirective(
            'report-uri',
            [$endpointUrl->toString()]
        );

        // support older Chrome & Edge browsers
        // see https://developer.chrome.com/blog/reporting-api-migration#what_remains_unchanged
        $this->cx->getResponse()->setHeader(
            'report-to',
            json_encode([
                'group' => $endpointName,
                'max_age' => 86400,
                'endpoints' => [['url' => $endpointUrl->toString()]],
            ])
        );

        // support newer Chrome & Edge browsers
        // see https://developer.chrome.com/docs/capabilities/web-apis/reporting-api
        $this->cx->getResponse()->setHeader(
            'reporting-endpoints',
            sprintf(
                '%s="%s"',
                $endpointName,
                $endpointUrl->toString()
            )
        );

        $this->cx->getResponse()->setContentSecurityPolicyDirective(
            'report-to',
            [$endpointName]
        );
    }

    /**
     * Report CSP violation
     */
    protected function ReportCsp(array $report): void {
        try {
            $violations = $this->fetchCspViolations($report);
        } catch (\Exception $e) {
            \DBG::debug($e->getMessage());
            \DBG::dump($report, DBG_DEBUG);
        }
        foreach ($violations as $violation) {
            \DBG::debug('CSP-report submitted for: ' . $violation['url']);
            \DBG::dump($violation['report'], DBG_DEBUG);
        }
    }

    /**
     * Fetch CSP violation reports
     *
     * @param array $report Data from one or more violation reports generated
     *     in response to a reporting-endpoints HTTP header or one CSP violation
     *     report in response to a  report-uri or report-to HTTP header.
     * @return array List of violation reports with each report having the keys
     *     'url' and 'report', where 'url' contains the document URL of the
     *     violation report and 'report' contains the reported violation data.
     */
    protected function fetchCspViolations(array $report): array {
        if (isset($report['csp-report'])) {
            return [$this->fetchViolationFromLegacyCspReport($report['csp-report'])];
        }
        $reports = [];
        foreach ($report as $violation) {
            $type = $violation['type'] ?? '';
            if ($type !== 'csp-violation') {
                continue;
            }
            $reports[] = [
                'url' => $violation['body']['documentURL'],
                'report' => $violation,
            ];
        }
        return $reports;
    }

    /**
     * Fetch legacy CSP violation report
     *
     * @param array $report Data from a CSP violation report generated in response
     *     to a report-uri or report-to HTTP header.
     * @return array Violation report with keys 'url' and 'report', where
     *     'url' contains the document URL of the violation report and 'report'
     *     contains the reported violation data.
     */
    protected function fetchViolationFromLegacyCspReport(array $report): array {
        if (!isset($report['document-uri'])) {
            throw new \Exception('Incomplete CSP-Report submitted');
        }
        return [
            'url' => $report['document-uri'],
            'report' => $report,
        ];
    }

    /**
     * Check if Content Security Policy for script-src shall be set
     *
     * @return bool True if Content Security Policy for script-src is enabled
     *     and shall be set in response.
     */
    public function isCspOnFetchDirectivesEnabled(): bool {
        if (
            $this->cx->isBackendMode()
            && preg_match('#^/cadmin/(Login/.*)?$#', $this->cx->getRequest()->getUrl()->getPathPart())
        ) {
            return true;
        }
        if (!$this->cx->isFrontendMode()) {
            return false;
        }
// TODO: rename option to 'cspForFetchDirectives'
        $cspForScriptSrc = \Cx\Core\Setting\Controller\Setting::getValue(
            'cspForScriptSrc',
            'Config'
        );
        return $cspForScriptSrc === 'on';
    }

    /**
     * Add $source to the Content-Security-Policy $fetchDirective.
     * Set sources are stored in {@see static::$cspSources}.
     *
     * @param string $fetchDirective The fetch directive to add $source to. Can
     *     be one of:
     *     - connect-src
     *     - font-src
     *     - frame-src
     *     - img-src
     *     - manifest-src
     *     - media-src
     *     - style-src
     *     - worker-src
     * @param string $source The source to trust.
     */
    public function registerTrustedCspSource(string $fetchDirective, string $source): void {
        if (
            !in_array(
                $fetchDirective,
                [
                    'connect-src',
                    'font-src',
                    'frame-src',
                    'img-src',
                    'manifest-src',
                    'media-src',
                    'style-src',
                    'worker-src',
                ]
            )
        ) {
            return;
        }
        $this->cspSources[$fetchDirective][] = $source;
    }

    /**
     * Check if Content Security Policy for script-src shall be set
     *
     * @return bool True if Content Security Policy for script-src is enabled
     *     and shall be set in response.
     */
    public function registerTrustedCspSources(string &$content): void {
        if (!$this->isCspOnFetchDirectivesEnabled()) {
            return;
        }
        \JS::markSafeInlineJavaScripts($content);
        \JS::markSafeStyleSources($content);
        $this->markTrustedCspSources($content);
    }

    /**
     * Marks specific HTML tags as trusted for CSP based on the predefined {@see static::$cspSourceTags}.
     * It processes the HTML content, finding tags defined in {@see static::$cspSourceTags} and wrapping them
     * with unique comments that include a dynamically generated ID.
     *
     * @param string &$content The HTML content to be processed. Passed by reference to modify directly.
     */
    protected function markTrustedCspSources(string &$content): void {
        foreach ($this->cspSourceTags as $cspDirective => $sourceTags) {
            $keys = array_keys($sourceTags);
            if (is_int($keys[0])) {
                $tagList = join('|', $sourceTags);
            } else {
                $tagList = join('|', $keys);
            }
            $content = preg_replace_callback(
                '/(<(' . $tagList . ')(?:>|\s[^>]*>))(.*?)(<\/\2>)/is',
                function ($match) use ($cspDirective) {
                    $sourceId = $match[2] . '_' .  ++static::$cspSourceIndex . '_' . md5($match[3]);
                    $this->cspSourceIds[$cspDirective][] = $sourceId;
                    $sourceIdComment = '<!-- ' .  $sourceId . ' -->';
                    return $match[1] . $sourceIdComment . $match[3] . $sourceIdComment . $match[4];
                },
                $content
            );
        }
    }

    /**
     * Fetches trusted CSP sources from the HTML content by identifying previously marked tags
     * with unique IDs and extracting their relevant 'src' attributes to form CSP-compliant URLs.
     * This function further processes nested tags if specified in {@see static::$cspSourceTags}.
     *
     * @param string|null &$content The HTML content to be processed. Passed by reference to modify directly.
     *                              If content is null or empty, the function will return early.
     */
    protected function fetchTrustedCspSources(?string &$content): void {
        if (!$this->isCspOnFetchDirectivesEnabled()) {
            return;
        }
        if (empty($content)) {
            return;
        }
        foreach ($this->cspSourceTags as $cspDirective => $sourceTags) {
            $keys = array_keys($sourceTags);
            $childTagList = '';
            if (is_int($keys[0])) {
                $tagList = join('|', $sourceTags);
            } else {
                $tagList = join('|', $keys);
                $tags = [];
                foreach ($sourceTags as $childTags) {
                    $tags = array_merge($tags, $childTags);
                }
                $childTagList = join('|', $tags);
            }
            $content = preg_replace_callback(
                '/(<(' . $tagList . ')(>|\s[^>]*>))(<!-- (\2_[0-9]+_[a-f0-9]{32}) -->)(.*?)\4(<\/\2>)/is',
                function ($match) use ($cspDirective, $childTagList) {
                    if (
                        isset($this->cspSourceIds[$cspDirective])
                        && in_array($match[5], $this->cspSourceIds[$cspDirective])
                    ) {
                        if (preg_match('/\ssrc\s*=\s*("[^"]+"|\'[^\']+\')[^>]*>/', $match[3], $srcMatch)) {
                            $source = substr($srcMatch[1], 1, -1);
                            $url = \Cx\Core\Routing\Url::fromMagic($source);
                            $this->cspSources[$cspDirective][] = $url->getProtocol() . '://' . $url->getDomain();
                        }
                        if (preg_match_all('/<(?:' . $childTagList . ')(?=\s)[^>]+(?<=\s)src\s*=\s*("[^"]+"|\'[^\']+\')[^>]*>/is', $match[6], $srcMatches, PREG_PATTERN_ORDER)) {
                            foreach ($srcMatches[1] as $srcMatch) {
                                $source = substr($srcMatch, 1, -1);
                                $url = \Cx\Core\Routing\Url::fromMagic($source);
                                $this->cspSources[$cspDirective][] = $url->getProtocol() . '://' . $url->getDomain();
                            }
                        }
                    } else {
                        \DBG::dump($match);
                        \DBG::debug('Tried to register non-trusted <' . $match[2] . '>-tag');
                    }
                    return $match[1] . $match[6] . $match[7];
                },
                $content
            );
        }
    }

    /**
     * Retrieves CSP (Content Security Policy) source definitions for a specific directive from the theme's configuration.
     * This method is designed to fetch CSP settings that are predefined within the active theme's component data.
     * It only operates in the frontend mode of the application, ensuring that CSP data is relevant to the rendered output.
     *
     * @param string $directive The CSP directive for which sources are to be retrieved, e.g., 'style-src', 'img-src'.
     * @return array An array of CSP sources associated with the specified directive. Returns an empty array if
     *     the directive is not set or the system is not in frontend mode.
     *
     * @uses \Cx\Core\Core\Controller\Cx::isFrontendMode() To check if the current mode is frontend.
     * @uses \Cx\Core\Http\Model\Entity\Response::getTheme() To access the theme associated with the current response.
     * @uses \Cx\Core\Html\Sigma\Model\Entity\Theme::getComponentData() To get the component data where CSP settings are stored.
     *
     * @example
     * // Assuming 'style-src' is a directive set in the theme's CSP configuration:
     * $scriptSources = $this->getCspSourcesFromTheme('style-src');
     * // Returns something like ['https://example.com', 'https://cdn.example.com']
     */
    protected function getCspSourcesFromTheme(string $directive): array {
        if (!$this->cx->isFrontendMode()) {
            return [];
        }
        if (!$this->cx->getResponse()->getTheme()) {
            return [];
        }
        $componentData = $this->cx->getResponse()->getTheme()->getComponentData();
        if (!isset($componentData['csp'][$directive])) {
            return [];
        }
        $sources = [];
        foreach ($componentData['csp'][$directive] as $source) {
            if (!is_string($source)) {
                \DBG::debug('Invalid format of CSP source for ' . $directive . ':');
                \DBG::dump($source, DBG_DEBUG);
                continue;
            }
            $sources[] = $source;
        }
        return $sources;
    }

    /**
     * Enables the use of 'unsafe-inline' for script sources in the CSP settings.
     * This setting allows inline scripts to be executed, which can be necessary
     * for certain legacy applications or specific inline script usage but reduces security.
     */
    public function enableCspScriptSrcUnsafeInline(): void {
        $this->useCspScriptSrcUnsafeInline = true;
    }

    /**
     * Enables the use of data URLs for image sources in the CSP settings.
     * This setting permits images to be loaded directly from data URLs,
     * which can be useful for embedded images but might increase exposure to data injection attacks.
     */
    public function enableCspImgSrcData(): void {
        $this->useCspImgSrcData = true;
    }

    /**
     * Enables the use of 'self' for worker sources in the CSP settings.
     * This restricts the sources from which web workers can be loaded to the same origin as the document,
     * enhancing security by preventing the execution of workers from untrusted sources.
     */
    public function enableCspWorkerSrcSelf(): void {
        $this->useCspWorkerSrcSelf = true;
    }

    /**
     * Whether to set Content-Security-Policy-Report-Only in backend or not.
     *
     * @return bool `true` if in backend mode and csp-report-only shall be enabled.
     * @todo    Remove report-only from backend
     */
    protected function useCspReportOnly(): bool {
        if (!$this->cx->isBackendMode()) {
            return false;
        }
        return \FWUser::getFWUserObject()->objUser->isLoggedIn();
    }

    /**
     * Configure default-src directive of the
     * Content-Security-Policy header.
     */
    protected function configureCspDefaultSrc(): void {
        if (!$this->isCspOnFetchDirectivesEnabled()) {
            return;
        }
        $this->cx->getResponse()->setContentSecurityPolicyDirective(
            'default-src',
            ['none'],
            $this->useCspReportOnly()
        );
    }

    /**
     * Configure child-src directive of the
     * Content-Security-Policy header.
     */
    protected function configureCspChildSrc(): void {
        if (!$this->isCspOnFetchDirectivesEnabled()) {
            return;
        }
        $this->cx->getResponse()->setContentSecurityPolicyDirective(
            'child-src',
            // child-src is a fallback for frame-src and worker-src.
            // as we set both directly, this can be set to 'none'
            ['none'],
            $this->useCspReportOnly()
        );
    }

    /**
     * Configure connect-src directive of the
     * Content-Security-Policy header.
     */
    protected function configureCspConnectSrc(): void {
        if (!$this->isCspOnFetchDirectivesEnabled()) {
            return;
        }
        $sources = array_merge(
            // only allow local connections by default
            ['self'],
            $this->getCspSourcesFromTheme('connect-src')
        );
        if (!empty($this->cspSources['connect-src'])) {
            $sources = array_merge(
                $sources,
                $this->cspSources['connect-src']
            );
        }
        $sources = array_unique($sources);
        $this->cx->getResponse()->setContentSecurityPolicyDirective(
            'connect-src',
            $sources,
            $this->useCspReportOnly()
        );
    }

    /**
     * Configure font-src directive of the
     * Content-Security-Policy header.
     */
    protected function configureCspFontSrc(): void {
        if (!$this->isCspOnFetchDirectivesEnabled()) {
            return;
        }
        $sources = array_merge(
            ['self', static::CLX_MEDIA_CDN],
            $this->getCspSourcesFromTheme('font-src')
        );
        if (!empty($this->cspSources['font-src'])) {
            $sources = array_merge(
                $sources,
                $this->cspSources['font-src']
            );
        }
        $sources = array_unique($sources);
        $this->cx->getResponse()->setContentSecurityPolicyDirective(
            'font-src',
            $sources,
            $this->useCspReportOnly()
        );
    }

    /**
     * Configure frame-src directive of the
     * Content-Security-Policy header.
     */
    protected function configureCspFrameSrc(): void {
        if (!$this->isCspOnFetchDirectivesEnabled()) {
            return;
        }
        $sources = $this->getCspSourcesFromTheme('frame-src');
        if (!empty($this->cspSources['frame-src'])) {
            $sources = array_merge(
                $sources,
                $this->cspSources['frame-src']
            );
        }
        $sources = array_unique($sources);
        if (empty($sources)) {
            $sources = ['none'];
        }
        $this->cx->getResponse()->setContentSecurityPolicyDirective(
            'frame-src',
            $sources,
            $this->useCspReportOnly()
        );
    }

    /**
     * Configure img-src directive of the
     * Content-Security-Policy header.
     */
    protected function configureCspImgSrc(): void {
        if (!$this->isCspOnFetchDirectivesEnabled()) {
            return;
        }
        $sources = array_merge(
            ['self', static::CLX_MEDIA_CDN],
            $this->getCspSourcesFromTheme('img-src')
        );
        if (!empty($this->cspSources['img-src'])) {
            $sources = array_merge(
                $sources,
                $this->cspSources['img-src']
            );
        }
        $sources = array_unique($sources);
        if ($this->useCspImgSrcData) {
            $sources[] = 'data:';
        }
        $this->cx->getResponse()->setContentSecurityPolicyDirective(
            'img-src',
            $sources,
            $this->useCspReportOnly()
        );
    }

    /**
     * Configure manifec-src directive of the
     * Content-Security-Policy header.
     */
    protected function configureCspManifestSrc(): void {
        if (!$this->isCspOnFetchDirectivesEnabled()) {
            return;
        }
        $sources = array_merge(
            ['self'],
            $this->getCspSourcesFromTheme('manifest-src')
        );
        if (!empty($this->cspSources['manifest-src'])) {
            $sources = array_merge(
                $sources,
                $this->cspSources['manifest-src']
            );
        }
        $sources = array_unique($sources);
        $this->cx->getResponse()->setContentSecurityPolicyDirective(
            'manifest-src',
            $sources,
            $this->useCspReportOnly()
        );
    }

    /**
     * Configure media-src directive of the
     * Content-Security-Policy header.
     */
    protected function configureCspMediaSrc(): void {
        if (!$this->isCspOnFetchDirectivesEnabled()) {
            return;
        }
        $sources = $this->getCspSourcesFromTheme('media-src');
        if (!empty($this->cspSources['media-src'])) {
            $sources = array_merge(
                $sources,
                $this->cspSources['media-src']
            );
        }
        $sources = array_unique($sources);
        if (empty($sources)) {
            $sources = ['none'];
        }
        $this->cx->getResponse()->setContentSecurityPolicyDirective(
            'media-src',
            $sources,
            $this->useCspReportOnly()
        );
    }

    /**
     * Configure object-src directive of the
     * Content-Security-Policy header.
     */
    protected function configureCspObjectSrc(): void {
        if (!$this->isCspOnFetchDirectivesEnabled()) {
            return;
        }
        $sources = $this->getCspSourcesFromTheme('object-src');
        if (!$sources) {
            $sources = ['none'];
        }
        $this->cx->getResponse()->setContentSecurityPolicyDirective(
            'object-src',
            $sources,
            $this->useCspReportOnly()
        );
    }

    /**
     * Configure script-src directive of the
     * Content-Security-Policy header.
     *
     * @param string $content HTML response to be sent back to the client
     * @todo Extend to backend
     */
    protected function configureCspScriptSrc(?string &$content): void {
        if (!$this->isCspOnFetchDirectivesEnabled()) {
            return;
        }
        if (empty($content)) {
            $sources = [];
        } else {
            // this must be done anyway, even if $this->useCspScriptSrcUnsafeInline is set
            $sources = \JS::hashTrustedCspSources($content);
            $sources = array_unique($sources);
        }
        if ($this->useCspScriptSrcUnsafeInline) {
            $sources = ['self', 'unsafe-inline',];
        } else {
            $sources[] = 'report-sample';
            $sources[] = 'nonce-' . \JS::getCurrentCspNonce();
            $sources[] = 'strict-dynamic';
            // TODO we should migrate all inline events so that we can drop unsafe-hashes
            $sources[] = 'unsafe-hashes';

            // support legacy browsers with 'unsafe-inline' and http(s)://'
            // note: this is ignored when using nonces by current browsers
            $sources[] = 'unsafe-inline';
            switch ($this->getSupportedProtocols()) {
                case 'http':
                    $sources[] = 'http:';
                    break;
                case 'https':
                    $sources[] = 'https:';
                    break;
                default:
                    $sources[] = 'https:';
                    $sources[] = 'http:';
                    break;
            }
        }
        $this->cx->getResponse()->setContentSecurityPolicyDirective(
            'script-src',
            $sources,
            $this->useCspReportOnly()
        );
    }

    /**
     * Configure style-src directive of the
     * Content-Security-Policy header.
     *
     * @param string $content HTML response to be sent back to the client
     */
    protected function configureCspStyleSrc(?string &$content): void {
        if (!$this->isCspOnFetchDirectivesEnabled()) {
            return;
        }
        if (empty($content)) {
            $sources = ['none'];
        } else {
            // note: we're not using nonces for style-src as there are too many
            // inline styles being injected by legacy code & used js libraries
            $sources = array_merge(
                ['self', 'unsafe-inline', static::CLX_MEDIA_CDN,],
                \JS::fetchTrustedCspStyleSources($content),
                $this->getCspSourcesFromTheme('style-src')
            );
            if (!empty($this->cspSources['style-src'])) {
                $sources = array_merge(
                    $sources,
                    $this->cspSources['style-src']
                );
            }
            $sources = array_unique($sources);
        }
        $this->cx->getResponse()->setContentSecurityPolicyDirective(
            'style-src',
            $sources,
            $this->useCspReportOnly()
        );
    }

    /**
     * Configure worker-src directive of the
     * Content-Security-Policy header.
     */
    protected function configureCspWorkerSrc(): void {
        if (!$this->isCspOnFetchDirectivesEnabled()) {
            return;
        }
        $sources = $this->getCspSourcesFromTheme('worker-src');
        if (!empty($this->cspSources['worker-src'])) {
            $sources = array_merge(
                $sources,
                $this->cspSources['worker-src']
            );
        }
        $sources = array_unique($sources);
        if ($this->useCspWorkerSrcSelf) {
            $sources[] = 'self';
        }
        if (empty($sources)) {
            $sources = ['none'];
        }
        $this->cx->getResponse()->setContentSecurityPolicyDirective(
            'worker-src',
            $sources,
            $this->useCspReportOnly()
        );
    }

    /**
     * Blocks MIME sniffing unless it has been disabled.
     */
    protected function blockMimeSniffing() {
        $blockMimeSniffing = \Cx\Core\Setting\Controller\Setting::getValue(
            'blockMimeSniffing',
            'Config'
        );
        if ($blockMimeSniffing == 'off') {
            return;
        }
        // see https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types#mime_sniffing
        $this->cx->getResponse()->setHeader('X-Content-Type-Options', 'nosniff');
    }


    /**
     * Scan input arguments for potential attacks
     */
    protected function scanRequest() {
        // scan input data
        switch ($this->cx->getMode()) {
            case \Cx\Core\Core\Controller\Cx::MODE_FRONTEND:
                // Webapp Intrusion Detection System
                $config = \Env::get('config');
                if ($config['coreIdsStatus']=='on') {
                    $this->reportingMode = true;
                }

                $_GET     = $this->detectIntrusion($_GET);
                $_POST    = $this->detectIntrusion($_POST);
                $_COOKIE  = $this->detectIntrusion($_COOKIE);
                $_REQUEST = $this->detectIntrusion($_REQUEST);
                break;
        }
    }

    /**
    * Get request info
    *
    * Lists the content for an array for sending it with an e-mail
    * @param     $reqarray The array to send the contents from.
    * @param    $arrname  The name in the array.
    * @return    string    The value ready to send
    **/
    function getRequestInfo($reqarray, $arrname)
    {
        $retdata = "";
        if(!is_array($reqarray))
            return "";

        // For each content of the $reqarray
        foreach($reqarray as $nname => $nval){
            // If this is an array
            if(is_array($nval)){
                // It's an array. Add the contents of it.
                $retdata .= $arrname." [$nname] : array {\r\n";
                $retdata .= $this->getRequestInfo($nval, $arrname." [$nname]");
                $retdata .= $arrname." [$nname] : }\r\n";
            } else {
                // It's no array, just add it
                $retdata .= $arrname." [$nname] : $nval\r\n";
            }
        }
        return $retdata;
    }

    /**
    * Reports a possible intrusion attempt to the administrator
    * @param   $type    The type of intrusion attempt to report.
    * @param   $file    The file requesting the report (defaults to "Filename not available")
    * @param   $line    The line number requesting the report (defaults to "Linenumber not available")
    **/
    function reportIntrusion($type, $file = "Filename not available", $line = "Linenumber not available")
    {

        $objDatabase = \Env::get('db');
        $config      = \Env::get('config');
        $remoteaddr        = isset($_SERVER['REMOTE_ADDR']) ? contrexx_raw2db($_SERVER['REMOTE_ADDR']) : "Not set";
        $httpxforwardedfor = isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? contrexx_raw2db($_SERVER['HTTP_X_FORWARDED_FOR']) : "Not set";
        $httpvia           = isset($_SERVER['HTTP_VIA']) ? contrexx_raw2db($_SERVER['HTTP_VIA']) : "Not set";
        $httpclientip      = isset($_SERVER['HTTP_CLIENT_IP']) ? contrexx_raw2db($_SERVER['HTTP_CLIENT_IP']) : "Not set";
        $gethostbyname     = gethostbyname($remoteaddr);
        if($gethostbyname == $remoteaddr)
            $gethostbyname = "No matching hostname";

        // Add all the user's info to $user
        $user = "REMOTE_ADDR : $remoteaddr\r\n".
                "HTTP_X_FORWARDED_FOR : $httpxforwardedfor\r\n".
                "HTTP_VIA : $httpvia\r\n".
                "HTTP_CLIENT_IP : $httpclientip\r\n".
                "GetHostByName : $gethostbyname\r\n";
        // Add all requested information
        foreach ($this->criticalServerVars as $serverVar) {
            $_SERVERlite[$serverVar] = $_SERVER[$serverVar] ?? null;
        }

        $httpheaders = function_exists('getallheaders') ? getallheaders() : null;
        $gpcs = "";
        $gpcs .= $this->getRequestInfo($httpheaders, "HTTP HEADER");
        $gpcs .= $this->getRequestInfo($_REQUEST, "REQUEST");
        $gpcs .= $this->getRequestInfo($_GET, "GET");
        $gpcs .= $this->getRequestInfo($_POST, "POST");
        $gpcs .= $this->getRequestInfo($_SERVERlite, "SERVER");
        $gpcs .= $this->getRequestInfo($_COOKIE, "COOKIE");
        $gpcs .= $this->getRequestInfo($_FILES, "FILES");
        if (isset($_SESSION)) {
            $gpcs .= $this->getRequestInfo($_SESSION, "SESSION");
        }

        // Get the data to insert in the database
        $cdate = time();
        $dbuser = htmlspecialchars(addslashes($user), ENT_QUOTES, CONTREXX_CHARSET);
        $dbuser = contrexx_raw2db($dbuser);
        $dbgpcs = htmlspecialchars(addslashes($gpcs), ENT_QUOTES, CONTREXX_CHARSET);
        $dbgpcs = contrexx_raw2db($dbgpcs);
        $where  = addslashes("$file : $line");
        $where  = contrexx_raw2db($where);

        // Insert the intrusion in the database
        $objDatabase->Execute("INSERT INTO ".DBPREFIX."ids (timestamp, type, remote_addr, http_x_forwarded_for, http_via, user, gpcs, file)
                VALUES(".$cdate.", '".$type."', '".$remoteaddr."', '".$httpxforwardedfor."', '".$httpvia."', '".$dbuser."', '".$dbgpcs."', '".$where."')");

        // The headers for the e-mail
        $emailto = $config['coreAdminName']." <".$config['coreAdminEmail'].">";

        // The message to send
        $message = "DATE : $cdate\r\nFILE : $where\r\n\r\n$user\r\n\r\n$gpcs";

        // Send the e-mail to the administrator
        $objMail = new \Cx\Core\MailTemplate\Model\Entity\Mail();

        $objMail->setFrom($config['coreAdminEmail'], $config['coreAdminName']);
        $objMail->Subject = $_SERVER['HTTP_HOST']." : $type";
        $objMail->isHTML(false);
        $objMail->Body = $message;
        $objMail->addAddress($emailto);
        $objMail->send();

    }

    /**
    * Detect intrusion
    *
    * Looks through an array and tries to detect possible hacking attempts.
    * @param   $array  The array (or string) to check for security.
    * @return  array   The array with the trusted values, or the string
    **/
    function detectIntrusion($array)
    {
     //print_r($array);
        // If it's not an array, test this record for safety and return the result
        if(!is_array($array)){
            while(1){
                $safe = 1;
                // Test the string (called $array) for cross site scripting attacks
                if(
                    // Disallow <*script
                    preg_match('/<[^>a-z0-9]*script[^a-z]+/i', $array) ||
                    // Disallow <*xml*
                    preg_match('/<[^>a-z0-9]*xml[^a-z]+/i', $array) ||
                    // Disallow <*style*
                    preg_match('/<[^>a-z0-9]*style[^a-z]+/i', $array) ||
                    // Disallow <*form*
                    preg_match('/<[^>a-z0-9]*form[^a-z]+/i', $array) ||
                    // Disallow <*input*
                    preg_match('/<[^>a-z0-9]*input[^a-z]+/i', $array) ||
                    // Disallow <*window*
                    preg_match('/<[^>a-z0-9]*window[^a-z]+/i', $array) ||
                    // Disallow <*alert*
                    preg_match('/<[^>a-z0-9]*alert[^a-z]+/i', $array) ||
                    // Disallow <*img*
                    preg_match('/<[^>a-z0-9]*img[^a-z]+/i', $array) ||
                     // Disallow <*cookie*
                    preg_match('/<[^>a-z0-9]*cookie[^a-z]+/i', $array) ||
                    // Disallow <*object*
                    preg_match('/<[^>a-z0-9]*object[^a-z]+/i', $array) ||
                    // Disallow <*iframe*
                    preg_match('/<[^>a-z0-9]*iframe[^a-z]+/i', $array) ||
                    // Disallow <*applet*
                    preg_match('/<[^>a-z0-9]*applet[^a-z]+/i', $array) ||
                    // Disallow <*meta*
                    preg_match('/<[^>a-z0-9]*meta[^a-z]+/i', $array) ||
                    // Disallow <*body*
                    preg_match('/<[^>a-z0-9]*body[^a-z]+/i', $array) ||
                    // Disallow <*font*
                    preg_match('/<[^>a-z0-9]*font[^a-z]+/i', $array) ||
                    // Disallow <*p*
                //    preg_match('/<[^>a-z0-9]*p[^a-z]+/i', $array) ||
                    // Disallow "javascript: and 'javascript
                    preg_match('/["|\']javascript:/i', $array) ||
                    // Disallow =javascript:
                    preg_match('/=javascript:/i', $array) ||
                    // Disallow "vbscript: and 'vbscript:
                    preg_match('/["|\']vbscript:/i', $array) ||
                    // Disallow =vbscript:
                    preg_match('/=vbscript:/i', $array) ||
                    // Disallow on*=
                    preg_match('/[^a-z0-9]*on[a-z]+\s*=/i', $array)
                  )
                {
                    // Report a potential cross site scripting attack
                    if($this->reportingMode == true){
                        $this->reportIntrusion("XSS Attack");
                    }
                    $safe = 0;

                    // Use special ways to protect to some cross site scriptings
                    if(
                        preg_match('/["|\']javascript:/i', $array) ||
                        preg_match('/=\s*javascript:/i', $array) ||
                        preg_match('/["|\']vbscript:/i', $array) ||
                        preg_match('/=\s*vbscript:/i', $array)
                      )
                    {
                        // Remove the ':'
                        $array = preg_replace('/(["|\']javascript):/i', '\1', $array);
                        $array = preg_replace('/(=\s*javascript):/i', '\1', $array);
                        $array = preg_replace('/(["|\']vbscript):/i', '\1', $array);
                        $array = preg_replace('/(=\s*vbscript):/i', '\1', $array);
                    }
                    if(preg_match('/[^a-z0-9]*on[a-z]+\s*=/i', $array)){
                        // Remove the =
                        $array = preg_replace('/([^a-z0-9]*on[a-z]+\s*)=/i', '\1', $array);
                    }
                    // Secure it using htmlspecialchars
                    $array = htmlspecialchars($array, ENT_QUOTES, CONTREXX_CHARSET);
                }

// This is crap!  Every second english language sentence matches those.
/*
                // Test for SQL injection
                if(
                    // Disallow "*or/and*=*" or "*or*like*"
                    preg_match('/([^a-z]+|^)(OR|AND)[^a-z]+.*(=|like)/i', $array) ||
                    // Disallow "*UNION*SELECT "
                    preg_match('/([^a-z]+|^)UNION[^a-z]+.*SELECT[\t ]+/i', $array)
                  )
                {
                    // Report for an intrusion attempt
                    if($this->reportingMode == true){
                        $this->reportIntrusion("SQL Injection");
                    }
                    $safe = 0;

                    // On "*or/and*=/like*", remove OR/AND
                    $array = eregi_replace("([^a-z]+|^)(OR|AND)([^a-z]+.*(=|like))", "\\1\\3", $array);

                    // On "*UNION*SELECT ", remove union
                    $array = eregi_replace("([^a-z]+|^)UNION([^a-z]+.*SELECT[\t ]+)", "\\1\\3", $array);
                }
*/

                // Return the untrusted value, it's fine
                if($safe == 1) {
                    return $array;
                }
            }
        }

        // The trusted value will become an array
        $trusted = array();

        // For each record in the array
        foreach($array as $nname => $untrusted){
            // Get untrusted's trusted value and store it
            $trusted[$nname] = $this->detectIntrusion($untrusted);
        }

        // Return the trusted array
        return $trusted;
    }

    /**
     * Artificially delay response
     *
     * Calling this method will halt the script execution until the elapsed
     * runtime has reached $microseconds.
     * Use this method to protect a sensitive operation against a timing
     * attack.
     *
     * @param int $microseconds Overall runtime in microseconds that must be
     *     reached until script execution is continued.
     */
    public function delayResponseUntil(int $microseconds): void {
        $elapsedSeconds = microtime(true) - $this->cx->getStartTime();
        $elapsedMicroSeconds = $elapsedSeconds * 1000000;
        $microseconds -= $elapsedMicroSeconds;
        \DBG::debug('Delay response by ' . $microseconds . ' µs');
        $microseconds = max(0, $microseconds);
        usleep((int)$microseconds);
    }
}
