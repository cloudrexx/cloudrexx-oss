<?php declare(strict_types=1);

/**
 * Cloudrexx
 *
 * @link      https://www.cloudrexx.com
 * @copyright Cloudrexx AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

namespace Cx\Core\Usercache\Controller;


/**
 * Class for early usage of usercache during stage 2.
 *
 * After init(), so as of postInit() use the ComponentController instead.
 * @copyright   Cloudrexx AG
 * @author      Michael Ritter <michael.ritter@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  core_usercache
 * @todo Refactor the code and clean up
 */
class Usercache {
    /**
     * memcached extension
     */
    const CACHE_ENGINE_MEMCACHED = 'memcached';

    /**
     * cache off
     */
    const CACHE_ENGINE_OFF = 'off';

    /**
     * @var array Contains the reference to the Doctrine cache engine used for
     *     the query & result cache as well as the reference to the Doctrine
     *     meta-data cache. Both cache engines are of type
     *     \Doctrine\Common\Cache\CacheProvider. The Query & result cache
     *     engine is insted by key `false`. The meta-data cache is indexed by
     *     key `true`.
     */
    protected array $doctrineCacheEngine = [];

    protected ?string $userCacheEngine = null;

    /**
     * Number added to cache prefix to allow "invalidating" the cache
     *
     * Flushing the Cache is not “possible” in a nice way as
     * https://www.php.net/manual/en/memcached.getallkeys.php is not an atomic
     * operation and https://www.php.net/manual/en/memcached.flush.php would
     * flush the whole server.
     * Therefore we need a way to flush only the entries of a website. We do
     * this using an increment number (which we cache as well). Incrementing
     * this number results in the website no longer “seeing” the old entries and
     * Memcached will drop them once they get too old.
     * @var int
     */
    protected int $cacheIncrement = 1;

    /**
     * Cache increment for codebase cache
     *
     * For explanation see {@see static::$cacheIncrement}
     * @var int
     */
    protected int $codeBaseCacheIncrement = 1;

    protected ?\Memcached $memcached = null;
    protected \Cx\Core\Core\Controller\Cx $cx;

    /**
     * @var arrray List of instances of this class indexed by config folder name
     */
    protected static array $instances = array();

    /**
     * Returns a matching instance for the given Cx instance
     * @param \Cx\Core\Core\Controller\Cx Instance of Cx
     * @return Instance of this class
     */
    public static function getInstance(\Cx\Core\Core\Controller\Cx $cx): Usercache {
        if (isset(static::$instances[$cx->getId()])) {
            return static::$instances[$cx->getId()];
        }
        static::$instances[$cx->getId()] = new static($cx);
        return static::getInstance($cx);
    }

    /**
     * Initializes a new instance and initializes usercache if active
     * @param \Cx\Core\Core\Controller\Cx Instance of Cx
     */
    protected function __construct(\Cx\Core\Core\Controller\Cx $cx) {
        $this->cx = $cx;
        global $_CONFIG;

        $this->userCacheEngine = static::CACHE_ENGINE_OFF;
        if (
            isset($_CONFIG['cacheUserCache'])
            && $_CONFIG['cacheUserCache'] === static::CACHE_ENGINE_MEMCACHED
        ) {
            $this->userCacheEngine = $_CONFIG['cacheUserCache'];
        }
        $this->initUserCaching();
    }

    /**
     * Initializes usercaching
     */
    protected function initUserCaching() {
        global $_CONFIG;

        if (
            $_CONFIG['cacheDbStatus'] != 'on' ||
            $_CONFIG['cacheUserCache'] != static::CACHE_ENGINE_MEMCACHED
        ) {
            $this->memcached = null;
            $this->cacheIncrement = 0;
            $this->codeBaseCacheIncrement = 0;
            return;
        }
        $memcachedConfiguration = $this->getConfiguration();
        $this->memcached = null; // needed for reinitialization
        if (class_exists('\Memcached')) {
            $memcached = new \Memcached();
            if (
                @$memcached->addServer($memcachedConfiguration['ip'], (int) $memcachedConfiguration['port'])
                && $memcached->getStats()
            ) {
                \DBG::debug('Connection to the following memcached server successful:');
                \DBG::dump($memcachedConfiguration, DBG_DEBUG);

                // sync increments with cache
                $cachedIncrement = $memcached->get($this->getCachePrefix(true));
                if (!$cachedIncrement || $this->cacheIncrement > $cachedIncrement) {
                    $memcached->set($this->getCachePrefix(true), $this->cacheIncrement);
                } else if ($cachedIncrement > $this->cacheIncrement) {
                    $this->cacheIncrement = $cachedIncrement;
                }
                $codeBaseCachedIncrement = $memcached->get($this->getCachePrefix(true, true));
                if (!$codeBaseCachedIncrement || $this->codeBaseCacheIncrement > $codeBaseCachedIncrement) {
                    $memcached->set($this->getCachePrefix(true, true), $this->codeBaseCacheIncrement);
                } else if ($codeBaseCachedIncrement > $this->codeBaseCacheIncrement) {
                    $this->codeBaseCacheIncrement = $codeBaseCachedIncrement;
                }
                $this->memcached = $memcached;
            } else {
                \DBG::log('Connection to memcached failed. Disabling cacheUserCache...');
                \DBG::dump($memcachedConfiguration);
                $this->memcached = null;
                $this->cacheIncrement = 0;
                $this->codeBaseCacheIncrement = 0;
            }
        }
    }

    /**
     * Re-initializes the usercache
     *
     * This is used to apply the new config if the config was changed.
     */
    public function reInit(): void {
        $this->initUserCaching();
    }

    /**
     * Returns the usercache configuration
     * @return array Array with keys "ip" and "port"
     */
    public function getConfiguration(): array {
        global $_CONFIG;
        $ip = '127.0.0.1';
        $port = '11211';

        if (!empty($_CONFIG['cacheUserCacheMemcachedConfig'])) {
            $settings = json_decode($_CONFIG['cacheUserCacheMemcachedConfig'], true);
            $ip = $settings['ip'];
            $port = $settings['port'];
        }

        return array('ip' => $ip, 'port' => $port);
    }

    /**
     * Tells whether a server is reachable with the current config
     * @return bool True if server can be reached, false otherwise
     */
    public function isValidConfiguration(): bool {
        $memcachedConfiguration = $this->getConfiguration();
        $memcached = new \Memcached();
        return (bool) (
            @$memcached->addServer(
                $memcachedConfiguration['ip'],
                (int) $memcachedConfiguration['port']
            )
            && $memcached->getStats()
        );
    }

    /**
     * Detects the correct doctrine cache driver for the user caching engine in use
     * @param bool $forMetaData Set to true to fetch the cache driver to be used
     *     for meta data caching.
     * @return \Doctrine\Common\Cache\CacheProvider The doctrine cache driver object
     */
    public function getDoctrineCacheDriver(
        bool $forMetaData = false
    ): \Doctrine\Common\Cache\CacheProvider {
        if (isset($this->doctrineCacheEngine[$forMetaData])) {
            return $this->doctrineCacheEngine[$forMetaData];
        }
        $userCacheEngine = $this->getUserCacheEngine();
        // check if user caching is active
        if (!$this->getUserCacheActive()) {
            $userCacheEngine = static::CACHE_ENGINE_OFF;
        }
        switch ($userCacheEngine) {
            case static::CACHE_ENGINE_MEMCACHED:
                $memcached = $this->getMemcached();
                if (!$memcached) {
                    return new \Doctrine\Common\Cache\ArrayCache();
                }
                $cache = new \Doctrine\Common\Cache\MemcachedCache();
                $cache->setMemcached($memcached);
                $cache->setNamespace($this->getCachePrefix(false, $forMetaData));
                break;
            default:
                $cache = new \Doctrine\Common\Cache\ArrayCache();
                break;
        }
        // set the doctrine cache engine to avoid getting it a second time
        $this->doctrineCacheEngine[$forMetaData] = $cache;
        return $cache;
    }

    /**
     * Deactivates Doctrine cache for this request
     *
     * Please note that components that directly use the underlying usercache
     * (like ClassLoader and DataSet) do still use usercache after this.
     */
    public function deactivateUserCacheForCurrentRequest(): void {
        $this->doctrineCacheEngine = [
            false => new \Doctrine\Common\Cache\ArrayCache(),
            true => new \Doctrine\Common\Cache\ArrayCache()
        ];
    }

    protected function getUserCacheActive() {
        global $_CONFIG;
        return
            isset($_CONFIG['cacheDbStatus'])
            && $_CONFIG['cacheDbStatus'] == 'on';
    }

    /**
     * Flushes the memcached cache of this instance.
     *
     * If component SystemInfo is licensed, then this flushes not only the
     * user-data cache ({@see static::flushUserDataCache()}, but also the
     * codebase cache ({@see static::flushCodeBaseCache()}.
     */
    public function clear(): void {
        if (!$this->memcached) {
            return;
        }
        $this->flushUserDataCache();
        $requiredComponent = 'SystemInfo';
        $cx = \Cx\Core\Core\Controller\Cx::instanciate();
        if (!in_array($requiredComponent, $cx->getLicense()->getLegalComponentsList())) {
            \DBG::log(
                sprintf(
                    'Flushing codebase cache denied as component %s is not licensed!',
                    $requiredComponent
                )
            );
            return;
        }
        $this->flushCodeBaseCache();
        // the following is a no-op as the classloader is part of the codebase
        // cache which just got flushed by $this->flushCodeBaseCache()
        // $cx->getClassLoader()->flushCache();
    }

    /**
     * Flush the user data from the memcached instance.
     * User data contains:
     * - result & query cache of doctrine ORM
     * - cached DataSets
     * - custom data
     *
     * @todo Migrate to Memcached::increment() once the $initial_value is
     *     supported by the PECL extension.
     */
    protected function flushUserDataCache(): void {
        $this->cacheIncrement++;
        $this->memcached->set(
            $this->getCachePrefix(true),
            $this->cacheIncrement
        );
    }

    /**
     * Flush the codebase cache from the memcached instance.
     * Codebase cache contains:
     * - classloader cache
     * - component metadata cache
     * - metadata of doctrine ORM
     *
     * @todo Migrate to Memcached::increment() once the $initial_value is
     *     supported by the PECL extension.
     */
    protected function flushCodeBaseCache(): void {
        $this->codeBaseCacheIncrement++;
        $this->memcached->set(
            $this->getCachePrefix(true, true),
            $this->codeBaseCacheIncrement
        );
    }

    /**
     * Wrapper to fetch an item from cache using the doctrine usercache cachedriver
     * @param string $id Id of the item
     * @param bool $fromCodebase Defaults to false. If set to true fetch is from codebase-wide cache
     * @return mixed     The item
     */
    public function fetch(string $id, bool $fromCodebase = false): mixed {
        if (!$this->memcached) {
            return null;
        }
        $key = $this->getKeyForId($id, $fromCodebase);
        $data = $this->memcached->get($key);
        \DBG::debug(
            sprintf(
                'Get %s from usercache: %s',
                $key,
                $this->memcached->getResultMessage()
            )
        );
        return $data;
    }

    /**
     * Wrapper to save an item to cache using the doctrine user usercache cachedriver
     * @param string $id Id of the item
     * @param mixed $data data of the item
     * @param int $lifeTime Expiraton time of the item (if it equals zero, the item never expires)
     * @param bool $fromCodebase Defaults to false. If set to true data is saved to codebase-wide cache
     */
    public function save(
        string $id,
        mixed $data,
        int $lifeTime = 0,
        bool $fromCodebase = false
    ): void {
        if (!$this->memcached) {
            return;
        }
        $key = $this->getKeyForId($id, $fromCodebase);
        $this->memcached->set($key, $data);
    }

    /**
     * Wrapper to delete an item from cache using the doctrine usercache cache driver
     * @param string $id Id of the item
     * @param bool $fromCodebase Defaults to false. If set to true data is deleted from codebase-wide cache
     */
    public function delete(string $id, bool $fromCodebase = false): void {
        if (!$this->memcached) {
            return;
        }
        $key = $this->getKeyForId($id, $fromCodebase);
        $this->memcached->delete($key);
    }

    protected function getUserCacheEngine() {
        return $this->userCacheEngine;
    }

    /**
     * @return \Memcached The memcached object
     */
    protected function getMemcached() {
        return $this->memcached;
    }

    /**
     * Whether user cache is active
     * @return bool True if usercache is active, false otherwise
     */
    public function isActive(): bool {
        return (bool) $this->memcached;
    }

    /**
     * Get the key to identify the element with $id in the memcached cache.
     *
     * @param string $id The ID of the element in the cache.
     * @param bool $fromCodebase Defaults to false. If set to true key for codebase-wide cache is returned
     * @return string The key used to identify $id in the memcached cache.
     */
    protected function getKeyForId(string $id, bool $fromCodebase = false): string {
        $key = $this->getCachePrefix(false, $fromCodebase) . $id;
        if ($this->isValidKey($key)) {
            return $key;
        }
        \DBG::debug('Key ' . $key .' is not supported by Memcached. Going to use md5-hash instead');
        return $this->getCachePrefix(false, $fromCodebase) . md5($id);
    }

    /**
     * Checks if a string is a valid memcached key according to section 'Keys'
     * in  https://github.com/memcached/memcached/blob/master/doc/protocol.txt
     *
     * @param string $key The string to be verified as a valid memcached key.
     * @return bool Whether $key is a valid memcached key.
     */
    protected function isValidKey(string $key): bool {
        if (strlen($key) > 250) {
            return false;
        }
        if (!ctype_graph($key)) {
            return false;
        }
        return true;
    }

    /**
     * Retunrns the CachePrefix related to this Domain
     * @param boolean $withoutIncrement (optional) If set to true returns the prefix without the increment
     * @param bool $fromCodebase Defaults to false. If set to true prefix for codebase-wide cache is returned
     * @global string $_DBCONFIG
     * @return string CachePrefix
     */
    protected function getCachePrefix(
        bool $withoutIncrement = false,
        bool $fromCodebase = false
    ): string {
        global $_DBCONFIG;

        if ($fromCodebase) {
            $cx = \Cx\Core\Core\Controller\Cx::instanciate();
            $prefix = 'codebase.' . basename($cx->getCodeBaseDocumentRootPath() . '.');
        } else {
            $prefix = $_DBCONFIG['database'] . '.' . $_DBCONFIG['tablePrefix'] . '.';
        }
        if ($withoutIncrement) {
            return $prefix;
        }
        if ($fromCodebase) {
            return $prefix . $this->codeBaseCacheIncrement . '.';
        } else {
            return $prefix . $this->cacheIncrement . '.';
        }
    }
}
