<?php declare(strict_types=1);

/**
 * Cloudrexx
 *
 * @link      https://www.cloudrexx.com
 * @copyright Cloudrexx AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

namespace Cx\Core\Usercache\Controller;

/**
 * Usercache handling
 *
 * Use the methods fetch(), save() and delete() to interact.
 * @copyright   Cloudrexx AG
 * @author      Michael Ritter <michael.ritter@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  core_usercache
 */
class ComponentController extends \Cx\Core\Core\Model\Entity\SystemComponentController {

    /**
     * @var Usercache Usercache instance
     */
    protected Usercache $usercache;

    /**
     * @inheritDoc
     */
    public function getControllerClasses() {
        return array();
    }

    /**
     * @inheritDoc
     */
    public function __construct(\Cx\Core\Core\Model\Entity\SystemComponent $systemComponent, \Cx\Core\Core\Controller\Cx $cx) {
        parent::__construct($systemComponent, $cx);
        $this->usercache = \Cx\Core\Usercache\Controller\Usercache::getInstance($cx);
    }

    /**
     * Detects the correct doctrine cache driver for the user caching engine in use
     * @param bool $forMetaData Set to true to fetch the cache driver to be used
     *     for meta data caching.
     * @return \Doctrine\Common\Cache\CacheProvider The doctrine cache driver object
     */
    public function getDoctrineCacheDriver(
        bool $forMetaData = false
    ): \Doctrine\Common\Cache\CacheProvider {
        return $this->usercache->getDoctrineCacheDriver($forMetaData);
    }

    /**
     * Deactivates Doctrine cache for this request
     *
     * Please note that components that directly use the underlying usercache
     * (like ClassLoader and DataSet) do still use usercache after this.
     */
    public function deactivateUserCacheForCurrentRequest(): void {
        $this->usercache->deactivateUserCacheForCurrentRequest();
    }

    /**
     * Clears all Memcacheddata related to this Domain if Memcached is installed
     */
    public function clear(): void {
        $this->usercache->clear();
    }

    /**
     * Wrapper to fetch an item from cache using the doctrine usercache cachedriver
     * @param string $id Id of the item
     * @param bool $fromCodebase Defaults to false. If set to true fetch is from codebase-wide cache
     * @return mixed     The item
     */
    public function fetch(string $id, bool $fromCodebase = false): mixed {
        return $this->usercache->fetch($id, $fromCodebase);
    }

    /**
     * Wrapper to save an item to cache using the doctrine user usercache cachedriver
     * @param string $id Id of the item
     * @param mixed $data data of the item
     * @param int $lifeTime Expiraton time of the item (if it equals zero, the item never expires)
     * @param bool $fromCodebase Defaults to false. If set to true data is saved to codebase-wide cache
     */
    public function save(
        string $id,
        mixed $data,
        int $lifeTime = 0,
        bool $fromCodebase = false
    ): void {
        $this->usercache->save($id, $data, $lifeTime, $fromCodebase);
    }

    /**
     * Wrapper to delete an item from cache using the doctrine usercache cache driver
     * @param string $id Id of the item
     * @param bool $fromCodebase Defaults to false. If set to true data is deleted from codebase-wide cache
     */
    public function delete(string $id, bool $fromCodebase = false): void {
        $this->usercache->delete($id, $fromCodebase);
    }

    /**
     * Whether user cache is active
     * @return bool True if usercache is active, false otherwise
     */
    public function isActive(): bool {
        return $this->usercache->isActive();
    }

    /**
     * Returns the usercache configuration
     * @return array Array with keys "ip" and "port"
     */
    public function getConfiguration(): array {
        return $this->usercache->getConfiguration();
    }

    /**
     * Tells whether a server is reachable with the current config
     * @return bool True if server can be reached, false otherwise
     */
    public function isValidConfiguration(): bool {
        return $this->usercache->isValidConfiguration();
    }

    /**
     * Re-initializes the usercache
     *
     * This is used to apply the new config if the config was changed.
     */
    public function reInit(): void {
        $this->usercache->reInit();
    }
}
