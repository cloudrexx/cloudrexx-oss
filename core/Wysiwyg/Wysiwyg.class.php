<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * Wysiwyg
 *
 * @copyright   CLOUDREXX CMS - CLOUDREXX AG
 * @author      CLOUDREXX Development Team <info@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  core_wysiwyg
 */

namespace Cx\Core\Wysiwyg;

/**
 * Class WysiwygException
 *
 * @copyright   Cloudrexx AG
 * @author      Manuel Schenk <manuel.schenk@comvation.com>
 * @version     1.0.0
 * @package     cloudrexx
 * @subpackage  core_wysiwyg
 */
class WysiwygException extends \Exception {}

/**
 * Wysiqyg class
 *
 * @copyright   CLOUDREXX CMS - CLOUDREXX AG
 * @author      Thomas Daeppen <thomas.daeppen@comvation.com>
 * @author      Michael Räss <michael.raess@comvation.com>
 * @author      Ueli Kramer <ueli.kramer@comvation.com>
 * @version     3.0.0
 * @package     cloudrexx
 * @subpackage  core_wysiwyg
 */

class Wysiwyg extends \Cx\Model\Base\EntityBase
{
    /**
     * Base configuration for the different types of wysiwyg editors.
     * Plugin specific notes:
     * * `bbcode`: According to its own {@see https://ckeditor.com/cke4/addon/bbcode documentation})
     *     the plugin does customize the editor configuration to better match
     *     the BBCode environment. As a result the editor is partly broken when
     *     the bbcode plugin gets initialized on startup. Therefore we must not
     *     include the bbcode plugin in the bundled source, but instead load it
     *     as extraPlugin on demand.
     * * `codemirror`: The CKBuilder somehow is unable to properly include the
     *     plugin. As a result we manually have to load the plugin through
     *     `extraPlugins` even though the plugin itself is part of the bundled
     *     src.
     *
     * @var array the types which are available for cloudrexx wysiwyg editors
     */
    protected $types = array(
        'small' => array(
            'toolbar' => 'Small',
            'width' => '100%',
            'height' => 200,
            'fullPage' => false,
            'extraPlugins' => array('codemirror'),
            'removePlugins' => array(),
        ),
        'full' => array(
            'toolbar' => 'Full',
            'width' => '100%',
            'height' => 450,
            'fullPage' => false,
            'extraPlugins' => array('codemirror'),
            'removePlugins' => array(),
        ),
        'fullpage' => array(
            'toolbar' => 'Full',
            'width' => '100%',
            'height' => 450,
            'fullPage' => true,
            'extraPlugins' => array('codemirror'),
            'removePlugins' => array(),
        ),
        'bbcode' => array(
            'toolbar' => 'BBCode',
            'width' => '100%',
            'height' => 200,
            'fullPage' => false,
            'extraPlugins' => array('bbcode'),
            'removePlugins' => array('codemirror'),
        ),
        'frontendeditingtitle' => array(
            'toolbar' => 'FrontendEditingTitle',
            'extraPlugins' => array(),
            'removePlugins' => array('bbcode', 'codemirror'),
            'forcePasteAsPlainText' => true,
            'basicEntities' => false,
            'entities' => false,
            'entities_latin' => false,
            'entities_greek' => false,
        ),
        'frontendeditingcontent' => array(
            'toolbar' => 'FrontendEditingContent',
            'extraPlugins' => array(),
            'removePlugins' => array('bbcode', 'codemirror'),
            'startupOutlineBlocks' => false,
        ),
    );

    /**
     * @var string the value for the textarea html attribute "name"
     */
    protected $name;

    /**
     * @var string the value for the textarea html attribute "value"
     */
    protected $value;

    /**
     * @var string the type of wysiwyg editor
     */
    protected $type;

    /**
     * @var int the language id of current language
     */
    protected $langId;

    /**
     * @var array array of extra plugins added for the wysiwyg editor
     */
    protected $extraPlugins;

    /**
     * @var array array of plugins to be deactivated in the wysiwyg editor
     */
    protected $removePlugins;

    /**
     * The SystemComponentController that did instanciate this Wysiwyg instance.
     *
     * @var \Cx\Core\Core\Model\Entity\SystemComponentController SystemComponentController that instanciated this Wysiwyg instance
     */
    protected $callingSystemComponentController;

     /**
      * Initialize WYSIWYG editor
      * If $systemComponentController is not specified, the call trace is searched for the component calling this constructor.
      * @param string $name the name content for name attribute
      * @param string $value (optional) content for value attribute
      * @param string $type (optional) the type of editor to use: possible types are "small", "full", "bbcode", default is "small"
      * @param null|int $langId (optional) the language id, by default FRONTEND_LANG_ID is used
      * @param array $extraPlugins (optional) extra plugins to activate
      * @param array $removePlugins (optional) deactivate plugins
      * @param \Cx\Core\Core\Model\Entity\SystemComponentController $systemComponentController (optional) ComponentController of the component that uses the object generated by this constructor
     * @throws WysiwygException If no $systemComponentController is provided and none can be found
     */
    public function __construct($name, $value = '', $type = 'small', $langId = null, $extraPlugins = array(), $removePlugins = array(), \Cx\Core\Core\Model\Entity\SystemComponentController $systemComponentController = null)
    {
        // Sets provided SystemComponentController that instanciated this Wysiwyg instance
        $this->callingSystemComponentController = $systemComponentController;
        if (!$this->callingSystemComponentController) {
            // Searches the calling SystemComponentController intelligently by RegEx on backtrace stack frame
            $traces = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 2);
            $trace = end($traces);
            if (empty($trace['class'])) {
                throw new WysiwygException('No SystemComponentController found that instanciated Wysiwyg');
            }
            $matches = array();
            preg_match(
                '/Cx\\\\(?:Core|Core_Modules|Modules)\\\\([^\\\\]*)\\\\/',
                $trace['class'],
                $matches
            );
            $this->callingSystemComponentController = $this->cx->getComponent($matches[1]);
        }

        $this->name = $name;
        $this->value = $value;
        $this->type = strtolower($type);
        $this->langId = $langId ? intval($langId) : FRONTEND_LANG_ID;
        $this->extraPlugins = $extraPlugins;
        $this->removePlugins = $removePlugins;
    }

    /**
     * Get the html source code for the wysiwyg editor
     *
     * @return string
     */
    public function getSourceCode()
    {
        \JS::activate('ckeditor');
        \JS::activate('jquery');

        $cxjs = \ContrexxJavascript::getInstance();
        $cxjs->setVariable('config_' . $this->name, $this->getConfig(), $this->getSystemComponent()->getName());

        \JS::registerCode('
            $J(function(){
                let config = cx.variables.get(\'config_' . $this->name . '\', \'' . $this->getSystemComponent()->getName() . '\');
                config.customConfig = CKEDITOR.getUrl(config.customConfig);
                CKEDITOR.replace(\'' . $this->name . '\', config);
            });
        ');

        return $this->getMediaBrowserIntegration()
            . '<textarea name="'.$this->name.'" style="width: 100%; height: ' . $this->types[$this->type]['height'] . 'px">'.$this->value.'</textarea>';
    }

    /**
     * Retrieves the Media Browser integration as an HTML-code for CKEditor.
     *
     * This method creates a hidden MediaBrowser instance for the component
     * that did create this Wysiwyg instance.
     *
     * @return string HTML code representing the Media Browser integration for CKEditor.
     */
    public function getMediaBrowserIntegration(): string {
        if (!\FWUser::getFWUserObject()->objUser->login(true)) {
            return '';
        }
        $mediaSource = $this->getMediaSource();
        if (!$mediaSource) {
            return '';
        }
        $mediaBrowserCkeditor = new \Cx\Core_Modules\MediaBrowser\Model\Entity\MediaBrowser(
            $this->callingSystemComponentController
        );
        $mediaBrowserCkeditor->setOptions(
            array(
                'type'           => 'button',
                'style'          => 'display:none',
                'id'             => 'ckeditor_image_button',
                'startmediatype' => $mediaSource->getName(),
            )
        );
        return $mediaBrowserCkeditor->getXHtml('mediabrowser');
    }

    /**
     * Get CKEditor initialization config
     *
     * @return  array   Config set of this Wysiwyg instance that can be used
     *     to instanciate the CKEditor.
     */
    public function getConfig(): array {
        $this->initUploaderForImagePasting();
        $config = $this->types[$this->type];
        $config['customConfig'] = $this->getComponentController()->getConfigPath();
        $extraPlugins = array_merge($this->extraPlugins, $this->types[$this->type]['extraPlugins']);
        if (!empty($extraPlugins)) {
            $config['extraPlugins'] = implode(',', $extraPlugins);
        }
        $removePlugins = array_merge($this->removePlugins, $this->types[$this->type]['removePlugins']);
        if (!empty($removePlugins)) {
            $config['removePlugins'] = implode(',', $removePlugins);
        }
        return $config;
    }

    /**
     * Get MediaSource based on the component
     *
     * @return Cx\Core\MediaSource\Model\Entity\MediaSource
     */
    public function getMediaSource()
    {
        $mediaSourceManager = $this->cx->getMediaSourceManager();
        $mediaSource = $mediaSourceManager
            ->getMediaSourceByComponent($this->callingSystemComponentController);

        if ($mediaSource) {
            return $mediaSource;
        }

        //If MediaSource does not exists, set the first MediaSource from the MediaSources list
        return current($mediaSourceManager->getMediaTypes());
    }

    /**
     * Initialize the {@see \Cx\Core_Modules\Uploader\Model\Entity\Uploader} to
     * handle the image pasting in the WYSIWYG editor.
     */
    protected function initUploaderForImagePasting(): void {
        if (!\FWUser::getFWUserObject()->objUser->login(true)) {
            return;
        }
        $mediaSource = $this->getMediaSource();
        if (!$mediaSource) {
            return;
        }
        $_ARRAYLANG = \Env::get('init')->getComponentSpecificLanguageData(
            $this->getSystemComponent()->getName(),
            $this->cx->isFrontendMode()
        );
        $uploader = new \Cx\Core_Modules\Uploader\Model\Entity\Uploader();
        $uploader->setOptions([
            'allowedExtensions' => ['png', 'gif', 'jpg', 'jpeg', 'webp',],
            'target-path' => $mediaSource->getDirectory()[0],
        ]);
        $cxJs = \ContrexxJavascript::getInstance();
        $cxJs->setVariable(
            [
                'ckeditorUploaderId' => $uploader->getId(),
                'imagePasteName' => $this->callingSystemComponentController->getName(),
                'TXT_WYSIWYG_IMAGE_PASTE_ERROR' => $_ARRAYLANG['TXT_WYSIWYG_IMAGE_PASTE_ERROR'],
            ],
            'wysiwyg'
        );
    }

    /**
     * Get safe BBCode
     *
     * @param string $bbcode the unsafe BBCode
     * @param bool $html return as html code
     * @return string
     */
    public static function prepareBBCodeForDb($bbcode, $html = false)
    {
        $bbcode = strip_tags($bbcode);
        if ($html) {
            $bbcode = self::prepareBBCodeForOutput($bbcode);
        }
        return contrexx_input2db($bbcode);
    }

    /**
     * Convert BBCode to HTML
     *
     * This code comes from the forum module, feel free to rewrite
     *
     * @param string $bbcode the BBCode which should be a html output
     * @return string the xhtml output
     */
    public static function prepareBBCodeForOutput($bbcode)
    {
        $BBCodeHandler = new \Cx\Core\Wysiwyg\BBCodeHandler();
        return $BBCodeHandler->parse($bbcode);
    }

    /**
     * Alias for the method getSourceCode()
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getSourceCode();
    }

    /**
     * @param int $langId
     */
    public function setLangId($langId)
    {
        $this->langId = $langId;
    }

    /**
     * @return int
     */
    public function getLangId()
    {
        return $this->langId;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = strtolower($type);
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param array $extraPlugins
     */
    public function setExtraPlugins($extraPlugins)
    {
        $this->extraPlugins = $extraPlugins;
    }

    /**
     * @return array
     */
    public function getExtraPlugins()
    {
        return $this->extraPlugins;
    }

    /**
     * Set a list of plugins that shall be deactivated
     *
     * @param array $removePlugins List of plugin names
     */
    public function setRemovePlugins($removePlugins)
    {
        $this->removePlugins = $removePlugins;
    }

    /**
     * Fetch the list of deactivated plugins
     *
     * @return array List of plugin names
     */
    public function getRemovePlugins()
    {
        return $this->removePlugins;
    }
}
