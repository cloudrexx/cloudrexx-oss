var wysiwygEditorMode = [];
for (var instanceName in CKEDITOR.instances) {
    CKEDITOR.instances[instanceName].on('paste', function (event) {
        cx.ui.messages.showLoad();
        var result   = wysiwygGetDataFromContent(event.data.dataValue);
        var callback = function(event, value) {
            event.editor.insertHtml(value);
            cx.ui.messages.removeAll();
        };

        if (!result.files || result.files.length === 0) {
            event.data.dataValue = result.content;
            cx.ui.messages.removeAll();
            return;
        }

        //Upload process for pasted images
        wysiwygUploadInlineFile(event, result.files, result.content, callback);
        event.data.dataValue = '';
    });
    CKEDITOR.instances[instanceName].on('mode', function (event) {
        if (wysiwygEditorMode[event.editor.name] == null) {
            wysiwygEditorMode[event.editor.name] = event.editor.mode;
        }
        if (
            wysiwygEditorMode[event.editor.name] === 'source' &&
            event.editor.mode === 'wysiwyg'
        ) {
            cx.ui.messages.showLoad();
            var result   = wysiwygGetDataFromContent(event.editor.getData());
            var callback = function(event, value) {
                event.editor.setData(value);
                cx.ui.messages.removeAll();
            };
            if (!result.files || result.files.length === 0) {
                event.editor.setData(result.content);
                cx.ui.messages.removeAll();
                return;
            }

            //Upload process for pasted images
            wysiwygUploadInlineFile(event, result.files, result.content, callback);
        }
        wysiwygEditorMode[event.editor.name] = event.editor.mode;
    });
}

/**
 * get file object from Data image present in the content
 *
 * @param {string} content     content
 * @returns {object}
 */
function wysiwygGetDataFromContent(content)
{
    var match,
        files = [],
        data;
    var imagePattern = {
        dataImagePattern:   /<img\s+[^>]*src=([\'\"])(data\:(\s|)image\/(\w{3,4})\;base64\,(\s|)([^\'\"]*)\s*)\1[^>]*>/g,
        inlineImagePattern: /url\(([\'\"])(data\:(\s|)image\/(\w{3,4})\;base64\,(\s|)([^\'\"]*)\s*)\1(\))/g
    };

    data = content;
    for (patternIdx in imagePattern) {
        while (match = imagePattern[patternIdx].exec(content)) {
            var file = wysiwygCreateBlobFromBase64String(match[2]);
            if (!file) {
                data = data.replace(match[0], '');
            } else {
                files.push({
                    file: file,
                    dataSrc: [match[0], match[2]]
                });
            }
        }
    }

    return {'files' : files, 'content' : data};
}

/**
 * Do the upload process
 *
 * @param {object} event   event object
 * @param {array}  files   array of upload files
 * @param {string} content pasted content
 */
async function wysiwygUploadInlineFile(event, files, content, callback)
{
    let uploadId = cx.variables.get('ckeditorUploaderId', 'wysiwyg');
    for (let i = 0; i < files.length; i++) {
        let uploadFile = files[i].file;
        currentImg = files[i].dataSrc[0];
        if (!uploadId) {
            content = content.replace(currentImg, '');
            continue;
        }
        const CHUNK_SIZE = 1024*500;
        const totalChunks = Math.ceil(uploadFile.size / CHUNK_SIZE);
        const fileName = uploadFile.name;
        let formData = new FormData();
        formData.append('name', fileName);
        formData.append('chunks', totalChunks);
        try {
            for (let c = 0; c < totalChunks; c++) {
                let CHUNK = uploadFile.slice(
                    c * CHUNK_SIZE,
                    Math.min(
                        uploadFile.size,
                        (c + 1) * CHUNK_SIZE
                    ),
                    uploadFile.type
                );
                formData.set('chunk', c);
                formData.set('file', CHUNK, fileName);
                await fetch('/api/Data/Json/Uploader/upload?id=' + uploadId, {
                    method: 'POST',
                    body: formData
                }).then(function(resp) {
                    return resp.text();
                }).then(function(text) {
                    let response = JSON.parse(text);
                    if (response.status != 'success') {
                        throw new Error('Upload failed');
                    }
                    if (!response.data.file.length) {
                        // chunk upload incomplete
                        return;
                    }
                    content = content.replace(
                        files[i].dataSrc[1],
                        response.data.file
                    );
                }).catch(function(error) {
                    throw error;
                });
            }
        } catch(e) {
            content = content.replace(currentImg, '');
            cx.ui.messages.add(
                cx.variables.get('TXT_WYSIWYG_IMAGE_PASTE_ERROR', 'wysiwyg'),
                'error'
            );
        }
    }
    callback(event, content);
}

/**
 * Create file by using data URI
 *
 * @param {string} dataURI data URI
 * @returns File
 */
function wysiwygCreateBlobFromBase64String(dataURI) {
    /* convert base64/URLEncoded data component to raw binary data held in a string */
    try {
        var byteString;
        if (dataURI.split(',')[0].indexOf('base64') >= 0) {
            byteString = atob(dataURI.split(',')[1]);
        } else {
            byteString = unescape(dataURI.split(',')[1]);
        }

        /* separate out the mime component*/
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

        /* write the bytes of the string to a typed array */
        var typedArray = new Uint8Array(byteString.length);
        for (var i = 0; i < byteString.length; i++) {
            typedArray[i] = byteString.charCodeAt(i);
        }

        var resultingBlob =  new Blob([typedArray], {type:mimeString});
        let extension = '';
        try {
            extension = mimeString.match(/\/(.*)$/)[1];
        } catch (e) {
            return false;
        }
        let name = cx.variables.get('imagePasteName', 'wysiwyg') + '.' + extension;
        return new File([resultingBlob], name, {type:mimeString});
    } catch (ex) {
        return false;
    }
}
