<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * This is the controllers for the component
 *
 * @copyright   Cloudrexx AG
 * @author      Sebastian Brand <sebastian.brand@comvation.com>
 * @package     cloudrexx
 * @subpackage  core_wysiwyg
 * @version     1.0.0
 */

namespace Cx\Core\Wysiwyg\Controller;

use Cx\Core\Wysiwyg\Model\Event\WysiwygEventListener;

/**
 * This is the main controller for the component
 *
 * @copyright   Cloudrexx AG
 * @author      Sebastian Brand <sebastian.brand@comvation.com>
 * @package     cloudrexx
 * @subpackage  core_wysiwyg
 * @version     1.0.0
 */
class ComponentController extends \Cx\Core\Core\Model\Entity\SystemComponentController implements \Cx\Core\Event\Model\Entity\EventListener, \Cx\Core\Json\JsonAdapter {

    /**
     * Location of the CKeditor library
     */
    const LIB_PATH = '/ckeditor/4.21.0';

    /**
     * @var array   List of cached DataSets
     *              (\Cx\Core_Modules\Listing\Model\Entity\DataSet) of
     *              Wysiwyg.yml files of themes.
     *              The array has the following structure:
     *              array(<themeId> => <DataSet>)
     */
    protected $wysiwygDataCache = array();

    /**
     * This function controlls the events from the eventListener
     *
     * @param string $eventName Name of the event
     * @param array $eventArgs Arguments of the event
     */
    public function onEvent($eventName, array $eventArgs) {
        switch ($eventName) {
            case 'wysiwygCssReload':
                $skinId = intval($eventArgs[0]['skin']) ?? 0;
                $result = $eventArgs[1];

                foreach ($this->getCustomCSSVariables($skinId) as $key => $val) {
                    $result[$key] = $val;
                }
                break;
            default:
                break;
        }
    }

    /**
     * Returns all Controller class names for this component (except this)
     *
     * @return array List of Controller class names (without namespace)
     */
    public function getControllerClasses() {
        return array('Backend', 'Toolbar');
    }

    /**
     * {@inheritdoc}
     */
    public function getControllersAccessableByJson() {
        return array(
            'ComponentController'
        );
    }

    /**
     * Returns the internal name used as identifier for this adapter
     * @return String Name of this adapter
     */
    public function getName() {
        return parent::getName();
    }

    /**
     * Returns an array of method names accessable from a JSON request
     * @return array List of method names
     */
    public function getAccessableMethods() {
        return array('getConfig');
    }

    /**
     * Returns default permission as object
     * @return Object
     */
    public function getDefaultPermissions() {
        return new \Cx\Core_Modules\Access\Model\Entity\Permission(
            ['http', 'https'],
            ['get', 'post'],
            false
        );
    }

    /**
     * Returns all messages as string
     * @return String HTML encoded error messages
     */
    public function getMessagesAsString() {
        return '';
    }

    /**
     * Returns all Wysiwyg templates in JSON to be used by the CKEditor
     *
     * Returns the Wysiwyg templates from the Wysiwyg.yml file from the theme
     * identified by $themeId. If no theme can be identified by $themeId, then
     * the Wysiwyg.yml from the default theme is loaded instead.
     *
     * @param   integer $themeId    The ID of the webdesign template (theme) to
     *                              load the Wysiwyg templates from.
     * @return  string  JSON-encoded string of Wysiwyg templates
     */
    public function getWysiwygTemplates($themeId) {
        // wysiwyg templates from webdesign template (theme)
        $templatesFromTheme = $this->getCustomWysiwygData($themeId, 'Templates');

        // wysiwyg templates from config section
        $templatesFromConfig = $this->getWysiwygTemplatesFromConfig();

        // merge templates from theme and config section together
        $templates = array_merge($templatesFromTheme, $templatesFromConfig);

        return json_encode($templates);
    }

    /**
     * Get custom config for the Wysiwyg editor that is defined in the
     * Wysiwyg.yml of a webdesign theme.
     *
     * @param   integer $themeId    The ID of the webdesign template to
     *                              load the Wysiwyg editor config from.
     * @param   integer $tabCount   Number of tabs to be added on each
     *                              line if the config spreads over multiple
     *                              lines. Defaults to 0 (no indent).
     * @return  string              Wysiwyg editor config loaded from
     *                              Wysiwyg.yml file of theme identified
     *                              by $themeId.
     */
    public function getCustomWysiwygEditorConfig($themeId, $tabCount = 0) {
        $data = $this->getCustomWysiwygData($themeId, 'Config');

        // the cloudrexx guidelines states that a tab consists of 4 spaces
        $tab = str_repeat(' ', 4);

        return join("\n" . str_repeat($tab, $tabCount), $data);
    }

    /**
     * Get custom JavaScript code for the Wysiwyg editor that is defined
     * in the Wysiwyg.yml of a webdesign theme.
     *
     * @param   integer $themeId    The ID of the webdesign template to
     *                              load the Wysiwyg editor code from.
     * @return  string              Wysiwyg editor code loaded from
     *                              Wysiwyg.yml file of theme identified
     *                              by $themeId.
     */
    public function getCustomWysiwygEditorJsCode($themeId) {
        $data = $this->getCustomWysiwygData($themeId, 'Code');
        return join("\n", $data);
    }

    /**
     * Returns the Wysiwyg templates from config section
     *
     * @return  array   List of Wysiwyg templates loaded from config section
     */
    protected function getWysiwygTemplatesFromConfig() {
        // fetch templates from configuration section
        $templates = array();
        $em = $this->cx->getDb()->getEntityManager();
        $wysiwygTemplatesRepo = $em->getRepository('Cx\Core\Wysiwyg\Model\Entity\WysiwygTemplate');

        $field = 'order';
        \Cx\Core\Setting\Controller\Setting::init($this->getName(), 'config', 'Yaml');
        if (\Cx\Core\Setting\Controller\Setting::getValue('sortBehaviour', $this->getName()) === 'alphabetical') {
            $field = 'title';
        }

        $wysiwygTemplates = $wysiwygTemplatesRepo->findBy(
            array('active' => '1'),
            array($field => 'ASC')
        );
        foreach ($wysiwygTemplates as $wysiwygTemplate) {
            $template = [
                'title'         => $wysiwygTemplate->getTitle(),
                'description'   => $wysiwygTemplate->getDescription(),
                'html'          => $wysiwygTemplate->getHtmlContent(),
            ];
            $image = $wysiwygTemplate->getImagePath();
            if ($image) {
                $thumbnailFormats = $this->cx->getMediaSourceManager()->getThumbnailGenerator()->getThumbnails();
                $pathInfo = pathInfo($image);
                $thumbnails = $this->cx->getMediaSourceManager()->getThumbnailGenerator()->getThumbnailsFromFile($pathInfo['dirname'], $pathInfo['basename'], true);
                foreach ($thumbnailFormats as $thumbnailFormat) {
                    if (!isset($thumbnails[$thumbnailFormat['size']])) {
                        continue;
                    }
                    $image = $thumbnails[$thumbnailFormat['size']];
                    break;
                }
                try {
                    // this is not a no-op! this will validate the image's path
                    new \Cx\Lib\FileSystem\File($image);
                    $template['image'] = $image;
                } catch (\Cx\Lib\FileSystem\FileSystemException $e) {}
            }
            $templates[] = $template;
        }

        return $templates;
    }

    /**
     * Returns the data (as an instance of
     * \Cx\Core_Modules\Listing\Model\Entity\DataSet) of the Wysiwyg.yml file
     * from the theme identified by $themeId. If no theme can be identified by
     * $themeId, then the Wysiwyg.yml from the default theme is loaded instead.
     * If no data can be loaded, then null is returned
     *
     * @param   integer $themeId    The ID of the webdesign template (theme) to
     *                              load the Wysiwyg templates from.
     * @return  \Cx\Core_Modules\Listing\Model\Entity\DataSet   Loaded DataSet
     *                                                          of Wysiwyg.yml
     */
    protected function getWysiwygDataFromTheme($themeId) {
        // fetch from local cache
        if (isset($this->wysiwygDataCache[$themeId])) {
            return $this->wysiwygDataCache[$themeId];
        }

        // init local cache
        $this->wysiwygDataCache[$themeId] = null;

        // fetch templates from webdesign template (theme)
        $themeRepo = new \Cx\Core\View\Model\Repository\ThemeRepository();

        // fetch specific theme by $themeId
        $themeFolder = '';
        if (!empty($themeId)) {
            $themeFolder = $themeRepo->findById($themeId)->getFoldername();
        }

        // fetch default theme as fallback
        if (empty($themeFolder)) {
            $themeFolder = $themeRepo->getDefaultTheme()->getFoldername();
        }

        // check if Wysiwyg.yml does exists
        $wysiwygDataPath = $this->cx->getClassLoader()->getFilePath($this->cx->getWebsiteThemesPath() . '/' . $themeFolder. '/Wysiwyg.yml');
        if ($wysiwygDataPath === false) {
            return null;
        }

        // load Wysiwyg.yml from theme
        $wysiwygData = \Cx\Core_Modules\Listing\Model\Entity\DataSet::load($wysiwygDataPath, false);
        if (!$wysiwygData) {
            return null;
        }

        // store in local cache for later usages
        $this->wysiwygDataCache[$themeId] = $wysiwygData;

        return $wysiwygData;
    }

    /**
     * Returns specific data from the Wysiwyg.yml of a webdesign theme.
     *
     * @param   integer $themeId    The ID of the webdesign template (theme) to
     *                              load the Wysiwyg data from.
     * @param   string  $key        The key by which the Wysiwyg data is
     *                              identified by.
     * @return  array               Data identified by $key of Wysiwyg.yml file
     *                              of theme identified by $themeId.
     */
    protected function getCustomWysiwygData($themeId, $key) {
        try {
            $wysiwygData = $this->getWysiwygDataFromTheme($themeId);
            if (!$wysiwygData) {
                return array();
            }

            if (!$wysiwygData->entryExists($key)) {
                return array();
            }

            return $wysiwygData->getEntry($key);
        } catch (\Exception $e) {
            \DBG::log($e->getMessage());
            return array();
        }
    }

    /**
     * find all custom css variables and return an array with the values
     *
     * @param integer $skinId skin id, default is 0
     * @return array List with needed wysiwyg options
     */
    public function getCustomCSSVariables(int $skinId = 0): array {
        $themeRepo = new \Cx\Core\View\Model\Repository\ThemeRepository();
        $skin = '';
        $content = '';
        $cssArr = array();
        $ymlOption = array();
        $componentData = array();
        \Cx\Core\Setting\Controller\Setting::init($this->getName(), 'config', 'Yaml');
        if (!\Cx\Core\Setting\Controller\Setting::isDefined('specificStylesheet')
            && !\Cx\Core\Setting\Controller\Setting::add('specificStylesheet', '0', ++$i, \Cx\Core\Setting\Controller\Setting::TYPE_CHECKBOX, '1', 'config')
        ){
            throw new \Exception("Failed to add new configuration option");
        }
        if (!\Cx\Core\Setting\Controller\Setting::isDefined('replaceActualContents')
            && !\Cx\Core\Setting\Controller\Setting::add('replaceActualContents', '0', ++$i, \Cx\Core\Setting\Controller\Setting::TYPE_CHECKBOX, '1', 'config')
        ){
            throw new \Exception("Failed to add new configuration option");
        }

        // fetch theme specified by $skinId
        if (
            !empty($skinId)
            && $themeRepo->findById($skinId)
        ) {
            $skin = $themeRepo->findById($skinId)->getFoldername();
            $componentData = $themeRepo->findById($skinId)->getComponentData();
        }

        // fetch default theme
        if (empty($skin)) {
            $skin = $themeRepo->getDefaultTheme()->getFoldername();
            $componentData = $themeRepo->getDefaultTheme()->getComponentData();
        }

        if(\Cx\Core\Setting\Controller\Setting::getValue('specificStylesheet', $this->getName())){
            $path = $this->cx->getClassLoader()->getFilePath($this->cx->getCodeBaseThemesPath() . '/' . $skin . '/index.html');
            if ($path) {
                $content = file_get_contents($path);
                $cssArr = \JS::findCSS($content, false);
            }
        }

        if(!empty($componentData['rendering']['wysiwyg'])){
            $ymlOption = $componentData['rendering']['wysiwyg'];
        }

        if (!empty($ymlOption['css'])) {
            $filePath = $this->cx->getWebsiteThemesWebPath() . '/' . $skin . '/' . $ymlOption['css'];
            if ($this->cx->getClassLoader()->getFilePath($filePath)) {
                $cssArr[] = $filePath;
            }
        }

        return array(
            'css' => $cssArr,
            'bodyClass' => !empty($ymlOption['bodyClass'])?$ymlOption['bodyClass']:'',
            'bodyId' => !empty($ymlOption['bodyId'])?$ymlOption['bodyId']:'',
        );
    }

    /**
     * @inheritdoc
     */
    public function registerEvents() {
        $this->cx->getEvents()->addEvent('wysiwygCssReload');
    }

    /**
    * @inheritdoc
    */
    public function registerEventListeners() {
        $eventListener = new WysiwygEventListener($this->cx);
        $this->cx->getEvents()->addEventListener('mediasource.load', $eventListener);
        $this->cx->getEvents()->addEventListener(
            'wysiwygCssReload',
            $this
        );
    }

    /**
     * Get the Toolbar of the given type
     *
     * Returns the toolbar of the desired based on the restricted of functions 
     * according to user group and default setting
     * @param string    $type   Type of desired Toolbar (one of the following:
     *                          small, full, frontendEditingContent,
     *                          frontendEditingTitle or bbcode)
     * @return string           Toolbar of the desired type based on the
     *                          restrictions according to user group and default
     *                          setting
     */
    public function getToolbar($type = 'Full') {
        $toolbarController = $this->getController('Toolbar');
        return $toolbarController->getToolbar($type);
    }

    /**
     * Get the buttons that shall be removed or unchecked
     * @return string
     * @internal param bool|false $buttonsOnly If set, returns only the buttons
     *                                      no config.removedButtons prefix
     * @internal param bool|false $isAccess If set, removes the prefix
     *                                      config.removedButtons from the string
     */
    public function getRemovedButtons() {
        $toolbarController = $this->getController('Toolbar');
        $buttons = $toolbarController->getRemovedButtons();
        return $buttons;
    }

    /**
     * Get the path to the CKeditor JavaScript library
     *
     * @return  string
     */
    public function getLibraryPath() {
        return $this->cx->getLibraryFolderName() . static::LIB_PATH;
    }

    /**
     * Get the path to the CKeditor config file
     *
     * @return  string
     */
    public function getConfigPath() {
        $this->getComponent('Security')->enableCspScriptSrcUnsafeInline();
        $localeIsoCode = '';
        $em = $this->cx->getDb()->getEntityManager();
        if ($this->cx->getMode() == \Cx\Core\Core\Controller\Cx::MODE_BACKEND) {
            try {
                // get ISO-639-1 code of backend language
                $backend = $em->find(
                    'Cx\Core\Locale\Model\Entity\Backend',
                    LANG_ID
                );
                $localeIsoCode = $backend->getIso1()->getId();
            } catch (\Throwable $e) {}
        }

        if (empty($localeIsoCode)) {
            try {
                // get currently selected frontend locale
                $locale = $em->find(
                    'Cx\Core\Locale\Model\Entity\Locale',
                    FRONTEND_LANG_ID
                );
                $localeIsoCode = $locale->getIso1()->getId();
            } catch (\Throwable $e) {}
        }

        return '/api/Data/Plain/' . $this->getName() . '/getConfig?locale=' . $localeIsoCode;
    }

    /**
     * {@inheritDoc}
     */
    public function preContentLoad(\Cx\Core\ContentManager\Model\Entity\Page $page) {
        // register CKeditor JavaScript library
        $jsLibraryPath = $this->getLibraryPath() . '/ckeditor.js';
        if (strpos($jsLibraryPath, '/') === 0) {
            $jsLibraryPath = substr($jsLibraryPath, 1);
        }
        \JS::registerJsLibrary(
            'ckeditor',
            array(
                'jsfiles'       => array(
                    $jsLibraryPath,
                ),
                'dependencies' => array('jquery'),
            )
        );
    }

    /**
     * Sends config for CKEditor to browser and ends request.
     * Sets content-type to: application/javascript
     */
    public function getConfig() {
        $this->cx->getResponse()->setContentType('application/javascript');
        $this->cx->getResponse()->setAbstractContent(
            $this->parseConfig()
        );
        $this->cx->getResponse()->setParser(
            function($response) {
                return $response->getAbstractContent();
            }
        );
        $this->cx->getResponse()->send();
    }

    /**
     * Builds the CKEditor config and returns it
     *
     * @return  string
     */
    protected function parseConfig() {
        $pageId = !empty($_GET['pageId']) ? $_GET['pageId'] : null;

        //get the main domain
        $domainRepository = $this->cx->getComponent('Net')->getDomainRepository();
        $mainDomain = $domainRepository->getMainDomain()->getName();

        //find the right css files and put it into the wysiwyg
        \Cx\Core\Setting\Controller\Setting::init($this->getName(), 'config', 'Yaml');

        $skinId = 0;
        if (!empty($pageId) && $pageId != 'new') {
            $em = $this->cx->getDb()->getEntityManager();
            $pageRepo = $em->getRepository('Cx\Core\ContentManager\Model\Entity\Page');
            $skinId = $pageRepo->find($pageId)->getSkin();
        }
        $ymlOption = $this->getCustomCSSVariables($skinId);

        // load language data
        $_ARRAYLANG = \Env::get('init')->getComponentSpecificLanguageData(
            $this->getName(),
            false
        );

        $css = '';
        if (count($ymlOption['css'])) {
            $css = "'" . implode("','", $ymlOption['css']) . "'";
        }

        $language = '';
        if (!empty($_GET['locale'])) {
            $language = "config.language = '" . preg_replace('/[^a-z]/', '', $_GET['locale']) . "';";
        }
        $baseUrl = \Cx\Core\Routing\Url::fromCapturedRequest('', $this->cx->getWebsiteOffsetPath(), array());
        $baseUrl->isFrontend(false);
        $baseHref = preg_replace('/^https?:/', '', $baseUrl->toString());
        $replaceActualContents = \Cx\Core\Setting\Controller\Setting::getValue('replaceActualContents', $this->getName())? 'true' : 'false';
        $customWysiwygEditorConfig = '';
        try {
            $customWysiwygEditorConfig = $this->getCustomWysiwygEditorConfig($skinId, 1);
        } catch (\Throwable $t) {
            \DBG::msg($t->getMessage());
        }
        $loadingTemplates = '{}';
        try {
            $loadingTemplates = $this->getWysiwygTemplates($skinId);
        } catch (\Throwable $t) {
            \DBG::msg($t->getMessage());
        }
        $removeMediaBrowserButton = 1;
        if (\FWUser::getFWUserObject()->objUser->login()) {
            if (\FWUser::getFWUserObject()->objUser->getAdminStatus()) {
                $removeMediaBrowserButton = 0;
            } else {
                $arrAssociatedGroupIds = \FWUser::getFWUserObject()->objUser->getAssociatedGroupIds();
                foreach ($arrAssociatedGroupIds as $groupId) {
                    $objGroup = \FWUser::getFWUserObject()->objGroup->getGroup($groupId);
                    if ($objGroup) {
                        if ($objGroup->getType() == 'backend') {
                            $isBackendGroup = true;
                            break;
                        }
                    }
                }
                if ($isBackendGroup) {
                    $removeMediaBrowserButton = 0;
                }
            }
        }
        $customWysiwygEditorJsCode = '';
        try {
            $customWysiwygEditorJsCode = $this->getCustomWysiwygEditorJsCode($skinId);
        } catch (\Throwable $t) {
            \DBG::msg($t->getMessage());
        }

        $placeholder = addslashes($_ARRAYLANG['TXT_WYSIWYG_EDITOR_PLACEHOLDER']);

        return <<<CONFIG
//if the wysiwyg css not defined in the session, then load the css variables and put it into the session
if(!cx.variables.get('css', 'wysiwyg')) {
    cx.variables.set('css', [$css], 'wysiwyg');
    cx.variables.set('bodyClass', '{$ymlOption['bodyClass']}', 'wysiwyg');
    cx.variables.set('bodyId', '{$ymlOption['bodyId']}', 'wysiwyg');
}

CKEDITOR.scriptLoader.load( '{$this->cx->getCodeBaseCoreModuleWebPath()}/MediaBrowser/View/Script/MediaBrowserCkeditorPlugin.js' );
CKEDITOR.scriptLoader.load( '{$this->cx->getCodeBaseCoreWebPath()}/Wysiwyg/View/Script/ImagePasteCkeditorPlugin.js' );
CKEDITOR.editorConfig = function( config )
{
    config.skin = 'moono-lisa';

    config.height = 307;
    config.uiColor = '#ececec';
    config.editorplaceholder = '$placeholder';
    config.colorButton_enableMore = false;

    $language
    config.forcePasteAsPlainText = false;
    config.enterMode = CKEDITOR.ENTER_BR;
    config.shiftEnterMode = CKEDITOR.ENTER_P;
    config.startupOutlineBlocks = true;
    config.allowedContent = true;

    config.ignoreEmptyParagraph = false;
    config.protectedSource.push(/<i[^>]*><\/i>/g);
    config.protectedSource.push(/<span[^>]*><\/span>/g);
    config.protectedSource.push(/<a[^>]*><\/a>/g);

    config.ignoreEmptyParagraph = false;
    config.protectedSource.push(/<i[^>]*><\/i>/g);
    config.protectedSource.push(/<span[^>]*><\/span>/g);
    config.protectedSource.push(/<a[^>]*><\/a>/g);

    config.tabSpaces = 4;
    config.baseHref = '{$baseHref}';
    config.templates_files = [ '' ];
    config.templates_replaceContent = $replaceActualContents;

    config.toolbar_Full = config.toolbar_Small = {$this->getToolbar()};

    config.toolbar_BBCode = {$this->getToolbar('bbcode')};

    config.toolbar_FrontendEditingContent = {$this->getToolbar('frontendEditingContent')};

    config.toolbar_FrontendEditingTitle = {$this->getToolbar('frontendEditingTitle')};

    // Allow div's within a's
    CKEDITOR.dtd['a']['div'] = 1;

    //Set the CSS Stuff
    // important: we must create a copy of the cx variable 'css' here, as it is
    // an array which would otherwise be assigned as a reference. Using a reference
    // would break any dynamic config updates as the CKEditor itself does update
    // the contentsCss option and would therefore overwrite the cx variable 'css'
    config.contentsCss = JSON.parse(JSON.stringify(cx.variables.get('css', 'wysiwyg')));
    config.bodyClass = cx.variables.get('bodyClass', 'wysiwyg');
    config.bodyId = cx.variables.get('bodyId', 'wysiwyg');
    if (
        window.location.pathname == cx.variables.get('cadminPath') + 'Config/Wysiwyg' ||
        window.location.pathname == cx.variables.get('cadminPath') + 'Access/group'
    ) {
        {$this->getRemovedButtons()};
    }

    // load custom config from Wysiwyg.yml of webdesign template
    $customWysiwygEditorConfig
};

//loading the templates
CKEDITOR.on('instanceReady',function(){
    var loadingTemplates = $loadingTemplates;
    for(var instanceName in CKEDITOR.instances) {
        loadingTemplates.button = CKEDITOR.instances[instanceName].getCommand("templates") //Reference to Template-Button

        // Define Standard-Path
        loadingTemplates.load = (function(){
            if (typeof this.button != 'undefined') {
                this.button.setState(CKEDITOR.TRISTATE_DISABLED) // Disable "Template"-Button
            }
            for(var i=0;i<this.length;i++){
                (function(item){
                    CKEDITOR.addTemplates('default',{
                        // CKeditor does not accept an empty imagesPath
                        // therefore, we have to perform a virtual traversal
                        // using a random folder path
                        imagesPath: cx.variables.get('basePath', 'contrexx') + 'random/..',
                        templates: this
                    });
                }).bind(this)(this[i])
            }
            if (typeof this.button != 'undefined') {
                this.button.setState(CKEDITOR.TRISTATE_ENABLE) // Enable "Template"-Button
            }
        }).bind(loadingTemplates)();
    }

    var translations = cx.variables.get('toolbarTranslations', 'toolbarConfigurator');
    if (translations) {
        cx.jQuery('div.toolbarModifier ul[data-type="table-body"] > li[data-type="group"] > ul > li[data-type="subgroup"] > p > span').each(
            function() {
                if (translations.hasOwnProperty(cx.jQuery(this).text())) {
                    var translation = cx.jQuery(this).text();
                    cx.jQuery(this).text(translations[translation]);
                }
            }
        );
    }
});

// add shadowbox functionality
CKEDITOR.on('dialogDefinition', function (event) {
    var editor = event.editor;
    var dialogDefinition = event.data.definition;

    // only add functionality to image dialog
    if (event.data.name != 'image') {
        return;
    }

    // add Shadowbox option to advanced tab
    var advancedTab = dialogDefinition.getContents( 'advanced' );
    if (advancedTab !== null) {
        // add checkbox
        advancedTab.add({
            type: 'checkbox',
            label: '{$_ARRAYLANG['TXT_WYSIWYG_MODAL_OPTION_LABEL']}',
            id: 'txtdlgGenShadowbox',
            onClick: function() {
                shadowboxSrc = this.getDialog().getContentElement( 'advanced', 'txtdlgGenShadowboxSrc' ).getElement();
                if (this.getValue()) {
                    shadowboxSrc.setStyle('display', 'block');
                    //if (!this.getDialog().getValueOf('advanced', 'txtdlgGenShadowboxSrc')) {
                        var imageSrc = this.getDialog().getValueOf('info', 'txtUrl');
                        var originalImage = imageSrc.replace(/\.thumb_([^.]+)\.(.{3,4})$/, '.$2').replace(/\.thumb$/,'')
                        this.getDialog().setValueOf('advanced', 'txtdlgGenShadowboxSrc', originalImage);
                    //}
                } else {
                    shadowboxSrc.setStyle('display', 'none');
                }
            },
            setup : function( type, element ) {
                //if ( type == LINK ) {}
                imgRel = element.getAttribute('data-shadowbox');
                if (!imgRel) {
                    this.setValue(false);
                } else {
                    this.setValue(true);
                }
            },
            commit : function( type, element ) {
                //if ( type == LINK ) {
                if ( this.getValue()) {
                    element.setAttribute('data-shadowbox', this.getDialog().getValueOf('advanced', 'txtdlgGenShadowboxSrc'));
                } else {
                    element.removeAttributes(['data-shadowbox']);
                }
            },
        });
        // add input field for shadowbox source
        advancedTab.add({
            type: 'text',
            label: '{$_ARRAYLANG['TXT_WYSIWYG_MODAL_OPTION_SRC']}',
            id: 'txtdlgGenShadowboxSrc',
            style: 'display:none',
            setup : function( type, element ) {
                //if ( type == LINK ) {}
                imgRel = element.getAttribute('data-shadowbox');
                this.setValue(imgRel);
                if (!imgRel) {
                    this.getElement().setStyle('display', 'none');
                } else {
                    this.getElement().setStyle('display', 'block');
                }
            }
        });
    }
});

// hide 'browse'-buttons in case the user is a sole frontend-user
// and is not permitted to access the MediaBrowser or Uploader
if ($removeMediaBrowserButton) {
    CKEDITOR.on('dialogDefinition', function(ev) {
        var dialogName       = ev.data.name;
        var dialogDefinition = ev.data.definition;

        if (dialogName == 'link') {
            dialogDefinition.getContents('info').remove('browse');
        }

        if (dialogName == 'image') {
            dialogDefinition.getContents('info').remove('browse');
            dialogDefinition.getContents('Link').remove('browse');
        }

        if (dialogName == 'flash') {
            dialogDefinition.getContents('info').remove('browse');
        }
    });
}

// load custom code from Wysiwyg.yml of webdesign template
$customWysiwygEditorJsCode

// attach the .equals method to Array's prototype to call it on any array
Array.prototype.equals = function (array) {
    // if the other array is a falsy value, return
    if (!array) {
        return false;
    }

    // compare lengths - can save a lot of time
    if (this.length != array.length) {
        return false;
    }

    for (var i = 0, l=this.length; i < l; i++) {
        // Check if we have nested arrays
        if (this[i] instanceof Array && array[i] instanceof Array) {
            // recurse into the nested arrays
            if (!this[i].equals(array[i])) {
                return false;
            }
        } else if (this[i] != array[i]) {
            // Warning - two different object instances will never be equal: {x:20} != {x:20}
            return false;
        }
    }
    return true;
};
CONFIG;
    }
}
