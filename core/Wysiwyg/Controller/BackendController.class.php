<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * Specific BackendController for this Component. Use this to easily create a backend view
 *
 * @copyright   Cloudrexx AG
 * @author      Sebastian Brand <sebastian.brand@comvation.com>
 * @package     cloudrexx
 * @subpackage  core_wysiwyg
 * @version     1.0.0
 */

namespace Cx\Core\Wysiwyg\Controller;

/**
 * Specific BackendController for this Component. Use this to easily create a backend view
 *
 * @copyright   Cloudrexx AG
 * @author      Sebastian Brand <sebastian.brand@comvation.com>
 * @package     cloudrexx
 * @subpackage  core_wysiwyg
 * @version     1.0.0
 */
class BackendController extends \Cx\Core\Core\Model\Entity\SystemComponentBackendController {
    /**
    * Returns a list of available commands (?act=XY)
    * @return array List of acts
    */
    public function getCommands() {
        return [];
    }

    /**
    * Use this to parse your backend page
    *
    * You will get the template located in /View/Template/{CMD}.html
    * You can access Cx class using $this->cx
    * To show messages, use \Message class
    * @param \Cx\Core\Html\Sigma $template Template for current CMD
    * @param array $cmd CMD separated by slashes
    * @global array $_ARRAYLANG Language data
    */
    public function parsePage(\Cx\Core\Html\Sigma $template, array $cmd, &$isSingle = false) {
        global $_ARRAYLANG;

        // note: this is a workaround as this section is not being called
        // directly by the SystemComponentController and therefore no access
        // check is performed.
        \Permission::checkAccess(192, 'static');

        // Parse entity view generation pages
        $entityClassName = $this->getNamespace() . '\\Model\\Entity\\' . current($cmd);
        if (in_array($entityClassName, $this->getEntityClasses())) {
            $this->parseEntityClassPage($template, $entityClassName, current($cmd));
            return;
        }

        // Not an entity, parse overview or settings
        switch (current($cmd)) {
            case 'Settings':
                \Cx\Core\Setting\Controller\Setting::init($this->getName(), 'config', 'Yaml');

                if(isset($_POST) && isset($_POST['bsubmit'])) {
                    \Cx\Core\Setting\Controller\Setting::set('specificStylesheet', isset($_POST['specificStylesheet'])?1:0);
                    \Cx\Core\Setting\Controller\Setting::set('replaceActualContents', isset($_POST['replaceActualContents'])?1:0);
                    \Cx\Core\Setting\Controller\Setting::set(
                        'sortBehaviour',
                        isset($_POST['sortBehaviour']) ? $_POST['sortBehaviour'] : 'custom'
                    );
                    \Cx\Core\Setting\Controller\Setting::storeFromPost();
                }

                $i = 0;
                if (!\Cx\Core\Setting\Controller\Setting::isDefined('specificStylesheet')
                    && !\Cx\Core\Setting\Controller\Setting::add('specificStylesheet', '0', ++$i, \Cx\Core\Setting\Controller\Setting::TYPE_CHECKBOX, '1', 'config')
                ){
                    throw new \Exception("Failed to add new configuration option");
                }
                if (!\Cx\Core\Setting\Controller\Setting::isDefined('replaceActualContents')
                    && !\Cx\Core\Setting\Controller\Setting::add('replaceActualContents', '0', ++$i, \Cx\Core\Setting\Controller\Setting::TYPE_CHECKBOX, '1', 'config')
                ){
                    throw new \Exception("Failed to add new configuration option");
                }
                if (
                    !\Cx\Core\Setting\Controller\Setting::isDefined('sortBehaviour') &&
                    !\Cx\Core\Setting\Controller\Setting::add(
                        'sortBehaviour',
                        'custom',
                        ++$i,
                        \Cx\Core\Setting\Controller\Setting::TYPE_RADIO,
                        'alphabetical:TXT_CORE_WYSIWYG_ALPHABETICAL,custom:TXT_CORE_WYSIWYG_CUSTOM',
                        'config'
                    )
                ) {
                    throw new \Exception('Failed to add new configuration option');
                }

                $tmpl = new \Cx\Core\Html\Sigma();
                \Cx\Core\Setting\Controller\Setting::show(
                    $tmpl,
                    'index.php?cmd=Config&act=Wysiwyg&tpl=Settings',
                    $_ARRAYLANG['TXT_CORE_WYSIWYG'],
                    $_ARRAYLANG['TXT_CORE_WYSIWYG_ACT_SETTINGS'],
                    'TXT_CORE_WYSIWYG_'
                );

                $template->setVariable('WYSIWYG_CONFIG_TEMPLATE', $tmpl->get());
                break;
            case 'Functions':
                $toolbarController = $this->getController('Toolbar');
                // check if the toolbar shall be saved
                if (isset($_POST) && isset($_POST['save'])) {
                    // Get the database connection
                    $dbCon = $this->cx->getDb()->getAdoDb();
                    // Check if there is already a default toolbar
                    $defaultToolbar = $dbCon->Execute('
                        SELECT `id` FROM `' . DBPREFIX . 'core_wysiwyg_toolbar`
                        WHERE `is_default` = 1
                        LIMIT 1');
                    // Check if the query did not fail
                    if (!$defaultToolbar) {
                        throw new \Exception('Failed to check for existing default toolbar!');
                    }
                    // Get the default toolbar id
                    $toolbarId = $defaultToolbar->fields['id'];
                    $toolbarController->store(
                        $_POST['removedButtons'],
                        $toolbarId,
                        true
                    );
                }
                $toolbarConfigurator = $toolbarController->getToolbarConfiguratorTemplate(
                    $this->getDirectory(false, true),
                    true
                );
                // Get the template and replace the placeholder
                $template->setVariable(
                    'WYSIWYG_CONFIG_TEMPLATE',
                    $toolbarConfigurator->get()
                );
                break;
            case '':
            default:
                if ($template->blockExists('overview')) {
                    $template->touchBlock('overview');
                }
                break;
        }
    }

    /**
     * This function returns the ViewGeneration options for a given entityClass
     *
     * @access protected
     * @global $_ARRAYLANG
     * @param $entityClassName contains the FQCN from entity
     * @param $dataSetIdentifier if $entityClassName is DataSet, this is used for better partition
     * @return array with options
     */
    protected function getViewGeneratorOptions($entityClassName, $dataSetIdentifier = '') {
        global $_ARRAYLANG;

        $classNameParts = explode('\\', $entityClassName);
        $classIdentifier = end($classNameParts);

        if (empty($classIdentifier)) {
            return array();
        }

        $sortBy = array('field' => ['order' => SORT_ASC]);
        $order  = array('order' => SORT_ASC);
        \Cx\Core\Setting\Controller\Setting::init($this->getName(), 'config', 'Yaml');
        if (\Cx\Core\Setting\Controller\Setting::getValue('sortBehaviour', $this->getName()) === 'alphabetical') {
            $sortBy = array();
            $order  = array('title' => SORT_ASC);
        }

        return array(
            'header' => '',
            'entityName' => $_ARRAYLANG['TXT_' . strtoupper($this->getType() . '_' . $this->getName() . '_ACT_' . $classIdentifier) . '_ENTITY'],
            'order' => array(
                'overview' => array(
                    'active',
                    'title',
                    'description',
                ),
                'form' => array(
                    'active',
                    'title',
                    'description',
                ),
            ),
            'functions' => array(
                'add'       => true,
                'edit'      => true,
                'delete'    => true,
                'order'     => $order,
                'paging'    => true,
                'filtering' => false,
                'sortBy'    => $sortBy,
                'status' => array('field' => 'active'),
            ),
            'fields' => array(
                'id' => array(
                    'showOverview' => false,
                ),
                'title' => array(
                    'header' => $_ARRAYLANG['TXT_' . strtoupper($this->getType() . '_' . $this->getName() . '_ACT_' . $classIdentifier) . '_TITLE'],
                    'table' => array(
                        'parse' => function($data, $rows, $options, $vgId) {
                            $editUrl = clone \Env::get('cx')->getRequest()->getUrl();
                            $editUrl->setParam('editid', '{' . $vgId . ',' . $rows['id'] . '}');
                            $data = '<a href="' . $editUrl . '" title="'.$data.'">'.$data.'</a>';
                            return $data;
                        },
                    ),
                ),
                'description' => array(
                    'header' => $_ARRAYLANG['TXT_' . strtoupper($this->getType() . '_' . $this->getName() . '_ACT_' . $classIdentifier) . '_DESCRIPTION'],
                ),
                'active' => array(
                    'header' => $_ARRAYLANG['TXT_' . strtoupper($this->getType() . '_' . $this->getName() . '_ACT_' . $classIdentifier) . '_STATE'],
                    'formtext' => $_ARRAYLANG['TXT_' . strtoupper($this->getType() . '_' . $this->getName() . '_ACT_' . $classIdentifier) . '_ACTIVE'],
                    'sorting' => false,
                ),
                'imagePath' => array(
                    'header' => $_ARRAYLANG['TXT_' . strtoupper($this->getType() . '_' . $this->getName() . '_ACT_' . $classIdentifier) . '_IMAGE_PATH'],
                    'type' => 'image',
                    'showOverview' => false,
                    'options' => array('startmediatype' => 'wysiwyg'),
                ),
                'htmlContent' => array(
                    'header' => $_ARRAYLANG['TXT_' . strtoupper($this->getType() . '_' . $this->getName() . '_ACT_' . $classIdentifier) . '_HTML_CONTENT'],
                    'showOverview' => false,
                    'type' => 'sourcecode',
                    'options' => array('mode' => 'html'),
                ),
                'order' => array(
                    'header' => $_ARRAYLANG['TXT_' . strtoupper($this->getType() . '_' . $this->getName() . '_ACT_' . $classIdentifier) . '_ORDER'],
                    'showOverview' => false,
                    'showDetail' => false,
                ),
            ),
        );
    }
}
