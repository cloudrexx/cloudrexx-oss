<?php
declare(strict_types=1);

/**
 * Cloudrexx
 *
 * @link      https://www.cloudrexx.com
 * @copyright Cloudrexx AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

namespace Cx\Core\Config\Model\Event;

/**
 * @copyright   Cloudrexx AG
 * @author      Thomas Wirz <thomas.wirz@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  core_config
 */
class ConfigEventListener extends \Cx\Core\Event\Model\Entity\DefaultEventListener {
    /**
     * @param array $config Config from event NetManager:getSenderDomains
     */
    public function NetManagerGetSenderDomains(array $config = []): array {
        $email = $this->cx->getConfig()->getBaseConfig('coreAdminEmail');
        $emailParts = explode('@', $email);
        $domain = array_pop($emailParts);
        $domain = \Cx\Core\Net\Controller\ComponentController::convertIdnToAsciiFormat(
            $domain
        );
        $langData = \Env::get('init')->getComponentSpecificLanguageData(
            'Config', false
        );
        return [$domain => [
            'strict' => false,
            'usages' => [[
                'editurl' => \Cx\Core\Routing\Url::fromBackend(
                    'Config',
                    '',
                    0,
                    ['tab-list0' => 'setting_tab-2']
                )->toString(),
                'title' => $langData['TXT_CORE_CONFIG_COREADMINEMAIL'],
            ]]
        ]];
    }
}
