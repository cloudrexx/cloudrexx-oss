<?php
declare(strict_types=1);

/**
 * Cloudrexx
 *
 * @link      https://www.cloudrexx.com
 * @copyright Cloudrexx
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

namespace Cx\Core\Config\Model\Entity;

/**
 * Must be loaded manually as the ClassLoader might not yet be present at this
 * stage.
 */
require_once dirname(__FILE__) . '/ConfigArray.class.php';

/**
 * @copyright   Cloudrexx AG
 * @author      Thomas Wirz <thomas.wirz@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  core_config
 */
class SystemConfig {

    /**
     * @var ConfigArray
     */
    protected $baseConfig;

    /**
     * @var ConfigArray
     */
    protected $dbConfig;

    /**
     * @var string Config file name, empty if no config file was loaded
     */
    protected $source = '';
    
    /**
     * If $configFilePath is not given, the configuration in the same codebase
     * as this file is loaded.
     * If globals $_CONFIG or $_DBCONFIG are set, they are used to initialize
     * the configuration.
     * @param string $configFilePath (optional) Absolute path to the configuration.php file to load
     */
    public function __construct(string $configFilePath = '') {
        $this->loadLegacyGlobals();
        if (!$this->baseConfig) {
            $this->baseConfig = new ConfigArray();
        }
        if (!$this->dbConfig) {
            $this->dbConfig = new ConfigArray('DB');
        }
        $this->setLegacyGlobals();
        $this->loadConfig($configFilePath);
        $this->fixPathConfig();
    }

    /**
     * Load {@see static::$baseConfig} and {@see static::$dbConfig}
     * from globals $_CONFIG and $_DBCONFIG if the globals are already set.
     * This is a work-around for sites that load two different config files
     * with one depending on the other.
     * **Important**: Do not rely on this feature as it will be dropped in the
     * future.
     */
    protected function loadLegacyGlobals(): void {
        global $_DBCONFIG, $_CONFIG;
        if (is_object($_CONFIG)) {
            $this->baseConfig = $_CONFIG;
        }
        if (is_object($_DBCONFIG)) {
            $this->dbConfig = $_DBCONFIG;
        }
    }

    /**
     * Add legacy wrappers for {@see static::$baseConfig} and {@see static::$dbConfig}
     * as globals $_CONFIG and $_DBCONFIG. Note that the config actually gets loaded
     * through those global wrappers in configuration.php and settings.php.
     */
    protected function setLegacyGlobals(): void {
        global $_DBCONFIG, $_CONFIG;
        $_CONFIG = $this->baseConfig;
        $_DBCONFIG = $this->dbConfig;
    }

    /**
     * Load global configuration from configuration.php
     *
     * Load {@see static::$baseConfig} (through wrapper $_CONFIG),
     * {@see static::$dbConfig} (through wrapper $_DBCONFIG) & $_PATHCONFIG.
     * Note that $_PATHCONFIG is not handled by this class for the following reason:
     * - $_PATHCONFIG is obsolete. See CLX-4284
     */
    protected function loadConfig(string $configFilePath = ''): void {
        if (!$configFilePath) {
            $configFilePath = dirname(__FILE__, 5) . '/config/configuration.php';
        }
        if (!file_exists($configFilePath)) {
            return;
        }
        $this->source = $configFilePath;
        \DBG::debug('Cx: LoadConfig: '.$configFilePath);
        include $configFilePath;
    }

    /**
     * Returns the config path 
     * @return string Config file path or empty string if no config file was loaded
     */
    public function getSource(): string {
        return $this->source;
    }

    /**
     * @return string
     *    Base configuration option identified by key $key from $_CONFIG
     *    in configuration.php and settings.php or overwritten by an associated
     *    environment variable of the form CLX_CONFIG_<KEY>. See
     *    {@see ConfigArray::offsetGet()} for a complete reference of the
     *    format of environment variables.
     */
    public function getBaseConfigString(string $key): string {
        return $this->baseConfig[$key];
    }

    /**
     * @return int
     *    Base configuration option identified by key $key from $_CONFIG
     *    in configuration.php and settings.php or overwritten by an associated
     *    environment variable of the form CLX_CONFIG_<KEY>. See
     *    {@see ConfigArray::offsetGet()} for a complete reference of the
     *    format of environment variables.
     */
    public function getBaseConfigInt(string $key): int {
        return $this->baseConfig[$key];
    }

    /**
     * @deprecated Use {@see static::getBaseConfigString()} or {@see static::getBaseConfigInt()} instead.
     * @return string|int
     *    Base configuration option identified by key $key from $_CONFIG
     *    in configuration.php and settings.php or overwritten by an associated
     *    environment variable of the form CLX_CONFIG_<KEY>. See
     *    {@see ConfigArray::offsetGet()} for a complete reference of the
     *    format of environment variables.
     */
    public function getBaseConfig(string $key) {
        return $this->baseConfig[$key];
    }

    /**
     * @return  ConfigArray
     *    Complete base configuration set by $_CONFIG in configuration.php
     *    and settings.php or overwritten by associated environment variables
     *    of the form CLX_CONFIG_<KEY>. See {@see ConfigArray::offsetGet()} for
     *    a complete reference of the format of environment variables.
     */
    public function getFullBaseConfig(): ConfigArray {
        return $this->baseConfig;
    }

    /**
     * @return  string
     *    Database configuration option identified by key $key from $_DBCONFIG
     *    in configuration.php or overwritten by an associated environment
     *    variable of the form CLX_DBCONFIG_<KEY>. See {@see ConfigArray::offsetGet()}
     *    for a complete reference of the format of environment variables.
     */
    public function getDbConfig(string $key): string {
        return $this->dbConfig[$key];
    }

    /**
     * @return  ConfigArray
     *    Complete database configuration set by $_DBCONFIG in configuration.php
     *    or overwritten by associated environment variables of the form
     *    CLX_DBCONFIG_<KEY>. See {@see ConfigArray::offsetGet()} for a
     *    complete reference of the format of environment variables.
     */
    public function getFullDbConfig(): ConfigArray {
        return $this->dbConfig;
    }

    /**
     * Ensures that $_PATHCONFIG is set correctly.
     * @deprecated  See CLX-4284
     */
    protected function fixPathConfig(): void {
        global $_PATHCONFIG;

        // in case they do not exist, initialize the path config vars
        foreach (
            array(
                'root',
                'root_offset',
                'installation_root',
                'installation_offset'
            ) as $idx
        ) {
            if (!isset($_PATHCONFIG['ascms_' . $idx])) {
                $_PATHCONFIG['ascms_' . $idx] = '';
            }
        }

        /**
         * Should we overwrite path configuration?
         */
        $fixPaths = false;
        // path configuration is empty, so yes, we should...
        if (empty($_PATHCONFIG['ascms_root'])) {
            $fixPaths = true;
        } else {
            if (
                substr(
                    !empty($_GET['__cap']) ? $_GET['__cap'] : '',
                    0,
                    strlen($_PATHCONFIG['ascms_root_offset'])
                ) != $_PATHCONFIG['ascms_root_offset']
            ) {
                // URL doesn't seem to start with provided offset
                $fixPaths = true;
            }
        }
        if ($fixPaths) {
            $this->detectSystemPaths(
                $_PATHCONFIG['ascms_root'],
                $_PATHCONFIG['ascms_root_offset']
            );
        }
        if ($fixPaths || empty($_PATHCONFIG['ascms_installation_root'])) {
            $_PATHCONFIG['ascms_installation_root'] = $_PATHCONFIG['ascms_root'];
            $_PATHCONFIG['ascms_installation_offset'] = $_PATHCONFIG['ascms_root_offset'];
        }
    }

    /**
     * Sets/overwrites $documentRoot to the document root of the php-fpm
     * environment and $rootOffset to the web offset path
     */
    protected function detectSystemPaths(string &$documentRoot, string &$rootOffset): void {
        // calculate correct offset path
        // turning '/myoffset/somefile.php' into '/myoffset'
        $rootOffset = '';
        $directories = explode('/', $_SERVER['SCRIPT_NAME']);
        for ($i = 0; $i < count($directories) - 1; $i++) {
            if ($directories[$i] !== '') {
                $rootOffset .= '/'.$directories[$i];
            }
        }

        // fix wrong offset if another file than index.php was requested
        // turning '/myoffset/core_module/somemodule' into '/myoffset'
        $fileRoot = dirname(__FILE__, 5);
        $nonOffset = preg_replace('#' . preg_quote($fileRoot) . '#', '', realpath($_SERVER['SCRIPT_FILENAME']));
        $nonOffsetParts = preg_split('#[/\\\\]#', $nonOffset);
        end($nonOffsetParts);
        unset($nonOffsetParts[key($nonOffsetParts)]);
        $nonOffset = implode('/', $nonOffsetParts);
        $rootOffset = preg_replace('#' . preg_quote($nonOffset) . '#', '', $rootOffset);

        // calculate correct document root
        // turning '/var/www/myoffset' into '/var/www'
        $documentRoot = '';
        $arrMatches = array();
        $scriptPath = str_replace('\\', '/', dirname(__FILE__, 3));
        if (preg_match("/(.*)(?:\/[\d\D]*){2}$/", $scriptPath, $arrMatches) == 1) {
            $scriptPath = $arrMatches[1];
        }
        if (preg_match("#(.*)". preg_quote($rootOffset) ."#", $scriptPath, $arrMatches) == 1) {
            $documentRoot = $arrMatches[1];
        }

        // fix wrong variable assignment in CLI
        if (empty($documentRoot) && !empty($rootOffset)) {
            $documentRoot = $rootOffset;
            $rootOffset = '';
        }
    }
}
