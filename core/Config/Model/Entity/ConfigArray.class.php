<?php
declare(strict_types=1);

/**
 * Cloudrexx
 *
 * @link      https://www.cloudrexx.com
 * @copyright Cloudrexx AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

namespace Cx\Core\Config\Model\Entity;

/**
 * Implementation of {@see \ArrayObject} with the addition that values will be
 * fetched from the environment instead from a locally set key.
 * {@inheritDoc}
 *
 * @copyright   Cloudrexx AG
 * @author      Thomas Wirz <thomas.wirz@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  core_core
 */
class ConfigArray extends \ArrayObject {

    /**
     * As getenv() is quite slow we cache the whole list of environment variables
     * @var array List of environment variables
     */
    protected static $env = array();

    /**
     * @var string
     */
    protected $prefix = '';

    /**
     * @var array
     */
    protected $config = [];

    /**
     * Construct a new Cloudrexx Config array
     *
     * @param   string  $prefix Use to set
     */
    public function __construct($prefix = '') {
        $this->prefix = 'CLX_' . strtoupper($prefix) . 'CONFIG_';
        if (!count(static::$env)) {
            static::$env = getenv();
            // TODO: This is a workaround for JSON-encoded values. See CLX-4345
            array_walk(
                static::$env,
                function(&$value, $key) {
                    if (!is_string($value)) {
                        return;
                    }
                    if (
                        !preg_match(
                            '/^".+"$/',
                            $value
                        )
                    ) {
                        return;
                    }
                    $value = preg_replace(['/(^"|"$)/','/\\\"/'], ['', '"'], $value);
                }
            );
        }
        parent::__construct();
    }

    /**
     * @inheritDoc
     */
    public function offsetExists($offset): bool {
        return isset($this->config[$offset]);
    }

    /**
     * @see static::offsetExists()
     */
    public function exists(string $key): bool {
        return $this->offsetExists($key);
    }

    /**
     * {@inheritDoc}
     * If an matching system environment variable is set, then the value of
     * that environment varible is returned instead. Any set value on this
     * array will have no effect in that case.
     * A matching system environment variable is defined as follows:
     * Each uppercase character in $offset (which should be in camelCase
     * notation) is prefixed by an underscore character, before converting the
     * resulting string all into uppercase and prefixing it with
     * CLX_<prefix>CONFIG_. Where <prefix> is substituted by $prefix passed to
     * the constructor of the object. Some examples follow.
     * If $offset is 'cacheUserCacheMemcachedConfig' and object has been
     * initialized with an empty $prefix, then the matching environment
     * variable is: CLX_CONFIG_CACHE_USER_CACHE_MEMCACHED_CONFIG
     * Or if $offset is 'host' and the object has been initialized with
     * $prefix set to 'DB', then its matching environment variable is:
     * CLX_DBCONFIG_HOST
     *
     * @return string|int|null
     *     Returned type depends on option identified by `$offset`. If `$offset`
     *     is undefined, then `null` is returned. **Important**: this will change
     *     in the future. Therefore you must not depend on that. Instead use
     *     {@see static::exists()} to check for the existance of an option.
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($offset) {
        $envVar = $this->prefix . strtoupper(
            preg_replace(
                '/([[:upper:]])/',
                '_\1',
                lcfirst($offset)
            )
        );
        if (isset(static::$env[$envVar])) {
            return static::$env[$envVar];
        }
        if (isset($this->config[$offset])) {
            return $this->config[$offset];
        }
        return null;
    }

    /**
     * @see static::offsetGet()
     */
    #[\ReturnTypeWillChange]
    public function get(string $key) {
        return $this->offsetGet($key);
    }

    /**
     * @inheritDoc
     */
    public function offsetSet($offset, $value): void {
        if (is_null($offset)) {
            $this->config[] = $value;
        } else {
            $this->config[$offset] = $value;
        }
    }

    /**
     * @see static::offsetSet()
     */
    public function set(string $key, $value): void {
        $this->offsetSet($key, $value);
    }

    /**
     * @inheritDoc
     */
    public function offsetUnset($offset): void {
        unset($this->config[$offset]);
    }

    /**
     * @see static::offsetUnset()
     */
    public function unset(string $key): void {
        $this->offsetUnset($offset);
    }
}
