<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * @copyright   CLOUDREXX CMS - CLOUDREXX AG
 * @author      Cloudrexx Development Team <info@cloudrexx.com>
 * @access      public
 * @package     cloudrexx
 * @subpackage  core_config
 */
global $_ARRAYLANG;
$_ARRAYLANG['TXT_SETTINGS_MENU_SYSTEM'] = 'Système';
$_ARRAYLANG['TXT_SETTINGS_ERROR_NO_WRITE_ACCESS'] = 'Impossible d"enregistrer les paramètres, car le fichier <strong>%s</strong> est protégé contre l"écriture!<br />Veuillez recommencer après avoir modifié ces droits d"accès!';
$_ARRAYLANG['TXT_SYSTEM_SETTINGS'] = 'Paramètres de base';
$_ARRAYLANG['TXT_ACTIVATED'] = 'Activé';
$_ARRAYLANG['TXT_DEACTIVATED'] = 'Déactivé';
$_ARRAYLANG['TXT_CORE_CONFIG_SITE'] = 'Site WEB';
$_ARRAYLANG['TXT_SAVE'] = 'Enregistrer';
$_ARRAYLANG['TXT_CORE_CONFIG_SYSTEMSTATUS'] = 'Statut de la page';
$_ARRAYLANG['TXT_CORE_CONFIG_SYSTEMSTATUS_TOOLTIP'] = 'Page activée?';
$_ARRAYLANG['TXT_CORE_CONFIG_COREIDSSTATUS'] = 'Système de sécurité';
$_ARRAYLANG['TXT_CORE_CONFIG_COREIDSSTATUS_TOOLTIP'] = 'Statut du reporting de la détection d"intrusion Cloudrexx';
$_ARRAYLANG['TXT_CORE_CONFIG_XMLSITEMAPSTATUS_TOOLTIP'] = 'Génération automatique d\'un sitemap XML dans le répertoire principal du site web sous <i>/sitemap.xml</i>. Le sitemap XML est un outil important pour l\'optimisation des moteurs de recherche (SEO) et améliore la découvrabilité et l\'indexation des sites web dans les résultats de recherche.';
$_ARRAYLANG['TXT_CORE_CONFIG_COREGLOBALPAGETITLE'] = 'Titre de page général';
$_ARRAYLANG['TXT_CORE_CONFIG_COREGLOBALPAGETITLE_TOOLTIP'] = 'Titre de page général, utilisable dans les modèles de page via la variable [[GLOBAL_TITLE]].';
$_ARRAYLANG['TXT_SETTINGS_DOMAIN_URL'] = 'URL de la page d"accueil';
$_ARRAYLANG['TXT_SETTINGS_DOMAIN_URL_HELP'] = 'Veuillez saisir l"URL de votre site, SANS slash ( / ) final!';
$_ARRAYLANG['TXT_CORE_CONFIG_COREPAGINGLIMIT'] = 'Nombre de résultats par page';
$_ARRAYLANG['TXT_CORE_CONFIG_COREPAGINGLIMIT_TOOLTIP_HELP'] = 'Valeurs valides: entre 1 et 1\'000';
$_ARRAYLANG['TXT_CORE_CONFIG_SEARCHDESCRIPTIONLENGTH'] = 'Nombre de caractères affichés dans les résultats de la recherche';
$_ARRAYLANG['TXT_CORE_CONFIG_SEARCHDESCRIPTIONLENGTH_TOOLTIP_HELP'] = 'Nombre de caractères pour la description des résultats de la recherche.';
$_ARRAYLANG['TXT_CORE_CONFIG_SESSIONLIFETIME'] = 'Durée de la session';
$_ARRAYLANG['TXT_CORE_CONFIG_SESSIONLIFETIME_TOOLTIP'] = 'Période après laquelle un utilisateur est automatiquement déconnecté du système en cas d\'inactivité.';
$_ARRAYLANG['TXT_CORE_CONFIG_SESSIONLIFETIME_TOOLTIP_HELP'] = 'Durée en secondes';
$_ARRAYLANG['TXT_CORE_CONFIG_SESSIONLIFETIMEREMEMBERME_TOOLTIP_HELP'] = 'Durée en secondes';
$_ARRAYLANG['TXT_CORE_CONFIG_DNSSERVER'] = 'Serveur DNS';
$_ARRAYLANG['TXT_CORE_CONFIG_COREADMINNAME'] = 'Nom de l"administrateur';
$_ARRAYLANG['TXT_CORE_CONFIG_COREADMINEMAIL'] = 'E-mail de l"administrateur';
$_ARRAYLANG['TXT_CORE_CONFIG_CONTACTFORMEMAIL'] = 'Adresse E-mail du formulaire de contact (standard)';
$_ARRAYLANG['TXT_CORE_CONFIG_CONTACTFORMEMAIL_TOOLTIP'] = 'Adresses e-mail des destinataires standards pour les nouveaux formulaires de contact.';
$_ARRAYLANG['TXT_CORE_CONFIG_CONTACTFORMEMAIL_TOOLTIP_HELP'] = 'Séparer les différentes adresses par des virgules.';
$_ARRAYLANG['TXT_CORE_CONFIG_SEARCHVISIBLECONTENTONLY'] = 'Rechercher uniquement dans les pages visibles';
$_ARRAYLANG['TXT_CORE_CONFIG_LANGUAGEDETECTION'] = 'Auto-détection de la langue';
$_ARRAYLANG['TXT_CORE_CONFIG_LANGUAGEDETECTION_TOOLTIP'] = 'Ce paramètre permet de choisir automatiquement la langue en fonction de celle utilisée par le Navigateur Internet de l"utilisateur.';
$_ARRAYLANG['TXT_CORE_CONFIG_GOOGLEMAPSAPIKEY_TOOLTIP'] = 'Clé globale Google-Map API pour le domaine principal.';
$_ARRAYLANG['TXT_CORE_CONFIG_GOOGLEMAPSAPIKEY_TOOLTIP_HELP'] = 'Les clés peuvent être obtenues <a href="https://developers.google.com/maps/documentation/embed/get-started?hl=fr#api_key">ici</a>.';
$_ARRAYLANG['TXT_CORE_CONFIG_GOOGLEMAPSAPIKEY'] = 'Clé globale Google-Map API';
$_ARRAYLANG['TXT_CORE_CONFIG_FRONTENDEDITINGSTATUS'] = 'Edition au Frontend';
$_ARRAYLANG['TXT_CORE_CONFIG_FRONTENDEDITINGSTATUS_TOOLTIP'] = 'Avec l"édition au Frontend, vous pouvez modifier votre page sans devoir vous connecter au Backend.';
$_ARRAYLANG['TXT_CORE_CONFIG_CORELISTPROTECTEDPAGES'] = 'Inclure pages à accès limité';
$_ARRAYLANG['TXT_CORE_CONFIG_CORELISTPROTECTEDPAGES_TOOLTIP'] = 'Inclus les pages à accès limités dans la Navigation, la recherche plein texte et le plan du site (Sitemap et XML-Sitemap) pour les utilisateurs non authentifiés.';
$_ARRAYLANG['TXT_CORE_CONFIG_DEFAULTMETAIMAGE'] = 'Image meta par défaut';
$_ARRAYLANG['TXT_SETTINGS_UPDATED'] = 'Paramètres mis à jour.';
$_ARRAYLANG['TXT_SETTINGS_ERROR_WRITABLE'] = 'n"a pas pu être mis à jour. Vérifiez les privilèges de fichier (666).';
$_ARRAYLANG['TXT_SETTINGS_DEFAULT_SMTP_CHANGED'] = 'Le compte SMTP %s sera désormais utilisé comme compte par défaut pour l"envoi d"E-mails.';
$_ARRAYLANG['TXT_SETTINGS_CHANGE_DEFAULT_SMTP_FAILED'] = 'Impossible de modifier le compte standard!';
$_ARRAYLANG['TXT_SETTINGS_SMTP_DELETE_SUCCEED'] = 'Compte SMTP %s supprimé.';
$_ARRAYLANG['TXT_SETTINGS_SMTP_DELETE_FAILED'] = 'Erreur lors de la suppression du compte SMTP %s!';
$_ARRAYLANG['TXT_SETTINGS_COULD_NOT_DELETE_DEAULT_SMTP'] = 'Impossible de supprimer le compte SMTP %s, car il est défini comme compte par défaut!';
$_ARRAYLANG['TXT_SETTINGS_EMAIL_ACCOUNTS'] = 'Comptes E-Mail';
$_ARRAYLANG['TXT_SETTINGS_ACCOUNT'] = 'Compte';
$_ARRAYLANG['TXT_SETTINGS_USERNAME'] = 'Identifiant';
$_ARRAYLANG['TXT_SETTINGS_STANDARD'] = 'Standard';
$_ARRAYLANG['TXT_SETTINGS_FUNCTIONS'] = 'Fonctions';
$_ARRAYLANG['TXT_SETTINGS_ADD_NEW_SMTP_ACCOUNT'] = 'Créer un nouveau compte SMTP';
$_ARRAYLANG['TXT_SETTINGS_CONFIRM_DELETE_ACCOUNT'] = 'Êtes-vous sûr de vouloir supprimer le compte SMTP %s?';
$_ARRAYLANG['TXT_SETTINGS_OPERATION_IRREVERSIBLE'] = 'Cette opération est irréversible!';
$_ARRAYLANG['TXT_SETTINGS_MODFIY'] = 'Editer';
$_ARRAYLANG['TXT_SETTINGS_DELETE'] = 'Supprimer';
$_ARRAYLANG['TXT_SETTINGS_EMPTY_ACCOUNT_NAME_TXT'] = 'Veuillez saisir un nom de compte!';
$_ARRAYLANG['TXT_SETTINGS_NOT_UNIQUE_SMTP_ACCOUNT_NAME'] = 'Veuillez utiliser un autre compte: le compte %s est déjà utilisé';
$_ARRAYLANG['TXT_SETTINGS_EMPTY_SMTP_HOST_TXT'] = 'Veuillez saisir un serveur SMTP!';
$_ARRAYLANG['TXT_SETTINGS_SMTP_ACCOUNT_UPDATE_SUCCEED'] = 'Compte SMTP %s mis à jour.';
$_ARRAYLANG['TXT_SETTINGS_SMTP_ACCOUNT_UPDATE_FAILED'] = 'Erreur lors de la mise à jour du compte SMTP!';
$_ARRAYLANG['TXT_SETTINGS_SMTP_ACCOUNT_ADD_SUCCEED'] = 'Compte SMTP ajouté.';
$_ARRAYLANG['TXT_SETTINGS_SMTP_ACCOUNT_ADD_FAILED'] = 'Erreur lors de l"ajout du nouveau compte SMTP!';
$_ARRAYLANG['TXT_SETTINGS_MODIFY_SMTP_ACCOUNT'] = 'Editer le compte SMTP';
$_ARRAYLANG['TXT_SETTINGS_NAME_OF_ACCOUNT'] = 'Nom du compte';
$_ARRAYLANG['TXT_SETTINGS_SMTP_SERVER'] = 'Serveur SMTP';
$_ARRAYLANG['TXT_SETTINGS_PASSWORD'] = 'Mot de passe';
$_ARRAYLANG['TXT_SETTINGS_SMTP_AUTHENTICATION_TXT'] = 'N"indiquer ni identifiant ni mot de passe si le serveur SMTP n"exige aucune authentification.';
$_ARRAYLANG['TXT_SETTINGS_SMTP_HOSTNAME_TXT'] = 'Le port par défaut pour SMTP est le port 25.<br><br>Pour utiliser SMTP sur SSL (ou TLS), définissez le protocole <code>ssl://</code> (ou <code>tls:// </ code>) devant le nom du serveur de courrier sortant et saisissez dans le champ Port le port correspondant sur lequel le serveur de courrier sortant accepte les connexions SSL.<br><br>Exemple:<br>Hôte: <code>ssl: //mail .example.com</code><br>Port: <code>465</code>';
$_ARRAYLANG['TXT_SETTINGS_BACK'] = 'Retour';
$_ARRAYLANG['TXT_SETTINGS_SAVE'] = 'Enregistrer';
