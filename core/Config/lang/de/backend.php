<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * @copyright   CLOUDREXX CMS - CLOUDREXX AG
 * @author      Cloudrexx Development Team <info@cloudrexx.com>
 * @access      public
 * @package     cloudrexx
 * @subpackage  core_config
 */
global $_ARRAYLANG;
$_ARRAYLANG['TXT_SETTINGS_MENU_SYSTEM'] = 'System';
$_ARRAYLANG['TXT_SETTINGS_MENU_CACHE'] = 'Caching';
$_ARRAYLANG['TXT_EMAIL_SERVER'] = 'E-Mail Server';
$_ARRAYLANG['TXT_SETTINGS_IMAGE'] = 'Bilder';
$_ARRAYLANG['TXT_CORE_WYSIWYG'] = 'WYSIWYG';
$_ARRAYLANG['TXT_LICENSE'] = 'Lizenzverwaltung';
$_ARRAYLANG['TXT_SETTINGS_ERROR_NO_WRITE_ACCESS'] = 'Auf die Datei %s besteht kein Schreibzugriff! Ohne Schreibzugriff auf diese Datei können keine Einstellungen vorgenommen werden.';
$_ARRAYLANG['TXT_SYSTEM_SETTINGS'] = 'Grundeinstellungen';
$_ARRAYLANG['TXT_CONFIG_MODULE_DESCRIPTION'] = 'Grundeinstellungen';
$_ARRAYLANG['TXT_ACTIVATED'] = 'Aktiviert';
$_ARRAYLANG['TXT_DEACTIVATED'] = 'Deaktiviert';
$_ARRAYLANG['TXT_CORE_CONFIG_SITE'] = 'Frontend';
$_ARRAYLANG['TXT_CORE_CONFIG_ADMINISTRATIONAREA'] = 'Backend';
$_ARRAYLANG['TXT_CORE_CONFIG_SECURITY'] = 'Sicherheit';
$_ARRAYLANG['TXT_CORE_CONFIG_CONTACTINFORMATION'] = 'Kontaktinformationen';
$_ARRAYLANG['TXT_SETTINGS_TITLE_DEVELOPMENT'] = 'Entwicklerwerkzeuge';
$_ARRAYLANG['TXT_CORE_CONFIG_OTHERCONFIGURATIONS'] = 'Mehr';
$_ARRAYLANG['TXT_DEBUGGING_STATUS'] = 'Debugging-Modus';
$_ARRAYLANG['TXT_DEBUGGING_FLAGS'] = 'Flags';
$_ARRAYLANG['TXT_SETTINGS_DEBUGGING_FLAG_LOG'] = 'Statusmeldungen / Ereignisse';
$_ARRAYLANG['TXT_SETTINGS_DEBUGGING_FLAG_PHP'] = 'PHP-Fehler';
$_ARRAYLANG['TXT_SETTINGS_DEBUGGING_FLAG_DB'] = 'Datenbank: Alle Abfragen (inkl. Änderungen und Fehler)';
$_ARRAYLANG['TXT_SETTINGS_DEBUGGING_FLAG_DB_TRACE'] = 'Datenbank: Abfrage-Rückverfolgung';
$_ARRAYLANG['TXT_SETTINGS_DEBUGGING_FLAG_DB_CHANGE'] = 'Datenbank: Änderungen (INSERT/UPDATE/DELETE)';
$_ARRAYLANG['TXT_SETTINGS_DEBUGGING_FLAG_DB_ERROR'] = 'Datenbank: Fehler';
$_ARRAYLANG['TXT_SETTINGS_DEBUGGING_FLAG_LOG_FILE'] = 'Ausgabe in Datei';
$_ARRAYLANG['TXT_DEBUGGING_EXPLANATION'] = 'Modus zur Fehlersuche für Cloudrexx-Entwickler. Wird nur für den momentan eingeloggten Benutzer aktiviert. Verfällt mit der Sitzung.';
$_ARRAYLANG['TXT_SAVE'] = 'Speichern';
$_ARRAYLANG['TXT_CORE_CONFIG_SYSTEMSTATUS'] = 'Seitenstatus';
$_ARRAYLANG['TXT_CORE_CONFIG_SYSTEMSTATUS_TOOLTIP'] = 'Ist die Seite online?';
$_ARRAYLANG['TXT_CORE_CONFIG_COREIDSSTATUS'] = 'Benachrichtigungen vom Sicherheitssystem';
$_ARRAYLANG['TXT_CORE_CONFIG_COREIDSSTATUS_TOOLTIP'] = 'Cloudrexx Intrusion Detection System - Sie erhalten Benachrichtigungen per E-Mail, wenn Angriffe abgewehrt wurden.';
$_ARRAYLANG['TXT_CORE_CONFIG_XMLSITEMAPSTATUS'] = 'XML-Sitemap';
$_ARRAYLANG['TXT_CORE_CONFIG_XMLSITEMAPSTATUS_TOOLTIP'] = 'Automatische Erzeugung einer XML-Sitemap im Hauptverzeichnis der Website unter <i>/sitemap.xml</i>. Die XML-Sitemap ist ein wichtiges Instrument der Suchmaschinenoptimierung (SEO) und verbessert die Auffindbarkeit und Indexierung von Websites in den Suchergebnissen.';
$_ARRAYLANG['TXT_CORE_CONFIG_COREGLOBALPAGETITLE'] = 'Globaler Seitentitel';
$_ARRAYLANG['TXT_CORE_CONFIG_COREGLOBALPAGETITLE_TOOLTIP'] = 'Der globale Seitentitel ist der Titel, der auf jeder Seite der Website angezeigt wird. Dieser Titel erscheint im Browser-Tab und kann von Suchmaschinen als Kurzbeschreibung für die Website verwendet werden. Der globale Seitentitel bietet eine prägnante Beschreibung des Website-Inhalts und kann auch den Markennamen der Website enthalten. Die Gestaltung des globalen Seitentitels ist ein wichtiger Aspekt der Suchmaschinenoptimierung (SEO) und kann die Sichtbarkeit der Website in den Suchergebnissen verbessern. Dieser Titel kann in Ihrem Theme mittels Platzhalter <code>[[GLOBAL_TITLE]]</code> eingebaut werden.';
$_ARRAYLANG['TXT_SETTINGS_DOMAIN_URL'] = 'Domain Ihrer Homepage';
$_ARRAYLANG['TXT_SETTINGS_DOMAIN_URL_HELP'] = 'Geben Sie hier die URL Ihrer Website an. Achten Sie darauf, dass am Ende der URL kein Slash (/) steht!';
$_ARRAYLANG['TXT_CORE_CONFIG_MAINDOMAINID'] = 'Hauptdomain';
$_ARRAYLANG['TXT_CORE_CONFIG_COREPAGINGLIMIT'] = 'Datensätze pro Seite';
$_ARRAYLANG['TXT_CORE_CONFIG_COREPAGINGLIMIT_TOOLTIP_HELP'] = 'Zwischen 1 und 1\'000 möglich.';
$_ARRAYLANG['TXT_CORE_CONFIG_CORESIMPLESELECTLIMIT'] = 'Anzahl der Datensätze im einfachen Auswahlmenü';
$_ARRAYLANG['TXT_CORE_CONFIG_CORESIMPLESELECTLIMIT_TOOLTIP'] = 'Anzahl Datensätze, die im einfachen Auswahlmenü dargestellt werden können. Wird diese Anzahl überschritten, wird das einfache Auswahlmenü durch zwei Auswahlmenüs ersetzt, um die Übersichtlichkeit zu verbessern.';
$_ARRAYLANG['TXT_CORE_CONFIG_SEARCHDESCRIPTIONLENGTH'] = 'Angezeigte Zeichen bei Suchergebnissen';
$_ARRAYLANG['TXT_CORE_CONFIG_SEARCHDESCRIPTIONLENGTH_TOOLTIP_HELP'] = 'Anzahl der Zeichen für die Beschreibung der Suchergebnissen.';
$_ARRAYLANG['TXT_CORE_CONFIG_SESSIONLIFETIME'] = 'Session Dauer';
$_ARRAYLANG['TXT_CORE_CONFIG_SESSIONLIFETIME_TOOLTIP'] = 'Zeitspanne, nach der ein Benutzer ohne Interaktion automatisch vom System abgemeldet wird.';
$_ARRAYLANG['TXT_CORE_CONFIG_SESSIONLIFETIME_TOOLTIP_HELP'] = 'Zeitspanne in Sekunden';
$_ARRAYLANG['TXT_CORE_CONFIG_SESSIONLIFETIMEREMEMBERME'] = 'Session Dauer (Angemeldet bleiben)';
$_ARRAYLANG['TXT_CORE_CONFIG_SESSIONLIFETIMEREMEMBERME_TOOLTIP'] = 'Session-Zeitspanne für Benutzer ohne Interaktion, die beim Login die Checkbox "Merken" gesetzt haben.';
$_ARRAYLANG['TXT_CORE_CONFIG_SESSIONLIFETIMEREMEMBERME_TOOLTIP_HELP'] = 'Zeitspanne in Sekunden';
$_ARRAYLANG['TXT_CORE_CONFIG_DEFAULTLANGUAGEID'] = 'Standardsprache';
$_ARRAYLANG['TXT_CORE_CONFIG_DEFAULTLOCALEID'] = 'Standard Sprachversion';
$_ARRAYLANG['TXT_CORE_CONFIG_DNSSERVER'] = 'Nameserver';
$_ARRAYLANG['TXT_CORE_CONFIG_DNSSERVER_TOOLTIP_HELP'] = 'Definieren Sie einen Nameserver, welcher für DNS-Abfragen genutzt werden soll.';
$_ARRAYLANG['TXT_CORE_CONFIG_COREADMINNAME'] = 'Name des Administrators';
$_ARRAYLANG['TXT_CORE_CONFIG_COREADMINEMAIL'] = 'E-Mail-Adresse des Administrators';
$_ARRAYLANG['TXT_CORE_CONFIG_CONTACTFORMEMAIL'] = 'Standard Empfänger-E-Mail-Adresse für Kontaktformulare';
$_ARRAYLANG['TXT_CORE_CONFIG_CONTACTFORMEMAIL_TOOLTIP'] = 'Standard Empfänger-E-Mail-Adressen für neue Kontaktformulare.';
$_ARRAYLANG['TXT_CORE_CONFIG_CONTACTFORMEMAIL_TOOLTIP_HELP'] = 'Mehrere Adressen können kommasepariert erfasst werden.';
$_ARRAYLANG['TXT_CORE_CONFIG_CONTACTCOMPANY'] = 'Firma';
$_ARRAYLANG['TXT_CORE_CONFIG_CONTACTADDRESS'] = 'Adresse';
$_ARRAYLANG['TXT_CORE_CONFIG_CONTACTZIP'] = 'PLZ';
$_ARRAYLANG['TXT_CORE_CONFIG_CONTACTPLACE'] = 'Ort';
$_ARRAYLANG['TXT_CORE_CONFIG_CONTACTCOUNTRY'] = 'Land';
$_ARRAYLANG['TXT_CORE_CONFIG_CONTACTPHONE'] = 'Telefon';
$_ARRAYLANG['TXT_CORE_CONFIG_CONTACTFAX'] = 'Fax';
$_ARRAYLANG['TXT_CORE_CONFIG_SEARCHVISIBLECONTENTONLY'] = 'Nur in sichtbaren Inhaltsseiten suchen';
$_ARRAYLANG['TXT_CORE_CONFIG_LANGUAGEDETECTION'] = 'Sprache automatisch erkennen';
$_ARRAYLANG['TXT_CORE_CONFIG_LANGUAGEDETECTION_TOOLTIP'] = 'Diese Einstellung bewirkt, dass automatisch die Standardsprache des Browsers ausgelesen und als Sprache verwendet wird.';
$_ARRAYLANG['TXT_CORE_CONFIG_GOOGLEMAPSAPIKEY_TOOLTIP'] = 'Globaler Google Maps API-Schlüssel.<br>Im Theme verfügbar über den Platzhalter <code>[[GOOGLE_MAPS_API_KEY]]</code>.';
$_ARRAYLANG['TXT_CORE_CONFIG_GOOGLEMAPSAPIKEY_TOOLTIP_HELP'] = 'Ein API-Schlüssel kann <a href="https://developers.google.com/maps/documentation/embed/guide#api-schlussel" target="_blank">hier</a> beantragt werden.';
$_ARRAYLANG['TXT_CORE_CONFIG_GOOGLEMAPSAPIKEY'] = 'Google Maps API-Schlüssel';
$_ARRAYLANG['TXT_CORE_CONFIG_FRONTENDEDITINGSTATUS'] = 'Frontend-Editing';
$_ARRAYLANG['TXT_CORE_CONFIG_FRONTENDEDITINGSTATUS_TOOLTIP'] = 'Mithilfe des Frontend-Editing bearbeiten Sie die Inhalte direkt im Frontend der Website. Als angemeldeter Benutzer wird im oberen Bereich der Website <i>Toolbar einblenden</i> angezeigt.';
$_ARRAYLANG['TXT_CORE_CONFIG_USECUSTOMIZINGS'] = 'Individuelle Kundenanpassungen';
$_ARRAYLANG['TXT_CORE_CONFIG_USECUSTOMIZINGS_TOOLTIP'] = 'Verwenden Sie diese Option, um individuelle Kundenanpassungen, welche sich im Ordner %1 befinden, zu aktivieren.';
$_ARRAYLANG['TXT_CORE_CONFIG_CORELISTPROTECTEDPAGES'] = 'Geschützte Seiten auflisten';
$_ARRAYLANG['TXT_CORE_CONFIG_CORELISTPROTECTEDPAGES_TOOLTIP'] = 'Dies definiert, ob bei der Navigation, Volltextsuche, Sitemap und XML-Sitemap auch geschützte Seiten aufgelistet werden sollen, wenn der Benutzer nicht authentifiziert ist.';
$_ARRAYLANG['TXT_CORE_CONFIG_TIMEZONE'] = 'Zeitzone';
$_ARRAYLANG['TXT_CORE_CONFIG_DASHBOARDNEWS'] = 'Dashboard News';
$_ARRAYLANG['TXT_CORE_CONFIG_DASHBOARDSTATISTICS'] = 'Dashboard Statistiken';
$_ARRAYLANG['TXT_CORE_CONFIG_GOOGLEANALYTICSTRACKINGID'] = 'Google Analytics 4 Property';
$_ARRAYLANG['TXT_CORE_CONFIG_GOOGLEANALYTICSTRACKINGID_TOOLTIP_HELP'] = 'Geben Sie hier Ihr <code>GA4</code>-Property ein. Diese finden Sie in Ihrem Google Analytics Konto unter Admin.';
$_ARRAYLANG['TXT_CORE_CONFIG_DEFAULTMETAIMAGE'] = 'Standard Metabild';
$_ARRAYLANG['TXT_CORE_CONFIG_DNSHOSTNAMELOOKUP'] = 'DNS Hostname Auflösung';
$_ARRAYLANG['TXT_CORE_CONFIG_DNSHOSTNAMELOOKUP_TOOLTIP'] = 'Aktivieren Sie diese Option, um die Auflösung der Hostnamen basierend auf der IP-Adresse der Website-Besucher zu aktivieren. Dadurch kann die Ladegeschwindigkeit der Website abnehmen.';
$_ARRAYLANG['TXT_CORE_CONFIG_PASSWORDCOMPLEXITY'] = 'Komplexes Passwort erzwingen';
$_ARRAYLANG['TXT_CORE_CONFIG_PASSWORDCOMPLEXITY_TOOLTIP'] = 'Das Passwort muss die folgenden Zeichen enthalten: Grossbuchstabe, Kleinbuchstabe und Zahl.';
$_ARRAYLANG['TXT_CORE_CONFIG_CAPTCHAMETHOD'] = 'CAPTCHA Methode';
$_ARRAYLANG['TXT_CORE_CONFIG_RECAPTCHASITEKEY'] = 'Öffentlicher Schlüssel (site key) für reCAPTCHA v2';
$_ARRAYLANG['TXT_CORE_CONFIG_RECAPTCHASITEKEY_TOOLTIP_HELP'] = 'Hinterlegen Sie hier Ihren <a href="https://www.google.com/recaptcha/admin/create" target="_blank">persönlich beantragten reCAPTCHA v2 site key</a>.';
$_ARRAYLANG['TXT_CORE_CONFIG_RECAPTCHASECRETKEY'] = 'Privater Schlüssel (secret key) für reCAPTCHA v2';
$_ARRAYLANG['TXT_CORE_CONFIG_RECAPTCHASECRETKEY_TOOLTIP_HELP'] = 'Hinterlegen Sie hier Ihren <a href="https://www.google.com/recaptcha/admin/create" target="_blank">persönlich beantragten reCAPTCHA v2 secret key</a>.';
$_ARRAYLANG['TXT_CORE_CONFIG_HCAPTCHASITEKEY'] = 'Öffentlicher Schlüssel (site key) für hCaptcha';
$_ARRAYLANG['TXT_CORE_CONFIG_HCAPTCHASITEKEY_TOOLTIP_HELP'] = 'Hinterlegen Sie hier Ihren <a href="https://dashboard.hcaptcha.com/signup" target="_blank">persönlich beantragten hCaptcha site key</a>.';
$_ARRAYLANG['TXT_CORE_CONFIG_HCAPTCHASECRETKEY'] = 'Privater Schlüssel (secret key) für hCaptcha';
$_ARRAYLANG['TXT_CORE_CONFIG_HCAPTCHASECRETKEY_TOOLTIP_HELP'] = 'Hinterlegen Sie hier Ihren <a href="https://dashboard.hcaptcha.com/signup" target="_blank">persönlich beantragten hCaptcha secret key</a>.';
$_ARRAYLANG['TXT_CORE_CONFIG_CONTREXX_CAPTCHA_LABEL'] = 'Cloudrexx';
$_ARRAYLANG['TXT_CORE_CONFIG_RECAPTCHA_LABEL'] = 'reCAPTCHA v2';
$_ARRAYLANG['TXT_CORE_CONFIG_HCAPTCHA_LABEL'] = 'hCaptcha';
$_ARRAYLANG['TXT_CORE_CONFIG_CAPTCHAMETHOD_TOOLTIP'] = 'Wählen Sie den CAPTCHA-Mechanismus aus, welcher zur Abwehr von Spam verwendet werden soll:<br><br><strong>Cloudrexx</strong><br>Lokaler Service. Geringer Schutz gegen Spam.<br><br><strong>hCaptcha®</strong><br>hCaptcha ist ein externer Service bereitgestellt von Intuition Machines, Inc. und bietet guten Schutz gegen Spam unter Wahrung der Privatsphäre der Besucher. <a href="https://www.hcaptcha.com/" target="_blank">Mehr Informationen</a>.<br><br><strong>reCAPTCHA v2</strong><br>reCAPTCHA v2 ist ein externer Service bereitgestellt von Google Inc. und bietet guten Schutz gegen Spam. Für die Nutzung wird ein Google-Konto benötigt. <a href="https://www.google.com/recaptcha/intro/index.html" target="_blank">Mehr Informationen</a>.';
$_ARRAYLANG['TXT_CORE_CONFIG_ALLOWCLIENTSIDESCRIPTUPLOAD'] = 'Hochladen von clientseitigen Skripten erlauben';
$_ARRAYLANG['TXT_CORE_CONFIG_ALLOWCLIENTSIDESCRIPTUPLOAD_TOOLTIP'] = 'Das Hochladen von clientseitigen Skripten (xhtml, xml, svg, shtml) stellt ein potentielles Sicherheitsrisiko dar, da Angreifer mit einem clientseitigen Skript eine Benutzersitzung und somit die Kontrolle der Website einnehmen können.';
$_ARRAYLANG['TXT_CORE_CONFIG_ALLOWCLIENTSIDESCRIPTUPLOADONGROUPS'] = 'Erlaubte Benutzergruppen zum Hochladen von clientseitigen Skripten';
$_ARRAYLANG['TXT_CORE_CONFIG_ADVANCEDUPLOADBACKEND'] = 'Erweiterte Dateiuploadmöglickeiten';
$_ARRAYLANG['TXT_CORE_CONFIG_ADVANCEDUPLOADFRONTEND'] = 'Erweiterte Dateiuploadmöglickeiten';
$_ARRAYLANG['TXT_CORE_CONFIG_FORCEPROTOCOLFRONTEND_TOOLTIP'] = 'Cloudrexx erzwingt standardmässig kein Protokoll. Sie können HTTPS erzwingen, um die Suchmaschinen-Bewertung zu verbessern.';
$_ARRAYLANG['TXT_CORE_CONFIG_FORCEPROTOCOLBACKEND_TOOLTIP'] = 'Cloudrexx erzwingt standardmässig kein Protokoll. Sie können HTTPS erzwingen, um die Sicherheit zu erhöhen.';
$_ARRAYLANG['TXT_CORE_CONFIG_FORCEPROTOCOLBACKEND'] = 'Verwendetes Protokoll';
$_ARRAYLANG['TXT_CORE_CONFIG_FORCEPROTOCOLFRONTEND'] = 'Verwendetes Protokoll';
$_ARRAYLANG['TXT_CORE_CONFIG_NOBODY_LABEL'] = 'Niemand';
$_ARRAYLANG['TXT_CORE_CONFIG_GROUPS_LABEL'] = 'Ausgewählte Benutzergruppen';
$_ARRAYLANG['TXT_CORE_CONFIG_ALL_LABEL'] = 'Jeder';
$_ARRAYLANG['TXT_SETTINGS_FORCE_PROTOCOL_NONE'] = 'dynamisch';
$_ARRAYLANG['TXT_SETTINGS_FORCE_PROTOCOL_HTTP'] = 'HTTP';
$_ARRAYLANG['TXT_SETTINGS_FORCE_PROTOCOL_HTTPS'] = 'HTTPS';
$_ARRAYLANG['TXT_CORE_CONFIG_FORCEDOMAINURL_TOOLTIP'] = 'Durch Aktivierung dieser Option wird die Website ausschliesslich über die gewählte Hauptdomain bereitgestellt. Das Erzwingen einer Hauptdomain trägt zur Sichtbarkeit in Suchmaschinen, zur Vermeidung von SEO-Problemen und zur Verbesserung des Brandings bei. Die Website ist dabei über Domain-Aliase weiterhin erreichbar.';
$_ARRAYLANG['TXT_CORE_CONFIG_FORCEDOMAINURL'] = 'Hauptdomain erzwingen';
$_ARRAYLANG['TXT_CORE_TIMEZONE_INVALID'] = 'Die gewählte Zeitzone ist ungültig.';
$_ARRAYLANG['TXT_SETTINGS_UPDATED'] = 'Die Einstellungen wurden gespeichert.';
$_ARRAYLANG['TXT_SETTINGS_ERROR_WRITABLE'] = 'konnte nicht geschrieben werden. Überprüfen Sie die Berechtigungen (666) der Datei.';
$_ARRAYLANG['TXT_SETTINGS_DEFAULT_SMTP_CHANGED'] = 'Das SMTP-Konto %s wird nun als Standardkonto für das Versenden von E-Mails verwendet.';
$_ARRAYLANG['TXT_SETTINGS_CHANGE_DEFAULT_SMTP_FAILED'] = 'Das Wechseln des Standard-Kontos schlug fehl!';
$_ARRAYLANG['TXT_SETTINGS_SMTP_DELETE_SUCCEED'] = 'Das SMTP-Konto %s wurde erfolgreich gelöscht.';
$_ARRAYLANG['TXT_SETTINGS_SMTP_DELETE_FAILED'] = 'Beim Löschen des SMTP-Kontos %s trat ein Fehler auf!';
$_ARRAYLANG['TXT_SETTINGS_COULD_NOT_DELETE_DEAULT_SMTP'] = 'Das SMTP-Konto %s kann nicht gelöscht werden, da es das Standardkonto ist!';
$_ARRAYLANG['TXT_SETTINGS_EMAIL_ACCOUNTS'] = 'E-Mail-Konten';
$_ARRAYLANG['TXT_SETTINGS_ACCOUNT'] = 'Name';
$_ARRAYLANG['TXT_SETTINGS_HOST'] = 'Host';
$_ARRAYLANG['TXT_SETTINGS_USERNAME'] = 'Benutzername';
$_ARRAYLANG['TXT_SETTINGS_STANDARD'] = 'Standard';
$_ARRAYLANG['TXT_SETTINGS_FUNCTIONS'] = 'Funktionen';
$_ARRAYLANG['TXT_SETTINGS_ADD_NEW_SMTP_ACCOUNT'] = 'E-Mail Server hinzufügen';
$_ARRAYLANG['TXT_SETTINGS_CONFIRM_DELETE_ACCOUNT'] = 'Möchten Sie das SMTP-Konto %s wirklich löschen?';
$_ARRAYLANG['TXT_SETTINGS_OPERATION_IRREVERSIBLE'] = 'Dieser Vorgang kann nicht rückgängig gemacht werden!';
$_ARRAYLANG['TXT_SETTINGS_MODFIY'] = 'Bearbeiten';
$_ARRAYLANG['TXT_SETTINGS_DELETE'] = 'Löschen';
$_ARRAYLANG['TXT_SETTINGS_EMPTY_ACCOUNT_NAME_TXT'] = 'Sie müssen einen Kontonamen definieren!';
$_ARRAYLANG['TXT_SETTINGS_NOT_UNIQUE_SMTP_ACCOUNT_NAME'] = 'Sie müssen einen anderen Kontonamen verwenden, da der Kontoname %s bereits verwendet wird!';
$_ARRAYLANG['TXT_SETTINGS_EMPTY_SMTP_HOST_TXT'] = 'Sie müssen einen SMTP-Server angeben!';
$_ARRAYLANG['TXT_SETTINGS_SMTP_ACCOUNT_UPDATE_SUCCEED'] = 'Das SMTP-Konto %s wurde erfolgreich aktualisiert.';
$_ARRAYLANG['TXT_SETTINGS_SMTP_ACCOUNT_UPDATE_FAILED'] = 'Beim Aktualisieren des SMTP-Kontos %s trat ein Fehler auf!';
$_ARRAYLANG['TXT_SETTINGS_SMTP_ACCOUNT_ADD_SUCCEED'] = 'Das Konto %s wurde erfolgreich hinzugefügt.';
$_ARRAYLANG['TXT_SETTINGS_SMTP_ACCOUNT_ADD_FAILED'] = 'Beim Hinzufügen des neuen SMTP-Kontos trat ein Fehler auf!';
$_ARRAYLANG['TXT_SETTINGS_MODIFY_SMTP_ACCOUNT'] = 'E-Mail Server bearbeiten';
$_ARRAYLANG['TXT_SETTINGS_NAME_OF_ACCOUNT'] = 'Name des Kontos';
$_ARRAYLANG['TXT_SETTINGS_SMTP_SERVER'] = 'SMTP-Server';
$_ARRAYLANG['TXT_SETTINGS_PORT'] = 'Port';
$_ARRAYLANG['TXT_SETTINGS_AUTHENTICATION'] = 'Authentifikation';
$_ARRAYLANG['TXT_SETTINGS_PASSWORD'] = 'Passwort';
$_ARRAYLANG['TXT_SETTINGS_SMTP_AUTHENTICATION_TXT'] = 'Geben Sie hier keinen Benutzernamen und Passwort an, wenn der angegebene SMTP-Server keine Authentifikation erfordert.';
$_ARRAYLANG['TXT_SETTINGS_SMTP_HOSTNAME_TXT'] = 'Der Standard-Port für SMTP ist Port 25.<br><br>Zur Verwendung von SMTP over SSL (oder TLS) setzen Sie das Protokoll <code>ssl://</code> (oder <code>tls://</code>) vor den Namen des Postausgangsservers und tragen den entsprechenden Port, über welcher der Postausgangsserver SSL Verbindungen annimmt, ins Feld Port ein.<br><br>Beispiel:<br>Host: <code>ssl://mail.beispiel.com</code><br>Port: <code>465</code>';
$_ARRAYLANG['TXT_SETTINGS_BACK'] = 'Abbrechen';
$_ARRAYLANG['TXT_SETTINGS_SAVE'] = 'Speichern';
$_ARRAYLANG['TXT_SETTINGS_IMAGE_TITLE'] = 'Bildeinstellungen';
$_ARRAYLANG['TXT_SETTINGS_IMAGE_CUT_WIDTH'] = 'Standardbreite für die Bildzuschneidung';
$_ARRAYLANG['TXT_SETTINGS_IMAGE_CUT_HEIGHT'] = 'Standardhöhe für die Bildzuschneidung';
$_ARRAYLANG['TXT_SETTINGS_IMAGE_SCALE_WIDTH'] = 'Standardbreite für die Skalierung';
$_ARRAYLANG['TXT_SETTINGS_IMAGE_SCALE_HEIGHT'] = 'Standardhöhe für die Skalierung';
$_ARRAYLANG['TXT_SETTINGS_IMAGE_COMPRESSION'] = 'Standardwert für die Kompression';
$_ARRAYLANG['TXT_IMAGE_THUMBNAILS'] = 'Vorschaubilder';
$_ARRAYLANG['TXT_IMAGE_THUMBNAILS_TYPE_1'] = 'Höhe';
$_ARRAYLANG['TXT_IMAGE_THUMBNAILS_TYPE_0'] = 'Breite';
$_ARRAYLANG['TXT_IMAGE_THUMBNAILS_ID'] = 'ID';
$_ARRAYLANG['TXT_IMAGE_THUMBNAILS_NAME'] = 'Name';
$_ARRAYLANG['TXT_IMAGE_THUMBNAILS_SIZE'] = 'Grösse';
$_ARRAYLANG['TXT_IMAGE_THUMBNAILS_MAXIMUM'] = 'Maximal %s';
$_ARRAYLANG['TXT_IMAGE_THUMBNAILS_MAX_SIZE'] = 'Maximale Grösse';
$_ARRAYLANG['TXT_IMAGE_THUMBNAILS_DELETE'] = 'Wirklich löschen?';
$_ARRAYLANG['TXT_IMAGE_THUMBNAILS_NEW'] = 'Neue Vorschaubilder Grösse';
$_ARRAYLANG['TXT_IMAGE_THUMBNAILS_RELOAD'] = 'Alle Vorschaubilder neu generieren';
$_ARRAYLANG['TXT_IMAGE_THUMBNAILS_FILE'] = 'Datei';
$_ARRAYLANG['TXT_IMAGE_THUMBNAILS_THUMBNAILS_GENERATED'] = 'Für %s Dateien Vorschaubilder generiert';
$_ARRAYLANG['TXT_IMAGE_THUMBNAILS_NO_FILES_WITHOUT_THUMBNAILS_FOUND'] = 'Keine Dateien ohne Vorschaubilder gefunden.';
$_ARRAYLANG['TXT_CORE_CONFIG_PORTFRONTENDHTTP'] = 'HTTP Port';
$_ARRAYLANG['TXT_CORE_CONFIG_PORTFRONTENDHTTPS'] = 'HTTPS Port';
$_ARRAYLANG['TXT_CORE_CONFIG_PORTBACKENDHTTP'] = 'HTTP Port';
$_ARRAYLANG['TXT_CORE_CONFIG_PORTBACKENDHTTPS'] = 'HTTPS Port';
$_ARRAYLANG['TXT_CORE_CONFIG_PDF'] = 'PDF Vorlagen';
$_ARRAYLANG['TXT_CONFIG_UNABLE_TO_SET_MAINDOMAIN'] = 'Hauptdomain kann nicht auf %s umgestellt werden, da die Website über diese Domain nicht erreichbar ist. Stellen Sie sicher, dass die DNS-Konfiguration korrekt ist.';
$_ARRAYLANG['TXT_CONFIG_UNABLE_TO_SET_PROTOCOL'] = 'Das Protokoll kann nicht umgestellt werden, da die Website über %s nicht erreichbar ist.';
$_ARRAYLANG['TXT_CONFIG_UNABLE_TO_FORCE_MAINDOMAIN'] = 'Die Hauptdomain kann nicht erzwungen werden, da die Website über diese Domain nicht erreichbar ist.';
$_ARRAYLANG['TXT_CORE_CONFIG_USEVIRTUALLANGUAGEDIRECTORIES'] = 'Virtuelle Sprachverzeichnisse verwenden';
$_ARRAYLANG['TXT_CONFIG_UNABLE_TO_SET_USEVIRTUALLANGUAGEDIRECTORIES'] = 'Die Option %s kann nicht deaktiviert werden, so lange mehrere Sprachversionen eingerichtet sind.';
$_ARRAYLANG['TXT_CORE_CONFIG_UPLOADFILESIZELIMIT'] = 'Datei-Upload-Limite';
$_ARRAYLANG['TXT_CORE_CONFIG_UPLOADFILESIZELIMIT_TOOLTIP'] = 'Die maximal erlaubte Grösse einer Datei, welche hochgeladen werden darf.';
$_ARRAYLANG['TXT_CORE_CONFIG_SESSIONBINDING'] = 'Session-Bindung';
$_ARRAYLANG['TXT_CORE_CONFIG_SESSIONBINDING_TOOLTIP'] = 'Die Sicherheit des Systems wird erhöht, indem die Benutzer-Sitzung an den Browser des Benutzers gebunden wird.';
$_ARRAYLANG['TXT_CORE_CONFIG_FRONTENDDENYIFRAMING'] = 'Fremd-Einbindung blockieren';
$_ARRAYLANG['TXT_CORE_CONFIG_FRONTENDDENYIFRAMING_TOOLTIP'] = 'Aktivieren Sie diese Option, um die Einbindung der Website (Frontend) durch Dritte (z.B. via iframe) zu blockieren.<br>Bei aktivierter Option wird die Einbindung via <code>Content-Security-Policy: frame-ancestors</code> HTTP-Header unterbunden.<br><br><i class="fa-solid fa-triangle-exclamation"></i> Die Fremd-Einbindung stellt ein Sicherheitsrisiko dar, da es einem Angreifer mittels Clickjacking ermöglicht, unerlaubten Zugriff zum System zu erlangen.<br><br><i class="fa-solid fa-circle-exclamation"></i> Die Fremd-Einbindung des Backends ist immer blockiert, auch dann wenn diese Option deaktiviert wird.';
$_ARRAYLANG['TXT_CORE_CONFIG_BLOCKMIMESNIFFING'] = 'MIME-Sniffing blockieren';
$_ARRAYLANG['TXT_CORE_CONFIG_BLOCKMIMESNIFFING_TOOLTIP'] = 'Aktivieren Sie diese Option, um MIME-Sniffing zu blockieren.<br>Bei aktivierter Option wird MIME-Sniffing via <code>X-Content-Type-Options: nosniff</code> HTTP-Header blockiert.<br><br><i class="fa-solid fa-triangle-exclamation"></i> Die Deaktivierung dieser Option stellt ein Sicherheitsrisiko dar, da es einem Angreifer die Ausführung bösartigem Code ermöglicht.';
$_ARRAYLANG['TXT_CORE_CONFIG_HSTSINCLUDESUBDOMAINS'] = 'Subdomains schützen';
$_ARRAYLANG['TXT_CORE_CONFIG_HSTSINCLUDESUBDOMAINS_TOOLTIP'] = 'Aktivieren Sie diese Option, um verbesserte Sicherheitsfunktionen auf alle Teile dieser Webseite, einschliesslich aller ihrer Subdomains, auszuweiten. Dies gewährleistet, dass Ihr gesamtes Seiten-Erlebnis sicherer gegen Online-Bedrohungen ist.<br><br><strong>Achtung:</strong> Aktivieren Sie diese Option nur, wenn alle Subdomains über eine gesicherte Verbindung (HTTPS) erreichbar sind. Der Zugriff auf Subdomains ohne eine gesicherte Verbindung ist danach nicht mehr möglich.<br><br>Damit wird die <a href="https://de.wikipedia.org/wiki/HTTP_Strict_Transport_Security" target="_blank">HSTS</a> Direktive <code>includeSubDomains</code> aktiviert.';
$_ARRAYLANG['TXT_CORE_CONFIG_HSTSPRELOAD'] = 'HTTPS auch für Erstbesucher';
$_ARRAYLANG['TXT_CORE_CONFIG_HSTSPRELOAD_TOOLTIP'] = 'Aktivieren Sie diese Option, um sicherzustellen, dass Verbindungen auf diese Seite immer über HTTPS erfolgen, auch für Erstbesucher.<br><br>Damit wird die <a href="https://de.wikipedia.org/wiki/HTTP_Strict_Transport_Security" target="_blank">HSTS</a> Direktive <code>preload</code> aktiviert.';
$_ARRAYLANG['TXT_CORE_CONFIG_CSPFORSCRIPTSRC'] = 'Nur vertrauenswürdige Ressourcen zulassen';
$_ARRAYLANG['TXT_CORE_CONFIG_CSPFORSCRIPTSRC_TOOLTIP'] = '<strong><i class="fa-solid fa-triangle-exclamation"></i>Dies ist eine experimentelle Funktion! Die korrekte Funktionsweise der Website kann dadurch beeinträchtigt werden.</strong><br><br>Aktivieren Sie diese Option, um die Ausführung von JavaScript Code und Einbindung von Ressourcen (Bilder, Schriften, Stylesheets, etc.) auf vertrauenswürdige Quellen einzuschränken. Dies erhöht die Sicherheit, indem potentiell schädliche Skripte und Ressourcen unbekannter Herkunft blockiert werden und schützt Ihre Daten vor potenziellen Bedrohungen. Dies erfolgt durch das Setzen einer <a href="https://de.wikipedia.org/wiki/Content_Security_Policy" target="_blank">Content Security Policy</a>.<br><br><i class="fa-solid fa-circle-exclamation"></i> Vor Aktivierung dieser Option müssen vertrauenswürdige Ressourcen im Theme registriert werden. <a href="https://cloudrexx.atlassian.net/wiki/spaces/HELP/pages/468386386/Welche+Meta-Informationen+zum+Theme+gibt+es" target="_blank">Mehr Informationen</a>.';
$_ARRAYLANG['TXT_CORE_CONFIG_UPGRADEINSECUREREQUESTS'] = 'Nur HTTPS-Verlinkungen zulassen';
$_ARRAYLANG['TXT_CORE_CONFIG_UPGRADEINSECUREREQUESTS_TOOLTIP'] = 'Aktivieren Sie diese Option, um sicherzustellen, dass alle Verlinkungen HTTPS verwenden.<br><br>Dies erfolgt durch setzen der <code>upgrade-insecure-requests</code> Direktive der <a href="https://de.wikipedia.org/wiki/Content_Security_Policy" target="_blank">Content Security Policy</a>.';
$_ARRAYLANG['TXT_CORE_CONFIG_TRUSTEDREVERSEPROXYCOUNT'] = 'Anzahl der vertrauenswürdigen Reverse-Proxys';
$_ARRAYLANG['TXT_CORE_CONFIG_TRUSTEDREVERSEPROXYCOUNT_TOOLTIP'] = 'Geben Sie die Anzahl der vertrauenswürdigen Reverse-Proxys (z. B. Load Balancer, CDNs) zwischen dem Client und dieser Website an. Diese Einstellung bestimmt, welche IP-Adresse aus dem <code>X-Forwarded-For</code>-Header als Client-IP-Adresse betrachtet wird. Setzen Sie den Wert auf <code>0</code> (Standard), wenn keine Reverse-Proxys verwendet werden.<br><br><i class="fa-solid fa-triangle-exclamation"></i> Das Festlegen dieser Option auf eine falsche Anzahl von Reverse-Proxys kann zur Nichtverfügbarkeit der Website führen.';
$_ARRAYLANG['TXT_CORE_CONFIG_FILE_EXTENSION_DENIED'] = 'Die Datei-Endung <code>%s</code> ist nicht erlaubt.';
$_ARRAYLANG['TXT_CORE_CONFIG_CONFIG_ALLOWCLIENTSIDESCRIPTUPLOAD'] = 'Mit der Konfigurationsoption %s kann dies erlaubt werden.';
