<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * @copyright   CLOUDREXX CMS - CLOUDREXX AG
 * @author      Cloudrexx Development Team <info@cloudrexx.com>
 * @access      public
 * @package     cloudrexx
 * @subpackage  core_config
 */
global $_ARRAYLANG;
$_ARRAYLANG['TXT_SETTINGS_MENU_SYSTEM'] = 'System';
$_ARRAYLANG['TXT_SETTINGS_MENU_CACHE'] = 'Caching';
$_ARRAYLANG['TXT_EMAIL_SERVER'] = 'Email server';
$_ARRAYLANG['TXT_SETTINGS_IMAGE'] = 'Images';
$_ARRAYLANG['TXT_CORE_WYSIWYG'] = 'WYSIWYG';
$_ARRAYLANG['TXT_LICENSE'] = 'License Management';
$_ARRAYLANG['TXT_SETTINGS_ERROR_NO_WRITE_ACCESS'] = 'The file %s is write-protected! No changes to the settings can be made until the write protection on that file has been removed!';
$_ARRAYLANG['TXT_SYSTEM_SETTINGS'] = 'Global configuration';
$_ARRAYLANG['TXT_CONFIG_MODULE_DESCRIPTION'] = 'Global configuration';
$_ARRAYLANG['TXT_ACTIVATED'] = 'Activated';
$_ARRAYLANG['TXT_DEACTIVATED'] = 'Deactivated';
$_ARRAYLANG['TXT_CORE_CONFIG_SITE'] = 'Frontend';
$_ARRAYLANG['TXT_CORE_CONFIG_ADMINISTRATIONAREA'] = 'Backend';
$_ARRAYLANG['TXT_CORE_CONFIG_SECURITY'] = 'Security';
$_ARRAYLANG['TXT_CORE_CONFIG_CONTACTINFORMATION'] = 'Contact Information';
$_ARRAYLANG['TXT_SETTINGS_TITLE_DEVELOPMENT'] = 'Development tools';
$_ARRAYLANG['TXT_CORE_CONFIG_OTHERCONFIGURATIONS'] = 'Other configuration options';
$_ARRAYLANG['TXT_DEBUGGING_STATUS'] = 'Debugging mode';
$_ARRAYLANG['TXT_DEBUGGING_FLAGS'] = 'Flags';
$_ARRAYLANG['TXT_SETTINGS_DEBUGGING_FLAG_LOG'] = 'Messages / Events';
$_ARRAYLANG['TXT_SETTINGS_DEBUGGING_FLAG_PHP'] = 'PHP errors';
$_ARRAYLANG['TXT_SETTINGS_DEBUGGING_FLAG_DB'] = 'Database: All queries (incl. changes und failed queries)';
$_ARRAYLANG['TXT_SETTINGS_DEBUGGING_FLAG_DB_TRACE'] = 'Database: Trace queries';
$_ARRAYLANG['TXT_SETTINGS_DEBUGGING_FLAG_DB_CHANGE'] = 'Database: Changes (INSERT/UPDATE/DELETE)';
$_ARRAYLANG['TXT_SETTINGS_DEBUGGING_FLAG_DB_ERROR'] = 'Database: Failed queries';
$_ARRAYLANG['TXT_SETTINGS_DEBUGGING_FLAG_LOG_FILE'] = 'Output to file';
$_ARRAYLANG['TXT_DEBUGGING_EXPLANATION'] = 'Mode that helps Cloudrexx developers with troubleshooting tasks. Activated for the currently logged in user only. Settings are discarded as soon as the session is closed.';
$_ARRAYLANG['TXT_SAVE'] = 'Save';
$_ARRAYLANG['TXT_CORE_CONFIG_SYSTEMSTATUS'] = 'Page status';
$_ARRAYLANG['TXT_CORE_CONFIG_SYSTEMSTATUS_TOOLTIP'] = 'Is the page online?';
$_ARRAYLANG['TXT_CORE_CONFIG_COREIDSSTATUS'] = 'Security system notifications';
$_ARRAYLANG['TXT_CORE_CONFIG_COREIDSSTATUS_TOOLTIP'] = 'Cloudrexx Intrusion Detection System - Reporting Status';
$_ARRAYLANG['TXT_CORE_CONFIG_XMLSITEMAPSTATUS'] = 'XML Sitemap';
$_ARRAYLANG['TXT_CORE_CONFIG_XMLSITEMAPSTATUS_TOOLTIP'] = 'Automatic generation of an XML sitemap in the root directory of the website under <i>/sitemap.xml</i>. The XML sitemap is an important tool for search engine optimization (SEO) and improves the discoverability and indexing of websites in search results.';
$_ARRAYLANG['TXT_CORE_CONFIG_COREGLOBALPAGETITLE'] = 'Global Page Title';
$_ARRAYLANG['TXT_CORE_CONFIG_COREGLOBALPAGETITLE_TOOLTIP'] = 'Global page title. It can be added to your design with the use of [[GLOBAL_TITLE]].';
$_ARRAYLANG['TXT_SETTINGS_DOMAIN_URL'] = 'URL of homepage';
$_ARRAYLANG['TXT_SETTINGS_DOMAIN_URL_HELP'] = 'URL of your Website. Please make sure that you don"t add a slash at the end of the URL! ( / )';
$_ARRAYLANG['TXT_CORE_CONFIG_MAINDOMAINID'] = 'Main domain';
$_ARRAYLANG['TXT_CORE_CONFIG_COREPAGINGLIMIT'] = 'Records per page';
$_ARRAYLANG['TXT_CORE_CONFIG_COREPAGINGLIMIT_TOOLTIP_HELP'] = 'Values between 1 and 1\'000 allowed.';
$_ARRAYLANG['TXT_CORE_CONFIG_CORESIMPLESELECTLIMIT'] = 'Number of data records in simple select menu';
$_ARRAYLANG['TXT_CORE_CONFIG_CORESIMPLESELECTLIMIT_TOOLTIP'] = 'Number of data records that can be displayed in the simple select menu. If this number is exceeded, the simple selection menu is replaced by two select menus to improve clarity.';
$_ARRAYLANG['TXT_CORE_CONFIG_SEARCHDESCRIPTIONLENGTH'] = 'Number of Characters in Search Results';
$_ARRAYLANG['TXT_CORE_CONFIG_SEARCHDESCRIPTIONLENGTH_TOOLTIP_HELP'] = 'Number of Characters displayed for the Description of the Search Results.';
$_ARRAYLANG['TXT_CORE_CONFIG_SESSIONLIFETIME'] = 'Session length';
$_ARRAYLANG['TXT_CORE_CONFIG_SESSIONLIFETIME_TOOLTIP'] = 'Time period after which a user is automatically logged out of the system due to inactivity.';
$_ARRAYLANG['TXT_CORE_CONFIG_SESSIONLIFETIME_TOOLTIP_HELP'] = 'Time period in seconds';
$_ARRAYLANG['TXT_CORE_CONFIG_SESSIONLIFETIMEREMEMBERME'] = 'Session length (remember me)';
$_ARRAYLANG['TXT_CORE_CONFIG_SESSIONLIFETIMEREMEMBERME_TOOLTIP'] = 'Session duration for users without interaction who have checked the "Remember me" checkbox at login.';
$_ARRAYLANG['TXT_CORE_CONFIG_SESSIONLIFETIMEREMEMBERME_TOOLTIP_HELP'] = 'Time period in seconds';
$_ARRAYLANG['TXT_CORE_CONFIG_DEFAULTLANGUAGEID'] = 'Default Language';
$_ARRAYLANG['TXT_CORE_CONFIG_DEFAULTLOCALEID'] = 'Default Locale';
$_ARRAYLANG['TXT_CORE_CONFIG_DNSSERVER'] = 'DNS Server';
$_ARRAYLANG['TXT_CORE_CONFIG_DNSSERVER_TOOLTIP_HELP'] = 'Set a name server to be used for DNS queries';
$_ARRAYLANG['TXT_CORE_CONFIG_COREADMINNAME'] = 'Administrators Name';
$_ARRAYLANG['TXT_CORE_CONFIG_COREADMINEMAIL'] = 'Email of administrator';
$_ARRAYLANG['TXT_CORE_CONFIG_CONTACTFORMEMAIL'] = 'E-Mail Address for Contact Form (default)';
$_ARRAYLANG['TXT_CORE_CONFIG_CONTACTFORMEMAIL_TOOLTIP'] = 'Default recipient email addresses for new contact forms.';
$_ARRAYLANG['TXT_CORE_CONFIG_CONTACTFORMEMAIL_TOOLTIP_HELP'] = 'Several addresses can be separated by comma.';
$_ARRAYLANG['TXT_CORE_CONFIG_CONTACTCOMPANY'] = 'Company';
$_ARRAYLANG['TXT_CORE_CONFIG_CONTACTADDRESS'] = 'Address';
$_ARRAYLANG['TXT_CORE_CONFIG_CONTACTZIP'] = 'ZIP-Code';
$_ARRAYLANG['TXT_CORE_CONFIG_CONTACTPLACE'] = 'Place';
$_ARRAYLANG['TXT_CORE_CONFIG_CONTACTCOUNTRY'] = 'Country';
$_ARRAYLANG['TXT_CORE_CONFIG_CONTACTPHONE'] = 'Phone';
$_ARRAYLANG['TXT_CORE_CONFIG_CONTACTFAX'] = 'Fax';
$_ARRAYLANG['TXT_CORE_CONFIG_SEARCHVISIBLECONTENTONLY'] = 'Seek only in visible contents';
$_ARRAYLANG['TXT_CORE_CONFIG_LANGUAGEDETECTION'] = 'Auto-Detect Language';
$_ARRAYLANG['TXT_CORE_CONFIG_LANGUAGEDETECTION_TOOLTIP'] = 'This configuration allows for language specific content based on browser setting';
$_ARRAYLANG['TXT_CORE_CONFIG_GOOGLEMAPSAPIKEY_TOOLTIP'] = 'Global Google Maps API key.<br>Available for integration in the webdesign template through placeholder [[GOOGLE_MAPS_API_KEY]].';
$_ARRAYLANG['TXT_CORE_CONFIG_GOOGLEMAPSAPIKEY_TOOLTIP_HELP'] = 'Issue your API key <a href="https://developers.google.com/maps/documentation/embed/guide#api_key" target="_blank">here</a>.';
$_ARRAYLANG['TXT_CORE_CONFIG_GOOGLEMAPSAPIKEY'] = 'Google Maps API key';
$_ARRAYLANG['TXT_CORE_CONFIG_FRONTENDEDITINGSTATUS'] = 'Frontend Editing';
$_ARRAYLANG['TXT_CORE_CONFIG_FRONTENDEDITINGSTATUS_TOOLTIP'] = 'Frontend Editing allows you to edit the content of your page without the need of logging in into your admin-panel.';
$_ARRAYLANG['TXT_CORE_CONFIG_USECUSTOMIZINGS'] = 'Customizing';
$_ARRAYLANG['TXT_CORE_CONFIG_USECUSTOMIZINGS_TOOLTIP'] = 'Use this option to activate customizings found in %1.';
$_ARRAYLANG['TXT_CORE_CONFIG_CORELISTPROTECTEDPAGES'] = 'List protected pages';
$_ARRAYLANG['TXT_CORE_CONFIG_CORELISTPROTECTEDPAGES_TOOLTIP'] = 'Defines if protected pages should be listed/included in the navigation, full text search, sitemap and XML-Sitemap if the user isn"t authenticated.';
$_ARRAYLANG['TXT_CORE_CONFIG_TIMEZONE'] = 'Timezone';
$_ARRAYLANG['TXT_CORE_CONFIG_DASHBOARDNEWS'] = 'Dashboard news';
$_ARRAYLANG['TXT_CORE_CONFIG_DASHBOARDSTATISTICS'] = 'Dashboard statistics';
$_ARRAYLANG['TXT_CORE_CONFIG_GOOGLEANALYTICSTRACKINGID'] = 'Google Analytics Tracking ID';
$_ARRAYLANG['TXT_CORE_CONFIG_GOOGLEANALYTICSTRACKINGID_TOOLTIP_HELP'] = 'Enter your Google Analytics tracking ID here. These can be found in your Google Analytics account under Admin => Tracking Code.';
$_ARRAYLANG['TXT_CORE_CONFIG_DEFAULTMETAIMAGE'] = 'Default meta image';
$_ARRAYLANG['TXT_CORE_CONFIG_DNSHOSTNAMELOOKUP'] = 'DNS Hostname lookup';
$_ARRAYLANG['TXT_CORE_CONFIG_DNSHOSTNAMELOOKUP_TOOLTIP'] = 'Activate to enable the lookup of hostnames of website users based on their IP address. Important: this might slow down the page speed in frontend';
$_ARRAYLANG['TXT_CORE_CONFIG_PASSWORDCOMPLEXITY'] = 'Passwords must meet the complexity requirements';
$_ARRAYLANG['TXT_CORE_CONFIG_PASSWORDCOMPLEXITY_TOOLTIP'] = 'Password must contain the following characters: upper and lower case character and number';
$_ARRAYLANG['TXT_CORE_CONFIG_CAPTCHAMETHOD'] = 'CAPTCHA method';
$_ARRAYLANG['TXT_CORE_CONFIG_RECAPTCHASITEKEY'] = 'Site key for reCAPTCHA v2';
$_ARRAYLANG['TXT_CORE_CONFIG_RECAPTCHASITEKEY_TOOLTIP_HELP'] = 'Personally acquired <a href="https://www.google.com/recaptcha/admin/create" target="_blank">reCAPTCHA v2 <strong>Site key</strong></a>.';
$_ARRAYLANG['TXT_CORE_CONFIG_RECAPTCHASECRETKEY'] = 'Secret key for reCAPTCHA v2';
$_ARRAYLANG['TXT_CORE_CONFIG_RECAPTCHASECRETKEY_TOOLTIP_HELP'] = 'Personally acquired <a href="https://www.google.com/recaptcha/admin/create" target="_blank">reCAPTCHA v2 <strong>Secret key</strong></a>.';
$_ARRAYLANG['TXT_CORE_CONFIG_HCAPTCHASITEKEY'] = 'Sitekey for hCaptcha';
$_ARRAYLANG['TXT_CORE_CONFIG_HCAPTCHASITEKEY_TOOLTIP_HELP'] = 'Personally acquired <a href="https://dashboard.hcaptcha.com/signup" target="_blank">hCaptcha <strong>Sitekey</strong></a>.';
$_ARRAYLANG['TXT_CORE_CONFIG_HCAPTCHASECRETKEY'] = 'Secret key for hCaptcha';
$_ARRAYLANG['TXT_CORE_CONFIG_HCAPTCHASECRETKEY_TOOLTIP_HELP'] = 'Personally acquired <a href="https://dashboard.hcaptcha.com/signup" target="_blank">hCaptcha <strong>Secret key</strong></a>.';
$_ARRAYLANG['TXT_CORE_CONFIG_CONTREXX_CAPTCHA_LABEL'] = 'Cloudrexx';
$_ARRAYLANG['TXT_CORE_CONFIG_RECAPTCHA_LABEL'] = 'reCAPTCHA v2';
$_ARRAYLANG['TXT_CORE_CONFIG_HCAPTCHA_LABEL'] = 'hCaptcha®';
$_ARRAYLANG['TXT_CORE_CONFIG_CAPTCHAMETHOD_TOOLTIP'] = 'Choose the CAPTCHA-mechanism to be used as protection against SPAM:<br><br><strong>Cloudrexx</strong><br>Local service. Low protection against SPAM.<br><br><strong>hCaptcha®</strong><br>hCaptcha is an external service provided by Intuition Machines, Inc. and provides good protection against SPAM while protecting the privacy of users. <a href="https://www.hcaptcha.com/" target="_blank">More information</a>.<br><br><strong>reCAPTCHA v2</strong><br>reCAPTCHA v2 is an external service provided by Google Inc. and provides good protection against SPAM. Requires a Google account. <a href="https://www.google.com/recaptcha/intro/index.html" target="_blank">More information</a>.';
$_ARRAYLANG['TXT_CORE_CONFIG_ALLOWCLIENTSIDESCRIPTUPLOAD'] = 'Allow upload of client-side scripts';
$_ARRAYLANG['TXT_CORE_CONFIG_ALLOWCLIENTSIDESCRIPTUPLOAD_TOOLTIP'] = 'The upload of client-side scripts (xhtml, xml, svg, shtml) is a potential security risk as an attacker can use a client-side script to hijack a browser session and take over the website. Don\'t allow the upload of client-side scripts unless you do fully trust your users.';
$_ARRAYLANG['TXT_CORE_CONFIG_ALLOWCLIENTSIDESCRIPTUPLOADONGROUPS'] = 'Allowed groups to upload client-side scripts';
$_ARRAYLANG['TXT_CORE_CONFIG_ADVANCEDUPLOADBACKEND'] = 'Advanced uploading tools';
$_ARRAYLANG['TXT_CORE_CONFIG_ADVANCEDUPLOADFRONTEND'] = 'Advanced uploading tools';
$_ARRAYLANG['TXT_CORE_CONFIG_FORCEPROTOCOLFRONTEND_TOOLTIP'] = 'By default, Cloudrexx does not enforce a protocol. You have the option to change the setting to force HTTPS in order to improve search engine ranking.';
$_ARRAYLANG['TXT_CORE_CONFIG_FORCEPROTOCOLBACKEND_TOOLTIP'] = 'By default, Cloudrexx does not enforce a protocol. You have the option to change the setting to force HTTPS in order to increase the security level.';
$_ARRAYLANG['TXT_CORE_CONFIG_FORCEPROTOCOLBACKEND'] = 'Protocol in use';
$_ARRAYLANG['TXT_CORE_CONFIG_FORCEPROTOCOLFRONTEND'] = 'Protocol in use';
$_ARRAYLANG['TXT_SETTINGS_FORCE_PROTOCOL_NONE'] = 'dynamic';
$_ARRAYLANG['TXT_SETTINGS_FORCE_PROTOCOL_HTTP'] = 'HTTP';
$_ARRAYLANG['TXT_SETTINGS_FORCE_PROTOCOL_HTTPS'] = 'HTTPS';
$_ARRAYLANG['TXT_CORE_CONFIG_FORCEDOMAINURL_TOOLTIP'] = 'Search engines interpret your homepage content as duplicated content as long as the homepage is accessible by multiple addresses. We recommend to activate this option.';
$_ARRAYLANG['TXT_CORE_CONFIG_FORCEDOMAINURL'] = 'Force main domain';
$_ARRAYLANG['TXT_CORE_TIMEZONE_INVALID'] = 'The selected timezone is not valid.';
$_ARRAYLANG['TXT_CORE_CONFIG_NOBODY_LABEL'] = 'Nobody';
$_ARRAYLANG['TXT_CORE_CONFIG_GROUPS_LABEL'] = 'Selected user groups';
$_ARRAYLANG['TXT_CORE_CONFIG_ALL_LABEL'] = 'Everybody';
$_ARRAYLANG['TXT_SETTINGS_UPDATED'] = 'Settings have been updated.';
$_ARRAYLANG['TXT_SETTINGS_ERROR_WRITABLE'] = 'could not be written. Please check file access permissions (666) of the file.';
$_ARRAYLANG['TXT_SETTINGS_DEFAULT_SMTP_CHANGED'] = 'The SMTP account %s has been set as the default account.';
$_ARRAYLANG['TXT_SETTINGS_CHANGE_DEFAULT_SMTP_FAILED'] = 'The change of the default account has failed';
$_ARRAYLANG['TXT_SETTINGS_SMTP_DELETE_SUCCEED'] = 'The SMTP account %s has been successfully deleted';
$_ARRAYLANG['TXT_SETTINGS_SMTP_DELETE_FAILED'] = 'An error occurred while deleting SMTP account %s!';
$_ARRAYLANG['TXT_SETTINGS_COULD_NOT_DELETE_DEAULT_SMTP'] = 'The SMTP account %s could not be deleted as it is the default account.';
$_ARRAYLANG['TXT_SETTINGS_EMAIL_ACCOUNTS'] = 'Email Accounts';
$_ARRAYLANG['TXT_SETTINGS_ACCOUNT'] = 'Account';
$_ARRAYLANG['TXT_SETTINGS_HOST'] = 'Host';
$_ARRAYLANG['TXT_SETTINGS_USERNAME'] = 'Username';
$_ARRAYLANG['TXT_SETTINGS_STANDARD'] = 'Default';
$_ARRAYLANG['TXT_SETTINGS_FUNCTIONS'] = 'Actions';
$_ARRAYLANG['TXT_SETTINGS_ADD_NEW_SMTP_ACCOUNT'] = 'Add';
$_ARRAYLANG['TXT_SETTINGS_CONFIRM_DELETE_ACCOUNT'] = 'Are you sure you wish to delete the SMTP account %s';
$_ARRAYLANG['TXT_SETTINGS_OPERATION_IRREVERSIBLE'] = 'This operation can not be undone';
$_ARRAYLANG['TXT_SETTINGS_MODFIY'] = 'Edit';
$_ARRAYLANG['TXT_SETTINGS_DELETE'] = 'Delete';
$_ARRAYLANG['TXT_SETTINGS_EMPTY_ACCOUNT_NAME_TXT'] = 'Please define an account name';
$_ARRAYLANG['TXT_SETTINGS_NOT_UNIQUE_SMTP_ACCOUNT_NAME'] = 'The account name %s already exists.';
$_ARRAYLANG['TXT_SETTINGS_EMPTY_SMTP_HOST_TXT'] = 'Please define an SMTP Server';
$_ARRAYLANG['TXT_SETTINGS_SMTP_ACCOUNT_UPDATE_SUCCEED'] = 'The SMTP account %s has been successfully updated';
$_ARRAYLANG['TXT_SETTINGS_SMTP_ACCOUNT_UPDATE_FAILED'] = 'An error occurred while updating SMTP account %s!';
$_ARRAYLANG['TXT_SETTINGS_SMTP_ACCOUNT_ADD_SUCCEED'] = 'The account %s has been deleted successfully.';
$_ARRAYLANG['TXT_SETTINGS_SMTP_ACCOUNT_ADD_FAILED'] = 'An error occurred while creating SMTP account %s!';
$_ARRAYLANG['TXT_SETTINGS_MODIFY_SMTP_ACCOUNT'] = 'Edit SMTP account';
$_ARRAYLANG['TXT_SETTINGS_NAME_OF_ACCOUNT'] = 'Account Name';
$_ARRAYLANG['TXT_SETTINGS_SMTP_SERVER'] = 'SMTP Server';
$_ARRAYLANG['TXT_SETTINGS_PORT'] = 'Port';
$_ARRAYLANG['TXT_SETTINGS_AUTHENTICATION'] = 'Authentication';
$_ARRAYLANG['TXT_SETTINGS_PASSWORD'] = 'Password';
$_ARRAYLANG['TXT_SETTINGS_SMTP_AUTHENTICATION_TXT'] = 'Username and password are not required if your SMTP Server does not require authentication';
$_ARRAYLANG['TXT_SETTINGS_SMTP_HOSTNAME_TXT'] = 'The default port for SMTP is port 25.<br><br>To use SMTP over SSL (or TLS), set the protocol <code>ssl://</code> (or <code>tls:// </code>) in front of the name of the outgoing mail server and enter the corresponding port over which the outgoing mail server accepts SSL connections in the Port field.<br><br>Example:<br>Host: <code>ssl://mail .example.com</code><br>Port: <code>465</code>';
$_ARRAYLANG['TXT_SETTINGS_BACK'] = 'Back';
$_ARRAYLANG['TXT_SETTINGS_SAVE'] = 'Save';
$_ARRAYLANG['TXT_SETTINGS_IMAGE_TITLE'] = 'Image settings';
$_ARRAYLANG['TXT_SETTINGS_IMAGE_CUT_WIDTH'] = 'Default width for image cropping';
$_ARRAYLANG['TXT_SETTINGS_IMAGE_CUT_HEIGHT'] = 'Default height for image cropping';
$_ARRAYLANG['TXT_SETTINGS_IMAGE_SCALE_WIDTH'] = 'Default width for scaling';
$_ARRAYLANG['TXT_SETTINGS_IMAGE_SCALE_HEIGHT'] = 'Default height for scaling';
$_ARRAYLANG['TXT_SETTINGS_IMAGE_COMPRESSION'] = 'Default compression value';
$_ARRAYLANG['TXT_IMAGE_THUMBNAILS_RELOAD'] = 'Regenerate all thumbnails';
$_ARRAYLANG['TXT_CORE_CONFIG_PORTFRONTENDHTTP'] = 'HTTP Port';
$_ARRAYLANG['TXT_CORE_CONFIG_PORTFRONTENDHTTPS'] = 'HTTPS Port';
$_ARRAYLANG['TXT_CORE_CONFIG_PORTBACKENDHTTP'] = 'HTTP Port';
$_ARRAYLANG['TXT_CORE_CONFIG_PORTBACKENDHTTPS'] = 'HTTPS Port';
$_ARRAYLANG['TXT_CORE_CONFIG_FAVICON'] = 'Favicon';
$_ARRAYLANG['TXT_CORE_CONFIG_PDF'] = 'PDF templates';
$_ARRAYLANG['TXT_CORE_CONFIG_ROBOTSTXT'] = 'robots.txt';
$_ARRAYLANG['TXT_CONFIG_UNABLE_TO_SET_MAINDOMAIN'] = 'Unable to change the main domain to %s as the website can not be reached through it. Please verify the DNS-configuration of the domain.';
$_ARRAYLANG['TXT_CONFIG_UNABLE_TO_SET_PROTOCOL'] = 'Unable to change the protocol as the website can not be reached on %s';
$_ARRAYLANG['TXT_CONFIG_UNABLE_TO_FORCE_MAINDOMAIN'] = 'Unable to force the main domain as the website can not be reached through it.';
$_ARRAYLANG['TXT_CORE_CONFIG_USEVIRTUALLANGUAGEDIRECTORIES']          = 'Use virtual language directories';
$_ARRAYLANG['TXT_CONFIG_UNABLE_TO_SET_USEVIRTUALLANGUAGEDIRECTORIES'] = 'The option %s can not be deactivated as long as there are multiple locales defined.';
$_ARRAYLANG['TXT_CORE_CONFIG_UPLOADFILESIZELIMIT'] = 'File upload limit';
$_ARRAYLANG['TXT_CORE_CONFIG_UPLOADFILESIZELIMIT_TOOLTIP'] = 'File size limit of files being uploaded.';
$_ARRAYLANG['TXT_CORE_CONFIG_SESSIONBINDING'] = 'Session Binding';
$_ARRAYLANG['TXT_CORE_CONFIG_SESSIONBINDING_TOOLTIP'] = '<i>Session Binding</i> increases the overall security of the system by binding the user-session to the user\'s browser. This makes it more difficult for an attacker to steal a victim\'s session';
$_ARRAYLANG['TXT_CORE_CONFIG_FRONTENDDENYIFRAMING'] = 'Block Website-Embedding';
$_ARRAYLANG['TXT_CORE_CONFIG_FRONTENDDENYIFRAMING_TOOLTIP'] = 'Activate this option to block embedding the website (frontend) by others (e.g. through an iframe).<br>If this option is activated, then embedding is prevented through the <code>Content-Security-Policy: frame-ancestors</code> HTTP header.<br><br><i class="fa-solid fa-triangle-exclamation"></i> Website-Embedding poses a security threat as it allows an attacker to gain control of the system through a technique called Clickjacking.<br><br><i class="fa-solid fa-circle-exclamation"></i> Embedding of the backend is always blocked, even if this option is disabled.';
$_ARRAYLANG['TXT_CORE_CONFIG_BLOCKMIMESNIFFING'] = 'Block MIME-sniffing';
$_ARRAYLANG['TXT_CORE_CONFIG_BLOCKMIMESNIFFING_TOOLTIP'] = 'Activate this option to block MIME-sniffing.<br>If this option is activated, then MIME-sniffing is blocked through the <code>X-Content-Type-Options: nosniff</code> HTTP header.<br><br><i class="fa-solid fa-triangle-exclamation"></i> Disabling this option poses a security threat as it allows an attacker to execute malicious code.';
$_ARRAYLANG['TXT_CORE_CONFIG_HSTSINCLUDESUBDOMAINS'] = 'Secure subdomains';
$_ARRAYLANG['TXT_CORE_CONFIG_HSTSINCLUDESUBDOMAINS_TOOLTIP'] = 'Activate this option to extend enhanced security features to every part of this website, including all its subdomains. This ensures that your entire site experience is more secure against online threats.<br><br><strong>Warning:</strong> Enable this option only, if all subdomains are accessible over a secure connection (HTTPS) as afterwards only secure connections to subdomains will be possible.<br><br>This will enable the <a href="https://en.wikipedia.org/wiki/HTTP_Strict_Transport_Security" target="_blank">HSTS</a> <code>includeSubDomains</code> directive.';
$_ARRAYLANG['TXT_CORE_CONFIG_HSTSPRELOAD'] = 'Use HTTPS for first-time visitors';
$_ARRAYLANG['TXT_CORE_CONFIG_HSTSPRELOAD_TOOLTIP'] = 'Activate this option to ensure connections made to this site are always done over HTTPS, even for first time visitors. This helps prevent attackers from intercepting or tampering with your data.<br><br>This will enable the <a href="https://en.wikipedia.org/wiki/HTTP_Strict_Transport_Security" target="_blank">HSTS</a> <code>preload</code> directive.';
$_ARRAYLANG['TXT_CORE_CONFIG_CSPFORSCRIPTSRC'] = 'Restrict resources to trusted sources';
$_ARRAYLANG['TXT_CORE_CONFIG_CSPFORSCRIPTSRC_TOOLTIP'] = '<strong><i class="fa-solid fa-triangle-exclamation"></i> This is an experimental function! This might break the website from working properly.</strong><br><br>Activate this option to only allow the execution of JavaScript code and inclusion of resources (images, fonts, styles, etc.) from trusted sources. This enhances the security by blocking potential harmful scripts from unknown sources using a <a href="https://en.wikipedia.org/wiki/Content_Security_Policy" target="_blank">Content Security Policy</a> for <code>script-src</code>, protecting your data from potential threats.<br><br><i class="fa-solid fa-circle-exclamation"></i> Before activating this option, all trusted resources must be registered in the theme. <a href="https://cloudrexx.atlassian.net/wiki/spaces/HELP/pages/468386386/Welche+Meta-Informationen+zum+Theme+gibt+es" target="_blank">More information</a>.';
$_ARRAYLANG['TXT_CORE_CONFIG_UPGRADEINSECUREREQUESTS'] = 'Allow only HTTPS-links';
$_ARRAYLANG['TXT_CORE_CONFIG_UPGRADEINSECUREREQUESTS_TOOLTIP'] = 'Activate this option to ensure that all links are using HTTPS.<br><br>This will set the <code>upgrade-insecure-requests</code> directive of the <a href="https://de.wikipedia.org/wiki/Content_Security_Policy" target="_blank">Content Security Policy</a>.';
$_ARRAYLANG['TXT_CORE_CONFIG_TRUSTEDREVERSEPROXYCOUNT'] = 'Number of Trusted Reverse Proxies';
$_ARRAYLANG['TXT_CORE_CONFIG_TRUSTEDREVERSEPROXYCOUNT_TOOLTIP'] = 'Specify the number of trusted reverse proxies (e.g., load balancers, CDNs) between the client and this website. This setting determines which IP address from the <code>X-Forwarded-For</code> header is considered the client\'s IP address. Set to <code>0</code> (default) if no reverse proxies are used.<br><br><i class="fa-solid fa-triangle-exclamation"></i> Setting this option to an incorrect number of reverse proxies may result in the website\'s unavailability.';
$_ARRAYLANG['TXT_CORE_CONFIG_FILE_EXTENSION_DENIED'] = 'The file extension <code>%s</code> is not allowed.';
$_ARRAYLANG['TXT_CORE_CONFIG_CONFIG_ALLOWCLIENTSIDESCRIPTUPLOAD'] = 'This can be allowed with the configuration option %s.';
