(function($){
    $(document).ready(function(){
        // initialize captcha config
        $('#captchaMethod').change(function() {
            var displayReCaptcha = ($.trim($(this).val()) === 'reCaptcha') ? 'table-row' : 'none';
            $('input[name="recaptchaSiteKey"]').closest('tr').css('display', displayReCaptcha);
            $('input[name="recaptchaSecretKey"]').closest('tr').css('display', displayReCaptcha);
            var displayhCaptcha = ($.trim($(this).val()) === 'hCaptcha') ? 'table-row' : 'none';
            $('input[name="hCaptchaSiteKey"]').closest('tr').css('display', displayhCaptcha);
            $('input[name="hCaptchaSecretKey"]').closest('tr').css('display', displayhCaptcha);
        });
        $('#captchaMethod').trigger('change');

        // initialize client side script upload config
        $('#allowClientsideScriptUpload').change(function() {
            var displayClientSideGroupSelection = ($.trim($(this).val()) === 'groups') ? 'table-row' : 'none';
            $('select[name="allowClientSideScriptUploadOnGroups[]"]').closest('tr').css('display', displayClientSideGroupSelection);
        });
        $('#allowClientsideScriptUpload').trigger('change');
        const toggleHttpsOptions = function() {
            let httpsIsEnforced = 'off';
            if (
                $('#forceProtocolFrontend').val() === 'https'
                && $('#forceProtocolBackend').val() === 'https'
            ) {
                httpsIsEnforced = 'on';
            } else {
                $('input[name="hstsIncludeSubDomains"][value="off"]').prop('checked', true);
                $('input[name="upgradeInsecureRequests"][value="off"]').prop('checked', true);
            }
            $('input[name="hstsIncludeSubDomains"]').closest('tr').css(
                'display',
                httpsIsEnforced == 'on' ? 'table-row' : 'none'
            );
            $('input[name="upgradeInsecureRequests"]').closest('tr').css(
                'display',
                httpsIsEnforced == 'on' ? 'table-row' : 'none'
            );
            if ($('input[name="hstsIncludeSubDomains"]:checked').val() === 'off') {
                $('input[name="hstsPreload"][value="off"]').prop('checked', true);
                $('input[name="hstsPreload"]').closest('tr').css('display', 'none');
            } else {
                $('input[name="hstsPreload"]').closest('tr').css('display', 'table-row');
            }
        };
        $('#forceProtocolFrontend').change(toggleHttpsOptions);
        $('#forceProtocolBackend').change(toggleHttpsOptions);
        $('input[name="hstsIncludeSubDomains"]').change(toggleHttpsOptions);
        $('input[name="upgradeInsecureRequests"]').change(toggleHttpsOptions);
        toggleHttpsOptions();
        $('input[name="cspForScriptSrc"]').closest('tr').find('td:first').append($('<span class="experimental"></span>'));
        $('input[name="trustedReverseProxyCount"]').closest('tr').find('td:first').append($('<span class="experimental"></span>'));
    });
})(cx.jQuery);
