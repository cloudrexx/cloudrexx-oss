# Cloudrexx #
Cloudrexx is an open source PHP based web content management system released under the GNU AGPL.

## Installation (automated) ##
Please follow the instructions on [dev.cloudrexx.com/Local installation/](https://dev.cloudrexx.com/Local%20installation/) to easily setup one or more local Cloudrexx installations based on Docker.

For Unix based systems with Docker and Docker-Compose the following bash command should install Cloudrexx in the current directory:

```bash
wget https://bitbucket.org/cloudrexx/cloudrexx-oss/raw/HEAD/cx && chmod +x cx && ./cx env init
```

## Bugtracker ##
Bugs are tracked on [bugs.cloudrexx.com](https://bugs.cloudrexx.com).

## Development and Contribution ##
* [How to contribute?](CONTRIBUTING.md)
* [Development Documentation](https://dev.cloudrexx.com)

## License ##
Cloudrexx
[https://www.cloudrexx.com](https://www.cloudrexx.com)
Cloudrexx AG 2007-2024

This program can be used under the terms of the GNU Affero General Public License, version 3.

The texts of the GNU Affero General Public License with an additional permission can be found at and in the LICENSE file you have received along with this program.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

"Cloudrexx" is a registered trademark of Cloudrexx AG. The licensing of the program under the AGPLv3 does not imply a trademark license. Therefore any rights, title and interest in our trademarks remain entirely with us.
