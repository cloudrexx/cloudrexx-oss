cx.ready(function() {
    const el = document.querySelector("#form-0-component");
    if (!el) {
        // We're not in edit mode
        return;
    }
    if (el.value == "") {
        return;
    }
    for (formField of document.querySelectorAll("#form-0-overview input, #form-0-overview select, #form-0-overview textarea")) {
        if (formField.classList.contains("translatable")) {
            continue;
        }
        if (formField.type && formField.type == "hidden") {
            continue;
        }
        // As the browser does not send disabled fields we need
        // to add hidden fields for them
        formField.disabled = true;
        if (formField.id == "form-0-active_no") {
            continue;
        }
        const hiddenField = document.createElement("input");
        hiddenField.type = "hidden";
        hiddenField.name = formField.name;
        hiddenField.value = formField.value;
        formField.after(hiddenField);
    }
});
