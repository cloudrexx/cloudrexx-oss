cx.privacy = {
    cookies: {
        cookiesToUpdate: {},
        getCategories: function() {
            let cookieCategories = cx.variables.get("cookieCategories", "Privacy");
            let categoryDescriptions = cx.variables.get("cookieCategoryDescriptions", "Privacy");
            let categories = {};
            for (category in cookieCategories) {
                categories[category] = {
                    id: category,
                    name: cookieCategories[category],
                    description: categoryDescriptions[category],
                };
            }
            return categories;
        },
        getCookiesByCategory: function(categoryId) {
            let cookies = cx.variables.get("cookies", "Privacy");
            return cookies[categoryId];
        },
        getCookie: function(cookieId) {
            cookieId = this.getCookieIdFromSystemName(cookieId);
            for (categoryId in this.getCategories()) {
                let cookies = this.getCookiesByCategory(categoryId);
                if (cookies && cookies.hasOwnProperty(cookieId)) {
                    return cookies[cookieId];
                }
            }
        },
        isCookieActive: function(cookieId) {
            cookieId = this.getCookieIdFromSystemName(cookieId);
            if (this.cookiesToUpdate.hasOwnProperty(cookieId)) {
                return this.cookiesToUpdate[cookieId];
            }
            if (!this.getCookie(cookieId).disableable) {
                return true;
            }
            let contents = this.getCookieContents();
            if (contents == "none") {
                return false;
            }
            if (contents == "accepted") {
                return true;
            }
            if (contents == "") {
                return false;
            }
            let cookieData = JSON.parse(contents);
            return cookieData.hasOwnProperty(cookieId) && cookieData[cookieId];
        },
        isCookieCategoryActive: function(categoryId) {
            let allAccepted = true;
            let allDeclined = true;
            if (!this.getCookiesByCategory(categoryId)) {
                return "no cookies";
            }
            for (cookieId in this.getCookiesByCategory(categoryId)) {
                if (!this.getCookie(cookieId).disableable) {
                    continue;
                }
                if (this.isCookieActive(cookieId)) {
                    allDeclined = false;
                } else {
                    allAccepted = false;
                }
                if (!allAccepted && !allDeclined) {
                    return "some";
                }
            }
            if (allDeclined) {
                return "none";
            } else {
                return "all";
            }
        },
        isCookieCategoryDisableable: function(categoryId) {
            let cookies = this.getCookiesByCategory(categoryId);
            for (cookieId in cookies) {
                if (cookies[cookieId].disableable) {
                    return true;
                }
            }
            return false;
        },
        setCookieActive: function(cookieId, active, persist) {
            cookieId = this.getCookieIdFromSystemName(cookieId);
            if (this.isCookieActive(cookieId) == active) {
                return;
            }
            this.cookiesToUpdate[cookieId] = active;
            cx.trigger("cookieChanged", "privacy", {cookie: this.getCookie(cookieId)});

            if (persist !== false) {
                this.updateCookie();
            }
        },
        setCookieCategoryActive: function(categoryId, active, persist) {
            // never disable essential cookies
            if (categoryId === 1) {
                return;
            }
            for (cookieId in this.getCookiesByCategory(categoryId)) {
                this.setCookieActive(cookieId, active, false);
            }
            if (persist !== false) {
                this.updateCookie();
            }
        },
        setAllCookiesActive: function(active) {
            for (categoryId in this.getCategories()) {
                this.setCookieCategoryActive(categoryId, active, false);
            }
            this.updateCookie();
        },
        getPrivacyNoteExpireTime: function() {
            var date = new Date(), expires = "", expireTime = "";
            switch (cx.variables.get("cookieNoteTtl", "Privacy")) {
                case "week":
                    date.setTime(date.getTime() + 7 * 24 * 60 * 60 * 1000);
                    expireTime = date.toUTCString();
                    break;
                case "month":
                    date.setMonth(date.getMonth() + 1);
                    expireTime = date.toUTCString();
                    break;
                case "year":
                    date.setFullYear(date.getFullYear() + 1);
                    expireTime = date.toUTCString();
                    break;
                case "unlimited":
                    date.setFullYear(date.getFullYear() + 10);
                    expireTime = date.toUTCString();
                    break;
                default:
                    return "";
                    break;
            }
            return "expires=" + expireTime;
        },
        isConsentSet: function() {
            return this.getCookieContents() != "";
        },
        updateCookie: function() {
            let allAccepted = true;
            let allDeclined = true;
            for (categoryId in this.getCategories()) {
                switch (this.isCookieCategoryActive(categoryId)) {
                    case "all":
                        allDeclined = false;
                        break;
                    case "none":
                        allAccepted = false;
                        break;
                    case "some":
                        allAccepted = false;
                        allDeclined = false;
                        break;
                }
                if (!allAccepted && !allDeclined) {
                    break;
                }
            }
            if (allAccepted) {
                this.setCookieContents("accepted");
                this.cookiesToUpdate = {};
                return;
            }
            if (allDeclined) {
                this.setCookieContents("none");
                this.cookiesToUpdate = {};
                return;
            }
            let cookieStates = {};
            for (categoryId in this.getCategories()) {
                for (cookieId in this.getCookiesByCategory(categoryId)) {
                    cookieStates[cookieId] = this.isCookieActive(cookieId);
                }
            }
            this.setCookieContents(JSON.stringify(cookieStates));
            this.cookiesToUpdate = {};
        },
        setCookieContents: function(contents) {
            document.cookie = "ClxCookieNote=" + contents + "; path=/;" + this.getPrivacyNoteExpireTime();
        },
        getCookieContents: function() {
            return ("; " + document.cookie).split("; ClxCookieNote=").pop().split(";").shift();
        },
        getCookieIdFromSystemName: function(cookieName) {
            if (
                !isNaN(cookieName) && 
                parseInt(Number(cookieName)) == cookieName && 
                !isNaN(parseInt(cookieName, 10))
            ) {
                return cookieName;
            }
            for (categoryId in this.getCategories()) {
                for (cookieId in this.getCookiesByCategory(categoryId)) {
                    if (this.getCookiesByCategory(categoryId)[cookieId].identifier == cookieName) {
                        return cookieId;
                    }
                }
            }
            console.log(Number.isInteger(cookieName), cookieName, typeof cookieName);
            throw "Unknown cookie requested: " + cookieName;
        }
    }
}
document.addEventListener("DOMContentLoaded", function() {
    const initEventListenerData = function(callback) {
        for (categoryId in cx.privacy.cookies.getCategories()) {
            for (cookieId in cx.privacy.cookies.getCookiesByCategory(categoryId)) {
                callback({
                    cookie: cx.privacy.cookies.getCookie(cookieId),
                    initCall: true
                });
            }
        }
    };
    // Case for already registered event listeners
    initEventListenerData(function(data) {
        cx.trigger(
            "cookieChanged",
            "privacy",
            data
        );
    });
    // Case for event listeners that are registered later on
    cx.bind("eventListenerAdded", function(eventArgs) {
        if (
            eventArgs.event != "cookieChanged" ||
            eventArgs.scope != "privacy"
        ) {
            return;
        }
        initEventListenerData(function(data) {
            eventArgs.callback(data);
        });
    });
});

