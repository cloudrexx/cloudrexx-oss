document.addEventListener("DOMContentLoaded", function() {
    // show/hide settings
    document.querySelector("#cookie-note-2022-details").addEventListener("click", function() {
        document.querySelector("#cookie-settings-popup").classList.toggle("hidden");
    });

    // show/hide details
    document.addEventListener("click", function(event) {
        if (!event.target.closest(".cookie-class-detail-link")) {
            return;
        }
        event.target.parentElement.querySelector(".cookie-class-list").classList.toggle("hidden");
    });

    // change cookie state
    document.querySelectorAll("#cookie-note input").forEach(function(input) {
        input.addEventListener("change", function(event) {
            if (input.dataset.cookieId) {
                cx.privacy.cookies.setCookieActive(
                    event.target.dataset.cookieId,
                    event.target.checked
                );
            } else if (input.dataset.categoryId) {
                cx.privacy.cookies.setCookieCategoryActive(
                    event.target.dataset.categoryId,
                    event.target.checked
                );
            }
            updateCheckboxes();
        });
    });

    // accept all
    document.querySelector("#cookie-note-2022-ok").addEventListener("click", function() {
        cx.privacy.cookies.setAllCookiesActive(true);
        closePrivacyNote();
        updateCheckboxes();
    });

    // close on "save"
    document.querySelector("#cookie-note-2022-save").addEventListener("click", function() {
        cx.privacy.cookies.updateCookie();
        closePrivacyNote();
        updateCheckboxes();
    });

    // accept category 1 on "nur essentielle"
    document.querySelector("#cookie-note-2022-nok").addEventListener("click", function(event) {
        cx.privacy.cookies.setAllCookiesActive(false);
        closePrivacyNote();
        updateCheckboxes();
    });

    // initialize all cookie states correctly
    updateCheckboxes();
    document.querySelectorAll("input[type=checkbox][data-category-id]").forEach(function(input) {
        if (!cx.privacy.cookies.isCookieCategoryDisableable(input.dataset.categoryId)) {
            input.parentElement.classList.add("hidden");
        }
    });

    // initialize note state
    if (cx.privacy.cookies.isConsentSet()) {
        closePrivacyNote();
    }

    function closePrivacyNote() {
        document.querySelector("#cookie-note").classList.add("hidden");
        setTimeout(function() {
            document.addEventListener("click", reopenEventListener);
        }, 100);
    }

    function reopenEventListener(event) {
        if (!event.target.closest("#cookie-note.hidden")) {
            return;
        }
        document.querySelector("#cookie-note").classList.remove("hidden");
        document.removeEventListener("click", reopenEventListener);
    }

    function updateCheckboxes() {
        document.querySelectorAll("#cookie-note input").forEach(function(input) {
            if (input.dataset.cookieId) {
                input.checked = cx.privacy.cookies.isCookieActive(input.dataset.cookieId);
            } else if (input.dataset.categoryId) {
                input.checked = cx.privacy.cookies.isCookieCategoryActive(
                    input.dataset.categoryId
                ) != "none";
            }
        });
    }
    // DO NOT USE THIS!
    document.clxUpdateCookieCheckboxes = updateCheckboxes;
});
