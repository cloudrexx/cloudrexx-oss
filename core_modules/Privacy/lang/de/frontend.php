<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * @copyright   CLOUDREXX CMS - CLOUDREXX AG
 * @author      Michael Ritter <michael.ritter@cloudrexx.com>
 * @access      public
 * @package     cloudrexx
 * @subpackage  coremodules_privacy
 */
global $_ARRAYLANG;
$_ARRAYLANG['TXT_PRIVACY_COOKIE_NOTE_TITLE'] = 'Datenschutz-Einstellungen';
$_ARRAYLANG['TXT_PRIVACY_COOKIE_NOTE_TEXT'] = 'Um unsere Webseite für Sie optimal zu gestalten und fortlaufend verbessern zu können, verwenden wir Cookies. Durch die weitere Nutzung der Webseite stimmen Sie der Verwendung von Cookies zu.';
$_ARRAYLANG['TXT_PRIVACY_COOKIE_NOTE_TEXT_2022'] = 'Diese Website verwendet Cookies und Targeting Technologien, um Ihnen ein besseres Internet-Erlebnis zu ermöglichen. Diese Technologien nutzen wir außerdem, um Ergebnisse zu messen, um zu verstehen, woher unsere Besucher kommen oder um unsere Website weiter zu entwickeln.';
$_ARRAYLANG['TXT_PRIVACY_COOKIE_NOTE_BUTTON'] = 'Verstanden';
$_ARRAYLANG['TXT_PRIVACY_COOKIE_NOTE_BUTTON_OK'] = 'Alle akzeptieren';
$_ARRAYLANG['TXT_PRIVACY_COOKIE_NOTE_BUTTON_NOK'] = 'Nur essentielle';
$_ARRAYLANG['TXT_PRIVACY_COOKIE_NOTE_BUTTON_SETTINGS'] = 'Details';
$_ARRAYLANG['TXT_PRIVACY_COOKIE_NOTE_PRE_LINK'] = 'Weitere Informationen zu Cookies erhalten Sie in unserer ';
$_ARRAYLANG['TXT_PRIVACY_COOKIE_NOTE_PRE_LINK_2022'] = 'Weitere Informationen zu Cookies erhalten Sie in unserer ';
$_ARRAYLANG['TXT_PRIVACY_COOKIE_NOTE_LINK'] = 'Datenschutzerklärung';
$_ARRAYLANG['TXT_PRIVACY_COOKIE_NOTE_LINK_2022'] = 'Datenschutzerklärung';
$_ARRAYLANG['TXT_PRIVACY_COOKIE_NOTE_POST_LINK'] = '.';
$_ARRAYLANG['TXT_PRIVACY_COOKIE_NOTE_POST_LINK_2022'] = ' und im <a href="?section=Imprint">Impressum</a>.';
$_ARRAYLANG['TXT_PRIVACY_COOKIE_NOTE_SHOW_DETAILS'] = 'Details anzeigen';
$_ARRAYLANG['TXT_PRIVACY_COOKIE_NOTE_ACCEPT'] = 'Akzeptieren';
$_ARRAYLANG['TXT_PRIVACY_COOKIE_NOTE_NAME'] = 'Name';
$_ARRAYLANG['TXT_PRIVACY_COOKIE_NOTE_USE'] = 'Zweck';
$_ARRAYLANG['TXT_PRIVACY_COOKIE_NOTE_COOKIE_NAMES'] = 'Cookie Namen';
$_ARRAYLANG['TXT_PRIVACY_COOKIE_NOTE_COOKIE_HOST'] = 'Cookie Host';
$_ARRAYLANG['TXT_PRIVACY_COOKIE_NOTE_PRIVACY_STATEMENT'] = 'Datenschutzerklärung';
$_ARRAYLANG['TXT_PRIVACY_COOKIE_NOTE_CLOSE'] = 'Schliessen';

$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_COOKIE_CATEGORY_1_NAME'] = 'Notwendig';
$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_COOKIE_CATEGORY_1_DESCRIPTION'] = 'Notwendige Cookies helfen dabei, eine Webseite nutzbar zu machen, indem sie Grundfunktionen wie Seitennavigation und Zugriff auf sichere Bereiche der Webseite ermöglichen. Die Webseite kann ohne diese Cookies nicht richtig funktionieren.';
$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_COOKIE_CATEGORY_2_NAME'] = 'Präferenzen';
$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_COOKIE_CATEGORY_2_DESCRIPTION'] = 'Präferenz-Cookies ermöglichen einer Webseite sich an Informationen zu erinnern, die die Art beeinflussen, wie sich eine Webseite verhält oder aussieht, wie z. B. Ihre bevorzugte Sprache oder die Region in der Sie sich befinden.';
$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_COOKIE_CATEGORY_3_NAME'] = 'Statistiken';
$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_COOKIE_CATEGORY_3_DESCRIPTION'] = 'Statistik-Cookies helfen Webseiten-Besitzern zu verstehen, wie Besucher mit Webseiten interagieren, indem Informationen anonym gesammelt und gemeldet werden.';
$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_COOKIE_CATEGORY_4_NAME'] = 'Marketing';
$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_COOKIE_CATEGORY_4_DESCRIPTION'] = 'Marketing-Cookies werden verwendet, um Besuchern auf Webseiten zu folgen. Die Absicht ist, Anzeigen zu zeigen, die relevant und ansprechend für den einzelnen Benutzer sind und daher wertvoller für Publisher und werbetreibende Drittparteien sind.';

$_ARRAYLANG['TXT_CORE_MODULE_PRIVACY_SYSCOOKIE_CLXCOOKIENOTE_NAME'] = 'Datenschutzhinweis';
$_ARRAYLANG['TXT_CORE_MODULE_PRIVACY_SYSCOOKIE_CLXCOOKIENOTE_DESCRIPTION'] = 'Einstellungen des Datenschutzhinweises merken.';
