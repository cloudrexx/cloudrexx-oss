<?php
global $_ARRAYLANG;

$_ARRAYLANG['TXT_CORE_MODULE_PRIVACY'] = 'Datenschutz';
$_ARRAYLANG['TXT_CORE_MODULE_PRIVACY_ACT_DEFAULT'] = 'Übersicht';
$_ARRAYLANG['TXT_CORE_MODULE_PRIVACY_ACT_COOKIE'] = 'Übersicht';
$_ARRAYLANG['TXT_CORE_MODULE_PRIVACY_ACT_SETTINGS'] = 'Einstellungen';
$_ARRAYLANG['TXT_CORE_MODULE_PRIVACY_ACT_SETTINGS_DEFAULT'] = 'Allgemein';
$_ARRAYLANG['TXT_CORE_MODULE_PRIVACY_COOKIENOTETYPE'] = 'Datenschutzhinweis-Typ';
$_ARRAYLANG['TXT_CORE_MODULE_PRIVACY_COOKIENOTETTL'] = 'Datenschutzhinweis Lebensdauer';

$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_OVERVIEW_HEADER'] = 'Datenschutzhinweis-Einträge';
$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_COOKIE_ENTITY_NAME'] = 'Datenschutzhinweis-Eintrag';

$_ARRAYLANG['TXT_CORE_MODULE_PRIVACY_OVERVIEW_TEXT'] = 'Dieser Bereich ermöglicht Einstellungen zu Datenschutz-Themen. Hier sehen Sie die von Cloudrexx gesetzten Cookies und können beliebig viele Cookies und Tracker erfassen. Die <a href="/cadmin/Privacy/Settings">Einstellungen</a> ermöglichen das Anzeigen eines einfachen oder erweiterten Datenschutzhinweises für die Besucher. Weitere Informationen dazu finden Sie im entsprechenden <a href="https://cloudrexx.atlassian.net/wiki/spaces/HELP/pages/498335745" target="_blank">Supportartikel</a> und in der <a href="https://dev.cloudrexx.com/Cookies" target="_blank">Entwicklerdokumentation</a>.';

$_ARRAYLANG['active'] = 'Status';
$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_EDIT_ACTIVE'] = 'Aktiv';
$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_ACTIVE_TOOLTIP'] = 'Inaktive Einträge werden auf der Website nicht angezeigt.';
$_ARRAYLANG['name'] = 'Titel';
$_ARRAYLANG['identifier'] = 'Identifikator';
$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_IDENTIFIER_TOOLTIP'] = 'Mit diesem Namen kann das Cookie im Code referenziert werden.';
$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_IDENTIFIER_ERROR_DUPLICATE'] = 'Der Identifikator "%s" ist bereits in Gebrauch und kann nur einmal verwendet werden.';
$_ARRAYLANG['category'] = 'Kategorie';
$_ARRAYLANG['description'] = 'Zweck';
$_ARRAYLANG['cookieNames'] = 'Cookie Namen';
$_ARRAYLANG['TXT_CORE_MODULE_PRIVACY_COOKIE_NAMES_TOOLTIP'] = 'Komma-getrennte Liste von Cookie Namen';
$_ARRAYLANG['host'] = 'Anbieter';
$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_HOST_TOOLTIP'] = 'Der Anbieter dieser Funktion.';
$_ARRAYLANG['privacyStatementUrl'] = 'URL der Datenschutzerklärung';

$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_SETTINGS_COOKIENOTETYPE_OFF'] = 'Deaktiviert';
$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_SETTINGS_COOKIENOTETYPE_ON'] = 'Nur Hinweis';
$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_SETTINGS_COOKIENOTETYPE_2022'] = 'Cookie- und Tracker-Auswahl';
$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_SETTINGS_COOKIENOTETTL'] = 'Cookie-Hinweis Lebensdauer';
$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_SETTINGS_COOKIENOTETTL_SESSION'] = 'Browsersitzung';
$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_SETTINGS_COOKIENOTETTL_WEEK'] = '1 Woche';
$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_SETTINGS_COOKIENOTETTL_MONTH'] = '1 Monat';
$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_SETTINGS_COOKIENOTETTL_YEAR'] = '1 Jahr';
$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_SETTINGS_COOKIENOTETTL_UNLIMITED'] = 'Unendlich';

$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_COOKIE_CATEGORY_1_NAME'] = 'Notwendig';
$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_COOKIE_CATEGORY_1_DESCRIPTION'] = 'Notwendige Cookies helfen dabei, eine Webseite nutzbar zu machen, indem sie Grundfunktionen wie Seitennavigation und Zugriff auf sichere Bereiche der Webseite ermöglichen. Die Webseite kann ohne diese Cookies nicht richtig funktionieren.';
$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_COOKIE_CATEGORY_2_NAME'] = 'Präferenzen';
$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_COOKIE_CATEGORY_2_DESCRIPTION'] = 'Präferenz-Cookies ermöglichen einer Webseite sich an Informationen zu erinnern, die die Art beeinflussen, wie sich eine Webseite verhält oder aussieht, wie z. B. Ihre bevorzugte Sprache oder die Region in der Sie sich befinden.';
$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_COOKIE_CATEGORY_3_NAME'] = 'Statistiken';
$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_COOKIE_CATEGORY_3_DESCRIPTION'] = 'Statistik-Cookies helfen Webseiten-Besitzern zu verstehen, wie Besucher mit Webseiten interagieren, indem Informationen anonym gesammelt und gemeldet werden.';
$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_COOKIE_CATEGORY_4_NAME'] = 'Marketing';
$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_COOKIE_CATEGORY_4_DESCRIPTION'] = 'Marketing-Cookies werden verwendet, um Besuchern auf Webseiten zu folgen. Die Absicht ist, Anzeigen zu zeigen, die relevant und ansprechend für den einzelnen Benutzer sind und daher wertvoller für Publisher und werbetreibende Drittparteien sind.';

$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_CATEGORY_TOOLTIP'] = '<p><b>Notwendig</b>: Notwendige Cookies helfen dabei, eine Webseite nutzbar zu machen, indem sie Grundfunktionen wie Seitennavigation und Zugriff auf sichere Bereiche der Webseite ermöglichen. Die Webseite kann ohne diese Cookies nicht richtig funktionieren.</p><p><b>Präferenzen</b>: Präferenz-Cookies ermöglichen einer Webseite sich an Informationen zu erinnern, die die Art beeinflussen, wie sich eine Webseite verhält oder aussieht, wie z. B. Ihre bevorzugte Sprache oder die Region in der Sie sich befinden.</p><p><b>Statistiken</b>: Statistik-Cookies helfen Webseiten-Besitzern zu verstehen, wie Besucher mit Webseiten interagieren, indem Informationen anonym gesammelt und gemeldet werden.</p><p><b>Marketing</b>: Marketing-Cookies werden verwendet, um Besuchern auf Webseiten zu folgen. Die Absicht ist, Anzeigen zu zeigen, die relevant und ansprechend für den einzelnen Benutzer sind und daher wertvoller für Publisher und werbetreibende Drittparteien sind.</p>';

$_ARRAYLANG['TXT_CORE_MODULE_PRIVACY_SYSCOOKIE_CLXCOOKIENOTE_NAME'] = 'Datenschutzhinweis';
$_ARRAYLANG['TXT_CORE_MODULE_PRIVACY_SYSCOOKIE_CLXCOOKIENOTE_DESCRIPTION'] = 'Einstellungen des Datenschutzhinweises merken.';
