<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * @copyright   CLOUDREXX CMS - CLOUDREXX AG
 * @author      Michael Ritter <michael.ritter@cloudrexx.com>
 * @access      public
 * @package     cloudrexx
 * @subpackage  coremodules_privacy
 */
global $_ARRAYLANG;
$_ARRAYLANG['TXT_PRIVACY_COOKIE_NOTE_TITLE'] = 'Privacy settings';
$_ARRAYLANG['TXT_PRIVACY_COOKIE_NOTE_TEXT'] = 'To provide you the best user experience on our website we do use cookies. By staying on this website you agree to our usage of cookies.';
$_ARRAYLANG['TXT_PRIVACY_COOKIE_NOTE_TEXT_2022'] = 'This website uses cookies an targeting technologies to provide you with a better internet experience. These technologies are furthermore used to measure findings and understand where our visitors come from or to improve our website.';
$_ARRAYLANG['TXT_PRIVACY_COOKIE_NOTE_BUTTON'] = 'Understood';
$_ARRAYLANG['TXT_PRIVACY_COOKIE_NOTE_BUTTON_OK'] = 'Accept all';
$_ARRAYLANG['TXT_PRIVACY_COOKIE_NOTE_BUTTON_NOK'] = 'Essential only';
$_ARRAYLANG['TXT_PRIVACY_COOKIE_NOTE_BUTTON_SETTINGS'] = 'Details';
$_ARRAYLANG['TXT_PRIVACY_COOKIE_NOTE_PRE_LINK'] = 'Please see our ';
$_ARRAYLANG['TXT_PRIVACY_COOKIE_NOTE_PRE_LINK_2022'] = 'You can get further information about cookies in our ';
$_ARRAYLANG['TXT_PRIVACY_COOKIE_NOTE_LINK'] = 'privacy policy';
$_ARRAYLANG['TXT_PRIVACY_COOKIE_NOTE_LINK_2022'] = 'privacy policy';
$_ARRAYLANG['TXT_PRIVACY_COOKIE_NOTE_POST_LINK'] = ' for more information regarding our use of cookies.';
$_ARRAYLANG['TXT_PRIVACY_COOKIE_NOTE_POST_LINK_2022'] = ' an in the <a href="?section=Imprint">imprint</a>.';
$_ARRAYLANG['TXT_PRIVACY_COOKIE_NOTE_SHOW_DETAILS'] = 'Show details';
$_ARRAYLANG['TXT_PRIVACY_COOKIE_NOTE_ACCEPT'] = 'Accept';
$_ARRAYLANG['TXT_PRIVACY_COOKIE_NOTE_NAME'] = 'Name';
$_ARRAYLANG['TXT_PRIVACY_COOKIE_NOTE_USE'] = 'Use';
$_ARRAYLANG['TXT_PRIVACY_COOKIE_NOTE_COOKIE_NAMES'] = 'Cookie Names';
$_ARRAYLANG['TXT_PRIVACY_COOKIE_NOTE_COOKIE_HOST'] = 'Cookie Host';
$_ARRAYLANG['TXT_PRIVACY_COOKIE_NOTE_PRIVACY_STATEMENT'] = 'Privacy statement';
$_ARRAYLANG['TXT_PRIVACY_COOKIE_NOTE_CLOSE'] = 'Close';

$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_COOKIE_CATEGORY_1_NAME'] = 'Necessary';
$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_COOKIE_CATEGORY_1_DESCRIPTION'] = 'Necessary cookies help make a website usable by enabling basic functions like page navigation and access to secure areas of the website. The website cannot function properly without these cookies.';
$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_COOKIE_CATEGORY_2_NAME'] = 'Preferences';
$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_COOKIE_CATEGORY_2_DESCRIPTION'] = 'Preference cookies enable a website to remember information that changes the way the website behaves or looks, like your preferred language or the region that you are in.';
$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_COOKIE_CATEGORY_3_NAME'] = 'Statistics';
$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_COOKIE_CATEGORY_3_DESCRIPTION'] = 'Statistic cookies help website owners to understand how visitors interact with websites by collecting and reporting information anonymously.';
$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_COOKIE_CATEGORY_4_NAME'] = 'Marketing';
$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_COOKIE_CATEGORY_4_DESCRIPTION'] = 'Marketing cookies are used to track visitors across websites. The intention is to display ads that are relevant and engaging for the individual user and thereby more valuable for publishers and third party advertisers.';

$_ARRAYLANG['TXT_CORE_MODULE_PRIVACY_SYSCOOKIE_CLXCOOKIENOTE_NAME'] = 'Privacy note';
$_ARRAYLANG['TXT_CORE_MODULE_PRIVACY_SYSCOOKIE_CLXCOOKIENOTE_DESCRIPTION'] = 'Save settings of the privacy note.';
