<?php
global $_ARRAYLANG;

$_ARRAYLANG['TXT_CORE_MODULE_PRIVACY'] = 'Privacy';
$_ARRAYLANG['TXT_CORE_MODULE_PRIVACY_ACT_DEFAULT'] = 'Overview';
$_ARRAYLANG['TXT_CORE_MODULE_PRIVACY_ACT_COOKIE'] = 'Overview';
$_ARRAYLANG['TXT_CORE_MODULE_PRIVACY_ACT_SETTINGS'] = 'Settings';
$_ARRAYLANG['TXT_CORE_MODULE_PRIVACY_ACT_SETTINGS_DEFAULT'] = 'General';
$_ARRAYLANG['TXT_CORE_MODULE_PRIVACY_COOKIENOTETYPE'] = 'Privacy note type';
$_ARRAYLANG['TXT_CORE_MODULE_PRIVACY_COOKIENOTETTL'] = 'Privacy note lifetime';

$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_OVERVIEW_HEADER'] = 'Privacy note entries';
$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_COOKIE_ENTITY_NAME'] = 'Privacy note entries';

$_ARRAYLANG['TXT_CORE_MODULE_PRIVACY_OVERVIEW_TEXT'] = 'This section allows privacy related settings. In the tab <a href="/cadmin/Privacy/Cookie">Cookies &amp; Tracker</a> you see the cookies set by Cloudrexx and can add your own cookies and trackers. The <a href="/cadmin/Privacy/Settings">Settings</a> allow to show a simple or extended privacy note to visitory...<br><br>You can find further information about this in the <a href="https://cloudrexx.atlassian.net/wiki/spaces/HELP/pages/498335745" target="_blank">Knowledgebase</a> and in the <a href="https://dev.cloudrexx.com/Cookies" target="_blank">Developer documentation</a>.';

$_ARRAYLANG['active'] = 'Status';
$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_EDIT_ACTIVE'] = 'Active';
$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_ACTIVE_TOOLTIP'] = 'Inaktive entries will not be shown on the website.';
$_ARRAYLANG['name'] = 'Title';
$_ARRAYLANG['identifier'] = 'Identifier';
$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_IDENTIFIER_TOOLTIP'] = 'The cookie can be referenced by this name in the code.';
$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_IDENTIFIER_ERROR_DUPLICATE'] = 'The identifier "%s" is already in use an can only be used once.';
$_ARRAYLANG['category'] = 'Category';
$_ARRAYLANG['description'] = 'Use';
$_ARRAYLANG['cookieNames'] = 'Cookie Names';
$_ARRAYLANG['TXT_CORE_MODULE_PRIVACY_COOKIE_NAMES_TOOLTIP'] = 'Comma-separated list of cookie names';
$_ARRAYLANG['host'] = 'Provider';
$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_HOST_TOOLTIP'] = 'The provider of this functionality.';
$_ARRAYLANG['privacyStatementUrl'] = 'Privacy statement URL';

$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_SETTINGS_COOKIENOTETYPE_OFF'] = 'Deaktiviert';
$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_SETTINGS_COOKIENOTETYPE_ON'] = 'Note only';
$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_SETTINGS_COOKIENOTETYPE_2022'] = 'Cookie and tracker selection';
$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_SETTINGS_COOKIENOTETTL'] = 'Cookie notice lifespan';
$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_SETTINGS_COOKIENOTETTL_SESSION'] = 'Browser session';
$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_SETTINGS_COOKIENOTETTL_WEEK'] = '1 week';
$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_SETTINGS_COOKIENOTETTL_MONTH'] = '1 month';
$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_SETTINGS_COOKIENOTETTL_YEAR'] = '1 year';
$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_SETTINGS_COOKIENOTETTL_UNLIMITED'] = 'Endless';

$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_COOKIE_CATEGORY_1_NAME'] = 'Necessary';
$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_COOKIE_CATEGORY_1_DESCRIPTION'] = 'Necessary cookies help make a website usable by enabling basic functions like page navigation and access to secure areas of the website. The website cannot function properly without these cookies.';
$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_COOKIE_CATEGORY_2_NAME'] = 'Preferences';
$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_COOKIE_CATEGORY_2_DESCRIPTION'] = 'Preference cookies enable a website to remember information that changes the way the website behaves or looks, like your preferred language or the region that you are in.';
$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_COOKIE_CATEGORY_3_NAME'] = 'Statistics';
$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_COOKIE_CATEGORY_3_DESCRIPTION'] = 'Statistic cookies help website owners to understand how visitors interact with websites by collecting and reporting information anonymously.';
$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_COOKIE_CATEGORY_4_NAME'] = 'Marketing';
$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_COOKIE_CATEGORY_4_DESCRIPTION'] = 'Marketing cookies are used to track visitors across websites. The intention is to display ads that are relevant and engaging for the individual user and thereby more valuable for publishers and third party advertisers.';

$_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_CATEGORY_TOOLTIP'] = '<p><b>Necessary</b>: Necessary cookies help make a website usable by enabling basic functions like page navigation and access to secure areas of the website. The website cannot function properly without these cookies.</p><p><b>Preferences</b>: Preference cookies enable a website to remember information that changes the way the website behaves or looks, like your preferred language or the region that you are in.</p><p><b>Statistics</b>: Statistic cookies help website owners to understand how visitors interact with websites by collecting and reporting information anonymously.</p><p><b>Marketing</b>: Marketing cookies are used to track visitors across websites. The intention is to display ads that are relevant and engaging for the individual user and thereby more valuable for publishers and third party advertisers.</p>';

$_ARRAYLANG['TXT_CORE_MODULE_PRIVACY_SYSCOOKIE_CLXCOOKIENOTE_NAME'] = 'Privacy note';
$_ARRAYLANG['TXT_CORE_MODULE_PRIVACY_SYSCOOKIE_CLXCOOKIENOTE_DESCRIPTION'] = 'Save settings of the privacy note.';
