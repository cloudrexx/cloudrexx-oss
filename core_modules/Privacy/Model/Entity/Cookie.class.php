<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * Class Cookie
 *
 * @copyright   CLOUDREXX CMS - Cloudrexx AG Thun
 * @author      Michael Ritter <michael.ritter@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  coremodules_privacy
 */

namespace Cx\Core_Modules\Privacy\Model\Entity;

/**
 * Class Cookie
 *
 * @copyright   CLOUDREXX CMS - Cloudrexx AG Thun
 * @author      Michael Ritter <michael.ritter@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  coremodules_privacy
 */
class Cookie extends \Cx\Model\Base\EntityBase implements \JsonSerializable, \Gedmo\Translatable\Translatable {
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var integer
     */
    protected $ord;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $identifier;

    /**
     * @var string
     */
    protected $component;

    /**
     * @var integer
     */
    protected $category;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var boolean
     */
    protected $active;

    /**
     * @var array
     */
    protected $cookieNames;

    /**
     * @var string
     */
    protected $host;

    /**
     * @var string
     */
    protected $privacyStatementUrl;

    /**
     * @var string
     */
    protected $locale;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Cookie
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set identifier
     *
     * @param string $identifier
     * @return Cookie
     */
    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;
    }

    /**
     * Get identifier
     *
     * @return string 
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * Set component
     *
     * @param string $component
     * @deprecated This should be solved differently, see schema file
     * @return Component name
     */
    public function setComponent($component)
    {
        $this->component = $component;
    }

    /**
     * Get component
     * @deprecated This should be solved differently, see schema file
     * @return string 
     */
    public function getComponent()
    {
        return $this->component;
    }

    /**
     * Set category
     *
     * @param integer $category
     * @return Cookie
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * Get category
     *
     * @return integer 
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Cookie
     */
    public function setDescription($description)
    {
        $this->description = $description;

    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Cookie
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @see getActive()
     */
    public function isActive() {
        return $this->getActive();
    }

    /**
     * Set cookieNames
     *
     * @param array $cookieNames
     * @return Cookie
     */
    public function setCookieNames($cookieNames)
    {
        $this->cookieNames = $cookieNames;
    }

    /**
     * Get cookieNames
     *
     * @return array 
     */
    public function getCookieNames()
    {
        return $this->cookieNames;
    }

    /**
     * Set host
     *
     * @param string $host
     * @return Cookie
     */
    public function setHost($host)
    {
        $this->host = $host;
    }

    /**
     * Get host
     *
     * @return string 
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * Set privacyStatementUrl
     *
     * @param string $privacyStatementUrl
     * @return Cookie
     */
    public function setPrivacyStatementUrl($privacyStatementUrl)
    {
        $this->privacyStatementUrl = $privacyStatementUrl;
    }

    /**
     * Get privacyStatementUrl
     *
     * @return string 
     */
    public function getPrivacyStatementUrl()
    {
        if (empty($this->privacyStatementUrl)) {
            return '';
        }
        return \Cx\Core\Routing\Url::fromMagic($this->privacyStatementUrl)->toString();
    }

    /**
     * Set ord
     *
     * @param int $ord
     */
    public function setOrd($ord)
    {
        $this->ord = $ord;
    }

    /**
     * Get ord
     *
     * @return int
     */
    public function getOrd()
    {
        return $this->ord;
    }

    #[\ReturnTypeWillChange]
    public function jsonSerialize() {
        $name = $this->getName();
        $description = $this->getDescription();
        if (!empty($this->getComponent())) {
            $lang = \Env::get('init')->getComponentSpecificLanguageData(
                $this->getComponent()
            );
            $cookieTextPrefix = strtoupper(
                'TXT_' . parent::getComponent($this->getComponent())->getType()
                    . '_' . $this->getComponent() . '_SYSCOOKIE_'
                    . $this->getIdentifier()
            );
            if (empty($this->getName())) {
                $name = $lang[$cookieTextPrefix . '_NAME'];
            }
            if (empty($this->getDescription())) {
                $description = $lang[$cookieTextPrefix . '_DESCRIPTION'];
            }
        }
        return array(
            'id' => $this->getId(),
            'order' => $this->getOrd(),
            'name' => $name,
            'identifier' => $this->getIdentifier(),
            'description' => $description,
            'disableable' => $this->getCategory() != 1,
            'cookieNames' => $this->getCookieNames(),
            'host' => $this->getHost(),
            'privacyStatementUrl' => $this->getPrivacyStatementUrl(),
        );
    }

    public function setTranslatableLocale($locale) {
        $this->locale = $locale;
    }
}
