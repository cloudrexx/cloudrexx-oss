<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * Main controller for Privacy
 *
 * @copyright   Cloudrexx AG
 * @author      Michael Ritter <michael.ritter@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  coremodule_privacy
 */

namespace Cx\Core_Modules\Privacy\Controller;

/**
 * Main controller for Privacy
 *
 * @copyright   Cloudrexx AG
 * @author      Michael Ritter <michael.ritter@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  coremodule_privacy
 */
class ComponentController extends \Cx\Core\Core\Model\Entity\SystemComponentController {

    /**
     * {@inheritdoc}
     */
    public function getControllerClasses() {
        // Return an empty array here to let the component handler know that there
        // does not exist a backend, nor a frontend controller of this component.
        return array('EsiWidget', 'Backend');
    }

    /**
     * {@inheritdoc}
     */
    public function getControllersAccessableByJson() {
        return array('EsiWidgetController');
    }

    /**
     * {@inheritdoc}
     */
    public function postInit(\Cx\Core\Core\Controller\Cx $cx)
    {
        $widgetController = $this->getComponent('Widget');
        $widget = new \Cx\Core_Modules\Widget\Model\Entity\EsiWidget(
            $this,
            'COOKIE_NOTE'
        );
        $widget->setEsiVariables(
            \Cx\Core_Modules\Widget\Model\Entity\EsiWidget::ESI_VAR_ID_LOCALE
        );
        $widgetController->registerWidget($widget);
    }

    /**
     * Registers event listeners
     */
    public function registerEventListeners() {
        $evm = $this->cx->getEvents();
        $cookieListener = new \Cx\Core_Modules\Privacy\Model\Event\CookieEventListener();
        $evm->addModelListener(\Doctrine\ORM\Events::onFlush, 'Cx\\Core_Modules\\Privacy\\Model\\Entity\\Cookie', $cookieListener);
        $evm->addModelListener(\Doctrine\ORM\Events::postFlush, 'Cx\\Core_Modules\\Privacy\\Model\\Entity\\Cookie', $cookieListener);
    }

    /**
     * {@inheritdoc}
     */
    public function postContentLoad(\Cx\Core\ContentManager\Model\Entity\Page $page) {
        if (
            $page->getModule() == $this->getName() &&
            $page->getCmd() == 'Settings' &&
            $this->cx->getRequest()->getHttpRequestMethod() == 'post'
        ) {
            \Cx\Core\Core\Controller\Cx::instanciate()->getEvents()->triggerEvent(
                'clearEsiCache',
                array(
                    'Widget',
                    array(
                        'COOKIE_NOTE',
                    ),
                )
            );
        }

        if ($this->cx->getMode() != \Cx\Core\Core\Controller\Cx::MODE_FRONTEND) {
            return;
        }
        \Cx\Core\Setting\Controller\Setting::init($this->getName(), 'cookieNote', 'FileSystem');
        if (
            \Cx\Core\Setting\Controller\Setting::getValue(
                'cookieNoteType',
                $this->getName()
            ) == 'off'
        ) {
            return;
        }
        \JS::registerCode(
            'var cookieNoteTtl = "' .
            \Cx\Core\Setting\Controller\Setting::getValue(
                'cookieNoteTtl',
                $this->getName()
            ) . '";'
        );
        \JS::registerCSS(substr($this->getDirectory(false, true) . '/View/Style/Frontend.css', 1));
        \JS::registerJS(substr($this->getDirectory(false, true) . '/View/Script/Frontend.js', 1));

        $em = $this->cx->getDb()->getEntityManager();
        $cookieRepo = $em->getRepository($this->getNamespace() . '\Model\Entity\Cookie');
        $cookies = array();
        foreach ($cookieRepo->findBy(array('active' => 1)) as $cookie) {
            if (!isset($cookies[$cookie->getCategory()])) {
                $cookies[$cookie->getCategory()] = array();
            }
            $cookies[$cookie->getCategory()][$cookie->getId()] = $cookie;
        }

        \ContrexxJavascript::getInstance()->setVariable(
            'cookieNoteTtl',
            \Cx\Core\Setting\Controller\Setting::getValue(
                'cookieNoteTtl',
                $this->getName()
            ),
            $this->getName()
        );
        \ContrexxJavascript::getInstance()->setVariable(
            'cookies',
            $cookies,
            $this->getName()
        );
        \ContrexxJavascript::getInstance()->setVariable(
            'cookieCategories',
            $this->getCookieCategories(),
            $this->getName()
        );
        \ContrexxJavascript::getInstance()->setVariable(
            'cookieCategoryDescriptions',
            $this->getCookieCategoryDescriptions(),
            $this->getName()
        );
        \JS::registerJS(
            substr($this->getDirectory(false, true) . '/View/Script/CookiesLibrary.js', 1)
        );
        $cookieNoteType = \Cx\Core\Setting\Controller\Setting::getValue(
            'cookieNoteType',
            $this->getName()
        );
        $templateSuffix = $cookieNoteType;
        if ($cookieNoteType == 'on') {
            $templateSuffix = '';
        }
        \JS::registerJS(
            substr(
                $this->getDirectory(
                    false,
                    true
                ) . '/View/Script/PrivacyNote' . $templateSuffix . '.js',
                1
            )
        );
        \JS::registerCSS(
            substr(
                $this->getDirectory(
                    false,
                    true
                ) . '/View/Style/PrivacyNote' . $templateSuffix . '.css',
                1
            )
        );

        /**
         * this is a workaround as ESI parsing does not seem to work
         * as expected on live, except for sites daeppen and ritt0r
         * for unknown reasons.
         */
        $widgetTemplate = new \Cx\Core\Html\Sigma();
        $widgetTemplate->setTemplate('{COOKIE_NOTE}');
        $this->getComponent('Widget')->getWidget('COOKIE_NOTE')->parse(
            $widgetTemplate,
            null,
            'View',
            'Theme',
            \Env::get('init')->getCurrentThemeId()
        );
        $widgetContent = $widgetTemplate->get();
        //\LinkGenerator::parseTemplate($widgetContent);
        $this->cx->getTemplate()->_blocks['__global__'] = preg_replace(
            '#</body[^>]*>#',
            $widgetContent . '\\0',
            //'<span>{COOKIE_NOTE}</span>\\0',
            $this->cx->getTemplate()->_blocks['__global__']
        );
    }

    /**
     * Returns a list of category names in the current Locale
     * @return array List of category names indexed by their ID
     */
    public function getCookieCategories() {
        global $_ARRAYLANG;

        if (!isset($_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_COOKIE_CATEGORY_1_NAME'])) {
            $_ARRAYLANG += \Env::get('init')->getComponentSpecificLanguageData($this->getName());
        }

        return array(
            1 => $_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_COOKIE_CATEGORY_1_NAME'],
            2 => $_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_COOKIE_CATEGORY_2_NAME'],
            3 => $_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_COOKIE_CATEGORY_3_NAME'],
            4 => $_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_COOKIE_CATEGORY_4_NAME'],
        );
    }

    /**
     * Returns a list of category descriptions in the current Locale
     * @return array List of category descriptions indexed by their ID
     */
    public function getCookieCategoryDescriptions() {
        global $_ARRAYLANG;

        if (!isset($_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_COOKIE_CATEGORY_1_DESCRIPTION'])) {
            $_ARRAYLANG += \Env::get('init')->getComponentSpecificLanguageData($this->getName());
        }

        return array(
            1 => $_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_COOKIE_CATEGORY_1_DESCRIPTION'],
            2 => $_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_COOKIE_CATEGORY_2_DESCRIPTION'],
            3 => $_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_COOKIE_CATEGORY_3_DESCRIPTION'],
            4 => $_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_COOKIE_CATEGORY_4_DESCRIPTION'],
        );
    }
}
