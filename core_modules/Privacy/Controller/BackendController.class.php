<?php declare(strict_types=1);

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * Backend controller for privacy component
 *
 * @copyright   Cloudrexx AG
 * @author      Michael Ritter <michael.ritter@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  coremodules_privacy
 */

namespace Cx\Core_Modules\Privacy\Controller;


/**
 * Backend controller for privacy component
 *
 * @copyright   Cloudrexx AG
 * @author      Michael Ritter <michael.ritter@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  coremodules_privacy
 */
class BackendController extends \Cx\Core\Core\Model\Entity\SystemComponentBackendController {

    /**
     * @inheritdoc
     */
    public function getCommands() {
        $cmds = parent::getCommands();
        $cmds['Settings'] = array();
        return $cmds;
    }

    /**
     * @inheritdoc
     */
    public function getPage(\Cx\Core\ContentManager\Model\Entity\Page $page) {
        if (!empty($_POST)) {
            // This needs to drop the cache of:
            // - ESI widget COOKIE_NOTE
            // - cx.variables. As cx.variables is not cached separately (yet)
            //   we need to drop the whole page cache for now.
            // TODO: Drop cache more selectively as soon as this is possible
            $this->getComponent('Cache')->clearCache();
        }
        parent::getPage($page);
    }

    /**
     * @inheritdoc
     */
    protected function getViewGeneratorOptions($entityClassName, $dataSetIdentifier = '') {
        global $_ARRAYLANG;

        $options = parent::getViewGeneratorOptions($entityClassName, $dataSetIdentifier);

        switch ($entityClassName) {
            case 'Cx\Core_Modules\Privacy\Model\Entity\Cookie':
                $options = array(
                    'info' => $_ARRAYLANG['TXT_CORE_MODULE_PRIVACY_OVERVIEW_TEXT'],
                    'header' => $_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_OVERVIEW_HEADER'],
                    'entityName' => $_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_COOKIE_ENTITY_NAME'],
                    'order' => array(
                        'overview' => array(
                            'active',
                        ),
                        'form' => array(
                            'active',
                            'category',
                            'identifier',
                            'name',
                            'description',
                        ),
                    ),
                    'rowAttributes' => function($data, $attributes) {
                        if (!empty($data['component'])) {
                            if (!isset($attributes['class'])) {
                                $attributes['class'] = '';
                            }
                            $attributes['class'] .= ' disabled';
                        }
                        return $attributes;
                    },
                    'fields' => array(
                        'id' => array(
                            'showOverview' => false,
                            'showDetail' => false,
                        ),
                        'ord' => array(
                            'showOverview' => false,
                            'showDetail' => false,
                        ),
                        'component' => array(
                            'showOverview' => false,
                            'showDetail' => true,
                            'type' => 'hidden',
                        ),
                        'category' => array(
                            'type' => 'select',
                            'validValues' => $this->getCookieCategories(),
                            'tooltip' => $_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_CATEGORY_TOOLTIP'],
                        ),
                        'identifier' => array(
                            'tooltip' => $_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_IDENTIFIER_TOOLTIP'],
                            'validValues' => '^[a-zA-Z0-9-_]+$',
                            'table' => array(
                                'parse' => function($value, $rowData, $fieldOptions, $viewGeneratorId) {
                                    return '<code>' . $value . '</code>';
                                },
                            ),
                        ),
                        'active' => array(
                            'formtext' => $_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_EDIT_ACTIVE'],
                            'tooltip' => $_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_ACTIVE_TOOLTIP'],
                        ),
                        'cookieNames' => array(
                            'showOverview' => false,
                            'type' => 'string',
                            'valueCallback' => function($fieldvalue) {
                                if (empty($fieldvalue)) {
                                    return '';
                                }
                                return implode(', ', $fieldvalue);
                            },
                            'storecallback' => function($value, $fieldName, $entity, $entityData) {
                                return serialize(array_map('trim', explode(',', $value)));
                            },
                            'tooltip' => $_ARRAYLANG['TXT_CORE_MODULE_PRIVACY_COOKIE_NAMES_TOOLTIP'],
                        ),
                        'host' => array(
                            'showOverview' => false,
                            'tooltip' => $_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_HOST_TOOLTIP'],
                        ),
                        'privacyStatementUrl' => array(
                            'attributes' => array('class' => 'translatable form-control'),
                            'showOverview' => false,
                        ),
                        'name' => array(
                            'attributes' => array('class' => 'translatable form-control'),
                            'valueCallback' => function($fieldvalue, $fieldname, $rowData, $fieldoption, $viewGenerator) {
                                // if we have a field value it takes precedence
                                if (
                                    empty($fieldvalue) &&
                                    !empty($rowData['component'])
                                ) {
                                    $cookieComponent = $rowData['component'];
                                    $lang = \Env::get('init')->getComponentSpecificLanguageData(
                                        $cookieComponent
                                    );
                                    $cookieTextPrefix = strtoupper(
                                        'TXT_' . $this->getComponent($cookieComponent)->getType()
                                        . '_' . $cookieComponent . '_SYSCOOKIE_'
                                        . $rowData['identifier']
                                    );
                                    return $lang[$cookieTextPrefix . '_NAME'];
                                }
                                return $fieldvalue;
                            },
                        ),
                        'description' => array(
                            'attributes' => array('class' => 'translatable form-control'),
                            'type' => 'text',
                            'valueCallback' => function($fieldvalue, $fieldname, $rowData, $fieldoption, $viewGenerator) {
                                // if we have a field value it takes precedence
                                if (
                                    empty($fieldvalue) &&
                                    !empty($rowData['component'])
                                ) {
                                    $cookieComponent = $rowData['component'];
                                    $lang = \Env::get('init')->getComponentSpecificLanguageData(
                                        $cookieComponent
                                    );
                                    $cookieTextPrefix = strtoupper(
                                        'TXT_' . $this->getComponent($cookieComponent)->getType()
                                        . '_' . $cookieComponent . '_SYSCOOKIE_'
                                        . $rowData['identifier']
                                    );
                                    return $lang[$cookieTextPrefix . '_DESCRIPTION'];
                                }
                                return $fieldvalue;
                            },
                        ),
                    ),
                    'functions' => array(
                        'status' => array(
                            'field' => 'active',
                        ),
                        'sortBy' => array(
                            'field' => array(
                                'ord' => SORT_ASC,
                            ),
                        ),
                        'order' => array(
                            'ord' => SORT_ASC,
                        ),
                        'add' => true,
                        'edit' => true,
                        'delete' => true,
                    ),
                );
                break;
        }
        return $options;
    }

    /**
     * @inheritdoc
     */
    protected function showOverviewPage() {
        return false;
    }

    /**
     * Callback for setting cookieNoteType
     * @return array List of dropdown options
     */
    public static function getCookieNoteTypeSelectionOptions() {
        global $_ARRAYLANG;

        return array(
            'off' => $_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_SETTINGS_COOKIENOTETYPE_OFF'],
            'on' => $_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_SETTINGS_COOKIENOTETYPE_ON'],
            '2022' => $_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_SETTINGS_COOKIENOTETYPE_2022'],
        );
    }

    /**
     * Callback for setting cookieNoteTtl
     * @return array List of dropdown options
     */
    public static function getCookieNoteTtlSelectionOptions() {
        global $_ARRAYLANG;

        return array(
            'session' => $_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_SETTINGS_COOKIENOTETTL_SESSION'],
            'week' => $_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_SETTINGS_COOKIENOTETTL_WEEK'],
            'month' => $_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_SETTINGS_COOKIENOTETTL_MONTH'],
            'year' => $_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_SETTINGS_COOKIENOTETTL_YEAR'],
            'unlimited' => $_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_SETTINGS_COOKIENOTETTL_UNLIMITED'],
        );
    }
}
