<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * Class EsiWidgetController
 *
 * @copyright   CLOUDREXX CMS - Cloudrexx AG Thun
 * @author      Michael Ritter <michael.ritter@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  coremodules_privacy
 */

namespace Cx\Core_Modules\Privacy\Controller;

/**
 * JsonAdapter Controller to handle EsiWidgets
 * Usage:
 * - Create a subclass that implements parseWidget()
 * - Register it as a Controller in your ComponentController
 *
 * @copyright   CLOUDREXX CMS - Cloudrexx AG Thun
 * @author      Michael Ritter <michael.ritter@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  coremodules_privacy
 */

class EsiWidgetController extends \Cx\Core_Modules\Widget\Controller\EsiWidgetController {

    /**
     * Parses a widget
     * @param string $name Widget name
     * @param \Cx\Core\Html\Sigma Widget template
     * @param \Cx\Core\Routing\Model\Entity\Response $response Current response
     * @param array $params Array of params
     */
    public function parseWidget($name, $template, $response, $params) {
        if ($name === 'COOKIE_NOTE') {
            \Cx\Core\Setting\Controller\Setting::init('Privacy', 'cookieNote', 'FileSystem');
            $cookieNoteType = \Cx\Core\Setting\Controller\Setting::getValue(
                'cookieNoteType',
                'Privacy'
            );
            $template->setRoot($this->getDirectory(false) . '/View/Template/Frontend');
            $templateFilename = 'PrivacyNote' . $cookieNoteType;
            if ($cookieNoteType == 'on') {
                $templateFilename = 'CookieNote';
            }
            // BEGIN: patch for api mode
            //      this is only required on live
            //      this is not required in CLX-1707 as widgets
            //      will always be parsed in frontedn mode
            if ($params['theme']) {
                $templateFilenameFromTheme = $this->cx->getClassLoader()->getFileFromTheme(
                    $this->getDirectory(false, true) . '/View/Template/Frontend/' . $templateFilename . '.html',
                    false,
                    $params['theme']
                );
                if ($templateFilenameFromTheme !== false) {
                    $template->setRoot('');
                    $templateFilename = substr($templateFilenameFromTheme, 0, -5);
                }
            }
            // END: patch for api mode
            $template->loadTemplateFile($templateFilename . '.html');
            foreach ($template->_blocks as &$block) {
                \LinkGenerator::parseTemplate($block);
            }
            $template->setGlobalVariable(
                // TODO: drop argument $params['locale']->getId() as it
                // is just a workaround until initialization of locale
                // (see CLX-1707) has been implemented/fixed
                \Env::get('init')->getComponentSpecificLanguageData(
                    $this->getSystemComponentController()->getName(),
                    true,
                    $params['locale']->getId()
                )
            );
            $em = $this->cx->getDb()->getEntityManager();
            $cookieRepo = $em->getRepository($this->getNamespace() . '\Model\Entity\Cookie');
            foreach ($this->getCookieCategories() as $categoryId=>$categoryName) {
                $cookies = $cookieRepo->findBy(
                    array(
                        'active' => true,
                        'category' => $categoryId
                    ),
                    array(
                        'ord' => 'ASC',
                    )
                );
                if (!count($cookies)) {
                    continue;
                }
                $template->setVariable(contrexx_raw2xhtml(array(
                    'COOKIE_CATEGORY_ID' => $categoryId,
                    'COOKIE_CATEGORY_NAME' => $categoryName,
                    'COOKIE_CATEGORY_DESCRIPTION' => $this->getCookieCategoryDescriptions()[$categoryId],
                )));
                foreach ($cookies as $cookie) {
                    $name = $cookie->getName();
                    $description = $cookie->getDescription();
                    if (!empty($cookie->getComponent())) {
                        // TODO: drop argument $params['locale']->getId() as it
                        // is just a workaround until initialization of locale
                        // (see CLX-1707) has been implemented/fixed
                        $lang = \Env::get('init')->getComponentSpecificLanguageData(
                            $cookie->getComponent(),
                            true,
                            $params['locale']->getId()
                        );
                        $cookieTextPrefix = strtoupper(
                            'TXT_' . $this->getComponent($cookie->getComponent())->getType()
                                . '_' . $cookie->getComponent() . '_SYSCOOKIE_'
                                . $cookie->getIdentifier()
                        );
                        if (empty($name)) {
                            $name = $lang[$cookieTextPrefix . '_NAME'];
                        }
                        if (empty($description)) {
                            $description = $lang[$cookieTextPrefix . '_DESCRIPTION'];
                        }
                    }
                    $template->setVariable(contrexx_raw2xhtml(array(
                        'COOKIE_ID' => $cookie->getId(),
                        'COOKIE_IDENTIFIER' => $cookie->getIdentifier(),
                        'COOKIE_NAME' => $name,
                        'COOKIE_COOKIE_NAMES' => implode(', ', $cookie->getCookieNames()),
                        'COOKIE_HOST' => $cookie->getHost(),
                        'COOKIE_PRIVACY_STATEMENT_URL' => $cookie->getPrivacyStatementUrl(),
                    )));
                    $template->setVariable(array(
                        'COOKIE_DESCRIPTION' => nl2br(contrexx_raw2xhtml($description)),
                    ));
                    if (count($cookie->getCookieNames())) {
                        $template->touchBlock('cookie_names');
                    } else {
                        $template->hideBlock('cookie_names');
                    }
                    if (!empty($cookie->getHost())) {
                        $template->touchBlock('cookie_host');
                    } else {
                        $template->hideBlock('cookie_host');
                    }
                    if (!empty($cookie->getPrivacyStatementUrl())) {
                        $template->touchBlock('cookie_privacystatement');
                    } else {
                        $template->hideBlock('cookie_privacystatement');
                    }
                    if ($cookie->getCategory() != 1) {
                        $template->touchBlock('cookie_disableable');
                    } else {
                        $template->hideBlock('cookie_disableable');
                    }
                    $template->parse('cookie');
                }
                $template->parse('cookie_category');
            }
            return;
        }
    }
}
