<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * Captcha
 *
 * @copyright   CLOUDREXX CMS - CLOUDREXX AG
 * @author      CLOUDREXX Development Team <info@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  coremodule_captcha
 */

namespace Cx\Core_Modules\Captcha\Controller;

/**
 * Captcha
 *
 * @copyright   CLOUDREXX CMS - CLOUDREXX AG
 * @author      CLOUDREXX Development Team <info@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  coremodule_captcha
 */
class Captcha {
    /**
     * @var Captcha Singleton instance of this class
     */
    protected static $instance = null;

    /**
     * @var CaptchaInterface Instance of the captcha method in use
     */
    protected $objCaptcha;

    /**
     * @var string Captcha method in use
     */
    protected string $method;

    /**
     * Creates the singleton instance of this class
     * @param $config
     */
    protected function __construct($config) {
        // explicitly load setting options from group 'security'
        \Cx\Core\Setting\Controller\Setting::init('Config', 'security');
        $this->method = \Cx\Core\Setting\Controller\Setting::getValue(
            'captchaMethod',
            'Config'
        );
        switch ($this->method) {
            case 'hCaptcha':
                $this->objCaptcha = new hCaptcha();
                break;

            case 'reCaptcha':
                $this->objCaptcha = new ReCaptcha();
                break;

            case 'contrexxCaptcha':
            default:
                $this->objCaptcha = new ContrexxCaptcha($config);
                break;
        }
    }

    #[\ReturnTypeWillChange]
    public function __call($name, $args) {
        return call_user_func_array(array($this->objCaptcha, $name), $args);
    }

    /**
     * Returns the name of the captcha method in use
     * @return string One of "contrexxCaptcha", "reCaptcha" or "hCaptcha"
     */
    public function getMethodName(): string {
        return $this->method;
    }

    /**
     * Returns a singleton instance of this class
     * @return Captcha Singleton instance of this class
     */
    public static function getInstance() {
        if (!isset(static::$instance)) {
            static::$instance = new static(\Env::get('config'));
        }
        return static::$instance;
    }
}
