<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * hCaptcha
 *
 * @copyright   CLOUDREXX CMS - CLOUDREXX AG
 * @author      Thomas Wirz <thomas.wirz@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  coremodule_captcha
 */

namespace Cx\Core_Modules\Captcha\Controller;

/**
 * hCaptcha
 *
 * @copyright   CLOUDREXX CMS - CLOUDREXX AG
 * @author      Thomas Wirz <thomas.wirz@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  coremodule_captcha
 */
class hCaptcha implements CaptchaInterface {
    /**
     * @var string
     */
    protected $site_key;

    /**
     * @var string
     */
    protected $secret_key;

    /**
     * Stores status if CAPTCHA has been successfully validated or not
     * @var boolean
     */
    protected $securityCheck;

    public function __construct() {
        \Cx\Core\Setting\Controller\Setting::init('Config', 'security');
        $this->site_key   = \Cx\Core\Setting\Controller\Setting::getValue('hCaptchaSiteKey', 'Config');
        $this->secret_key = \Cx\Core\Setting\Controller\Setting::getValue('hCaptchaSecretKey', 'Config');
    }

    /**
     * Get captcha code
     *
     * @param integer $tabIndex
     * @return string
     */
    public function getCode($tabIndex = null) {
        \DBG::log('CAPTCHA: generate (hCaptcha)');
        $tabIndexAttr = '';
        if (isset($tabIndex)) {
            $tabIndexAttr = "data-tabindex=\"$tabIndex\"";
        }

        $cx = \Cx\Core\Core\Controller\Cx::instanciate();
        $cx->getComponent('Security')->registerTrustedCspSource('frame-src', 'https://hcaptcha.com');
        $cx->getComponent('Security')->registerTrustedCspSource('frame-src', 'https://*.hcaptcha.com');
        $cx->getComponent('Security')->registerTrustedCspSource('style-src', 'https://hcaptcha.com');
        $cx->getComponent('Security')->registerTrustedCspSource('style-src', 'https://*.hcaptcha.com');
        $cx->getComponent('Security')->registerTrustedCspSource('connect-src', 'https://hcaptcha.com');
        $cx->getComponent('Security')->registerTrustedCspSource('connect-src', 'https://*.hcaptcha.com');

        $lang   = \FWLanguage::getLanguageCodeById(FRONTEND_LANG_ID);
        $code   = <<<HTML
<div id="captcha" class="h-captcha" data-sitekey="{$this->site_key}" $tabIndexAttr></div>
<script>
function _clxHCaptchaInit() {
    if (typeof(clxHCaptchaInit) != 'undefined') {
        return clxHCaptchaInit();
    }
    hcaptcha.render('captcha');
};
</script>
<script type="text/javascript" src="https://js.hcaptcha.com/1/api.js?hl=$lang&onload=_clxHCaptchaInit&render=explicit" async defer></script>
HTML;
        \JS::markSafeInlineJavaScripts($code);
        return $code;
    }

    /**
     * Get captcha validation code
     *
     * @return string Returns JS code
     */
    public function getJSValidationFn() {
        $captchaValidationCode = <<<JSCaptchaValidation
        if (\$J('#captcha').length) {
            var response = hcaptcha.getResponse();
            if (response.length == 0) {
                isCaptchaOk = false;
            }
        }
JSCaptchaValidation;
        return $captchaValidationCode;
    }

    /**
     * Check the captcha code
     *
     * @return boolean
     */
    public function check() {
        if (isset($this->securityCheck)) {
            return $this->securityCheck;
        }
        if (empty($_POST['h-captcha-response'])) {
            return false;
        }
        $this->securityCheck = $this->verify(
            $_POST['h-captcha-response']
        );
        if ($this->securityCheck) {
            \DBG::log('CAPTCHA: successful (hCaptcha)');
        } else {
            \DBG::log('CAPTCHA: failed (hCaptcha)');
        }
        return $this->securityCheck;
    }

    /**
     * @param   string $response The hCaptcha response acquired by the user.
     * @return boolean
     */
    protected function verify($response): bool {
        try {
            $request = new \HTTP_Request2(
                'https://hcaptcha.com/siteverify',
                \HTTP_Request2::METHOD_POST
            );
            $request->addPostParameter(array(
                'secret' => $this->secret_key,
                'response' => $response,
            ));
            $verifyResponse = $request->send();
            if ($verifyResponse->getStatus() != 200) {
                return false;
            }
            $responseData = json_decode(
                $verifyResponse->getBody()
            );
            if (!$responseData) {
                return false;
            }
            return (bool) $responseData->success;
        } catch (\HTTP_Request2_Exception $e) {
            \DBG::log('Verifying hCaptcha failed!');
            \DBG::log($e->getMessage());
            \DBG::dump($request, DBG_DEBUG);
            return false;
        }
    }

    /**
     * Disable the captcha check
     */
    public function disable() {
        $this->securityCheck = true;
    }
}
