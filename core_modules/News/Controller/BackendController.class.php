<?php declare(strict_types=1);

/**
 * Cloudrexx
 *
 * @link      https://www.cloudrexx.com
 * @copyright Cloudrexx AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

namespace Cx\Core_Modules\News\Controller;

/**
 * @copyright   Cloudrexx AG
 * @author      Michael Ritter <michael.ritter@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  coremodule_news
 */
class BackendController extends \Cx\Core\Core\Model\Entity\SystemComponentBackendController {

    /**
     * {@inheritdoc}
     */
    protected function showOverviewPage() {
        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function getCommands() {
        $this->defaultPermission = new \Cx\Core_Modules\Access\Model\Entity\Permission(
            array('http', 'https'),
            array('get', 'post'),
            true,
            array(),
            array(178),
        );
        $cmds = array('', 'category');
        $query = '
            SELECT
                `value`
            FROM
                `' . DBPREFIX . 'module_news_settings`
            WHERE
                `name` LIKE "news_use_types"
        ';
        $objResult = $this->cx->getDb()->getAdoDb()->Execute($query);
        if ($objResult && $objResult->fields && $objResult->fields['value']) {
            $cmds[] = 'newstype';
        }
        $cmds[] = 'settings';
        return $cmds;
    }

    public function getPage(\Cx\Core\ContentManager\Model\Entity\Page $page) {
        global $_CORELANG, $subMenuTitle, $objTemplate;

        $this->cx->getTemplate()->addBlockfile('CONTENT_OUTPUT', 'content_master', 'ContentMaster.html');
        $objTemplate = $this->cx->getTemplate();

        \Permission::checkAccess(10, 'static');
        $subMenuTitle = $_CORELANG['TXT_NEWS_MANAGER'];
        $objNews      = new NewsManager();
        $objNews->getPage();
    }
}
