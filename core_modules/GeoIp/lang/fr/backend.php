<?php
declare(strict_types=1);
/**
 * Cloudrexx
 *
 * @link      https://www.cloudrexx.com
 * @copyright Cloudrexx AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

// Let's start with module info:
$_ARRAYLANG['TXT_CORE_MODULE_GEOIP']                = 'Services basés sur la localisation';
$_ARRAYLANG['TXT_CORE_MODULE_GEOIP_DESCRIPTION']    = 'Services de fourniture de contenu géolocalisé (IP GeoLocation).';

// Here come the ACTs:
$_ARRAYLANG['TXT_CORE_MODULE_GEOIP_ACT_DEFAULT']    = 'Aperçu';

$_ARRAYLANG['TXT_CORE_MODULE_GEOIP_SERVICESTATUS']  = 'Statut du service GeoIP';
$_ARRAYLANG['TXT_CORE_MODULE_GEOIP_SETTINGS']       = 'Paramètres';
