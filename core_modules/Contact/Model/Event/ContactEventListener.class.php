<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * Class ContactEventListener
 *
 * @copyright   CLOUDREXX CMS - Cloudrexx AG Thun
 * @author      Robin Glauser <robin.glauser@comvation.com>
 * @package     cloudrexx
 * @subpackage  coremodule_contact
 * @version     1.0.0
 */

namespace Cx\Core_Modules\Contact\Model\Event;

use Cx\Core\Core\Controller\Cx;
use Cx\Core\Event\Model\Entity\EventListener;
use Cx\Core\MediaSource\Model\Entity\MediaSourceManager;
use Cx\Core\MediaSource\Model\Entity\MediaSource;
use Cx\Core\Event\Model\Entity\DefaultEventListener;

/**
 * Class ContactEventListener
 *
 * @copyright   CLOUDREXX CMS - Cloudrexx AG Thun
 * @author      Robin Glauser <robin.glauser@comvation.com>
 * @package     cloudrexx
 * @subpackage  coremodule_contact
 * @version     1.0.0
 */
class ContactEventListener extends DefaultEventListener  {

    /**
     * @param MediaSourceManager $mediaBrowserConfiguration
     */
    public function mediasourceLoad(MediaSourceManager $mediaBrowserConfiguration)
    {
        global $_ARRAYLANG;
        \Env::get('init')->loadLanguageData('Contact');
        $mediaType = new MediaSource(
            'attach',
            // hotfix as CLX-1045 is not yet live
            $_ARRAYLANG['TXT_CONTACT_UPLOADS'] ?? '',
            array(
                $this->cx->getWebsiteImagesAttachPath(),
                $this->cx->getWebsiteImagesAttachWebPath(),
            ),
            array(
                'read' => array(),
                'write' => array(
                    'any' => array(
                        84, // Forms
                        85, // Settings
                    ),
                ),
            )
        );
        $mediaBrowserConfiguration->addMediaType($mediaType);
    }

    /**
     * @param array $config Config from event NetManager:getSenderDomains
     */
    public function NetManagerGetSenderDomains(array $config = []): array {
        $senderDomains = [];
        $reportUsages = $config['usages'] ?? false;
        if ($reportUsages) {
            $query = '
                SELECT
                    `sender_email`,
                    GROUP_CONCAT(`id`) AS `usages`
                FROM `' . DBPREFIX . 'module_contact_form`
                GROUP BY `sender_email`
            ';
        } else {
            $query = '
                SELECT
                    DISTINCT `sender_email`
                    FROM `' . DBPREFIX . 'module_contact_form`
            ';
        }
        $result = $this->cx->getDb()->getAdoDb()->Execute($query);
        if (!$result || !$result->RecordCount()) {
            return $senderDomains;
        }
        while (!$result->EOF) {
            $domain = $this->getDomainFromEmail($result->fields['sender_email']);
            $data = [
                'strict' => false,
            ];
            if ($reportUsages) {
                $data['usages'] = explode(',', $result->fields['usages']);
            }
            $senderDomains[$domain] = $data;
            $result->MoveNext();
        }
        if (!$reportUsages) {
            return $senderDomains;
        }
        $contactLib = new \Cx\Core_Modules\Contact\Controller\ContactLib();
        $contactLib->initContactForms();
        foreach ($senderDomains as &$senderDomain) {
            foreach ($senderDomain['usages'] as $idx => $formId) {
                $form = $contactLib->arrForms[$formId];
                if (isset($form['lang'][FRONTEND_LANG_ID])) {
                    $langId = FRONTEND_LANG_ID;
                } elseif (isset($form['lang'][\FWLanguage::getDefaultLangId()])) {
                    $langId = \FWLanguage::getDefaultLangId();
                } else {
                    $langId = key($form['lang']);
                }
                $senderDomain['usages'][$idx] = [
                    'editurl' => \Cx\Core\Routing\Url::fromBackend(
                        'Contact',
                        'forms',
                        0,
                        ['tpl' => 'edit', 'formId' => $formId, 'tab-value-tab' => 'notification']
                    )->toString(),
                    'title' => $form['lang'][$langId]['name'],
                ];
            }
        }
        return $senderDomains;
    }

    /**
     * Get the domain of email address $email
     *
     * If $email is an empty string or not a valid email address
     * then the domain of the basic setting coreAdminEmail is returned.
     *
     * @param string $email Email to fetch the domain from
     * @return string Domain part of email
     */
    protected function getDomainFromEmail(string $email): string {
        // note: this serves two purposes:
        // - $email === '' -> no recipient set
        // - $email is invalid
        if (!\FWValidator::isEmail($email)) {
            $email = $this->cx->getConfig()->getBaseConfig('coreAdminEmail');
        }
        $emailParts = explode('@', $email);
        $domain = array_pop($emailParts);
        return \Cx\Core\Net\Controller\ComponentController::convertIdnToAsciiFormat(
            $domain
        );
    }
}
