<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * Class FrontendController
 *
 * This is the frontend controller for the frontend editing.
 * This adds the necessary javascripts and toolbars
 *
 * @copyright   CLOUDREXX CMS - Cloudrexx AG Thun
 * @author      Ueli Kramer <ueli.kramer@comvation.com>
 * @package     cloudrexx
 * @subpackage  coremodule_frontendediting
 * @version     1.0.0
 */

namespace Cx\Core_Modules\FrontendEditing\Controller;

/**
 * Class FrontendController
 *
 * This is the frontend controller for the frontend editing.
 * This adds the necessary javascripts and toolbars
 *
 * @copyright   CLOUDREXX CMS - Cloudrexx AG Thun
 * @author      Ueli Kramer <ueli.kramer@comvation.com>
 * @package     cloudrexx
 * @subpackage  coremodule_frontendediting
 * @version     1.0.0
 */
class JsonFEController extends \Cx\Core\Core\Model\Entity\Controller implements \Cx\Core\Json\JsonAdapter
{
    public function getName() {
        return 'FE';
    }
    
    public function getMessagesAsString() {
        return '';
    }
    
    public function getAccessableMethods() {
        return array(
            'getToggleButton' => new \Cx\Core_Modules\Access\Model\Entity\Permission(array(), array(), false),
            // note: intentionally no permissions are set, as access control
            // get's handled by the exposed method itself
            'getPage',
        );
    }
    
    public function getDefaultPermissions() {
        return null;
    }
    
    public function getToggleButton($params) {
        if (
            !$this->frontendEditingIsActive(true, false) ||
            !isset($params['get']) ||
            !isset($params['get']['lang'])
        ) {
            return array('content' => '');
        }
        // load FE lang vars
        $lang = \Env::get('init')->getComponentSpecificLanguageData(
            parent::getName(),
            true,
            \FWLanguage::getLanguageIdByCode($params['get']['lang'])
        );
        // load ToolbarCache.html template
        $template = new \Cx\Core\Html\Sigma($this->getDirectory(false) . '/View/Template/Generic');
        $template->loadTemplateFile('ToolbarCache.html');
        // parse template
        $template->setVariable($lang);
        return array('content' => $template->get());
    }

    /**
     * Get data of a {@see \Cx\Core\ContentManager\Model\Entity\Page}
     * to be used by frontend editing editor
     *
     * This is a wrapper for {@see \Cx\Core\ContentManager\controller\JsonPage::get()}
     * to only get those page properties that are required by the frontend editing.
     * Access permission checks are performed by {@see \Cx\Core\ContentManager\controller\JsonPage::get()}.
     *
     * @param array $params {@see \Cx\Core\ContentManager\controller\JsonPage::get()}
     * @return array Result of {@see \Cx\Core\ContentManager\controller\JsonPage::get()},
     *     but reduced to just the following keys:
     *     - type
     *     - module
     *     - title
     *     - content
     *     - historyId
     *     - editingStatus
     *     - id
     */
    public function getPage(array $params): array {
        $jsonData =  new \Cx\Core\Json\JsonData();
        $response = $jsonData->data(
            'page',
            'get',
            $params
        );
        if ($response['status'] !== 'success') {
            throw new \Exception($response['message']);
        }
        $properties = [
            'type',
            'module',
            'title',
            'content',
            'historyId',
            'editingStatus',
            'id',
        ];
        $data = [];
        foreach ($properties as $property) {
            $data[$property] = $response['data'][$property];
        }
        return $data;
    }
}
