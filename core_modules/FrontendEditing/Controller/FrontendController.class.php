<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * Class FrontendController
 *
 * This is the frontend controller for the frontend editing.
 * This adds the necessary javascripts and toolbars
 *
 * @copyright   CLOUDREXX CMS - Cloudrexx AG Thun
 * @author      Ueli Kramer <ueli.kramer@comvation.com>
 * @package     cloudrexx
 * @subpackage  coremodule_frontendediting
 * @version     1.0.0
 */

namespace Cx\Core_Modules\FrontendEditing\Controller;

/**
 * Class FrontendController
 *
 * This is the frontend controller for the frontend editing.
 * This adds the necessary javascripts and toolbars
 *
 * @copyright   CLOUDREXX CMS - Cloudrexx AG Thun
 * @author      Ueli Kramer <ueli.kramer@comvation.com>
 * @package     cloudrexx
 * @subpackage  coremodule_frontendediting
 * @version     1.0.0
 */
class FrontendController extends \Cx\Core\Core\Model\Entity\Controller
{
    /**
     * Init the frontend editing.
     *
     * Register the javascripts and css files
     * Adds the used language variables to cloudrexx-js variables, so the toolbar has access to these variables
     *
     * @param ComponentController $componentController
     */
    public function initFrontendEditing(\Cx\Core_Modules\FrontendEditing\Controller\ComponentController $componentController)
    {
        global $_ARRAYLANG;

        // get necessary objects
        $objInit = \Env::get('init');
        $page = $this->cx->getPage();

        // add css and javascript file
        $jsFilesRoot = substr(ASCMS_CORE_MODULE_FOLDER . '/' . $componentController->getName() . '/View/Script', 1);

        \JS::registerCSS(substr(ASCMS_CORE_MODULE_FOLDER . '/' . $componentController->getName() . '/View/Style/Main.css', 1));
        \JS::registerJS($jsFilesRoot . '/Main.js');
        \JS::registerJS('core/Core/View/Script/BackendPaging.js');
        \JS::activate('cx');
        // not used for contrexx version 3.1
//        \JS::registerJS($jsFilesRoot . '/CKEditorPlugins.js');

        // activate ckeditor
        \JS::activate('ckeditor');
        \JS::activate('js-cookie');

        // load language data
        $_ARRAYLANG = $objInit->loadLanguageData('FrontendEditing');
        $langVariables = array(
            'TXT_FRONTEND_EDITING_SHOW_TOOLBAR' => $_ARRAYLANG['TXT_FRONTEND_EDITING_SHOW_TOOLBAR'],
            'TXT_FRONTEND_EDITING_HIDE_TOOLBAR' => $_ARRAYLANG['TXT_FRONTEND_EDITING_HIDE_TOOLBAR'],
            'TXT_FRONTEND_EDITING_PUBLISH' => $_ARRAYLANG['TXT_FRONTEND_EDITING_PUBLISH'],
            'TXT_FRONTEND_EDITING_SUBMIT_FOR_RELEASE' => $_ARRAYLANG['TXT_FRONTEND_EDITING_SUBMIT_FOR_RELEASE'],
            'TXT_FRONTEND_EDITING_REFUSE_RELEASE' => $_ARRAYLANG['TXT_FRONTEND_EDITING_REFUSE_RELEASE'],
            'TXT_FRONTEND_EDITING_SAVE' => $_ARRAYLANG['TXT_FRONTEND_EDITING_SAVE'],
            'TXT_FRONTEND_EDITING_EDIT' => $_ARRAYLANG['TXT_FRONTEND_EDITING_EDIT'],
            'TXT_FRONTEND_EDITING_CANCEL_EDIT' => $_ARRAYLANG['TXT_FRONTEND_EDITING_CANCEL_EDIT'],
            'TXT_FRONTEND_EDITING_FINISH_EDIT_MODE' => $_ARRAYLANG['TXT_FRONTEND_EDITING_FINISH_EDIT_MODE'],
            'TXT_FRONTEND_EDITING_THE_DRAFT' => $_ARRAYLANG['TXT_FRONTEND_EDITING_THE_DRAFT'],
            'TXT_FRONTEND_EDITING_SAVE_CURRENT_STATE' => $_ARRAYLANG['TXT_FRONTEND_EDITING_SAVE_CURRENT_STATE'],
            'TXT_FRONTEND_EDITING_CONFIRM_BLOCK_SAVE' => $_ARRAYLANG['TXT_FRONTEND_EDITING_CONFIRM_BLOCK_SAVE'],
            'TXT_FRONTEND_EDITING_MODULE_PAGE' => $_ARRAYLANG['TXT_FRONTEND_EDITING_MODULE_PAGE'],
            'TXT_FRONTEND_EDITING_NO_TITLE_AND_CONTENT' => $_ARRAYLANG['TXT_FRONTEND_EDITING_NO_TITLE_AND_CONTENT'],
            'TXT_FRONTEND_EDITING_CONFIRM_UNSAVED_EXIT' => $_ARRAYLANG['TXT_FRONTEND_EDITING_CONFIRM_UNSAVED_EXIT'],
            'TXT_FRONTEND_EDITING_DRAFT' => $_ARRAYLANG['TXT_FRONTEND_EDITING_DRAFT'],
            'TXT_FRONTEND_EDITING_PUBLISHED' => $_ARRAYLANG['TXT_FRONTEND_EDITING_PUBLISHED'],
        );

        // note: the input field name 'fe_title' is not actually being used by
        // Wysiwyg as we're just using Wysiwyg::getConfig()
        $wysiwygTitle = new \Cx\Core\Wysiwyg\Wysiwyg(
            'fe_title',
            '',
            'frontendEditingTitle'
        );
        // note: the input field name 'fe_content' is not actually being used by
        // Wysiwyg as we're just using Wysiwyg::getConfig()
        $wysiwygContent = new \Cx\Core\Wysiwyg\Wysiwyg(
            'fe_content',
            '',
            'frontendEditingContent'
        );

        $contrexxJavascript = \ContrexxJavascript::getInstance();
        $contrexxJavascript->setVariable(
            [
                'ckeditorTitleConfig' => $wysiwygTitle->getConfig(),
                'ckeditorContentConfig' => $wysiwygContent->getConfig(),
            ],
            'FrontendEditing'
        );

        // add toolbar to html
        $this->prepareTemplate($componentController, $wysiwygContent);

        // assign js variables
        $contrexxJavascript->setVariable('langVars', $langVariables, 'FrontendEditing');
        $contrexxJavascript->setVariable('pageId', $page->getId(), 'FrontendEditing');
        $contrexxJavascript->setVariable('hasPublishPermission', \Permission::checkAccess(78, 'static', true), 'FrontendEditing');
    }

    /**
     * Adds the toolbar to the current html structure (after the starting body tag)
     *
     * @param ComponentController $componentController
     * @param \Cx\Core\Wysiwyg\Wysiwyg Wysiwyg instance to use for fetching the
     *     MediaBrowser integration.
     */
    private function prepareTemplate(
        ComponentController $componentController,
        \Cx\Core\Wysiwyg\Wysiwyg $wysiwyg
    ) {
        global $_ARRAYLANG, $objTemplate;

        // get necessary objects
        $objInit = \Env::get('init');
        $page = $this->cx->getPage();

        // init component template object
        $componentTemplate = new \Cx\Core\Html\Sigma(ASCMS_CORE_MODULE_PATH . '/' . $componentController->getName() . '/View/Template/Generic');
        $componentTemplate->setErrorHandling(PEAR_ERROR_DIE);

        // add div for toolbar after starting body tag
        $componentTemplate->loadTemplateFile('Toolbar.html');

        $objUser = $this->cx->getUser()->objUser;
        $firstname = $objUser->getProfileAttribute('firstname');
        $lastname = $objUser->getProfileAttribute('lastname');
        $componentTemplate->setGlobalVariable(array(
            'LOGGED_IN_USER' => !empty($firstname) && !empty($lastname) ? $firstname . ' ' . $lastname : $objUser->getUsername(),
            'TXT_LOGOUT' => $_ARRAYLANG['TXT_FRONTEND_EDITING_TOOLBAR_LOGOUT'],
            'TXT_FRONTEND_EDITING_TOOLBAR_OPEN_CM' => $_ARRAYLANG['TXT_FRONTEND_EDITING_TOOLBAR_OPEN_CM'],
            'TXT_FRONTEND_EDITING_HISTORY' => $_ARRAYLANG['TXT_FRONTEND_EDITING_HISTORY'],
            'TXT_FRONTEND_EDITING_TOOLBAR_SAVE_BLOCK' => $_ARRAYLANG['TXT_FRONTEND_EDITING_TOOLBAR_SAVE_BLOCK'],
            'LINK_LOGOUT' => contrexx_raw2xhtml($objInit->getUriBy('section', 'logout')),
            'LINK_PROFILE' => ASCMS_PATH_OFFSET . '/cadmin/index.php?cmd=Access&amp;act=user&amp;tpl=modify&amp;id=' . $objUser->getId(),
            'LINK_CM' => ASCMS_PATH_OFFSET . '/cadmin/index.php?cmd=ContentManager&amp;page=' . $page->getId() . '&amp;tab=content',
        ));

        // MediaBrowser used by the WYSIWYG-editor for the image & link dialogue
        $componentTemplate->setVariable(
            'MEDIABROWSER_BUTTON_CKEDITOR',
            $wysiwyg->getMediaBrowserIntegration()
        );

        $objTemplate->_blocks['__global__'] = preg_replace('/<body[^>]*>/', '\\0' . $componentTemplate->get(), $objTemplate->_blocks['__global__']);
    }
}
