<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * Class AccessEventListener
 *
 * @copyright   CLOUDREXX CMS - Cloudrexx AG Thun
 * @author      Robin Glauser <robin.glauser@comvation.com>
 * @package     cloudrexx
 * @subpackage  coremodule_access
 * @version     1.0.0
 */

namespace Cx\Core_Modules\Access\Model\Event;

use Cx\Core\Event\Model\Entity\DefaultEventListener;
use Cx\Core\MediaSource\Model\Entity\MediaSourceManager;
use Cx\Core\MediaSource\Model\Entity\MediaSource;

/**
 * Class AccessEventListener
 *
 * @copyright   CLOUDREXX CMS - Cloudrexx AG Thun
 * @author      Robin Glauser <robin.glauser@comvation.com>
 * @author      Thomas Wirz <thomas.wirz@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  coremodule_access
 * @version     1.0.0
 */
class AccessEventListener extends DefaultEventListener
{

    /**
     * @param MediaSourceManager $mediaBrowserConfiguration
     */
    public function mediasourceLoad(
        MediaSourceManager $mediaBrowserConfiguration
    ) {
        global $_ARRAYLANG;
        $mediaType = new MediaSource(
            'access',
            // hotfix as CLX-1045 is not yet live
            $_ARRAYLANG['TXT_USER_ADMINISTRATION'] ?? '',
            array(
                $this->cx->getWebsiteImagesAccessPath(),
                $this->cx->getWebsiteImagesAccessWebPath(),
            ),
            array(18)
        );
        $mediaBrowserConfiguration->addMediaType($mediaType);
    }

    /**
     * @param array $config Config from event NetManager:getSenderDomains
     */
    public function NetManagerGetSenderDomains(array $config = []): array {
        $senderDomains = [];
        $reportUsages = $config['usages'] ?? false;
        $excludedMailKey = 'reset_pw';
        if ($reportUsages) {
            $query = '
                SELECT
                    `sender_mail`,
                    `type`,
                    `lang_id`,
                    `subject`
                FROM `' . DBPREFIX . 'access_user_mail`
                WHERE `sender_mail` != \'\' AND `type` != ?
            ';
        } else {
            $query = '
                SELECT
                    DISTINCT `sender_mail`
                FROM `' . DBPREFIX . 'access_user_mail`
                WHERE `sender_mail` != \'\' AND `type` != ?
            ';
        }
        $result = $this->cx->getDb()->getAdoDb()->Execute($query, $excludedMailKey);
        if (!$result || !$result->RecordCount()) {
            return $senderDomains;
        }
        while (!$result->EOF) {
            try {
                $domain = $this->getDomainFromEmail($result->fields['sender_mail']);
            } catch (\Exception $e) {
                \DBG::debug($e->getMessage());
                $result->MoveNext();
                continue;
            }
            if (!isset($senderDomains[$domain])) {
                $senderDomains[$domain] = [
                    'strict' => false,
                    'usages' => [],
                ];
            }
            if (!$reportUsages) {
                $result->MoveNext();
                continue;
            }
            $senderDomains[$domain]['usages'][] = [
                'editurl' => \Cx\Core\Routing\Url::fromBackend(
                    'Access',
                    'config',
                    0,
                    [
                        'tpl' => 'modifyMail',
                        'type' => $result->fields['type'],
                        'access_mail_lang' => $result->fields['lang_id'],
                    ]
                )->toString(),
                'title' => $result->fields['subject'],
            ];
            $result->MoveNext();
        }
        return $senderDomains;
    }

    /**
     * Get the domain of email address $email
     *
     * @param string $email Email to fetch the domain from
     * @throws \Exception If $email not a valid email address
     * @return string Domain part of email
     */
    protected function getDomainFromEmail(string $email): string {
        if (!\FWValidator::isEmail($email)) {
            throw new \Exception('Invalid email: ' . $email);
        }
        $emailParts = explode('@', $email);
        $domain = array_pop($emailParts);
        return \Cx\Core\Net\Controller\ComponentController::convertIdnToAsciiFormat(
            $domain
        );
    }
}
