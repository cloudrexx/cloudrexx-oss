<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * @copyright   CLOUDREXX CMS - CLOUDREXX AG
 * @author      Cloudrexx Development Team <info@cloudrexx.com>
 * @access      public
 * @package     cloudrexx
 * @subpackage  coremodule_cache
 */
global $_ARRAYLANG;
$_ARRAYLANG['TXT_CACHE_ERR_NOTWRITABLE'] = 'Le dossier choisi pour le cache est protégé en écriture. Veuillez affecter le privilège chmod 777 au dossier suivant:';
$_ARRAYLANG['TXT_CACHE_ERR_NOTEXIST'] = 'Le dossier suivant, prévu pour le cache, n"existe pas:';
$_ARRAYLANG['TXT_SETTINGS_MENU_CACHE'] = 'Cache';
$_ARRAYLANG['TXT_CACHE_STATS'] = 'Statistiques';
$_ARRAYLANG['TXT_CACHE_CONTREXX_CACHING'] = 'Cloudrexx caching';
$_ARRAYLANG['TXT_CACHE_USERCACHE'] = 'Database cache engine';
$_ARRAYLANG['TXT_CACHE_OPCACHE'] = 'Program code cache engine';
$_ARRAYLANG['TXT_CACHE_PROXYCACHE'] = 'Proxy cache';
$_ARRAYLANG['TXT_CACHE_EMPTY'] = 'Vider le cache';
$_ARRAYLANG['TXT_CACHE_ZEND_OPCACHE'] = 'Zend OPCache';
$_ARRAYLANG['TXT_CACHE_MEMCACHED'] = 'Memcached';
$_ARRAYLANG['TXT_CACHE_ZEND_OPCACHE_ACTIVE_INFO'] = 'Zend OPCache is active, as soon as the php directive "opcache.enable" has been set to "On".';
$_ARRAYLANG['TXT_CACHE_MEMCACHED_ACTIVE_INFO'] = 'Memcached is active, as soon as the Memcached server is running and the configuration is correct.';
$_ARRAYLANG['TXT_CACHE_MEMCACHED_CONFIG_INFO'] = 'If you want to use Memcached, the configuration (IP address and port number) has to be correct.';
$_ARRAYLANG['TXT_CACHE_ENGINE'] = 'Engine';
$_ARRAYLANG['TXT_CACHE_INSTALLATION_STATE'] = 'Installed';
$_ARRAYLANG['TXT_CACHE_ACTIVE_STATE'] = 'Active';
$_ARRAYLANG['TXT_CACHE_CONFIGURATION_STATE'] = 'Configured';
$_ARRAYLANG['TXT_SAVE'] = 'Enregistrer';
$_ARRAYLANG['TXT_ACTIVATED'] = 'Activé';
$_ARRAYLANG['TXT_DEACTIVATED'] = 'Désactivé';
$_ARRAYLANG['TXT_CACHE_SETTINGS_STATUS'] = 'Cache';
$_ARRAYLANG['TXT_CACHE_SETTINGS_STATUS_HELP'] = 'Statut du cache: (on | off)';
$_ARRAYLANG['TXT_CACHE_SETTINGS_EXPIRATION'] = 'Expiration';
$_ARRAYLANG['TXT_CACHE_SETTINGS_EXPIRATION_HELP'] = 'Les pages cachées expirent après cette durée (en secondes) et seront dès lors renouvelées.';
$_ARRAYLANG['TXT_CACHE_EMPTY_DESC'] = 'Cliquer sur ce bouton pour vider le cache des pages visitées. Il se reconstitue au fur et à mesure des prochaines pages visitées.';
$_ARRAYLANG['TXT_CACHE_EMPTY_DESC_FILES_AND_ENRIES'] = 'With a click on the button, you can remove the current cache content. The cached files and entries will be recreated while viewing the pages.';
$_ARRAYLANG['TXT_CACHE_EMPTY_DESC_FILES'] = 'With a click on the button, you can remove the current cache content. The cached files will be recreated while viewing the pages.';
$_ARRAYLANG['TXT_CACHE_EMPTY_DESC_MEMCACHED'] = 'With a click on the button, you can mark the current cache content as outdated. The cached entries will be updated while viewing the pages.';
$_ARRAYLANG['TXT_CACHE_STATS_FILES'] = 'Pages mises en cache';
$_ARRAYLANG['TXT_CACHE_STATS_FOLDERSIZE'] = 'Taille du dossier';
$_ARRAYLANG['TXT_STATS_CACHE_SIZE'] = 'Quantité de données stockées';
$_ARRAYLANG['TXT_DISPLAY_CONFIGURATION'] = 'Afficher la configuration';
$_ARRAYLANG['TXT_HIDE_CONFIGURATION'] = 'Masquer la configuration';
$_ARRAYLANG['TXT_CACHE_VARNISH'] = 'Varnish';
$_ARRAYLANG['TXT_CACHE_PROXY_IP'] = 'Adresse IP du proxy';
$_ARRAYLANG['TXT_CACHE_PROXY_PORT'] = 'Proxy Port';
$_ARRAYLANG['TXT_SETTINGS_UPDATED'] = 'Paramètres mis à jour.';
$_ARRAYLANG['TXT_CACHE_FOLDER_EMPTY'] = 'Cache vidé.';
$_ARRAYLANG['TXT_CACHE_EMPTY_SUCCESS'] = 'Cache has been emptied.';
