<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */


namespace Cx\Core_Modules\TemplateEditor\Model\Repository;

use Cx\Core_Modules\TemplateEditor\Model\Entity\Preset;
use Cx\Core_Modules\TemplateEditor\Model\Storable;

/**
 * Class ThemeOptionsRepository
 *
 * @copyright   CLOUDREXX CMS - CLOUDREXX AG
 * @author      Robin Glauser <robin.glauser@cloudrexx.com>
 * @package     contrexx
 * @subpackage  core_module_templateeditor
 */
class PresetRepository
{

    /**
     * @var Storable
     */
    protected $storage;

    /**
     * @param Storable $storage
     */
    public function __construct(Storable $storage)
    {
        $this->storage = $storage;
    }

    /**
     * @param       $name
     *
     * @return Preset
     */
    public function getByName($name)
    {
        $preset = null;
        $cx = \Cx\Core\Core\Controller\Cx::instanciate();
        try {
            $preset = $this->storage->retrieve($name);
        } catch (\Cx\Core_Modules\TemplateEditor\Model\PresetRepositoryException $e) {
            \DBG::debug($e->getMessage());
        } catch (\Symfony\Component\Yaml\Exception\ParseException $e) {
            \DBG::debug($e->getMessage());
        }
        if (
            !$preset
            && $name != Preset::DEFAULT
        ) {
            return $this->getByName(Preset::DEFAULT);
        }
        if (!$preset) {
            $langData = \Env::get('init')->getComponentSpecificLanguageData(
                'TemplateEditor',
                false
            );
            $name = $langData['TXT_CORE_MODULE_TEMPLATEEDITOR_NONE'];
            $preset = [
                'options' => [],
            ];
        }
        return Preset::createFromArray($name, $preset);
    }

    /**
     * Find all presets
     *
     * @return array
     */
    public function findAll()
    {
        return $this->storage->getList();
    }

    /**
     * Save a ThemeOptions entity to the component.yml file.
     *
     * @param Preset $entity
     *
     * @return bool
     */
    public function save($entity)
    {
        return $this->storage->persist($entity->getName(), $entity);
    }

    /**
     * @param $entity
     */
    public function remove($entity) {
        return $this->storage->remove($entity->getName());
    }


}
