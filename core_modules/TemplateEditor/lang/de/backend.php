<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * This is the english language file for backend mode.
 * This file is included by Contrexx and all entries are set as placeholder
 * values for backend ACT template by SystemComponentBackendController
 *
 * @copyright   CLOUDREXX CMS - Cloudrexx AG Thun
 * @author      Michael Ritter <michael.ritter@comvation.com>
 * @package     contrexx
 * @subpackage  core_module_templateeditor
 */

global $_ARRAYLANG;

// Let's start with module info:
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR'] = 'TemplateEditor';
// Here come the ACTs:
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_ACT_DEFAULT'] = 'Übersicht';
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_ACT_SETTINGS'] = 'Einstellungen';

// Now our content specific values:
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_SELECT'] = 'Auswählen';
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_CANCEL'] = 'Abbrechen';
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_BACK_TO_LAYOUTMANAGER'] = 'Zurück zur Übersicht';
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_LAYOUT'] = 'Layout';
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_PRESET'] = 'Vorlage';
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_SAVE'] = 'Speichern';
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_SAVE_TITLE'] = 'Sind Sie sicher?';
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_SAVE_CONTENT'] = 'Durch das Speichern werden die Änderungen übernommen. Falls diese Vorlage aktiv ist, werden die Änderungen für die Besucher Ihrer Website sichtbar.';
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_CHANGE_PICTURE'] = 'Ändern';
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_REMOVE_PICTURE'] = 'Entfernen';
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_CHOOSE_PICTURE'] = 'Bild auswählen';
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_ADD_PICTURE'] = 'Bild hinzufügen';

$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_VIEW'] = 'Ansicht wechseln';
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_VIEW_PHONE'] = 'Handy';
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_VIEW_TABLET'] = 'Tablet';
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_VIEW_DESKTOP'] = 'Desktop';
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_VIEW_FULL'] = 'Voll';
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_COLOR_WRONG_FORMAT'] = 'Falsches Farbformat. Sollte #ffffff, ein Farbname (z.B. olive) oder nur fff sein.';
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_TEXT_WRONG_FORMAT'] = "Text \"%s\" passt nicht zum gegebenen Regex: ";
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_IMAGE_FILE_NOT_FOUND'] = "Datei \"%s\" nicht gefunden.";
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_VALUE_EMPTY'] = "Darf nicht leer sein.";
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_NO_OPTIONS_HELP'] = "Für dieses Theme sind keine Platzhalter vorgegeben. Weitere Infos dazu finden Sie in unserer Wissensdatenbank.";
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_NO_OPTIONS_LINKNAME'] = "So fügen Sie Ihrer Vorlage Optionen hinzu";

$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_ACTIVATE_PRESET'] = "Aktivieren";
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_ACTIVATE_PRESET_MORE'] = "Diese Vorlage auf der Website aktivieren";
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_ADD_PRESET'] = "Neue Vorlage";
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_ADD_PRESET_MORE'] = "Eine neue Vorlage auf einer bereits bestehenden Vorlage erstellen.";
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_REMOVE_PRESET'] = "Löschen";
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_REMOVE_PRESET_MORE'] = "Diese Vorlage löschen";
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_PRESET_ACTIVE'] = "Aktiv";
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_RESET_PRESET'] = "Zurücksetzen";
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_RESET_PRESET_MORE'] = "Diese Vorlage auf den Anfangszustand zurücksetzen.";

$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_YES'] = "Ja";
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_NO'] = "Nein";

$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_ACTIVATE_PRESET_TITLE'] = "Vorlage aktivieren";
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_ADD_PRESET_TITLE'] = "Neue Vorlage erstellen";
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_ACTIVATE_PRESET_TEXT'] = "Durch das Aktivieren der Vorlage wird diese auf der Website aktiv und wird für alle Besucher sichtbar.";
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_NEW_PRESET_TEXT'] = "Erstellen Sie eine neue Vorlage basierend auf einer bestehenden Vorlage.";
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_NEW_PRESET_TEXT_NOT_ALLOWED_CHARACTERS'] = "Die Vorlage darf nur Buchstaben und Zahlen beinhalten.";
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_NEW_PRESET_NAME'] = "Name";
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_NEW_PRESET_PRESET'] = "Vorlage";
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_REMOVE_PRESET_TEXT'] = "Möchten Sie diese Vorlage entfernen?";
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_REMOVE_PRESET_DEFAULT_WARNING'] = "Die Vorlage Default kann nicht entfernt werden.";
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_RESET_PRESET_TEXT'] = "Möchten Sie die Vorlage auf den Anfangszustand zurücksetzen?";
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_REMOVE_PRESET_TEXT_IS_ACTIVE'] = "Diese Vorlage ist momentan aktiv. Falls Sie die Vorlage löschen, wird die Default Vorlage aktiviert.";

$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_OPTION_TYPE_MISSING'] = 'Option %s kann nicht angezeigt werden, da kein Option-Typ (type) definiert ist.';
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_OPTION_TYPE_INVALID'] = 'Option %1$s kann nicht angezeigt werden, da der Option-Typ %2$s ungültig ist.';
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_NONE'] = 'Keine';
