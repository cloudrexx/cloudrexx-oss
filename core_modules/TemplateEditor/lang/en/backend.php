<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * This is the english language file for backend mode.
 * This file is included by Contrexx and all entries are set as placeholder
 * values for backend ACT template by SystemComponentBackendController
 *
 * @copyright   CLOUDREXX CMS - Cloudrexx AG Thun
 * @author      Michael Ritter <michael.ritter@comvation.com>
 * @package     contrexx
 * @subpackage  core_module_templateeditor
 */

global $_ARRAYLANG;

// Let's start with module info:
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR'] = 'TemplateEditor';
// Here come the ACTs:
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_ACT_DEFAULT'] = 'Overview';
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_ACT_SETTINGS'] = 'Settings';
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_ACT_SETTINGS_DEFAULT'] = 'Mailing';
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_ACT_SETTINGS_HELP'] = 'Help';

// Now our content specific values:
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_SELECT'] = 'Select';
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_CANCEL'] = 'Cancel';
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_BACK_TO_LAYOUTMANAGER'] = 'Back to overview';
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_LAYOUT'] = 'Layout';
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_PRESET'] = 'Preset';
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_SAVE'] = 'Save';
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_SAVE_TITLE'] = 'Are you sure?';
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_SAVE_CONTENT'] = 'This will make all your current changes permanent and visible to the user of your website, if this preset is active.';
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_CHANGE_PICTURE'] = 'Change';
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_REMOVE_PICTURE'] = 'Remove';
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_CHOOSE_PICTURE'] = 'Choose picture';
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_ADD_PICTURE'] = 'Add picture';

$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_VIEW'] = 'Change view';
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_VIEW_PHONE'] = 'Phone';
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_VIEW_TABLET'] = 'Tablet';
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_VIEW_DESKTOP'] = 'Desktop';
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_VIEW_FULL'] = 'Full';
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_COLOR_WRONG_FORMAT'] = 'Wrong color format. Should be eg. #fff, a color name (olive) or just fff.';
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_TEXT_WRONG_FORMAT'] = "String \"%s\" doesn't match given regex: ";
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_IMAGE_FILE_NOT_FOUND'] = "File \"%s\" not found.";
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_VALUE_EMPTY'] = "Can't be empty.";
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_NO_OPTIONS_HELP'] = "This template has no options. To learn how to add options to your template visit the wiki page.";
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_NO_OPTIONS_LINKNAME'] = "Wiki: How to add options to your template.";

$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_ACTIVATE_PRESET'] = "Activate";
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_ADD_PRESET'] = "New Preset";
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_REMOVE_PRESET'] = "Remove";
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_PRESET_ACTIVE'] = "Active";
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_RESET_PRESET'] = "Reset";
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_ACTIVATE_PRESET_MORE'] = "Activate this preset on the website.";
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_ADD_PRESET_MORE'] = "Create a new preset based on a existing preset.";
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_REMOVE_PRESET_MORE'] = "Remove this preset.";
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_RESET_PRESET_MORE'] = "Reset the preset to the initial status.";

$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_YES'] = "Yes";
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_NO'] = "No";

$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_ACTIVATE_PRESET_TITLE'] = "Activate preset";
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_ADD_PRESET_TITLE'] = "Create new preset";
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_ACTIVATE_PRESET_TEXT'] = "When activating the preset, it will be visible for all visitors of your website.";
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_NEW_PRESET_TEXT'] = "Create a new preset based on a existing preset.";
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_NEW_PRESET_NAME'] = "Name";
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_NEW_PRESET_PRESET'] = "Preset";
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_REMOVE_PRESET_TEXT'] = "Would you like to remove this preset?";
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_REMOVE_PRESET_DEFAULT_WARNING'] = "The preset Default can't be removed.";
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_RESET_PRESET_TEXT'] = "Do you want to reset this preset to the initial state?";
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_REMOVE_PRESET_TEXT_IS_ACTIVE'] = "This preset is active at the moment. If you remove it, the Default preset will be activated.";


$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_OPTION_TYPE_MISSING'] = 'Option %s cannot be displayed because no option type (type) is defined.';
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_OPTION_TYPE_INVALID'] = 'Option %1$s cannot be displayed because the option type %2$s is invalid.';
$_ARRAYLANG['TXT_CORE_MODULE_TEMPLATEEDITOR_NONE'] = 'None';
