<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * Main controller for Login
 *
 * @copyright   Cloudrexx AG
 * @author      Project Team SS4U <info@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  coremodule_login
 */

namespace Cx\Core_Modules\Login\Controller;

/**
 * Main controller for Login
 *
 * @copyright   Cloudrexx AG
 * @author      Project Team SS4U <info@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  coremodule_login
 */
class ComponentController extends \Cx\Core\Core\Model\Entity\SystemComponentController {

    public function getControllerClasses() {
        // Return an empty array here to let the component handler know that there
        // does not exist a backend, nor a frontend controller of this component.
        return array();
    }

    /**
     * Load your component.
     *
     * @param \Cx\Core\ContentManager\Model\Entity\Page $page       The resolved page
     */
    public function load(\Cx\Core\ContentManager\Model\Entity\Page $page) {
        global $objTemplate;
        switch ($this->cx->getMode()) {
            case \Cx\Core\Core\Controller\Cx::MODE_FRONTEND:
                $sessionObj = $this->getComponent('Session')->getSession();
                $objLogin = new \Cx\Core_Modules\Login\Controller\Login(\Env::get('cx')->getPage()->getContent());
                $pageTitle = \Env::get('cx')->getPage()->getTitle();
                $pageMetaTitle = \Env::get('cx')->getPage()->getMetatitle();
                \Env::get('cx')->getPage()->setContent($objLogin->getContent($pageMetaTitle, $pageTitle));
                break;

            case \Cx\Core\Core\Controller\Cx::MODE_BACKEND:
                if (
                    \FWUser::getFWUserObject()->objUser->login(true)
                    && \FWUser::getFWUserObject()->objUser->hasBackendAccess()
                    && (
                        empty($_GET['act'])
                        || $_GET['act'] != 'verify'
                    )
                ) {
                    \Cx\Core\Csrf\Controller\Csrf::redirect(
                        \Cx\Core\Routing\Url::fromBackend('Home')
                    );
                }
                $this->cx->getTemplate()->addBlockfile('CONTENT_OUTPUT', 'content_master', 'ContentMaster.html');
                $objTemplate = $this->cx->getTemplate();
                $objLoginManager = new \Cx\Core_Modules\Login\Controller\LoginManager();
                $objLoginManager->getPage();
                break;

            default:
                break;
        }
    }

    /**
     * Do something before main template gets parsed
     *
     * @param \Cx\Core\Html\Sigma                       $template   The main template
     */
    public function preFinalize(\Cx\Core\Html\Sigma $template) {
        if ($this->cx->getMode() != \Cx\Core\Core\Controller\Cx::MODE_FRONTEND) {
            return;
        }
        // make all language data of Core component globally available
        $template->setVariable(\Env::get('init')->getComponentSpecificLanguageData('Core'));
    }

    /**
     * @inheritDoc
     */
    public function registerEventListeners() {
        $this->cx->getEvents()->addEventListener(
            'NetManager:getSenderDomains',
            new \Cx\Core_Modules\Login\Model\Event\LoginEventListener($this->cx)
        );
    }

    /**
     * Get the URL passed to the login from through the URL modificator `redirect`.
     *
     * @return  string Base64 encoded URL where the authenticated user shall
     *     be redirectd to.
     */
    public function getRedirectFromRequest(): string {
        $redirect = '';
        if ($this->cx->getRequest()->hasParam('redirect')) {
            $redirect = $this->cx->getRequest()->getParam('redirect');
        } elseif ($this->cx->getRequest()->hasParam('redirect', false)) {
            $redirect = $this->cx->getRequest()->getParam('redirect', false);
        }
        if (empty($redirect)) {
            return '';
        }
        $validator = new \CxValidateRegexp(
            ['pattern' => '#^[a-z0-9+/]+={0,2}$#i']
        );
        if (!$validator->isValid($redirect)) {
            return '';
        }
        return $redirect;
    }
}
