<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * Class WidgetEventListener
 *
 * @copyright   CLOUDREXX CMS - Cloudrexx AG Thun
 * @author      Project Team SS4U <info@comvation.com>
 * @package     cloudrexx
 * @subpackage  coremodule_widget
 * @version     1.0.0
 */

namespace Cx\Core_Modules\Widget\Model\Event;

/**
 * Class WidgetEventListener
 *
 * @copyright   CLOUDREXX CMS - Cloudrexx AG Thun
 * @author      Project Team SS4U <info@comvation.com>
 * @package     cloudrexx
 * @subpackage  coremodule_widget
 * @version     1.0.0
 */
class WidgetEventListener extends \Cx\Core\Event\Model\Entity\DefaultEventListener {

    /**
     * @var array List of names of the widgets currently being parsed.
     */
    protected array $currentWidgetParseStack = array();

    /**
     * Access event listener for clearing esi cache
     *
     * @param array $eventArgs
     *
     * return null
     */
    public function clearEsiCache($eventArgs): void {
        if (empty($eventArgs) || $eventArgs[0] != 'Widget') {
            return;
        }

        $widgetNames = isset($eventArgs[1]) ? $eventArgs[1] : array();
        $widgets     = $this->cx->getComponent('Widget')->getWidgets();

        if (empty($widgets)) {
            return;
        }

        if (empty($widgetNames) || !is_array($widgetNames)) {
            foreach ($widgets as $widget) {
                $widget->clearCache();
            }
            return;
        }

        foreach ($widgets as $widget) {
            if (in_array($widget->getName(), $widgetNames)) {
                $widget->clearCache();
            }
        }
    }

    /**
     * Increases the current widget parsing nesting level on parsing start
     * @param array $eventArgs
     */
    protected function widgetWidgetPreParse(array $eventArgs): void {
        array_push($this->currentWidgetParseStack, $eventArgs['widgetName']);
    }

    /**
     * Decreases the current widget parsing nesting level on parsing start
     * @param array $eventArgs
     */
    protected function widgetWidgetPostParse(array $eventArgs): void {
        array_pop($this->currentWidgetParseStack);
    }

    /**
     * Returns the widget parsing nesting level
     * @return int Widget parsing nesting level. 0 if we're not currently parsing widgets.
     */
    public function getWidgetParseNestingLevel(): int {
        return count($this->currentWidgetParseStack);
    }

    /**
     * Returns the stack of currently parsed widgets
     *
     * This will only have more than one element when widgets contain widgets.
     * @return array List of widget names
     */
    public function getWidgetParseStack(): array {
        return $this->currentWidgetParseStack;
    }

    /**
     * Work-around for parent not passing along all event arguments
     * @inheritdoc
     */
    public function onEvent($eventName, array $eventArgs) {
        return parent::onEvent($eventName, array($eventArgs));
    }
}
