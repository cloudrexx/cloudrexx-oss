let currentlyOpenMediaBrowserReferenceEl = null;
cx.ready(function() {
    for (const el of document.querySelectorAll(".mediabrowser-imagechooser")) {
        const browseEl = el.querySelector(".browse");
        browseEl.addEventListener("click", function(e) {
            currentlyOpenMediaBrowserReferenceEl = el;
            e.preventDefault();
            this.parentElement.querySelector(".mediabrowser-button").dispatchEvent(
                new Event("click")
            );
        });
        const deleteEl = el.querySelector(".deletePreviewImage");
        deleteEl.addEventListener("click", function(e) {
            e.preventDefault();
            const previewImage = el.querySelector(".preview");
            previewImage.classList.add("empty");
            previewImage.src = previewImage.dataset["placeholderImg"];
            el.querySelector("input").value = "";
            el.querySelector(".title").title = el.querySelector(".title").dataset["placeholderTitle"];
        });
        if (el.querySelector("input").value === el.querySelector("img").dataset.placeholderImg) {
            el.querySelector("input").value = "";
            el.querySelector(".preview").classList.add("empty");
        }
    }
});
function mediabrowserImagechooserCallback(data) {
    if (!currentlyOpenMediaBrowserReferenceEl) {
        return;
    }
    const el = currentlyOpenMediaBrowserReferenceEl;
    if (data.type !== "file" || !data.data[0]) {
        return;
    }
    if (
        !["gif", "png", "jpg", "jpeg", "webp", "svg", "ico"].includes(
            data.data[0].datainfo.extension.toLowerCase()
        )
    ) {
        return;
    }
    const previewImage = el.querySelector(".preview");
    let previewSrc = data.data[0].datainfo.filepath;
    if (data.data[0].datainfo.hasPreview) {
        previewSrc = data.data[0].datainfo.preview;
    }
    previewImage.classList.remove("empty");
    previewImage.src = previewSrc;
    el.querySelector("input").value = data.data[0].datainfo.filepath;
    el.querySelector(".title").title = data.data[0].datainfo.name;
}

