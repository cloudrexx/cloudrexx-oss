<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * Class MediaBrowser
 *
 * @copyright   Cloudrexx AG
 * @author      Robin Glauser <robin.glauser@comvation.com>
 * @package     cloudrexx
 * @subpackage  coremodule_mediabrowser
 */
namespace Cx\Core_Modules\MediaBrowser\Model\Entity;

/**
 * Class MediaBrowserException
 *
 * @copyright   Cloudrexx AG
 * @author      Manuel Schenk <manuel.schenk@comvation.com>
 */
class MediaBrowserException extends \Exception {}

/**
 * Class MediaBrowser
 *
 * @copyright   Cloudrexx AG
 * @author      Robin Glauser <robin.glauser@comvation.com>
 */
class MediaBrowser extends \Cx\Model\Base\EntityBase
{
    protected static $optionValues = [
        'views',
        'startview',
        'startmediatype',
        'mediatypes',
        'multipleselect',
        'modalopened',
        'modalclosed'
    ];

    /**
     * The set options for the mediabrowser
     * @var Array
     */
    protected $options = array();

    /**
     * The SystemComponentController that did instanciate this MediaBrowser
     * instance.
     *
     * @var \Cx\Core\Core\Model\Entity\SystemComponentController SystemComponentController that instanciated this MediaBrowser instance
     */
    protected $callingSystemComponentController;

    /**
     * @var string $entity
     */
    protected $entity;


    /**
     * Create new instance of mediabrowser and register in componentcontroller.
     * If $systemComponentController is not specified, the call trace is searched for the component calling this constructor.
     * @param \Cx\Core\Core\Model\Entity\SystemComponentController $systemComponentController (optional) ComponentController of the component that uses the object generated by this constructor
     * @param string $entity MediaBrowser instantiated with optional entity
     * @throws MediaBrowserException If no $systemComponentController is provided and none can be found
     */
    public function __construct($systemComponentController = null, $entity = '')
    {
        // Sets provided SystemComponentController that instanciated this MediaBrowser instance
        $this->callingSystemComponentController = $systemComponentController;
        if (!$this->callingSystemComponentController) {
            // Searches the calling SystemComponentController intelligently by RegEx on backtrace stack frame
            $traces = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 2);
            $trace = end($traces);
            if (empty($trace['class'])) {
                throw new MediaBrowserException('No SystemComponentController found that instanciated MediaBrowser');
            }
            $matches = array();
            preg_match(
                '/Cx\\\\(?:Core|Core_Modules|Modules)\\\\([^\\\\]*)\\\\/',
                $trace['class'],
                $matches
            );
            $this->callingSystemComponentController = $this->getComponent($matches[1]);
        }

        // sets js variable for current component
        \ContrexxJavascript::getInstance()->setVariable(
            'component',
            $this->callingSystemComponentController->getSystemComponent()->getName(),
            'mediabrowser'
        );

        $this->entity = $entity;

        $this->getComponentController()->addMediaBrowser($this);

        $this->options = array(
            'data-cx-mb',
            'class' => "mediabrowser-button button"
        );
    }

    /**
     * Set a mediabrowser option
     *
     * @param $options
     */
    function setOptions($options)
    {
        $this->options = array_merge($this->options, $options);
    }

    /**
     * Get a option
     *
     * @param $option
     *
     * @return string
     */
    function getOption($option)
    {
        if (isset($this->options[$option])) {
            return $this->options[$option];
        }
        return null;
    }

    /**
     * Set a Javascript callback when the modal gets closed
     *
     * @param $callback array Callback function name
     */
    function setCallback($callback)
    {
        $this->options['data-cx-Mb-Modalclosed'] = $callback;
    }

    /**
     * Get all Options as a String
     *
     * @return string
     */
    function getOptionsString()
    {
        $optionsString = "";
        foreach ($this->options as $key => $value) {
            if (is_int($key)) {
                $optionsString .= $value . ' ';
            } else {
                if (in_array($key, self::$optionValues)) {
                    $key = 'data-cx-Mb-' . $key;
                }
                $optionsString .= $key . '="' . $value . '" ';
            }
        }
        return $optionsString;
    }

    /**
     * Handles cleanup/sanitation for FileInterface instances or file path variables
     *
     * If $file is a string it's converted to a FileInterface instance.
     * If the file does not exist (or the file path was empty) the fallback file
     * is used instead.
     * @param \Cx\Lib\FileSystem\FileInterface|string $file The file to sanitize
     * @param \Cx\Lib\FileSystem\FileInterface $fallbackFile The FileInterface instance to use as fallback
     * @return \Cx\Lib\FileSystem\FileInterface The resulting FileInterface instance
     */
    protected function getFileWithFallback(
        \Cx\Lib\FileSystem\FileInterface|string $file,
        \Cx\Lib\FileSystem\FileInterface $fallbackFile,
    ): \Cx\Lib\FileSystem\FileInterface {
        try {
            if (!is_object($file)) {
                $file = new \Cx\Lib\FileSystem\File($file);
            }
            if (!$file->exists()) {
                throw new \Cx\Lib\FileSystem\FileSystemException();
            }
        } catch (\Cx\Lib\FileSystem\FileSystemException $e) {
            $file = $fallbackFile;
        }
        return $file;
    }

    /**
     * Returns the markup to show an image chooser form element with preview
     *
     * Automatically displays a thumbnail of the supplied image if available.
     *
     * For MediaBrowser options see
     * [Mediabrowser options](https://dev.cloudrexx.com/Mediabrowser/#options)
     *
     * @param string $fieldName Name of the form field to contain the selected image src
     * @param string \Cx\Lib\FileSystem\FileInterface|$imagePath Full path to currently selected image or empty string
     * @param string $startMediaType (optional) Media type to start MediaBrowser with
     * @param string $mediaTypes (optional) Media types available in MediaBrowser
     * @param string $startView (optional) View to start MediaBrowser with
     * @param string $views (optional) Views to show in MediaBrowser
     * @param string \Cx\Lib\FileSystem\FileInterface|$imagePlaceholderPath (optional) Full path to placeholder image to use
     * @param string \Cx\Lib\FileSystem\FileInterface|$imageThumbnailPath (optional) Full path to currently selected image's thumbnail
     * @return string HTML code
     */
    public function getImageChooserMarkup(
        string $fieldName,
        \Cx\Lib\FileSystem\FileInterface|string $imagePath,
        string $startMediaType = 'files',
        string $mediaTypes = 'all',
        string $startView = 'filebrowser',
        string $views = 'filebrowser,uploader',
        \Cx\Lib\FileSystem\FileInterface|string $imagePlaceholderPath = '',
        \Cx\Lib\FileSystem\FileInterface|string $imageThumbnailPath = ''
    ): string {
        \JS::registerCSS(substr($this->getDirectory(false, true) . '/View/Style/ImageChooser.css', 1));
        \JS::registerJS(substr($this->getDirectory(false, true) . '/View/Script/ImageChooser.js', 1));

        // Handle image placeholder file (fall back to default img)
        $imagePlaceholderFile = $this->getFileWithFallback(
            $imagePlaceholderPath,
            new \Cx\Lib\FileSystem\PrivilegedFile(
                $this->getDirectory(false) . '/View/Media/no_picture.gif'
            )
        );
        // Handle image file (fall back to placeholder file)
        $imageFile = $this->getFileWithFallback(
            $imagePath,
            $imagePlaceholderFile
        );
        // Handle automatic thumbnail image file (fall back to image file)
        if (!empty($imagePath)) {
            try {
                $imagePath = \ImageManager::getThumbnailFilename($imagePath);
            } catch (\Cx\Core\MediaSource\Model\Entity\MediaSourceManagerException $e) {
                $imagePath = '';
            }
        }
        $imageThumbnailFile = $this->getFileWithFallback(
            $imagePath,
            $imageFile
        );
        // Handle supplied thumbnail image file (fall back to automatic thumbnail)
        $imageThumbnailFile = $this->getFileWithFallback(
            $imageThumbnailPath,
            $imageThumbnailFile
        );

        // Configure MediaBrowser
        $this->setCallback('mediabrowserImagechooserCallback');
        $this->setOptions(array(
            'type'           => 'button',
            'views'          => $views,
            'startview'      => $startView,
            'startmediatype' => $startMediaType,
            'mediatypes'     => $mediaTypes,
            'style'          => 'display:none'
        ));

        // Set title
        $lang = \Env::get('init')->getComponentSpecificLanguageData(
            $this->getName(),
            false
        );
        $title = $lang['TXT_MEDIABROWSER_IMAGE_CHOOSER_CHOOSE'];
        if (!empty(basename((string) $imageFile))) {
            $title = basename((string) $imageFile);
        }

        // Push to template and output/return
        $markup = new \Cx\Core\Html\Sigma();
        $markup->loadTemplateFile(
            $this->getDirectory(false) . '/View/Template/MediaBrowserImageChooser.html'
        );
        $markup->toggleBlock('empty', (string) $imageFile == (string) $imagePlaceholderFile);
        $markup->setVariable($lang);
        $markup->setVariable(array(
            'FIELD_NAME' => contrexx_raw2xhtml($fieldName),
            'CURRENT_IMG' => contrexx_raw2xhtml($imageFile->getRelativeFilePath()),
            'CURRENT_IMG_TITLE' => contrexx_raw2xhtml($title),
            'CURRENT_IMG_THUMBNAIL' => contrexx_raw2xhtml($imageThumbnailFile->getRelativeFilePath()),
            'PLACEHOLDER_IMG' => contrexx_raw2xhtml($imagePlaceholderFile->getRelativeFilePath()),
            'MEDIA_BROWSER_BUTTON' => $this->getXHtml(),
        ));
        return $markup->get();
    }

    /**
     * Get the rendered mediabrowser button
     *
     * @param string $buttonName
     *
     * @return string
     */
    function getXHtml($buttonName = "MediaBrowser")
    {
        $button = new \Cx\Core\Html\Sigma();
        $button->loadTemplateFile($this->cx->getCodeBaseCoreModulePath() . '/MediaBrowser/View/Template/MediaBrowserButton.html');
        $button->setVariable(array(
            'MEDIABROWSER_BUTTON_NAME' => $buttonName,
            'MEDIABROWSER_BUTTON_OPTIONS' => $this->getOptionsString()
        ));
        return $button->get();
    }

    /**
     * Add a class to the button
     *
     * @param $class
     *
     * @return self
     */
    public function addClass($class) {
        $this->addOption('class', $class);
        return $this;
    }

    protected function addOption($optionName, $value) {
        $option = $this->getOption($optionName);
        $optionValues = explode(' ', $option);
        if (!in_array($value, $optionValues)) {
            $optionValues[] = $value;
        }
        $this->setOptions(array($optionName => implode(' ', $optionValues)));
    }
}
