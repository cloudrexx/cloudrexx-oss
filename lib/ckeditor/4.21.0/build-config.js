﻿/**
 * @license Copyright (c) 2003-2023, CKSource Holding sp. z o.o. All rights reserved.
 * For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license/
 */

/**
 * This file was added automatically by CKEditor builder.
 * You may re-use it at any time to build CKEditor again.
 *
 * If you would like to build CKEditor online again
 * (for example to upgrade), visit one the following links:
 *
 * (1) https://ckeditor.com/cke4/builder
 *     Visit online builder to build CKEditor from scratch.
 *
 * (2) https://ckeditor.com/cke4/builder/284a08d4a6b34eb1a3d7f0fda121a665
 *     Visit online builder to build CKEditor, starting with the same setup as before.
 *
 * (3) https://ckeditor.com/cke4/builder/download/284a08d4a6b34eb1a3d7f0fda121a665
 *     Straight download link to the latest version of CKEditor (Optimized) with the same setup as before.
 *
 * NOTE:
 *    This file is not used by CKEditor, you may remove it.
 *    Changing this file will not change your CKEditor configuration.
 */

var CKBUILDER_CONFIG = {
	skin: 'moono-lisa',
	preset: 'full',
	ignore: [
		'.DS_Store',
		'.bender',
		'.editorconfig',
		'.gitattributes',
		'.gitignore',
		'.idea',
		'.jscsrc',
		'.jshintignore',
		'.jshintrc',
		'.mailmap',
		'.npm',
		'.nvmrc',
		'.travis.yml',
		'README.md',
		'bender-err.log',
		'bender-out.log',
		'bender.ci.js',
		'bender.js',
		'dev',
		'gruntfile.js',
		'less',
		'node_modules',
		'package-lock.json',
		'package.json',
		'tests'
	],
	plugins : {
		'basicstyles' : 1,
		'bidi' : 1,
		'blockquote' : 1,
		'clipboard' : 1,
		'codemirror' : 1,
		'colorbutton' : 1,
		'colordialog' : 1,
		'contextmenu' : 1,
		'copyformatting' : 1,
		'dialogadvtab' : 1,
		'div' : 1,
		'editorplaceholder' : 1,
		'elementspath' : 1,
		'enterkey' : 1,
		'entities' : 1,
		'filebrowser' : 1,
		'find' : 1,
		'floatingspace' : 1,
		'font' : 1,
		'format' : 1,
		'horizontalrule' : 1,
		'htmlwriter' : 1,
		'image' : 1,
		'indentblock' : 1,
		'indentlist' : 1,
		'justify' : 1,
		'link' : 1,
		'list' : 1,
		'liststyle' : 1,
		'magicline' : 1,
		'maximize' : 1,
		'newpage' : 1,
		'pastefromgdocs' : 1,
		'pastefromlibreoffice' : 1,
		'pastefromword' : 1,
		'pastetext' : 1,
		'removeformat' : 1,
		'resize' : 1,
		'scayt' : 1,
		'selectall' : 1,
		'showblocks' : 1,
		'showborders' : 1,
		'sourcearea' : 1,
		'specialchar' : 1,
		'stylescombo' : 1,
		'tab' : 1,
		'table' : 1,
		'tableresize' : 1,
		'tableselection' : 1,
		'tabletools' : 1,
		'templates' : 1,
		'toolbar' : 1,
		'undo' : 1,
		'wysiwygarea' : 1
	},
	languages : {
		'da' : 1,
		'de' : 1,
		'en' : 1,
		'fr' : 1,
		'it' : 1,
		'ru' : 1
	}
};