(function(){ //autoexec-closure

//first dependencies: logic
//find correct dialog css
var lang = cx.variables.get('language', 'contrexx');
var requiredFiles = cx.variables.get('lazyLoadingFiles', 'contrexx');
//second dependencies: i18n
var datepickerI18n = cx.variables.get('datePickerI18nFile', 'jQueryUi');
if (datepickerI18n) {
    requiredFiles.push(datepickerI18n);
}

//load the css and jquery ui plugin
cx.internal.dependencyInclude(
    requiredFiles,
    function() {

        /**
         * Cloudrexx JS API: User Interface extension
         */
        var UI = function() {
            //we want jQuery at $ locally
            var $ = cx.jQuery;

            /**
             * A cloudrexx dialog.
             * 
             * @param array options {
             *     [ title: 'the title', ]
             *     content: <content-div> | 'html code',
             *     modal: <boolean>,
             *     autoOpen: <boolean> (default true)
             * }
             */
            var Dialog = function(options) {
                var opened = false; //is the dialog opened?

                //option handling
                var title = options.title;
                var content = options.content;

                var autoOpen;
                if(typeof(options.autoOpen) != "undefined")
                    autoOpen = options.autoOpen;
                else
                    autoOpen = true;
                
                var modal;
                if(typeof(options.modal) != "undefined") 
                    modal = options.modal;
                else
                    modal = false;

                var height = options.height ? options.height : 0;
                var width = options.width ? options.width : 0;
                var classname = options.dialogClass ? options.dialogClass : 0;
                var resizable = options.resizable ? options.resizable : 'true';
                var buttons = options.buttons ? options.buttons : {};
                var openHandler = options.open ? options.open : function () {};
                var closeHandler = options.close ? options.close : function () {};

                var position = options.position;

                //events the user specified handlers for
                var requestedEvents = options.events ? options.events : null;
                
                var dialogDiv;

                //event handling
                var events = new cx.tools.Events();

                //create bind to new event on the dialog for each bind request of user
                events.newBehaviour(function(name){
                    dialogDiv.dialog().bind('dialog'+name, function(){
                        events.notify(name);
                    });
                });

                var createDialogDiv = function() {

                    if(typeof(content) != 'string') { //content is a div
                        dialogDiv = $(content);
                    }
                    else { //content is a html string
                        //create a hidden div...
                        dialogDiv = $('<div></div>').css({display:'none'});
                        //...set the content and append it to the body
                        dialogDiv.html(content).appendTo('body:first');
                    }

                    if(title) //set title if specified (user could also set it in html)
                        dialogDiv.attr('title',title);

                    //remove all script tags; jquery fires DOM ready-event on dialog creation
                    //scripts have already been parsed once at this point and would be parsed
                    //twice if they're in a "jQuery(function(){...})"-statement
                    var scripts = dialogDiv.find("script").remove();

                    //the options that we pass to the jquery ui dialog constructor
                    var dialogOptions = {
                        dialogClass:classname,
                        modal:modal,
                        resizable:resizable,
                        open: function (event, ui) {
                            opened = true;
                            $J(event.target).parent().css('top', '30%');
                            $J(event.target).parent().css('position', 'fixed');
                            openHandler(event, ui);
                        },
                        close: function (event, ui) {
                            opened = false;
                            closeHandler(event, ui);
                        },
                        autoOpen:autoOpen,
                        position:position,
                        buttons:buttons
                    };          
                    //handle height and width if set
                    if(height > 0)
                        dialogOptions.height = height;

                    if(width > 0)
                        dialogOptions.width = width;
                    if(width < 600)
                        dialogOptions.width = 600;

                    var otherDialogOptions=[
                        'appendTo',
                        'classes',
                        'closeOnEscape',
                        'closeText',
                        'draggable',
                        'hide',
                        'maxHeight',
                        'maxWidth',
                        'minHeight',
                        'minWidth',
                        'show'
                    ];
                    for (option in otherDialogOptions) {
                        if (typeof(options[otherDialogOptions[option]]) != 'undefined') {
                            dialogOptions[otherDialogOptions[option]] = options[otherDialogOptions[option]];
                        }
                    }

                    //init jquery ui dialog
                    dialogDiv.dialog(dialogOptions);

                    //bind all requested events
                    if(requestedEvents) {
                        $.each(requestedEvents, function(event, handler){
                            events.bind(event, handler);
                        });
                    }
                };

                createDialogDiv();
               
                //public properties of Dialog
                return {
                    close: function() {
                        dialogDiv.dialog('close');
                    },
                    open: function() {
                        dialogDiv.dialog('open');
                    },
                    getElement: function() {
                        return dialogDiv;
                    },
                    isOpen: function() {
                        return opened;
                    },
                    bind: function(event, handler) {
                        events.bind(event, handler);
                    }
                };        
            };

            /**
             * A cloudrexx tooltip.
             */
            var Tooltip = function(element) {

                selectors = new Array();
                if (typeof element != "undefined") {
                    selectors.push(element);
                } else {
                    // set default selector
                    selectors.push('.tooltip-trigger');
                }

                cx.jQuery(selectors).each(function(i, selector) {

                    cx.jQuery(selector).each(function(i, element) {

                        defaultOptions = {
                            "position": {
                                "x": "right",
                                "y": "center"
                            },
                            "offset": {
                                "x": 10,
                                "y": 0
                            },
                            "predelay": 250,
                            "relative": true
                        };

                        if ((options = cx.jQuery(this).data('tooltip-options')) && (typeof options != "undefined")) {

                            if (typeof options.position != "undefined") {
                                if (typeof options.position.x != "undefined") {
                                    defaultOptions.position.x = options.position.x;
                                }
                                if (typeof options.position.y != "undefined") {
                                    defaultOptions.position.y = options.position.y;
                                }
                            }

                            if (typeof options.offset != "undefined") {
                                if (typeof options.offset.x != "undefined") {
                                    defaultOptions.offset.x = options.offset.x;
                                }
                                if (typeof options.offset.y != "undefined") {
                                    defaultOptions.offset.y = options.offset.y;
                                }
                            }

                            if (typeof options.predelay != "undefined") {
                                defaultOptions.predelay = options.predelay;
                            }

                            if (typeof options.relative != "undefined") {
                                defaultOptions.relative = options.relative;
                            }

                        }

                        cx.jQuery(element).tooltip({
                            position: defaultOptions.position.y+' '+defaultOptions.position.x,
                            offset: [defaultOptions.offset.y, defaultOptions.offset.x],
                            predelay: defaultOptions.predelay,
                            relative: defaultOptions.relative,
                            onShow: function(objEvent) {
                                if (cx.variables.get("mode") != "backend") {
                                    return;
                                }
                                // get pos of trigger
                                let top = this.getTrigger()[0].getBoundingClientRect().top
                                let ancestor = element.parentElement;
                                while (ancestor && getComputedStyle(ancestor).position === 'static') {
                                    ancestor = ancestor.parentElement;
                                }
                                if (ancestor) {
                                    // correct pos as tooltip top is relative to content
                                    top = top - ancestor.getBoundingClientRect().y;
                                    // account for scroll offset
                                    top = top + ancestor.scrollTop;
                                }
                                // account for desired position
                                switch (this.getConf().position[0]) {
                                    case 'top':
                                        top = top - this.getTip()[0].offsetHeight;
                                        break;
                                    case 'center':
                                        top = top - (this.getTip()[0].offsetHeight / 2) + (this.getTrigger()[0].offsetHeight / 2);
                                        break;
                                    case 'bottom':
                                        top = top + this.getTrigger()[0].offsetHeight;
                                        break;
                                }
                                // ensure tooltip does not go outside upper side of content
                                if (top < 10) {
                                    top = 10;
                                }
                                this.getTip()[0].style.top = top + "px";
                            }
                        }).dynamic();

                    });

                });

            };

            var Expand = function(){
                
                var findNextExpanding = function(ele){
                    var element = jQuery(ele).next('.cx-expanding');
                    while(element.length < 1){
                        if(jQuery(ele).next('.cx-expanding').length > 0) {
                            element = jQuery(ele).next('.cx-expanding').first();
                        } else {
                            ele = jQuery(ele).parent();
                        }
                        if ( typeof element == 'undefined' ) {
                            jQuery(ele).children().each(function() {
                                if(jQuery(this).find('.cx-expanding').length > 0){
                                    element = jQuery(this).find('.cx-expanding').first();
                                }
                            });
                        }
                    }

                    return element;
                };
                
                var setAsUp = function(ele, isInitialising){
                    jQuery(ele).removeClass('cx-expandUp');
                    jQuery(ele).addClass('cx-expandDown');
                    jQuery(ele).children('span.cx-expandDownText').css('display', 'inline');
                    jQuery(ele).children('span.cx-expandUpText').css('display', 'none');
                    if(true == isInitialising){
                       findNextExpanding(ele).fadeOut().css('display', 'none');
                    }else{
                        findNextExpanding(ele).fadeOut();
                    }
                };
                
                var setAsDown = function(ele){
                    jQuery(ele).removeClass('cx-expandDown');
                    jQuery(ele).addClass('cx-expandUp');
                    jQuery(ele).children('span.cx-expandDownText').css('display', 'none');
                    jQuery(ele).children('span.cx-expandUpText').css('display', 'inline');
                    findNextExpanding(ele).fadeIn();
                };
                
                jQuery('.cx-expand').each(function(){
                    setAsUp(this, true);
                    jQuery(this).click(function(){
                        if(jQuery(this).hasClass('cx-expandDown')){
                            setAsDown(this);
                        }else{
                            setAsUp(this);
                        }
                    });
                });
            }

            var messages = {
                add: function(msg, type) {
                    cx.tools.StatusMessage.removeAllDialogs();
                    cx.tools.StatusMessage.showMessage(msg, type, 10000);
                },
                showLoad: function() {
                    cx.tools.StatusMessage.removeAllDialogs();
                    cx.tools.StatusMessage.showMessage("<div id=\"loading\"><img src=\"" + cx.variables.get('basePath', 'contrexx') +  "lib/javascript/jquery/jstree/themes/default/throbber.gif\" alt=\"\" /><span>Loading...</span></div>");
                },
                removeAll: function() {
                    cx.tools.StatusMessage.removeAllDialogs();
                }
            };
            
            var confirmFunction = function(text, type) {
                if (text == undefined) {
                    text = cx.variables.get(type, "contrexx/lang");
                }
                return confirm(text);
            };
            
            var forms = {
                // get all cx ui forms or a specific one
                get: function(id) {
                    if (id != undefined) {
                        return cx.jQuery("#form-" + id);
                    }
                    return cx.jQuery("form.cx-ui");
                },
                // validates a form, usage: cx.ui.forms.validate(cx.ui.forms.get(<id>));
                validate: function(form, skipMessage) {
                    if (!form) {
                        return false;
                    }
                    form.find("input,textarea,select").each(function(index, el) {
                        if (!cx.jQuery(el).is(".chosen-choices *")) {
                            cx.jQuery(el).trigger("keyup");
                        }
                    });
                    if (form.find(".error").length) {
                        if (!skipMessage) {
                            cx.ui.messages.add(
                                cx.variables.get("Form/Error", "core/Html/lang"),
                                "error"
                            );
                        }
                        return false;
                    }
                    return true;
                },
                // fix form ids, this is necessary if forms are loaded via ajax
                fix: function() {
                    var ids = [];
                    var needFix = [];
                    cx.jQuery.each(cx.ui.forms.get(), function(index, el) {
                        var id = cx.jQuery(el).attr("id").split("-")[1];
                        if (ids.indexOf(id) != -1) {
                            needFix.push(el);
                            return;
                        }
                        ids.push(id);
                    });
                    if (!needFix.length) {
                        return;
                    }
                    var i = ids.sort().pop();
                    i++;
                    cx.jQuery.each(needFix, function(index, el) {
                        cx.jQuery(el).attr("id", "form-" + i);
                        // This fixes tablist names (avoid duplicate tablist names with modals)
                        for (const tab of el.querySelectorAll("[data-tablist]")) {
                            let tablistParts = tab.dataset["tablist"].split("-");
                            if (i == tablistParts[1]) {
                                continue;
                            }
                            tablistParts[1] = i;
                            tab.dataset["tablist"] = tablistParts.join("-");
                            // during init of cx.ui.tabs this list did not exist, add it
                            if (!cx.ui.tabs.lists.includes(tab.dataset["tablist"])) {
                                cx.ui.tabs.lists.push(tab.dataset["tablist"]);
                            }
                            cx.ui.tabs.select(tab.dataset["tablist"]);
                        }
                        i++;
                    });
                    cx.trigger('Html:postFormFix', 'ViewGenerator');
                },
                updateRelation: function(element, internal) {
                    if (internal !== undefined) {
                        element.parents(".group").toggle(internal);
                        if (0 > cx.jQuery.inArray(element.prop("tagName").toLowerCase(), [
                            "input",
                            "select",
                            "textarea"
                        ])) {
                            element = element.find("input, select, textarea");
                        }
                    } else {
                        internal = true;
                        element = cx.jQuery(element);
                    }
                    var relationId = element.data("relatingid");
                    var condition = element.data("condition");
                    if (!relationId || !condition) {
                        return;
                    }
                    var value = element.val();
                    var cond = new RegExp("^" + condition + "$");
                    var show = cond.test(value) && internal;
                    cx.ui.forms.updateRelation(cx.jQuery(relationId), show);
                }
            };

            var intro = {
                /**
                 * Triggers the intro with all steps in group
                 * If no group is specified all steps are shown
                 * @todo: change this from cookie based to user based
                 * @param string group Optional group name to limit steps to.
                 *                      If a group is specified already shown
                 *                      steps are repeated.
                 * @param boolean force Internal, do not use!
                 */
                show: function(group, force) {
                    steps = cx.variables.get("steps", "View/intro");
                    if (!steps) {
                        return;
                    }
                    if (force == undefined) {
                        force = true;
                    }
                    if (group == undefined) {
                        cx.jQuery.each(steps, function(index, el) {
                            cx.ui.intro.show(index, false);
                        });
                        return;
                    }
                    if (!steps[group]) {
                        return;
                    }
                    steps = steps[group];

                    try {
                        var seenIntros = JSON.parse(Cookies.get("view-intro"));
                    } catch(e) {}

                    if (!seenIntros) {
                        seenIntros = {};
                    }
                    // refresh cookie
                    Cookies.set("view-intro", JSON.stringify(seenIntros), {expires: 365});

                    if (!seenIntros[group]) {
                        seenIntros[group] = {}
                    }

                    // buildup array and if time is greater than any of the array: skip
                    var selectedSteps = [];
                    var newSeenSteps = seenIntros[group];
                    cx.jQuery.each(steps, function(index, el) {
                        if (seenIntros[group] & (1<<index) && !force) {
                            return true; // continue
                        }
                        if (!cx.jQuery(el.element).length) {
                            return true; // continue
                        }
                        newSeenSteps = newSeenSteps | (1<<index);
                        selectedSteps.push(el);
                    });
                    if (!selectedSteps.length) {
                        return;
                    }
                    const intro = introJs();
                    intro.setOptions({
                        nextLabel: cx.variables.get("TXT_CORE_VIEW_INTRO_NEXT", "Core/lang"),
                        prevLabel: cx.variables.get("TXT_CORE_VIEW_INTRO_BACK", "Core/lang"),
                        skipLabel: cx.variables.get("TXT_CORE_VIEW_INTRO_STOP", "Core/lang"),
                        doneLabel: cx.variables.get("TXT_CORE_VIEW_INTRO_STOP", "Core/lang"),
                        disableInteraction:true,
                        showStepNumbers: false,
                        tooltipPosition: "auto",
                        steps: selectedSteps
                    });
                    const setSeen = function() {
                        seenIntros[group] = newSeenSteps;
                        Cookies.set("view-intro", JSON.stringify(seenIntros), {expires: 365});
                    }
                    introJs.fn.oncomplete(setSeen);
                    introJs.fn.onexit(setSeen);
                    intro.start();
                }
            }

            const Tabs = function() {
                // initialize lists
                let tablists = [];
                for (const el of document.querySelectorAll("[data-tablist]")) {
                    const tablist = el.dataset["tablist"];
                    if (!tablists.includes(tablist)) {
                        tablists.push(tablist);
                    }
                }
                const getTabName = function(tabElement) {
                    if (!tabElement.classList.contains("tab")) {
                        throw new Error("Element is not a tab");
                    }
                    return Array.from(tabElement.classList).filter(function (i) {
                        return i != "tab" && i != "active" && i != "adminlist";
                    }).join(".");
                };
                const updateHistory = function(tablistName, tabName, historyMode) {
                    // update url (history)
                    if (historyMode == "ignore") {
                        return;
                    }
                    const oldTabState = (history.state && history.state.hasOwnProperty("cx-ui-tabs")) ? history.state["cx-ui-tabs"] : {}
                    let newState = structuredClone(history.state || {});
                    if (typeof newState != "object") {
                        // history seems to be controlled by some other code
                        return;
                    }
                    if (!newState.hasOwnProperty("cx-ui-tabs")) {
                        newState["cx-ui-tabs"] = {};
                    }
                    newState["cx-ui-tabs"][tablistName] = tabName;
                    if (JSON.stringify(newState["cx-ui-tabs"]) === JSON.stringify(oldTabState)) {
                        return;
                    }
                    let url = new URL(window.location);
                    url.searchParams.set("tab-" + tablistName, tabName);
                    if (historyMode == "replace") {
                        history.replaceState(
                            newState,
                            null,
                            url
                        );
                    } else {
                        history.pushState(
                            newState,
                            null,
                            url
                        );
                    }
                }
                const getTabNameFromElement = function(tabElement) {
                    return Array.from(tabElement.classList).filter(function (i) {
                        return i != "tab" && i != "active" && i != "adminlist" && i != "dependency-met";
                    }).join(".");
                };
                const getTabNames = function(tablistName) {
                    let tabNames = [];
                    for (
                        const tab of document.querySelectorAll(
                            ".tab[data-tablist=" + tablistName + "]:not([style*='display: none'])"
                        )
                    ) {
                        if (tab.dataset && tab.dataset.depends && !tab.classList.contains("dependency-met")) {
                            continue;
                        }
                        tabNames.push(getTabNameFromElement(tab));
                    }
                    return tabNames;
                };
                // add event listeners
                cx.bind(
                    "selectTab", 
                    function(args) {
                        const tablistName = args["tablist"];
                        if (!tablists.includes(tablistName)) {
                            return;
                        }
                        let tabName = args["tabname"];
                        const tabNames = getTabNames(tablistName);
                        if (!tabNames.length) {
                            console.info("No non-hidden tab to show for list", tablistName);
                            return;
                        }
                        if (!tabNames.includes(tabName)) {
                            tabName = tabNames[0];
                        }
                        // select tab
                        const tabToSelect = document.querySelectorAll(".tab[data-tablist=" + tablistName + "]." + tabName);
                        if (!tabToSelect) {
                            console.error("No tab named \"" + tabName + "\" in list \"" + tablistName + "\"");
                            return;
                        }
                        for (const tab of document.querySelectorAll(".tab[data-tablist=" + tablistName + "]")) {
                            tab.classList.remove("active");
                        }
                        for (const tab of tabToSelect) {
                            tab.classList.add("active");
                        }
                        // set tab links active/inactive
                        for (
                            const el of document.querySelectorAll(
                                "[data-tablist=" + tablistName + "] [data-tablink], [data-tablist=" + tablistName + "][data-tablink]"
                            )
                        ) {
                            if (
                                !el.dataset["tablist"] &&
                                el.closest("[data-tablist]").dataset["tablist"] != tablistName
                            ) {
                                continue;
                            }
                            el.classList.remove("active");
                        }
                        for (
                            const el of document.querySelectorAll(
                                "[data-tablist=" + tablistName + "] [data-tablink=" + tabName + "], [data-tablist=" + tablistName + "][data-tablink=" + tabName + "]"
                            )
                        ) {
                            el.classList.add("active");
                        }
                        for (const el of document.querySelectorAll("#content form[action]")) {
                            if (el.dataset.fixedTablists) {
                                const fixedTablists = el.dataset.fixedTablists.split(",");
                                if (fixedTablists.includes(tablistName)) {
                                    continue;
                                }
                            }
                            if (el.method == "get") {
                                let tabEl = el.querySelector("input[name=" + "tab-" + tablistName + "]");
                                if (!tabEl) {
                                    tabEl = document.createElement("input");
                                    tabEl.type = "hidden";
                                    tabEl.name = "tab-" + tablistName;
                                    el.append(tabEl);
                                }
                                tabEl.value = tabName;
                            } else {
                                const url = new URL(el.getAttribute("action"), document.location.origin);
                                url.searchParams.set("tab-" + tablistName, tabName);
                                el.setAttribute("action", url);
                            }
                        }
                        let historyMode = "";
                        if (args.hasOwnProperty("history")) {
                            historyMode = args.history;
                        }
                        updateHistory(tablistName, tabName, historyMode);
                        cx.trigger(
                            "tabSelected",
                            "cx.ui.tabs",
                            {
                                "tablist": tablistName,
                                "tabname": tabName,
                            }
                        );
                    },
                    "cx.ui.tabs"
                );
                for (const el of document.querySelectorAll("[data-tablist] [data-tablink], [data-tablist][data-tablink]")) {
                    let tablist = "";
                    if (el.dataset["tablist"]) {
                        tablist = el.dataset["tablist"];
                    } else {
                        tablist = el.closest("[data-tablist]").dataset["tablist"];
                    }
                    // If tab has no content, drop tab
                    if (!document.querySelector("." + el.dataset.tablink + ".tab[data-tablist=" + tablist + "]")) {
                        console.error("Tab without content encountered", tablist, el.dataset.tablink);
                        el.remove();
                    }
                    el.dataset["tabTitle"] = el.text;
                    el.addEventListener("click", function(e) {
                        e.preventDefault();
                        cx.trigger(
                            "selectTab",
                            "cx.ui.tabs",
                            {
                                "tablist": tablist,
                                "tabname": el.dataset["tablink"],
                            }
                        );
                    });
                }
                // register history hook
                window.addEventListener("popstate", function(e) {
                    if (!e.state || !e.state.hasOwnProperty("cx-ui-tabs")) {
                        return;
                    }
                    for (tablistName in e.state["cx-ui-tabs"]) {
                        cx.trigger(
                            "selectTab",
                            "cx.ui.tabs",
                            {
                                "tablist": tablistName,
                                "tabname": e.state["cx-ui-tabs"][tablistName],
                                "history": "ignore",
                            }
                        );
                    }
                });
                // show first tab of each list
                const params = (new URL(document.URL)).searchParams;
                for (const tablistName of tablists) {
                    let selectedTab = "";
                    if (params.has("tab-" + tablistName)) {
                        selectedTab = params.get("tab-" + tablistName);
                    }
                    cx.trigger(
                        "selectTab",
                        "cx.ui.tabs",
                        {
                            "tablist": tablistName,
                            "tabname": selectedTab,
                            "history": "replace",
                        }
                    );
                }
                const getTabInfoFromElement = function(el) {
                    // 1. element is a tab link (might be within tablist)
                    // 2. element is a child of a tab link
                    if (el.closest("[data-tablink]")) {
                        el = el.closest("[data-tablink]");
                        if (el.dataset.tablist) {
                            return {
                                "tablist": el.dataset["tablist"],
                                "tabname": el.dataset.tablink,
                                "type": "link",
                                "element": el,
                            };
                        }
                        return {
                            "tablist": el.closest("[data-tablist]").dataset.tablist,
                            "tabname": el.dataset.tablink,
                            "type": "link",
                            "element": el,
                        };
                    }
                    // 3. element is a tab container
                    // 4. element is a child of a tab container
                    if (el.closest(".tab")) {
                        el = el.closest(".tab");
                        return {
                            "tablist": el.dataset.tablist,
                            "tabname": getTabNameFromElement(el),
                            "type": "tab",
                            "element": el,
                        };
                    }
                    return {};
                };
                return {
                    "lists": tablists,
                    "getSelectedTabName": function(tablistName) {
                        const selectedTab = document.querySelector("[data-tablist=" + tablistName + "].tab.active");
                        if (!selectedTab) {
                            return "";
                        }
                        return getTabNameFromElement(selectedTab);
                    },
                    "getTabNames": getTabNames,
                    "select": function(tablistName, tabName) {
                        if (!tabName) {
                            tabName = "";
                        }
                        cx.trigger(
                            "selectTab",
                            "cx.ui.tabs",
                            {
                                "tablist": tablistName,
                                "tabname": tabName,
                            }
                        );
                    },
                    /**
                     * Returns tab info for a given element
                     *
                     * The element can be:
                     * 1. a tab link (might be within tablist)
                     * 2. a child of a tab link
                     * 3. a tab container
                     * 4. a child of a tab container
                     * 5. none of the above
                     *
                     * The return value is an object with the following keys:
                     * - "tablist": Name of the tablist the element relates to
                     * - "tabname": Name of the tab the element relates to
                     * - "type": Either "link" (cases 1 & 2) or "tab" (cases 3 & 4)
                     * - "element": The link element or the tab container element
                     *
                     * If the element is within nested tabs, the closest tab info
                     * is returned.
                     *
                     * If the element is not part of a tab or tab link an empty
                     * object is returned.
                     *
                     * @param el Element to get tab info of
                     * @return object
                     */
                    "getTabInfoFromElement": getTabInfoFromElement,

                    /**
                     * Selects tab(s) based on an element.
                     *
                     * The element can be within tab content or the tab container
                     * itself or within the tab list or any link to a tab. If
                     * tabs are nested all higher level tabs are selected so that
                     * the element is shown.
                     * @param el Element to show tab of
                     */
                    "selectByElement": function(el) {
                        let tabsToSelect = [];
                        let i = 0; // just to make sure we never get an endless loop
                        while (true) {
                            const tabinfo = getTabInfoFromElement(el);
                            i++;
                            if (i > 10) {
                                console.error("Tab nesting limit reached!");
                                break;
                            }
                            if (!Object.keys(tabinfo).length) {
                                break;
                            }
                            tabsToSelect.push(tabinfo);
                            el = tabinfo.element.parentElement;
                        };
                        for (const tabinfo of tabsToSelect) {
                            cx.ui.tabs.select(tabinfo.tablist, tabinfo.tabname);
                        }
                    },
                };
            }

            //public properties of UI
            return {
                confirm: confirmFunction,
                dialog: function(options)
                {
                    return new Dialog(options);
                },
                tooltip: function(element)
                {
                    return new Tooltip(element);
                },
                expand: function(){
                    return new Expand();
                },
                messages: messages,
                forms: forms,
                intro: intro,
                tabs: new Tabs
            };
        };

        //add the functionality to the global cx object
        cx.ui = new UI();

        //initialize tooltips
        cx.ui.tooltip();
        
        //initialize expands
        cx.ui.expand();

        // show intro(s)
        cx.ui.intro.show();
    }, //end of dependencyInclude: callback
    true //dependencyInclude: chain
);

cx.ready(function() {
    function toggleElement(el, dependency) {
        if (dependency.checked) {
            el.classList.add("dependency-met");
            if (
                el.classList.contains("tab") &&
                cx.ui.tabs.getSelectedTabName(el.dataset.tablist) == ""
            ) {
                cx.trigger("selectTab", "cx.ui.tabs", {
                    "tablist": el.dataset.tablist,
                    "tabname": ""
                });
            }
        } else {
            el.classList.remove("dependency-met");
            if (el.classList.contains("tab")) {
                cx.trigger("selectTab", "cx.ui.tabs", {
                    "tablist": el.dataset.tablist,
                    "tabname": cx.ui.tabs.getSelectedTabName(el.dataset.tablist)
                });
            }
        }
    }
    for (const el of document.querySelectorAll("[data-depends]")) {
        const dependency = document.getElementById(el.dataset.depends);
        toggleElement(el, dependency);
        let dependencyElements = [dependency];
        if (dependency.matches("[type=radio]")) {
            dependencyElements = document.querySelectorAll("input[type=radio][name=" + dependency.name + "]");
        }
        for (dependencyElement of dependencyElements) {
            dependencyElement.addEventListener("change", function(e) {
                toggleElement(el, dependency);
            });
        }
    }
});

//end of autoexec-closure
})();
