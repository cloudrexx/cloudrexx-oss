<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * A response to a request
 * @copyright   CLOUDREXX CMS - CLOUDREXX AG
 * @author      Michael Ritter <michael.ritter@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  lib_net
 * @link        http://www.cloudrexx.com/ cloudrexx homepage
 * @since       5.0.0
 */

namespace Cx\Lib\Net\Model\Entity;

/**
 * An exception while sending a response
 * @copyright   CLOUDREXX CMS - CLOUDREXX AG
 * @author      Michael Ritter <michael.ritter@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  lib_net
 * @link        http://www.cloudrexx.com/ cloudrexx homepage
 * @since       5.0.0
 */
class ResponseException extends \Exception {}

/**
 * A response to a request
 * @copyright   CLOUDREXX CMS - CLOUDREXX AG
 * @author      Michael Ritter <michael.ritter@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  lib_net
 * @link        http://www.cloudrexx.com/ cloudrexx homepage
 * @since       5.0.0
 */
class Response {
    /**
     * @var \DateTime Expiration date
     */
    protected $expirationDate;

    /**
     * @var string Content type
     */
    protected $contentType = 'text/plain';

    /**
     * @var array   Policy-Directives to be set as Content-Security-Policy
     *              header. The array has the following structure:
     *              [
     *                  <directive> =>  [
     *                      <value>,
     *                      ...
     *                  ],
     *                  <directive> =>  [
     *                      ...
     *                  ],
     *                  ...
     *              ]
     */
    protected $contentSecurityPolicy = array();

    /**
     * @var array   Policy-Directives to be set as Content-Security-Policy-Report-Only
     *              header. The array has the following structure:
     *              [
     *                  <directive> =>  [
     *                      <value>,
     *                      ...
     *                  ],
     *                  <directive> =>  [
     *                      ...
     *                  ],
     *                  ...
     *              ]
     */
    protected $contentSecurityPolicyReportOnly = array();

    /**
     * @var Request (optional) Request
     */
    protected $request = null;

    /**
     * @var int Response code
     */
    protected $code;

    /**
     * @var object|callable Parser to parse abstract content
     */
    protected $parser;

    /**
     * @var string Message
     */
    protected $message;

    /**
     * @var mixed Abstract response data
     */
    protected $abstractContent;

    /**
     * @var string Response data
     */
    protected string $parsedContent = '';

    /**
     * @var array List of headers (key=>value)
     */
    protected $headers = array();

    /**
     * @var bool True if the response has parsed content (is reset when parser or abstract content changes)
     */
    protected bool $isParsed = false;

    /**
     * Creates a new Response
     * @param mixed $abstract Abstract response data
     * @param int $code (optional) Response code, default is 200
     * @param Request $request (optional) Request object, default is null
     * @param \DateTime $expirationDate (optional) Expire date for this response, default is null
     */
    public function __construct($abstractContent, $code = 200, $request = null, $expirationDate = null) {
        $this->setAbstractContent($abstractContent);
        $this->setCode($code);
        $this->setRequest($request);
        $this->setExpirationDate($expirationDate);
    }

    /**
     * Returns the expiration date for this Response
     * @return \DateTime Expire date
     */
    public function getExpirationDate() {
        return $this->expirationDate;
    }

    /**
     * Sets the expiration date for this Response
     * @param \DateTime $expirationDate Expiration date (can be set to null)
     */
    public function setExpirationDate($expirationDate) {
        $this->expirationDate = $expirationDate;
    }

    /**
     * Returns the content type
     * @return String Content type
     */
    public function getContentType() {
        return $this->contentType;
    }

    /**
     * Sets the content type
     * @param string $contentType Content type
     */
    public function setContentType($contentType) {
        $this->contentType = $contentType;
        $this->headers['Content-Type'] = $contentType;
    }

    /**
     * Set the Content-Disposition of the response.
     * @param   string  $disposition    Either 'inline' or 'attachment'
     */
    public function setContentDisposition($disposition) {
        if (!in_array(
            $disposition,
            array('inline', 'attachment')
        )) {
            return;
        }
        $this->headers['Content-Disposition'] = $disposition;
    }

    /**
     * Get the Content-Disposition of the response.
     * Defaults to 'inline' in case non has been set.
     * @return string Either 'inline' or 'attachment'
     */
    public function getContentDisposition() {
        if (!isset($this->headers['Content-Disposition'])) {
            return 'inline';
        }

        return $this->headers['Content-Disposition'];
    }

    /**
     * Get the directives of the set Content-Security-Policy of the response.
     * @return  array   The directives and their set values.
     */
    public function getContentSecurityPolicy() {
        return $this->contentSecurityPolicy;
    }

    /**
     * Update the Content-Security-Policy header of the response based on the
     * set directives set through setContentSecurityPolicyDirective().
     *
     * @param bool $reportOnly If set to `true`, then the Content-Security-Policy-Report-Only
     *     header is set instead of the Content-Security-Policy header.
     */
    protected function updateContentSecurityPolicy(bool $reportOnly = false) {
        if ($reportOnly) {
            $cspHeader = 'Content-Security-Policy-Report-Only';
            $policy = $this->contentSecurityPolicyReportOnly;
        } else {
            $cspHeader = 'Content-Security-Policy';
            $policy = $this->contentSecurityPolicy;
        }
        $policies = array();
        foreach ($policy as $directive => $values) {
            $value = join(' ', $values);
            if (!$value && $directive != 'upgrade-insecure-requests') {
                continue;
            }
            $policies[] = $directive . ' ' . $value;
        }
        if (
            !$policies
            && isset($this->headers[$cspHeader])
        ) {
            unset($this->headers[$cspHeader]);
            return;
        }
        $this->headers[$cspHeader] = join('; ', $policies);
    }

    /**
     * Set a Content-Security-Policy directive for the response.
     * @param   string  $directive The CSP directive to set
     * @param   array   $values     The values to set the CSP directive to
     */
    public function setContentSecurityPolicyDirective($directive, array $values, bool $reportOnly = false) {
        array_walk(
            $values,
            function(&$item, $key) {
                if (
                    in_array(
                        $item,
                        array(
                            'none',
                            'self',
                            'strict-dynamic',
                            'report-sample',
                            'unsafe-inline',
                            'unsafe-eval',
                            'unsafe-hashes',
                            'unsafe-allow-redirects',
                        )
                    )
                    || preg_match('/^(sha(256|384|512)-|nonce-[a-f0-9]{64}$)/', $item)
                ) {
                    $item = "'" . $item . "'";
                }
            }
        );
        if ($reportOnly) {
            $this->contentSecurityPolicyReportOnly[$directive] = $values;
        } else {
            $this->contentSecurityPolicy[$directive] = $values;
        }
        $this->updateContentSecurityPolicy($reportOnly);
    }

    /**
     * Returns the Request object for this Response
     * @return \Request Request this Response is for
     */
    public function getRequest() {
        return $this->request;
    }

    /**
     * Sets the Request object for this Response
     * @param \Request $request Request this Response is for
     */
    public function setRequest($request) {
        $this->request = $request;
    }

    /**
     * Returns the response code
     * @return int Response code
     */
    public function getCode() {
        return $this->code;
    }

    /**
     * Sets the response code
     * @param int $code Response code
     */
    public function setCode($code) {
        $this->code = $code;
    }

    /**
     * Returns the message
     * Message may be used for user interaction or debugging and is "part" of abstract content
     * @return string Message
     */
    public function getMessage() {
        return $this->message;
    }

    /**
     * Sets the message
     * Message may be used for user interaction or debugging and is "part" of abstract content
     * @param string $message Message for user interaction or debugging
     */
    public function setMessage($message) {
        $this->message = $message;
    }

    /**
     * Returns the abstract response data
     * @return mixed Abstract response data
     */
    public function getAbstractContent() {
        return $this->abstractContent;
    }

    /**
     * Sets abstract response data
     * @param mixed $abstractContent Abstract response data
     */
    public function setAbstractContent($abstractContent) {
        $this->abstractContent = $abstractContent;
        $this->isParsed = false;
    }

    /**
     * Sets the parser
     * This can be an inline function or a class with a method like:
     * string public function(Response $response);
     * Parser needs to return parsed content and set content type
     * @param object|callable $parser Parser to parse abstract content
     */
    public function setParser($parser) {
        $this->parser = $parser;
        $this->isParsed = false;
    }

    /**
     * Returns the parser
     * @return object|callable Parser
     */
    public function getParser() {
        return $this->parser;
    }

    /**
     * Parses this response
     * @todo: Check if casting of parser result is still necessary
     */
    public function parse() {
        if (is_callable($this->getParser())) {
            $parser = $this->getParser();
            $this->parsedContent = (string) $parser($this);
            $this->isParsed = true;
            return;
        }
        $this->parsedContent = (string) $this->getParser()->parse($this);
        $this->isParsed = true;
    }

    /**
     * Returns the parsed response data
     * @return string Parsed response data
     */
    public function getParsedContent(): string {
        if (!$this->isParsed) {
            $this->parse();
        }
        return $this->parsedContent;
    }

    /**
     * Sets parsed response data
     *
     * This should only be used directly for edge-cases. One example would be
     * if we're just forwarding an already parsed response (proxy / internal
     * redirect). All normal requests (frontend, backend, api) should set
     * abstract content and a parser instead!
     * @todo: Check if casting $parsedContent is still necessary
     * @param string $parsedContent Parsed response data
     */
    public function setParsedContent($parsedContent): void {
        $this->parsedContent = (string) $parsedContent;
        \DBG::debug('Setting parsed response content directly!');
        $this->isParsed = true;
    }

    /**
     * Sets a header
     * For 'Content-Type' or 'Expires' headers please use
     * setContentType() and setExpirationDate()
     * @param string $key Header key
     * @param string $value Header value
     * @throws ResponseException When trying to set 'Content-Type' or 'Expires' header
     */
    public function setHeader($key, $value) {
        if ($key == 'Content-Type') {
            throw new ResponseException('Please use setContentType()');
        } else if ($key == 'Expires') {
            throw new ResponseException('Please use setExpirationDate()');
        } else if ($key == 'Content-Security-Policy') {
            throw new ResponseException('Please use setContentSecurityPolicyDirective()');
        }
        if (empty($value)) {
            unset($this->headers[$key]);
        } else {
            $this->headers[$key] = $value;
        }
    }

    /**
     * Returns a header's value
     * @param string $key Header key
     * @throws ResponseException When trying to get a non-set header
     * @return string Header value
     */
    public function getHeader($key) {
        if (!isset($this->headers[$key])) {
            throw new ResponseException('No such header set');
        }
        return $this->headers[$key];
    }

    /**
     * Returns a list of headers
     * Please note that this does not include 'Content-Type' and 'Expires' headers
     * @return array Key=>value type array
     */
    public function getHeaders() {
        return $this->headers;
    }

    /**
     * Exposes any set HTTP header to the client and returns this response to
     * the client or returns it as string if $finishRequest is set to false.
     * @param  boolean $finishRequest  Defaults to true to end cx execution by
     *                 returning the parsed content to the client and throwing
     *                 an InstanceException.
     * @return string  Parsed response.
     * @throws \Cx\Core\Core\Controller\InstanceException
     * @throws ResponseException If content type of response is not set.
     */
    public function send($finishRequest = true) {
        if (
            !$this->isParsed ||
            empty($this->getContentType())
        ) {
            $this->parse();
        }
        if (empty($this->getContentType())) {
            throw new ResponseException('Content type is not set');
        }
        header('Content-Type: ' . $this->getContentType());
        if ($this->getExpirationDate()) {
            header('Expires: ' . $this->getExpirationDate()->format('r'));
        }
        foreach ($this->getHeaders() as $key=>$value) {
            header($key . ': ' . $value);
        }
        if ($finishRequest) {
            echo $this->getParsedContent();
            throw new \Cx\Core\Core\Controller\InstanceException();
        }
        return $this->getParsedContent();
    }
}
