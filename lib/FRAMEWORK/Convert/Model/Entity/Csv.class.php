<?php declare(strict_types=1);

/**
 * Cloudrexx
 *
 * @link      https://www.cloudrexx.com
 * @copyright Cloudrexx AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

namespace Cx\Lib\Convert\Model\Entity;

/**
 * Represents data loaded from or stored in CSV format
 * @copyright   Cloudrexx AG
 * @author      Michael Ritter <michael.ritter@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  lib_convert
 */
class Csv {
    protected string $separator;
    protected string $enclosure;

    /**
     * Constructor
     *
     * @param string $separator (optional) Separator character to use, defaults to `;`
     * @param string $enclosure (optional) Enclosure character to use, defaults to `"`
     */
    public function __construct(string $separator = ';', string $enclosure = '"') {
        $this->separator = $separator;
        $this->enclosure = $enclosure;
    }

    /**
     * Returns the content of a csv file indexed by the first row containing data
     *
     * Please note that this method will attempt to write to the file if no
     * UTF-8 BOM is detected.
     *
     * If $limit and $offset are used:
     * - If $columnNamesInFirstRow is true, the first row is always read
     * - If $columnNamesInFirstRow is true, this row does not count towards the limit
     * - Empty rows are skipped and do not count towards limit or offset
     *
     * The $callback will be called for each non-empty row. It will receive the
     * parsed CSV data (indexed by header rows if $columnNamesInFirstRow is
     * true) and the number of the entry (nth non-empty entry).
     * @param \Cx\Lib\FileSystem\File $file File instance to read CSV from
     * @param Callable $callback Will be called for each row if provided.
     * @param bool $columnNamesInFirstRow (optional) If set to false, the first row is treated as data
     * @param int $limit (optional) Limit the number of rows returned.
     * @param int $offset (optional) Offset as of which entry limit is counted.
     * @throws \Cx\Lib\File\System\FileException If the file could not be opened
     */
    public function parseFile(
        \Cx\Core\Core\Controller\Cx $cx,
        \Cx\Lib\FileSystem\File $file,
        Callable $callback,
        bool $columnNamesInFirstRow = true,
        int $limit = -1,
        int $offset = -1
    ): void {
        $handle = $file->getHandle();

        // PHP 8.1 breaks UTF-8 BOM detection (see https://github.com/php/php-src/pull/10373)
        // in mb_detect_encoding(). This should be fixed again in PHP 8.3.
        // However, checking for the BOM beforehand allows us to skip further
        // encoding checks (see TODO below) if the file has a UTF-8 BOM.
        // The BOM is then handled by fgetcsv().
        //
        // As our internal charset is always UTF-8 we only need to check encoding
        // of files without a UTF-8 BOM.
        // @todo: find a way to detect file encoding without loading the whole file at once
        if (!$this->startsWithUtf8Bom($handle)) {
            $importFileName = $cx->getWebsiteCachePath() . '/Convert/';
            \Cx\Lib\FileSystem\FileSystem::make_folder($importFileName, true, true);
            $importFileName .=  md5($file->getAbsoluteFilePath());
            $importFileName .= '.' . filemtime($file->getAbsoluteFilePath()) . '.import';
            \DBG::debug('Convert: Import filename for "' . $file->getAbsoluteFilePath() . '" is: ' . $importFileName);
            try {
                $importFile = new \Cx\Lib\FileSystem\PrivilegedFile(
                    $importFileName
                );
                if (!file_exists($importFileName)) {
                    $content = $file->getData();
                    // Initialize the encoding order list.
                    // Add CONTREXX_CHARSET at first position of the encoding list
                    $encodings = preg_grep(
                        '/^' . preg_quote(CONTREXX_CHARSET, '/') . '$/',
                        mb_list_encodings(),
                        PREG_GREP_INVERT
                    );
                    array_unshift($encodings, CONTREXX_CHARSET);

                    // try to detect the encoding
                    $encoding = mb_detect_encoding($content, $encodings, true);

                    // fix encoding in case it differs from CONTREXX_CHARSET
                    if (
                        // only proceed in case an encoding was detected
                        $encoding !== false &&
                        $encoding != CONTREXX_CHARSET
                    ) {
                        // finally, convert the file to CONTREXX_CHARSET
                        $content = mb_convert_encoding($content, CONTREXX_CHARSET, $encoding);
                    }
                    $importFile->write($content);
                }
                $handle = $importFile->getHandle();
            } catch (\Cx\Lib\FileSystem\FileSystemException $e) {
                \DBG::log('Error reading or writing import file: ' . $e->getMessage());
            }
        }

        // Get the longest line
        // Note: While this might seem stupid, it is actually a bit faster than
        // letting fgetcsv() detect line lengths.
        // Depending on the ratio between total number of rows and $limit this
        // is counter-productive. However, for >100K lines this is in the region
        // of ~300ms. Instead of implementing the limit logic here as well
        // (which would make it way slower if done properly) the tradeoff is to
        // always execute this, even with low values for $limit.
        $len = 0;
        while (!feof($handle)) {
            $data = fgets($handle);
            if ($data === false && feof($handle)) {
                break;
            }
            $length = strlen($data);
            $len = ($length > $len) ? $length : $len;
        }
        fseek($handle, 0);

        // Load column names
        $columnNames = array();
        if ($columnNamesInFirstRow) {
            while (($data = fgetcsv($handle, $len, $this->separator, $this->enclosure))) {
                $dataAvailable = false;
                foreach ($data as $value) {
                    if (!empty($value)) {
                        $dataAvailable = true;
                        break;
                    }
                }
                if (!$dataAvailable) {
                    continue;
                }
                foreach ($data as $index => $field) {
                    if (empty($field)){
                        $field = 'emptyField_' . $index;
                    }
                    $columnNames[] = $field;
                }
                break;
            }
        }

        if ($offset == -1) {
            $offset = 0;
        }

        $numberOfDataEntries = 0;
        while (($data = fgetcsv($handle, $len, $this->separator, $this->enclosure))) {
            $dataAvailable = false;
            foreach ($data as $value) {
                if (!empty($value)) {
                    $dataAvailable = true;
                    break;
                }
            }

            if (!$dataAvailable) {
                continue;
            }

            if ($offset != -1 && $numberOfDataEntries < $offset) {
                $numberOfDataEntries++;
                continue;
            }
            if ($limit != -1 && $numberOfDataEntries >= $offset + $limit) {
                break;
            }

            // Add fields to data
            if ($columnNamesInFirstRow) {
                $columnNamesSize = count($columnNames);
                $dataSize = count($data);
                if ($dataSize > $columnNamesSize) {
                    $data = array_slice($data, 0, $columnNamesSize);
                } elseif ($dataSize < $columnNamesSize) {
                    $data = array_pad($data, $columnNamesSize, '');
                }
                $data = array_combine($columnNames, $data);
            }
            $callback($data, $numberOfDataEntries);
            $numberOfDataEntries++;
        }
        fclose($handle);
    }

    /**
     * Check if string starts with UTF-8 BOM
     *
     * @param \resource File handle to test for UTF-8 BOM
     * @return bool True of $string stars with the UTF-8 BOM
     */
    protected function startsWithUtf8Bom($fileHandle): bool {
        $pointerPos = ftell($fileHandle);
        fseek($fileHandle, 0);
        $first3Bytes = fread($fileHandle, 3);
        fseek($fileHandle, $pointerPos);
        return $first3Bytes === hex2bin('EFBBBF');
    }
}
