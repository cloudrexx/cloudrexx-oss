<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * FileSystem
 *
 * @copyright   CLOUDREXX CMS - CLOUDREXX AG
 * @author      CLOUDREXX Development Team <info@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  lib_filesystem
 */

namespace Cx\Lib\FileSystem;

/**
 * FileSystemException
 *
 * @copyright   CLOUDREXX CMS - CLOUDREXX AG
 * @author      Thomas Däppen <thomas.daeppen@comvation.com>
 * @version     3.0.0
 * @package     cloudrexx
 * @subpackage  lib_filesystem
 */

class FileSystemException extends \Exception {};

/**
 * File System
 * Collection of file system manipulation tools
 *
 * @copyright   CLOUDREXX CMS - CLOUDREXX AG
 * @author      Janik Tschanz <janik.tschanz@comvation.com>
 * @author      Reto Kohli <reto.kohli@comvation.com>
 * @package     cloudrexx
 * @subpackage  lib_filesystem
 */
class FileSystem
{
    /**
     * chmod mode for folders (rwxrwxrwx)
     *
     * Note that 0775 is *not* sufficient for moving uploaded files
     * (in many cases anyway).
     */
    const CHMOD_FOLDER = 0777;
    /**
     * chmod mode for files (rw-rw-r--)
     *
     * Note that 0664 is sufficient in *most* cases.
     * (if it's not, it's not safe).
     */
    const CHMOD_FILE   = 0664;

    const CHMOD_OTHER_EXECUTE   = 1;
    const CHMOD_OTHER_WRITE     = 2;
    const CHMOD_OTHER_READ      = 4;
    const CHMOD_GROUP_EXECUTE   = 8;
    const CHMOD_GROUP_WRITE     = 16;
    const CHMOD_GROUP_READ      = 32;
    const CHMOD_USER_EXECUTE    = 64;
    const CHMOD_USER_WRITE      = 128;
    const CHMOD_USER_READ       = 256;

    /**
     * Internal error numbers, stored in $error
     */
    const ERROR_NONE                  = 0;
    const ERROR_FILE_NOT_FOUND        = 1;
    const ERROR_FOLDER_NOT_FOUND      = 2;
    const ERROR_CANNOT_CREATE_FILE    = 101;
    const ERROR_CANNOT_CREATE_FOLDER  = 102;
    const ERROR_CANNOT_MOVE_FILE      = 111;
    const ERROR_INVALID_FILETYPE      = 201;
    const ERROR_FILESIZE_TOO_BIG      = 202;
    const ERROR_MISSING_ARGUMENT      = 301;

    /**
     * Define some constants which are relevant for the filename
     */
    const MAX_FILENAME_LENGTH = 255;
    const DOT_LENGTH = 1;
    // Add more as needed.  Don't forget to add core language entries, like
    // $_ARRAYLANG['TXT_CORE_FILE_ERROR_#'] = "Oh my, an error!";
    // where # is the error number.

    /**
     * Internal error number
     *
     * See {@see getError()}.
     * @var   integer
     */
    private static $error = self::ERROR_NONE;


    /**
     * Returns the current error number, if any, or zero.
     *
     * Note that the internal $error variable is cleared,
     * so you *SHOULD* call this once and get a sensible result.
     * @return  integer           The error number, or zero
     */
    static function getError()
    {
        $error = self::$error;
        self::$error = self::ERROR_NONE;
        return $error;
    }


    /**
     * Returns the current error string, if any, or the empty string.
     *
     * Calls {@see getError()}, thus clearing the error number in
     * the $error class variable.
     * @return  integer           The error number, or zero
     */
    static function getErrorString()
    {
        global $_CORELANG;

        return $_CORELANG['TXT_CORE_FILE_ERROR_'.self::getError()];
    }

    /**
     * Check if PHP has write access to this installations files
     */
    public static function hasPhpWriteAccess() {
        // get the user-ID of the user who owns the images folder
        $cx = \Cx\Core\Core\Controller\Cx::instanciate();
        $fileOwnerUserId = fileowner($cx->getWebsiteImagesPath());
        if (!$fileOwnerUserId) {
            return false;
        }

        // get the user-ID of the user running the PHP-instance
        if (function_exists('posix_getuid')) {
            $phpUserId = posix_getuid();
        } else {
            $phpUserId = getmyuid();
        }

        // check if the file we're going to work with is owned by the PHP user
        if ($fileOwnerUserId == $phpUserId) {
            return true;
        }

        // as the user-ID of the PHP process is not the same as the owner
        // of this script, we can assume that PHP will most likely
        // not have write access when required
        return false;
    }

    /**
     * Copy a non-privileged folder to a non-privileged location.
     *
     * @todo    Drop this method is favor of {@see static::copy_folder()}.
     *     Migrate logic of $ignoreExists to the usages of this method when migrating
     *     to {@see static::copy_folder()}.
     * @todo    Migrate logic of 'prevent infinit copy loop' to {@see static::copy_folder()}.
     * @deprecated Use {@see static::copy_folder()} instead.
     */
    function copyDir($orgPath, $orgWebPath, $orgDirName, $newPath, $newWebPath, $newDirName, $ignoreExists = false)
    {
        $orgWebPath=$this->checkWebPath($orgWebPath);
        $newWebPath=$this->checkWebPath($newWebPath);

        // verify operation is done within system's document root
        if (
            !$this->isValidFilePath($orgPath . $orgDirName) ||
            !$this->isValidFilePath(dirname($newPath . $newDirName)) ||
            // prevent infinit copy loop
            (
                strpos($newPath . $newDirName, $orgPath . $orgDirName) === 0 &&
                substr($newPath . $newDirName, strlen($orgPath . $orgDirName), 1) === '/'
            )
        ) {
            return 'error';
        }

        if (file_exists($newPath.$newDirName) && !$ignoreExists) {
            $newDirName = $newDirName.'_'.time();
        }
        if (!$this->make_folder($newPath . $newDirName)) {
            return 'error';
        }
        $directory = @opendir($orgPath.$orgDirName);
        $file = @readdir($directory);
        while ($file) {
            if ($file!='.' && $file!='..') {
                if (!is_dir($orgPath.$orgDirName.'/'.$file)) {
                        $this->copyFile($orgPath, $orgDirName.'/'.$file, $newPath, $newDirName.'/'.$file, $ignoreExists);
                } else {
                    $this->copyDir($orgPath, $orgWebPath, $orgDirName.'/'.$file, $newPath, $newWebPath, $newDirName.'/'.$file, $ignoreExists);
                }
            }
            $file = @readdir($directory);
        }
        closedir($directory);
        return '';
    }

    /**
     * Copy a non-privileged file to a non-privileged location.
     *
     * @todo    Drop this method is favor of {@see File::copy()}.
     *     Migrate logic of $ignoreExists to the usages of this method when migrating
     *     to {@see File::copy()}.
     * @deprecated Use {@see File::copy()} instead.
     */
    function copyFile($orgPath, $orgFileName, $newPath, $newFileName, $ignoreExists = false)
    {
        // verify operation is done within system's document root
        if (   !static::isValidFilePath($orgPath . $orgFileName)
            || !static::isValidFilePath(dirname($newPath . $newFileName))
        ) {
            return 'error';
        }

        if (file_exists($newPath.$newFileName)) {
            $info   = pathinfo($newFileName);
            $exte   = $info['extension'];
            $exte   = (!empty($exte)) ? '.' . $exte : '';
            $part   = substr($newFileName, 0, strlen($newFileName) - strlen($exte));
            if (!$ignoreExists) {
                $newFileName  = $part . '_' . (time()) . $exte;
            }
        }
        if (copy($orgPath.$orgFileName, $newPath.$newFileName)) {
            \Cx\Lib\FileSystem\FileSystem::makeWritable($newPath.$newFileName);
            $this->callAddEvent($newPath, $newFileName);
        } else {
            $newFileName = 'error';
        }
        return $newFileName;
    }


    /**
     * @deprecated Use {@see \Cx\Lib\FileSystem\File::delete()} instead.
     */
    function delFile($path, $webPath, $fileName)
    {
        try {
            $file = new \Cx\Lib\FileSystem\File($path . $fileName);
            $file->delete();
            $this->callDeleteEvent($path, $fileName);
            return $fileName;
        } catch (\Cx\Lib\FileSystem\FileSystemException $e) {
            \DBG::msg($e->getMessage());
            return 'error';
        }
    }


    function checkWebPath($webPath)
    {
        if (substr($webPath, 0, 1) == '/') {
            $webPath = substr($webPath, 1);
        } else {
            $webPath = $webPath;
        }

        return $webPath;
    }


    // replaces some characters
    public static function replaceCharacters($string) {
        // cloudrexx file name policies
        $string = \FWValidator::getCleanFileName($string);

        // media library special changes; code depends on those
        // replace $change with ''
        $change = array('+', '#');

        // replace $signs1 with $signs
        $signs1 = array(' ', 'ä', 'ö', 'ü', 'ç');
        $signs2 = array('_', 'ae', 'oe', 'ue', 'c');

        foreach ($change as $str) {
            $string = str_replace($str, '_', $string);
        }

        for ($x = 0; $x < count($signs1); $x++) {
            $string = str_replace($signs1[$x], $signs2[$x], $string);
        }

        $string = str_replace('__', '_', $string);
        if (strlen($string) > self::MAX_FILENAME_LENGTH) {
            $info       = pathinfo($string);
            $stringExt  = $info['extension'];
            $stringName = substr($string, 0, strlen($string) - (strlen($stringExt) + self::DOT_LENGTH));
            $stringName = substr($stringName, 0, self::MAX_FILENAME_LENGTH - (strlen($stringExt) + self::DOT_LENGTH));
            $string     = $stringName.'.'.$stringExt;
        }

        return $string;
    }


    /**
     * Sanitizes the given path.
     *
     * @param   string       $path
     * @return  bool|string  $path
     */
    public static function sanitizePath($path) {
        $path =    !empty($path)
                && $path != '..'
                && strpos($path, '../')  === false
                && strpos($path, '..\\') === false
                && strpos($path, '/..')  === false
                && strpos($path, '\..')  === false ? trim($path) : false;

        return $path;
    }


    /**
     * Sanitizes the given file name.
     *
     * @param   string       $file
     * @return  bool|string  $file
     */
    public static function sanitizeFile($file) {
        $file = !empty($file) ? basename(trim($file)) : false;
        if ($file == '..') $file = false;

        return $file;
    }


    function setChmod($path, $webPath, $fileName)
    {
        if (!file_exists($path.$fileName)) return false;
        if (is_dir($path.$fileName)) {
            if (@chmod($path.$fileName, self::CHMOD_FOLDER)) return true;
        } else {
            if (@chmod($path.$fileName, self::CHMOD_FILE )) return true;
        }
        return false;
    }

    /**
     * Call Indexer event to delete
     * ToDo: Use \Cx\Core\MediaSource\Model\Entity\LocalFile when FileSystem work smart
     *
     * @param $path string path to file or directory
     * @param $name string name of file or directory
     *
     * @throws \Cx\Core\Event\Controller\EventManagerException
     * @return void
     */
    protected function callDeleteEvent($path, $name)
    {
        $this->callEvent(
            'Remove',
            array(
                'path' => $path,
                'name' => $name
            )
        );
    }

    /**
     * Same as callDeleteEvent, but static
     * @see callDeleteEvent()
     */
    protected static function callDeleteEventStatic($path, $name) {
        static::callEventStatic(
            'Remove',
            array(
                'path' => $path,
                'name' => $name
            )
        );
    }

    /**
     * Call Indexer event to update
     * ToDo: Use \Cx\Core\MediaSource\Model\Entity\LocalFile when FileSystem work smart
     *
     * @param $path    string path to file or directory
     * @param $name    string name of file or directory
     * @param $oldname string old name of file or directory
     *
     * @throws \Cx\Core\Event\Controller\EventManagerException
     * @return void
     */
    protected function callUpdateEvent($path, $name, $oldname)
    {
        $this->callEvent(
            'Update',
            array(
                'path' => $path . $name,
                'oldPath' => $path . $oldname,
            )
        );
    }

    /**
     * Call Indexer event to add
     * ToDo: Use \Cx\Core\MediaSource\Model\Entity\LocalFile when FileSystem work smart
     *
     * @param $path string path to file or directory
     * @param $name string name of file or directory
     *
     * @throws \Cx\Core\Event\Controller\EventManagerException
     * @return void
     */
    protected function callAddEvent($path, $name)
    {
        $this->callEvent(
            'Add',
            array(
                'path' => $path . $name
            )
        );
    }

    protected function callEvent($event, $params)
    {
        $cx = \Cx\Core\Core\Controller\Cx::instanciate();
        $cx->getEvents()->triggerEvent('MediaSource.File:' . $event, $params);
    }

    /**
     * Same as callEvent, but static
     * @see callEvent()
     * @deprecated Use non-static method instead
     */
    protected static function callEventStatic($event, $params) {
        $cx = \Cx\Core\Core\Controller\Cx::instanciate();
        $cx->getEvents()->triggerEvent('MediaSource.File:' . $event, $params);
    }


////////////////////////////////////////////////////////////////////////////////
// New static methods replacing the old object methods
////////////////////////////////////////////////////////////////////////////////
// These are simplified to use a single path argument only.
// Any of these use paths that are *ALWAYS* relative to the
// \Env::get('cx')->getWebsiteDocumentRootPath().
// Other arguments *MUST NOT* contain "path" in their name in any case;
// rather call them "folder_name" or "file_name".
////////////////////////////////////////////////////////////////////////////////

    /**
     * Moves an uploaded file to a specified path
     *
     * Returns true if the file name is valid, the file type matches one of
     * the accepted file types, if specified, is not too large, and can be
     * moved successfully to its target folder.
     * Missing folders are created.  If this fails, returns false.
     * Mind that the target path *MUST NOT* include ASCMS_PATH, and *SHOULD*
     * not include ASCMS_PATH_OFFSET.  The latter will be cut off, however.
     * The $target_path argument, given by reference, is fixed accordingly.
     * If the file name found in $upload_field_name is empty, returns the
     * empty string.
     * Non-positive values for $maximum_size are ignored, as are empty
     * values for $accepted_types.
     * @param   string  $upload_field_name  File input field name
     * @param   string  $target_path        Target path, relative to the
     *                                      document root, including the file
     *                                      name, by reference.
     * @param   integer $maximum_size       The optional maximum allowed file size
     * @param   string  $accepted_types     The optional allowed MIME type
     * @return  boolean                     True on success, the empty string
     *                                      if there is nothing to do, or
     *                                      false otherwise
     * @author  Reto Kohli <reto.kohli@comvation.com>
     * @since   2.2.0
     */
    static function upload_file_http(
        $upload_field_name, &$target_path,
        $maximum_size=0, $accepted_types=false)
    {
        // Skip files that are not uploaded at all
        if (empty($_FILES[$upload_field_name])) {
//DBG::log("File::upload_file_http($upload_field_name, $target_path, $maximum_size, $accepted_types): No file for index $upload_field_name<br />");
            return '';
        }

        self::path_relative_to_root($target_path);
//DBG::log("File::upload_file_http($upload_field_name, $target_path, $maximum_size, $accepted_types): Fixed target path $target_path<br />");
        if (   empty($upload_field_name)
            || empty($target_path)) {
//DBG::log("File::upload_file_http($upload_field_name, $target_path, $maximum_size, $accepted_types): Missing mandatory argument<br />");
            self::$error = self::ERROR_MISSING_ARGUMENT;
            return false;
        }
        $tmp_path = $_FILES[$upload_field_name]['tmp_name'];
        $file_name = $_FILES[$upload_field_name]['name'];
        if (   $accepted_types
            && !\Filetype::matchMimetypes($file_name, $accepted_types)) {
//DBG::log("File::upload_file_http(): Error: Found no matching MIME type for extension ($file_name)<br />");
            self::$error = self::ERROR_INVALID_FILETYPE;
            return false;
        }
        if ($maximum_size > 0 && filesize($tmp_path) > $maximum_size) {
//DBG::log("File::upload_file_http($upload_field_name, $target_path, $maximum_size, $accepted_types): Size greater than $maximum_size<br />");
            self::$error = self::ERROR_FILESIZE_TOO_BIG;
            return false;
        }
        // Create the target folder if it doesn't exist
        if (!File::make_folder(dirname($target_path))) {
//DBG::log("File::upload_file_http(): Failed to create folder ".dirname($target_path)." for $target_path<br />");
            self::$error = self::ERROR_CANNOT_CREATE_FOLDER;
            return false;
        }
        if (move_uploaded_file(
            $tmp_path, \Env::get('cx')->getWebsiteDocumentRootPath().'/'.$target_path)) {
//DBG::log("File::upload_file_http($upload_field_name, $target_path, $maximum_size, $accepted_types): File successfully moved to $target_path<br />");
            return true;
        }
//DBG::log("File::upload_file_http($upload_field_name, $target_path, $maximum_size, $accepted_types): move_uploaded_file failed<br />");
        self::$error = self::ERROR_CANNOT_MOVE_FILE;
        return false;
    }


    /**
     * Takes the path given by reference and removes any leading
     * folders up to and including the ASCMS_PATH_OFFSET, including
     * path separators (\ and /).
     *
     * Important note: The regex used to cut away the excess path
     * is non-greedy and works fine in most cases.  However, there
     * is a small risk that it may go wrong if two things occur at the
     * same time, namely:
     * - the ASCMS_PATH_OFFSET is not part of the path provided, and
     * - the path contains a folder or file with the same name.
     * If this is the case, you can either change the offset or the
     * name of the subfolder or file, whichever is acceptable.
     * @param   string    $path       Any absolute or relative path
     * @return  void
     * @author  Reto Kohli <reto.kohli@comvation.com>
     * @since   2.2.0
     */
    static function path_relative_to_root(&$path)
    {
        if (strpos($path, \Env::get('cx')->getWebsiteDocumentRootPath()) === 0) {
            $path = substr($path, strlen(\Env::get('cx')->getWebsiteDocumentRootPath()) + 1);
        } elseif (strpos($path, \Env::get('cx')->getCodeBaseDocumentRootPath()) === 0) {
            $path = substr($path, strlen(\Env::get('cx')->getCodeBaseDocumentRootPath()) + 1);
        } elseif (\Env::get('cx')->getCodeBaseOffsetPath() && strpos($path, \Env::get('cx')->getCodeBaseOffsetPath()) === 0) {
            $path = substr($path, strlen(\Env::get('cx')->getCodeBaseOffsetPath()) + 1);
        } elseif (strpos($path, '/') === 0) {
            $path = substr($path, 1);
        }
    }

    static function path_absolute_to_os_root(&$path) {

        // $path is specified by absolute file system path of operating system
        if (   strpos($path, \Env::get('cx')->getWebsiteDocumentRootPath()) === 0
            || strpos($path, \Env::get('cx')->getCodeBaseDocumentRootPath()) === 0
        ) {
            $path = $path;
        // $path is specified by relative path of Website's offset path
        } elseif (\Env::get('cx')->getWebsiteOffsetPath() && strpos($path, \Env::get('cx')->getWebsiteOffsetPath()) === 0) {
            $path = \Env::get('cx')->getWebsitePath() . $path;
        // $path is specified by absolute path from Website's document root
        } elseif (strpos($path, '/') === 0) {
            $path = \Env::get('cx')->getWebsiteDocumentRootPath() . $path;
        // $path path is unkown -> assuming its relative from Website's document root
        } else {
            $path = \Env::get('cx')->getWebsiteDocumentRootPath() . '/'.$path;
        }

    }

    /**
     * Creates the folder for the given path
     *
     * If the path already exists, returns true if it's a folder, or
     * false if it's a file.
     * @param   string  $folder_path    The directory path.
     * @param   boolean $recursive      Allows the creation of nested
     *                                  directories specified in the directory.
     * @param   boolean $privileged     Allows the creation of directories
     *                                  in privileged locations.
     * @return  boolean                   True on success, false otherwise
     */
    static function make_folder($folder_path, $recursive = false, $privileged = false)
    {
        self::path_relative_to_root($folder_path);
        if (!static::isValidFilePath($folder_path, $privileged)) {
            return false;
        }
        if (self::exists($folder_path)) {
            if (is_dir(\Env::get('cx')->getWebsiteDocumentRootPath().'/'.$folder_path)) {
//DBG::log("File::make_folder($folder_path): OK, folder $folder_path exists already<br />");
                return true;
            }
//DBG::log("File::make_folder($folder_path): FAIL, a file of the name $folder_path exists already<br />");
            return false;
        }

        \Cx\Lib\FileSystem\FileSystem::makeWritable(
            dirname(\Env::get('cx')->getWebsiteDocumentRootPath().'/'.$folder_path),
            $privileged
        );
        @mkdir(\Env::get('cx')->getWebsiteDocumentRootPath().'/'.$folder_path, self::CHMOD_FOLDER, $recursive ? true : false);
        if (!self::exists($folder_path)) {
//DBG::log("File::make_folder($folder_path): FAIL, cannot create folder ".\Env::get('cx')->getWebsiteDocumentRootPath()."/$folder_path<br />");
            return false;
        }
        $flags = self::chmod($folder_path, self::CHMOD_FOLDER);
        if ($flags == self::CHMOD_FOLDER) {
//DBG::log("File::make_folder($folder_path): OK, folder $folder_path created and chmodded<br />");
            return true;
        }
//        else {
//DBG::log("File::make_folder($folder_path): FAILED to chmod $folder_path, returned flags: ".decoct($flags)."<br />");
//        }
        if (!self::exists($folder_path)) {
//DBG::log("File::make_folder($folder_path): FAIL, folder $folder_path does still not exist<br />");
            return false;
        }
//DBG::log("File::make_folder($folder_path): created folder $folder_path, but FAILED to chmod!<br />");
        return true;
    }

    /**
     * Copies a folder recursively from the source to the target path
     *
     * If $force is true, the folder and its contents are copied even if
     * a folder of the same name exists in the target path already.
     * Otherwise, false is returned.
     * @param   string    $source_path    The path of the source folder
     * @param   string    $target_path    The path of the target folder
     * @param   string    $force          Force copying if true
     * @return  boolean                   True on success, false otherwise
     */
    public static function copy_folder($source_path, $target_path, $force=false, $privileged = false)
    {

        self::path_absolute_to_os_root($source_path);
        self::path_absolute_to_os_root($target_path);

        if (   !static::isValidFilePath($source_path, $privileged)
            || !static::isValidFilePath($target_path, $privileged)
        ) {
            return false;
        }

        if (self::exists($target_path)) {
            if (!$force)
                return false;
        } else {
            if (!self::make_folder($target_path, false, $privileged)) {
                return false;
            }
        }

        $directory = @opendir($source_path);
        $file = @readdir($directory);
        while ($file) {
            if (preg_match('/^\.\.?$/', $file)) {
                $file = @readdir($directory);
                continue;
            }
            if (is_dir($source_path.'/'.$file)) {
                if (!self::copy_folder(
                    $source_path.'/'.$file, $target_path.'/'.$file, $force, $privileged)) {
                    return false;
                }
            } else {
                try {
                    if ($privileged) {
                        $objFile = new \Cx\Lib\FileSystem\PrivilegedFile($source_path.'/'.$file);
                    } else {
                        $objFile = new \Cx\Lib\FileSystem\File($source_path.'/'.$file);
                    }
                    if (!$objFile->copy($target_path.'/'.$file, $force)) {
                        return false;
                    }
                } catch (FileSystemException $e) {
                    \DBG::log($e->getMessage());
                    return false;
                }
            }
            $file = @readdir($directory);
        }
        closedir($directory);
        return true;
    }


    /**
     * Deletes the given folder name from the path
     *
     * If $force is true, recursively deletes any content of the folder
     * first.  Otherwise, if the folder is not empty, false is returned.
     * Returns true if the folder was deleted.
     * @param   string    $folder_path  The folder path
     * @param   boolean   $force        If true, deletes contents of the folder
     * @return  boolean                 True on success, false otherwise
     */
    public static function delete_folder(
        $folder_path,
        $force = false,
        $privileged = false
    ) {
        if (!static::isValidFilePath($folder_path, $privileged)) {
            return false;
        }
        self::path_relative_to_root($folder_path);
        $resource = @opendir(\Env::get('cx')->getWebsiteDocumentRootPath().'/'.$folder_path);
        if (!$resource) return false;
        $file = @readdir($resource);
        while ($file !== false) {
            if (preg_match('/^\.\.?$/', $file)) {
                $file = @readdir($resource);
                continue;
            }
            if (!$force) {
                closedir($resource);
                return false;
            }
            $file_path = $folder_path.'/'.$file;
            if (is_file(\Env::get('cx')->getWebsiteDocumentRootPath().'/'.$file_path)) {
                try {
                    $objFile = new \Cx\Lib\FileSystem\PrivilegedFile($file_path);
                    $objFile->delete();
                } catch (FileSystemException $e) {
                    \DBG::log($e->getMessage());
                    return false;
                }
            } else {
                if (!self::delete_folder($file_path, $force, $privileged)) return false;
            }
            $file = @readdir($resource);
        }
        closedir($resource);

        \Cx\Lib\FileSystem\FileSystem::makeWritable(
            dirname(\Env::get('cx')->getWebsiteDocumentRootPath().'/'.$folder_path),
            $privileged
        );
        return @rmdir(\Env::get('cx')->getWebsiteDocumentRootPath().'/'.$folder_path);
    }

    /**
     * Deletes the file from the path specified
     *
     * Returns true if the file doesn't exist in the first place.
     * @param   string    $file_path      The path of the file
     * @return  boolean                   True on success, false otherwise
     */
    public static function delete_file($file_path)
    {
        try {
            $objFile = new \Cx\Lib\FileSystem\File($file_path);
            $objFile->delete();
            static::callDeleteEventStatic(
                pathinfo($file_path, PATHINFO_DIRNAME) . '/',
                pathinfo($file_path, PATHINFO_BASENAME)
            );
            return true;
        } catch (FileSystemException $e) {
            \DBG::log($e->getMessage());
        }

        return false;
    }

    /**
     * Makes a valid file path
     *
     * Replaces non-ASCII and some other characters in the string
     * given by reference with underscores.
     * @param   string    $path       The path (or any other string)
     * @return  void
     * @todo    Test!
     * @todo    Replace non-ASCII charactes with octal values
     */
    static function clean_path(&$path)
    {
        $path = preg_replace(
            '/[¦"@*#°%§&¬|¢?\'´`^~¨£$<>\200-\377]/', '_', $path
        );
//        $path = preg_replace(
//            '/^(.+?){0,40}.*?(\.[^.]+)?$/', '\1\2', $path);
    }


    /**
     * Renames or moves a file or folder
     *
     * If a file or folder with the $to_path already exists, and $force
     * is false, returns false.
     * @param   string    $from_path    The original path
     * @param   string    $to_path      The destination path
     * @param   boolean   $force        Overwrites the destination if true
     * @param   boolean   $privileged   Set to true to allow operation on privileged
     *     file system locations (see {@see FileSystemFile::$privileged}).
     * @return  boolean                 True on success, false otherwise
     */
    static function move($from_path, $to_path, $force = false, $privileged = false)
    {
        self::path_relative_to_root($from_path);
        self::path_relative_to_root($to_path);

        if (   !static::isValidFilePath($from_path, $privileged)
            || !static::isValidFilePath($to_path, $privileged)
        ) {
            return false;
        }

        if (self::exists($to_path) && !$force)
            return false;
        if (!rename(
            \Env::get('cx')->getWebsiteDocumentRootPath().'/'.$from_path,
            \Env::get('cx')->getWebsiteDocumentRootPath().'/'.$to_path)) {
            return false;
        }
        $cx = \Cx\Core\Core\Controller\Cx::instanciate();
        $fileSystem = new \Cx\Lib\FileSystem\FileSystem();
        $fileSystem->callUpdateEvent(
            $cx->getWebsiteDocumentRootPath() . '/' .
            pathinfo($to_path, PATHINFO_DIRNAME) . '/',
            pathinfo($to_path, PATHINFO_BASENAME),
            $from_path
        );
        return self::chmod(
            $to_path,
            (is_file(\Env::get('cx')->getWebsiteDocumentRootPath().'/'.$from_path)
              ? self::CHMOD_FILE : self::CHMOD_FOLDER));
    }

    /**
     * Applies the flags to the given path
     *
     * The path may be a file or a folder.  The flags are considered to be
     * octal values, as required by chmod().
     * @param   string    $path       The path to be chmodded
     * @param   integer   $flags      The flags to apply
     * @return  boolean               True on success, false otherwise
     */
    static function chmod($path, $flags)
    {
        self::path_relative_to_root($path);
        if (!self::exists($path)) {
//DBG::log("File::chmod($path, ".decoct($flags)."): FAIL, folder $path does not exist<br />");
            return false;
        }
        if (@chmod(\Env::get('cx')->getWebsiteDocumentRootPath().'/'.$path, $flags)) {
//DBG::log("File::chmod($path, ".decoct($flags)."): OK, folder $path chmodded<br />");
            return true;
        }
//DBG::log("File::chmod($path, ".decoct($flags)."): FAILED to chmod folder $path<br />");
        return false;
    }

    public static function touch($path)
    {
        try {
            $objFile = new \Cx\Lib\FileSystem\File($path);
            $objFile->touch();
            return true;
        } catch (FileSystemException $e) {
            \DBG::log($e->getMessage());
        }

        return false;
    }

    /**
     * Add write access to $path.
     *
     * @param   string  $path Path to file or folder
     * @param   boolean $privileged Set to `true` if $path is a privileged file
     *     system location (see {@see FileSystemFile::$privileged}).
     * @return  boolean `true` if write access on $path is set.
     */
    public static function makeWritable($path, $privileged = false)
    {
        try {
            if ($privileged) {
                $objFile = new \Cx\Lib\FileSystem\PrivilegedFile($path);
            } else {
                $objFile = new \Cx\Lib\FileSystem\File($path);
            }
            $objFile->makeWritable();
            return true;
        } catch (FileSystemException $e) {
            \DBG::log($e->getMessage());
        }

        return false;
    }


    /**
     * Wrapper for file_exists()
     *
     * Prepends \Env::get('cx')->getWebsiteDocumentRootPath() to the path.
     * The file is stat()ed before calling file_exists() in order to
     * update a potentially outdated cache.
     * @param   string    $path     The file or folder path
     * @return  boolean             True if the file exists, false otherwise
     */
    static function exists($path)
    {
        self::path_relative_to_root($path);
        // Clear the file cache.  file_exists() relies on that too much
        clearstatcache(true, \Env::get('cx')->getWebsiteDocumentRootPath().'/'.$path);
        $result = file_exists(\Env::get('cx')->getWebsiteDocumentRootPath().'/'.$path);
//if ($result) {
//DBG::log("File::exists($path): file ".\Env::get('cx')->getWebsiteDocumentRootPath()."/$path exists<br />");
//} else {
//DBG::log("File::exists($path): file ".\Env::get('cx')->getWebsiteDocumentRootPath()."/$path does not exist<br />");
//}
        return $result;
    }

    /**
     * Verify if $path is a valid file path on which file manipulation is allowed.
     *
     * A valid file path is one of the following locations:
     * - /images
     * - /media
     * - /themes
     * - /tmp/session-<sid>
     *
     * @param   boolean $privileged Set to TRUE to assess every path within the
     *                              document root as TRUE and not only the ones
     *                              listed above.
     * @param   boolean $sanitizePath Set to `false` to not sanitize the path.
     *     If set to `true` (default), then `$path` will be converted into its
     *     absolute realpath on the filesystem before its being validated.
     * @return  boolean  True if $path is a file located in one of the
     * allowed locations.
     */
    public static function isValidFilePath($path, $privileged = false, $sanitizePath = true) {
        if ($sanitizePath) {
            try {
                $file = new \Cx\Lib\FileSystem\PrivilegedFile($path);
                $path = $file->getAbsoluteFilePath();
            } catch (\Cx\Lib\FileSystem\FileSystemException $e) {
                \DBG::debug('Denied access to invalid file path: ' . $path);
                return false;
            }
        }
        $cx = \Cx\Core\Core\Controller\Cx::instanciate();
        if (
            $privileged
            && (
                strpos($path, $cx->getCodeBaseDocumentRootPath() . '/') === 0
                || strpos($path, $cx->getWebsiteDocumentRootPath() . '/') === 0
            )
        ) {
            return true;
        }
        if ($privileged) {
            \DBG::debug('Denied access to external file path: ' . $path);
            return false;
        }
        $nonPrivilegedPaths = [
            $cx->getWebsiteImagesPath(),
            $cx->getWebsiteDocumentRootPath() . \Cx\Core\Core\Controller\Cx::FOLDER_NAME_MEDIA,
            $cx->getCodeBaseThemesPath(),
            $cx->getWebsiteThemesPath(),

        ];
        $session = $cx->getComponent('Session')->getSession(false);
        if ($session) {
            $nonPrivilegedPaths[] = $session->getTempPath();
        }
        foreach ($nonPrivilegedPaths as $nonPrivilegedPath) {
            if (
                strpos($path, $nonPrivilegedPath . '/') === 0
                || $path === $nonPrivilegedPath
            ) {
                return true;
            }
        }
        \DBG::debug('Denied access to privileged file path: ' . $path);
        return false;
    }
}
