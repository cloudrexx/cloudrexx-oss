<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * File
 *
 * @copyright   CLOUDREXX CMS - CLOUDREXX AG
 * @author      CLOUDREXX Development Team <info@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  lib_filesystem
 */

namespace Cx\Lib\FileSystem;

/**
 * FileException
 *
 * @copyright   CLOUDREXX CMS - CLOUDREXX AG
 * @author      CLOUDREXX Development Team <info@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  lib_filesystem
 */
class FileException extends FileSystemException {};

/**
 * File
 *
 * @todo        This should be merged with FileSystemFile and FileSystemFile removed.
 *                  Having two separate classes was necessary to implement transparent
 *                  file operations via FTP, which is no longer supported.
 * @copyright   CLOUDREXX CMS - CLOUDREXX AG
 * @author      CLOUDREXX Development Team <info@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  lib_filesystem
 */
class File implements FileInterface
{
    const UNKNOWN_ACCESS  = 0;
    const PHP_ACCESS      = 1;

    /**
     * The value of $file that has been passed to the constructor.
     * @var string
     */
    protected $file = null;

    /**
     * @var FileSystemFile  Local FileSystemFile instance of the instanciated
     *                      filesystem file.
     *                      In case access mode is PHP_ACCESS or UNKNOWN_ACCESS
     *                      then all file operations will be performed directly
     *                      on this instance.
     */
    protected $fsFile = null;

    private $accessMode = null;

    /**
     * Instanciate a file system file to perform operations on.
     * By default only the instantiation of files located in one of the
     * following locations is allowed:
     * - /images
     * - /media
     * - /themes
     * - /tmp/session-<sid>
     *
     * @param   string  $file Path to file
     * @throws  FileException   In case $file is not a valid path or $file
     *     points to a location this instance is not allowed to access to.
     */
    public function __construct($file)
    {
        if (preg_match('#(?:^\.\.|/\.\.)/#', $file)) {
            throw new FileException('Invalid file path: '.$file);
        }

        $this->file = str_replace('\\', '/', $file);
        $this->setAccessMode();

        if (!$this->fsFile) {
            throw new FileException(
                sprintf(
                    'Failed to instanciate FileSystemFile for %s',
                    $file
                )
            );
        }
    }

    /**
     * @throws  FileSystemFileException See {@see FileSystemFile::__construct()}
     * @return  FileSystemFile
     */
    protected function getFileInstance(): FileSystemFile {
        return new FileSystemFile($this->file);
    }

    private function setAccessMode()
    {
        try {
            $this->fsFile = $this->getFileInstance();
        } catch (FileSystemFileException $e) {
            \DBG::debug('FileSystemFile: ' . $e->getMessage());
            return false;
        }

        // get the user-ID of the user who owns the loaded file
        try {
            $fileOwnerUserId = $this->fsFile->getFileOwner();
            \DBG::debug('File (FileSystem): '.$this->file.' is owned by '.$fileOwnerUserId);
        } catch (FileSystemFileException $e) {
            \DBG::debug('FileSystemFile: '.$e->getMessage());
            \DBG::debug('File: CAUTION: '.$this->file.' is owned by an unknown user!');
            return false;
        }

        // get the user-ID of the user running the PHP-instance
        if (function_exists('posix_getuid')) {
            $phpUserId = posix_getuid();
        } else {
            $phpUserId = getmyuid();
        }
        \DBG::debug('File (PHP): Script user is '.$phpUserId);

        // check if the file we're going to work with is owned by the PHP user
        if ($fileOwnerUserId == $phpUserId) {
            $this->accessMode = self::PHP_ACCESS;
            \DBG::debug('File: Using FileSystem access');
            return true;
        }

        // the file to work on is not owned by the PHP user
        \DBG::debug('File: CAUTION: '.$this->file.' is owned by an unknown user!');
        $this->accessMode = self::UNKNOWN_ACCESS;
        return false;
    }

    public function getAccessMode()
    {
        return $this->accessMode;
    }

    /**
     * @inheritDoc
     */
    public function getHandle(bool $writable = false) {
        return $this->fsFile->getHandle();
    }

    public function getData()
    {
        if (!file_exists($this->file)) {
            throw new FileSystemException('Unable to read data from file '.$this->file.'!');
        }
        $data = file_get_contents($this->file);
        if ($data === false) {
            throw new FileSystemException('Unable to read data from file '.$this->file.'!');
        }

        return $data;
    }

    /**
     * Write data specified by $data to file
     * @param   string
     * @throws  FileSystemException if writing to file fails
     * @return  TRUE on sucess
     */
    public function write($data)
    {
        // use PHP
        if (   $this->accessMode == self::PHP_ACCESS
            || $this->accessMode == self::UNKNOWN_ACCESS
        ) {
            try {
                // try regular file access first
                $this->fsFile->write($data);
                return true;
            } catch (FileSystemFileException $e) {
                \DBG::log('FileSystemFile: '.$e->getMessage());
            }
        }

        throw new FileSystemException('File: Unable to write data to file '.$this->file.'!');
    }

    public function append($data) {
        // use PHP
        if (   $this->accessMode == self::PHP_ACCESS
            || $this->accessMode == self::UNKNOWN_ACCESS
        ) {
            try {
                // try regular file access first
                $this->fsFile->append($data);
                return true;
            } catch (FileSystemFileException $e) {
                \DBG::log('FileSystemFile: '.$e->getMessage());
            }
        }

        throw new FileSystemException('File: Unable to append data to file '.$this->file.'!');
    }

    /**
     * Creates files if it doesn't exists yet
     *
     * @throws FileSystemException if file does not exist and creating fails
     * @return TRUE on success
     */
    public function touch()
    {
        // use PHP
        if (   $this->accessMode == self::PHP_ACCESS
            || $this->accessMode == self::UNKNOWN_ACCESS
        ) {
            try {
                // try regular file access first
                $this->fsFile->touch();
                return true;
            } catch (FileSystemFileException $e) {
                \DBG::log('FileSystemFile: '.$e->getMessage());
            }
        }

        throw new FileSystemException('File: Unable to touch file '.$this->file.'!');
    }

    public function copy($dst, $force = false)
    {
        if (!$force && file_exists($dst)) {
            return true;
        }

        // $dst can be a relative path
        $path = \Env::get('cx')->getWebsiteDocumentRootPath();
        if (
            $path != \Env::get('cx')->getCodeBaseDocumentRootPath()
            && strpos($dst, \Env::get('cx')->getCodeBaseDocumentRootPath()) === 0
        ) {
            if (!$this->fsFile->isPrivileged()) {
                throw new FileSystemException('non-permitted copy operation');
            }
            $path = \Env::get('cx')->getCodeBaseDocumentRootPath();
        } elseif (!empty(\Env::get('cx')->getCodeBaseOffsetPath()) && strpos($dst, \Env::get('cx')->getCodeBaseOffsetPath()) === 0) {
            if (!$this->fsFile->isPrivileged()) {
                throw new FileSystemException('non-permitted copy operation');
            }
            $path = \Env::get('cx')->getCodeBaseOffsetPath();
        }
        $relPath    = str_replace($path, '', $dst);
        $pathInfo   = pathinfo($relPath);
        $arrFolders = explode('/', $pathInfo['dirname']);

        foreach ($arrFolders as $folder) {
            if (empty($folder)) continue;
            $path .= '/' . $folder;
            if (!is_dir($path)) {
                \Cx\Lib\FileSystem\FileSystem::make_folder($path, false, $this->fsFile->isPrivileged());
            }
        }

        // use PHP
        if (   $this->accessMode == self::PHP_ACCESS
            || $this->accessMode == self::UNKNOWN_ACCESS
        ) {
            try {
                // try regular file access first
                $this->fsFile->copy($dst);
                return true;
            } catch (FileSystemFileException $e) {
                \DBG::log('FileSystemFile: '.$e->getMessage());
            }
        }

        throw new FileSystemException('File: Unable to copy file '.$this->file.'!');
    }

    /**
     * @inheritDoc
     */
    public function exists(): bool {
        return $this->fsFile->exists();
    }

    public function rename($dst, $force = false)
    {
        return $this->move($dst, $force);
    }

    public function move($dst, $force = false)
    {
        if (!$force && file_exists($dst)) {
            return true;
        }

        // $dst can be a relative path
        $path = \Env::get('cx')->getWebsiteDocumentRootPath();
        if (
            $path != \Env::get('cx')->getCodeBaseDocumentRootPath()
            && strpos($dst, \Env::get('cx')->getCodeBaseDocumentRootPath()) === 0
        ) {
            if (!$this->fsFile->isPrivileged()) {
                throw new FileSystemException('non-permitted copy operation');
            }
            $path = \Env::get('cx')->getCodeBaseDocumentRootPath();
        } elseif (!empty(\Env::get('cx')->getCodeBaseOffsetPath()) && strpos($dst, \Env::get('cx')->getCodeBaseOffsetPath()) === 0) {
            if (!$this->fsFile->isPrivileged()) {
                throw new FileSystemException('non-permitted copy operation');
            }
            $path = \Env::get('cx')->getCodeBaseOffsetPath();
        }
        $relPath    = str_replace($path, '', $dst);
        $pathInfo   = pathinfo($relPath);
        $arrFolders = explode('/', $pathInfo['dirname']);

        foreach ($arrFolders as $folder) {
            if (empty($folder)) continue;
            $path .= '/' . $folder;
            if (!is_dir($path)) {
                \Cx\Lib\FileSystem\FileSystem::make_folder($path, false, $this->fsFile->isPrivileged());
            }
        }

        // use PHP
        if (   $this->accessMode == self::PHP_ACCESS
            || $this->accessMode == self::UNKNOWN_ACCESS
        ) {
            try {
                // try regular file access first
                $this->fsFile->move($dst);
                return true;
            } catch (FileSystemFileException $e) {
                \DBG::log('FileSystemFile: '.$e->getMessage());
            }
        }

        throw new FileSystemException('File: Unable to copy file '.$this->file.'!');
    }

    /**
     * Sets write access to file's owner
     *
     * @throws FileSystemException if setting write access fails
     * @return  TRUE if file is already writable or setting write access was successful
     */
    public function makeWritable()
    {
        // use PHP
        if (   $this->accessMode == self::PHP_ACCESS
            || $this->accessMode == self::UNKNOWN_ACCESS
        ) {
            try {
                $this->fsFile->makeWritable();
                return true;
            } catch (FileSystemFileException $e) {
                \DBG::log('FileSystemFile: '.$e->getMessage());
            }
        }

        throw new FileSystemException('File: Unable to set write access to file '.$this->file.'!');
    }

    /**
     * Removes file
     *
     * @throws FileSystemException if removing of file fails
     * @return TRUE if file has successfully been removed
     */
    public function delete()
    {
        $objFile = null;

        // use PHP
        if (   $this->accessMode == self::PHP_ACCESS
            || $this->accessMode == self::UNKNOWN_ACCESS
        ) {
            try {
                $objFile = $this->getFileInstance();
                $objFile->delete();
            } catch (FileSystemFileException $e) {
                \DBG::log('FileSystemFile: '.$e->getMessage());
            }
        }

        if ($objFile) {
            clearstatcache(true, $objFile->getAbsoluteFilePath());
        }
        if ($objFile && file_exists($objFile->getAbsoluteFilePath())) {
            throw new FileSystemException('File: Unable to delete file '.$this->file.'!');
        }

        return true;
    }

    /**
     * Get absolute path of file
     *
     * @return string Absolute path of file
     */
    public function getAbsoluteFilePath()
    {
        return $this->fsFile->getAbsoluteFilePath();
    }

    /**
     * Get relative path of file
     *
     * @return string Relative path of file
     */
    public function getRelativeFilePath(): string {
        return $this->fsFile->getRelativeFilePath();
    }

    /**
     * @inheritDoc
     */
    public function isWithinWebsiteData(): bool {
        return $this->fsFile->isWithinWebsiteData();
    }

    /**
     * @inheritDoc
     */
    public function __toString(): string {
        return $this->fsFile->__toString();
    }
}
