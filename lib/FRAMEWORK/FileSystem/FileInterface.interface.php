<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * File Interface
 *
 * @copyright   CLOUDREXX CMS - CLOUDREXX AG
 * @author      CLOUDREXX Development Team <info@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  lib_filesystem
 */

namespace Cx\Lib\FileSystem;

/**
 * FileInterface
 *
 * @copyright   CLOUDREXX CMS - CLOUDREXX AG
 * @author      CLOUDREXX Development Team <info@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  lib_filesystem
 */
interface FileInterface {
    public function write($data);
    public function append($data);
    public function touch();
    public function copy($dst);
    public function rename($dst, $force = false);
    public function move($dst, $force = false);
    public function delete();
    public function makeWritable();
    public function getAbsoluteFilePath();
    public function getRelativeFilePath(): string;

    /**
     * Returns whether this file is part of website data
     *
     * @return bool True if this is a website data file
     */
    public function isWithinWebsiteData(): bool;

    /**
     * Returns the path to this file
     *
     * If the file is within the website directory a relative path is returned.
     * Otherwise the absolute path is returned to avoid confusion.
     * @return string Path to this file
     */
    public function __toString(): string;

    /**
     * Wrapper for file_exists()
     *
     * @return bool True if the file exists
     */
    public function exists(): bool;

    /**
     * Returns a file handle for the given file
     *
     * If you want to use this handle for writing you need to ensure proper
     * locking yourself!
     * @param bool $writable Set to true if you need to write to the handle
     * @throws FileException If the file handle cannot be opened or granted
     * @return resource A file handle
     */
    public function getHandle(bool $writable = false);
}
