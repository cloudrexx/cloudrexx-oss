<?php

/**
 * Cloudrexx
 *
 * @link      https://www.cloudrexx.com
 * @copyright Cloudrexx AG
 *
 * This file is part of Cloudrexx.
 *
 * Cloudrexx is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3 of the License.
 *
 * Cloudrexx is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * Image manager
 *
 * @TODO This class currently mixes two things: It represents an image and it
 *          provides helper methods. Helper methods should probably be static.
 * @copyright   Cloudrexx AG
 * @author      Paulo M. Santos <pmsantos@astalavista.net>
 * @author      Michael Ritter <michael.ritter@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  lib
 */
class ImageManager {
    /**
     * @todo While this is inteded to be a resource this is sometimes set to string
     * @var resource Current image
     */
    protected $orgImage;

    /**
     * @todo This should be protected and be accessed via getter instead
     * @todo While this is inteded to be a int this is sometimes set to string
     * @var int Width of this image
     */
    public $orgImageWidth;

    /**
     * @todo This should be protected and be accessed via getter instead
     * @todo While this is inteded to be a int this is sometimes set to string
     * @var int Height of this image
     */
    public $orgImageHeight;

    /**
     * @todo While this is inteded to be a int this is sometimes set to string or bool
     * @var int Current image type, one of the IMG_TYPE_... constants below
     */
    protected $orgImageType;

    /**
     * @var string Filename of this image
     */
    protected $orgImageFile;

    /**
     * @todo While this is inteded to be a resource this is sometimes set to string
     * @todo This should not exist. Instead we should directly work with $orgImage
     * @var resource Image currently being edited
     */
    protected $newImage;

    /**
     * @todo While this is inteded to be a int this is sometimes set to string
     * @todo This should not exist. Instead we should directly work with $orgImage
     * @var int Width of the image currently being edited
     */
    protected $newImageWidth;

    /**
     * @todo While this is inteded to be a int this is sometimes set to string
     * @todo This should not exist. Instead we should directly work with $orgImage
     * @var int Height of the image currently being edited
     */
    protected $newImageHeight;

    /**
     * @todo While this is inteded to be a int this is sometimes set to string
     * @todo This should not exist. Instead we should directly work with $orgImage
     * @var int Quality of the image currently being edited
     */
    protected $newImageQuality;

    /**
     * @todo While this is inteded to be a int this is sometimes set to string or bool
     * @todo This should not exist. Instead we should directly work with $orgImage
     * @var int Current image type, one of the IMG_TYPE_... constants below
     */
    protected $newImageType;

    /**
     * @todo This should not exist. Instead we should directly work with $orgImage
     * @var string Filename of the image currently being edited
     */
    protected $newImageFile;

    /**
     * @var bool Whether the current image has been checked to be a valid image
     */
    protected bool $imageCheck = false;

    /**
     * @var int Image type GIF
     */
    const IMG_TYPE_GIF = 1;

    /**
     * @var int Image type JPEG
     */
    const IMG_TYPE_JPEG = 2;

    /**
     * @var int Image type PNG
     */
    const IMG_TYPE_PNG = 3;

    /**
     * @var int Image type WEBP
     */
    const IMG_TYPE_WEBP = 18;

    /**
     * Caches the ICC default color space profile
     *
     * Never use this directly, always use getIccProfile() instead!
     * @var string
     */
    protected static $iccProfile;

    /**
     * Constructor
     *
     * @TODO: This should directly do what loadImage() does.
     */
    public function __construct() {
        $this->_resetVariables();
    }

    /**
     * Gets several image information and saves it in the image variables.
     *
     * SHOULD NOT be called for SVG images, as the image functions used
     * do not support that format.
     * This method does not use exceptions because it is called by older
     * methods without catchers.
     * @access  public
     * @param   string  $file  Path and filename of the existing image.
     * @return  bool           True on success, false otherwise.
     */
    public function loadImage($file) {
        $this->_resetVariables();
        $this->orgImageFile = $file;
        $this->orgImageType = $this->_isImage($this->orgImageFile);

        if ($this->orgImageType) {
            $getImage             = $this->_getImageSize($this->orgImageFile);
            $this->orgImageWidth  = $getImage[0];
            $this->orgImageHeight = $getImage[1];
            $this->orgImage       = $this->_imageCreateFromFile($this->orgImageFile);
            $this->imageCheck = true;

            if ($this->orgImage) return true;

            $this->_resetVariables();
            return false;
        }

        $this->_resetVariables();
        return false;
    }

    /**
     * Add Background Layer
     *
     * This scales the image to a size that it fits into the rectangle defined by width $width and height $height.
     * Spaces at the edges will be padded with the color $bgColor.
     * @param   array   $bgColor is an array containing 3 values, representing the red, green and blue portion (0-255) of the desired color.
     * @param   integer The width of the rectangle
     * @param   integer The height of the rectangle
     */
    public function addBackgroundLayer($bgColor, $width=null, $height=null) {
        if (function_exists ("imagecreatetruecolor")) {
            $this->newImage = imagecreatetruecolor($width, $height);
            // GD > 2 check
            if (!$this->newImage) {
                $this->newImage = ImageCreate($this->newImageWidth, $this->newImageHeight);
            }
        } else {
            $this->newImage = ImageCreate($width, $height);
        }
        imagefill($this->newImage, 0, 0,
            imagecolorallocate($this->newImage,
                $bgColor[0], $bgColor[1], $bgColor[2]));
        $ratio = max($this->orgImageWidth / $width, $this->orgImageHeight / $height);
        $scaledWidth = $this->orgImageWidth / $ratio;
        $scaledHeight = $this->orgImageHeight / $ratio;
        $offsetWidth = ($width - $scaledWidth) / 2;
        $offsetHeight = ($height - $scaledHeight) / 2;
        imagecopyresized($this->newImage, $this->orgImage,
            $offsetWidth, $offsetHeight, 0, 0,
            $scaledWidth, $scaledHeight,
            $this->orgImageWidth, $this->orgImageHeight);
        $this->imageCheck = true;
        $this->newImageQuality = 100;
        $this->newImageType = $this->orgImageType;
    }

    /**
     * Creates a thumbnail of a picture
     *
     * Note that all "Path" parameters are required to bring along their
     * own trailing slash.
     * This is a no-op for SVG images, and returns true.
     * @param   string  $strPath
     * @param   string  $strWebPath
     * @param   string  $file
     * @param   int     $maxSize        The maximum width or height
     * @param   int     $quality
     * @param   string  $thumb_name
     * @param   bool    $generateThumbnailByRatio
     *
     * @return  bool                    True on success, false otherwise
     */
    public function _createThumb($strPath, $strWebPath, $file, $maxSize=80, $quality=90, $thumb_name='', $generateThumbnailByRatio = false) {
        if (!file_exists($strPath.$file)) {
            \DBG::log('Unable to create thumbnail for non-existing image: ' . $strPath . $file);
            return false;
        }
        if (preg_match('/\.svg$/i', $file)) {
            return true;
        }
        $_objImage = new ImageManager();
        $file      = basename($file);
        $tmpSize   = getimagesize($strPath.$file);
        if (!$tmpSize) {
            \DBG::log('Unable to fetch image size dimensions for thumbnail creation of ' . $strPath . $file);
            return false;
        }
        $factor = 1;
        if ($tmpSize[0] > $tmpSize[1]) {
           $factor = $maxSize / $tmpSize[0];
        } else {
           $factor = $maxSize / $tmpSize[1] ;
        }
        $thumbWidth  = max($tmpSize[0] * $factor, 1);
        $thumbHeight = max($tmpSize[1] * $factor, 1);
        if (!$_objImage->loadImage($strPath.$file)) return false;

        if ($generateThumbnailByRatio && !$_objImage->resizeImageWithAspectRatio($thumbWidth, $thumbHeight, $quality)) {
            return false;
        } elseif (!$generateThumbnailByRatio && !$_objImage->resizeImage($thumbWidth, $thumbHeight, $quality)) {
            return false;
        }

        if (!(strlen($thumb_name) > 0)) {
            $thumb_name = self::getThumbnailFilename($file);
        }
        if (!$_objImage->saveNewImage($strPath.$thumb_name, false, false)) return false;
        if (!\Cx\Lib\FileSystem\FileSystem::makeWritable($strPath.$thumb_name)) return false;
        return true;
    }

    /**
     * Create a thumbnail of a picture.
     *
     * This is very much like {@link _createThumb()}, but provides more
     * parameters.  Both the width and height of the thumbnail may be
     * specified; the picture will still be scaled to fit within the given
     * sizes while keeping the original width/height ratio.
     * In addition to that, this method tries to delete an existing
     * thumbnail before attempting to write the new one.
     * This is a no-op for SVG images, and returns true.
     * Note that all "Path" parameters are required to bring along their
     * own trailing slash.
     *
     * @todo    Refactor redundant code
     * @param   string  $strPath        The image file folder
     * @param   string  $strWebPath     The image file web folder
     * @param   string  $file           The image file name
     * @param   integer $maxWidth       The maximum width of the image
     * @param   integer $maxHeight      The maximum height of the image
     * @param   integer $quality        The desired jpeg thumbnail quality
     * @param   string  $thumbNailSuffix  Suffix of the thumbnail. Default is 'thumb'
     * @param   string  $strPathNew     Image file store folder. Default is $strPath
     * @param   string  $strWebPathNew  Image file web store folder. Default is $strWebPath
     * @param   string  $fileNew        Image file store name. Default is $file
     * @return  bool                    True on success, false otherwise.
     */
    public function _createThumbWhq(
        $strPath, $strWebPath, $file, $maxWidth=80, $maxHeight=80, $quality=90,
        $thumbNailSuffix='.thumb', $strPathNew=null, $strWebPathNew=null, $fileNew=null,
        $fixSize = false,
        $crop = false
    ) {
        if (preg_match('/\.svg$/i', $file)) {
            return true;
        }
        // Do *NOT* strip subfolders from the file name!
        // This would break correct operation in some places (shop)
        // Fix your own code to provide the file name only if you need to.
        //$file      = basename($file);
        if (empty($fileNew))       $fileNew       = $file;
        if (empty($strPathNew))    $strPathNew    = $strPath;
        if (empty($strWebPathNew)) $strWebPathNew = $strWebPath;
        $tmpSize = getimagesize($strPath.$file);
        if (!$tmpSize) {
            $tmpSize = array(
                0 => 1,
                1 => 1,
            );
        }
        // reset the ImageManager
        $this->imageCheck = true;

        if (
            \Cx\Core\Setting\Controller\Setting::getValue(
                'CLX2298_EnableThumbnailFormatCustomization',
                'Config'
            ) === 'on'
        ) {
            if (!$this->loadImage($strPath.$file)) return false;
            $thumbWidth  = $maxWidth;
            $thumbHeight = $maxHeight;
            $offsetLeft = 0;
            $offsetTop = 0;

            $fit = $fixSize && !$crop;
            $bgColor = '#ffffff';
            if ($fixSize) {
                $rationWidth = $this->orgImageWidth / $maxWidth;
                $rationHeight = $this->orgImageHeight / $maxHeight;
                if (
                    $crop
                    && $rationWidth < $rationHeight
                    || $fit
                    && $rationWidth > $rationHeight
                ) {
                    $offsetTop = ($this->orgImageHeight - ($maxHeight * $rationWidth)) / 2;
                    $this->orgImageHeight = $this->orgImageHeight / $rationHeight * $rationWidth;
                } else {
                    $offsetLeft = ($this->orgImageWidth - ($maxWidth * $rationHeight)) / 2;
                    $this->orgImageWidth = $this->orgImageWidth / $rationWidth * $rationHeight;
                }
            } else {
                $width       = $tmpSize[0];
                $height      = $tmpSize[1];
                $widthRatio  = $width/$maxWidth;
                $heightRatio = $height/$maxHeight;
                $thumbWidth  = 0;
                $thumbHeight = 0;
                if ($widthRatio < $heightRatio) {
                    $thumbHeight = $maxHeight;
                    $thumbWidth  = $width*$maxHeight/$height;
                } else {
                    $thumbWidth  = $maxWidth;
                    $thumbHeight = $height*$maxWidth/$width;
                }
            }
            if (!$this->resize($thumbWidth, $thumbHeight, $quality, $offsetLeft, $offsetTop, $fit, $bgColor)) return false;
        } else {
            $width       = $tmpSize[0];
            $height      = $tmpSize[1];
            $widthRatio  = $width/$maxWidth;
            $heightRatio = $height/$maxHeight;
            $thumbWidth  = 0;
            $thumbHeight = 0;
            if ($widthRatio < $heightRatio) {
                $thumbHeight = $maxHeight;
                $thumbWidth  = $width*$maxHeight/$height;
            } else {
                $thumbWidth  = $maxWidth;
                $thumbHeight = $height*$maxWidth/$width;
            }
            if (!$this->loadImage($strPath.$file)) return false;
            if (!$this->resizeImage($thumbWidth, $thumbHeight, $quality)) return false;
        }
        if (is_file($strPathNew.$fileNew.$thumbNailSuffix)) {
            if (!\Cx\Lib\FileSystem\FileSystem::delete_file($strPathNew.$fileNew.$thumbNailSuffix)) return false;
        }
        if (!$this->saveNewImage($strPathNew.$fileNew.$thumbNailSuffix, false, false)) return false;
        if (!\Cx\Lib\FileSystem\FileSystem::makeWritable($strPathNew.$fileNew.$thumbNailSuffix)) return false;
        return true;
    }

    /**
     * Resizes the original image to the given dimensions and stores it as a new.
     * This method does not use exceptions because it is called by older methods without catchers.
     *
     * @access  public
     * @param   string   $width    The width of the new image.
     * @param   string   $height   The height of the new image.
     * @param   string   $quality  The quality for the new image.
     * @return  booelan            True on success, false otherwise.
     */
    public function resizeImage($width, $height, $quality) {
        if (!$this->imageCheck) return false;

        //Create a new image for given size
        $this->createNewImageForResize($width, $height, $quality);
        if (function_exists('imagecopyresampled')) { //resampled is gd2 only
            imagecopyresampled(
                $this->newImage,
                $this->orgImage,
                0,
                0,
                0,
                0,
                (int) $this->newImageWidth,
                (int) $this->newImageHeight,
                (int) $this->orgImageWidth,
                (int) $this->orgImageHeight
            );
        } else {
            imagecopyresized($this->newImage,
                $this->orgImage,
                0,
                0,
                0,
                0,
                (int) $this->newImageWidth,
                (int) $this->newImageHeight,
                (int) $this->orgImageWidth,
                (int) $this->orgImageHeight
            );
        }

        if ($this->newImage) {
            return true;
        }
        return false;
    }

    /**
     * @todo Merge with {@see static::resizeImage()}
     */
    protected function resize(
        $width,
        $height,
        $quality = 100,
        $offsetLeft = 0,
        $offsetTop = 0,
        $fit = false,
        $backgroundFillColor = ''
    ): bool {
        if (!$this->imageCheck) return false;

        $setBackground = !empty($backgroundFillColor);
        if (
            $fit
            && $width > $this->orgImageWidth
        ) {
            $setBackground = true;
        }

        // this modifies the following properties:
        // - $this->newImageWidth
        // - $this->newImageHeight
        // - $this->orgImageWidth
        // - $this->orgImageHeight
        //$this->createNewImageForResize($width, $height, $quality, $setBackground, $backgroundFillColor);
        $this->createNewImageForResize($width, $height, $quality, $setBackground);

        if (
            $fit
            && $this->newImageWidth > $this->orgImageWidth
        ) {
            $dstX = round(($this->newImageWidth - $this->orgImageWidth) / 2);
            $newWidth  = $this->orgImageWidth;
        } else {
            $dstX      = 0;
            $newWidth  = $this->newImageWidth;
        }

        if (
            $fit
            && $this->newImageHeight > $this->orgImageHeight
        ) {
            $dstY = round(($this->newImageHeight - $this->orgImageHeight) / 2);
            $newHeight = $this->orgImageHeight;
        } else {
            $dstY      = 0;
            $newHeight = $this->newImageHeight;
        }

        if (function_exists('imagecopyresampled')) { //resampled is gd2 only
            $imgFn = 'imagecopyresampled';
        } else {
            $imgFn = 'imagecopyresized';
        }

        $imgFn(
            $this->newImage,
            $this->orgImage,
            $dstX,
            $dstY,
            $offsetLeft,
            $offsetTop,
            $newWidth,
            $newHeight,
            $this->orgImageWidth,
            $this->orgImageHeight
        );

        if ($this->newImage) {
            return true;
        }
        return false;
    }

    /**
     * create new image for resize
     *
     * @param   string   $width    The width of the new image.
     * @param   string   $height   The height of the new image.
     * @param   string   $quality  The quality for the new image.
     */
    protected function createNewImageForResize($width, $height, $quality, $setWhiteBackground = false): void {
        if ($this->newImage) {
            $this->orgImage       = $this->newImage;
            $this->orgImageWidth  = $this->newImageWidth;
            $this->orgImageHeight = $this->newImageHeight;
        }

        $this->newImageWidth = (int) $width;
        $this->newImageHeight = (int) $height;
        $this->newImageQuality = $quality;
        $this->newImageType = $this->orgImageType;

        if (function_exists('imagecreatetruecolor')) {
            $this->newImage = @imagecreatetruecolor($this->newImageWidth, $this->newImageHeight);
            // GD > 2 check
            if ($this->newImage) {
                if ($setWhiteBackground) {
                    imagefill($this->newImage, 0, 0, imagecolorallocate($this->newImage, 255, 255, 255));
                } else {
                    $this->setTransparency();
                }
            } else {
                $this->newImage = imagecreate($this->newImageWidth, $this->newImageHeight);
            }
        } else {
            $this->newImage = imagecreate($this->newImageWidth, $this->newImageHeight);
        }
    }

    /**
     * Resize the image with aspect ratio and fill the white color in empty area
     *
     * @param   string   $width    The width of the new image.
     * @param   string   $height   The height of the new image.
     * @param   string   $quality  The quality for the new image.
     *
     * @return boolean
     */
    public function resizeImageWithAspectRatio($width, $height, $quality): bool {
        if (!$this->imageCheck) return false;

        $setWhiteBackground = ($width  > $this->orgImageWidth) ? true : false;
        //Create a new image for given size
        $this->createNewImageForResize($width, $height, $quality, $setWhiteBackground);

        $dstX      = ($this->newImageWidth  > $this->orgImageWidth) ? round(($this->newImageWidth - $this->orgImageWidth) / 2) : 0;
        $dstY      = ($this->newImageHeight > $this->orgImageHeight) ? round(($this->newImageHeight - $this->orgImageHeight) / 2) : 0;
        $newWidth  = ($this->newImageWidth  > $this->orgImageWidth) ? $this->orgImageWidth : $this->newImageWidth;
        $newHeight = ($this->newImageHeight > $this->orgImageHeight) ? $this->orgImageHeight : $this->newImageHeight;

        if (function_exists('imagecopyresampled')) { //resampled is gd2 only
            imagecopyresampled($this->newImage, $this->orgImage, $dstX, $dstY, 0, 0, $newWidth, $newHeight, $this->orgImageWidth, $this->orgImageHeight);
        } else {
            imagecopyresized($this->newImage, $this->orgImage, $dstX, $dstY, 0, 0, $newWidth, $newHeight, $this->orgImageWidth, $this->orgImageHeight);
        }

        if ($this->newImage) {
            return true;
        }
        return false;
    }

    /**
     * Add transparency to new image
     *
     * Define a color as transparent or add alpha channel,
     * depending on the image file type.
     *
     * @return void
     */
    protected function setTransparency(): void {
        switch ($this->orgImageType) {
            case self::IMG_TYPE_GIF:
                $transparentColorIdx = imagecolortransparent($this->orgImage);
                if (
                    $transparentColorIdx >= 0
                    && $transparentColorIdx < imagecolorstotal($this->orgImage)
                ) {
                    // it's transparent
                    $transparentColor = imagecolorsforindex($this->orgImage, $transparentColorIdx);
                    $transparentColorIdx = imagecolorallocate($this->newImage, $transparentColor['red'], $transparentColor['green'], $transparentColor['blue']);
                    imagefill($this->newImage, 0, 0, $transparentColorIdx);
                    imagecolortransparent($this->newImage, $transparentColorIdx);
                }
                break;
            case self::IMG_TYPE_PNG:
            case self::IMG_TYPE_WEBP:
                imagealphablending($this->newImage, false);
                $colorTransparent = imagecolorallocatealpha($this->newImage, 0, 0, 0, 127);
                imagefill($this->newImage, 0, 0, $colorTransparent);
                imagesavealpha($this->newImage, true);
                break;
        }
    }

    /**
     * Saves the new image wherever you want
     *
     * @todo    In case the PHP script has no write access to the location set by $this->newImageFile,
     *          the image shall be sent to the output buffer and then be put into the new file
     *          location using the FileSystemFile object.
     * @todo    Check for all usages of this method if argument
     *          $injectTransparency must be set to FALSE.
     * @access  public
     * @param   string    $file             The path for the image file to be written.
     * @param   booelan   $forceOverwrite   Force overwriting existing files if true.
     * @param   booelan   $injectTransparency Whether or not to set transparency
     *                                      info on new image.
     * @return  boolean                     True on success, false otherwise.
     */
    public function saveNewImage($file, $forceOverwrite=false, $injectTransparency = true) {
// TODO: Add some sort of diagnostics (Message) here and elsewhere in this class
        if (!$this->imageCheck) return false;
        if (empty($this->newImage)) return false;
        if (file_exists($file)) {
            if (!$forceOverwrite) {
                return false;
            }
            \Cx\Lib\FileSystem\FileSystem::makeWritable($file);
        } else {
            try {
                $objFile = new \Cx\Lib\FileSystem\File($file);
                $objFile->touch();
            } catch (\Cx\Lib\FileSystem\FileSystemException $e) {
                \DBG::msg($e->getMessage());
            }
        }
// TODO: Unfortunately, the functions imagegif(), imagejpeg(), imagepng() and imagewebp() can't use the Cloudrexx FileSystem wrapper,
//       therefore we need to set the global write access image files.
//       This issue might be solved by using the output-buffer and write the image manually afterwards.
//
//       IMPORTANT: In case something went wrong (see bug #1441) and the path $strPathNew.$strFileNew refers to a directory
//       we must abort the operation here, otherwise we would remove the execution flag on a directory, which would
//       cause to remove any browsing access to the directory.
        if (is_dir($file)) {
            return false;
        }
        \Cx\Lib\FileSystem\FileSystem::chmod($file, 0666);//\Cx\Lib\FileSystem\FileSystem::CHMOD_FILE);

        $this->newImageFile = $file;

        if (
            $injectTransparency &&
            $this->newImageType == self::IMG_TYPE_PNG
        ) {
            $this->setTransparency();
        }

        try {
            $this->outputImage($this->newImageFile);
        } catch (\Exception $e) {
            return false;
        }
        return true;
    }

    /**
     * Returns the proper quality value for creating an image of the
     * current type
     *
     * The value returned is the original value set, except for type PNG.
     * The latter requires the quality to be in the range [0..9] instead
     * of [0..100].
     * @return  integer             The quality, scaled for the current type
     */
    public function getQuality() {
        switch($this->newImageType) {
            case self::IMG_TYPE_PNG:
                return min(9, intval($this->newImageQuality / 10));
                break;
        }
        return $this->newImageQuality;
    }

    /**
     * Outputs the new image in the browser
     * @access   public
     * @return   boolean      True on success, false otherwise
     */
    public function showNewImage() {
        $this->newImage     = !empty($this->newImage)     ? $this->newImage     : $this->orgImage;
        $this->newImageType = !empty($this->newImageType) ? $this->newImageType : $this->orgImageType;

        if (!$this->imageCheck) return false;
        if (empty($this->newImage)) return false;

        if ($this->newImageType == self::IMG_TYPE_PNG) {
            $this->setTransparency();
        }

        try {
            $this->outputImage();
        } catch (\Exception $e) {
            return false;
        }
        return true;
    }

    /**
     * Outputs the edited image ($this->newImage)
     *
     * Takes type and quality (if available) from $this->newImageType
     * and $this->getQuality() into account.
     * If $output is null the correct headers are set.
     * @param resource|string|null $output (optional) File to save output to or null to output to browser
     * @throws \Exception If the type of the edited image is unknown
     */
    protected function outputImage($output = null): void {
        $contentType = '';
        switch ($this->newImageType) {
            case self::IMG_TYPE_GIF:
                $contentType = 'image/gif';
                $function = 'imagegif';
                if (!function_exists($function)) {
                    $function = 'imagejpeg';
                }
                break;
            case self::IMG_TYPE_JPEG:
                $contentType = 'image/jpeg';
                $function = 'imagejpeg';
                break;
            case self::IMG_TYPE_PNG:
                $contentType = 'image/png';
                $function = 'imagepng';
                break;
            case self::IMG_TYPE_WEBP:
                $contentType = 'image/webp';
                $function = 'imagewebp';
                break;
            default:
                throw new \Exception('Unknown type');
        }
        if ($output === null) {
            header('Content-type: ' . $contentType);
        }
        // Only adjust quality, if it is set.
        if (
            $this->newImageQuality != ''
            # imagegif does not have $quality argument
            && $function !== 'imagegif'
        ) {
            $function($this->newImage, $output, $this->getQuality());
        } else {
            $function($this->newImage, $output);
        }
    }

    /**
     * Resets all object variables.
     *
     * @return   void
     */
    protected function _resetVariables(): void {
        $this->orgImage        = '';
        $this->orgImageWidth   = '';
        $this->orgImageHeight  = '';
        $this->orgImageType    = '';
        $this->orgImageFile    = '';

        $this->newImage        = '';
        $this->newImageWidth   = '';
        $this->newImageHeight  = '';
        $this->newImageQuality = 100;
        $this->newImageType    = '';
        $this->newImageFile    = '';
        $this->imageCheck = false;
    }

    /**
     * Creates an image from an image file
     *
     * SHOULD NOT be called for SVG images, as the image functions used
     * do not support that format.
     * @access   private
     * @param    string   $file   The path of the image
     * @return   resource         The image on success, the empty or false string otherwise
     */
    protected function _imageCreateFromFile($file) {
        $arrSizeInfo = getimagesize($file);
        if (!is_array($arrSizeInfo)) return false;

        // fetch channels of image
        if (
            !isset($arrSizeInfo['channels']) ||
            $arrSizeInfo['channels'] < 3
        ) {
            $channels = 3;
        } else {
            $channels = $arrSizeInfo['channels'];
        }

        // fetch bits per pixel of image
        if (!isset($arrSizeInfo['bits'])) {
            $bits = 8;
        } else {
            $bits = $arrSizeInfo['bits'];
        }

        $type = $this->_isImage($file);

        // see comments of https://www.php.net/manual/en/function.imagecreatefromjpeg.php
        $fudgeFactor = 2.2;

        // width * height * "fudge factor"
        $potentialRequiredMemory = $arrSizeInfo[0] * $arrSizeInfo[1] * $fudgeFactor;

        // multiply required memory by number of bits and channels per pixel
        // depending on image type
        switch($type) {
            case self::IMG_TYPE_GIF:
                $function = 'imagecreatefromgif';
                break;
            case self::IMG_TYPE_JPEG:
                $function = 'imagecreatefromjpeg';
                $potentialRequiredMemory *= $bits / 8 * $channels;
                break;
            case self::IMG_TYPE_PNG:
                $function = 'imagecreatefrompng';
                $potentialRequiredMemory *= $bits;
                break;
            case self::IMG_TYPE_WEBP:
                // Animated WebPs are not supported, same as GIF.
                // Lossless WebPs are supported as of PHP 8.1.
                $function = 'imagecreatefromwebp';
                $potentialRequiredMemory *= $bits;
                break;
            default:
                return '';
        }

        // round potential required memory to int
        $potentialRequiredMemory = round($potentialRequiredMemory);

        require_once(ASCMS_FRAMEWORK_PATH.'/System.class.php');
        $objSystem = new FWSystem();
        if ($objSystem === false) return false;
        $memoryLimit = $objSystem->getBytesOfLiteralSizeFormat(
            @ini_get('memory_limit'));
        if (empty($memoryLimit)) {
            // set default php memory limit of 8 MBytes
            $memoryLimit = 8 * pow(1024, 2);
        }
        // fetch already used memory
        if (function_exists('memory_get_usage')) {
            $potentialRequiredMemory += memory_get_usage();
        } else {
            // as we are unable to fetch the currently used memory,
            // we do continue our calculation with an expected
            // amount of used memory of 32 MB
            $potentialRequiredMemory += 32 * pow(1024, 2);
        }
        if ($potentialRequiredMemory > $memoryLimit) {
            // try to set a higher memory_limit
            if (   !ini_set('memory_limit', $potentialRequiredMemory)
                || $memoryLimit == $objSystem->getBytesOfLiteralSizeFormat(ini_get('memory_limit')))
                return '';
        }
        return $function($file);
    }

    /**
     * Returns the image type as determined by getimagesize() for the given file.
     *
     * Only accepts supported web image types (GIF, JPG, PNG or WEBP).
     * If the function imagecreatefromgif() is not available, GIF images aren't
     * accepted.
     * False is returned for images/files that are not supported.
     * @param     string    $file   The file path of the image
     * @return    bool|integer           The file type on success, false otherwise
     */
    public static function _isImage($file) {
        try {
            $fsFile = new \Cx\Lib\FileSystem\File($file);
            $file = $fsFile->getAbsoluteFilePath();
        } catch (\Cx\Lib\FileSystem\FileSystemException $e) {
            return false;
        }

        if (is_dir($file)) {
            return false;
        }

        $finfo = new \finfo(FILEINFO_MIME_TYPE);
        $mimeType = $finfo->file($file);

        if (strpos($mimeType, 'image') !== 0) {
            return false;
        }

        $type = substr($mimeType, strpos($mimeType, '/') + 1);
        switch ($type) {
            case 'gif':
                return static::IMG_TYPE_GIF;
                break;

            case 'jpeg':
                return static::IMG_TYPE_JPEG;
                break;

            case 'png':
                return static::IMG_TYPE_PNG;
                break;

            case 'webp':
                return static::IMG_TYPE_WEBP;
                break;
        }
        return false;
    }

    /**
     * Gets the size of the image
     * @access    private
     * @param     string    $file   The path of the image
     * @return    array             The array as returned by @getimagesize($file)
     *                              on success, false otherwise
     * @todo      This method is completely redundant.  It does exactly the same
     *            as calling @getimagesize($file) directly! - Remove.
     */
    public function _getImageSize($file) {
        $getImageSize = @getimagesize($file);
        if ($getImageSize) return $getImageSize;
        return false;
    }

    /**
     * Returns the file name for the thumbnail image
     *
     * Replaces the .png extension with .jpg if a thumbnail in PNG format is
     * already present.  This is for backwards compatibility with versions up
     * to 2.2 which did not create PNG thumbnails.
     * Appends the .thumb extension if not already present.
     * @param   string    $file_name        The image file name
     * @return  string                      The thumbnail file name
     */
    public static function getThumbnailFilename($file_name) {
        $thumbnail = \Cx\Core\Core\Controller\Cx::instanciate()
            ->getMediaSourceManager()
            ->getThumbnailGenerator()
            ->getThumbnailFilename($file_name);
        return $thumbnail;
    }

    /**
     * Rotates the image 90 degrees to the left or right.
     *
     * @access  public
     * @param   float   $angle  Rotation angle, in degrees. The rotation angle is interpreted as the number of degrees to rotate the image anticlockwise.
     * @return  bool            True on success, false otherwise.
     */
    public function rotateImage($angle) {
        if ($this->imageCheck) {
            $angle = ($angle <= 360) || ($angle >= 0) ? $angle : 0;
            $this->newImage = imagerotate($this->orgImage, $angle, 0);

            if ($this->newImage) {
                $this->setTransparency();

                $this->newImageWidth = imagesx($this->newImage);
                $this->newImageHeight = imagesy($this->newImage);
                $this->newImageType  = $this->orgImageType;

                return true;
            }

            throw new Exception('Could not rotate image');
        }

        throw new Exception('Is not a valid image');
    }

    /**
     * Crops the image with the given coordinates.
     *
     * @access  public
     * @param   int   $x       X-coordinate for the new image.
     * @param   int   $y       Y-coordinate for the new image.
     * @param   int   $width   Width for the new image.
     * @param   int   $height  Height for the new image.
     * @return  bool           True on success, false otherwise.
     */
    public function cropImage($x, $y, $width, $height) {
        if ($this->imageCheck) {
            $this->newImageWidth  = $width;
            $this->newImageHeight = $height;
            $this->newImageType   = $this->orgImageType;

            if ($this->newImage) {
                $this->orgImage = $this->newImage;
            }

            if (function_exists('imagecreatetruecolor')) {
                $this->newImage = @imagecreatetruecolor($this->newImageWidth, $this->newImageHeight);
                // GD > 2 check
                if ($this->newImage) {
                    $this->setTransparency();
                } else {
                    $this->newImage = imagecreate($this->newImageWidth, $this->newImageHeight);
                }
            } else {
                $this->newImage = imagecreate($this->newImageWidth, $this->newImageHeight);
            }

            imagecopy (
                $this->newImage,      // Source of new image
                $this->orgImage,      // Source of original image
                0,                    // X-coordinate of new image
                0,                    // Y-coordinate of new image
                $x,  // X-coordinate of original image
                $y,  // Y-coordinate of original image
                $this->newImageWidth, // New image width
                $this->newImageHeight // New image height
            );

            if (!empty($this->newImage)) {
                return true;
            }

            throw new Exception('Could not crop image');
        }

        throw new Exception('Is not a valid image');
    }

    /**
     * Fix image orientation based on its Exif-data
     *
     * @param string $filePath File path
     */
    public function fixImageOrientation($filePath) {
        if (!function_exists('exif_read_data')) {
            return;
        }

        // See EXIF "Supported filetypes" option in phpinfo()
        if (
            !in_array(
                exif_imagetype($filePath),
                array(IMAGETYPE_JPEG, IMAGETYPE_TIFF_II, IMAGETYPE_TIFF_MM)
            )
        ) {
            return;
        }

        // The method exif_read_data() reads a header information from an image.
        // It returns an associated array where the array indexes are the header
        // names and the array values are the values associated with those
        // headers.
        // If no data can be returned, exif_read_data() will return FALSE.
        $exif = exif_read_data($filePath);
        if (empty($exif['Orientation'])) {
            return;
        }

        $this->loadImage($filePath);
        // The information from the accelerometer is stored in the orientation
        // field of the Exchangeable Image File Format (Exif) metadata.
        // If the image has rotated, Exif data should have 'Orientation' in its
        // header information. The orientation is one of 3, 6 or 8. Here
        // orientation 3 will rotate 180 degree left, orientation 6 will
        // rotate 90 degree right and orientation 8 will rotate 90 degree left.
        // For more information about the Orientation image data structure
        // property see:
        // http://www.digicamsoft.com/exif22/exif22/html/exif22_24.htm?gInitialPosX=10px&gInitialPosY=10px&gZoomValue=100
        // or check http://www.cipa.jp for the latest Exif standard doc
        try {
            // Note that $this->rotateImage() does rotate the image anticlockwise
            switch ($exif['Orientation']) {
                case 3:
                    $this->rotateImage(180);
                    break;
                case 6:
                    $this->rotateImage(-90);
                    break;
                case 8:
                    $this->rotateImage(90);
                    break;
                default :
                    return;
            }
            $this->saveNewImage($filePath, true);
        } catch (\Exception $ex) {
            \DBG::msg($ex->getMessage());
        }
    }

    /**
     * Ensure the image uses the correct color space profile
     *
     * The W3C has defined sRGB as the default color space
     * ({@see https://www.w3.org/Graphics/Color/sRGB}). sRGB (standard Red Green
     * Blue) has been standardized by the International Electrotechnical
     * Commission (IEC) as IEC 61966-2-1
     * ({@see https://webstore.iec.ch/publication/6168}). The International
     * Color Consortium (ICC) has published a color profile for the default
     * color space sRGB. The profile is called 'IEC 61966-2-1 Default RGB Colour
     * Space - sRGB' ({@see http://color.org/srgbprofiles.xalter}).
     * Therefore, it is recommended that images (who do use color profiles) do
     * use the profile by the ICC. Otherwise the display is unkown as not all
     * end user devices do properly support color space profiles and do instead
     * most likely assume that an image uses the IEC 61966-2-1 Default RGB
     * Colour Space. Further, PHP's GD-library does also not support color space
     * profiles. Therefore, it does also always assume an image uses the ICC
     * standard profile. The latter causes unexpected results when performing
     * any image manipulation with GD on images that are not using the ICC
     * standard color space profile.
     * As a result, we need to ensure that all images that are uploaded are
     * using the ICC standard color space profile. If required, this means that
     * we have to convert any non-confirming images on the fly.
     *
     * @param string $filePath Image file path
     */
    public function fixImageColorSpace($filePath) {
        if (!extension_loaded('Imagick')) {
            \DBG::log(
                'PHP extension Imagick not installed.' .
                ' Unable to verify image\' color space profile'
            );
            return;
        }

        // abort for images that do not support color space profiles
        $this->loadImage($filePath);
        if (
            !in_array(
                $this->orgImageType,
                array(
                    static::IMG_TYPE_PNG,
                    static::IMG_TYPE_JPEG,
                )
            )
        ) {
            return;
        }

        // ICC default color space profile model is:
        // "IEC 61966-2-1 Default RGB Colour Space - sRGB"
        // Unfortunately, the naming is not standardized.
        // Therefore we have to match by regexp
        $iccDefaultModel = '/IEC\s+61966[-.]2[-.]1/i';

        // load image
        $image = new \Imagick($filePath);

        // verify image' color space profile
        $iccModel = $image->getImageProperty('icc:model');
        if (preg_match($iccDefaultModel, $iccModel)) {
            return;
        }

        // transform image' color space to sRGB in case it's not already using it
        $colorspace = $image->getImageColorspace();
        if ($colorspace != \Imagick::COLORSPACE_SRGB) {
            $image->transformImageColorspace(\Imagick::COLORSPACE_SRGB);
        }

        // assign ICC default sRGB color space profile
        $image->profileImage(
            'icc',
            $this->getIccProfile()
        );

        // update transformed image to disk
        $image->writeImage($filePath);
    }

    /**
     * Get the ICC default color space profile.
     * The profile will be loaded from the filesystem if not yet done.
     *
     * @return  string The ICC default color space profile
     */
    protected function getIccProfile(): string {
        if (!isset(static::$iccProfile)) {
            $cx = \Cx\Core\Core\Controller\Cx::instanciate();
            static::$iccProfile = file_get_contents(
                $cx->getCodeBaseLibraryPath() . '/ICCProfiles/sRGB2014.icc'
            );
        }
        return static::$iccProfile;
    }
}
