<?php

/**
 * Cloudrexx
 *
 * @link      https://www.cloudrexx.com
 * @copyright Cloudrexx AG
 *
 * This file is part of Cloudrexx.
 *
 * Cloudrexx is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3 of the License.
 *
 * Cloudrexx is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

// disabled logging at all
define('DBG_NONE',              0);

// enables logging of PHP issues (native & E_USER*)
define('DBG_PHP',               1<<0);

// legacy database abstraction log verbosity
define('DBG_ADODB_SELECT',      1<<1);
define('DBG_ADODB_CHANGE',      1<<3);
define('DBG_ADODB',             DBG_ADODB_SELECT | DBG_ADODB_CHANGE);
define('DBG_ADODB_TRACE',       1<<2);
define('DBG_ADODB_ERROR',       1<<4);

// doctrine database abstraction log verbosity
define('DBG_DOCTRINE_SELECT',   1<<5);
define('DBG_DOCTRINE_CHANGE',   1<<7);
define('DBG_DOCTRINE',          DBG_DOCTRINE_SELECT | DBG_DOCTRINE_CHANGE);
define('DBG_DOCTRINE_TRACE',    1<<6);
define('DBG_DOCTRINE_ERROR',    1<<8);

// overall database abstraction log verbosity
define('DBG_DB_SELECT',         DBG_ADODB_SELECT | DBG_DOCTRINE_SELECT);
define('DBG_DB_CHANGE',         DBG_ADODB_CHANGE | DBG_DOCTRINE_CHANGE);
define('DBG_DB',                DBG_DB_SELECT | DBG_DB_CHANGE);
define('DBG_DB_TRACE',          DBG_ADODB_TRACE | DBG_DOCTRINE_TRACE);
define('DBG_DB_ERROR',          DBG_ADODB_ERROR | DBG_DOCTRINE_ERROR);
define('DBG_DB_DUMP_ON_ERROR',  1<<10);

// log stream targets
define('DBG_LOG_FILE',          1<<9);
define('DBG_LOG_MEMORY',        1<<11);

// enbales logging of important messages
define('DBG_LOG',               1<<12);
// enables profiling
define('DBG_PROFILE',           1<<13);
define('DBG_PROFILE_ON',        DBG_PROFILE);
define('DBG_PROFILE_OFF',       1<<15);
define('DBG_PROFILE_FORCE',     1<<16);
// enables logging of debug information
define('DBG_DEBUG',             1<<14);

// Full debugging
define('DBG_ALL',
      DBG_PHP
    | DBG_DB | DBG_DB_TRACE | DBG_DB_ERROR | DBG_DB_DUMP_ON_ERROR
    | DBG_LOG_FILE  | DBG_LOG_MEMORY
    | DBG_LOG | DBG_PROFILE | DBG_DEBUG);

DBG::init();

/**
 * @copyright   Cloudrexx AG
 * @author      Thomas Wirz <thomas.wirz@cloudrexx.com>
 * @author      Michael Ritter <michael.ritter@cloudrexx.com>
 * @author      David Vogt <david.vogt@comvation.com>
 * @package     cloudrexx
 * @subpackage  lib_dbg
 */
class DBG
{
    protected static $fileskiplength = 0;
    protected static $log_php      = 0;
    protected static $mode         = 0;
    protected static $logPrefix = '';
    protected static $logHash= '';

    /**
     * Time in microseconds of first log event
     *
     * @var float
     */
    protected static float $startTime = 0;

    /**
     * List of registered log streams
     *
     * @var array
     */
    protected static $logStreams = [];

    /**
     * Name of the direct cli log stream
     *
     * @var string
     */
    protected const LOG_STREAM_NAME_CLI = 'cli';

    /**
     * Name of the memory log stream
     *
     * @var string
     */
    protected const LOG_STREAM_NAME_MEMORY = 'memory';

    /**
     * Name of the default log stream
     *
     * @var string
     */
    protected const LOG_STREAM_NAME_DEFAULT = 'dbg.log';

    /**
     * Name of the extended log stream
     *
     * @var string
     */
    protected const LOG_STREAM_NAME_EXTENDED = 'extended_dbg.log';

    /**
     * Bitwise combination of all DBG_* flags supported by {@see DBG_Log} to
     * set log verbosity
     *
     *
     * @var int
     */
    public const FLAGS_LOG_VERBOSITY = DBG_NONE
        | DBG_LOG
        | DBG_DEBUG
        | DBG_PHP
        | DBG_DB
        | DBG_DB_TRACE
        | DBG_DB_ERROR;

    /**
     * Bitwise combination of all DBG_PROFILE_* flags supported by {@see DBG_Log}
     * for controlling the profiling behaviour
     *
     * @var int
     */
    public const FLAGS_LOG_PROFILING = DBG_PROFILE_ON
        | DBG_PROFILE_OFF
        | DBG_PROFILE_FORCE;

    /**
     * Bitwise combination of all DBG_* flags supported by {@see DBG_LogStream}
     * to set log verbosity
     *
     * @todo This should be split up into two constants. One for log verbosity
     *     and one for profiling setup.
     * @var int
     */
    public const FLAGS_LOG_STREAM_VERBOSITY = DBG::FLAGS_LOG_VERBOSITY | DBG_PROFILE;

    /**
     * Tracks if the 'Request parsing completed'-log job has been
     * registered already. If so, then register_shutdown_function()
     * will not be called multiple times.
     *
     * @var bool
     */
    protected static $finishLineWriteRegistered = false;

    /**
     * List of classes defined by PHP
     */
    protected static $builtinClasses = [];

    /**
     * List of interfaces defined by PHP
     */
    protected static $builtinInterfaces = [];

    /**
     * Threshold in seconds that will cause {@see DBG::time()} to dump a stack
     * trace when the time delta between two log entries breaches.
     */
    const DBG_PROFILE_LOG_DELTA_THRESHOLD = 0.2;

    public function __construct()
    {
        throw new Exception('This is a static class! No need to create an object!');
    }

    /**
     * Activates debugging according to the bits given in $mode
     *
     * See the constants defined early in this file.
     * An empty $mode defaults to
     *  DBG_ALL & ~DBG_LOG_FILE
     * @param   integer     $mode       The optional debugging mode bits
     */
    public static function activate($mode = null): void {
        // generate a hash to be used for associating all logs to the same request
        if (empty(self::$logHash)) {
            self::$logHash = base_convert(
                preg_replace('/[^\d]/', '', microtime()),
                10,
                36
            );
        }

        if (!self::$fileskiplength) {
            self::$fileskiplength = strlen(dirname(dirname(dirname(dirname(__FILE__))))) + 1;
        }
        $oldMode = self::$mode;
        // note: as self::$mode is initially set to DBG_NONE, this will
        // automatically enable DBG_LOG on the first DBG::activate() call
        if (self::$mode === DBG_NONE) {
            // activate DBG_LOG by default
            self::$mode = DBG_LOG;
        }
        if ($mode === DBG_NONE) {
            self::$mode = DBG_NONE;
        } elseif ($mode === null) {
            self::$mode = self::$mode | (DBG_ALL & ~DBG_LOG_FILE);
        } else {
            self::$mode = self::$mode | $mode;
        }
        self::__internal__setup();
        if ($mode !== DBG_NONE) {
            if ($oldMode === DBG_NONE) {
                self::debug('DBG enabled ('.self::getActivatedFlagsAsString().')');
                self::stack(DBG_DEBUG);
            } else {
                self::debug('DBG mode changed ('.self::getActivatedFlagsAsString().')');
            }
        }
    }

    /**
     * Get a list of all available DBG flags that can be used on
     * {@see static::activate()} and related methods during runtime.
     * This does include all DBG flags except the following
     * - DBG_NONE
     * - DBG_ALL
     * - DBG_LOG_ (for changing the output device)
     * - already enabled flags
     *
     * @return  array   Array of all DBG flags having the form:
     *     ```
     *     [
     *         '<flag>' -> <bit>,
     *         ...
     *     ]
     */
    public static function getUsableFlags(): array {
        $flags = preg_grep(
            '/^DBG_(?!NONE|ALL|LOG_)/',
            array_keys(get_defined_constants())
        );
        $usableFlags = array_filter(
            $flags,
            function ($flag) {
                return !(static::getMode() & constant($flag));
            }
        );
        return array_combine(
            $usableFlags,
            $usableFlags
        );
    }

    /**
     * Retrieve a string representation of activated flags
     *
     * Identify all set DBG flags in `$mode` and returned them as a string,
     * joined by " | ". If `$mode` is not set (or set to `0`) then the DBG
     * flags are being identified as set by {@see DBG::getMode()}.
     *
     * @param int $mode Optional mode to evaluate. If not provided, the method
     *     uses the default mode returned by \DBG::getMode().
     * @return string A string containing the activated flags joined by " | ".
     */
    public static function getActivatedFlagsAsString(int $mode = 0): string {
        if (!$mode) {
            $mode = \DBG::getMode();
        }
        $constants = get_defined_constants(true);
        $userConstants = array_keys($constants['user']);
        $flags = array_filter(
            $userConstants,
            function($constant) use ($mode) {
                return    strpos($constant, 'DBG_') === 0
                       && constant($constant)
                       && ($mode & constant($constant)) === constant($constant);
            }
        );
        return join(' | ', $flags);
    }

    public static function activateIf($condition, $mode = null): void {
        if (
            (!is_callable($condition) && $condition) ||
            (is_callable($condition) && $condition())
        ) {
            static::activate($mode);
        }
    }

    public static function isIp($ip): bool {
        try {
            return \Cx\Core\Routing\Model\Entity\Request::getClientIp() == $ip;
        } catch (\Exception $e) {
            return false;
        }
    }

    public static function hasCookie($cookieName): bool {
        return isset($_COOKIE[$cookieName]);
    }

    public static function hasCookieValue($cookieName, $cookieValue): bool {
        if (!static::hasCookie($cookieName)) {
            return false;
        }
        return $_COOKIE[$cookieName] == $cookieValue;
    }

    /**
     * Will enable debugging as defined by the environment variable `CLX_DBG_FLAGS`.
     * If variable is not set or value of `CLX_DBG_FLAGS` translates into no valid mode,
     * then debugging will be disabled.
     * Set `CLX_DBG_FLAGS` the same way as you would in PHP - as a pipe delimited
     * string of the desired `DBG_*` mode constants. However the whole value must
     * be enclosed by quotes. I.e.: `CLX_DBG_FLAGS='DBG_PHP | DBG_LOG_FILE'`
     */
    public static function init(): void {
        static::$startTime = microtime(true);
        static::$builtinClasses = array_diff(
            get_declared_classes(),
            ['DBG']
        );
        static::$builtinInterfaces = get_declared_interfaces();
        $envPrefix = getenv('CLX_DBG_LOG_PREFIX', true);
        if ($envPrefix) {
            static::setLogPrefix($envPrefix);
        }
        $mode = 0;
        $envConfig = getenv('CLX_DBG_FLAGS', true);
        if ($envConfig) {
            $joinFlags = function($mode, $item) {
                $item = trim($item);
                if (!defined($item)) {
                    return $mode;
                }
                return $mode | constant($item);
            };
            $mode = array_reduce(
                explode('|', $envConfig),
                $joinFlags,
                $mode
            );
        }
        if ($mode) {
            static::activate($mode);
            return;
        }
        static::deactivate();
    }

    /**
     * Get a list of PHP classes that have been loaded until this point in time.
     *
     * @param   bool    $native Set to `true` to also include PHP's native classes.
     * @param   bool    $sort   Set to `true` to return the list in alphabetical order.
     * @return  array   List of loaded classes until now.
     */
    public static function getLoadedClasses(bool $native = false, bool $sort = false): array {
        $declaredClasses = get_declared_classes();
        if (!$native) {
            $declaredClasses = array_diff(
                $declaredClasses,
                static::$builtinClasses
            );
        }
        if ($sort) {
            asort($declaredClasses);
        }
        $declaredClasses = array_values($declaredClasses);
        return $declaredClasses;
    }

    /**
     * Get a list of PHP interfaces that have been loaded until this point in time.
     *
     * @param   bool    $native Set to `true` to also include PHP's native interfaces.
     * @param   bool    $sort   Set to `true` to return the list in alphabetical order.
     * @return  array   List of loaded interfaces until now.
     */
    public static function getLoadedInterfaces(bool $native = false, bool $sort = false): array {
        $declaredInterfaces = get_declared_interfaces();
        if (!$native) {
            $declaredInterfaces = array_diff(
                $declaredInterfaces,
                static::$builtinInterfaces
            );
        }
        if ($sort) {
            asort($declaredInterfaces);
        }
        $declaredInterfaces = array_values($declaredInterfaces);
        return $declaredInterfaces;
    }

    /**
     * Get a list of PHP files that have been loaded until this point in time.
     *
     * @return  array   List of loaded PHP files until now listed in alphabetical order.
     */
    public static function getLoadedFiles(): array {
        $declaredFiles = [];
        foreach (
            array_merge(
                static::getLoadedClasses(),
                static::getLoadedInterfaces(),
            ) as $class
        ) {
            $reflection = new \ReflectionClass($class);
            $file = $reflection->getFileName();
            if (!$file) {
                continue;
            }
            $declaredFiles[] = $file;
        }
        $declaredFiles = array_unique($declaredFiles);
        asort($declaredFiles);
        $declaredFiles = array_values($declaredFiles);
        return $declaredFiles;
    }

    /**
     * Deactivates debugging according to the bits given in $mode
     *
     * See the constants defined early in this file.
     * An empty $mode defaults to DBG_ALL, thus disabling debugging completely
     * @param   integer     $mode       The optional debugging mode bits
     */
    public static function deactivate($mode = null): void {
        if (empty($mode)) {
            self::$mode = DBG_NONE;
        } else {
            self::$mode = self::$mode  & ~$mode;
        }
        if ($mode === DBG_NONE) {
            self::log('DBG disabled ('.self::getActivatedFlagsAsString().')');
            self::stack();
        }
        self::__internal__setup();
    }

    /**
     * Set up debugging
     *
     * Called by both {@see activate()} and {@see deactivate()}
     */
    public static function __internal__setup(): void {
        // log to file dbg.log
        if (self::$mode & DBG_LOG_FILE) {
            self::enable_file();
        } else {
            self::disable_file();
        }
        // log to memory
        if (self::$mode & DBG_LOG_MEMORY) {
            self::enable_memory();
        } else {
            self::disable_memory();
        }
        if (
            self::$mode
            && !(self::$mode & DBG_LOG_FILE)
            && !(self::$mode & DBG_LOG_MEMORY)
            && class_exists('Cx\Core\Core\Controller\Cx')
            && \Cx\Core\Core\Controller\Cx::isCliCall()
        ) {
            self::enableCliOutput();
        }
        // log php warnings/erros/notices...
        if (self::$mode & DBG_PHP) {
            self::enable_error_reporting();
        } else {
            self::disable_error_reporting();
        }
    }

    /**
     * Returns the current debugging mode bits
     * @return  integer         The debugging mode bits
     */
    public static function getMode(): int {
        return self::$mode;
    }

    /**
     * Register a stream to send logs to
     *
     * @param DBG_LogStream $stream Instance to register as a log stream
     * @param string $name Name to refer to $stream.
     * @return bool True if registering was successful. If $name is already in
     *     use, then `false` is returned.
     */
    public static function registerLogStream(
        DBG_LogStream $stream,
        string $name
    ): bool {
        if (isset(static::$logStreams[$name])) {
            return false;
        }
        static::$logStreams[$name] = $stream;
        return true;
    }

    /**
     * Deregister a log stream
     *
     * After successful deregistration, no further logs are sent to the stream
     * identified by $name.
     *
     * @param string $name Name of the stream to deregister.
     * @return bool True if deregistering was successful. If $name is not in
     *     use,then `false` is returned.
     */
    public static function deregisterLogStream(
        string $name
    ): bool {
        if (!isset(static::$logStreams[$name])) {
            return false;
        }
        unset(static::$logStreams[$name]);
        return true;
    }

    /**
     * @return string Path to storage location for log files
     */
    protected static function getLogStoragePath(): string {
        // note: this is used ahead of Cx initialization,
        // therefore we can't use Cx::getWebsiteTempPath()
        return dirname(__FILE__, 4) . '/tmp/log/';
    }

    /**
     * Enables logging to a file
     *
     * Disables logging to memory in turn.
     */
    protected static function enable_file(): void {
        if (isset(static::$logStreams[static::LOG_STREAM_NAME_DEFAULT])) {
            static::$logStreams[static::LOG_STREAM_NAME_DEFAULT]->setMode(
                static::$mode
            );
            return;
        }
        $fh = fopen(
            static::getLogStoragePath() . static::LOG_STREAM_NAME_DEFAULT,
            'a'
        );
        if ($fh) {
            static::registerLogStream(
                new DBG_FileLogStream(
                    static::$mode,
                    static::$startTime,
                    $fh
                ),
                static::LOG_STREAM_NAME_DEFAULT
            );
            return;
        }
        http_response_code(502);
        die('Unable to initialize logging!');
    }

    /**
     * Disables logging to a file
     */
    protected static function disable_file(): void {
        static::deregisterLogStream(
            static::LOG_STREAM_NAME_DEFAULT
        );
    }

    /**
     * Enables logging to memory
     */
    protected static function enable_memory(): void {
        $stream = new DBG_LogStream(
            // do not profile logs in memory
            static::FLAGS_LOG_VERBOSITY,
            static::$startTime
        );
        static::registerLogStream(
            $stream,
            static::LOG_STREAM_NAME_MEMORY
        );
    }

    /**
     * Disables logging to memory
     */
    protected static function disable_memory(): void {
        static::deregisterLogStream(
            static::LOG_STREAM_NAME_MEMORY
        );
    }

    /**
     * Register a log stream to output to stdout
     */
    protected static function enableCliOutput(): void {
        static::registerLogStream(
            new DBG_CliLogStream(
                static::$mode,
                static::$startTime
            ),
            static::LOG_STREAM_NAME_CLI
        );
    }

    /**
     * Wrapper for {@see static::log_time()} by setting $logIsAfterCall to false.
     *
     * @param   string  $comment    Optional message to append to log entry.
     */
    public static function time(string $comment = ''): void {
        static::_log(
            'LOG: ' . $comment,
            DBG_LOG | DBG_PROFILE_FORCE
        );
    }

    protected static function enable_error_reporting(): void {
        self::$log_php = E_ALL;
        error_reporting(self::$log_php);
        set_error_handler('DBG::phpErrorHandler');
    }

    protected static function disable_error_reporting(): void {
        self::$log_php = 0;
        error_reporting(0);
        restore_error_handler();
    }

    public static function cleanfile($f): string {
        return substr($f, self::$fileskiplength);
    }

    /**
     * Log the current script file position or a specific step from the stack-
     * trace by setting `$level` to the desired level.
     *
     * @param   int $level Set level of the stack-trace to log.
     * @param int $modeConstraint Set to any of the available DBG basic flags to
     *     constrain the execution of the method to a specific DBG mode.
     */
    public static function trace($level=0, $modeConstraint = DBG_LOG): void {
        $callers = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, $level + 1);
        $f = self::cleanfile($callers[$level]['file']);
        $l = $callers[$level]['line'];
        self::_log("TRACE:  $f : $l", $modeConstraint | DBG_PROFILE_OFF);
    }

    /**
     * Log the script file position along with the caller to the current method
     *
     * @param int $modeConstraint Set to any of the available DBG basic flags to
     *     constrain the execution of the method to a specific DBG mode.
     */
    public static function calltrace($modeConstraint = DBG_LOG): void {
        $level = 1;
        $callers = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 2);
        $c = isset($callers[$level]['class']) ? $callers[$level]['class'] : null;
        $f = $callers[$level]['function'];
        self::trace($level, $modeConstraint);
        $sf = self::cleanfile($callers[$level]['file']);
        $sl = $callers[$level]['line'];
        self::_log("        ".(empty($c) ? $f : "$c::$f")." FROM $sf : $sl", $modeConstraint);
    }

    /**
     * Dump content of $val into the log.
     *
     * @param mixed $val
     * @param int $modeConstraint Set to any of the available DBG basic flags to
     *     constrain the execution of the method to a specific DBG mode.
     */
    public static function dump($val, $modeConstraint = DBG_LOG): void {
        self::_escapeDoctrineDump($val);

        if ($val === null) {
            $out = 'NULL';
        } else {
            $out = print_r($val, true);
            $out = str_replace("\n", "\n" . str_repeat(' ', 47), $out);
        }
        self::_log('DUMP: ' . $out, $modeConstraint);
    }

    protected static function _escapeDoctrineDump(&$val): void {
        // TODO: implement own dump-method that is able to handle recursive references
        if (is_object($val)) {
            // prevent dumping of sensitive arguments on production environment
            if (
                $val instanceof SensitiveParameterValue
                && getenv('CLX_DEV_MODE') !== '1'
            ) {
                $val = '!! Instance of SensitiveParameterValue -> not dumping in production mode !!';
            } else {
                $val = \Doctrine\Common\Util\Debug::export($val, 2);
            }
        } else if (is_array($val)) {
            foreach ($val as &$entry) {
                self::_escapeDoctrineDump($entry);
            }
        }
    }

    /**
     * Dump a stack trace into the log.
     *
     * @param int $modeConstraint Set to any of the available DBG basic flags to
     *     constrain the execution of the method to a specific DBG mode.
     */
    public static function stack($modeConstraint = DBG_LOG): void {
        $callers = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);

        // remove call to this method (DBG::stack())
        array_shift($callers);

        self::_log("TRACE:  === STACKTRACE BEGIN ===", $modeConstraint | DBG_PROFILE_OFF);
        $err = error_reporting(E_ALL ^ E_NOTICE);
        foreach ($callers as $c) {
            $file  = (isset($c['file']) ? self::cleanfile($c['file']) : 'n/a');
            $line  = (isset ($c['line']) ? $c['line'] : 'n/a');
            $class = isset($c['class']) ? $c['class'] : null;
            $func  = $c['function'];
            self::_log("        $file : $line (".(empty($class) ? $func : "$class::$func").")", $modeConstraint | DBG_PROFILE_OFF);
        }
        error_reporting($err);
        self::_log("        === STACKTRACE END ====", $modeConstraint | DBG_PROFILE_OFF);
    }

    /**
     * @deprecated Use {@see DBG::log()} or {@see DBG::debug()} instead
     * @param string $message The message to log.
     * @param int $modeConstraint Set to any of the available DBG basic flags to
     *     constrain the execution of the method to a specific DBG mode.
     */
    static function msg($message, $modeConstraint = DBG_LOG): void {
        self::_log('MSG: '.$message, $modeConstraint);
    }

    public static function debug($message): void {
        self::_log('DEBUG: '.$message, DBG_DEBUG);
    }

    /**
     * This method is only used if logging to a file
     * @param unknown_type $errno
     * @param unknown_type $errstr
     * @param unknown_type $errfile
     * @param unknown_type $errline
     */
    public static function phpErrorHandler($errno, $errstr, $errfile, $errline): void {
        if (self::$log_php & $errno) {
            $type = $errno;
            switch ($errno) {
                case E_ERROR:
                    $type = 'FATAL ERROR';
                    break;
                case E_WARNING:
                    $type = 'WARNING';
                    break;
                case E_PARSE:
                    $type = 'PARSE ERROR';
                    break;
                case E_NOTICE:
                    $type = 'NOTICE';
                    break;
                case E_CORE_ERROR:
                    $type = 'E_CORE_ERROR';
                    break;
                case E_CORE_WARNING:
                    $type = 'E_CORE_WARNING';
                    break;
                case E_COMPILE_ERROR:
                    $type = 'E_COMPILE_ERROR';
                    break;
                case E_COMPILE_WARNING:
                    $type = 'E_COMPILE_WARNING';
                    break;
                case E_USER_ERROR:
                    $type = 'E_USER_ERROR';
                    break;
                case E_USER_WARNING:
                    $type = 'E_USER_WARNING';
                    break;
                case E_USER_NOTICE:
                    $type = 'E_USER_NOTICE';
                    break;
                case E_STRICT:
                    $type = 'STRICT';
                    break;
                case E_RECOVERABLE_ERROR:
                    $type = 'E_RECOVERABLE_ERROR';
                    break;
                case E_DEPRECATED:
                    $type = 'E_DEPRECATED';
                    break;
                case E_USER_DEPRECATED:
                    $type = 'E_USER_DEPRECATED';
                    break;
            }
            self::_log("PHP: $type: $errstr in $errfile on line $errline", DBG_PHP);

            // Catch infinite loop produced by var_export()
            if ($errstr == 'var_export does not handle circular references') {
                self::log('Cancelled script execution to prevent memory overflow caused by var_export()');
                self::stack();
                exit;
            }
        }
    }

    /**
     * Writes the last line of a request to the log
     * @param \Cx\Core\Core\Controlller\Cx $cx Cx instance of the request
     * @param bool $cached Whether this request is answered from cache
     * @param string $outputModule (optional) Name of the output module
     */
    public static function writeFinishLine(
        \Cx\Core\Core\Controller\Cx $cx,
        bool $cached,
        string $outputModule = null
    ): void {
        if (static::$finishLineWriteRegistered) {
            return;
        }
        static::$finishLineWriteRegistered = true;
        $requestInfo = isset($_SERVER['REQUEST_URI']) ? str_replace('"', '<quote>', $_SERVER['REQUEST_URI']) : '';
        $requestIp = '';
        try {
            $requestIp = \Cx\Core\Routing\Model\Entity\Request::getClientIp();
            if ($requestIp === '127.0.0.1') {
                \DBG::dump($_SERVER);
            }
        } catch (\Exception $e) {}
        /*
        // TODO: add option to enable/disable ip anonymization (CLX-4544)
        // anonymize ip
        $requestIpParts = explode('.', $requestIp);
        end($requestIpParts);
        $requestIpParts[key($requestIpParts)] = '[...]';
        $requestIp = implode('.', $requestIpParts);
        */
        $requestHost = isset($_SERVER['REMOTE_HOST']) ? $_SERVER['REMOTE_HOST'] : $requestIp;
        $requestUserAgent = isset($_SERVER['HTTP_USER_AGENT']) ? str_replace('"', '<quote>', $_SERVER['HTTP_USER_AGENT']) : '';
        $cachedStr = $cached ? 'cached' : 'uncached';
        $userHash = '';
        $logPrefix = static::$logPrefix;
        $logHash = static::$logHash;
        $module = '';
        $cmd = '';
        $stats = $cx->getComponent(
            'Stats'
        );
        if ($stats) {
            $userHash = $stats->getCounterInstance()->getUniqueUserId();
        }

        // fetch parsed response code
        if ($cx->getResponse()) {
            $httpResponseCode = $cx->getResponse()->getCode();
        } else {
            // as fallback fetch response code from set headers
            $httpResponseCode = http_response_code();
        }
        // fetch parsed page
        if (
            $cx->getResponse() &&
            $cx->getResponse()->getPage() &&
            $cx->getResponse()->getPage()->getModule()
        ) {
            $module = $cx->getResponse()->getPage()->getModule();
            $cmd = $cx->getResponse()->getPage()->getCmd();
        }
        // fetch request method
        $requestMethod = '';
        if ($cx->getRequest()) {
            $requestMethod = $cx->getRequest()->getHttpRequestMethod();
        } elseif ($cached) {
            $requestMethod = strtolower($_SERVER['REQUEST_METHOD']);
        }

        // we can't determine if user is signed-in on cached or error-responses,
        // therefore $auth will be set to 'n/a' if response is served from
        // cache or from Error component
        $auth = 'noauth';
        if (
            $cached
            || (
                $module == 'Error'
                && $httpResponseCode == 404
            )
        ) {
            $auth = 'n/a';
        } else {
            try {
                if (\FWUser::getFWUserObject()->objUser->isLoggedIn()) {
                    $auth = 'auth';
                }
            } catch (\Exception $e) {}
        }

        if (\Cx\Core\Core\Controller\Cx::isCliCall()) {
            $requestedHost = '';
        } else {
            $requestedHost = $_SERVER['HTTP_X_FORWARDED_HOST'] ?? ($_SERVER['HTTP_HOST'] ?? '');
            $requestedHost = str_replace('"', '', $requestedHost);
        }

        register_shutdown_function(
            function() use (
                $cx,
                $requestInfo,
                $requestIp,
                $requestHost,
                $requestUserAgent,
                $cachedStr,
                $userHash,
                $outputModule,
                $httpResponseCode,
                $logPrefix,
                $logHash,
                $module,
                $cmd,
                $requestMethod,
                $auth,
                $requestedHost
            ) {
                $parsingTime = $cx->stopTimer();
                $format = '(Cx: %1$s) "Request parsing completed" "%12$s" "%13$s" "%14$s" %2$s "%3$s" "%4$s" "%5$s" "%6$s" "%7$s" "%8$s" "%9$s" "%10$s" "%11$s" "%15$s" "%16$s" "%17$s" "%18$s" "%19$s" "%20$s"';
                $log = sprintf(
                    $format,
                    $cx->getId(),
                    $parsingTime,
                    $cachedStr,
                    $requestInfo,
                    $requestIp,
                    $requestHost,
                    $requestUserAgent,
                    memory_get_peak_usage(true),
                    $userHash,
                    $outputModule,
                    $httpResponseCode,
                    $logPrefix,
                    $logHash,
                    $cx->getMode(),
                    $module,
                    $cmd,
                    $requestMethod,
                    getmypid(),
                    $auth,
                    $requestedHost
                );
                \DBG::log($log);
            }
        );
    }

    public static function log($text, $additional_args=null): void {
        self::_log('LOG: '.$text, DBG_LOG);
    }

    protected static function _log($text, $type = DBG_LOG): void {
        $log = new DBG_Log(
            $text,
            $type,
            static::$logHash,
            static::$logPrefix
        );
        foreach (static::$logStreams as $logStream) {
            $logStream->push($log);
        }
    }

    /**
     * @deprecated Use {@see DBG::getLogsFromStream()} instead.
     */
    public static function getMemoryLogs(): array {
        try {
            $streamLogs = static::getLogsFromStream(
                static::LOG_STREAM_NAME_MEMORY
            );
        } catch (\Exception $e) {
            return [];
        }
        $logs = [];
        foreach ($streamLogs as $log) {
            if (
                $log->getType() & DBG_DB
                || $log->getType() & DBG_DEBUG
            ) {
                continue;
            }
            $logs[] = $log->getMsg();
        }
        return $logs;
    }

    /**
     * @param string $name Name to refer to $stream.
     * @return array Logs from the stream identified by $name. Each log of the
     *     returned array is an instance of {@see DBG_Log}.
     */
    public static function getLogsFromStream(string $name): array {
        if (!isset(static::$logStreams[$name])) {
            throw new \Exception('Unknown stream ' . $name);
        }
        return static::$logStreams[$name]->getStack();
    }

    /**
     * @deprecated Use {@see DBG::appendLogsToStreams()} instead
     */
    public static function appendLogs($logs): void {
        foreach ($logs as $log) {
            self::_log($log, DBG_DEBUG);
        }
    }

    /**
     * Append logs to all registered log streams
     *
     * @param array $logs Logs to append to each registered stream. Each log
     *     must be an instance of {@see DBG_Log}.
     */
    public static function appendLogsToStreams($logs): void {
        foreach ($logs as $log) {
            foreach (static::$logStreams as $logStream) {
                $logStream->push($log);
            }
        }
    }

    /**
     * Enable extended log stream (`/tmp/log/extended_dbg.log`)
     *
     * @param int $modeConstraint Set to any of the available DBG basic flags to
     *     constrain the extended logging to the specific log verbosity.
     *     Defaults to {@see \DBG::FLAGS_LOG_VERBOSITY}.
     */
    protected static function enableExtendedLog(
        $modeConstraint = \DBG::FLAGS_LOG_VERBOSITY
    ): void {
        if (isset(static::$logStreams[static::LOG_STREAM_NAME_EXTENDED])) {
            static::$logStreams[static::LOG_STREAM_NAME_EXTENDED]->setMode(
                $modeConstraint
            );
            return;
        }
        $fh = fopen(
            static::getLogStoragePath() . static::LOG_STREAM_NAME_EXTENDED,
            'a'
        );
        if (!$fh) {
            trigger_error(
                sprintf(
                    'Unable to dump extended log to %s!',
                    static::LOG_STREAM_NAME_EXTENDED
                ),
                E_USER_ERROR
            );
            return;
        }
        \DBG::registerLogStream(
            new \DBG_FileLogStream(
                $modeConstraint,
                static::$startTime,
                $fh
            ),
            static::LOG_STREAM_NAME_EXTENDED
        );
    }

    /**
     * Dump collected logs (by memory when using `DBG_LOG_MEMORY`) to
     * extended log stream (`/tmp/log/extended_dbg.log`).
     *
     * @param int $modeConstraint Set to any of the available DBG basic flags to
     *     constrain the dumping to the specific log verbosity. Defaults to
     *     {@see \DBG::FLAGS_LOG_VERBOSITY}. If set to {@see \DBG::FLAGS_LOG_VERBOSITY},
     *     but `DBG_DB_DUMP_ON_ERROR` is not set on {@see \DBG} iteself, then
     *     `DBG_DB` won't be set.
     */
    public static function dumpExtendedLog(
        int $modeConstraint = \DBG::FLAGS_LOG_VERBOSITY
    ): void {
        if (
            $modeConstraint === \DBG::FLAGS_LOG_VERBOSITY
            && !(static::$mode & DBG_DB_DUMP_ON_ERROR)
        ) {
            $modeConstraint = $mode ^ DBG_DB;
        }
        static::enableExtendedLog($modeConstraint);
        if (!isset(static::$logStreams[static::LOG_STREAM_NAME_MEMORY])) {
            return;
        }
        $logs = static::getLogsFromStream(static::LOG_STREAM_NAME_MEMORY);
        foreach ($logs as $log) {
            static::$logStreams[static::LOG_STREAM_NAME_EXTENDED]->push($log);
        }
    }

    public static function logSQL($sql, $type): void {
        if (strpos($sql, 'password') !== false) {
            $sql = '*LOGIN (query suppressed)*';
        }
        $sql = preg_replace(['/[\n\r]/', '/\s{2,}/'], ['', ' '], $sql);
        if ($type & DBG_DB_ERROR) {
            self::_log('SQL: ERROR: '.$sql, $type);
            return;
        }
        if (preg_match('#^(UPDATE|DELETE|INSERT|ALTER|CREATE|DROP)#', $sql)) {
            $type = $type & DBG_DB_CHANGE;
        } else {
            $type = $type & DBG_DB_SELECT;
        }

        self::_log('SQL: '.$sql, $type);
        if (
            (static::getMode() & DBG_ADODB_TRACE) && ($type & DBG_ADODB)
            || (static::getMode() & DBG_DOCTRINE_TRACE) && ($type & DBG_DOCTRINE)
        ) {
            self::stack($type);
        }
    }

    /**
     * Set a text that will be put in front of all log messages
     *
     * @todo The log prefix should be a property of a log stream.
     * @param   string  $prefix The text that shall be put in front of log messages
     */
    public static function setLogPrefix($prefix = ''): void {
        self::$logPrefix = $prefix;
    }

    /**
     * Get currently set log prefix
     *
     * @todo The log prefix should be a property of a log stream.
     * @return string The set log prefix.
     */
    public static function getLogPrefix(): string {
        return static::$logPrefix;
    }

    /**
     * Reset the text that is being put in front of log messags
     */
    public static function resetLogPrefix(): void {
        self::setLogPrefix();
    }

    public static function getLogHash(): string {
        return self::$logHash;
    }
}

/**
 * @copyright   Cloudrexx AG
 * @author      Thomas Wirz <thomas.wirz@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  lib_dbg
 */
class DBG_Log {
    /**
     * The actual log message
     *
     * @var string
     */
    protected string $msg;

    /**
     * Timestamp of log with microseconds
     *
     * @var float
     */
    protected float $time;

    /**
     * Set DBG flags on log
     *
     * @var int
     */
    protected int $type;

    /**
     * Set DBG profiling flags on log
     *
     * @var int
     */
    protected int $profiling;

    /**
     * DBG hash to logically associate the log instance with a request
     *
     * @var string
     */
    protected string $logHash = '';

    /**
     * Prefix to logically associate the log instance with a Cx instance
     *
     * @var string
     */
    protected string $logPrefix = '';

    /**
     * Call stack of log
     *
     * @var array
     */
    protected array $stack = [];

    /**
     * Internal counter of instances having a stack trace set
     *
     * @var int
     */
    protected static int $stackCnt = 0;

    /**
     * Limit of instances that can have a stack trace
     *
     * This is used to prevent a memory overflow in case of a log-leak.
     *
     * @var int
     */
    protected const STACK_CNT_LIMIT = 1000;

    /**
     * @param string $msg The log message
     * @param int $type The log flags
     */
    public function __construct(
        string $msg,
        int $type,
        string $hash = '',
        string $prefix = ''
    ) {
        static::$stackCnt++;
        $this->msg = $msg;
        $this->type = $type & DBG::FLAGS_LOG_VERBOSITY;
        $this->profiling = $type & DBG::FLAGS_LOG_PROFILING;
        $this->time = microtime(true);
        $this->logHash = $hash;
        $this->logPrefix = $prefix;
        // do not collect any profiling data if profiling for this log instance
        // is disabled
        if ($this->profiling & DBG_PROFILE_OFF) {
            return;
        }
        if (static::$stackCnt > static::STACK_CNT_LIMIT) {
            return;
        }
        $this->stack = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 10);
    }

    /**
     * Get the log message
     *
     * @return string The log message
     */
    public function getMsg(): string {
        return $this->msg;
    }

    /**
     * Get the log verbosity flag mask
     *
     * @return int The set log verbosity flag mask
     */
    public function getType(): int {
        return $this->type;
    }

    /**
     * Get the profiling flag mask
     *
     * @return int The set profiling flag mask
     */
    public function getProfiling(): int {
        return $this->profiling;
    }

    /**
     * Get the time of the log
     *
     * @return float The log time
     */
    public function getTime(): float {
        return $this->time;
    }

    /**
     * Set the time of the log
     * @param float $time Set the time of the log
     */
    public function setTime(float $time): void {
        $this->time = $time;
    }

    /**
     * Get the associated request hash of the log
     *
     * @return string The request hash of log time
     */
    public function getLogHash(): string {
        return $this->logHash;
    }

    /**
     * Get the associated prefix of the log
     *
     * @return string The prefix of log time
     */
    public function getLogPrefix(): string {
        return $this->logPrefix;
    }

    /**
     * Get the stack trace of the log
     *
     * @return array The stack trace of the log. The stack is limited to
     *     10 stack frames.
     */
    public function getStack(): array {
        return $this->stack;
    }
}

/**
 * @copyright   Cloudrexx AG
 * @author      Thomas Wirz <thomas.wirz@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  lib_dbg
 */
class DBG_LogStream {
    /**
     * Debug mode of log stream
     *
     * Supports all DBG_* flags, except for DBG_LOG_FILE and DBG_LOG_MEMORY.
     *
     * @todo This should be split up into two properties. One for log verbosity
     *     and one for profiling setup.
     * @var int
     */
    protected $mode = 0;

    /**
     * Date time format used for logs
     *
     * @var string
     */
    protected const DATE_TIME_FORMAT = 'Y-m-d H:i:s';

    /**
     * Container for logs
     *
     * Each log is an array having the keys `log` and `type`.
     * `log` holds the actual log string.
     * `type` is one of the DBG_ flags.
     *
     * @var array
     */
    protected $stack = [];

    /**
     * Time in microseconds of first log event
     *
     * @var float
     */
    protected float $startTime = 0;

    /**
     * Delta in microseconds since last log event
     *
     * @var float
     */
    protected float $lastTime = 0;

    /**
     * @param int $mode DBG_* flags
     * @param float $startTime Time in microseconds of first log event of this
     *     request. If set to `0` (default), then the current time will be used.
     */
    public function __construct(
        int $mode,
        float $startTime = 0
    ) {
        if (!$startTime) {
            $startTime = microtime(true);
        }
        $this->startTime = $startTime;
        $this->lastTime = $startTime;
        $this->setMode($mode);
    }

    /**
     * @param int $mode DBG_* flags
     */
    public function setMode(int $mode) {
        $this->mode = $mode & DBG::FLAGS_LOG_STREAM_VERBOSITY;
    }

    /**
     * Add new log `$log` to the stack (using {@see static::attachLog()})
     * and flush it to the stream (using {@see static::flush()}
     *
     * Depending on {@see static::$mode} and {@see DBG_Log->getType()} a
     * profiling log will also be flushed to the stream by {@see static::flush()}.
     *
     * @param DBG_Log $log Log instance to add to the stack
     */
    public function push(DBG_Log $log): void {
        if ($this->mode === DBG_NONE) {
            return;
        }
        if (($this->mode & $log->getType()) !== $log->getType()) {
            return;
        }
        $this->attachLog($log);
        $this->flush();
    }

    /**
     * Add new log to the stack
     *
     * @param DBG_Log $log Log instance to add to the stack
     */
    protected function attachLog(
        DBG_Log $log
    ): void {
        $this->stack[] = $log;
    }

    /**
     * Flush the latest log (from {@see DBG_Log::$stack}) to the stream
     *
     * In case any of the following conditions are true, then this also
     * generates a log entry that contains profiling information by using
     * {@see static::profileLog()}:
     * - if `$log->getProfiling()` is set to DBG_PROFILE_FORCE
     * - if {@see static::$mode} is set to DBG_PROFILE and `$log->getType()`
     *   is not set to DBG_PROFILE_OFF
     */
    protected function flush(): void {
        $log = end($this->stack);
        if ($log->getProfiling() & DBG_PROFILE_FORCE) {
            $this->profileLog($log, false);
            return;
        }
        $this->flushLog($log);
        if (
            !($log->getProfiling() & DBG_PROFILE_OFF)
            && $this->mode & DBG_PROFILE
        ) {
            $this->profileLog($log);
        }
    }

    /**
     * Flush log to the stream
     *
     * @param DBG_Log $log Log instance to flush
     */
    protected function flushLog(DBG_Log $log): void {}

    /**
     * This flushes a log entry (using {@see DBG_Log::flushLog()} that contains
     * the time of the log entry, as well as the time that has been passed since:
     * - the last log entry was generated
     * - and request initialization
     * Further, the generated log entry will contain the caller of this call.
     * Callers from filesystem ({@see \Cx\Lib\FileSystem} and database layers
     * (AdoDB and Doctrine) will not be logged, but instead their callers.
     * If the delta to the last generated log entry is greater than
     * {@see \DBG::DBG_PROFILE_LOG_DELTA_THRESHOLD} and mode `DBG_DEBUG`
     * is enabled, then a stacktrace {@see DBG::stack()} is dumped.
     *
     * @param DBG_Log $log Log instance to profile.
     * @param boolean $logIsAfterCall Set to `true` if this call is called after
     *     a reference call. If set, then this will generate a log entry stating
     *     `TIME: ... AFTER`. If set to `false`, the log entry will state
     *     `TIME: ... AT`.
     */
    protected function profileLog(
        DBG_Log $log,
        bool $logIsAfterCall = true
    ): void {
        $diff_last  = round($log->getTime() - $this->lastTime, 6);
        $diff_start = round($log->getTime() - $this->startTime, 6);
        $this->lastTime = $log->getTime();
        $callers = $log->getStack();
        while ($caller = array_shift($callers)) {
            if (!isset($caller['file'])) {
                break;
            }
            if (
                $caller['file'] != __FILE__
                && !preg_match('/doctrine/i', $caller['file'])
                && !preg_match('/adodb/i', $caller['file'])
                && !preg_match('#/FileSystem/#', $caller['file'])
            ) {
                break;
            }
        }
        $source = 'anonymous';
        if (isset($caller['file'])) {
            $source = \DBG::cleanfile($caller['file']) . ':' . $caller['line'] . ' ->';
        }
        $time = $this->getDate($log->getTime(), 'H:i:s.u');
        $target = '';
        if (
            isset($caller['class'])
            && isset($caller['type'])
        ) {
            $target = $caller['class'] . $caller['type'];
        }
        if (isset($caller['function'])) {
            $target .= $caller['function'] . '()';
        }
        $logPrefix = '';
        if (preg_match('/^([^:]+:\s?)/', $log->getMsg(), $match)) {
            $logPrefix = $match[1];
        }
        $comment = '';
        if ($logIsAfterCall) {
            $callRelation = 'AFTER';
        } else {
            $callRelation = 'AT';
            $comment = $log->getMsg();
        }
        $msg = sprintf(
            '%sTIME: %s (diff: %f, startdiff: %f) %s %s %s: %s',
            $logPrefix,
            $time,
            $diff_last,
            $diff_start,
            $callRelation,
            $source,
            $target,
            $comment
        );

        $profileLog = new DBG_Log(
            $msg,
            $log->getType(),
            $log->getLogHash(),
            $log->getLogPrefix()
        );
        $profileLog->setTime($log->getTime());
        $this->flushLog($profileLog);
        if ($diff_last < \DBG::DBG_PROFILE_LOG_DELTA_THRESHOLD) {
            return;
        }
        $thresholdLog = new DBG_Log(
            sprintf(
                '%sTIME: Delta between previous two logs is larger than %f!',
                $logPrefix,
                \DBG::DBG_PROFILE_LOG_DELTA_THRESHOLD
            ),
            $log->getType(),
            $log->getLogHash(),
            $log->getLogPrefix()
        );
        $thresholdLog->setTime($log->getTime());
        $this->flushLog($thresholdLog);
    }

    /**
     * Get all logs
     *
     * @return array List of logs. See {@DBG_LogStream::$stack} for structure of
     *     logs.
     */
    public function getStack(): array {
        return $this->stack;
    }

    /**
     * Convert a given timestamp (with microseconds) to a formatted UTC date string
     *
     * This method takes a floating-point timestamp, generated using
     * `microtime(true)` and returns the corresponding date-time string in UTC timezone formatted according to $format.
     *
     * @param float $time The timestamp to convert, as generated by
     *     `microtime(true)`
     * @param string $format The date-time format to use
     * @return string The formatted date-time string in UTC timezone
     */
    protected function getDate(float $time, string $format): string {
        return (
            DateTime::createFromFormat(
                'U.u',
                sprintf('%.6F', $time),
                new DateTimeZone('UTC')
            )
        )->format(
            $format
        );
    }
}

/**
 * @copyright   Cloudrexx AG
 * @author      Thomas Wirz <thomas.wirz@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  lib_dbg
 */
class DBG_FileLogStream extends DBG_LogStream {
    /**
     * File system handle to push logs to
     *
     * @var resource
     */
    protected $dbg_fh = null;

    /**
     * @param int $mode DBG_* flags
     * @param float $startTime Time in microseconds of first log event of this
     *     request
     * @param resource $fh File system handle to push logs to
     */
    public function __construct(
        int $mode,
        float $startTime,
        $fh
    ) {
        parent::__construct(
            $mode,
            $startTime
        );
        $this->dbg_fh = $fh;
    }

    /**
     * @inheritDoc
     */
    protected function flushLog(DBG_Log $log): void {
        $time = $this->getDate(
            $log->getTime(),
            static::DATE_TIME_FORMAT
        );
        $prefix = '';
        if ($log->getLogPrefix() !== '') {
            $prefix .= $log->getLogPrefix() . ' - ';
        }
        $prefix .= $log->getLogHash();
        fputs(
            $this->dbg_fh,
            $time . ' "(' . $prefix . ')" ' . $log->getMsg() . "\n"
        );
    }

    /**
     * @throws \Exception Always, as this method is not supported.
     */
    public function getStack(): array {
        throw new \Exception('This log stream does not support ' . __METHOD__);
    }

    /**
     * Release file system handle
     */
    public function __destruct() {
        fclose($this->dbg_fh);
    }
}

/**
 * @copyright   Cloudrexx AG
 * @author      Thomas Wirz <thomas.wirz@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  lib_dbg
 */
class DBG_CliLogStream extends DBG_LogStream {
    /**
     * @inheritDoc
     */
    protected function flushLog(DBG_Log $log): void {
        echo $log->getMsg() . PHP_EOL;
        // force log message output
        if (ob_get_level()) {
            ob_flush();
        }
    }

    /**
     * @throws \Exception Always, as this method is not supported.
     */
    public function getStack(): array {
        throw new \Exception('This log stream does not support ' . __METHOD__);
    }
}
