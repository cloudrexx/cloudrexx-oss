<?php

/**
 * Cloudrexx
 *
 * @link      https://www.cloudrexx.com
 * @copyright Cloudrexx AG
 *
 * This file is part of Cloudrexx.
 *
 * Cloudrexx is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3 of the License.
 *
 * Cloudrexx is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

namespace Cx\Lib\DBG;

/**
 * @copyright   Cloudrexx AG
 * @author      Cloudrexx Development Team <info@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  lib_dbg
 */
class DoctrineSQLLogger implements \Doctrine\DBAL\Logging\SQLLogger
{
    /**
     * {@inheritdoc}
     */
    public function startQuery($sql, array $params = null, array $types = null): void {
        if (   !(\DBG::getMode() & DBG_DOCTRINE)
            && !(\DBG::getMode() & DBG_DOCTRINE_CHANGE)
            && !(\DBG::getMode() & DBG_DOCTRINE_ERROR)
        ) {
            return;
        }

        // prepare SQL statement
        if ($params) {
            $sql = str_replace('?', "'%s'", $sql);
            foreach ($params as &$param) {
                // serialize arrays
                if (is_array($param)) {
                    $param = serialize($param);
                } elseif (is_object($param)) {
                    // serialize objects
                    switch (get_class($param)) {
                        case 'DateTime':
                            // output DateTime object as date literal
                            $param = $param->format(ASCMS_DATE_FORMAT_DATETIME);
                            break;
                        default:
                            break;

                    }
                }
            }
            $sql = vsprintf($sql, $params);
        }

        // note: we don't need to handle errors, as doctrine does throw an
        // exception (which is then handled by Cx) if a query fails
        \DBG::logSQL($sql, DBG_DOCTRINE);
    }

    /**
     * {@inheritdoc}
     */
    public function stopQuery(): void {}
}
