# Contributing

First of all: thanks in advance for contributing!

## Bug reports
If you want to submit a bug report please do so using https://cloudrexx.atlassian.net/servicedesk/customer/portal/5/group/9/create/35

## Pull request
If you want to create a pull request, please make sure your code follows our coding guidelines: https://dev.cloudrexx.com/Guidelines/

Pull Requests criteria:

- Pull requests merge to "main" branch
- Single features/bugfixes per pull request. DO NOT mix multiple things in one pull request!
- Don't add complete Cloudrexx components. If you want to add your own component, please contact us at support@cloudrexx.com.

More information on how you can create pull requests in Bitbucket can be found here:

- https://support.atlassian.com/bitbucket-cloud/docs/fork-a-repository/
- https://www.atlassian.com/git/tutorials/making-a-pull-request


Thanks again,

The Cloudrexx team
