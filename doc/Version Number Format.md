Version numbers are structured like:

``` php
<major release>.<minor release>.<service pack>[.<hotfix>]
```

A version number cannot contain more than 4 two-digit blocks (split in dots). (The amount of digits for each block must be defined, so the version number can be converted to a number. The maximum number of 4 blocks is the result of the maximum size of an integer.. For more digits in one block, the functions down below have to be edited. (on 64Bit systems it would be possible to make 9 digits in one block.).)

To compare the numbers they have to be converted to an integer. The following functions can be used. adjustVersions() must be used if version numbers should be compared:

``` php
function versionToInt($version) {
    $parts = explode('.', $version);
    $int = current($parts);
    unset($parts[key($parts)]);
    foreach ($parts as $part) {
        $int *= 100;
        $int += $part;
    }
    return $int;
}
```

``` php
function intToVersion($versionAsInt) {
    $version = '';
    while ($versionAsInt > 0) {
        $part = $versionAsInt % 100;
        if ($part != 0 || $version != '' || $versionAsInt < 99999) {
            $version = $part . '.' . $version;
        }
        $versionAsInt -= $part;
        $versionAsInt /= 100;
    }
    return substr($version, 0, -1);
}
```

``` php
function adjustVersions(&$versionAsInt1, &$versionAsInt2) {
    $length1 = strlen($versionAsInt1);
    $length2 = strlen($versionAsInt2);
    if ($length1 % 2 != 0) {
        $versionAsInt1 = '0' . $versionAsInt1;
        $length1++;
    }
    if ($length2 % 2 != 0) {
        $versionAsInt2 = '0' . $versionAsInt2;
        $length2++;
    }
    $finalLength = $length1 > $length2 ? $length1 : $length2;
    $versionAsInt1 = str_pad($versionAsInt1, $finalLength, '0');
    $versionAsInt2 = str_pad($versionAsInt2, $finalLength, '0');
}
```
