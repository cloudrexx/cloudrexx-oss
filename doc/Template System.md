Cloudrexx uses the [PEAR HTML Sigma Template](https://pear.php.net/manual/en/package.html.html-template-sigma.php) templating system.

Cloudrexx implements a wrapper to the *PEAR HTML Sigma Template* engine that enables Customizings. You are advised to use the Cloudrexx wrapper ([\Cx\Core\Html\Sigma](https://api.contrexx.com/latest/classes/Cx.Core.Html.Sigma.html)) to make full usage of the Customizings functionality. The wrapper provides the whole function-set as provided by the underlying *PEAR HTML Sigma Template* engine.

The documentation of *PEAR HTML Sigma Template* can be found at <https://pear.php.net/package/HTML_Template_Sigma/docs>.

## Template Elements

The template system supports the following elements:

- [Placeholder](#placeholder-format)
- [Block](#template-block-format)
- [Callback](#callback-format)

### Placeholder format

Cloudrexx uses a slightly differtent placeholder format than what [PEAR HTML Sigma Template](https://pear.php.net/manual/en/package.html.html-template-sigma.intro-syntax.php) uses. In general Cloudrexx uses the following format (notation as regexp):
`\[\[[0-9A-Z_]+\]\]`

A few examples follow:
`[[DATE_YEAR]]`
`[[PRINT_URL]]`
`[[CONTACT_ADDRESS]]`

For the naming conventions that should be applied to placeholders, please refer to the related article [Naming conventions](Naming conventions.md).

#### Note for developers and webdesigners

It is important to know that the above *placeholder format* is due to historical reasons and is only used in the **view** (GUI) of Cloudrexx. Internally (in the database and file system), Cloudrexx uses the placeholder format of [PEAR HTML Sigma Template](https://pear.php.net/manual/en/package.html.html-template-sigma.intro-syntax.php). Which is as follows:
`{[0-9A-Za-z_-]+}`

Therefore, when interacting directly with the database, please be aware of that the placeholders do not use the same format as the ones that are getting displayed in the *backend* and *Frontend-Editing* of Cloudrexx. Same applies to any data that is stored in the file system. Especially if you intent to build a new or modify an existing *webdesign-template* using your favorite text-editor, remember to use the appropriate format (`{[0-9A-Za-z_-]+}`) when inserting placeholders.

#### Interface text - language variables

As Cloudrexx is by design a multilingual system, each interface text is being implemented by a so called *"language placeholder"* or *"language variable"*. Those placeholders use a special naming convention that is described in the article [Text-Variable](Interface Text.md).

### Template Block format

Template blocks are used to enclose other elements (HTML-code or other [template elements](#template-elements)) to either hide or touch them once or multiple times by iterting over a list of data.
The notation is as follows:
``` html
    <!-- BEGIN <name> -->
    hier goes the content of the block
    <!-- END <name> -->
```
Where `<name>` is to be replaced by a unique name of the format `[a-z0-9_]+`.
See article [Naming Conventions (Template blocks)](Naming conventions.md).

### Callback format

Callback functions provide basic parsing functionality to the template system.
Their notation is as follows:
``` html
    func_<name>(<arg1>[,<arg2>,..])
```
Where `<name>` is the name of the callback function. Own callback functions can be implemented using [\Cx\Core\Html\Sigma::addCallbackPlaceholder()](https://dev.cloudrexx.com/phpdoc/classes/Cx-Core-Html-Sigma.html#method_addCallbackPlaceholder).
Existing callback functions are listed in the [placeholder documentation](https://wiki.cloudrexx.com/Knowledgebase:Platzhalterverzeichnis_Cloud#Sigma_Funktionen).

## Usage

Instead of instantiating a *PEAR HTML Sigma Template* object, you would simply instantiate a `\Cx\Core\Html\Sigma` object. You can then use all functions provided by *PEAR HTML Sigma Template* on the newly created instance of `\Cx\Core\Html\Sigma`.

``` php
// instantiate a new template object
$objTemplate = new \Cx\Core\Html\Sigma('path/to/template/file.html');

// parse a template variable
$objTemplate->setvariable('TXT_FOO', 'bar');

// output parsed template
echo $objTemplate->get();
```

### Front- and Backend Controller

The instantiation of `\Cx\Core\Html\Sigma` is done automatically by the *[Component Handler](https://api.contrexx.com/latest/classes/Cx.Core.Core.Model.Entity.SystemComponentBackendController.html)*, as well as loading of the requested content page including the associated application template (I.e: *View/Template/Default.html*). The initialized template object `$template` can then be used in the [`parsePage()`](Component Definition.md) method of the component controller.

``` php
// parse a template variable
$template->setvariable('TXT_FOO', 'bar');
```

## Template Files

Template files are raw `.html` files that contain the HTML code for the front- and backend view.

### Storage location

| Type     | Location                  | Notes                        |
|----------|---------------------------|------------------------------|
| Frontend | `View/Template/Frontend/` | A.k.a. Application Templates |
| Backend  | `View/Template/Backend/`  |                              |
| Generic  | `View/Template/Generic/`  |                              |

