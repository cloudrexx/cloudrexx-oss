The following files can be used to create a navigation:

-   navbar.html
-   subnavbar.html

If you want to implement more than two navigations you can do it with these:

-   navbar2.html
-   navbar3.html
-   subnavbar2.html
-   subnavbar3.html

Use the following placeholders to integrate them into the template:

-   [[NAVBAR_FILE]] - navbar.html
-   [[NAVBAR2_FILE]] - navbar2.html
-   [[NAVBAR3_FILE]] - navbar3.html
-   [[SUBNAVBAR_FILE]] - subnavbar.html
-   [[SUBNAVBAR2_FILE]] - subnavbar2.html
-   [[SUBNAVBAR3_FILE]] - subnavbar3.html

## Navigation vs. Subnavigation

A subnavigation depends on the currently active page. For example for the following tree

-   Home
-   Products
    -   Shop
        -   Special offers
        -   Cart
        -   Conditions
    -   Other
-   About
-   Sitemap

If you want a main navigation for the first two levels (as seen on [demo.cloudrexx.com](https://demo.cloudrexx.com/)) you would use a **navigation** file containing the code shown in Dropdown Navigation.

For a second navigation showing all other levels, but only of the current branch (as seen on [demo.cloudrexx.com](https://demo.cloudrexx.com/)) you would use a **subnavigation** file containing the following

``` html
    <!-- BEGIN navigation_dropdown -->
    <ul id="subnavigation">
        <!-- BEGIN level_1 -->
        <li class="level-1 [[STYLE]] [[CSS_NAME]]">
            <a href="[[URL]]" class="[[STYLE]] [[CSS_NAME]]" title="[[NAME]]" target="[[TARGET]]" >[[NAME]]</a>[[SUB_MENU]]
        </li>
        <!-- END level_1 -->
        <!-- BEGIN level_2 -->
        <li class="level-2 [[STYLE]] [[CSS_NAME]]">
            <a href="[[URL]]" class="[[STYLE]] [[CSS_NAME]]" title="[[NAME]]" target="[[TARGET]]" >[[NAME]]</a>[[SUB_MENU]]
        </li>
        <!-- END level_2 -->
        <!-- BEGIN level_3 -->
        <li class="level-3 [[STYLE]] [[CSS_NAME]]">
            <a href="[[URL]]" class="[[STYLE]] [[CSS_NAME]]" title="[[NAME]]" target="[[TARGET]]" >[[NAME]]</a>[[SUB_MENU]]
        </li><!-- END level_3 -->
        <!-- BEGIN level_4 -->
        <li class="level-4 [[STYLE]] [[CSS_NAME]]">
            <a href="[[URL]]" class="[[STYLE]] [[CSS_NAME]]" title="[[NAME]]" target="[[TARGET]]" >[[NAME]]</a>
        </li>
        <!-- END level_4 -->
    </ul>
    <!-- END navigation_dropdown  -->
```

Assuming, that you are looking at the "Shop" page, the **navigation** will show the following:

-   Home
-   Products
    -   Shop
-   About
-   Sitemap

while the **subnavigation** will show

-   Products
    -   Shop
        -   Special offers
        -   Cart
        -   Conditions
    -   Other

### Navigation mode juggling

Since Contrexx 3.0 Service Pack 4, one can change the mode of a navigation (i.e. treat a navigation like a subnavigation or a subnavigation like a navigation). This is done by using the placeholders [[LEVELS_FULL]] (means: treat like a navigation) and [[LEVELS_BRANCH]] (means: treat like a subnavigation).

The following shows how a subnavigation can be treated as a normal navigation:

``` html
<!-- BEGIN navigation_dropdown -->
[[LEVELS_FULL]]
 <ul id="subnavigation">
     <!-- BEGIN level_1 -->
     <li class="level-1 [[STYLE]] [[CSS_NAME]]">
         <a href="[[URL]]" class="[[STYLE]] [[CSS_NAME]]" title="[[NAME]]" target="[[TARGET]]" >[[NAME]]</a>[[SUB_MENU]]
     </li>
     <!-- END level_1 -->
     <!-- BEGIN level_2 -->
     <li class="level-2 [[STYLE]] [[CSS_NAME]]">
         <a href="[[URL]]" class="[[STYLE]] [[CSS_NAME]]" title="[[NAME]]" target="[[TARGET]]" >[[NAME]]</a>[[SUB_MENU]]`  
     </li>
...
```

This feature should be avoided and is thought for making updates easier only.

## Types

There are three types of navigations you can use in a navigation file (mentioned above).

### Navigation

-   Simple standard type
-   Not nested (children aren't in the same tag as the parent)
-   If the current page is invisible the children are still parsed.
-   There are only `<li>` tags even for the children.

For template syntax and examples see [placeholder directory](https://wiki.cloudrexx.com/Knowledgebase:Platzhalterverzeichnis_Cloud#navigation_navigation).

### Nested Navigation

-   Advanced version of the standard navigation above
-   If the current page is invisible the children are still parsed.
-   Children are parsed in a new `<ul>` tag.
-   You have an "option" placholder like [[levels_XY]] to define how many levels you like to parse. The "X" for the start level and the "Y" for the end level.
    -   [[levels_13]] - Levels 1-3
    -   [[levels_11]] - Only one level (level 1)
    -   [[levels_2+]] - All levels starting from level 2
    -   [[levels_13_full]] - All levels 1-3
    -   [[levels_13_branch]] - Only sub-pages in levels 1-3 of the current page

For template syntax and examples see [placeholder directory](https://wiki.cloudrexx.com/Knowledgebase:Platzhalterverzeichnis_Cloud#navigation_nested_navigation).

### Dropdown Navigation

-   Nested navigation (children are in the same tag as the parent)
-   The entire page structure will always be parsed (all levels).
-   If a page has children they are parsed with a new `<ul>` tag at the position you placed the [[SUB_MENU]] placeholder.

For template syntax and examples see [placeholder directory](https://wiki.cloudrexx.com/Knowledgebase:Platzhalterverzeichnis_Cloud#navigation_navigation_dropdown).

## Navtree / Breadcrumb

The following placeholders are normally used to create a breadcrumb:

-   [[NAVTREE]] - The parent pages
-   [[NAVTITLE]] - The current page

``` html
<div id="breadcrumb">
    You are here:&nbsp;&nbsp;&nbsp;<a href="{NODE_HOME}">Home</a> &gt; {NAVTREE}<div class="current">{NAVTITLE}</div>
</div>
```
