## Check for active (installed) module

``` php
if (contrexx_isModuleActive($moduleName)) {
echo "$moduleName is active/installed";
}
```