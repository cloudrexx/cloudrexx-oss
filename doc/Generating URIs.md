There are many cases where you need to generate a link for the output. The way of generating a link/URI depends on how the request was made and what purpose the link/URI is for.

## Contrexx 3

Please note, that the \Cx\Core\Routing\Url class is still under development. We will make some improvements to use this class more widely...

### The requested page

In Contrexx 3 you can get the URL object of the requested page as follows:

``` php
// returns the URL object of the resolved page
// Always use 'clone' for this, so the original Url object will not get edited
$myUrl = clone \Env::get('Resolver')->getUrl();
```

### Any page

The URL class provides methods to get the URL of any page. For example you can:

``` php
// URL for a page
$myUrl = \Cx\Core\Routing\Url::fromPage($pageObject);

// URL for a page id
$myUrl = \Cx\Core\Routing\Url::fromPageId($pageId);

// URL for a node
$myUrl = \Cx\Core\Routing\Url::fromNode($nodeObject);

// URL for a node id
$myUrl = \Cx\Core\Routing\Url::fromNodeId($nodeId);

// URL for a module
$myUrl = \Cx\Core\Routing\Url::fromModuleAndCmd($modulename, $command, $languageId);

// URL for document root
$myUrl = \Cx\Core\Routing\Url::fromDocumentRoot();
```

### Working with URLs

``` php
// you can set parameters. new ones are added, existing ones overwritten
// if you set a parameter to null, it will be removed
$myUrl->setParam($parameter_name, $parameter_value);

// The magic method __toString() returns the correct URL for use in the code (Form actions, links, etc.)
echo $myUrl;

// If you need the full URL including hostname you might use explicit toString:
echo $myUrl->toString();
```

### Use Case

Generating a link for the cart view, leading the customer back to the detailed view of that article (including selected product options)

``` php
// Get an URL
$myUrl = \Cx\Core\Routing\Url::fromModuleAndCmd('shop', 'detail');

// Add some parameters
$myUrl->setParam('referer', 'cart');
$myUrl->setParam('productId', $cartEntryId);

// Use it
$objTemplate->setVariable('SHOP_CART_LINK', $myUrl);
```

## Contrexx 2

### Tools

#### Escaping File Paths

The method **FWValidator::getEscapedSource()** can be used to prepare the path of a file for its use in the source attribute of a HTML-tag. The method will ensure that special characters like spaces or quotes will be escaped correctly.

``` php
// This will output the string:<img src="/images/content/foo%20bar%20%22%20%24.jpg" />
echo '<img src="'.FWValidator::getEscapedSource('/images/content/foo bar " $.jpg').'" />';
```

### Request: Frontend

#### Output: Frontend - direct (Browser)

``` php
/* some output examples:
 *
 * # useVirtualLanguagePath is set to off
 * /index.php
 *
 * # useVirtualLanguagePath is set to off and the cms is located in the subdirectory website
 * /website/index.php
 *
 * # useVirtualLanguagePath is set to on and the user requested the english content
 * /en/index.php
 *
 * # useVirtualLanguagePath is set to on and the user requested the english content
 * # in addition the cms is located in the subdirectory website
 * /website/en/index.php
 *
 */
$uri = CONTREXX_SCRIPT_PATH
```

#### Output: Frontend - indirect (RSS Feeds, E-Mails)

``` php
/* some output examples:
 *
 * # useVirtualLanguagePath is set to off
 * https://www.example.com/index.php
 *
 * # useVirtualLanguagePath is set to off and the cms is located in the subdirectory website
 * https://www.example.com/website/index.php
 *
 * # useVirtualLanguagePath is set to on and the user requested the english content
 * https://www.example.com/en/index.phpp
 *
 * # useVirtualLanguagePath is set to on and the user requested the english content
 * # in additino the cms is located in the subdirectory website
 * https://www.example.com/website/en/index.php
 *
 */
$uri = ASCMS_PROTOCOL.'://'
        .$_CONFIG['domainUrl']
        .($_SERVER['SERVER_PORT'] == 80 ? NULL : ':'.intval($_SERVER['SERVER_PORT']))
        .CONTREXX_SCRIPT_PATH
```

### Request: Backend

#### Output: Frontend - direct (Browser)

``` php
/* some output examples:
 *
 * # useVirtualLanguagePath is set to off
 * https://www.example.com/index.php
 *
 * # useVirtualLanguagePath is set to off and the cms is located in the subdirectory website
 * https://www.example.com/website/index.php
 *
 * # useVirtualLanguagePath is set to on and the user requested the english content
 * https://www.example.com/en/index.php
 *
 * # useVirtualLanguagePath is set to on and the user requested the english content
 * # in addition the cms is located in the subdirectory website
 * https://www.example.com/website/en/index.php
 *
 */
$uri = ASCMS_PATH_OFFSET
        .($_CONFIG['useVirtualLanguagePath'] == 'on' ? '/'.FWLanguage::getLanguageParameter($intLanguageId, 'lang') : NULL)
        .'/'.CONTREXX_DIRECTORY_INDEX
```

#### Output: Frontend - indirect (RSS Feeds, E-Mails)

``` php
/* some output examples:
 *
 * # useVirtualLanguagePath is set to off
 * https://www.example.com/index.php
 *
 * # useVirtualLanguagePath is set to off and the cms is located in the subdirectory website
 * https://www.example.com/website/index.php
 *
 * # useVirtualLanguagePath is set to on and the user requested the english content
 * https://www.example.com/en/index.php
 *
 * # useVirtualLanguagePath is set to on and the user requested the english content
 * # in addition the cms is located in the subdirectory website
 * https://www.example.com/website/en/index.php
 *
 */
$uri = ASCMS_PROTOCOL.'://'
        .$_CONFIG['domainUrl']
        .($_SERVER['SERVER_PORT'] == 80 ? NULL : ':'.intval($_SERVER['SERVER_PORT']))
        .ASCMS_PATH_OFFSET
        .($_CONFIG['useVirtualLanguagePath'] == 'on' ? '/'.FWLangugaeg::getLanguageParameter($intLanguageId, 'lang') : NULL)
        .'/'.CONTREXX_DIRECTORY_INDEX
```

