Namespaces are used to pack code. A namespace has effect to classes, interfaces, functions and constants. For the sake of convenience the following text will only contain the word "classes". The namespaces in Contrexx are built with the following concept:

## Scheme

Base structure of namespaces: `\Cx\Area[\Subarea]\Component[\Subcomponent[\...]]`

Names in namespaces are singular and written [CamelCase](https://en.wikipedia.org/wiki/Camelcase#Programming_and_coding). The complete code is located in 'Cx' namespace. These are the exceptions (will be loaded by LegacyClassLoader):

-   Compatibility/Interfaces to old codes (global namespace)
-   External libraries

Namespaces basically don't have an effect on the folder structure. Due to the Contrexx [ClassLoader](ClassLoader.md) it is necessary that the path from the third level is the same like the namespace.

### Areas

On the second level there are the namespaces:

-   Core
-   Core_Modules
-   Model
-   Lib
-   Modules

#### Core

The namespace \Cx\Core contains the classes which belong to the Core of Contrexx. These classes are located in the folder /core.

#### Core_Modules

Analog to the namespace \Cx\Core the namespace \Cx\Core_Modules contains the core modules of Contrexx.

#### Model

The namespace \Cx\Model contains classes which are used for the data abstraction, what means e.g. Doctrine's entities. There are also classes which are not located in /model (e.g. classes based on AdoDB)!

The model namespace is split into the subareas Entities, Repositories, Base, Events and Proxies. This namespace is temporarily and will be replaced by Contrexx 3.1.

#### Lib

The namespace \Cx\Lib contains classes from the Contrexx framework, files located in /lib/FRAMEWORK.

#### Modules

The namespace \Cx\Modules contains the classes from modules, files located in /modules (if they don't belong to the model namespace).

### Components

The third level should contain the name of the affecting software part (module's name or name of the library). This is not possible for the namespaces \Cx\Core and \Cx\Model. For these namespaces there should be used thematically convenient names (e.g. \Cx\Core\Routing or \Cx\Model\ContentManager).

## Examples

| File                                                                  | Namespace                           |
|-----------------------------------------------------------------------|-------------------------------------|
| /core/Routing/Url.class.php                                           | \Cx\Core\Routing                    |
| /lib/FRAMEWORK/Captcha.interface.php                                  | \Cx\Lib\Captcha                     |
| /model/entities/Cx/Model/ContentManager/Repository/NodeRepository.php | \Cx\Model\ContentManager\Repository |

/core/Routing/Url.class.php:

``` php
namespace Cx\Core\Routing;

[...]

class Url {
[...]
```

New \Cx\Core\Routing\Url object from another namespace:

``` php
$myUrl = new \Cx\Core\Routing\Url($url);
```

Call a function in the global namespace from another namespace:

``` php
\DBG::activate(DBG_PHP);
```
