This page describes how to create intros for components or features in Cloudrexx.

## Quick start

An intro consists of several steps. Each step has an element that will get highlighted and a text that is shown.

The simplest case to show an intro is:

``` php
public function postContentLoad() {
    $this->getComponent('View')->addIntroSteps(array(
        array(
            'element' => '#myElement',
            'intro' => 'Some text',
        )
    ));
}
```

## How it works

The example above shows an intro using the [Intro.js library](https://introjs.com/). Once the user has seen the intro a cookie is set accordingly so it does not get shown twice.

`addIntroSteps()` has a second parameter called `group`. This can be used to show different intros on the same page or the same intro on multiple pages. If no group is specified `<componentName>-<section>` is used as default.

## Integration into cx.ui

Using JavaScript `cx.ui.intro.show("<group>")` can be used to show an intro. If this is called the intro is shown regardless whether the user has already seen it or not.

## Known limitations

-   No more than 32 steps can be added per group.
-   Shown steps are saved in a Cookie rather than per user.

