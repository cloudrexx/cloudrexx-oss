Cloudrexx is multilingual. All interface text is localized to the user's selected locale. This must be taken into account when developing new extensions to Cloudrexx. Interface text must therefore not be placed hard-coded (in a specific locale) to the view layer. Instead of placing hard-coded text into the view layer, so called *text-variables* must be used.

## Text-Variable

A **text-variable** refers to a certain element of the global array **$_ARRAYLANG**.

### Scheme

The scheme of a text-variable is as follow:

    TXT_<COMPOMENT_NAME>_<SHORT_AND_CONCISE_DESCRIPTION_OF_TEXT>

Whereas `<COMPOMENT_NAME>` is the name, in uppercase, of the compoment (module) the text-variable is in use (i.e. NEWS, SHOP) and `<SHORT_AND_CONCISE_DESCRIPTION_OF_TEXT>` should be a short and concise description of the text itself. The text-variable is written in USU-Notation according to the [Naming conventions](Naming conventions.md) of Contrexx.

    $_ARRAYLANG['TXT_NEWS_ADD_MESSAGE'] = "Add new message";

### Organization

All *text-variables* of a certain compoment (module) must be placed in the component' language folder (*/lang*). I.e. for the News component, all *text-variables* (interface text) are located in the folder *core_modules/News/**lang***.

Furthermore, the *text-variables* of the *frontend* are separated from those of the *backend*. *Text-variables' of the *frontend* are to be put in the file **frontend.php** and those of the*backend'' into the file **backend.php**.

The interface text (files frontend.php / backend.php) of a certain language must then be put into a separate folder in the component' language folder, where the language's [ISO 639-1 code](https://en.wikipedia.org/wiki/ISO_639-1) is used as the folder's name.

I.e. the English interface text of the *News* component is located in *core_modules/News/lang/**en**/*.

The content of a component' language folder would in general look like the following:

``` php
lang
├── de
│   ├── backend.php
│   └── frontend.php
├── dk
│   ├── backend.php
│   └── frontend.php
├── en
│   ├── backend.php
│   └── frontend.php
├── fr
│   ├── backend.php
│   └── frontend.php
├── it
│   ├── backend.php
│   └── frontend.php
└── ru
    ├── backend.php
    └── frontend.php
```


### Fallback / Base

Whenever a specific localized version of a *text-variable* can't be loaded (due to it's missing definition), it's English localization is used as a fallback. Therefore, every ***text-variable* must at least be localized in English** (lang/en/frontend.php or lang/en/backend.php).

## Loading

### Automatically

The *Text-Variables* of the requested component (identified by *section* in frontend and *cmd* in backend) are automatically being loaded (by *\InitCMS*) and can be accessed through the global **$_ARRAYLANG** variable.

### Manually

If the *Text-Variables* of any other than the requested component are required, then those can manually be loaded. The prefered way to do so is to fetch the *Text-Variables* as follows:

``` php
// Fetch frontend text-variables of component Calendar
$langData = \Env::get('init')->getComponentSpecificLanguageData('Calendar');

// Fetch backend text-variables of component Calendar
$langData = \Env::get('init')->getComponentSpecificLanguageData('Calendar', false);
```

The above method will not pollute the *Text-Variables* into the global *$_ARRAYLANG* variable. However the loading of the *Text-Variables* is being cached, whereas the method below does not cache the loaded *Text-Variables* and does instead fetch the data over and over again. Therefore the above method should be favored over the method below.

The following method allows to inject the *Text-Variables* of a component into the global variable *$_ARRAYLANG*:

``` php
// Load text-variables of component Calendar.
// The text-variables will afterwards be available through the global $_ARRAYLANG variable
\Env::get('init')->loadLanguageData('Calendar');
```

## Usage

Let's assume we have the following template:

``` html
<h2>{TXT_NEWS_OVERVIEW}</h2>
```

This template contains the *text-variable* **TXT_NEWS_OVERVIEW**. This *text-variable* is subject to the component **News**, due to the component part (**NEWS**) of its name.

The selection of the correct localized version of a *text-variable* is done automatically by Cloudrexx. To output the correct interface text we would now do the following:

``` php
// Initialize template system
$templateRepo = $this->getComponentController()->getDirectory(
    true, true
) . '/View/Template/Backend';
$objTemplate = new \Cx\Core\Html\Sigma($templateRepo);

// Load view (template file)
$filename_of_template = 'module_news_overview.html';
$objTemplate->loadTemplatefile($filename_of_template);

// Set interface text
$objTemplate->setVariable('TXT_NEWS_OVERVIEW', $_ARRAYLANG['TXT_NEWS_OVERVIEW']);
```

Depending on the user's selected locale, this will now output the localized version of the *text-variable* *TXT_NEWS_OVERVIEW*.

