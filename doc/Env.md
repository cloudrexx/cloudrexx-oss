Env is a class to store/retrieve objects and data. In Contrexx, there are three concepts of how to retrieve data. All of them have their advantages and disadvatages:

## Global

In the code there are lots of places where you can see something like this:

``` php
global $objDatabase;
```

That's a very easy way to retrieve data, but we don't have any control what is used where and it's not object oriented at all. Do not use this concept if you write code!

## Dependency injection

In new code, all dependencies are "injected". This means, that every object gets all the things it needs as a parameter for the constructor or method. This might look something like this:

``` php
class SomeClass {
    private $objDatabase;
    [...]

    public function __construct($objDatabase) {
        $this->objDatabase = $objDatabase;
        [...]
    }
    [...]
}
```

This is ideal for unit testing, because we can see what is needed to run a test against a certain object or method. Furthermore, we don't pollute the global namespace. Use this, whenever you can!

## Env

In newer pieces of code, relying on old code you might run across code like this:

``` php
\Env::get('db');
```

\Env encapsulates all "globally" available stuff, so we won't run into any conflicts with third-party libraries using global. It's not that ideal for unit testing, but we don't need to rewrite all the old code to achieve this.

The following indexes are set:

| Index                    | Backend | Frontend | Legacy       | Description                               |
|--------------------------|---------|----------|--------------|-------------------------------------------|
| config                   | X       | X        | $_CONFIG     | Global configuration                      |
| ftpConfig                | X       | X        | $_FTPCONFIG  | FTP configuration                         |
| ClassLoader              | X       | X        | -            | \Cx\Core\ClassLoader\ClassLoader instance |
| em                       | X       | X        | -            | Doctrine entity manager                   |
| db                       | X       | X        | $objDatabase | AdoDB instance                            |
| cx                       | X       | X        | -            | Contrexx main class instance              |
| pageguard                | X       | X        | -            | \PageGuard instance                       |
| module2id                | X       | X        | -            | Module conversion table from API.php      |
| id2module                | X       | X        | -            | Module conversion table from API.php      |
| init                     | X       | X        | $objInit     | \InitCMS instance                         |
| Resolver                 | X       | X        | -            | \Cx\Core\Routing\Resolver instance        |
| virtualLanguageDirectory |         | X        | -            | 'de', 'en' etc.                           |

Use this, whenever rewriting to dependency injection is too much work.

