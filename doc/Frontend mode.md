The [System Mode](System Modes.md) `Frontend` is the default mode. It is used to process regular HTTP(S)-requests made to the website (from web browsers).

## Invocation

The _Frontend_ mode is automatically invocated on all HTTP(S)-requests made to Cloudrexx, except for those requests made to the endpoints `/cadmin` ([Backend](Backend mode.md)) and `/api` ([Command](Command mode.md)).

## Output

The generated output must be a HTML-valid webpage.

!!! note

    If you need to output data in another format (i.e.: JSON or a byte-stream) do instead implement an [Exposed Method](Exposed methods.md) and call it through the [api](Exposed methods.md#as-a-http-endpoint) endpoint.

## Rendering

Based on the requested [`Locale`](Localization.md#locale-entity) and _Channel_[^1], the matching `\Cx\Core\View\Model\Entity\Frontend` entity is loaded which defines the `\Cx\Core\View\Model\Entity\Theme` that will be used for output rendering.

[^1]: List of supported _channels_: `web` (default), `mobile`^deprecated^, `print`, `pdf` & `app`
