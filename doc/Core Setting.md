The Setting component provides a generic interface for handling configuration options. Configuration data can be stored in the database or the file system and can be manipulated through a GUI or the command line.

## Integration

### Initialization

The first step is to initialize a repository. A repository is a container of setting options of a specific component. The repository can either be stored in the database or in the file system using the appropriate storage engine. All options in a repository are organized in groups.

!!! note

    If the repository does not yet exists, it'll be created on the fly on storage.

The syntax to initialize a repository is as follows:

``` php
\Cx\Core\Setting\Controller\Setting::init('<component>', '<group>', '<engine>');
```

Where the arguments have the following meaning:

| Argument        | Description                                         | Optional |
|-----------------|-----------------------------------------------------|----------|
| **&lt;component&gt;** | The component to load the options from. I.e: 'Shop' | No       |
| **&lt;group&gt;**     | Load only the options of group *&lt;group&gt;*            | Yes      |
| **&lt;engine&gt;**    | Set engine to use            | Yes      |

!!! example  

    Initialize repository of component *TodoManager* with engine *FileSystem*:

    ``` php
    \Cx\Core\Setting\Controller\Setting::init('TodoManager', null, 'FileSystem');
    ```

This will store the setting data in the file **config/TodoManager.yml**.

### Add/define configuration options

A setting repository can hold an unlimited number of options.

The syntax to add a new configuration option is as follows:

``` php
\Cx\Core\Setting\Controller\Setting::add(
    '<option-name>',
    '<option-value>',
    <order-id>,
    '<option-type>',
    '<option-data>',
    '<group>'
);
```

Where the arguments have the following meaning:

| Argument           | Description                                                                                                                                                                                                                             | Optional |
|--------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------|
| **&lt;option-name&gt;**  | The name of the configuration option to add                                                                                                                                                                                             | No       |
| **&lt;option-value&gt;** | The initial default value of the configuration option                                                                                                                                                                                   | No       |
| **&lt;order-id&gt;**     | An integer used to put the option in order (in a GUI) in relation to the other defined configuration options. The higher the integer is, the later will the option be parsed (in relation to other options having a lower integer set). | Yes      |
| **&lt;option-type&gt;**  | Any of the available option types. Defaults to 'text'.                                                                                                                                                      | Yes      |
| **&lt;option-data&gt;**  | Additional option types specific data. I.e. for option type *Radio* the available radio options are defined through this argument.                                                                          | Yes      |
| **&lt;group&gt;**        | Assign the option to the specified group. If left empty, the option will be added to the last registered/initialized group.                                                                                                             | No       |

!!! example 

    !!! warning

        The repository must have been initialized before a new option can be added.

    Add a simple text option to group *config*:

    ``` php
    \Cx\Core\Setting\Controller\Setting::add(
        'simpleTextOption',
        'defaultText',
        1,
        \Cx\Core\Setting\Controller\Setting::TYPE_TEXT,
        null,
        'config'
    );
    ```

#### Add settings for a component
The easiest way to add settings for a component is to specify them in [Component.yml](Component.yml.md). The structure defined for this is as follows:

```yaml
<group>:
  -
    name: <option-name>
    value: <option-value>
    ord: <order-id>
    type: <option-type>
    values: <option-data>
    flags:
      - <flag>
      ...
```
The placeholders have the exact same meaning as specified in the table above. All indexes except for `flags` are mandatory. The following flags are known:

* `user`: Creates a user-specific setting.
* `preInit`: Set this if you need this setting to be available in [preInit hooks](Bootstrapping.md/#preInit).

The "section" is automatically set to the component's name.

By default the "Database" engine is used, unless one of the component's settings has the `preInit` flag set in which case the "FileSystem" engine is used for all settings for this component.

### Show configuration GUI

A simple configuration GUI can be generated from an initialized setting repository. If the setting repository contains more than one group, then only the options of a selected group can get parsed or the options of each group can be parsed into a separate tab.

The basic syntax is as follows:

``` php
\Cx\Core\Setting\Controller\Setting::show(
    <template-object>,
    '<url-of-current-section>',
    '<component>',
    '<tab-name>',
    '<text-variable-prefix>',
    <readonly>
);
```

Where the arguments have the following meaning:

| Argument                     | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   | Optional |
|------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------|
| **&lt;template-object&gt;**        | Instance of \Cx\Core\Html\Sigma into which the configuration GUI will be parsed into. The instance can either be empty (no template loaded) or have a custom template set to be used for parsing the configuration GUI. If the instance has no template set, then the default template for the configuration GUI will be used and parsed. After the GUI has been parsed it can be fetched from the template instance using the method *\Cx\Core\Html\Sigma::get()*.                                                                                                                                                                           | No       |
| **&lt;url-of-current-section&gt;** | The URI of the current backend section. I.e.: /cadmin/Config                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  | No       |
| **&lt;component&gt;**              | The component to parse the options from. If not set, then options of the currently initialized component are parsed.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          | Yes      |
| **&lt;tab-name&gt;**               | When set, it will generate a tab by the name *&lt;tab-name&gt;*. This is used if there are more than one group present in the setting repository.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   | Yes      |
| **&lt;text-variable-prefix&gt;**   | For each configuration option, a label and tooltip can be assigned through a Text-Variable. The taken Text-Variable for the label of the option is based on the capitalized form of the option's name and the prefix *&lt;text-variable-prefix&gt;*. The *&lt;text-variable-prefix&gt;* defaults to *TXT_*. I.e. for option *simpleTextOption* the Text-Variable for the label has the form *TXT_SIMPLETEXTOPTION*. Note: According to the guidelines the prefix must have the following scheme: *TXT_&lt;COMPONENT&gt;_*. More information can be found here: [Text-Variable](Interface Text.md). | Yes      |
| **&lt;readonly&gt;**               | Whether the configuration GUI shall be in read-only mode which does not allow any modification of the parsed options. Defaults to **false**                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   | Yes      |

!!! example  

    The following generates the configuration GUI for the options of group *general* of component *TodoManager*:

    ``` php
    $template = new \Cx\Core\Html();
    \Cx\Core\Setting\Controller\Setting::init('TodoManager', 'general');
    \Cx\Core\Setting\Controller\Setting::show(
        $template,
        '/cadmin/TodoManager/Setting',
        'TodoManager',
        'General',
        'TXT_TODOMANAGER_'
    );
    echo $template->get();
    ```

### Store changes from configuration GUI

Changes made to any options in the configuration GUI can be updated in the repository using the method *\Cx\Core\Setting\Controller\Setting::storeFromPost()*.

!!! example  

    Automatically store all options from POST-data (from auto-generated view):

    ``` php
    \Cx\Core\Setting\Controller\Setting::init('TodoManager', 'general');
    if (isset($_POST) && !empty($_POST['bsubmit'])) {
        \Cx\Core\Setting\Controller\Setting::storeFromPost();
    }
    ```

### Fetch data of options

The data of the configuration options can be accessed using the method *\Cx\Core\Setting\Controller\Setting::getValue()*.

!!! warning

    Before any data can be accessed, the setting repository must be initialized first.

!!! example  

    ``` php
    $mode = \Cx\Core\Setting\Controller\Setting::getValue('mode');
    ```

    Once a setting repository has been initialized, it can be accessed in the same cx-request without having the need to re-initialize it again. To ensure the option is fetched from the repository of choice, simple add the repository's component as second argument to *\Cx\Core\Setting\Controller\Setting::getValue()*:

    ``` php
    $mode = \Cx\Core\Setting\Controller\Setting::getValue('mode', 'Config');
    ```

## Global Configuration

The **Global Configuration** section (*Administration &gt; Global Configuration &gt; System*) does use this library to store and manage its configuration options. As a special extension to the `Setting` library, the configuration options of the *Global Configuration* are shadowed into the file `config/settings.php`. This is used to make the *Global Configuration options* available in a very early stage of the [bootstrapping process](Bootstrapping.md).

!!! note

    The `Setting` library is not available until the `postInit`-hook. To overcome this issue, the *Global Configuration options* are accessible through `$this->cx->getConfig()->getBaseConfig($key)` in earlier stages.

!!! warning

    However, from the `postInit`-hook onwards you must always use the `Setting` library to access the *Global Configuration options*. Accessing the *Global Configuration options* through `$this->cx->getConfig()` after the `postComponentLoad`-hook (which is the predecessor of `postInit`) is regarded as deprecated code. More information can be found here: [`postInit`](Bootstrapping.md).

## Engines

The interface provides the following storage engines that can be used as the target medium for storage.

<table>
<thead>
<tr class="header">
<th><p>Engine</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>Database</strong> (default)</p></td>
<td><p>Stores the configuration options in the database (<em>table contrexx_core_setting</em>)</p></td>
</tr>
<tr class="even">
<td><p><strong>FileSystem</strong></p></td>
<td><p>Stores the configuration options in the filesystem in plain YAML notation (in ''config/</p>
<section>
<p>.yml'')</p></td>
</tr>
<tr class="odd">
<td><p><strong>Yaml</strong> <sup>Deprecated</sup></p></td>
<td><p>Stores the configuration options as serialized objects in the filesystem in YAML notation (in ''config/</p>
<section>
<p>.yml'')</p></td>
</tr>
</tbody>
</table>

## Option Types

The library supports the following option types.

<table>
<thead>
<tr class="header">
<th><p>Option</p></th>
<th><p>Type (PHP)</p></th>
<th><p>Type (CLI)</p></th>
<th><p>Description</p></th>
<th><p>Additional specific data</p></th>
<th><p>Examples</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>Dropdown</strong></p></td>
<td><p>TYPE_DROPDOWN</p></td>
<td><p>dropdown</p></td>
<td><p>A HTML-select dropdown. Options are defined in attribute <em>values</em> either as a static comma-separated list or dynamic by referencing a callback function.</p></td>
<td><dl>
<dt>Static list:</dt>
&#10;</dl>
<p>Separate options by comma: <option>[,<option>]</p>
<p>To use custom option keys, do use the scheme <key>:<option>[,<key>:<option>]</p>
<dl>
<dt>Dynamic list:</dt>
&#10;</dl>
<p>Use the following scheme to specify a callback function to fetch the available options:</p>
<pre class="json"><code>{src:\Fully\Qualified\ClassName::Method()}</code></pre></td>
<td><pre class="php"><code>\Cx\Core\Setting\Controller\Setting::add(
    &#39;dropdownOption&#39;,
    &#39;0&#39;,
    1,
    \Cx\Core\Setting\Controller\Setting::TYPE_DROPDOWN,
    &#39;first-option,second-option,third-option&#39;,
    &#39;demo&#39;
);</code></pre>
<pre class="php"><code>\Cx\Core\Setting\Controller\Setting::add(
    &#39;dropdownOption&#39;,
    &#39;0&#39;,
    1,
    \Cx\Core\Setting\Controller\Setting::TYPE_DROPDOWN,
    &#39;option1:first-option,option2:second-option,option3:third-option&#39;,
    &#39;demo&#39;
);</code></pre>
<pre class="php"><code>\Cx\Core\Setting\Controller\Setting::add(
    &#39;dropdownOption&#39;,
    &#39;0&#39;,
    1,
    \Cx\Core\Setting\Controller\Setting::TYPE_DROPDOWN,
    &#39;{src:\\&#39; . __CLASS__ . &#39;::getOptions()}&#39;,
    &#39;demo&#39;
);</code></pre></td>
</tr>
<tr class="even">
<td><p><strong>Multiselect</strong></p></td>
<td><p>TYPE_DROPDOWN_MULTISELECT</p></td>
<td><p>dropdown_multiselect</p></td>
<td><p>A HTML-select element with multiple selection.</p></td>
<td><p>Options are defined analogous to type <em>Dropdown</em></p></td>
<td><pre class="php"><code>\Cx\Core\Setting\Controller\Setting::add(
    &#39;multiselectOption&#39;,
    &#39;0&#39;,
    1,
    \Cx\Core\Setting\Controller\Setting::TYPE_DROPDOWN_MULTISELECT,
    &#39;{src:\\&#39; . __CLASS__ . &#39;::getOptions()}&#39;,
    &#39;demo&#39;
);</code></pre></td>
</tr>
<tr class="odd">
<td><p><strong>User profile attribute dropdown</strong></p></td>
<td><p>TYPE_DROPDOWN_USER_CUSTOM_ATTRIBUTE</p></td>
<td><p>dropdown_user_custom_attribute</p></td>
<td><p>HTML-select dropdown of preconfigured profile attributes of users</p></td>
<td><p>-</p></td>
<td><pre class="php"><code>\Cx\Core\Setting\Controller\Setting::add(
    &#39;userAttributeOption&#39;,
    &#39;0&#39;,
    1,
    \Cx\Core\Setting\Controller\Setting::TYPE_DROPDOWN_USER_CUSTOM_ATTRIBUTE,
    null,
    &#39;demo&#39;
);</code></pre></td>
</tr>
<tr class="even">
<td><p><strong>User group dropdown</strong></p></td>
<td><p>TYPE_DROPDOWN_USERGROUP</p></td>
<td><p>dropdown_usergroup</p></td>
<td><p>HTML-select dropdown of existing user groups</p></td>
<td><p>-</p></td>
<td><pre class="php"><code>\Cx\Core\Setting\Controller\Setting::add(
    &#39;userGroupOption&#39;,
    &#39;0&#39;,
    1,
    \Cx\Core\Setting\Controller\Setting::TYPE_DROPDOWN_USERGROUP,
    null,
    &#39;demo&#39;
);</code></pre></td>
</tr>
<tr class="odd">
<td><p><strong>WYSIWYG</strong></p></td>
<td><p>TYPE_WYSIWYG</p></td>
<td><p>wysiwyg</p></td>
<td><p>WYSIWYG-editor for HTML content</p></td>
<td><p>-</p></td>
<td><pre class="php"><code>\Cx\Core\Setting\Controller\Setting::add(
    &#39;wysiwygOption&#39;,
    &#39;&#39;,
    1,
    \Cx\Core\Setting\Controller\Setting::TYPE_WYSIWYG,
    null,
    &#39;demo&#39;
);</code></pre></td>
</tr>
<tr class="even">
<td><p><strong>File upload</strong></p></td>
<td><p>TYPE_FILEUPLOAD</p></td>
<td><p>fileupload</p></td>
<td><p><a href="Development_Uploader" title="wikilink">Uploader</a>-element</p></td>
<td><p>-</p></td>
<td><pre class="php"><code>\Cx\Core\Setting\Controller\Setting::add(
    &#39;wysiwygOption&#39;,
    &#39;&#39;,
    1,
    \Cx\Core\Setting\Controller\Setting::TYPE_FILEUPLOAD,
    null,
    &#39;demo&#39;
);</code></pre></td>
</tr>
<tr class="odd">
<td><p><strong>Text</strong></p></td>
<td><p>TYPE_TEXT</p></td>
<td><p>text</p></td>
<td><p>A regular HTML-input element</p></td>
<td><p>Set special input parsing behavior through JSON-encoded option <code>type</code> in attribute <em>values</em>. The following types are supported:<ul>
<li><code>filesize</code>: causes the value to be converted into a literal representation of a file size (i.e.: 1 KB instead of 1024): <code>{"type":"filesize"}</code></li>
<li><code>senderAddress</code>: add input validation for email address sender validation: <code>{"type":"senderAddress"}</code></li>
</ul></p></td>
<td><pre class="php"><code>\Cx\Core\Setting\Controller\Setting::add(
    &#39;wysiwygOption&#39;,
    &#39;&#39;,
    1,
    \Cx\Core\Setting\Controller\Setting::TYPE_TEXT,
    null,
    &#39;demo&#39;
);</code></pre>
<pre class="php"><code>\Cx\Core\Setting\Controller\Setting::add(
    &#39;wysiwygOption&#39;,
    &#39;&#39;,
    1,
    \Cx\Core\Setting\Controller\Setting::TYPE_TEXT,
    &#39;{&quot;type&quot;:&quot;filesize&quot;}&#39;,
    &#39;demo&#39;
);</code></pre></td>
</tr>
<tr class="even">
<td><p><strong>Multiline text</strong></p></td>
<td><p>TYPE_TEXTAREA</p></td>
<td><p>textarea</p></td>
<td><p>HTML-textarea</p></td>
<td><p>-</p></td>
<td><pre class="php"><code>\Cx\Core\Setting\Controller\Setting::add(
    &#39;wysiwygOption&#39;,
    &#39;&#39;,
    1,
    \Cx\Core\Setting\Controller\Setting::TYPE_TEXTAREA,
    null,
    &#39;demo&#39;
);</code></pre></td>
</tr>
<tr class="odd">
<td><p><strong>E-mail</strong></p></td>
<td><p>TYPE_EMAIL</p></td>
<td><p>email</p></td>
<td><p>HTML-input type for email</p></td>
<td><p>-</p></td>
<td><pre class="php"><code>\Cx\Core\Setting\Controller\Setting::add(
    &#39;emailOption&#39;,
    &#39;&#39;,
    1,
    \Cx\Core\Setting\Controller\Setting::TYPE_EMAIL,
    null,
    &#39;demo&#39;
);</code></pre></td>
</tr>
<tr class="even">
<td><p><strong>Checkbox</strong></p></td>
<td><p>TYPE_CHECKBOX</p></td>
<td><p>checkbox</p></td>
<td><p>HTML-input type checkbox</p></td>
<td><p>-</p></td>
<td><pre class="php"><code>\Cx\Core\Setting\Controller\Setting::add(
    &#39;checkboxOption&#39;,
    &#39;0&#39;,
    1,
    \Cx\Core\Setting\Controller\Setting::TYPE_CHECKBOX,
    null,
    &#39;demo&#39;
);</code></pre></td>
</tr>
<tr class="odd">
<td><p><strong>Radio</strong></p></td>
<td><p>TYPE_RADIO</p></td>
<td><p>radio</p></td>
<td><p>HTMl-input type radio</p></td>
<td><p>Options are defined analogous to type <em>Dropdown</em></p></td>
<td><pre class="php"><code>\Cx\Core\Setting\Controller\Setting::add(
    &#39;radioOption&#39;,
    &#39;0&#39;,
    1,
    \Cx\Core\Setting\Controller\Setting::TYPE_RADIO,
    &#39;0:first-option,1:second-option,2:third-option&#39;,
    &#39;demo&#39;
);</code></pre></td>
</tr>
<tr class="even">
<td><p><strong>Date</strong></p></td>
<td><p>TYPE_DATE</p></td>
<td><p>date</p></td>
<td><p>Date selector</p></td>
<td><p>-</p></td>
<td><pre class="php"><code>\Cx\Core\Setting\Controller\Setting::add(
    &#39;dateOption&#39;,
    &#39;&#39;,
    1,
    \Cx\Core\Setting\Controller\Setting::TYPE_DATE,
    null,
    &#39;demo&#39;
);</code></pre></td>
</tr>
<tr class="odd">
<td><p><strong>Datetime</strong></p></td>
<td><p>TYPE_DATETIME</p></td>
<td><p>datetime</p></td>
<td><p>Datetime selector</p></td>
<td><p>-</p></td>
<td><pre class="php"><code>\Cx\Core\Setting\Controller\Setting::add(
    &#39;dateTimeOption&#39;,
    &#39;&#39;,
    1,
    \Cx\Core\Setting\Controller\Setting::TYPE_DATETIME,
    null,
    &#39;demo&#39;
);</code></pre></td>
</tr>
<tr class="even">
<td><p><strong>Image</strong></p></td>
<td><p>TYPE_IMAGE</p></td>
<td><p>image</p></td>
<td><p>An image selection option. Two modes are suppored: <strong>reference</strong> (default) and <strong>copy</strong></p></td>
<td><dl>
<dt>Reference</dt>
&#10;</dl>
<p>Mode <strong>reference</strong> is used to store the path to the selected image.</p>
<dl>
<dt>Copy</dt>
&#10;</dl>
<p>Mode <strong>copy</strong> is used to copy the selected image to the location set in attribute <em>value</em>.</p></td>
<td><pre class="php"><code>\Cx\Core\Setting\Controller\Setting::add(
    &#39;imageOption&#39;,
    &#39;&#39;,
    1,
    \Cx\Core\Setting\Controller\Setting::TYPE_IMAGE,
    null,
    &#39;demo&#39;
);</code></pre>
<pre class="php"><code>\Cx\Core\Setting\Controller\Setting::add(
    &#39;imageOption&#39;,
    &#39;favicon.ico&#39;,
    1,
    \Cx\Core\Setting\Controller\Setting::TYPE_IMAGE,
    &#39;{&quot;type&quot;:&quot;copy&quot;}&#39;,
    &#39;demo&#39;
);</code></pre></td>
</tr>
<tr class="odd">
<td><p><strong>File content</strong></p></td>
<td><p>TYPE_FILECONTENT</p></td>
<td><p>file</p></td>
<td><p>A HTML-textarea with content of specified file.</p></td>
<td><p>Specify path of file to edit in attribute <em>values</em>. Path to file must be relative to the installations document root.</p></td>
<td><pre class="php"><code>\Cx\Core\Setting\Controller\Setting::add(
    &#39;fileContentOption&#39;,
    &#39;&#39;,
    1,
    \Cx\Core\Setting\Controller\Setting::TYPE_FILECONTENT,
    &#39;robots.txt&#39;,
    &#39;demo&#39;
);</code></pre></td>
</tr>
<tr class="even">
<td><p><strong>Button</strong> <sup>Experimental</sup></p></td>
<td><p>TYPE_BUTTON</p></td>
<td><p>button</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>Checkbox group</strong> <sup>Experimental</sup></p></td>
<td><p>TYPE_CHECKBOXGROUP</p></td>
<td><p>checkboxgroup</p></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## Advanced usages

### Conditional option adding

Add new configuration option only if it does not yet exist and throw exception in case the operation fails.

``` php
// initialize section
\Cx\Core\Setting\Controller\Setting::init('Demo', 'config', 'FileSystem');

// add new configuration option, if it does not yet exist
if (
    !\Cx\Core\Setting\Controller\Setting::isDefined('simpleTextOption') &&
    !\Cx\Core\Setting\Controller\Setting::add(
        'simpleTextOption',
        'defaultText',
        1,
        \Cx\Core\Setting\Controller\Setting::TYPE_TEXT,
        null,
        'config'
    )
) {
    throw new \Exception("Failed to add new configuration option");
}
```

### Array populating options

If section is already initialized, we can define behavior of populating this section with data. For that purpose we have 5th parameter. Assuming we have already initialized section Config\Yaml and have following data in it's array: `array(1,2)`, and by calling init of this section we would retrieve `array(2,3,4)`. Then:

`NOT_PUPULATE` (default)  

``` php
// would check if section exists and returns, doing no action; We still have array(1,2)
\Cx\Core\Setting\Controller\Setting::init('Config', '','Yaml', null, Setting::NOT_PUPULATE);
```

`POPULATE`  

``` php
// would retrieve elements from Yaml and add elements that not exist to array; In the end we will have array(1,2,3,4)
\Cx\Core\Setting\Controller\Setting::init('Config', '','Yaml', null, Setting::POPULATE);
```

`REPOPULATE`  

``` php
// would replace array with new data;  In the end we will have array(2,3,4);
\Cx\Core\Setting\Controller\Setting::init('Config', '', 'Yaml', null, Setting::REPOPULATE);
```

### Switching section, engine, module

There are several ways to switch between section and engine once initialized.

By calling `setEngineType` (Recommended)  

``` php
// initialize section
\Cx\Core\Setting\Controller\Setting::init('Config', '','Yaml');

//would switch to the section TodoManager, engine FileSystem and group config; If engine not exists - returns false;
\Cx\Core\Setting\Controller\Setting::setEngineType('TodoManager', 'FileSystem', 'config');
```

By calling `init()`  

``` php
// initialize section
\Cx\Core\Setting\Controller\Setting::init('Config', '','Yaml');

//would switch to the section TodoManager, engine FileSystem and group config; if section not exists - would initialize it
\Cx\Core\Setting\Controller\Setting::init('TodoManager', 'config','FileSystem');
```

### Fetching data

Fetch all configuration options at once:

`getSettings()` (Recommended)  

Fetch all options from a repository:

``` php
\Cx\Core\Setting\Controller\Setting::init('TodoManager', '', 'FileSystem');
$engine = \Cx\Core\Setting\Controller\Setting::getSettings('TodoManager', 'FileSystem');
$data = $engine->getArraySettings();
```

Fetch options of a specific group of a repository:

``` php
\Cx\Core\Setting\Controller\Setting::init('TodoManager', '', 'FileSystem');
$data = \Cx\Core\Setting\Controller\Setting::getSettings('TodoManager', 'FileSystem', 'config');
```

`getArray()`  

Fetch all options from a repository:

``` php
\Cx\Core\Setting\Controller\Setting::init('TodoManager', '', 'FileSystem');
$options = \Cx\Core\Setting\Controller\Setting::getArray('TodoManager');
```

Fetch options of a specific group of a repository:

``` php
\Cx\Core\Setting\Controller\Setting::init('TodoManager', '', 'FileSystem');
$options = \Cx\Core\Setting\Controller\Setting::getArray('TodoManager','config');
```

### Display configuration options

Integrate external data into configuration GUI (like [MailTemplate](MailTemplate.md) GUI)

``` php
// initialize section
\Cx\Core\Setting\Controller\Setting::init('Shop', 'config');

// show external generated view of MailTemplate
$objTemplate = new \Cx\Core\Html\Sigma('/path/to/html/template');
\Cx\Core\Setting\Controller\Setting::show_external(
    $objTemplate,
    'Mail Templates',
    \MailTemplate::overview(
         'TodoManager',
         'config',
         \Cx\Core\Setting\Controller\Setting::getValue(
             'numof_mailtemplate_per_page_backend'
         )
    )->get()
);
\Cx\Core\Setting\Controller\Setting::show_external(
    $objTemplate,
    'Manage Mail Template',
    \MailTemplate::edit('shop')->get()
);

// TODO: instead of using echo, do assign the returned string to a template variable 
echo $objTemplate->get();
```

### Update configuration option

Update single configuration option  

``` php
// initialize section
\Cx\Core\Setting\Controller\Setting::init('TodoManager', null, 'FileSystem');

// set new value to configuration option
\Cx\Core\Setting\Controller\Setting::set('email', $email);

// store changed configuration option
if (\Cx\Core\Setting\Controller\Setting::update('email')) {
    echo 'Config successfully updated';
}
```

Update multiple configuration options  

``` php
// initialize section
\Cx\Core\Setting\Controller\Setting::init('TodoManager', null, 'FileSystem');

// set new values to configuration options
\Cx\Core\Setting\Controller\Setting::set('email', $email);
\Cx\Core\Setting\Controller\Setting::set('address', $address);
\Cx\Core\Setting\Controller\Setting::set('user', $user);

// store all changed configuration options
if (\Cx\Core\Setting\Controller\Setting::updateAll() === true) { // updateAll() returns NULL on a no-op
    echo 'Config successfully updated';
}
```

## CLI

The Setting component is available through the [CLI interface of Cloudrexx](CLI script.md).

It provides the ability to list all options from a specific repository or to add/remove and get/set options in a repository.

### Fetch options

Synopsis  

``` php
cx Setting list <component> [-group=<group>] [-engine=<engine>] [-repository=<repository>]
```

<table>
<thead>
<tr class="header">
<th><p>Argument</p></th>
<th><p>Description</p></th>
<th><p>Optional</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>&lt;component&gt;</strong></p></td>
<td><p>The component to load the options from. I.e: 'Shop'</p></td>
<td><p>No</p></td>
</tr>
<tr class="even">
<td><p><strong>&lt;group&gt;</strong></p></td>
<td><p>Load only the options of group <em><group></em></p></td>
<td><p>Yes</p></td>
</tr>
<tr class="odd">
<td><p><strong>&lt;engine&gt;</strong></p></td>
<td><p>Set <a href="#Engines" title="wikilink">engine</a> to use</p></td>
<td><p>Yes</p></td>
</tr>
<tr class="even">
<td><p><strong>&lt;repository&gt;</strong></p></td>
<td><p>Relative path to the directory where the repository is located. I.e. if the repository of the component Shop is located at tmp/data/Shop.yml then you would set this argument to: tmp/data</p>
<p>This option defaults to directory "config" of the Cloudrexx installation.</p></td>
<td><p>Yes</p></td>
</tr>
</tbody>
</table>

!!! example  

    List all options from Yaml repository of component Config:

    ``` php
    cx Setting list Config -engine=Yaml
    ```

### Add option

Synopsis  

``` php
cx Setting add <component> [-group=<group>] [-engine=<engine>] [-repository=<repository>] <name> <value> <ord> <type> <values>
```

<table>
<thead>
<tr class="header">
<th><p>Argument</p></th>
<th><p>Description</p></th>
<th><p>Optional</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>&lt;component&gt;</strong></p></td>
<td><p>The component to load the options from. I.e: 'Shop'</p></td>
<td><p>No</p></td>
</tr>
<tr class="even">
<td><p><strong>&lt;group&gt;</strong></p></td>
<td><p>Load only the options of group <em><group></em></p></td>
<td><p>Yes</p></td>
</tr>
<tr class="odd">
<td><p><strong>&lt;engine&gt;</strong></p></td>
<td><p>Set <a href="#Engines" title="wikilink">engine</a> to use</p></td>
<td><p>Yes</p></td>
</tr>
<tr class="even">
<td><p><strong>&lt;repository&gt;</strong></p></td>
<td><p>Relative path to the directory where the repository is located. I.e. if the repository of the component Shop is located at tmp/data/Shop.yml then you would set this argument to: tmp/data</p>
<p>This option defaults to directory "config" of the Cloudrexx installation.</p></td>
<td><p>Yes</p></td>
</tr>
<tr class="odd">
<td><p><strong>&lt;name&gt;</strong></p></td>
<td><p>The name of the configuration option to add</p></td>
<td><p>No</p></td>
</tr>
<tr class="even">
<td><p><strong>&lt;value&gt;</strong></p></td>
<td><p>The initial default value of the configuration option</p></td>
<td><p>No</p></td>
</tr>
<tr class="odd">
<td><p><strong>&lt;ord&gt;</strong></p></td>
<td><p>An integer used to put the option in order (in a GUI) in relation to the other defined configuration options. The higher the integer is, the later will the option be parsed (in relation to other options having a lower integer set).</p></td>
<td><p>No</p></td>
</tr>
<tr class="even">
<td><p><strong>&lt;type&gt;</strong></p></td>
<td><p>Any of the available <a href="#Option_Types" title="wikilink">option types</a>. Defaults to 'text'.</p></td>
<td><p>No</p></td>
</tr>
<tr class="odd">
<td><p><strong>&lt;values&gt;</strong></p></td>
<td><p>Additional <a href="#Option_Types" title="wikilink">option types</a> specific data. I.e. for option type <em>Radio</em> the available radio options are defined through this argument.</p>

<p><strong>Note</strong>: For <a href="#Option_Types" title="wikilink">option types</a> without specific data do set to an empty quoted string <code>''</code></p>
</td>
<td><p>No</p></td>
</tr>
</tbody>
</table>

Examples  

``` php
cx Setting add Config -group=site -engine=Yaml useVirtualLanguageDirectories on 1 radio on:TXT_ACTIVATED,off:TXT_DEACTIVATED
```

``` php
cx Setting add Config -group=administrationArea -engine=Yaml portBackendHTTPS 80 1 text ''
```

### Alter option

Synopsis

``` php
cx Setting alter <component> [-group=<group>] [-engine=<engine>] [-repository=<repository>] <name> <new_type> <new_values> [<new_group> [<new_ord>]]
```

<table>
<thead>
<tr class="header">
<th><p>Argument</p></th>
<th><p>Description</p></th>
<th><p>Optional</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>&lt;component&gt;</strong></p></td>
<td><p>The component to load the option from. I.e: 'Shop'</p></td>
<td><p>No</p></td>
</tr>
<tr class="even">
<td><p><strong>&lt;group&gt;</strong></p></td>
<td><p>The group to load the option from</p></td>
<td><p>Yes</p></td>
</tr>
<tr class="odd">
<td><p><strong>&lt;engine&gt;</strong></p></td>
<td><p>Set <a href="#Engines" title="wikilink">engine</a> to use</p></td>
<td><p>Yes</p></td>
</tr>
<tr class="even">
<td><p><strong>&lt;repository&gt;</strong></p></td>
<td><p>Relative path to the directory where the repository is located. I.e. if the repository of the component Shop is located at tmp/data/Shop.yml then you would set this argument to: tmp/data</p>
<p>This option defaults to directory "config" of the Cloudrexx installation.</p></td>
<td><p>Yes</p></td>
</tr>
<tr class="odd">
<td><p><strong>&lt;name&gt;</strong></p></td>
<td><p>The name of the configuration option to alter</p></td>
<td><p>No</p></td>
</tr>
<tr class="even">
<td><p><strong>&lt;new_type&gt;</strong></p></td>
<td><p>Any of the available <a href="#Option_Types" title="wikilink">option types</a>.</p></td>
<td><p>No</p></td>
</tr>
<tr class="odd">
<td><p><strong>&lt;new_values&gt;</strong></p></td>
<td><p>Additional <a href="#Option_Types" title="wikilink">option types</a> specific data. I.e. for option type <em>Radio</em> the available radio options are defined through this argument.</p>

<p><strong>Note</strong>: For <a href="#Option_Types" title="wikilink">option types</a> without specific data do set to an empty quoted string <code>''</code></p>
</td>
<td><p>No</p></td>
</tr>
<tr class="even">
<td><p><strong>&lt;new_group&gt;</strong></p></td>
<td><p>New group to assign the option to</p></td>
<td><p>Yes</p></td>
</tr>
<tr class="odd">
<td><p><strong>&lt;new_ord&gt;</strong></p></td>
<td><p>An integer used to put the option in order (in a GUI) in relation to the other defined configuration options. The higher the integer is, the later will the option be parsed (in relation to other options having a lower integer set).</p></td>
<td><p>Yes</p></td>
</tr>
</tbody>
</table>

Examples

``` php
cx Setting alter Config -group=site -engine=Yaml useVirtualLanguageDirectories checkbox ''
```

``` php
cx Setting alter Config -group=administrationArea -engine=Yaml portBackendHTTPS text '' administrationArea 10
```

### Delete option

Synopsis  

``` php
cx Setting delete <component> [-group=<group>] [-engine=<engine>] [-repository=<repository>] <name>
```

<table>
<thead>
<tr class="header">
<th><p>Argument</p></th>
<th><p>Description</p></th>
<th><p>Optional</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>&lt;component&gt;</strong></p></td>
<td><p>The component to load the options from. I.e: 'Shop'</p></td>
<td><p>No</p></td>
</tr>
<tr class="even">
<td><p><strong>&lt;group&gt;</strong></p></td>
<td><p>Load only the options of group <em><group></em></p></td>
<td><p>Yes</p></td>
</tr>
<tr class="odd">
<td><p><strong>&lt;engine&gt;</strong></p></td>
<td><p>Set <a href="#Engines" title="wikilink">engine</a> to use</p></td>
<td><p>Yes</p></td>
</tr>
<tr class="even">
<td><p><strong>&lt;repository&gt;</strong></p></td>
<td><p>Relative path to the directory where the repository is located. I.e. if the repository of the component Shop is located at tmp/data/Shop.yml then you would set this argument to: tmp/data</p>
<p>This option defaults to directory "config" of the Cloudrexx installation.</p></td>
<td><p>Yes</p></td>
</tr>
<tr class="odd">
<td><p><strong>&lt;name&gt;</strong></p></td>
<td><p>The name of the configuration option to delete</p></td>
<td><p>No</p></td>
</tr>
</tbody>
</table>

!!! example  

    Delete option used to define if virtual language directories shall be used:

    ``` php
    cx Setting delete Config -engine=Yaml useVirtualLanguageDirectories
    ```

### Update option

Synopsis  

``` php
cx Setting set <component> [-group=<group>] [-engine=<engine>] [-repository=<repository>] <name> <value>
```

<table>
<thead>
<tr class="header">
<th><p>Argument</p></th>
<th><p>Description</p></th>
<th><p>Optional</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>&lt;component&gt;</strong></p></td>
<td><p>The component to load the options from. I.e: 'Shop'</p></td>
<td><p>No</p></td>
</tr>
<tr class="even">
<td><p><strong>&lt;group&gt;</strong></p></td>
<td><p>Load only the options of group <em><group></em></p></td>
<td><p>Yes</p></td>
</tr>
<tr class="odd">
<td><p><strong>&lt;engine&gt;</strong></p></td>
<td><p>Set <a href="#Engines" title="wikilink">engine</a> to use</p></td>
<td><p>Yes</p></td>
</tr>
<tr class="even">
<td><p><strong>&lt;repository&gt;</strong></p></td>
<td><p>Relative path to the directory where the repository is located. I.e. if the repository of the component Shop is located at tmp/data/Shop.yml then you would set this argument to: tmp/data</p>
<p>This option defaults to directory "config" of the Cloudrexx installation.</p></td>
<td><p>Yes</p></td>
</tr>
<tr class="odd">
<td><p><strong>&lt;name&gt;</strong></p></td>
<td><p>The name of the configuration option to update</p></td>
<td><p>No</p></td>
</tr>
<tr class="even">
<td><p><strong>&lt;value&gt;</strong></p></td>
<td><p>The new value of the configuration option to store</p></td>
<td><p>No</p></td>
</tr>
</tbody>
</table>

!!! example  

    Activate the use of virtual language directories:

    ``` php
    Setting set Config -engine=Yaml useVirtualLanguageDirectories on
    ```

### Fetch option

Synopsis  

``` php
cx Setting get <component> [-group=<group>] [-engine=<engine>] [-repository=<repository>] <name>
```

<table>
<thead>
<tr class="header">
<th><p>Argument</p></th>
<th><p>Description</p></th>
<th><p>Optional</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>&lt;component&gt;</strong></p></td>
<td><p>The component to load the options from. I.e: 'Shop'</p></td>
<td><p>No</p></td>
</tr>
<tr class="even">
<td><p><strong>&lt;group&gt;</strong></p></td>
<td><p>Load only the options of group <em><group></em></p></td>
<td><p>Yes</p></td>
</tr>
<tr class="odd">
<td><p><strong>&lt;engine&gt;</strong></p></td>
<td><p>Set <a href="#Engines" title="wikilink">engine</a> to use</p></td>
<td><p>Yes</p></td>
</tr>
<tr class="even">
<td><p><strong>&lt;repository&gt;</strong></p></td>
<td><p>Relative path to the directory where the repository is located. I.e. if the repository of the component Shop is located at tmp/data/Shop.yml then you would set this argument to: tmp/data</p>
<p>This option defaults to directory "config" of the Cloudrexx installation.</p></td>
<td><p>Yes</p></td>
</tr>
<tr class="odd">
<td><p><strong>&lt;name&gt;</strong></p></td>
<td><p>The name of the configuration option to fetch</p></td>
<td><p>No</p></td>
</tr>
</tbody>
</table>

!!! example  

    Get if the usage of virtual language directories is activated:

        cx Setting get Config -engine=Yaml useVirtualLanguageDirectories

## API Documentation

**TODO: This should be moved into the API documentation**

-   **static function changed()**
    -   TRUE, if minimum one of the loaded setting has been changed (*updateAll()* will check these)
    -   *\Cx\Core\Setting\Controller\Setting::changed()*

-   **static function tab_index($tab_index=null)**
    -   Get or set the current tab
    -   *\Cx\Core\Setting\Controller\Setting::tab_index(integer $tab_index)*

-   **static function init($section, $group = null, $engine = 'Database', $fileSystemConfigRepository = null, $populate = 0)**
    -   Activate or load the setting for a specific module (as the case may be constricted on a given group)
    -   Section must be set up
    -   The Engine type Database or File system, Default to set Database, You may use engine type FileSystem
    -   Database engine: *\Cx\Core\Setting\Controller\Setting::init($section, $group,$engine = 'Database')*
    -   FileSystem engine: *\Cx\Core\Setting\Controller\Setting::init($section, $group,$engine = 'FileSystem')*
    -   Yaml engine: *\Cx\Core\Setting\Controller\Setting::init($section, $group,$engine = 'Yaml')*
    -   Once you initialize section, it is stored in global Setting array and you don't not need to reinitialize it, unless other specified.
    -   You may set behavior of what to do when section already exists with $populate:
        -   *\Cx\Core\Setting\Controller\Setting::NOT_POPULATE - do nothing, return; default behavior*
        -   *\Cx\Core\Setting\Controller\Setting::POPULATE - add values to existing array*
        -   *\Cx\Core\Setting\Controller\Setting::REPOPULATE - reload section; default behavior in old logic*
        -   *Example: \Cx\Core\Setting\Controller\Setting::init($section, $group, $engine, null, \Cx\Core\Setting\Controller\Setting::POPULATE)*

-   **static function flush()**
    -   Reset the content of the class (forces for example the re-initialization at the next getArray())
    -   *\Cx\Core\Setting\Controller\Setting::flush()*

-   **static function getArray($section = null, $group=null)**
    -   Returns array of current engine based on $section and $group.
    -   If no section is specified, currently set section is taken into consideration.

-   **static function getValue($name, $section = null)**
    -   Returns the value for the setting with the given name
    -   If $section is specified, loads value from $section, otherwise loads value from currently set section
    -   *\Cx\Core\Setting\Controller\Setting::getValue($name, $section)*

-   **static function set($name, $value)**
    -   Set the value of the setting with the given name
    -   *\Cx\Core\Setting\Controller\Setting::set($name,$value)*

-   **static function updateAll()**
    -   Stores the loaded settings into the database or FileSystem\Yaml (only if minimum one change have been done)
    -   *\Cx\Core\Setting\Controller\Setting::updateAll()*

-   **static function update($name)**
    -   Stores one setting (without check!)
    -   Updates the value for the given name in the settings,note that this method does not work for adding new settings.
    -   *\Cx\Core\Setting\Controller\Setting::update($name)*

-   *'static function add($name, $value, $ord=false, $type='text', $values=*, $group=null)'''
    -   Adds a new setting to the group
    -   This method is only used for an installation script for a new module or will be used in update.
    -   *\Cx\Core\Setting\Controller\Setting::add( $name, $value, $ord, $type, $values,$group)*

-   **static function storeFromPost()**
    -   Update and store all settings found in the post array
    -   *\Cx\Core\Setting\Controller\Setting::storeFromPost()*

-   **static function delete($name=null, $group=null)**
    -   Deletes the setting with the given name. Only for advanced users!
    -   Delete one or more records from the Database or FileSystem
    -   At least one of the parameter values must be non-empty.
    -   *\Cx\Core\Setting\Controller\Setting::delete($name, $group)*

-   **static function show(&$objTemplateLocal, $uriBase, $section=, $tab_name=, $prefix='TXT\_')**
    -   Shows the settings in the given template object
    -   *\Cx\Core\Setting\Controller\Setting::show($objTemplate, $uriBase, $section, $tab_name, $prefix)*

-   **static function show_section(&$objTemplateLocal, $section=, $prefix='TXT\_')**
    -   Shows the settings for the given group in the given template object
    -   *\Cx\Core\Setting\Controller\Setting::show_section($objTemplate, $section, $prefix)*

-   **static function show_external(&$objTemplateLocal, $tab_name, $content)**
    -   Includes an external site (e.g. MailTemplates) into the Setting view (as a new tab)
    -   Adds an external settings view to the current template
    -   *\Cx\Core\Setting\Controller\Setting::show_external($objTemplate, $tab_name, $content)*

-   **static function storeFromPost()**
    -   Stores POST data and saves it to Database\FileSystem\Yaml using set() and updateAll().
    -   Should always be used to store POST data.
    -   *\Cx\Core\Setting\Controller\Setting::storeFromPost()*

-   **static function getSectionEngine($section = null, $engine = null)**
    -   Returns engine of section.
    -   If no section specified takes currenty set section.
    -   If section don't exist throws exception.
    -   If $engine don't exist takes default engine of section.
    -   *\Cx\Core\Setting\Controller\Setting::getSectionEngine($section, $engine)*

-   **static function getSectionEngine($section = null, $engine = null)**
    -   Returns engine of section.
    -   If no section specified takes currenty set section.
    -   If section don't exist throws exception.
    -   If $engine don't exist takes default engine of section.
    -   *\Cx\Core\Setting\Controller\Setting::getSectionEngine($section, $engine)*

-   **static function setSectionEngine($oSectionEngine, $populate)**
    -   Sets engine for section. Uses by init() but can also be called separately;
    -   If engine of section already exists - acts depending of $populate behavior:
        -   *\Cx\Core\Setting\Controller\Setting::NOT_POPULATE - do nothing, return; default behavior*
        -   *\Cx\Core\Setting\Controller\Setting::POPULATE - add values to existing array*
        -   *\Cx\Core\Setting\Controller\Setting::REPOPULATE - reload section; default behavior in old logic*
    -   *\Cx\Core\Setting\Controller\Setting::setSectionEngine($oSectionEngine, $populate)*

-   **static function setEngineType($section, $engine, $group = null)**
    -   Sets current section, engine and group;
    -   Once all data is initialized, **should be used instead of init(); in case you want to get specific section\engine\group** (i.e. in tabs)!
    -   *\Cx\Core\Setting\Controller\Setting::setEngineType($section, $engine, $group)*

-   **static function addToArray($name, $value)**
    -   Adds an element to array.
    -   Current section and engine are taken into consideration.
    -   *\Cx\Core\Setting\Controller\Setting::addToArray($name, $value)*

-   **static function getSettings($section = null, $engine = null, $group = null)**
    -   Basic method to get data array easy based of section, engine and group. **Recommended to use!**
    -   If section is null returns whole multidimensional array of all engines and sections.
    -   If section is set returns multidimensional array of all engines of that section.
    -   If section and engine is set returns data array from that section and engine.
    -   If section, engine and group are specified returns elements of concrete group from section and engine array.
    -   ''\Cx\Core\Setting\Controller\Setting::getSettings($section = null, $engine = null, $group = null)

-   **static function getCurrentSettings()**
    -   Returns data array based on currently set section, engine and group.
    -   Current section, engine and group can be set up via *\Cx\Core\Setting\Controller\Setting::setEngineType($section, $engine, $group)*
    -   **Recommended to use in most of cases** (i.e. in storeFromPost or other functions that work with data arrays).
    -   *\Cx\Core\Setting\Controller\Setting::getCurrentSettings()*

