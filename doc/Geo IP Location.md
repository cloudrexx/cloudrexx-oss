## Introduction
The component `GeoIp` provides geo IP location features.

## Setup ##
The `GeoIp` component does require a IP-location database to work. The latter is not part of the distribution. As it has to be obtained directly from [MaxMind](https://www.maxmind.com).

After obtaining an account-id and license-key, the IP-location database can be integrated by extending the `docker-compose.yml` file as follows:

1.  Add new service `geoipupdate` to section `services`:
    ``` yml
      geoipupdate:
        image: ghcr.io/maxmind/geoipupdate
        restart: unless-stopped
        environment:
          - GEOIPUPDATE_ACCOUNT_ID=${GEOIPUPDATE_ACCOUNT_ID}
          - GEOIPUPDATE_LICENSE_KEY=${GEOIPUPDATE_LICENSE_KEY}
          - GEOIPUPDATE_EDITION_IDS=GeoLite2-Country
          - GEOIPUPDATE_DB_DIR=/tmp/GeoIp
          - GEOIPUPDATE_FREQUENCY=72
        networks:
          - geoipupdate
        volumes:
          - ./core_modules/GeoIp/Data:/tmp/GeoIp"
    ```
    
    !!! note
        Replace `${GEOIPUPDATE_ACCOUNT_ID}` and `${GEOIPUPDATE_LICENSE_KEY}` by your account-id
        and license-key or set those environment variables in a [env_file](https://docs.docker.com/compose/compose-file/05-services/#env_file).
    
2.  Add new network `geoipupdate` to section `networks`:
    ``` yml
      geoipupdate:
    ```
    
3.  Restart env:
    ``` bash
    cx env restart
    ```

!!! info
    This product includes GeoLite2 Data created by MaxMind, available from [https://www.maxmind.com](https://www.maxmind.com).
