This page is about the Cloudrexx CLI script (`./cx`). It focuses on technical "nice to know"s.

## Structure

The script is divided into three parts: Windows, Unix and PHP:

### Bash 3 / Bash 4

The script requires Bash version 4. In order for the script to run on Bash 3 (which is default on MacOS) we route all calls through a wrapping container (docker image [`cloudrexx/ubuntu`](https://hub.docker.com/r/cloudrexx/ubuntu)). This is only done for Bash 3. Calls on Bash 4 and newer are executed directly.

In order to allow the `cx` script inside the wrapping Ubuntu container to control Docker on the host system the Docker socket is mounted to that container. This works nicely and allows to control Docker from inside the container. However, Docker Compose has an issue when mounting the current directory using ".": If run from inside the container, it tries to mount the container's current directory. In order to circumvent that, the script passes the argument `--proxy-host-dir=<cd>` to all calls of `cx`. This way the script running in the container knows the directory on the host and can change the `docker-compose.yml` accordingly. The same can be achieved by setting the environment variable `PROXY_HOST_DIR` which is used for when calling `./cx env shell --wrapper`.

### Windows / Unix

Using the same script for Unix and Windows is achieved by using the following scheme:

``` bash
@GOTO WIN \
2>/dev/null

# Unix part

exit
:WIN

REM Batch script
```

The first two lines instruct Windows to jump to the marker WIN and bash to route the error message that @GOTO WIN is not a valid command to /dev/null. The only issue with this is that the file can not start with a shebang, as Windows would throw an error on that.

The Windows part (batch script) is a simplistic wrapper to call the Unix part in a Docker container running Ubuntu. It checks if a file named `cx` is available in the current directory. If not, it instructs the Ubuntu container to download it. After that it calls `./cx` inside the Ubuntu container with all arguments you passed to it on Windows.

The wrapping mechanism is the same as for Bash version 3.

### Bash / PHP

The Unix part contains some internal commands. For all other calls the Unix part is a simple wrapper to call `php index.php` in the correct Docker container. If the container is not available, it tries to use the PHP executable on the host (if any). For the PHP part see [Command Mode](Command mode.md).

## Internal Commands

### `env`

This command manages an environment. There are two different types of environments: vhost and standalone.

#### Standalone Environments

In standalone environments the web-container's port on which it accepts connections is directly bound to the hosts port. Therefore there can only be one standalone environment per not-yet-occupied port on your system.

#### Vhost Environments

Vhost environments are all on the same port. In order for this to work, there needs to be a proxy container running. For more info see [envs command](#envs)

#### Subcommand description

<table>
<thead>
<tr class="header">
<th><p>Subcommand</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><code>init</code></p></td>
<td><p>Performs the following (in this order):</p>
<ul>
<li>Checks out the repositories (and sets "assume-unchanged" for tmp/* and config/*)</li>
<li>Internally calls <code>env config</code> to configure the environment</li>
<li>Sets the <code>CONTREXX_INSTALLED</code> constant to <code>true</code></li>
<li>Internally calls <code>env up</code> to start the docker containers</li>
<li>Internally calls <code>env update --db --drop-users</code> to initialize the database</li>
<li>Auto-configures caching for versions 5 and newer</li>
</ul></td>
</tr>
<tr class="even">
<td><p><code>config</code></p></td>
<td><p>Allows to automatically or manually configure the environment as well as displaying the current configuration. The configuration is saved in <code>config/settings.php</code>, <code>config/configuration.php</code> and <code>docker-compose.yml</code>. <code>_meta/docker-compose.tpl</code> is used as a template for <code>docker-compose.yml</code></p></td>
</tr>
<tr class="odd">
<td><p><code>up</code></p></td>
<td><p>Calls <code>docker-compose --project-name &lt;name&gt; up -d</code> and waits for the database server to be ready. <code>&lt;name&gt;</code> is set to your hostname without the dots, prefixed by <code>clxenv</code>.</p></td>
</tr>
<tr class="even">
<td><p><code>down</code></p></td>
<td><p>Calls <code>docker-compose --project-name &lt;name&gt; down</code>. The <code>-v</code> flag is added to that call if <code>--purge</code> is specified.</p></td>
</tr>
<tr class="odd">
<td><p><code>restart</code></p></td>
<td><p>Internally calls <code>down</code> and <code>up</code></p></td>
</tr>
<tr class="even">
<td><p><code>update</code></p></td>
<td><p>Updates both repositories and reloads the database if necessary. You can use <code>--db</code> to only do a database reload</p></td>
</tr>
<tr class="odd">
<td><p><code>status</code></p></td>
<td><p>Shows the status of the docker containers associated with the current environment by calling <code>docker-compose --project-name &lt;name&gt; ps</code></p></td>
</tr>
<tr class="even">
<td><p><code>info</code></p></td>
<td><p>Internally calls <code>status</code> and <code>config --show</code></p></td>
</tr>
<tr class="odd">
<td><p><code>shell</code></p></td>
<td><p>Opens an interactive bash shell to the web container</p></td>
</tr>
</tbody>
</table>

#### Automatic configuration

Automatically, the following presets are set:

| Version | PHP version | Database image | PHP image | Cache | Other |
| --- |--- | --- |--- | --- |--- |
| Cloudrexx 5 and newer | 8.2 | mariadb:10.6 | cloudrexx/php:&lt;php-version&gt;-fpm | All caches active | |
| Contrexx 3-4 | 5.6 | mariadb:5 | cloudrexx/web:PHP&lt;php-version&gt;-with-mysql | Inactive | Ports other than 80 are not supported |
| Contrexx 1-2 | 5.3 | mariadb:5 | cloudrexx/web:PHP&lt;php-version&gt;-with-mysql | Inactive | Ports other than 80 are not supported |

#### Known issues

-   The port for phpMyAdmin cannot be changed. Therefore the initialization of a second (non-vhost-) environment (even if on another port) fails.
-   On Windows you might encounter some permission problems. This is a known limitation to Docker on Windows [^1]

See [Missing things / Enhancement ideas](#enhancement-ideas-known-problems) for a complete list.

### `envs`

This command manages Docker containers which are not specific to one environment. Currently this manages an NGINX proxy with docker-gen [^2] which allows virtual hosting with no manual configuration. The proxy could be extended with automatically generated SSL certificates [^3].

#### Subcommand description

| Subcommand | Description                                                                             |
|------------|-----------------------------------------------------------------------------------------|
| `up`       | Starts the proxy                                                                        |
| `down`     | Stops the proxy                                                                         |
| `restart`  | Internally calls `down` and `up`                                                        |
| `status`   | Calls `docker ps` for the proxy container                                               |
| `info`     | Calls `docker ps` with a filter to show all containers which are part of an environment |
| `shell`    | Opens an interactive shell on the proxy container                                       |
| `debug`    | Shows logs from the NGINX proxy (wrapper for `docker logs -f`)                          |
| `list`     | Show a list of all environments that are up                                             |
| `find`     | Shows the working directory of an environment                                           |

#### Make one ENV accessible from another computer

As the `*.clx.dev` domains always point to localhost, another computer won't be able to access your ENVs using such a domain. So in order to allow access from another computer you need to find some domain (or IP. For simplicity we simply say "domain" from now on, even if it might be an IP address.) that points the other computer to yours. Possible values are your computer's hostname or IP address. Depending on your network there might be more possibilities.

Once you have such a domain, do the following in the main directory of the ENV you want to make accessible:

``` sh
./cx env down
# edit docker-compose.yml in your favorite editor
# look for the environment variable "VIRTUAL_HOST" in the first service ("web").
# add a comma (",", without a space) and your domain after the current value of "VIRTUAL_HOST"
# save (and close) the file
./cx env up
```

And there you go, your environment is now available on both of these domains. You can add as many domains as you want for each ENV (just don't use the same for multiple ENVs).

!!! note

    you may need to disable Cloudrexx' domain enforcement in order to allow the other computer to navigate the site.

[^1]: [Logs and troubleshooting - Docker Documentation](https://docs.docker.com/docker-for-windows/troubleshoot/#permissions-errors-on-data-directories-for-shared-volumes)
[^2]: [NGINX proxy with docker-gen](https://github.com/jwilder/nginx-proxy)
[^3]: [Let's encrypt NGINX proxy companion](https://github.com/JrCs/docker-letsencrypt-nginx-proxy-companion)

### `debug`

By default this shows the last fatal error of the Cloudrexx installation (using the debug log file `/tmp/log/dbg.log`).

With `--request` only the last request's log (from `/tmp/log/dbg.log`) is shown (piped to *less*).

With `--follow` opens the log stream (of `/tmp/log/dbg.log`).

With `--web` shows the log of the web server (Apache) (continuously).

### `help`

This command shows the help of all commands that are internally available. In addition it tries to call the help page via PHP and add the help from there to itself (basically listing the [command mode commands](Command mode.md)). This can easily be seen when running this command twice: once with the environment up and once down.

## Docker setup

### Images

The Docker images used by `cx` are hosted on [Docker Hub](https://hub.docker.com/u/cloudrexx). The following list shows the PHP extensions available in the Docker images provided by Cloudrexx. The images are based on the official PHP images (if not stated otherwise). PHP extensions are listed by:
``` bash
docker run --rm cloudrexx/web:PHP<php_version> php -r 'echo implode("\n", get_loaded_extensions()) . PHP_EOL;'
```

| Extension | PHP 5.3 [^4] | PHP 5.6 | PHP 7.0 | PHP 7.1 | PHP 7.2-fpm | PHP 7.3-fpm | PHP 7.4-fpm | PHP 8.0-fpm | PHP 8.1-fpm | PHP 8.2-fpm |
| - | :-: | :-: | :-: | :-: | :-: | :-: | :-: | :-: | :-: | :-: |
| Core | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: |
| date | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: |
| ereg | :material-check: | :material-check: | | | | | | | | |
| libxml | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: |
| openssl | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: |
| pcre | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: |
| sqlite3 | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: |
| zlib | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: |
| ctype | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: |
| curl | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: |
| dom | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: |
| fileinfo | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: |
| filter | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: |
| ftp | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: |
| hash | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: |
| iconv | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: |
| json | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: |
| mbstring | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: |
| SPL | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: |
| PDO | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: |
| session | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: |
| posix | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: |
| readline | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: |
| recode | :material-check: | | | | | | | | | |
| Reflection | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: |
| standard | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: |
| SimpleXML | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: |
| pdo_sqlite | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: |
| Phar | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: |
| tokenizer | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: |
| xml | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: |
| xmlreader | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: |
| xmlwriter | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: |
| mysqlnd | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: |
| bcmath | | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: |
| exif | | | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: |
| gd | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: |
| intl | | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: |
| mcrypt | | :material-check: | :material-check: | :material-check: | | | | | | |
| mysqli | :material-check: | :material-check: | :material-check: | :material-check: | | | | | | |
| pdo_mysql | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: |
| zip | | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: |
| memcached | | | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: |
| Zend OPcache | | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: |
| mysql| :material-check: | :material-check: | | | | | | | | |
| soap | :material-check: | | | | | | | | | |
| SQLite | :material-check: | | | | | | | | | |
| apc | :material-check: | | | | | | | | | |
| igbinary | | | | | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: |
| imagick | | | | | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: |
| sockets | | | | | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: | :material-check: |
| yaml | | | | | | | :material-check: | :material-check: | :material-check: | :material-check: |

[^4]: Based on [orsolin/docker-php-5.3-apache](https://hub.docker.com/r/orsolin/docker-php-5.3-apache/)

### Additional installations

#### Xdebug

Use the following command to activate the PHP Xdebug extension in an environment. Please note that this is not a persistent change and will be dropped when you restart your environment.

``` bash
./cx debug --install-x-debug
```

#### Composer

All images as of php:7.4-fpm have composer installed. It can be called as follows:
``` bash
./cx env composer --version
```

For older images the following command can be used to install it:

``` bash
./cx env exec --root "curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer"
```

If your cx version is to old you can call composer like this:

``` bash
./cx env exec "composer --version"
```

#### NodeJS / NPM

If you need NodeJS/NPM in your container you can install it with the following one-liner:

``` bash
./cx env exec --root "curl -sL https://deb.nodesource.com/setup_14.x | bash - && apt-get install -y nodejs"
```

After that you can call npm like this:

``` bash
./cx env exec "npm -v"
```

#### strace

!!! danger

    Due to security restrictions, *strace* can't be used by default in a Docker container. The following guide let's you disable certain security features of Docker to make *strace* working. You must never apply the following configuration on a production environment as it makes the container vulnerable to attacks.  

To allow debugging a process using strace the web container has to be configured as follows:

First, ensure the container is not running:

    ./cx env down

Then, add the following options to **docker-compose.yml**:

``` yaml
security_opt:
- seccomp:unconfined
privileged: true
```

Example of modified *docker-compose.yml* file:

``` yaml
services:
  web:
    image: "cloudrexx/web:PHP5.6-with-mysql"
    hostname: "example.clx.dev
    ports:
      - "80:80"
    volumes:
      - .:/var/www/html
    depends_on:
      - db
      - usercache
    networks:
      - front-end
      - back-end
    security_opt:
      - seccomp:unconfined
    privileged: true
```

Start up the environment:

    ./cx env up

Install *strace* in the web container

    ./cx env exec --root "apt update && apt install strace"

Finally, *strace* is available for usage:

    ./cx env exec --root "strace -p <PID>"

### HTTPS

HTTPS support is provided by the proxy container [^5], but is not yet fully automated. In order to activate HTTPS support do the following:

1.  Create a directory for the certificates
    
    !!! example
        ``` bash
        mkdir -p ~/.clx/certs/
        ```

1.  (Re)start envs with HTTPS support by using the argument `--certs-dir`
    
    !!! example
        ``` bash
        cx envs restart --certs-dir=$(echo ~/.clx/certs/)
        ```
    
    !!! warning
        The path set to `--certs-dir` must not end with a slash (`/`)!
    
    !!! warning
        The most common pitfall causing your environment to not work is forgetting to set the `--certs-dir` option!

1.  For each domain that you want to have HTTPS support, you'll have to add a private (`<domain>.key`) and public certificate key (`<domain>.crt`) file to your certificate directory.

    !!! note
        You'll have to import any self-signed certificates into your browser to have it accepted.
        Using `mkcert` is advised here, as it automatically ads the certificates to Firefox, Chromium and Chrome.

    !!! example
        Add self-signed certificates for `cloudrexx.clx.dev` and `*.cloudrexx.clx.dev`:
        ``` bash
        sudo apt-get install mkcert libnss3-tools
        domain="cloudrexx.clx.dev" && mkcert -cert-file ~/.clx/certs/$domain.crt -key-file ~/.clx/certs/$domain.key $domain *.$domain
        ```
    
    !!! warning
        When using an older version of `mkcert` than `1.4.4`, you'll have to
        import the certificate into Firefox manually. Do so by calling
        `mkcert -CAROOT` to get the location of the root-CA certificate. Then
        add this manually to the trust store of Firefox.

[^5]: [SSL Support](https://github.com/nginx-proxy/nginx-proxy/tree/main/docs#ssl-support)

### Connect with MySQL Workbench

In order to connect to the database instance directly (for example to use reverse engineering with MySQL Workbench) follow these steps:

-   Shut your environment down
    ``` bash
    cx env down
    ```
-   Edit the file docker-compose.yml in the root of your installation
-   In the section of the "db" container add the following lines

    ``` bash
    ports:
      - 3306:3306
    ```

-   Save the file and start the environment
-   The database should now be reachable on localhost on the default port.

!!! warning

    -   Please note that only one env can be configured to do so at a time. Otherwise the database server will not start. You may choose different ports to expose different database servers at the same time.
    -   The file docker-compose.yml will get overwritten on
        ``` bash
        cx env config
        ```

        If you want to do this in a persistent way you may add it to _meta/docker-compose.tpl or fork the scripts repository.

### Connect multiple ENVs to each other

[Envs](#env) that are using the root-domain `.clx.dev` can talk to each other using their hostnames.

!!!note
    An arbitrary root-domain can be set using `cx envs up --root-domain=<root-domain>`. If argument `--root-domain` is left out, then root-domain is set to `clx.dev`.

!!!note
    [Envs](#env) that are not using the root-domain can still talk to each other. However, to reach an [env](#env) that is not using the set root-domain, must be addressed using its _web_-container's full name. To get the full name of the _web_-container, do call the following in the root directory of your [ENV](#env) (assuming you use [Envs](#envs)):
    
    ``` bash
    echo "$(./cx envs list --dir=$PWD --real)_web_1"
    ```
    
    Then you can do the following in another [Env](#env) on the same machine:
    
    ``` bash
    ./cx env shell
    curl "https://<name_of_first_env>"
    ```

### Development vs Production Environment

Use `./cx env config --mode=<mode>` to switch between development and production environment. Available values for `<mode>` are:

-   `development` (default if not explicitly set)
-   `production`

#### Differences

| Setup                                                                                       | `development` | `production` |
|---------------------------------------------------------------------------------------------|---------------|--------------|
| phpMyAdmin Container                                                                        | Yes           | No           |
| Mailhog Container                                                                           | Yes           | No           |
| php-fpm [`pm`](https://www.php.net/manual/en/install.fpm.configuration.php)                 | `dynamic`     | `static`     |
| php-fpm [`pm.max_children`](https://www.php.net/manual/en/install.fpm.configuration.php)    | `5`           | `100`        |
| php-fpm [`zend.assertions`](https://www.php.net/manual/en/ini.core.php#ini.zend.assertions) | `1`           | `-1`         |

## Enhancement ideas / Known problems

### General

-   ENHANCEMENT: Add autocompletion
-   ENHANCEMENT: Add use-case for installing from package using the installer
-   PROBLEM: Cache autoconfig support for pre 5 is missing
-   PROBLEM: There's no way to access the MySQL server directly (which is a problem for the workflow when using MySQL workbench)
-   PROBLEM: If the directory used as web-root is owned by root, permissions do not work properly. This is because the user IDs cannot be mapped correctly.
-   PROBLEM: In case the name of the directory where the environment is being initialized does only contain numbers, then the script doesn't work as intented as it does wrongly interprete the hostname as IP address.
-   PROBLEM: `cx` does automatically read all data from standard input (in case there is any). This breaks the ability to call `cx` from within a `while`-loop reading from standard input as in `while read input; do cx <command>; done;`
      
    Workaround: call `cx` with input redirection. I.e.: `cx <command> < /dev/null`

    Proposed solution: do implement an argument (i.e.: `-n`) that prevents `cx` from reading from standard input

### Commands

#### Env

-   ~~./cx env init should force domain URL and protocol~~
-   ~~./cx env down should be able to shut the environment down even if the configuration has changed since "up"~~
-   ~~./cx env update --db should work for non-dockerized databases~~
-   ~~./cx env up should show URLs~~
-   ENHANCEMENT: `./cx env init`: GIT checkout output very long (branch list). This is very annoying on Windows, since output is very slow there
-   ENHANCEMENT: Add parameters to `./cx env shell` to start shell of all containers
-   ENHANCEMENT: Autoscaling cannot be configured
-   ENHANCEMENT: `./cx env config` should check if the configured images exist before writing `docker-compose.yml`
-   ENHANCEMENT: Admin contact should be set
-   ENHANCEMENT: `./cx env update` should have an argument to specify another SQL dump
-   PROBLEM: Make the port for phpMyAdmin configurable
-   PROBLEM: Changing the hostname loses the db volume
-   PROBLEM: `./cx env update` should update GIT first, then Docker, then reconfigure the environment (only if `cx` or `docker-compose.tpl` has changed, ask user if he wants to, db will get dropped), then update the database
-   PROBLEM: `./cx env update` assumes that the environment is up but does not check it
-   PROBLEM: `./cx env update` does not update `_meta`

#### Envs

-   ~~Automatically adopt/release existing environments on up/down~~
-   ~~Add subcommand "./cx envs locate &lt;env&gt;" to find working directory of an environment (or integrate this in "./cx envs list")~~
    -   ~~Allow filtering to get all environments that are pointing to the same working directory~~
-   ENHANCEMENT: HTTPS support is missing
-   ENHANCEMENT: If socket path is not standard linux config, proxy container will not work

#### Debug

-   ENHANCEMENT: Add parameter to debug all of the environment's containers

#### Other commands

### Platform specific

#### Windows

-   ~~Add a shortcut for the bash shell of the Ubuntu wrapper container~~
-   ~~./cx help fails if a command is specified (request is redirected to PHP)~~
-   ~~Commands executed in the wrapper may fail due to missing proxy host directory~~
-   ENHANCEMENT: Add a default user to cloudrexx/ubuntu image (other than root)
-   ENHANCEMENT: Add development environment scripts to cloudrexx/ubuntu image
-   ENHANCEMENT: If socket path is not standard linux config, proxy container will not work
-   INFO: After "./cx env init", cx is deleted. The script automatically re-fetches it on next run. If you choose another branch than "master" (or a repository with a different state than the default one) you may have the wrong version of cx. Additionally, the command shows "error: unable to create file cx: No such file or directory" during execution.
    -   The reason for this is Windows' behavior with files that are executed. They seem to be locked somehow.
-   PROBLEM: There might be a problem if the directory name is long

#### MacOS X

-   ~~PROBLEM: The script does not work as MacOS includes Bash version 3 (version 4 is required).~~
-   PROBLEM: The local directory (/root/cx) displayed in message "sudo cp /root/cx /usr/local/bin" is wrong (its taken from the wrapper container instead from the host).

### Version specific

#### 1.x

All problems for versions 2, 3 and 4 will exist as well.

-   PROBLEM: Untested

#### 2.x

All problems for versions 3 and 4 will exist as well. No known problems specific to this version.

#### 3.x

All problems for version 4 will exist as well. No known problems specific to this version.

#### 4.x

-   ~~./cx env config does not automatically configure the environment~~
-   ~~Port config is missing~~
-   ~~Offset path in .htaccess is wrong~~
-   PROBLEM: Caching autoconfig is missing
-   PROBLEM: .gitignore is not up to date in branch / .gitignore should be forced for all versions
-   PROBLEM: Config might get lost

#### 5.x

-   PROBLEM: Port configuration might get overwritten.
