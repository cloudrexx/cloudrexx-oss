## (PHP) i18n with cxjs

In `lib/FRAMEWORK/cxjs/i18n`, you will find so-called i18nProviders. `ContrexxJavascriptI18nProvider` is an interface implemented by classes in order to provide i18n/l10n data to javascript.

### Learning by watching: an example

Given we want to internationalize our HelloWorld function in javascript:
```javascript
function helloWorld() {
    alert('hello world');
}
```

Easily done. Create your I18nProvider, let's say in lib/FRAMEWORK/cxjs/i18n/helloWorld.class.php:
```php
<?php
class HelloWorldI18nProvider implements ContrexxJavascriptI18nProvider {
    public function getVariables($langCode) {
        $vars;
        switch($langCode) {
            case 'de':
                $vars = array('message' => 'Hallo Welt');
                break;
            case 'fr':
                $vars = array('message' => 'Bonjour la monde');
                break;          
            default:
                $vars = array('message' => 'Hello world');
        }        
        return $vars;
    }
}
```

Needless to say this is more or less dummy logic. Your internationalization can be as sophisticated as you like it to be. And what does our function look like now?
```javascript
cx.ready(function() {
    function helloWorld() {
        alert(cx.variables.get('message','helloWorld')I;
    }
});
```
You can see two things:
* the variable message was automatically initialized in scope helloWorld (which is the first part of our filename helloWorld.class.php)
* our function needs to be defined inside cx.ready(function() {...}), to make sure the framework finished loading.
