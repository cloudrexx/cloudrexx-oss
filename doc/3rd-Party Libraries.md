## Using Third-party libraries

If you decide to use a 3rd-party library, you'll have to make sure its license is compatible with the Contrexx license. List of currently used 3rd-party libraries: <https://api.contrexx.com/latest/third_party_libraries.html>

### Compatible Licenses

A list of known licenses that are **compatible** with Contrexx are as follows:

-   [Apache License](https://en.wikipedia.org/wiki/Apache_License)
-   [BSD License](https://en.wikipedia.org/wiki/BSD_licenses#Proprietary_software_licenses_compatibility) (except the *Original "BSD License"*, which is not compatible!):
-   [ISC License](https://en.wikipedia.org/wiki/ISC_license)
-   [GNU Lesser General Public License (LGPL)](https://en.wikipedia.org/wiki/GNU_Lesser_General_Public_License)
-   [MIT License](https://en.wikipedia.org/wiki/MIT_License) (*Expat License*, but not the *MIT/X Consortium License*!)
-   [Mozilla Public License (MPL)](https://en.wikipedia.org/wiki/Mozilla_Public_License)
-   [X11 License](https://directory.fsf.org/wiki/License:X11)

### Incompatible Licenses

A list of known licenses that are **incompatible** with Contrexx are as follows:

-   [GPL](https://en.wikipedia.org/wiki/GNU_General_Public_License)
-   Original "BSD License"
-   [XFree86](https://directory.fsf.org/wiki/License:XFree86_1.1) (MIT/X Consortium License)

## Include 3rd-Party library

To include a library into the Cloudrexx source, create folder for it under /lib (e.g. /lib/ADOdb) and copy the library's source to it. Make sure that the license and version of the library are easily detectable within the source (normally this is easy to achieve by simply keeping the license and readme files of the library).

Tip: For JavaScript libraries it is a good idea to copy them to a sub-folder named after its version (e.g. /lib/CKEditor/4.6.2). This way, browsers need to fetch the new versions of the JavaScript files after any update to the library.

### Update 3rd-Party library

Simply update the source in /lib. The same rules as for new libraries apply.

### Customize a 3rd-party library

Customizing a 3rd-party library within /lib is prohibited. Most newer libraries allow extensions via OOP inheritance and/or dependency injection. The only exception to this rule are fixes that will be in the next version of the library anyway.

