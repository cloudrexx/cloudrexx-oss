## Requirements (DRAFT)

!!! warning

    This section contains incomplete/false information

In general, the files of the Contrexx installation must be owned by the same user as the PHP instance of the web-server is running under

### Apache Module

If PHP is used as Apache Module (Apache-Handler), then the Contrexx files must be owned by the user under which the Apache Webserver is running. This would usually be *www-data*. However the common use case is, that you won't be able to change the owner of the Contrexx files to the appropriate user. I.e. on shared hosting platforms, where you usually upload your files by FTP, the owner of your files will be your FTP-account. In such cases, it is inevitable to activate the FTP-support of Contrexx.

### FastCGI

-   If PHP is used through the FastCGI interface, then the Contrexx files must be owned by the user under which the Apache Webserver is running

### IIS

-   Set write access manually

## Purpose

-   Permission access is handled by Contrexx (no more need for setting file permissions manually)
      
    Do not set file permissions manually using chmod(), instead use *\Cx\Lib\FileSystem\FileSystem::makeWritable()*
-   Write access to files limited to the owner of the file (no more global write access!)

## Current API

### File

The library `\Cx\Lib\FileSystem\File` allows file system operations on non-privileged files. Non-privileged files are files located in one of the following locations:

-   `/images`
-   `/media`
-   `/themes`
-   `/tmp/session-<sid>`

!!! note

    To be able to access files outside of the non-privileged locations, the library `\Cx\Lib\FileSystem\PrivilegedFile` can be used instead. Simply replace `\Cx\Lib\FileSystem\File` by `\Cx\Lib\FileSystem\PrivilegedFile` in the examples below.

#### Create

``` php
// Create new (empty) file
try {
    $objFile = new \Cx\Lib\FileSystem\File($absolute_path_to_file);
    $objFile->touch();
} catch (\Cx\Lib\FileSystem\FileSystemException $e) {
    \DBG::msg($e->getMessage());
}
```

#### Delete

``` php
// Delete file
try {
    $objFile = new \Cx\Lib\FileSystem\File($absolute_path_to_file);
    $objFile->delete();
} catch (\Cx\Lib\FileSystem\FileSystemException $e) {
    \DBG::msg($e->getMessage());
}
```

#### Write

``` php
// Write data to file
try {
    // $absolute_path_to_file can point to a new or existing file
    $objFile = new \Cx\Lib\FileSystem\File($absolute_path_to_file);
    $objFile->write($data);
} catch (\Cx\Lib\FileSystem\FileSystemException $e) {
    \DBG::msg($e->getMessage());
}
```

#### Read

``` php
// Read data from file
try {
    $objFile = new \Cx\Lib\FileSystem\File($absolute_path_to_file);
    $data = $objFile->getData();
} catch (\Cx\Lib\FileSystem\FileSystemException $e) {
    \DBG::msg($e->getMessage());
}
```

#### Permissions / Write Access

``` php
// Add write access to existing file
try {
    $objFile = new \Cx\Lib\FileSystem\File($absolute_path_to_file_or_directory);
    if ($objFile->makeWritable()) {
        echo 'file is writable';
    }
} catch (\Cx\Lib\FileSystem\FileSystemException $e) {
    \DBG::msg($e->getMessage());
}
```

#### Copy

``` php
// copy file
try {
    $objFile = new \Cx\Lib\FileSystem\File($absolute_path_to_existing_file);
    $objFile->copy($absolute_path_of_new_file_location_incl_file_name);
} catch (\Cx\Lib\FileSystem\FileSystemException $e) {
    \DBG::msg($e->getMessage());
}
```

#### Move

``` php
// move file
try {
    $objFile = new \Cx\Lib\FileSystem\File($absolute_path_to_existing_file);
    $objFile->move($absolute_path_of_new_file_location_incl_file_name, $overwrite_existing_file = false);
} catch (\Cx\Lib\FileSystem\FileSystemException $e) {
    \DBG::msg($e->getMessage());
}
```

### Folder

#### Create

``` php
# Create new folder
\Cx\Lib\FileSystem\FileSystem::make_folder($absolute_path_to_folder);
```

#### Delete

``` php
# Delete folder
\Cx\Lib\FileSystem\`[`FileSystem::delete_folder($absolute_path_to_directory`](FileSystem::delete_folder($absolute_path_to_directory)`, $recursive = true);
```

#### Permissions / Write Access

``` php
# Add write access to existing folder
try {
    $objFile = new \Cx\Lib\FileSystem\File($absolute_path_to_file_or_directory);
    if ($objFile->makeWritable()) {
        echo 'file is writable';
    }
} catch (\Cx\Lib\FileSystem\FileSystemException $e) {
    \DBG::msg($e->getMessage());
}
```

## Future API

-   All methods in this section throw a \Cx\Lib\FileSystem\FileSystemException on error.
-   There are a few more methods available that are not listed here. See core/MediaSource/ModelEntity/File.class.php for a complete list of public methods.

### File

#### Create

``` php
$file = $this->getComponent('MediaSource')->getFile(
    $filepath
);
$file->write();
```

#### Delete

``` php
$file = $this->getComponent('MediaSource')->getFile(
    $filepath
);
$file->remove();
```

#### Write

``` php
$file = $this->getComponent('MediaSource')->getFile(
    $filepath
);
$file->write($content);
```

#### Read

``` php
$file = $this->getComponent('MediaSource')->getFile(
    $filepath
);
$content = $file->read();
```

#### Copy

``` php
$file = $this->getComponent('MediaSource')->getFile(
    $filepath
);
$destinationFile = $this->getComponent('MediaSource')->getFile(
    $destinationFilepath
);
$file->copyTo($destinationFilepath);
```

#### Move

``` php
$file = $this->getComponent('MediaSource')->getFile(
    $filepath
);
$destinationFile = $this->getComponent('MediaSource')->getFile(
    $destinationFilepath
);
$file->moveTo($destinationFilepath);
```

### Folder

#### Create

``` php
$file = $this->getComponent('MediaSource')->getFile(
    $filepath
);
$file->setType(\Cx\Core\MediaSource\Model\Entity\File::TYPE_DIRECTORY);
$file->write();
```

#### Delete

``` php
$file = $this->getComponent('MediaSource')->getFile(
    $filepath
);
$file->remove();
```

