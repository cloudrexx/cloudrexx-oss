The following table explains the main folder structure of Cloudrexx:

| Directory      | Usage                                                               | Remarks                                                               |
|----------------|---------------------------------------------------------------------|-----------------------------------------------------------------------|
| `config`       | Configuration files                                                 |                                                                       |
| `core`         | [Components](Component Definition.md) of type `core`                |                                                                       |
| `core_modules` | [Components](Component Definition.md) of type `core_module`         |                                                                       |
| `customizing`  | Customized files                                                    |                                                                       |
| `feed`         | RSS newsfeed                                                        | Deprecated in favor of a [HTTP command](Command mode.md)              |
| `images`       | Images and media accessible via web                                 | Deprecated in favor of `/media`                                       |
| `installer`    | Installation routine and data                                       | Can be removed after installation                                     |
| `lang`         | Main language files                                                 | Deprecated in favor of `/core/Core/lang`                              |
| `lib`          | External libraries                                                  |                                                                       |
| `FRAMEWORK`    | Libraries by Cloudrexx                                              | Files directly in this folder are deprecated                          |
| `media`        | Media files accessible via web                                      |                                                                       |
| `model`        | Doctrine proxies, table prefix listener and EntityBase              | Deprecated in favor of `/core/Model/...`                              |
| `modules`      | [Components](Component Definition.md) of type `module`              |                                                                       |
| `themes`       | Themes                                                              |                                                                       |
| `tmp`          | Temporary files                                                     |                                                                       |
| `update`       | Update to newer Cloudrexx version                                   | Can be removed after update                                           |

