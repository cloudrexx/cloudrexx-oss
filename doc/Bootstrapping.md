This article describes the initialization and request parsing process of Cloudrexx.

## Bootstrapping Process

A simple overview of the bootstrapping process of Cloudrexx (referred to as **Cx Init** - see [Component User Framework](Component User Framework.md) and [Cx Class](https://api.contrexx.com/latest/classes/Cx.Core.Core.Controller.Cx.html)) is shown in the following illustration:

![Cx Init Sequence Diagram](img/SequenceDiagramCxInit.png)  

### Component Hooks

The following scheme illustrates the available component-hooks in relation to the bootstrapping process:

Color code:

-   <div style="height: 1em; width: 1em; background: #FF8F8F; display: inline-block;"></div> Part of core bootstrapping process, but handled by another component than Core
-   <div style="height: 1em; width: 1em; background: #52A8FF; display: inline-block;"></div> Hooks called on all affected components
-   All other colors are only for nesting level distinction. Those things are done by the Core component.

<div style="background:#6BB521; padding: 0 1px 1px 1px; margin:0;">

<h3 id="STAGE_1_init">STAGE 1: init</h3>

<div style="background:#E7F2D8;padding: .5em;">
<div style="background:#FFB643; padding: 1px; margin:10px 10px 10px 20px;">
<div style="background:#FCE2B8;padding: .5em;">

Map request (HTTP, CLI) to new Cx-Instance

</div>
</div>
</div>
</div>
<div style="background:#6BB521; padding: 0 1px 1px 1px; margin:0;">

<h3 id="STAGE_2_config">STAGE 2: config</h3>

<div style="background:#E7F2D8;padding: .5em;">
<div style="background:#FFB643; padding: 1px; margin:10px 10px 10px 20px;">

System Initialization

<div style="background:#FCE2B8;padding: .5em;">
<div style="background:#bcbcbc; padding: 1px; margin:10px 10px 10px 20px;">
<div style="background:#E1E1E1;padding: .5em;">

Load system config (config/configuration.php)

</div>
</div>
<div style="background:#bcbcbc; padding: 1px; margin:10px 10px 10px 20px;">
<div style="background:#E1E1E1;padding: .5em;">

Load base configuration (config/settings.php)

</div>
</div>
<div style="background:#bcbcbc; padding: 1px; margin:10px 10px 10px 20px;">
<div style="background:#E1E1E1;padding: .5em;">

Verify system health

</div>
</div>
<div style="background:#bcbcbc; padding: 1px; margin:10px 10px 10px 20px;">
<div style="background:#E1E1E1;padding: .5em;">

Initialize customizing setup

</div>
</div>
<div style="background:#bcbcbc; padding: 1px; margin:10px 10px 10px 20px;">
<div style="background:#E1E1E1;padding: .5em;">

Initialize system mode (Minimal, Command, Frontend, Backend)

</div>
</div>
<div style="background:#52A8FF; padding: 1px; margin:10px 10px 10px 20px;">

<h4 id="preInit">preInit()</h4>

<div style="background:#A3E0FF;padding: .5em;">

Do something before system initialization

USE CAREFULLY, DO NOT DO ANYTHING COSTLY HERE! CALCULATE YOUR STUFF AS LATE AS POSSIBLE. This event must be registered in the preInit-Hook definition file config/preInitHooks.yml.

Notable component usages  

-   `Cache` for Page cache initialization
-   `Net` for Domain repository initialization

</div>
</div>
<div style="background:#bcbcbc; padding: 1px; margin:10px 10px 10px 20px;">
<div style="background:#E1E1E1;padding: .5em;">

Loading ClassLoader, EventManager, Env, DB, API and InitCMS (Env, API and InitCMS are deprecated)

</div>
</div>
<div style="background:#bcbcbc; padding: 1px; margin:10px 10px 10px 20px;">

Loading components

<div style="background:#E1E1E1;padding: .5em;">
<div style="background:#52A8FF; padding: 1px; margin:10px 10px 10px 20px;">

<h4>preComponentLoad()</h4>

<div style="background:#A3E0FF;padding: .5em;">

Do something before component load

USE CAREFULLY, DO NOT DO ANYTHING COSTLY HERE! CALCULATE YOUR STUFF AS LATE AS POSSIBLE. This event must be registered in the preComponentLoad-Hook definition file config/preComponentLoadHooks.yml.

</div>
</div>
<div style="background:#6BB521; padding: 1px; margin:10px 10px 10px 20px;">
<div style="background:#E7F2D8;padding: .5em;">

Components are loaded

</div>
</div>
<div style="background:#52A8FF; padding: 1px; margin:10px 10px 10px 20px;">

<h4>registerEvents()</h4>

<div style="background:#A3E0FF;padding: .5em;">

Register your events here

Do not do anything else here than list statements like $this-&gt;cx-&gt;getEvents()-&gt;addEvent($eventName);

</div>
</div>
<div style="background:#52A8FF; padding: 1px; margin:10px 10px 10px 20px;">

<h4>registerEventListeners()</h4>

<div style="background:#A3E0FF;padding: .5em;">

Register your event listeners here

USE CAREFULLY, DO NOT DO ANYTHING COSTLY HERE! CALCULATE YOUR STUFF AS LATE AS POSSIBLE.

Keep in mind, that you can also register your events later. Do not do anything else here than initializing your event listeners and list statements like $his-&gt;cx-&gt;getEvents()-&gt;addEventListener($eventName, $listener);

</div>
</div>
<div style="background:#52A8FF; padding: 1px; margin:10px 10px 10px 20px;">

<h4>postComponentLoad()</h4>

<div style="background:#A3E0FF;padding: .5em;">

Do something after all active components are loaded

USE CAREFULLY, DO NOT DO ANYTHING COSTLY HERE! CALCULATE YOUR STUFF AS LATE AS POSSIBLE.

</div>
</div>
</div>
</div>
<div style="background:#52A8FF; padding: 1px; margin:10px 10px 10px 20px;">

<h4 id="postInit">postInit()</h4>

<div style="background:#A3E0FF;padding: .5em;">

Do something after system initialization

USE CAREFULLY, DO NOT DO ANYTHING COSTLY HERE! CALCULATE YOUR STUFF AS LATE AS POSSIBLE.

</div>
</div>
</div>
</div>
</div>
</div>

<h3>STAGE 3</h3>

[Minimal mode](Minimal mode.md) does not have a stage 3. After initializing Cloudrexx in minimal mode, you may do whatever you want.

<div style="background:#6BB521; padding: 0 1px 1px 1px; margin:5px 0 0 0; width:49%; float:left;">

<h4 id="STAGE_3_front_and_backend">[Frontend](Frontend mode.md) / [Backend](Backend mode.md) Mode</h4>

<div style="background:#E7F2D8;padding: .5em;">
<div style="background:#FFB643; padding: 1px; margin:10px 10px 10px 20px;">

Request parsing

<div style="background:#FCE2B8;padding: .5em;">
<div style="background:#bcbcbc; padding: 1px; margin:10px 10px 10px 20px;">

Resolving

<div style="background:#E1E1E1;padding: .5em;">
<div style="background:#52A8FF; padding: 1px; margin:10px 10px 10px 20px;">

<h5>preResolve()</h5>

<div style="background:#A3E0FF;padding: .5em;">

Do something before resolving is done

USE CAREFULLY, DO NOT DO ANYTHING COSTLY HERE! CALCULATE YOUR STUFF AS LATE AS POSSIBLE

</div>
</div>
<div style="background:#FF8F8F; padding: 1px; margin:10px 10px 10px 20px;">

Resolving

<div style="background:#FFCCCC;padding: .5em;">
<div style="background:#52A8FF; padding: 1px; margin:10px 10px 10px 20px;">

<h5>resolve()</h5>

<div style="background:#A3E0FF;padding: .5em;">

Called for additional, component specific resolving

If /en/Path/to/Page is the path to a page for this component a request like /en/Path/to/Page/with/some/parameters will give an array like array('with', 'some', 'parameters') for $parts

PLEASE MAKE SURE THIS METHOD IS MOCKABLE. IT MAY ONLY INTERACT WITH adjustResponse() HOOK.

This may be used to redirect to another page

</div>
</div>
</div>
</div>
<div style="background:#52A8FF; padding: 1px; margin:10px 10px 10px 20px;">

<h5>postResolve()</h5>

<div style="background:#A3E0FF;padding: .5em;">

Do something after resolving is done

USE CAREFULLY, DO NOT DO ANYTHING COSTLY HERE! CALCULATE YOUR STUFF AS LATE AS POSSIBLE

</div>
</div>
<div style="background:#52A8FF; padding: 1px; margin:10px 10px 10px 20px;">

<h5>adjustResponse()</h5>

<div style="background:#A3E0FF;padding: .5em;">

Called on the resolved component

Do something with the Response object

You may do page alterations here (like changing the metatitle)

You may do response alterations here (like set headers)

PLEASE MAKE SURE THIS METHOD IS MOCKABLE. IT MAY ONLY INTERACT WITH resolve() HOOK.

</div>
</div>
</div>
</div>
</div>
</div>
<div style="background:#FFB643; padding: 1px; margin:10px 10px 10px 20px;">

Content Loading

<div style="background:#FCE2B8;padding: .5em;">
<div style="background:#52A8FF; padding: 1px; margin:10px 10px 10px 20px;">

<h5 id="preContentLoad">preContentLoad()</h5>

<div style="background:#A3E0FF;padding: .5em;">

Do something before content is loaded from DB

USE CAREFULLY, DO NOT DO ANYTHING COSTLY HERE! CALCULATE YOUR STUFF AS LATE AS POSSIBLE

</div>
</div>
<div style="background:#bcbcbc; padding: 1px; margin:10px 10px 10px 20px;">

Load content

<div style="background:#E1E1E1;padding: .5em;">
<div style="background:#52A8FF; padding: 1px; margin:10px 10px 10px 20px;">

<h5>preContentParse()</h5>

<div style="background:#A3E0FF;padding: .5em;">

Do something before a module is loaded

This method is called only if any module gets loaded for content parsing

USE CAREFULLY, DO NOT DO ANYTHING COSTLY HERE! CALCULATE YOUR STUFF AS LATE AS POSSIBLE

</div>
</div>
<div style="background:#52A8FF; padding: 1px; margin:10px 10px 10px 20px;">

<h5 id="load">load()</h5>

<div style="background:#A3E0FF;padding: .5em;">

Load your component. It is needed for this request.

This loads your FrontendController or BackendController depending on the mode Cx runs in. For modes other than frontend and backend, nothing is done. If you you'd like to name your Controllers differently, or have another use case, overwrite this.

</div>
</div>
<div style="background:#52A8FF; padding: 1px; margin:10px 10px 10px 20px;">

<h5>postContentParse()</h5>

<div style="background:#A3E0FF;padding: .5em;">

Do something after a module is loaded

This method is called only if any module gets loaded for content parsing

USE CAREFULLY, DO NOT DO ANYTHING COSTLY HERE! CALCULATE YOUR STUFF AS LATE AS POSSIBLE

</div>
</div>
</div>
</div>
<div style="background:#52A8FF; padding: 1px; margin:10px 10px 10px 20px;">

<h5 id="postContentLoad">postContentLoad()</h5>

<div style="background:#A3E0FF;padding: .5em;">

Do something after content is loaded from DB

USE CAREFULLY, DO NOT DO ANYTHING COSTLY HERE! CALCULATE YOUR STUFF AS LATE AS POSSIBLE

</div>
</div>
</div>
</div>
<div style="background:#FFB643; padding: 1px; margin:10px 10px 10px 20px;">

Finalizing

<div style="background:#FCE2B8;padding: .5em;">
<div style="background:#52A8FF; padding: 1px; margin:10px 10px 10px 20px;">

<h5>preFinalize()</h5>

<div style="background:#A3E0FF;padding: .5em;">

Do something before main template gets parsed

USE CAREFULLY, DO NOT DO ANYTHING COSTLY HERE! CALCULATE YOUR STUFF AS LATE AS POSSIBLE

</div>
</div>
<div style="background:#bcbcbc; padding: 1px; margin:10px 10px 10px 20px;">
<div style="background:#E1E1E1;padding: .5em;">

Parses the main template in order to finish request

</div>
</div>
<div style="background:#52A8FF; padding: 1px; margin:10px 10px 10px 20px;">

<h5>postFinalize()</h5>

<div style="background:#A3E0FF;padding: .5em;">

Do something after main template got parsed

USE CAREFULLY, DO NOT DO ANYTHING COSTLY HERE!

</div>
</div>
<div style="background:#bcbcbc; padding: 1px; margin:10px 10px 10px 20px;">
<div style="background:#E1E1E1;padding: .5em;">

Output generated response to client

</div>
</div>
</div>
</div>
<div style="background:#FFB643; padding: 1px; margin:10px 10px 10px 20px;">

Shutdown

<div style="background:#FCE2B8;padding: .5em;">
<div style="background:#bcbcbc; padding: 1px; margin:10px 10px 10px 20px;">
<div style="background:#E1E1E1;padding: .5em;">

Close session

</div>
</div>
<div style="background:#bcbcbc; padding: 1px; margin:10px 10px 10px 20px;">
<div style="background:#E1E1E1;padding: .5em;">

Flush system log

</div>
</div>
</div>
</div>
</div>
</div>
<div style="background:#6BB521; padding: 0 1px 1px 1px; margin:5px 0 0 0; width:49%; float:right;">

<h4 id="STAGE_3_command">Command mode</h4>

<div style="background:#E7F2D8;padding: .5em;">
<div style="background:#FFB643; padding: 1px; margin:10px 10px 10px 20px;">

Request parsing

<div style="background:#FCE2B8;padding: .5em;">
<div style="background:#bcbcbc; padding: 1px; margin:10px 10px 10px 20px;">

Resolving

<div style="background:#E1E1E1;padding: .5em;">
<div style="background:#52A8FF; padding: 1px; margin:10px 10px 10px 20px;">

<h5>getCommandsForCommandMode()</h5>

<div style="background:#A3E0FF;padding: .5em;">

Do something before resolving is done

USE CAREFULLY, DO NOT DO ANYTHING COSTLY HERE! CALCULATE YOUR STUFF AS LATE AS POSSIBLE

</div>
</div>
<div style="background:#52A8FF; padding: 1px; margin:10px 10px 10px 20px;">

<h5>hasAccessToExecuteCommand()</h5>

<div style="background:#A3E0FF;padding: .5em;">

Do something before resolving is done

USE CAREFULLY, DO NOT DO ANYTHING COSTLY HERE! CALCULATE YOUR STUFF AS LATE AS POSSIBLE

</div>
</div>
</div>
</div>
<div style="background:#bcbcbc; padding: 1px; margin:10px 10px 10px 20px;">

Content generation

<div style="background:#E1E1E1;padding: .5em;">
<div style="background:#52A8FF; padding: 1px; margin:10px 10px 10px 20px;">

<h5>getCommandDescription()</h5>

<div style="background:#A3E0FF;padding: .5em;">

Do something before resolving is done

USE CAREFULLY, DO NOT DO ANYTHING COSTLY HERE! CALCULATE YOUR STUFF AS LATE AS POSSIBLE

</div>
</div>
<div style="background:#52A8FF; padding: 1px; margin:10px 10px 10px 20px;">

<h5>executeCommand()</h5>

<div style="background:#A3E0FF;padding: .5em;">

Do something before resolving is done

USE CAREFULLY, DO NOT DO ANYTHING COSTLY HERE! CALCULATE YOUR STUFF AS LATE AS POSSIBLE

</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
