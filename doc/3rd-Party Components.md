This article covers some topics about Cloudrexx components that are not developed by Cloudrexx AG itself but are (or should get) published to the Cloudrexx cloud system. These topics are not necessarily applicable for components running on on-premise installations.

## Basics

The basic process is simple: You develop a component, Cloudrexx AG reviews it to ensure stability of the live system and publishes the code to the Cloudrexx cloud.

Please note that there may be charges for review and operation of your component and that review dates must be scheduled in advance.

We strongly recommend to contact us as early as possible, ideally during the planning phase of your component. This way we can assist you during planning to simplify development and coordinate time schedules.

Please also note that there are requirements for such components. Those include:

-   Follow our [development guidelines](Guidelines.md). This simplifies review and maintenance.
-   Always use framework functionalities where applicable. Especially file system and network operations MUST be routed through the [file system abstraction](FileSystem.md) and [network abstraction](Network.md) layer.

## Live system state

### Current state

Currently the Cloudrexx cloud runs on:

-   Linux
-   PHP 8.2
    -   libmemcached 1.0.18
    -   Zend OPCache
-   MariaDB 10.6
-   Apache 2.4
-   Memcached 1.4.15

### Planned updates

There are currently plans to change the following:

-   Upgrade to PHP 8.1
-   Source out user data to an external storage

## Review

You can submit your component on the following site for review: <https://cloudrexx.atlassian.net/servicedesk/customer/portal/5/group/24/create/78>

### Common Review Issues

-   Source code must be written in en-US (class/method/property names, inline comments, docblocks, etc.). The source code must not contain any interface text, except the language-files themselves in the component's */lang* directory. Exception to this policy are exception messages (if not displayed to the user in normal operation) and command mode command descriptions and code.
-   The line containing the closing bracket must have the same indention as the line containing the opening bracket.
-   Distinguish between code (/codebase) data and user (/website) data when reading or writing to or from file-system.
-   Remove unnecessary files and folders from your component. This includes empty Doctrine repositories.
-   If your component has a model, its ERD must be included in the component's */Doc* directory.

