There are three different approaches on how the system can be customized without touching the original source code and ensure a future update to a newer version will be painless. Those approaches are:

| Scope                                       | Impact                  | Purpose                                                                                                                                                                                                                                          | Implementation                                        |
|---------------------------------------------|-------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------|
| Global  | Whole system            | Global customizings can be used to extend existing functionality on applications or implement completely new applications.                                                                                                                       | Customizing folder /customizing                       |
| Website | Only a specific website | In a MultiSite environment it is possible to implement customizings that are specific to a particular website. Website specific customizings can be used to alter a particular application only for a single website in a MultiSite environment. | Data repository folder of website /websites/&lt;website&gt; |
| Theme    | Only a specific theme   | Theme specific customizings can be used adjust the frontend view of applications to match the layout of a particular design theme.                                                                                                               | Design theme folder /themes/&lt;theme&gt;                   |

## Global Customizings

### Basic

Generally all files can be copied to the customizing directory in due consideration of the folder structure.

So a whole shop module could be copied to the folder /customizing/modules and the shop will still work. Requirements for a working Customizing are:

### Requirements

-   Customizing-folder has to be defined (default is /customizing)
-   Customizings have to be activated (default is deactivated, option "Customizing" under "Global Configuration" - "Other configuration options" in Cloudrexx administration)
-   The file is a PHP file or will be loaded from the template system or from a JS class
-   The file will not be called directly (e.g. /index.php, /cadmin/index.php, /core/License/versioncheck.php, and so on)
-   The file has to be in the same folder (from /modules/shop/index.class.php to e.g. /customizing/modules/shop/index.class.php)

### Constraints

This feature does not work for these cases:

-   Javascript files included in template files (can be loaded with JS::activate())
-   Directly called files (index.php, and so on)
-   Manually included php files (via require(_once) or include(_once)) without \Env::get('ClassLoader')->loadFile($path);
-   Images and other resources (can be included on php side. The following method will return the absolute path to the requested file: \Env::get('ClassLoader')->getFilePath($path);)

See the [ClassLoader article](ClassLoader.md) for detailed information about ClassLoaders.

## Website Customizings

Generally all files can be copied to the data repository directory of a website in due consideration of the folder structure.

So a whole shop module could be copied to the folder /websites/&lt;website&gt;/modules and the shop will still work.

## Theme Customizings

Generally all files from the *View*-layer (files located in the *View* folder of a Component) can be copied to a Theme folder in due consideration of the folder structure, but by omitting the *View*-folder itself.

I.e. a set of customized images of the Shop application can be stored in /themes/&lt;theme&gt;/modules/Shop/Media/