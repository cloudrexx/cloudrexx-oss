What is a **Nested Set**? See <https://en.wikipedia.org/wiki/Nested_set_model>

## Integration in Cloudrexx

Cloudrexx provides two methods/libraries to implement a Nested Set. Depending on the database driver that shall be used you would use one or the other.

| Method | Name                                                                                   | Compatible DB Driver | Documentation                                                                                                                                                                                                                                                         | Reference Implementations                         |
|--------|----------------------------------------------------------------------------------------|----------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------|
| Legacy | [DB_NestedSet](https://pear.php.net/package/DB_NestedSet/docs)                          | AdoDb                | <ul><li> [Example](https://pear.php.net/manual/en/package.DB_NestedSet.php) </li><li> [Main documentation](https://pear.php.net/package/DB_NestedSet/docs) </li><li> [A Lengthier Introduction to DB_NestedSet](https://jystewart.net/2005/03/02/a-lengthier-introduction-to-db_nestedset/) </li></ul> | <ul><li> Model **Category** of component **News** </li></ul>      |
| ORM    | [Gedmo\Tree](https://github.com/Atlantic18/DoctrineExtensions/blob/master/doc/tree.md) | Doctrine             | <ul><li> [Main documentation](https://github.com/Atlantic18/DoctrineExtensions/blob/master/doc/tree.md)          </li></ul>                                                                                                                                                           | <ul><li> Model **Node** of component **ContentManager** </li></ul> |

