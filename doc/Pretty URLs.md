This article describes how pretty URLs can be used in components.

## Resolving

If we have a page of type "application" with application set to "MyApp" at /en/MyAppPage we can add arbitrary path parts (as long as they do not match an existing sub-page's slug) and resolver will still resolve to the same page.

If we open /en/MyAppPage/Foo/Bar, resolver will resolve this to /en/MyAppPage and call "MyApp"'s resolve($parts, $page) method. $page will be set to the page object at /en/MyAppPage and $parts will be set to array('Foo', 'Bar').

This can be used to resolve pretty URLs in components.

## How to create slugs

Whenever possible, use Doctrine sluggable behavior to generate unique slugs. If this can not be use for any reason, use the following snippet instead:

``` php
// This just replaces non-URL characters. You need to ensure uniqueness yourself!
$slug = $this->cx->getComponent('Model')->slugify($title);
```

## Canonical URL

Cloudrexx automatically sets a canonical header for most pages. For application pages this can not be done automatically. Therefore all components should set the canonical URL for all their pages in adjustResponse():

``` php
public function adjustResponse(\Cx\Core\Routing\Model\Entity\Response $response) {
    // TODO: set $canonicalUrl to correct instance of \Cx\Core\Routing\Url
    $response->setHeader(
        'Link',
        '<' . $canonicalUrl->toString() . '>; rel="canonical"
    );
}
```

## Language navigation

Cloudrexx automatically generates a language navigation which allows to switch between the different languages of a page. For pretty URLs there is no way to detect which pretty URL relates to which other pretty URL for another language. Therefore components using pretty URLs need to populate this information.

This is done by adding [virtual pages](Virtual Pages.md) for at least all available language versions of the currently resolved pretty URL:

``` php
public function adjustResponse(\Cx\Core\Routing\Model\Entity\Response $response) {
    // TODO: Create an array with all current page slugs and their locales
    // Format: $pageInfo = array('<pageSlug>' => '<localeObject>', ...);
    // TODO: Select correct parent page as $page
    $em = $this->cx->getDb()->getEntityManager();
    $nodeRepo = $em->getRepository('Cx\Core\ContentManager\Model\Entity\Node');
    $nodeRepo->addVirtualTranslatedNode($page->getNode(), $pageInfo);
}
```

## ESI Widgets

ESI Widgets which need to know about the resolved pretty URL should set the following ESI parameters: ESI_VAR_PAGE and ESI_VAR_ADDITIONAL_PATH. For more info, see [Widgets](Widgets.md).

