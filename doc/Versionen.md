Für den On-Premise Betrieb steht eine [Docker-Umgebung](https://bitbucket.org/cloudrexx/cloudrexx-oss/src/main/readme.md) bereit.

<table>
<thead>
<tr class="header">
<th><p>Release</p></th>
<th><p>Version</p></th>
<th><p>Code Name</p></th>
<th><p>Release Date</p></th>
<th><p>min. PHP</p></th>
<th><p>max. PHP</p></th>
<th><p>min. MySQL / MariaDB</p></th>
<th><p>max. MySQL / MariaDB</p></th>
<th><p>Support Ende</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="version_5.1.1" title="wikilink">5.1 SP1</a></p></td>
<td><p>5.1.1</p></td>
<td rowspan="2"><p>Beezer</p></td>
<td><p>26.03.2021</p></td>
<td rowspan="2"><p>7.3.0</p></td>
<td rowspan="2"><p>7.4</p></td>
<td rowspan="2"><p>5.6 / 10.2 <a href="#fn1" class="footnote-ref" id="fnref1" role="doc-noteref"><sup>1</sup></a></p></td>
<td rowspan="2"><p>8.0 / 10.5</p></td>
<td style="background-color: #A0E75A"><p>31.12.2022</p></td>
</tr>
<tr class="even">
<td><p><a href="version_5.1.0" title="wikilink">5.1</a></p></td>
<td><p>5.1.0</p></td>
<td><p>15.01.2021</p></td>
<td style="background-color: #FA735D"><p>31.12.2021</p></td>
</tr>
<tr class="odd">
<td><p><a href="version_5.0.2" title="wikilink">5.0 SP2</a></p></td>
<td><p>5.0.2</p></td>
<td rowspan="3"><p>Nandri</p></td>
<td><p>12.12.2018</p></td>
<td rowspan="3"><p>7.0.0</p></td>
<td rowspan="3"><p>7.2</p></td>
<td rowspan="3"><p>5.6 / 10.0</p></td>
<td rowspan="3"><p>5.7 <a href="#fn2" class="footnote-ref" id="fnref2" role="doc-noteref"><sup>2</sup></a> / 10.1 <a href="#fn3" class="footnote-ref" id="fnref3" role="doc-noteref"><sup>3</sup></a></p></td>
<td rowspan="3" style="background-color:#FA735D; vertical-align:top;"><p>31.12.2021</p></td>
</tr>
<tr class="even">
<td><p><a href="version_5.0.1" title="wikilink">5.0 SP1</a></p></td>
<td><p>5.0.1</p></td>
<td><p>23.10.2018</p></td>
</tr>
<tr class="odd">
<td><p><a href="version_5.0.0" title="wikilink">5.0</a></p></td>
<td><p>5.0.0</p></td>
<td><p>05.10.2018</p></td>
</tr>
<tr class="even">
<td><p><a href="version_4.0.0" title="wikilink">4.0</a></p></td>
<td><p>4.0.0</p></td>
<td><p>Eric S. Raymond</p></td>
<td><p>17.12.2014</p></td>
<td rowspan="9"><p>5.3.0</p></td>
<td rowspan="9"><p>5.5 <a href="#fn4" class="footnote-ref" id="fnref4" role="doc-noteref"><sup>4</sup></a></p></td>
<td rowspan="9"><p>5.0 / 5.5</p></td>
<td rowspan="9"><p>5.6 <a href="#fn5" class="footnote-ref" id="fnref5" role="doc-noteref"><sup>5</sup></a> / 10.0 <a href="#fn6" class="footnote-ref" id="fnref6" role="doc-noteref"><sup>6</sup></a></p></td>
<td rowspan="9" style="background-color:#FA735D; vertical-align:top;"><p>31.12.2018</p></td>
</tr>
<tr class="odd">
<td><p><a href="version_3.2.0" title="wikilink">3.2</a></p></td>
<td><p>3.2.0</p></td>
<td><p>Speedy Gonzales</p></td>
<td><p>07.07.2014</p></td>
</tr>
<tr class="even">
<td><p><a href="version_3.1.1" title="wikilink">3.1 SP1</a></p></td>
<td><p>3.1.1</p></td>
<td rowspan="2"><p>Libertas</p></td>
<td><p>23.12.2013</p></td>
</tr>
<tr class="odd">
<td><p><a href="version_3.1.0" title="wikilink">3.1</a></p></td>
<td><p>3.1.0</p></td>
<td><p>01.10.2013</p></td>
</tr>
<tr class="even">
<td><p><a href="version_3.0.4" title="wikilink">3.0 SP4</a></p></td>
<td><p>3.0.4</p></td>
<td rowspan="5"><p>Nikola Tesla</p></td>
<td><p>30.05.2013</p></td>
</tr>
<tr class="odd">
<td><p><a href="version_3.0.3" title="wikilink">3.0 SP3</a></p></td>
<td><p>3.0.3</p></td>
<td><p>12.04.2013</p></td>
</tr>
<tr class="even">
<td><p><a href="version_3.0.2" title="wikilink">3.0 SP2</a></p></td>
<td><p>3.0.2</p></td>
<td><p>19.02.2013</p></td>
</tr>
<tr class="odd">
<td><p><a href="version_3.0.1" title="wikilink">3.0 SP1</a></p></td>
<td><p>3.0.1</p></td>
<td><p>17.01.2013</p></td>
</tr>
<tr class="even">
<td><p>3.0</p></td>
<td><p>3.0.0</p></td>
<td><p>12.11.2012</p></td>
</tr>
<tr class="odd">
<td><p><a href="version_2.2.6" title="wikilink">2.2 SP6</a></p></td>
<td><p>2.2.6</p></td>
<td rowspan="7"><p>Forecast: Cloudy</p></td>
<td><p>09.02.2012</p></td>
<td rowspan="16"><p>5.2.0</p></td>
<td rowspan="16"><p>5.3 <a href="#fn7" class="footnote-ref" id="fnref7" role="doc-noteref"><sup>7</sup></a></p></td>
<td rowspan="16"><p>4.1.2 / 5.5</p></td>
<td rowspan="16"><p>5.5 / 5.5</p></td>
<td rowspan="48" style="background-color:#FA735D; vertical-align:top;"><p>31.12.2013</p></td>
</tr>
<tr class="even">
<td><p><a href="version_2.2.5" title="wikilink">2.2 SP5</a></p></td>
<td><p>2.2.5</p></td>
<td><p>07.11.2011</p></td>
</tr>
<tr class="odd">
<td><p><a href="version_2.2.4" title="wikilink">2.2 SP4</a></p></td>
<td><p>2.2.4</p></td>
<td><p>14.08.2011</p></td>
</tr>
<tr class="even">
<td><p><a href="version_2.2.3" title="wikilink">2.2 SP3</a></p></td>
<td><p>2.2.3</p></td>
<td><p>08.07.2011</p></td>
</tr>
<tr class="odd">
<td><p><a href="version_2.2.2" title="wikilink">2.2 SP2</a></p></td>
<td><p>2.2.2</p></td>
<td><p>31.05.2011</p></td>
</tr>
<tr class="even">
<td><p><a href="version_2.2.1" title="wikilink">2.2 SP1</a></p></td>
<td><p>2.2.1</p></td>
<td><p>15.05.2011</p></td>
</tr>
<tr class="odd">
<td><p><a href="version_2.2.0" title="wikilink">2.2</a></p></td>
<td><p>2.2.0</p></td>
<td><p>25.04.2011</p></td>
</tr>
<tr class="even">
<td><p>2.1 SP5</p></td>
<td><p>2.1.5</p></td>
<td rowspan="6"><p>Oscar</p></td>
<td><p>07.03.2011</p></td>
</tr>
<tr class="odd">
<td><p>2.1 SP4</p></td>
<td><p>2.1.4</p></td>
<td><p>12.01.2011</p></td>
</tr>
<tr class="even">
<td><p>2.1 SP3</p></td>
<td><p>2.1.3</p></td>
<td><p>22.01.2010</p></td>
</tr>
<tr class="odd">
<td><p>2.1 SP2</p></td>
<td><p>2.1.2</p></td>
<td><p>06.08.2009</p></td>
</tr>
<tr class="even">
<td><p>2.1 SP1</p></td>
<td><p>2.1.1</p></td>
<td><p>27.05.2009</p></td>
</tr>
<tr class="odd">
<td><p>2.1</p></td>
<td><p>2.1.0</p></td>
<td><p>19.03.2009</p></td>
</tr>
<tr class="even">
<td><p>2.0 SP2</p></td>
<td><p>2.0.2</p></td>
<td rowspan="3"><p>Calling Elvis</p></td>
<td><p>04.11.2008</p></td>
</tr>
<tr class="odd">
<td><p>2.0 SP1</p></td>
<td><p>2.0.1</p></td>
<td><p>14.08.2008</p></td>
</tr>
<tr class="even">
<td><p>2.0</p></td>
<td><p>2.0.0</p></td>
<td><p>17.06.2008</p></td>
</tr>
<tr class="odd">
<td><p>1.2 SP3</p></td>
<td><p>1.2.3</p></td>
<td rowspan="4"><p>Cow Feeder</p></td>
<td><p>20.04.2008</p></td>
<td rowspan="32"><p>4.3.1</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1.2 SP2</p></td>
<td><p>1.2.2</p></td>
<td><p>02.04.2008</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1.2 SP1</p></td>
<td><p>1.2.1</p></td>
<td><p>29.03.2008</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1.2</p></td>
<td><p>1.2.0</p></td>
<td><p>09.01.2008</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1.1 SP3</p></td>
<td><p>1.1.3</p></td>
<td rowspan="4"><p>Paris Hilton</p></td>
<td><p>23.05.2007</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1.1 SP2</p></td>
<td><p>1.1.2</p></td>
<td><p>16.05.2007</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1.1 SP1</p></td>
<td><p>1.1.1</p></td>
<td><p>10.05.2007</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1.1</p></td>
<td><p>1.1.0</p></td>
<td><p>03.05.2007</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1.0 SP9.10.1</p></td>
<td><p>1.0.9.10.1</p></td>
<td rowspan="14"><p>Let's rock IT</p></td>
<td><p>15.11.2006</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1.0 SP9.10</p></td>
<td><p>1.0.9.10</p></td>
<td><p>14.11.2006</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1.0 SP9.9.1</p></td>
<td><p>1.0.9.9.1</p></td>
<td><p>19.10.2006</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1.0 SP9.9</p></td>
<td><p>1.0.9.9</p></td>
<td><p>18.10.2006</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1.0 SP9.8</p></td>
<td><p>1.0.9.8</p></td>
<td><p>21.09.2006</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1.0 SP9.7</p></td>
<td><p>1.0.9.7</p></td>
<td><p>05.09.2006</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1.0 SP9.6</p></td>
<td><p>1.0.9.6</p></td>
<td><p>23.08.2006</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1.0 SP9.5</p></td>
<td><p>1.0.9.5</p></td>
<td><p>15.08.2006</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1.0 SP9.4.1</p></td>
<td><p>1.0.9.4.1</p></td>
<td><p>09.08.2006</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1.0 SP9.4</p></td>
<td><p>1.0.9.4</p></td>
<td><p>08.08.2006</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1.0 SP9.3</p></td>
<td><p>1.0.9.3</p></td>
<td><p>07.08.2006</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1.0 SP9.2</p></td>
<td><p>1.0.9.2</p></td>
<td><p>06.08.2006</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1.0 SP9.1</p></td>
<td><p>1.0.9.1</p></td>
<td><p>01.08.2006</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1.0 SP9</p></td>
<td><p>1.0.9</p></td>
<td><p>21.07.2006</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1.0 SP8</p></td>
<td><p>1.0.8</p></td>
<td><p>Pimp My CMS</p></td>
<td><p>01.02.2006</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1.0 SP7</p></td>
<td><p>1.0.7</p></td>
<td rowspan="2"><p>Amazonas</p></td>
<td><p>05.01.2006</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1.0 SP6</p></td>
<td><p>1.0.6</p></td>
<td><p>10.10.2005</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1.0 SP5</p></td>
<td><p>1.0.5</p></td>
<td><p>Romina</p></td>
<td><p>19.07.2005</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1.0 SP4a</p></td>
<td><p>1.0.4a</p></td>
<td rowspan="6"><p>contreXX</p></td>
<td><p>10.07.2005</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1.0 SP4</p></td>
<td><p>1.0.4</p></td>
<td><p>27.06.2005</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1.0 SP3</p></td>
<td><p>1.0.3</p></td>
<td><p>10.06.2005</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1.0 SP2</p></td>
<td><p>1.0.2</p></td>
<td><p>12.05.2005</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1.0 SP1</p></td>
<td><p>1.0.1</p></td>
<td><p>14.04.2005</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1.0</p></td>
<td><p>1.0.0</p></td>
<td><p>04.04.2005</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td colspan="5"><div style="float: left; margin-left: 1em;">
<p><small><span title="Old version, no longer supported" style="border-left: 1.2em solid #FA735D; padding-left: 0.3em; white-space: nowrap;">Old version</span></small></p>
</div>
<div style="float: left; margin-left: 1em;">
<p><small><span title="An older version, yet still supported" style="border-left: 1.2em solid #FCED77; padding-left: 0.3em; white-space: nowrap;">Older version, still supported</span></small></p>
</div>
<div style="float: left; margin-left: 1em;">
<p><small><span title="Latest stable version" style="border-left: 1.2em solid #A0E75A; padding-left: 0.3em; white-space: nowrap;"><b>Latest version</b></span></small></p>
</div>
<div style="float: left; margin-left: 1em; display: none;">
<p><small><span title="Latest preview of a future release" style="border-left: 1.2em solid #FCB058; padding-left: 0.3em; white-space: nowrap;">Latest preview version</span></small></p>
</div>
<div style="float: left; margin-left: 1em;">
<p><small><span title="A future release" style="border-left: 1.2em solid skyBlue; padding-left: 0.3em; white-space: nowrap;">Future release</span></small></p>
</div></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>
<section id="footnotes" class="footnotes footnotes-end-of-document" role="doc-endnotes">
<hr />
<ol>
<li id="fn1">Might also work with MariaDB 10.1 and older. Only partially tested.<a href="#fnref1" class="footnote-back" role="doc-backlink">↩︎</a></li>
<li id="fn2">Might also work with MySQL 8. Only partially tested. Please note PHP's support of caching_sha2_password <a href="https://php.net/manual/en/mysqli.requirements.php">1</a>.<a href="#fnref2" class="footnote-back" role="doc-backlink">↩︎</a></li>
<li id="fn3">Might also work with MariaDB 10.2 and 10.3. Only partially tested.<a href="#fnref3" class="footnote-back" role="doc-backlink">↩︎</a></li>
<li id="fn4">Also works with PHP 5.6 if the patches listed in <a href="https://support.cloudrexx.com/support/solutions/articles/5000659864-upgrade-auf-php-5-6">Upgrade auf PHP 5.6</a> are applied.<a href="#fnref4" class="footnote-back" role="doc-backlink">↩︎</a></li>
<li id="fn5">Also works with MySQL 5.7 / MariaDB 10.1 if the patches <a href="https://bugs.cloudrexx.com/cloudrexx/ticket/2781">#2781</a> and <a href="https://bugs.cloudrexx.com/cloudrexx/ticket/2796">#2796</a> are applied.<a href="#fnref5" class="footnote-back" role="doc-backlink">↩︎</a></li>
<li id="fn6"></li>
<li id="fn7">Might work with PHP up to 5.6 using patches listed in <a href="https://support.cloudrexx.com/support/solutions/articles/13000003183-probleme-mit-php-5-4-5-5">Probleme mit PHP 5.4 / 5.5</a><a href="#fnref7" class="footnote-back" role="doc-backlink">↩︎</a></li>
</ol>
</section>

