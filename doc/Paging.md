The Cloudrexx Framework provides a function to automatically create a uniform paging.

![Paging](img/Paging_example.png)

## Initiate paging
``` php
return \Paging::get(
    string $uri_parameter,
    string $paging_text,
    int $numof_rows,
    ?int $results_per_page = 0,
    bool $showeverytime = false,
    ?int $position = null,
    ?string $parameter_name = null,
    bool $addCsrfToken = true,
    int $viewGeneratorId = -1,
    bool $useSession = false
);
```

The parameters mean the following:

Parameter | Description
------| -----------
`string $uri_paramter` | Additional URI parameters, *MUST* start with an URI encoded ampersand (&amp;). Can be left blank.
`string $paging_text` | The text to be put in front of the paging. Can be left blank.
`int $numof_rows` | The number of rows available
`int? $results_per_page = 0` | The optional maximum number of rows to be shown on a single page. Defaults to the corePagingLimit setting.
`bool $showeverytime = false` | If true, the paging is shown even if $numof_rows is less than $results_per_pageIndicates whether it should be displayed even if it is not necessary. Always true for backend paging.
`?int $position = null` | The optional starting position offset.
`?string $parameter_name = null` | The optional name for the URI parameter.  Will be determined automatically if empty.
`bool $addCsrfToken = true` | Set to `false` (defaults to `true`) to not inject a CSRF token into the paging URLs. This might be useful when the paging will be used over AJAX where CSRF is not required.
`int $viewGeneratorId = -1` | The ID of the VG to generate URIs for.
`bool $useSession = false` | Whether to use session to save position. Defaults to false.

The function returns a string with the HTML code for the paging.

## Difference between front- and backend
The framework automatically distinguishes between front- and backend. The
backend paging is a bit more modern and built and styled differently.

## Paging via XHR
If you load content via XHR and need paging the input field to jump to a page
does not automatically reload the page with the correct parameter. Instead an
event is triggered so you can handle the correct action yourself. Example:

``` js
cx.bind(
    "jumpToPage",
    function(args) {
        // args.offset contains the offset to jump to
        // args.el contains the element that triggered the jump
    },
    "paging"
);
```
