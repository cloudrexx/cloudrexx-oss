Virtual pages are pages (and nodes) which only exist for the runtime, they won't be persisted. The feature exists since Contrexx version 3.

## Sense

The virtual page enables each module (and all code parts in Contrexx) to extend the navigation tree. This is useful to e.g. show categories of the mediadir module in the navigation or to generate a content page from a calendar event, without accessing the content manager within the calendar module. Virtual pages are used for the page preview in the content manager.

## Implementation

### Basics

The virtual pages will only be added to the tree, they are not in the repository, but they are accessible with the methods `PageRepository->getTreeByTitle()` and `PageRepository->getPagesAtPath()`.

To add a new page on the root level the following code is enough:

``` php
// New page and node
$page = new \Cx\Model\ContentManager\Page();
  
// Get node to insert our page before (optional)
$beforeNode = $this->nodeRepo->findOneBy(array('id'=>2));
  
// Add virtual page before $beforeNode
$this->pageRepo->addVirtualPage($page, $beforeNode->getPage(BACKEND_LANG_ID)->getSlug());
```

To add a new page on another level the following code is the right one:

``` php
// New page and node
$page = new \Cx\Model\ContentManager\Page();
$node = new \Cx\Model\ContentManager\Node();
  
// Add node to a node
$node->setParent($this->nodeRepo->findOneBy(array('id'=>1));
  
// Add page to the node
$page->setNode($node);

// Get node to insert our page before (optional)
$beforeNode = $this->nodeRepo->findOneBy(array('id'=>2));
  
// Add virtual page before $beforeNode
$this->pageRepo->addVirtualPage($page, $beforeNode->getPage(BACKEND_LANG_ID)->getSlug());
```

For the new page there are some requirements to fit:

-   the page must have a related node
-   the node must have knowledge of the new page (both calls are necessary, `$page->setNode()` and `$node->addPage()`)
-   the node must have a parent node from the repository (in the example up above, the root node). Here are two calls necessary too, `setParent()` and `addChildren()`

Apart from that the page can be modified however you want. The call `$page->setVirtual(false)` will everytime result in an Exception for a virtual page.

### Position

With help of the optional parameters `$beforeSlug` of the method `addVirtualPage()`, the page can be positioned within the current level. If the parameter `$beforeSlug` is set, the new virtual page will be inserted in front of the page with the provided slug. If there is no page with the given slug, the new virtual page will be added at the end of the level.

