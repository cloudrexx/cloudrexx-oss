The `YamlRepository` is a YAML-file based alternative to the database based [Doctrine Entity repository](Doctrine.md).

## Implementation

### Model

Models that shall be managed by a `YamlRepository` must extend the base model `\Cx\Core\Model\Model\Entity\YamlEntity` and must be stored in the folder `/Model/Entity` of the associated component.

!!! example

    The model for the entity `Foo` would be stored as `modules/MyComponent/Model/Entity/Foo.class.php` and would consist of the following base code:

    ``` php
    <?php

    namespace Cx\Modules\MyComponent\Model\Entity;

    class Foo extends \Cx\Core\Model\Model\Entity\YamlEntity {}
    ```

### Repository

Entity repositories must extend `\Cx\Core\Model\Controller\YamlRepository`, must be stored in the folder `/Model/Repository` of the associated component and must follow the naming scheme `<Entity>Repository`.

!!! example

    The repository for the entity `Foo` would be stored as `modules/MyComponent/Model/Repository/FooRepository.class.php` and would consist of the following base code:

    ``` php
    <?php

    namespace Cx\Modules\MyComponent\Model\Repository;

    class FooRepository extends \Cx\Core\Model\Controller\YamlRepository {

        public function __construct() {
            $cx = \Cx\Core\Core\Controller\Cx::instanciate();
            parent::__construct($cx->getWebsiteConfigPath() . '/FooRepository.yml');
        }
    }
    ```

## Usage

Working with entities from a `YamlRepository` is almost identical with entities originating from a [Doctrine Entity repository](Doctrine.md).

### Initialization

The initialization of a models repository can either be done directly by instantiating the repository or through the Entity Manager.

#### Direct Repository Instantiation

``` php
$repo = new \Cx\Modules\MyComponent\Model\Repository\FooRepository();
```

#### Repository from Entity Manager

A `YamlRepository` can also be fetched through the [Entity Manager](Doctrine.md):

``` php
$repo = $this->cx->getDb()->getEntityManager()->getRepository(
    'Cx\\Modules\\MyComponent\\Model\\Entity\\Foo'
);
```

!!! note

    This only works after the database layer has been initialized.

### Fetching Entities

Analogous to the [Doctrine Entity repository](Doctrine.md), the `YamlRepository` provides the following methods for fetching entities:

-   `findAll()`
-   `find($id)`
-   `findBy($criteria)`

#### Examples

Fetch entity having ID 3  

``` php
$repo = new \Cx\Modules\MyComponent\Model\Repository\FooRepository();
$foo = $repo->find(3);
```

Fetch entities by a specific property  

``` php
$repo = new \Cx\Modules\MyComponent\Model\Repository\FooRepository();
$entity = $repo->findBy(
    ['location' => 'Bar']
);
```

### Persisting Entities

New entities are added to the repository through method `add()`:

``` php
$foo = new \Cx\Modules\MyComponent\Model\Entity\Foo();
$repo->add($foo);
$repo->flush();
```

!!! note

    See Initialization on how to fetch `$repo`.

!!! warning 

    To persist new entities in the file system, the repository must be flushed using the method `flush()`.

### Updating Entities

To persist any changes made to the entities of a repository, the repository must be flushed to the file system by calling `flush()`:

``` php
$foo->setLocation('Bar');
$repo->flush();
```

!!! note

    See Initialization on how to fetch `$repo`.

### Checking for existing entities

Use method `isManaged()` to check if an entity is already managed by a repository:

``` php
$foo->setLocation('Bar');
if (!$repo->isManaged($foo)) {
    $repo->add($foo);
}
$repo->flush();
```

!!! note
    
    See Initialization on how to fetch `$repo`.

### Removing Entities

Existing entities are removed from the repository through method `remove()`:

``` php
$foo = $repo->find($id);
$repo->remove($foo);
$repo->flush();
```

!!! note

    See Initialization on how to fetch `$repo`.

!!! warning

    To permanently remove entities from the file system, the repository must be flushed using the method `flush()`.