This page describes the functionalities and the used library around the 2FA login. The 2FA is not implemented yet!

## What ist 2FA?

2FA provides an additional layer of security for an user account. It ensures that only the given user can access the account, even if someone else knows the password.

For more informations, feel free to read through this article: <https://en.wikipedia.org/wiki/Wikipedia:Simple_2FA>

## Implemented library

To implement the extension of the 2FA, the following library was used:

-   Release-Version: 1.6.5
-   Library: <https://github.com/RobThree/TwoFactorAuth>

As usual, the library is implemented at the following location:

-   lib
    -   \TwoFactorAuthentication

Wiki-Article &gt; [Libraries](3rd-Party Libraries.md)

### Usage

First of all, some tipps that should help you to get started:

``` php
//Create a new TwoFactorAuth instance

$tfa = new RobThree\Auth\TwoFactorAuth('Cloudrexx Test');
```

The constructor acceppts the following params:

| Param           | Default value | Use                                                    |
|-----------------|---------------|--------------------------------------------------------|
| $issuer         | null          | This name will be shown in the 2FA-App of the customer |
| $digits         | 6             | The number of digits the codes will be                 |
| $period         | 30            | The number of seconds a generated code will be valid   |
| $algorithm      | sha1          | The algorithm used to hash                             |
| $qrcodeprovider | null          | The QR-Code provider                                   |
| $rngprovider    | null          | The number generator provider                          |
| $timeprovider   | null          | The time provider                                      |

For sure you can overwrite those default values, but you have to be sure, that the authenticator app supports those changes.

Now we can create a secret for an user. This method accepts two arguments $bits (d:80) and $requirecryptosecure (d:true). Bits are the number of bits generated for the secret. Make sure it is a multiple of 8. The final argument is used to ensure the secret cryptographically.

``` php
//Create a secret for an user

$secret = $tfa->createSecret();
```

You can also use a more user-friendly opportunity, by displaying a QR-Code.

You can use one of the three built-in provider:

-   GoogleQRProvider (used in Cloudrexx case)
-   QRServerProvider
-   QRicketProvider

``` php
//Display QR-Code

$tfa->getQRCodeImageAsDataUri('Cloudrexx Test', $secret);
```

After the secret is added to a 2FA-App, you need to make sure the secret was entered, or scanned, correctly. You need to verify this by having the user enter a generated code. To check if the generated code is valid you can use the verifyCode() method.

``` php
//Verify code

$result = $tfa->verifyCode($secret, $code);
```

The method will return a bool if the code was valid or not. After this, your generated secret is ready to be stored.

The method verifyCode() accepts those additional arguments:

| Param        | Default value | Use                                                     |
|--------------|---------------|---------------------------------------------------------|
| $discrepancy | 1             | Specifies how many periods the time gets checked        |
| $time        | null          | Allows you to check a code for a specific point in time |

After the code has been verified correct, you can store the secret in the database.

## Cx\Core_Modules\Login\Controller\TwoFactorAuthentication

The TwoFactorAuthentication class is used as a simplified abstraction of the implemented library explained above.

It provides the following methods that are used to realise a 2FA:

-   createSecret()
-   getCode()
-   verifyCode()
-   getQRCodeImageAsDataUri()

Functions to store or delete a 2FA entity, are located in the user component.

If you want to implement a new 2FA method, you can easily extend this class with your needed adaptations
