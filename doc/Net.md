The component `Net` provides useful methods for dealing with domains as well as it is the maintainer of the domain repository of the Cloudrexx installation.

## Domains

All mapped domains (managed by component [NetManager](NetManager.md)) of a Cloudrexx website are stored in a domain repository by the component `Net`.

!!! note

    The component `Net` provides the legacy environment variable `$_CONFIG['domainUrl']`.

### Domain Repository

The domain repository holds the list of mapped domains of a Cloudrexx installation and can be accessed as follows:

``` php
$repo = $this->cx->getComponent('Net')->getDomainRepository();
```

The domain repository is a [YamlRepository](YamlRepository.md) and does therefore provide the common getters `findAll()`, `find($id)` and `findBy($criteria)` for fetching any of the mapped domains (instances of `\Cx\Core\Net\Model\Entity\Domain`).

### Main domain

The main domain (as set by configuration option **Main domain** under *Administration &gt; Global Configuration &gt; System &gt; Site*) can be fetched as follows:

``` php
$repo = $this->cx->getComponent('Net')->getDomainRepository();
$domain = $repo->getMainDomain();
```

### Hostname domain

The name of the server can be fetched as domain instance as follows:

``` php
$repo = $this->cx->getComponent('Net')->getDomainRepository();
$hostDomain = $repo->getHostDomain();
```

### Hostname

The hostname of the server (as string) can be fetched as follows:

``` php
$hostname = $this->cx->getComponent('Net')->getHostname();
```

## Helpers

### Internationalized domain name conversion

Use the following helpers to convert an [internationalized domain name](https://en.wikipedia.org/wiki/Internationalized_domain_name) into [Punycode](https://en.wikipedia.org/wiki/Punycode) and vice-versa:

``` php
$punycode = $this->cx->getComponent('Net')->convertIdnToAsciiFormat($idnName);
```

``` php
$idnName = $this->cx->getComponent('Net')->convertIdnToUtf8Format($punycode);
```

### Resolve hostname of IP

Get the hostname of an IP-address:

``` php
$host = $this->cx->getComponent('Net')->getHostByAddr($ip);
```

!!! note

    If option **DNS Hostname lookup** (under *Administration &gt; Global Configuration &gt; System &gt; Other configuration options*) is disabled, then this method will simply return the value of `$ip`.

### DNS Query
Use [`\Cx\Core\Net\Controller\Controller\ComponentController::getDnsResponse()`](https://dev.cloudrexx.com/phpdoc/classes/Cx-Core-Net-Controller-ComponentController.html#method_getDnsResponse) for executing DNS queries.

!!! example
    
    ```php
    try {
        $response = $this->cx->getComponent('Net')->getDnsResponse(
            'cloudrexx.com',
            'TXT'
        );
    } catch (\Exception $e) {
        \DBG::debug($e->getMessage());
    }
    ```
