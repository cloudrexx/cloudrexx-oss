## Introduction

![Mediabrowser](img/Mediabrowser.png)

The MediaBrowser provides access to ressources ([Content Manager Pages](Content.md), [MediaSource Files/Objects](MediaSource.md)). Additionally, it allows the upload of new files (through component [Uploader](Uploader.md)).

## Integration
### Image chooser

If you want to let the user choose an image you can use the image chooser integration. See [`Cx\Core_Modules\MediaBrowser\Model\Entity::getImageChooserMarkup()`](/phpdoc/classes/Cx-Core-Modules-MediaBrowser-Model-Entity-MediaBrowser.html#method_getImageChooserMarkup).

!!! warning

    Currently, this only works in backend.

!!! example

    ```php
    $mediaBrowser = new \Cx\Core_Modules\MediaBrowser\Model\Entity\MediaBrowser();
    $template->setVariable(
        'MY_IMAGE',
        $mediaBrowser->getImageChooserMarkup(
            <inputName>,
            <currentImgFile>,
            <initialMediaType>,
            <availableMediaTypes>,
            <currentImgThumbnailFile>,
            <startView>,
            <availableViews>,
            <placeholderImgFile>
        )
    );

    ```
    Only `<inputName>` and `<currentImgFile>` are required.

### Button

To include the MediaBrowser in your own component through a HTML-button element, do as follows:

1.  Create new instance of \Cx\Core_Modules\MediaBrowser\Model\Entity\MediaBrowser:

``` php
$mediaBrowser = new \Cx\Core_Modules\MediaBrowser\Model\Entity\MediaBrowser();
```

2.  Configure MediaBrowser using options. I.e.:

``` php
$mediaBrowser->setOptions(array(
    'type' => 'button',
    'id' => 'myMediaBrowserBtn',
    'views' => 'uploader,filebrowser,sitestructure,',
    'startview' => 'sitestructure',
));
```

3.  Register the name of a JavaScript callback (that will be triggered once the user did select a ressource in the MediaBrowser):
    
``` php
$mediaBrowser->setCallback('javascript_callback_function');
```

    See Callback arguments
4.  Call the method *MediaBrowser::getXHtml($label)* on your instance to fetch the HTML-snippet:

``` php
$html = $mediaBrowser->getXHtml($_ARRAYLANG['TXT_CORE_CM_BROWSE']);
```

    $label will be used as display name of the button
5.  Insert the fetched HTML-snippet in the (Sigma) template:

``` php
$this->template->setVariable('MEDIABROWSER_BUTTON', $html);
```

This will create a HTML-button with the label $label set. On trigger, the MediaBrowser will appear on the screen.

### Custom layout integration

If you want to open the MediaBrowser not through the default HTML-button (as generated through *MediaBrowser::getXHtml($label)*, but instead through a custom trigger, then simple hide the generated HTML-button (using CSS) and trigger the open event manuell as follows:

``` js
var mediabrowser__closed_callback = function (callback) {
    ...
}

jQuery('#mediabrowser_button').trigger("click", {
    callback: mediabrowser__closed_callback,
    cxMbViews: 'filebrowser,uploader',
    cxMbStartview: 'MediaBrowserList'
});
```

With this method you can also add temporary options and an individual callback.

### Options

The following options are available and can be set as follows:

``` php
$mediaBrowser->setOptions(array(
    'option' => 'value'
));
```

| Option name    | Possible datatypes | Possible values                                                                                                                                                                                             | Default value | Description                                                                                                                                                                                                                       |
|----------------|--------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| type           | string             | button                                                                                                                                                                                                      |               | Set the *type* attribute of the HTML-*input* element                                                                                                                                                                              |
| id             | string             |                                                                                                                                                                                                             |               | Set the *id* attribute of the HTML-*input* element.                                                                                                                                                                               |
| views          | string             | sitestructure / uploader / filebrowser                                                                                                                                                                      | all           | Set the views (as comma-separated list) that shall be loaded and be available in the left navigation pane.                                                                                                                        |
| startview      | string             | sitestructure / uploader / filebrowser                                                                                                                                                                      | filebrowser   | Set the default view to display when loading the MediaBrowser. Note: If you do register your own view, you can also set that as *startView*.                                                                                      |
| startmediatype | string             | Any registred [MediaSource](MediaSource.md). I.e.: files / media1 / media2 / media3 / media4 / attach / shop / gallery / access / mediadir / downloads / calendar / podcast / etc.                  | files         | Set initial MediaSource to display (for *view* *filebrowser*)                                                                                                                                                                     |
| mediatypes     | string             | all or a list of any registred [MediaSource](MediaSource.md). I.e.: files / media1 / media2 / media3 / media4 / attach / shop / gallery / access / mediadir / downloads / calendar / podcast / etc. | all           | Set the list of available MediaSources in *view* *filebrowser*.                                                                                                                                                                   |
| multipleselect | boolean            | true / false                                                                                                                                                                                                | false         | Whether or not the selection of multiple ressources is permitted                                                                                                                                                                  |
| modalopened    | mixed              | false / name of callback as string                                                                                                                                                                          | false         | Set JavaScript callback to be called after the MediaBrowser has been opened. I.e.: `$mediabrowser->setOption(array('modalopened' => 'mediabrowser__opened_callback',));``window.mediabrowser__opened_callback = function(){...};` |
| modalclosed    | mixed              | false / name of callback as string                                                                                                                                                                          | false         | Can be used instead of MediaBrowser::setCallback(). I.e.: `$mediabrowser->setOption(array('modalclosed' => 'mediabrowser__closed_callback',));``window.mediabrowser__closed_callback = function(data){...};`                      |

### Callback arguments

Depending on the user's action, the data passed to the modalclose callback has the following form:

#### File has been selected

``` json
{
    type:"file",
    data:[{
         datainfo:{
            filepath:"/images/content/Screenshot-from-2014-11-24-104815.png",
            name:"Screenshot-from-2014-11-24-104815.png",
            size:"341.94 KB",
            cleansize:350150,
            extension:"Png",
            preview:"/images/content/Screenshot-from-2014-11-24-104815.thumb_thumbnail.png",
            active:false,
            type:"file",
            thumbnail: {
                "150":"/images/content/Screenshot-from-2014-11-24-104815.thumb_thumbnail.png",
                "300":"/images/content/Screenshot-from-2014-11-24-104815.thumb_medium.png",
                "640":"/images/content/Screenshot-from-2014-11-24-104815.thumb_large.png"
            }
        },
        $$hashKey:"object:740"
    }]
}
```

#### Content Manager Page has been selected

``` json
{
    type:"page",
    locale: "de",
    data:[{
        click:"javascript:{setUrl('[[NODE_HOME]]',null,null,'de/Willkommen','page')}",
        name:"Willkommen",
        extension:"Html",
        level:0,
        url:"/Willkommen",
        node:"[[NODE_HOME]]",
        localization: [{
            de: {
                name: "Willkommen",
                node: "[[NODE_HOME_DE]]",
                url: "/Willkommen"
            },
            en: {
                name: "Willkommen",
                node: "[[NODE_HOME_EN]]",
                url: "/Welcome"
            },
            ...
        }],
        $$hashKey:"object:789"
    }]
}
```

#### Nothing selected

If the user has closed the MediaBrowser without any selection:

``` json
{
    type:"close",
    data:[]
}
```

### Add own view

To add your own view, do as follows:

1.  Register an [EventLister](Event Management.md) to event **MediaBrowser.Plugin:initialize** in ComponentController:

``` php
public function registerEventListeners() {
    $this->cx->getEvents()->addEventListener(`  
        'MediaBrowser.Plugin:initialize',`  
        new \Cx\Modules\<component>\Model\Event\MyEventListener($this->cx)`  
    );`
}
```

2.  Register view in \Cx\Modules\&lt;component&gt;\Model\Event\MyEventListener:

``` php
public function mediaBrowserPluginInitialize() {
    $this->getComponent('MediaBrowser')->registerCustomJs('modules/<component>/View/Script/MediaBrowser.js');`
}
```
    
3.  Implement a [JsonData](Exposed methods.md) adapter as provider for the view's data
4.  Inject view into MediaBrowser in modules/&lt;component&gt;/View/Script/MediaBrowser.js

``` php
cx.ready(function() {
    angular.module('MediaBrowser')
    .config(["dataTabsProvider", function(dataTabsProvider) {
        dataTabsProvider
        .add({
            label: cx.variables.get(
                'TXT_MODULE_MYCOMPONENT', 'mediabrowser'),
            icon: 'icon-myicon',
            controller: 'MyCtrl',
            name: 'myview',
            templateUrl: cx.variables.get('basePath', 'contrexx') +
                'modules/<component>/View/Template/MyView.html',
            // force listing of tab in every section
            always: true
        });
    }])
    .service('MyService', ['$q',
        function($q) {
            return {
                refresh: refresh
        };
            function refresh() {
                var deferred = $q.defer();
                // fetch data from above implemented JsonData adapter
                cx.ajax('MyComponent','getEntries')
                .success(function(response) {
                    if (response.data instanceof Object) {
                        deferred.resolve(response.data);
                    } else if (response.match(/login_form/)) {
                        deferred.reject(cx.variables.get(
                            'TXT_FILEBROWSER_LOGGED_OUT', 'mediabrowser'));
                    } else {
                        deferred.reject("An error occured while fetching Entries");
                    }
                }).error(function() {
                    deferred.reject("An error occured while fetching Entries");
                });
                return deferred.promise;
            }
        }
    ])
    .controller('MyCtrl', [
        '$scope', 'mediabrowserConfig', 'MyService',
        function(
            $scope, mediabrowserConfig, MyService
        ) {
            $scope.dblclick = dblclick;
            if (!$scope.entriesLoaded) {
            MyService.refresh().then(function(entities) {
                    $scope.entriesLoaded = entities;
                    if ($scope.entriesLoaded) {
                        $scope.entries = $scope.entriesLoaded[cx.variables.get('language', 'mediabrowser')];
                    }
                });
            }
            function dblclick(entry) {
                var fn = mediabrowserConfig.get('callbackWrapper');
                if (typeof fn === 'function') {
                    fn({
                        type: 'MyEntry',
                        data: entry
                    });
                }
                $scope.closeModal();
            }
        }
    ]);
});
```

5.  Add template for view in modules/&lt;component&gt;/View/Template/Generic/MyView.html

``` html
<div id="myview" ng-controller="MyCtrl">
    <div class="fileOptions">
        <input type="search" id="myFilter" class="searchterm"
            placeholder="{{::'TXT_FILEBROWSER_SEARCHTERM_PLACEHOLDER'| translate}}"
            ng-model="searchterm">
    </div>
    <div class="tabMediaBrowserContent contentWithOptions">
        <div class="mediaBrowserListCtrl">
            <div class="loadingPlatform" ng-hide="entries || error">
                <i class="icon-loading"></i>
                <div class="loadingText1">
                    Loading
                </div>
            </div>
            <table class="adminlist my-list-entries" ng-show="entries">
                <tbody>
                    <tr>
                        <th>
                            Entry
                        </th>
                    </tr>
                    <tr ng-repeat="entry in entries| filter: {name:searchterm}"
                        ng-class="{active: true === entry.active}"
                        ng-if="entries"
                        class="data-row files animation">
                        <td>
                            <a ng-dblclick="dblclick(entry)">
                                <div class="fileName">
                                    {{::entry.name}}
                                </div>
                            </a>
                        </td>
                    </tr>
                    <tr ng-show="error">
                        <td>
                            {{::'TXT_FILEBROWSER_ERROR_HAS_HAPPEND'| translate }}
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="tabMediaBrowserFooter">
        <button type="submit" class="btn btn-primary"
            ng-disabled="noFileSelected">
            {{::'TXT_FILEBROWSER_CHOOSE_FILE'| translate }}
        </button>
        <button type="submit" class="btn btn-primary"
            ng-click="cancel()">
            {{::'TXT_FILEBROWSER_CANCEL'| translate }}
        </button>
    </div>
</div>
```
