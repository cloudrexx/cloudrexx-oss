## How to contribute?

See documentation here [CONTRIBUTING.md](https://bitbucket.org/cloudrexx/cloudrexx-oss/src/main/CONTRIBUTING.md)

## General Links

- [PHP Documentation](https://dev.cloudrexx.com/phpdoc)
- [Guidelines](Guidelines.md)
- [Customizing](Customizing.md)
- [Local installation](Local installation.md)
- [App Tutorial](App Tutorial.md)
- [Migration Guides](Migration Guides.md)

## Framework

### Model

- [Doctrine ORM](Doctrine.md)
- [Legacy Database Abstraction Layer](Guidelines.md#legacy)
- [Nested Set](Nested Set.md)
- [YamlRepository](YamlRepository.md)

### GUI / Client-side

- [AJAX Object Interface](Exposed methods.md)
- [Datepicker](Datepicker.md)
- [Dialog (modal window)](Javascript Framework UI.md#cxuidialogoptions)
- [HTML](Core Html.md)
- [Interface Text](Interface Text.md)
- [JavaScript](JavaScript.md)
- [User Live Search](User Live Search.md)
- [Navigation](Navigation.md)
- [Paging](Paging.md)
- [Tooltip](Tooltip.md)
- [WYSIWYG Editor](Wysiwyg Editor.md)
- [Status Message (Javascript)](Javascript Framework.md#cxtoolsstatusmessage)
- [Status Message (PHP)](Core Message.md)
- [Twitter Bootstrap](Twitter Bootstrap.md)

### Frontend / View

- [Captcha Protection](Captcha.md)
- [Mailing](MailTemplate.md)

### User Management / Permission / Access Control

- [Permissions control](Permissions.md)
- [User / Group](User Group.md)

### Content

- [Content](Content.md)
- [MediaBrowser](Mediabrowser.md)
- [Generating URIs](Generating URIs.md)
- [LinkGenerator](LinkGenerator.md)
- [LinkSanitizer](LinkSanitizer.md)
- [Virtual Pages](Virtual Pages.md)
- [XMLSitemap](XMLSitemap.md)

### Date / Time

- [Timezone](Timezone.md)

### File operations

- [File System Operations](FileSystem.md)
- [Uploader](Uploader.md)
- [MediaBrowser](Mediabrowser.md)

### Core

- [Caching](Caching.md)
- [Constants](Constants.md)
- [Country](Country.md)
- [Env (Environment variables)](Env.md)
- [Events](Event Management.md)
- [Full-Text Search](Full-Text Search.md)
- [HTML](Core Html.md)
- [Listing](Listing.md)
- [MailTemplate](MailTemplate.md)
- [Message](Core Message.md)
- [Page Tree](Page Tree.md)
- [Request information (URL/Protocol/etc.)](Generating URIs.md#the-requested-page)
- [Security](Security.md)
- [Session Handling](Session handling.md)
- [Setting (Configuration / Settings)](Core Setting.md)
- [Sorting](Core Sorting.md)
- [System Log (SysLog)](System Log.md)
- [System Tools](System.md)
- [Template System](Template System.md)
- [Text](Core Text.md)
- [Widgets](Widgets.md)

## Interfaces

- [RESTful API](RESTful API.md)
- [Replication](Replication.md)
- [AJAX Object Interface](Exposed methods.md)
- [Import / Export](Import Export.md)

### Misc

- [Languages](FWLanguage.md)
- [ModuleChecker](ModuleChecker.md)
- [Cron Jobs](CronJobs.md)
- [Component.yml](Component.yml.md) (PROPOSAL)

### Update

- [UpdateUtil](Update Script.md)

## Development

- [RESTful API](RESTful API.md)
- [Customizing](Customizing.md)
- [Debugging](Debugging.md)
- [Guidelines](Guidelines.md)
- [Unit Tests](Unit testing.md)
- [Workbench](Workbench.md) (Draft)

### Tutorials

1. [Environment Setup](Local installation.md)
2. [Create Component](App Tutorial.md)

## Architecture

- [Filesystem Structure](Filesystem Structure.md)
- [Namespaces](Namespaces.md)
- [System Modes](System Modes.md)
- [Bootstrapping](Bootstrapping.md)
  - [Command mode](Command mode.md)
  - [Minimal mode](Minimal mode.md)
- [ClassLoader](ClassLoader.md)
- [Doctrine](Doctrine.md)

## 3rd Party Resources

### Testing

- [Web Application Load Simulator](https://sourceforge.net/projects/loadsim/)

### JavaScript

- Event visualizer [https://www.sprymedia.co.uk/article/Visual+Event+2](https://www.sprymedia.co.uk/article/Visual+Event+2)

### Document Processing

- MS Word Document processor: [PHPDOCX](https://www.phpdocx.com/)
