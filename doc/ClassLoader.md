There are two ClassLoaders. They are trying to load classes consecutively which are not explicitly included (e.g. with `require_once()`). And they are trying to load them in the following order:

### ClassLoader

The main ClassLoader is `\Cx\Core\ClassLoader\ClassLoader`. It loads classes using the namespaces according to the [namespace concept](Namespaces.md). If the class/the interface exists in the customizing directory, this one will be loaded.

### LegacyClassLoader

The LegacyClassLoader is `\Cx\Core\ClassLoader\LegacyClassLoader`. It loads "old" classes, which don't have a correct namespace. Third party classes (e.g. PEAR) will be loaded with this class loader as well. This ClassLoader will disappear fairly long-term.

To find classes it uses a rash of rules, which covers a big part of the classes to find (see the following section Ruleset). If the correct class cannot be found, the LegacyClassLoader searches in the complete source code for a matching file. In each case it is trying to load the classes from the customizing directory first.

If a class could be found, it is saved into _/tmp/legacyClassCache.tmp_. So only the first request will be slow, all other will be faster due to this cache.

Format errors in _/tmp/legacyClassCache.tmp_ will result in a white page. If you will find a white page perhaps the file is the reason for it. Please only edit this file manually if you know what you are doing!! It is a serialized array.

#### Ruleset

The LegacyClassLoader works with this rash of rules. It checks whether the file exists. `$className` is the name of the class to load. `$frontend` is true if the request is a frontend request or false if the request is a backend request. `$moduleName` is the result of the formula: `preg_replace('/mediadirectory/', 'mediadir', strtolower(preg_replace('/Library/', '', $className)))`.

Core files

    ASCMS_CORE_PATH . '/' . $className . '.class.php'

Lib files

    ASCMS_LIBRARY_PATH . '/' . $className . '.php'

/lib/FRAMEWORK/User

    ASCMS_FRAMEWORK_PATH . '/User/' . $className . '.class.php'

/lib/FRAMEWORK

    ASCMS_FRAMEWORK_PATH . '/' . preg_replace('/FW/', '', $className) . '.class.php'

PEAR

    ASCMS_LIBRARY_PATH . '/PEAR/' . preg_replace('/_/', '/', preg_replace('/PEAR\/', '', $className . '/')) . end(preg_split('/_/', $className)) . '.php'

Model

    ASCMS_MODEL_PATH . '/entities/Cx/Model/Base/' . $className . '.php'

Module index files

    ASCMS_CORE_MODULE_PATH . '/' . strtolower($className) . ($frontend ? '/index.class.php' : '/admin.class.php')
    ASCMS_MODULE_PATH . '/' . strtolower($className) . ($frontend ? '/index.class.php' : '/admin.class.php')

Module main libraries

    ASCMS_CORE_MODULE_PATH . '/' . $moduleName . '/lib/' . $moduleName . 'Lib.class.php'
    ASCMS_CORE_MODULE_PATH . '/' . $moduleName . '/lib/Lib.class.php'
    ASCMS_CORE_MODULE_PATH . '/' . $moduleName . '/lib/lib.class.php'
    ASCMS_CORE_MODULE_PATH . '/' . $moduleName . '/Lib.class.php'
    ASCMS_MODULE_PATH . '/' . $moduleName . '/lib/' . $moduleName . 'Lib.class.php'
    ASCMS_MODULE_PATH . '/' . $moduleName . '/lib/Lib.class.php'
    ASCMS_MODULE_PATH . '/' . $moduleName . '/lib/lib.class.php'
    ASCMS_MODULE_PATH . '/' . $moduleName . '/Lib.class.php'

Module model (files in (core_)modules/{moduleName}/lib

    ASCMS_CORE_MODULE_PATH . '/' . current(preg_split('/[A-Z]/', lcfirst($className))) . '/lib/' . $className . '.class.php'
    ASCMS_CORE_MODULE_PATH . '/' . current(preg_split('/[A-Z]/', lcfirst($className))) . '/lib/' . substr($name, strlen(current(preg_split('/[A-Z]/', lcfirst($className)))) . '.class.php'
    ASCMS_CORE_MODULE_PATH . '/' . current(preg_split('/[A-Z]/', lcfirst($className))) . '/lib/' . strtolower(substr($name, strlen(current(preg_split('/[A-Z]/', lcfirst($className))))) . '.class.php'
    ASCMS_MODULE_PATH . '/' . current(preg_split('/[A-Z]/', lcfirst($className))) . '/lib/' . $className . '.class.php'
    ASCMS_MODULE_PATH . '/' . current(preg_split('/[A-Z]/', lcfirst($className))) . '/lib/' . substr($name, strlen(current(preg_split('/[A-Z]/', lcfirst($className)))) . '.class.php'
    ASCMS_MODULE_PATH . '/' . current(preg_split('/[A-Z]/', lcfirst($className))) . '/lib/' . strtolower(substr($name, strlen(current(preg_split('/[A-Z]/', lcfirst($className))))) . '.class.php'

EXCEPTIONS

    ASCMS_MODULE_PATH . '/' . current(preg_split('/[A-Z]/', lcfirst($className))) . '/' . $className . '.class.php' **Data module**
    ASCMS_MODULE_PATH . '/shop/lib/' . $className . '.class.php' **Shop**
    ASCMS_FRAMEWORK_PATH . '/File/' . $className . '.class.php' **FileSystem**
    ASCMS_FRAMEWORK_PATH . '/cxjs/' . $className . '.class.php' **CxJs**
    ASCMS_FRAMEWORK_PATH . '/cxjs/' . $className . '.interface.php' **CxJs**
    ASCMS_FRAMEWORK_PATH . '/cxjs/i18n/' . preg_replace('/JQueryUiI18nProvider/', 'jQueryUi', $className) . '.class.php' **CxJs**

If no rule matches, so `fallbackLoad()` will be called `glob()`.

## Customizings

See the [customizing article](Customizing.md) for detailed information.

Because the class loaders \Cx\Core\ClassLoader\ClassLoader and \Cx\Core\ClassLoader\LegacyClassLoader are searching in the customizing directory first, the developer can copy files with consideration of the normal folder structure to the customizing directory. These classes will replace the normal classes automatically.

## Overview

The following drawing shows an overview over the functionality of both ClassLoaders:

![Contrexx 3 ClassLoader (incl. LegacyClassLoader)](img/Cx3ClassLoader.jpg)

