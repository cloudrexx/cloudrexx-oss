Cloudrexx has its own session handler in order to be independent from server configuration. In backend mode, session is initialized automaticly, but in frontend, the session must be initialized explicitly.

## Usage & Implementation

### Initialization

To initialize a session, do as follows:

``` php
$session = $this->cx->getComponent('Session')->getSession();
```

This will either initialize a new session or resume an existing one (in case the request did supply a valid session ID).

### Resume existing session

To resume an existing session, but not to initialize a new session, do as follows:

``` php
// note: in case no existing session exists, $session will be set to NULL
// otherwise it will contain an instance of \Cx\Core\Session\Model\Entity\Session
$session = $this->cx->getComponent('Session')->getSession(false);
```

### Checking whether the session is initialized

To check if a session has been initialized, do as follows:

``` php
if ($this->cx->getComponent('Session')->isInitialized()) }
    \DBG::log('Session has been initialized');
}
```

### Close / Release session

To close an initialized session, simply call `session_write_close()`:

``` php
session_write_close();
```

This will release any blocking locks on the session and will thus allow processing of parallel requests.

### Destroy session

To destroy a session, do as follows:

``` php
// fetch existing session (but do not initialize a new one)
$session = $this->cx->getComponent('Session')->getSession(false);
if ($session) {
    // this will destroy the current session.
    // all session data will be lost permanently afterwards
    $session->destroy();
}
```

### Delete session

To delete an existing session, do as follows:

``` php
// fetch existing session (but do not initialize a new one)
$session = $this->cx->getComponent('Session')->getSession();
if ($session) {
    // permanently delete all data of the current session
    $session->destroy($session->getIdOfActiveSession());
}
```

### Delete all sessions of a particular user

To delete all sessions of a particular user, do as follows:

``` php
$session = $this->cx->getComponent('Session')->getSession();
$session->cmsSessionDestroyByUserId($userId);
```

!!! warning

    This currently only works if the call is made from within an existing session.

### Fetch session data

``` php
// ensure session is initialized
$this->cx->getComponent('Session')->getSession();
// scalar values can be accessed directly:
$var = $_SESSION['scalar_value'];

// multidimensional arrays must be fetch through the helper <code>toArray()</code>:
$array = $_SESSION['any_key_to_array_data']->toArray();
```

### Use temporary session storage

Each session provides a temporary storage location that will be flushed automatically once the session expires. Assess as follows:

``` php
$session = $this->cx->getComponent('Session')->getSession();
// fetch absolute storage location path
$path = $session->getTempPath();

// fetch relative storage location path (to be used for accessing through HTTP)
$path = $session->getWebTempPath();
```

## Technical Information

### Security

#### Session Expiration

A session will automatically expire after a certain amount of time of inactivity by the client. The expiration timeout can be configured over the option **Session length** in the console under *Administration &gt; Global Configuration &gt; System &gt; Administration area*.

#### Session Binding

A session is bound to the client over the following HTTP-headers: - `User-Agent` - `Accept-Language`

If the client sends any different values for those headers after a valid session has been initialized, then access to the session will be denied and a new session will be initialized for the client (-&gt; a new session cookie will be generated).

!!! note

    When working with the developer tools of your browser, it is expected that your session is being dropped constantly. This is due to the fact that depending on the feature being used by the developer tools, the browser switches the User-Agent in a frequent manner. To overcome this issue, the Sessing-Binding can be disabled in the console under *Administration &gt; Global Configuration &gt; System &gt; Security*.

### Session ID

The session ID has the following format:
`[a-v0-9]{32}`

### Storage

All session data is stored in the database in the following tables:

-   `contrexx_sessions`
-   `contrexx_session_variable`

### Session Data

Session data is accessible through the pseudo-array `$_SESSION` (see Fetch session data), which (in contrary to native PHP where the superglobal is a variable of type `array`) is an instance of [`\Cx\Core\Model\RecursiveArrayAccess`](https://dev.cloudrexx.com/phpdoc/classes/Cx-Core-Model-RecursiveArrayAccess.html). `\Cx\Core\Model\RecursiveArrayAccess` allows us to track in depth which part of the session data has been altered and must be flushed to the database. Without this, we would need to flush the whole session to the database afer each processed request, which would vastly slow done the system.

!!! warning

    The length of keys is limited to 100 characters.
