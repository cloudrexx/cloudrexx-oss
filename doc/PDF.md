The PDF component provides the ability to create PDF documents from HTML templates.

## Rendering

Rendering is done by using the [mPDF](https://mpdf.github.io/) library. The following table lists the used version of mPDF:

| Cloudrexx | mPDF |
|-----------|------|
| Cloud     | 6.1  |
| 5.0.3     | 6.1  |
| 5.0.2     | 6.1  |
| 5.0.1     | 6.1  |
| 5.0.0     | 6.1  |

Refer to the user manual for a short introduction on how to [render a PDF-document](https://cloudrexx.atlassian.net/wiki/spaces/HELP/pages/470679935/Gestaltung+dynamischer+PDF-Dokumente#Gestaltung).

## Integration

### Templates

A *PDF Template* consists of UTF-8 encoded HTML code with optional CSS and is managed under *Administration &gt; Global Configuration &gt; PDF Templates*.

The following example creates a PDF based on a template and replaces the placeholders "PLACEHOLDER_FOO", "PLACEHOLDER_BAR" and the placeholder "PLACEHOLDER \_IN_BLOCK_FOO" withing the block "BLOCK_FOO":

``` php
$pdf = $cx->getComponent('Pdf');
$pdfTemplateId = // ID of a PDF template which has been created under "Administration" > "Global Configuration" > "PDF Templates".
$substitution = array(
    'PLACEHOLDER_FOO' => 'Hello',
    'PLACEHOLDER_BAR' => 'World',
    'BLOCK_FOO' => array(
        0 => array(
            'PLACEHOLDER_IN_BLOCK_FOO' => 'Test',
        ),
    ),
);
$file = $pdf->generatePDF($pdfTemplateId, $substitution, 'pdf', true);
echo $file['filePath'];
echo $file['fileName'];
```

### Manually create a PDF

The following example creates a PDF based on the HTML code in $tplContent without any substitution:

``` php
$pdf = new \Cx\Core_Modules\Pdf\Model\Entity\PdfDocument();
$pdf->SetTitle($fileName . '.pdf');
$pdf->setContent($tplContent);
$pdf->setDestination('F');
$pdf->setFilePath($session->getTempPath() . '/' . $fileName . '.pdf');
$pdf->Create();
```

