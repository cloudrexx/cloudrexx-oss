## Abstract/Idea

The following class diagram shows the idea, how the core of Contrexx should look like in the future:

![Component User Framework](img/ComponentUserFramework.png)

Here are some thoughts which lead to the diagram above:

-   All "real-life-things"/models are based on a single class: EntityBase
-   Most "real-life-things"/models have a creator or owner, so they are based on 'Component' class
-   Most "real-life-things"/models also have a name and belong to a group and are therefore based on 'User'
-   Based on Unix, a user belongs to a (primary) group
-   A SystemComponent (formerly known as 'module', 'core_module', 'library', 'template' and 'part of core') belongs to a group, has a creator and a name and is therefore a 'User'

The diagram above is just a visualization of an idea. An implementation of these principles will probably need some more classes around and in between (as you can see for example in the version implemented in 3.1).

## Contrexx 3.1

Since we cannot change everything from one day to another, we decided to implement the user part later, so the class diagram for the core of Contrexx 3.1 looks like this (everything in the namespace \Cx\Core\Component was moved to \Cx\Core\Core):

![Component User Framework in Contrexx 3.1](img/ComponentUserFramework31.png)

## Component Framework v2

This is currently in development.

This introduces a way to let each component decide what version of the framework it wants to use. This allows to make breaking changes to the interface between the base system and the components.

As of component framework version 2 each component has a file Component.yml in its root folder. Components without that file are considered to use the component framework v1.

The following example of a Component.yml shows some of the concepts and ideas:

``` yaml
version: 2 # Indicates the component framework version for later versions
controllers:
  Frontend: #TODO
  Backend:
    backendTabs:
      hello:
        context: overview # Creates an overview page (TODO: add introduction page)
      bar:
        context: entity # Creates an entity listing view
        entity: "Cx\\Core\\ContentManager\\Model\\Entity\\Page"
        vgConfig: "test" # Uses the ViewGenerator config called "test"
      world:
        context: settings # Shows the settings for this component
        permission: test # Needs the permission called "test" to access this tab
        children: # This tab has children
          foo:
            context: mail # Shows MailTemplates for this component
events: # Simple list of event names that this component provides
eventListeners:
  MyListener:
    type: model # Creates a listener of type ModelEventListener
    entity: "Cx\\Core\\ContentManager\\Model\\Entity\\Page" # Listens for events of this entity
    events: # Listens to the following events
      - postFlush
vgConfigs: # List of configurations for ViewGenerator
  test: <Include: Test.yml> # Includes the file Data/Test.yml here, TODO: find better location for these files
commands: # This component defines the following command mode commands
  bla:
    permission: test # TODO
    shortDesc: "something short"
    longDesc: "Some rather overly-long description text."
    jsonAdapter: # TODO or use component-controller-action, replace by ExposedMethods

permissions: # TODO, lists permission sets
  test:
    persistant: true
    requireLogin: true
    requiredAccessIDs:
      - 17
    allowedProtocols:
      - https
accessIds: # TODO: Adds these access IDs on install
BackendAreas: # TODO: Adds these backend areas on install

setup: # TODO
  ContentManager: #pages
  Settings:
    asfd:
      type: string
      default: xy
      options: ""
widgets: # TODO, handle availability via permissions?
DataSource:
DataAccesses: # maybe merge with dataAccesses?
MediaSource: # might be same as dataSources
  MailTemplate:
  Cron:
```

The included Test.yml could look like this:

``` yaml
test:
  "Cx\\Core\\ContentManager\\Model\\Entity\\Page":
    fields:
      id:
        showOverview: false
```

### Goals

-   PHP code should only be written to introduce business logic.
-   Simple components can be written without any line of PHP code.
-   Allow simpler management of components due to less and more streamlined code.
-   Allow simpler management of components due to more modularization.
-   All [component hooks](Bootstrapping.md) should only be configs in a YAML file.
-   Fix some bugs of v1 (f.e. ViewGenerator configuration namings)

### Current state

-   Versioning and base features are implemented rudimentarily
-   All setup features are not yet implemented
-   YAML includes are implemented rudimentarily
-   Speed test and optimization pending

The current state can be found here: <https://github.com/hbdsklf/LimeCMS/tree/component-framework-v2>

