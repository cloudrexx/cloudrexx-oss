Cloudrexx contains a *Full-Text Search* application that is used to look up *content* in the available *applications* by a certain **keyword**.

Currently, the following applications have been integrated into the *Full-Text Search* application:

| Application                               | Integrated since |
|-------------------------------------------|------------------|
| Calendar                                  | 1.2.0            |
| [Content](Content.md)                     | 1.0.0            |
| Directory                                 | 1.1.0            |
| DocSys                                    | 1.0.0            |
| Downloads                                 | 5.0.0            |
| Forum                                     | 2.0.0            |
| Gallery                                   | 1.0.5            |
| Media                                     | 5.0.0            |
| MediaDir                                  | 5.0.0            |
| MemberDir                                 | 1.1.0            |
| News                                      | 1.0.0            |
| Podcast                                   | 1.0.9            |
| Shop                                      | 1.0.0            |

## Application Integration

Register an [Event Listener](Event Management.md) to the *Event* **SearchFindContent** in the component's ComponentController hook **registerEventListeners()**. Then when the *Event* *SearchFindContent* is triggered, the instance of *Full-Text Search* component (*\Cx\Core_Modules\Search\Controller\Search*) is passed as argument to the registered *Event Listeners*.

### ComponentController

Example of registering the *EventListener* in a *ComponentController*:

``` php
public function registerEventListeners() {
    $this->cx->getEvents()->addEventListener(
        'SearchFindContent',
        new \Cx\Core_Modules\News\Model\Event\NewsEventListener()
    );
}
```

### EventListener

Example of *EventListener*:

``` php
<?php
/**
 * ...
 */

namespace Cx\Modules\Demo\Model\Event;

/**
 * ...
 */
class DemoEventListener implements \Cx\Core\Event\Model\Entity\DefaultEventListener {
    public function SearchFindContent($search) {
        // fetch main application page of current locale
        $em = $this->cx->getDb()->getEntityManager();
        $pageRepo = $em->getRepository('Cx\Core\ContentManager\Model\Entity\Page');
        $page = $pageRepo->findOneByModuleCmdLang('Demo', '', FRONTEND_LANG_ID);

        // Abort in case page is not eligible to be listed in search results.
        // This will check if the page is properly published and accessible by the user
        if (!$search->isPageListable($page)) {
            return;
        }

        // fetch search results
        $result = new \Cx\Core_Modules\Listing\Model\Entity\DataSet(
            $this->getResultForSearchComponent(
                $search
            )
        );

        // attach results to search component
        $search->appendResult($result);
    }

    protected function getResultForSearchComponent(
        \Cx\Core_Modules\Search\Controller\Search $search
    ) {
        $results[] = array();

        // note: implement a method that fetches the content from the component
        foreach (
            $this->fetchResults($search->getTerm() as $result)
        ) {
            // preprocess content to be used in search results page
            $content = $search->parseContentForResultDescription(
                $result->getContent()
            );

            $results[] = array(
                // $score should be an int between 1 and 100 to indicate
                // the relevance of the result
                'Score'     => $score,
                // the title of the result
                'Title'     => $result->getTitle(),
                // the short description (preview) to be shown
                'Content'   => $content,
                // URL to the detail section of the result
                'Link'      => (string) $result->getUrl(),
                // name of this component
                'Component' => 'Demo',
            );
        }

        return $results;
    }
}
```

## Search through content of files

To lookup files by searching through their content, you can implement an [Indexer](Indexer.md).

