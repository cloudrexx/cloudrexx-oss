The Sorting class creates an object which allows to easy create and display sorted views. The intention is to use this class everywhere in Contrexx as a gateway for sortings.

This class saves sortings in the user's session, also for different views.

## Methods

-   **function __construct(&$baseUri, $arrField, $flagDefaultAsc=true, $orderUriParameter=self::DEFAULT_PARAMETER_NAME)**
    -   Constructs a sorting with the given uri
    -   Fields are in the array as ("FieldName" =&gt; "displayed name")
    -   Fieldnams can be in a SQL syntax: id, 'id' or 'table'.'id'
-   **function getUri_entities($field=null)**
    -   Returns the URL with the current sorting (NULL) or with the provided field
    -   Usage e.g. as URL for the paging, so the sorting will still be set.
-   **function getOrderReverseUriEncoded($field=null)**
    -   Returns the URL with the current sorting (NULL) or with the provided field, but in the inverse order
-   **function getHeaderArray()**
    -   Returns an array (FieldName =&gt; HTML for the head field) for all fields
-   **function getHeaderForField($field)**
    -   Returns the String with the HTML for the head field

### Examples

``` php
$uri = Html::getRelativeUri_entities();
$objSorting = new Sorting($uri, 
    array(
        'id' => $_ARRAYLANG['TXT_SHOP_MANUFACTURER_ID'],
        'name' => $_ARRAYLANG['TXT_SHOP_MANUFACTURER_NAME'],
        'url' => $_ARRAYLANG['TXT_SHOP_MANUFACTURER_URL'],
    ), true, 'order_manufacturer');

self::$objTemplate->setVariable(array(
    'SHOP_HEADER_ID' => $objSorting->getHeaderForField('id'),
    'SHOP_HEADER_NAME' => $objSorting->getHeaderForField('name'),
    'SHOP_HEADER_URL' => $objSorting->getHeaderForField('url'),
));

$arrManufacturers = Manufacturer::getArray($count,
    $objSorting->getOrder(), Paging::getPosition(), $limit);

$i = 0;
foreach ($arrManufacturers as $manufacturer_id => $arrManufacturer) {
    self::$objTemplate->setVariable(array(
        'SHOP_MANUFACTURER_ID' => $manufacturer_id,
        'SHOP_MANUFACTURER_NAME' => $arrManufacturer['name'],
        'SHOP_MANUFACTURER_URL' => $arrManufacturer['url'],
        'SHOP_ROWCLASS' => 'row'.(++$i % 2 + 1),
    ));
    self::$objTemplate->parse('manufacturer');
}

$paging = Paging::get($objSorting::getUri_entities(),
    $_ARRAYLANG['TXT_SHOP_MANUFACTURERS'], $count, $limit, true);
```

More examples are available in the shop since Contrexx version 3.0

