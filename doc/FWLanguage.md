This article is a stub and needs further information.

## Language constants

BACKEND_LANG_ID  

Is set to the visible backend language in the backend **only** (Interface language). In the frontend, it is **NOT** defined! It indicates a backend user and his currently selected language. Use this in methods that are intended **for backend use only**. It **MUST NOT** be used to determine the language for any kind of content!

FRONTEND_LANG_ID  

Is set to the selected frontend or content language both in the back- and frontend. It **always** represents the language of content being viewed or edited. Use FRONTEND_LANG_ID for that purpose **only**!

LANG_ID  

Is set to the same value as BACKEND_LANG_ID in the backend, and to the same value as FRONTEND_LANG_ID in the frontend. It **always** represents the current users' selected language. It *MUST NOT* be used to determine the language for any kind of content!

## API

``` php
FWLanguage::getNameArray($mode='frontend')
```

``` php
FWLanguage::getDefaultLangId()
```

``` php
FWLanguage::getLanguageArray()
```

``` php
FWLanguage::getActiveFrontendLanguages()
```

``` php
FWLanguage::getLanguageParameter($id, $index)
```

``` php
FWLanguage::getMenu($selectedId=0, $menuName='', $onchange='')
```

``` php
FWLanguage::getMenuActiveOnly($selectedId=0, $menuName='', $onchange='')
```

``` php
FWLanguage::getMenuoptions($selectedId=0, $flagInactive=false)
```

``` php
FWLanguage::getLangIdByIso639_1($langCode)
```

``` php
FWLanguage::getLanguageCodeById($langId)
```

``` php
FWLanguage::getLanguageIdByCode($code)
```

