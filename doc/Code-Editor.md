The Code-Editor of Cloudrexx is based on the [Ace code editor](https://ace.c9.io/).

## Integration

| Website    | <https://ace.c9.io/>                                                                                                  |
|------------|-----------------------------------------------------------------------------------------------------------------------|
| Source     | <https://github.com/ajaxorg/ace>                                                                                      |
| Version    | [`package 26.04.15` (~ `1.1.9`)](https://github.com/ajaxorg/ace-builds/tree/9d12839819e8a8b90f4a0aba382cac9a273b5983) |
| Location   | `lib/ace`                                                                                                             |
| Build Info | `lib/ace/package.json`                                                                                                |
| Extensions | `searchbox`                                                                                                           |

### Syntax validation & highlighting

Syntax validation & highlighting is currently enabled for the following languages in Cloudrexx:

| Language   | File extension |
|------------|----------------|
| HTML [^1]   | `html`         |
| CSS        | `css`          |
| JavaScript | `js`           |
| YAML       | `yml` `yaml`   |

!!! note

    The ace code editor supports syntax validation & highlighting for over 110 languages. The integration in Cloudrexx however has been reduced to just the few languages mentioned above to keep the integration lean.

[^1]: HTML is the fallback for all other file extensions not listed in this table.