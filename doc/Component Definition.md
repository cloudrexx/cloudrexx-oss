This article describes the structure and function of a Cloudrexx component (`\Cx\Core\Core\Model\Entity\SystemComponent`). For a tutorial on how to create your own component, do see [Create Cloudrexx App](https://www.cloudrexx.com/de/ueber-uns/app-tutorial).

Components are self-contained parts of Cloudrexx. A component can be added or removed, without affecting other components. Components can have dependencies to other components (for Example: a lot of different components need the [Html component](Core Html.md) to be present).

So when creating a component, all your code must be located within one single directory. If you change any line of code outside of your component's directory, you're probably doing something wrong...

### System Component

Every component is represented by an entity of `\Cx\Core\Core\Model\Entity\SystemComponent` in the *System Component Repository* (`\Cx\Core\Core\Model\Repository\SystemComponentRepository`) that is managed by the *Component Handler* (`\Cx\Core\Core\Controller\ComponentHandler`).

The `ComponentController` (which must extend `\Cx\Core\Core\Model\Entity\SystemComponentController`) of every component provides the following magic methods:

-   `$this->getName()` - Returns the name of the component
-   `$this->getDirectory()` - Returns the file system path where the component is located
-   `$this->getType()` - Returns the type of the component
-   `$this->cx` - Shortcut to the instance of \Cx\Core\Core\Controller\Cx</code> that did instantiate the component

## Component types

Here's a simple graphic showing whether your code will be part of `lib`, `core`, `core_modules` or `modules`:

![Core Module](img/CoreModule.png)

If you are creating/adding a library, simply create the directory `/lib/{name_of_your_lib}` and start developing your lib. You may skip the following steps!

### Lib

Integrate third-party libraries according to [Third-party libraries](3rd-Party Libraries.md).

## Directory structure

New components use the following directory structure:

<table class="wikitable">

<tbody><tr>
<th colspan="4"> Directory
</th>
<th> Usage
</th>
<th> Deployment
</th></tr>
<tr>
<td colspan="4"> <code>Controller</code>
</td>
<td> Controller classes
</td>
<td> x
</td></tr>
<tr>
<td colspan="4"> <code>Data</code>
</td>
<td> Data for the component
</td>
<td> x
</td></tr>
<tr>
<td width="20">
</td>
<td colspan="3"> <code>Data.sql</code>
</td>
<td> Contains base data for this component
</td>
<td> -
</td></tr>
<tr>
<td>
</td>
<td colspan="3"> <code>OldStructureWithTestData.sql</code>
</td>
<td> Contains old structure with some data for testing
<div style="background-color: rgb(183, 221, 232);padding:10px;font-style:italic;"><b>Note</b>: For legacy components (without doctrine models), do put the SQL DDL statements in here.</div>
</td>
<td> -
</td></tr>
<tr>
<td>
</td>
<td colspan="3"> <code>Migration.sql</code>
</td>
<td> Migrates <code>OldStructureWithTestData.sql</code>
<div style="background-color: rgb(183, 221, 232);padding:10px;font-style:italic;"><b>Note</b>: For legacy components (without doctrine models), this file must be present (can be empty though) to have the command <code>cx env component update &lt;componentType&gt; &lt;componentName&gt; --db</code> work.</div>
</td>
<td> -
</td></tr>
<tr>
<td>
</td>
<td colspan="3"> *
</td>
<td> All other files and folders within this folder
</td>
<td> x
</td></tr>
<tr>
<td colspan="4"> <code>Dev</code>
</td>
<td> Development scripts
</td>
<td> -
</td></tr>
<tr>
<td colspan="4"> <code>Doc</code>
</td>
<td> ERD, PDFs, etc.
</td>
<td> -
</td></tr>
<tr>
<td colspan="4"> <code>Model</code>
</td>
<td> Model classes and definitions
</td>
<td> x
</td></tr>
<tr>
<td>
</td>
<td colspan="3"> <code>Entity</code>
</td>
<td> Model entity classes
</td>
<td> x
</td></tr>
<tr>
<td>
</td>
<td colspan="3"> <code>Event</code>
</td>
<td> Event listener classes
</td>
<td> x
</td></tr>
<tr>
<td>
</td>
<td colspan="3"> <code>Repository</code>
</td>
<td> Model repository classes
</td>
<td> x
</td></tr>
<tr>
<td>
</td>
<td colspan="3"> <code>Yaml</code>
</td>
<td> Model YAML definitions
</td>
<td> x
</td></tr>
<tr>
<td colspan="4"> <code>Testing</code>
</td>
<td> Testing scripts and classes
</td>
<td> -
</td></tr>
<tr>
<td>
</td>
<td colspan="3"> <code>UnitTest</code>
</td>
<td> UnitTests
</td>
<td> -
</td></tr>
<tr>
<td colspan="4"> <code>View</code>
</td>
<td> View files
</td>
<td> x
</td></tr>
<tr>
<td>
</td>
<td colspan="3"> <code>Media</code>
</td>
<td> Images, PDFs, etc.
</td>
<td> x
</td></tr>
<tr>
<td>
</td>
<td colspan="3"> <code>Script</code>
</td>
<td> JavaScript
</td>
<td> x
</td></tr>
<tr>
<td>
</td>
<td colspan="3"> <code>Style</code>
</td>
<td> CSS
</td>
<td> x
</td></tr>
<tr>
<td>
</td>
<td colspan="3"> <code>Templates</code>
</td>
<td> Template files
</td>
<td> x
</td></tr>
<tr>
<td>
</td>
<td width="20">
</td>
<td colspan="2"> <code>Frontend</code>
</td>
<td> Template files for frontend mode
</td>
<td> x
</td></tr>
<tr>
<td>
</td>
<td width="20">
</td>
<td colspan="2"> <code>Backend</code>
</td>
<td> Template files for backend mode
</td>
<td> x
</td></tr>
<tr>
<td>
</td>
<td width="20">
</td>
<td colspan="2"> <code>Command</code>
</td>
<td> Template files for command mode
</td>
<td> x
</td></tr>
<tr>
<td>
</td>
<td width="20">
</td>
<td colspan="2"> <code>Generic</code>
</td>
<td> Template files used in more than one mode
</td>
<td> x
</td></tr>
<tr>
<td colspan="4"> <code>lang</code>
</td>
<td> Language files
</td>
<td> x
</td></tr>
<tr>
<td>
</td>
<td colspan="3"> <code>en</code>
</td>
<td> English language files
</td>
<td> x
</td></tr>
<tr>
<td>
</td>
<td width="20">
</td>
<td colspan="2"> <code>frontend.php</code>
</td>
<td> English frontend mode language file
</td>
<td> x
</td></tr>
<tr>
<td>
</td>
<td width="20">
</td>
<td colspan="2"> <code>backend.php</code>
</td>
<td> English backend mode language file
</td>
<td> x
</td></tr>
<tr>
<td>
</td>
<td colspan="3"> ...
</td>
<td> Other languages
</td>
<td> x
</td></tr>
<tr>
<td colspan="4"> <code>README.md</code>
</td>
<td> Component description. (Filename is case insensitive).
</td>
<td> -
</td></tr></tbody></table>

-   All of these directories are optional, `README.md` is mandatory.
-   The structure can be extended according to your needs, but do not add any directories to any level specified here. If you add a directory, write its name in singular CamelCase (see [Naming conventions](Naming conventions.md)).
-   All directories with sub-directories should not directly contain files (you may break this rule in directories you create yourself, but you shouldn't).
-   All files are named in CamelCase (except `README.md`) and do not contain the Component's name (again see [Naming conventions](Naming conventions.md)).

### What data to store where

Where to store data depends on whether the data is per website, per component (multiple websites may use the same codebase) or per user and whether it should be publicly available or not:

| Data per  | Public | Temporary | File system location                             | Example                                         |
|-----------|--------|-----------|--------------------------------------------------|-------------------------------------------------|
| Component | Yes    | No        | `<componentFolder>/View/Media/*`                 | Manual for this component as a PDF              |
| Component | No     | No        | `<componentFolder>/Data/*`                       | Static component specific SSL certificate       |
| Website   | Yes    | No        | `/media/public/<componentName>/*`                | Downloads without access restrictions           |
| Website   | No     | No        | `/media/private/<componentName>/*`               | Downloads with access restrictions              |
| Website   | Yes    | Yes       | `/tmp/public/cache/<componentName>/*`            | Cached images and thumbnails from remote system |
| Website   | No     | Yes       | `/tmp/cache/<componentName>/*`                   | Cached non-public imports from CSV              |
| Session   | Yes    | Yes       | `/tmp/public/<folderId>/*` [^1]                  | [Export](Autogenerated Views.md)                |
| Session   | No     | Yes       | `/tmp/session_<sessionId>/<componentName>/*`     | [Uploader](Uploader.md)                         |

[^1]: See \Cx\Core\Core\Controller\ComponentController::getPublicUserTempFolder()

## MVC

Strictly follow the [MVC](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93controller) pattern by putting

-   the model logic (including any business logic) into the **/Model** directory of a component
-   the output/GUI elements into the **/View** directory of a component
-   and the controller logic into the **/Controller** directory of a component.

This also means that neither the **/Model**, nor the **/Controller** elements must contain any HTML code. Instead do use the [Html](Core Html.md) component to generate HTML output.

### Model

The model of a component consist of

-   the actual models (under `/Model/Entity`) with its definition (under `/Model/Yaml`) and associated repositories (under `/Model/Repository`)
-   and optional [*Event Listeners*](Event Management.md) (under `/Model/Event`).

Cloudrexx supports the following two types of repositories to manage the entities of models. For each of your models, do choose the one most appropriate. If unsure, do use `\Doctrine\ORM\EntityRepository`.

| Storage Engine     | Repository                                                                            | Description                                                                                                                                                                                                                                                                                                                                                                                                                                          |
|--------------------|---------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Database (default) | [`\Doctrine\ORM\EntityRepository`](Doctrine.md#entity-repository-integration) | Use to store entities in the database by using the [Doctrine ORM](Doctrine.md). Refer to [Entity Integration](Doctrine.md#entity-integration), [Entity Repository Integration](Doctrine.md#entity-repository-integration) and [Mapping Driver](Doctrine.md#mapping-driver) on how to implement a model (under `/Model/Entity`), its definition (under `/Model/Yaml`) and its repository (under `/Model/Repository`). |
| File System        | [`\Cx\Core\Model\Controller\YamlRepository`](YamlRepository.md)               | Use to store entities (as `\Cx\Core\Model\Model\Entity\YamlEntity`) in the file system. This is mainly useful for entities that shall be accessible in cases when no database is present (e.g. when delivering a cached response).                                                                                                                                                                                                                   |

### View

The view is generated using the [Template System](Template System.md). As stated in section Controller, backend templates are located in `{your_component_directory}/View/Template/Backend`. Frontend templates are loaded based on the [resolved page](Routing.md). If the page content contains the placeholder `[[APPLICATION_DATA]]` then this placeholder is replaced by the associated application template. The application templates are located in `{your_component_directory}/View/Template/Frontend`. By default the application template `Default.html` of the resolved component is loaded. If the current page has the property `cmd` set for which an application template exists, then this template will be used instead. Application templates can be overwritten by placing a file using the same name in `{template_directory}/{your_component_type}/{your_component_name}/Template/Frontend`.

#### JavaScript and CSS

If you create a component using the [Workbench's *create*](Workbench.md) command it automatically creates a JavaScript- and CSS-file for both, the front- and backend views (`Frontend.js` / `Backend.js` / `Frontend.css` / `Backend.css`). Those files are automatically loaded in the related mode (`fontend` / `backend`) as long as they exist. Drop them if you don't need them.

### Controller

If you create a component using the [Workbench's *create*](Workbench.md) command it adds two controllers to your Component: A `FrontendController` and a `BackendController`. They do most of the work that is needed for almost every component. Both of them contain a `parsePage()` method (that gets called by the [`load`-hook](Bootstrapping.md)) with a bit of code to start with.

#### Backend sub navigation

The `BackendController` also has a method named `getCommands()`. You may return a one- or two-dimensional array of strings. There is an empty entry that will be rendered as `Default`. This will automatically generate a sub navigation. For example:

``` php
[
    '',
    'lists',
    'users' => [
        '',
        'edit',
        'import',
    ],
    'news',
    'dispatch' => [
        '',
        'templates',
        'interface',
        'confightml',
        'activatemail',
        'confirmmail',
        'notificationmail',
        'system',
    ],
]
```

This would generate a navigation matching the one of the e-mail marketing module. For every level, a default is added, so the first entry can be omitted. If you only need one navigation point, you may return an empty array (default). The backend controller then searches for language variables of the following scheme: `TXT_{COMPONENT_TYPE}_{COMPONENT_NAME}_ACT_{1ST_LEVEL}(_{2ND_LEVEL})`. For the added default, `DEFAULT` is used for 1st and 2nd level.

It is possible to restrict access to selected sections by assigning a [`Permission`](Permission class.md) instance to them. Unauthorised sections will not be listed in the generated navigation. I.e.:

``` php
[
    '',
    'lists' => [
        'permission' => new \Cx\Core_Modules\Access\Model\Entity\Permission(
            // set constraints
        ),
    ],
    'users' => [
        '',
        'edit' => [
            'permission' => new \Cx\Core_Modules\Access\Model\Entity\Permission(
                // set constraints
            ),
        ],
        'import' => [
            'permission' => new \Cx\Core_Modules\Access\Model\Entity\Permission(
                // set constraints
            ),
        ],
    ],
]
```

#### Content

The method `parsePage()` gets called by passing along the following two arguments:

| Argument | Type | In `Frontend` [System Mode](System Modes.md) | In `Backend` [System Mode](System Modes.md) |
|----------|------|--------------------------------|-------------|
| `$template` | `\Cx\Core\Html\Sigma` | The [Template System instance](Template System.md) has the content of the [resolved page](Routing.md#resolved-page) loaded (and the placeholder `[[APPLICATION_DATA]]` replaced by the associated application template). | The template file (from `{your_component_directory}/View/Template`) associated to the set value of `$cmd` is loaded if it matches an entry from `getCommands()`. Otherwise the template `Default.html` is loaded. |
| `$cmd` | `String` | Set to the page's _area_ property. | Set to the requested command. |

#### Write own ComponentController <span id="componentcontroller"></span>

By default, the `\Cx\Core\Core\Model\Entity\SystemComponentController` class is used as main controller for your Component. It registers the controllers `FrontendController` and `BackendController` for you. If any of the following conditions are true, you need to write your own `ComponentController`:

-   You want to [add another Controller](#add-another-controller)
-   You [don't need the Frontend- or BackendController](#dont-need-front-or-backendcontroller)
-   You want to [respond to any of the Component Hooks](#respond-to-component-hooks)
-   You want to [expose a method](#expose-methods)
-   [Add documentation URLs](#add-documentation-urls) for your component
-   Use [command mode](#use-command-mode)
-   You want to [register an event](#register-an-event)
-   You want to [listen to an event](#listen-to-an-event)

In order to write your own ComponentController you can simply create a class named `ComponentController` which extends `\Cx\Core\Core\Model\Entity\SystemComponentController` in the file `<yourComponentDirectory>/Controller/ComponentController.class.php`.

The following sections explain how to proceed for the use cases listed above:

##### Add another controller
1.  Create the file `<yourComponentDirectory>/Controller/<yourControllerName>Controller.class.php`
1.  Define the class `<yourComponentNamespace>\Controller\<yourControllerName>Controller` within the file. Let it extend `\Cx\Core\Core\Model\Entity\Controller`
1.  Register the controller in your `ComponentController` as shown by the following example:
    ``` php
    /**
     * {@inheritdoc}
     */
    public function getControllerClasses()
    {
        return array('Frontend', 'Backend', '<yourControllerName>');
    }
    ```

##### Don't need Front- or BackendController
Adjust the list in the method [`getControllerClasses()`](https://dev.cloudrexx.com/phpdoc/classes/Cx-Core-Core-Model-Entity-SystemComponentController.html#method_getControllerClasses) accordingly (see use case "Add another controller" above).

##### Respond to Component hooks
Implement the matching method (of the [component hook](Bootstrapping.md#component-hooks)) in your `ComponentController` according to its DocBlock.

##### Expose methods
Select a Controller (or create a new one) to register for exposing methods. Then follow [Expose a controller's method](Exposed methods.md#expose-a-controllers-method).

##### Add documentation URLs
Set one or more of the following properties in your `ComponentController`:
``` php
    /**
     * URL pointing to the end-user documentation for this component
     *
     * @var string End-user documentation URL
     */
    protected $enduserDocumentationUrl = '';

    /**
     * URL pointing to the template definitions for this component
     *
     * @var string Template documentation URL
     */
    protected $templateDocumentationUrl = '';

    /**
     * URL pointing to the developer documentation for this component
     *
     * @var string Developer documentation URL
     */
    protected $developerDocumentationUrl = '';
```

##### Use command mode
Overwrite the methods [`getCommandsForCommandMode()`](https://dev.cloudrexx.com/phpdoc/classes/Cx-Core-Core-Model-Entity-SystemComponentController.html#method_getCommandsForCommandMode), [`getCommandDescription()`](https://dev.cloudrexx.com/phpdoc/classes/Cx-Core-Core-Model-Entity-SystemComponentController.html#method_getCommandDescription) and [`executeCommand()`](https://dev.cloudrexx.com/phpdoc/classes/Cx-Core-Core-Model-Entity-SystemComponentController.html#method_executeCommand) according to their specification. See [Register your own command](Command mode.md#register-your-own-command)

##### Register an event
Implement method [`registerEvents()`](https://dev.cloudrexx.com/phpdoc/classes/Cx-Core-Core-Model-Entity-SystemComponentController.html#method_registerEvents) according the [documentation](Event Management.md#register-event).

##### Listen to an event
Implement method [`registerEventListeners()`](https://dev.cloudrexx.com/phpdoc/classes/Cx-Core-Core-Model-Entity-SystemComponentController.html#method_registerEventListener) according the [documentation](Event Management.md#register-listener).
