Unit testing stands for code-based software testing. In simple terms: You write tests for implemented features (also in PHP). Those tests can be executed as many times as you want to. Since the computer is testing, the results are always accurate and almost immediately available.

## Why write unit tests? A plea for unit testing

**Less bugs:** Since the tests are meant to be executed over and over again (this is called *testsuite*), they will always discover bugs, which would have stayed undiscovered otherwise. Furthermore new tests can be written, for newly discovered bugs (this is called *regression testing*). Learn from your mistakes - The same bug should never appear twice!

**Less development effort**:

-   Bugs are discovered early and do not have to be fixed later
-   You instantly know when a new functionality affects an old one, since the appropriate test does not succeed anymore

You may not be faster while developing, but you definitely decrease the maintenance effort.

**Test as frequently as desired**: The execution of the testsuite costs.. an enter or a double click. When executing it you will see instantly, if the program still works as exptected. Even more important: You will see instantly when you have created a bug. Writing tests is a one-time effort, which provides long-term security.

**Simpler rewriting and changing**: Larger code changes always come with a certain risk. With aid of the testsuite it can be ascertained wether the code still does the same or not. Tested APIs complain when they don't work anymore due to changes. Since unit tests always work with the public methods of classes, the magic behind them can arbitrarily be changed, without losing the security of everything working as usual.

**Unit tests document**: Most unit tests are perfect examples for the usage of a class. Examples say more than a thousand words! :-)

**Trust in the code base**: Tested code provides the security of knowing that it works. Instead of fixing bugs, there's more time for developing new features. "Trust" is to be understood externally as well: Good developerst will have more trust for a library/API, which is tested.

**It's fun**: Code you know for sure it works, code you can change comfortably without having to test everything manually... sounds like fun, doesn't it?

## Content

Unit testing happens based on PHPUnit ([manual](https://www.phpunit.de/manual/current/en/index.html)).

An introduction to the topic is available [in the zend dev-zone](https://devzone.zend.com/article/2772) (recommended). For developers doing new development we recommend the preceding reading [of this entry in the google testing blog](https://googletesting.blogspot.com/2008/08/by-miko-hevery-so-you-decided-to.html).

## Cloudrexx directory structure

### Tests

*Examples can be found in core/ContentManager/Testing/UnitTest*

These should be based on the directory structure of Cloudrexx itself.  
Tests have to be placed in the subdirectory *Testing/UnitTest* of the component, which is being tested.  
For example if you write tests for the **Page** class of the **ContentManager** component, located in *core/ContentManager/Model/Entity/Page.class.php*, the test-class has to be placed in *core/ContentManager/Testing/UnitTest/PageTest.class.php* (Note the naming of the file *Test.php*).

### Test cases

The test class extends the class of it's test case ([manual](https://www.phpunit.de/manual/current/en/testcase-extensions.html)). Cloudrexx-specific test cases are found in *core/Test/Model/Entity*.

### Running tests

Tests are executed using the [Workbench](Workbench.md). Use the following command to execute all unit tests:

``` bash
./cx workbench test
```

For more information about the *test*-command, execute the following command:

``` bash
./cx workbench help test
```
