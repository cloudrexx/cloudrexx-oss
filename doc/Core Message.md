The Message class contains (mostly static) methods and constants which are helping you with displaying and saving messages for the user. The intention of the class is to consistently and authenticaly give all messages from Contrexx to the user. The messages will be saved in the user's session. If the output of a message is not possible, e.g. because the program will be interrupted earlier, the messages will be shown on the next successful page load.

## Constants

The following constants are defining the four types of messages:

``` php
const CLASS_ERROR = 'error';
const CLASS_WARN = 'warning';
const CLASS_INFO = 'information';
const CLASS_OK = 'ok';
```

## Methods

-   **static function clear()**
    -   Deletes all messages
-   **static function have($class=null)**
    -   TRUE, if there are messages (of the given class), otherwise FALSE
-   **static function save()**
    -   Stores the message on the stack and deletes from the session
-   **static function restore()**
    -   Overwrites the session messages with the message on the top of the stack. If the stack is empty, the message in the session will be deleted too.
-   **static function add($message, $class=self::CLASS_INFO)**
    -   Adds a new message to the stack with the given message and the given type.
-   **static function ok($message)**
    -   Shorthand for messages of the type CLASS_OK
-   **static function information($message)**
    -   Shorthand for messages of the type CLASS_INFO
-   **static function warning($message)**
    -   Shorthand for messages of the type CLASS_WARN
-   **static function error($message)**
    -   Shorthand for messages of the type CLASS_ERROR
-   **static function show($objTemplateLocal=null)**
    -   Shows the existing messages and deletes them from the session.
-   **static function get()**
    -   Calls the messages and get them (in priority order descending) and deletes the retrieved ones.
    -   This method is only useful for *old* modules, which don't support Message and want to get an own status from the class.
    -   Don't use it if you can solve it the other way. Please rewrite the module (see Example).

## Placeholder

The class uses separate methods to replace the different placeholders (frontend / backend).

-   Backend: CONTENT_(OK|STATUS)_MESSAGE, CONTENT_MESSAGE_TYPE (old scheme!)
-   Frontend: MESSAGE_CLASS, MESSAGE_TEXT (new scheme, will also be used in backend as soon as possible!)
-   In both cases the circumfluent block is named "messages".

!!! example

    In future the messages should be used with this format:

    ``` html
    <!-- BEGIN messages -->
    <span class="[[MESSAGE_CLASS]]">[[MESSAGE_TEXT]]</span>
    <!-- END messages -->
    ```
