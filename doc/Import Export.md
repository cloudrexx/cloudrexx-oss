Import and export should be done using ViewGenerator options (see
[CLX-5609](https://cloudrexx.atlassian.net/browse/CLX-5609)). If you need to
implement some custom data handling for some reason, the model classes of the
lib component Convert can be used.

## Using Convert model classes

\Cx\Lib\Convert\Model\Entity contains models for data in different formats. They
allow reading from and writing to file, buffer or string and should be used
instead of using any native PHP functions (for example `fgetcsv()`) directly.

Currently, only reading from a CSV file is supported. This can be done as follows:

```
$objFile = new \Cx\Lib\FileSystem\File('my-file.csv');
$csvImporter = new \Cx\Lib\Convert\Model\Entity\Csv();
$data = $csvImporter->parseFile(
    $objFile,
    function(array $lineData, int $lineNumber): void {
        // ...
    }
);
```

The callback gets called for any non-empty row of the data file. Rows only
containing separator and enclosures are treated as empty.

By default the first (non-empty) row is used as headers, by which the data will
be indexed. If you don't want that you can pass `false` as the third parameter.

For more info about the available parameters, see [code documentation](/phpdoc/classes/Cx-Lib-Model-Entity-Csv.html).


## Import (deprecated)

The following is the description of the deprecated Import controller for reference.

Als erstes muss die Import Klasse eingebunden werden und ein Objekt davon erstellt werden:

``` php
$importlib = new \Cx\Lib\Convert\Controller\Import();
```

Der Importvorgang erfolgt nun in 3 Schritten:

1.  Auswahl der Datei, Angabe von Optionen (Typ der zu importierenden Datei [csv, excel]). Die Datei wird beim Abschicken des Formulars hochgeladen. (Fileselect)
2.  Die Datei wird in den tmp Ordner verschoben und ein erstes mal geparst. Dabei werden die Feldnamen herausgelesen. Erledigt wird dies von einer weiteren Klasse. Danach wird das Formular zum zuweisen der Felder angezeigt. Die Optionen von Punkt 1 werden per hidden Felder weitergegeben. (Fieldselect)
3.  Nun wird die Datei ein zweites mal geparst und die Daten werden herausgelesen. Aufgrund der Zuweisungsangaben (Punkt 2) des Users wird ein array mit den Daten zurück gegeben. Die Datei wird gelöscht, die Arbeit der Importklasse ist erledigt.

Entsprechend dieser drei Schritte ist nun folgendes zu tun: (Ein gutes Beispiel befindet sich in der Datei modules/memberdir/admin.class.php, _import() Methode)

### If Verzweigungen

Im code, wo die Import Klasse verwendet werden soll, müssen einige if-Verzweigungen gemacht werden. Hier ein Beispiel:

``` php
if (isset($_POST['import_cancel'])) {
    // Abbrechen. Siehe Abbrechen 
} elseif ($_POST['fieldsSelected']) { 
      // Speichern der Daten. Siehe Final weiter unten. 
} elseif ($_FILES['importfile']['size'] == 0) { 
     // Dateiauswahldialog. Siehe Fileselect 
} else { 
     // Felderzuweisungsdialog. Siehe Fieldselect 
}
```

### Fileselect

Beim Importlib Objekt muss die entsprechende Methode aufgerufen werden:

``` php
$importlib->initFileSelectTemplate($this->_objTpl);
```

Übergeben werden muss das Template Objekt. Die Variable wird per Referenz übergeben.

Die Methode zeigt nun das Standard Fileselect-Formular an. Falls noch weitere Optionen benötigt werden, können die wie folgt hinzugefügt werden:

``` php
$this->_objTpl->setVariable(array(
    'IMPORT_ACTION'      => '?cmd=memberdir&amp;act=import',
    'IMPORT_ADD_NAME'    => 'Kategorie',
    'IMPORT_ADD_VALUE'   => $this->_getCategoryMenu(),
    'IMPORT_ROWCLASS'    => 'row2',
    'TXT_HELP'           => 'W&auml;hlen Sie hier eine Datei aus, deren Inhalt importiert werden soll:'
)); 
$this->_objTpl->parse("additional");
```

Der **TXT_HELP** Platzhalter ist für die Anzeige eines Hilfe-Texts.

### Fieldselect

Auch hier muss eine Methode des Importlib Objekts aufgerufen werden, mit dem Templateobjekt als Parameter:

``` php
$importlib->initFieldSelectTemplate($this->_objTpl, $given_fields);
```

Der zweite Parameter ist ein Array mit den 'gegebenen' Werten, also den Werten die in der Rechten Spalte angezeigt werden bei der Feldauswahl. Das Array muss folgende Struktur haben:

``` php
Array
(
    [Key] => Value,
    [2] => "Beispiel"
)
```

Falls noch Optionen vom Fileselect (additional options) durchgereicht werden müssen, können auch noch zusätzliche hidden-input-Tags geparst werden:

``` php
$this->_objTpl->setVariable(array(
    'IMPORT_HIDDEN_NAME'    => 'category',
    'IMPORT_HIDDEN_VALUE'   => 5,
));
```

### Final

Am Ende muss noch die Methode getFinalData aufgerufen werden:

``` php
$data = $importlib->getFinalData($fields);
```

Übergeben wird ein Array mit den Feldnamen.

``` php
Array
(
    [Key] => Value,
    [2] => "Beispiel"
)
```

Man erhält nun ein Array mit den Daten. Das Array hat folgende Struktur:

``` php
Array
(
    [0] => Array
        (
            [key1] => Wert1
            [feldname1] => Wert1
            [key2] => Wert2
            [feldname] => Wert2
        )
    [1] => Array
        (
            [1] => Astalavista
            [Firmenname] => Astalavista
            [2] => Schmid
            [Kontaktperson] => Schmid
        )
)
```

Nun müssen die Daten nur noch gespeichert werden ;)

### Abbrechen

Damit der Benutzer den Vorgang auch abbrechen kann, muss noch die Cancel Methode implementiert werden. Anschliessend empfiehlt es sich, eine Weiterleitung zu machen.

``` php
$importlib->cancel(); 
header("Location: index.php?cmd=memberdir&act=import");
exit;
```

