## Setup

There are several ways on how debugging can be enabled in Cloudrexx:

-   [In Backend (over GUI)](#backend)
-   [In Source Code (using PHP)](#php)
-   [In Environment (through `cx env`)](#env)

### Backend

Debugging can be enabled under *Administration &gt; Global Configuration &gt; Development tools* in the Backend. This will enable the debugging on a per-session basis. Meaning that the debugging will only be enabled for the user who has enabled it. All other users won't be affected by the debugging output.

![*Development tools* under *Global Configuration*](img/Cx3_development_tools.png)

### PHP

Debugging can be activated/deactivated using the following two commands:

``` php
// activate debugging
\DBG::activate();
```

``` php
// deactivate debugging
\DBG::deactivate();
```

Both methods [`\DBG::activate()`](https://dev.cloudrexx.com/phpdoc/classes/DBG.html#method_activate) and [`\DBG::deactivate()`](https://dev.cloudrexx.com/phpdoc/classes/DBG.html#method_deactivate) accept a bitwise combination of the available [debug flags](#debug-flags). Therefore you can individually activate/deactivate each debug flag throughout your code.

!!! example

    ``` php
    // activate PHP error-reporting + log to file
    \DBG::activate(DBG_PHP | DBG_LOG_FILE);

    [...]

    // later in the code, activate ADOdb error log to monitor some specific SQL queries
    \DBG::activate(DBG_ADODB_ERROR);

    [...]

    // deactivate ADOdb error log for rest of page request (could be activated again later in the code if needed)
    \DBG::deactivate(DBG_ADODB_ERROR);

    // deactivate whole debugging output
    \DBG::deactivate();
    ```

### Env

In a Cloudrexx environment ([`cx env`](CLI script.md)) debugging is enabled by default and can be adjusted through the environment variable `CLX_DBG_FLAGS` in the `docker-compose.yml` file.

Set `CLX_DBG_FLAGS` to a pipe delimited string of [debug flags](#debug-flags). I.e.:

    CLX_DBG_FLAGS='DBG_PHP | DBG_LOG_FILE'

!!! warning

    The value of `CLX_DBG_FLAGS` must be enclosed by quotes.

To disable debugging just set: `CLX_DBG_FLAGS='DBG_NONE'`.

## Integration

Informational [logging](#logging) should be kept to a minimum to prevent log cluttering. Instead implement [debug statements](#debug-logs) for in-depth analysis in case of an unhandled exception by [dumping an extended log](#dump-extended-log).

### Logging

The methods [`\DBG::dump()`](https://dev.cloudrexx.com/phpdoc/classes/DBG.html#method_dump), [`\DBG::log()`](https://dev.cloudrexx.com/phpdoc/classes/DBG.html#method_log) and [`\DBG::msg()`](https://dev.cloudrexx.com/phpdoc/classes/DBG.html#method_msg) can be used to output custom debug information:

``` php
// dump variable information (similar to like PHP's var_dump())
\DBG::dump($array);

// output a message
\DBG::log($message);
```

#### Mode constraint

The following methods do support a second argument `$modeConstraint` (which defaults to `DBG_LOG`) that causes them to only generate the logs if the set [debug flag(s)](#debug-flags) (by `$modeConstraint`) are set:
-   [`\DBG::dump()`](https://dev.cloudrexx.com/phpdoc/classes/DBG.html#method_dump)
-   [`\DBG::msg()`](https://dev.cloudrexx.com/phpdoc/classes/DBG.html#method_msg)
-   [`\DBG::trace()`](https://dev.cloudrexx.com/phpdoc/classes/DBG.html#method_trace)
-   [`\DBG::calltrace()`](https://dev.cloudrexx.com/phpdoc/classes/DBG.html#method_calltrace)
-   [`\DBG::stack()`](https://dev.cloudrexx.com/phpdoc/classes/DBG.html#method_stack)

### Debug Logs

Generate logs only if `DBG_DEBUG` has been enabled, either by using [`\DBG::debug()`](https://dev.cloudrexx.com/phpdoc/classes/DBG.html#method_debug) or by supplying the flag `DBG_DEBUG` as constraint to [`\DBG::dump()`](https://dev.cloudrexx.com/phpdoc/classes/DBG.html#method_dump), [`\DBG::msg()`](https://dev.cloudrexx.com/phpdoc/classes/DBG.html#method_msg), [`\DBG::trace()`](https://dev.cloudrexx.com/phpdoc/classes/DBG.html#method_trace), [`\DBG::calltrace()`](https://dev.cloudrexx.com/phpdoc/classes/DBG.html#method_calltrace) or [`\DBG::stack()`](https://dev.cloudrexx.com/phpdoc/classes/DBG.html#method_stack):

``` php
\DBG::debug($log);
\DBG::dump($data, DBG_DEBUG);
\DBG::msg($message, DBG_DEBUG);
\DBG::trace(0, DBG_DEBUG);
\DBG::calltrace(DBG_DEBUG);
\DBG::stack(DBG_DEBUG);
```

### Tracing

PHP code can be traced using [`\DBG::trace()`](https://dev.cloudrexx.com/phpdoc/classes/DBG.html#method_trace) and [`\DBG::calltrace()`](https://dev.cloudrexx.com/phpdoc/classes/DBG.html#method_calltrace):

``` php
// output method/function name where \DBG::trace() is called
\DBG::trace();
```

Backtrace can be output using [`\DBG::stack()`](https://dev.cloudrexx.com/phpdoc/classes/DBG.html#method_stack):

``` php
// output backtrace (wrapper for PHP's debug_backtrace())
\DBG::stack();
```

SQL queries can be traced using the [debug flag](#debug-flags) `DBG_DB_TRACE`:

``` php
// activate SQL query tracing
\DBG::activate(DBG_ADODB | DBG_ADODB_TRACE);
```

### Dump extended log

To prevent the file log stream (`DBG_LOG_FILE`) to be cluttered with debug logs on a production environment, log verbosity can be kept low (i.e. by not setting `DBG_DEBUG` and most of the `DBG_DB*` flags). Then to still collect debug logs in the event of an error, the memory log stream (`DBG_LOG_MEMORY`) can be used to collect all log statements during execution. The collected data can then be flushed to the extended log stream (`/tmp/log/extended_dbg.log`) using [`\DBG::dumpExtendedLog()`](https://dev.cloudrexx.com/phpdoc/classes/DBG.html#method_dumpExtendedLog).


!!! note

    On a Cloudrexx environment ([`cx env`](CLI script.md)), the [debug flag](#debug-flags) `DBG_LOG_MEMORY` is set by default and [`\DBG::dumpExtendedLog()`](https://dev.cloudrexx.com/phpdoc/classes/DBG.html#method_dumpExtendedLog) is called in case of an unhandled throwable by [`\Cx\Core\Core\Controller\Cx`](https://dev.cloudrexx.com/phpdoc/classes/Cx-Core-Core-Controller-Cx.html#method___construct).


## Inspect

Depending on the enabled [log targets](#log-targets), the logs are either appended to the logfile `/tmp/log/dbg.log` (if `DBG_LOG_FILE` is set) or sent to stdout (if neither of `DBG_LOG_FILE` and `DBG_LOG_MEMORY` are set and the request was made over [CLI](Command mode.md)).

!!! note
    When running an env using `cx env` then the logs are accessible through [`cx debug`](CLI script.md#debug) by default (or if `DBG_LOG_FILE` is set).

## Debug Flags

Different levels of debugging can be enabled in [PHP](#php) or through the [environment](#env) by setting individual debug flags.
The flags serve different purposes:

-   [Log targets](#log-targets)
-   [PHP log verbosity](#php-log-verbosity)
-   [Database log verbosity](#database-log-verbosity)

### Log targets

| Flag                   | Description |
|------------------------|--|
| `DBG_LOG_FILE`         | Enables log streaming to log file (`/tmp/log/dbg.log`). |
| `DBG_LOG_MEMORY`       | Enables log streaming to memory. Memory logs can be fetched using [`\DBG::getMemoryLogs()`](https://dev.cloudrexx.com/phpdoc/classes/DBG.html#method_getMemoryLogs). The memory stream is being used by [`\DBG::dumpExtendedLog()`](https://dev.cloudrexx.com/phpdoc/classes/DBG.html#method_dumpExtendedLog) which gets triggered by [`\Cx\Core\Core\Controller\Cx`](https://dev.cloudrexx.com/phpdoc/classes/Cx-Core-Core-Controller-Cx.html#method___construct) if a throwable occurs. |

!!! note
    
    If logging is enabled, but neither of `DBG_LOG_FILE` and `DBG_LOG_MEMORY` are set, then logs are streamed to stdout, but only if the request has been made over [CLI](Command mode.md).

### PHP log verbosity

| Flag                   | Description |
|------------------------|--|
| `DBG_NONE`             | Disable DBG along with its error-handling _**Warning**{ .admonition-title } Disabling DBG is not advised!_{ .admonition .warning } |
| `DBG_PHP`              | Log & handle PHP errors/warnings/notices _**Note**{ .admonition-title } This should always be set._{ .admonition .note } |
| `DBG_LOG`              | Log messages/events from [`\DBG::dump()`](https://dev.cloudrexx.com/phpdoc/classes/DBG.html#method_dump), [`\DBG::log()`](https://dev.cloudrexx.com/phpdoc/classes/DBG.html#method_log), [`\DBG::msg()`](https://dev.cloudrexx.com/phpdoc/classes/DBG.html#method_msg) & [`\DBG::stack()`](https://dev.cloudrexx.com/phpdoc/classes/DBG.html#method_stack) _**Note**{ .admonition-title } `DBG_LOG` is enabled by default. Must manually be disabled using `\DBG::deactivate(DBG_LOG)`._{ .admonition .note } |
| `DBG_PROFILE`          | Profile log events. Logs time (by using [`\DBG::time()`](https://dev.cloudrexx.com/phpdoc/classes/DBG.html#method_time)) of each log entry as well as the delta to the previous log entry |
| `DBG_DEBUG`            | Log debug statements from [`\DBG::debug()`](https://dev.cloudrexx.com/phpdoc/classes/DBG.html#method_debug) |
| `DBG_ALL`              | Shortcut for: `DBG_PHP` \| `DBG_DB` \| `DBG_DB_TRACE` \| `DBG_DB_ERROR` \| `DBG_DB_DUMP_ON_ERROR` \| `DBG_LOG_FILE` \| `DBG_LOG_MEMORY` \| `DBG_LOG` \| `DBG_PROFILE` \| `DBG_DEBUG` |

### Database log verbosity

Set database abstraction layer log verbosity.

| Flag                   | Description |
|------------------------|--|
| `DBG_DB`               | Shortcut for: `DBG_DOCTRINE | DBG_ADODB` |
| `DBG_DB_SELECT`        | Shortcut for: `DBG_DOCTRINE_SELECT | DBG_ADODB_SELECT` |
| `DBG_DB_CHANGE`        | Shortcut for: `DBG_DOCTRINE_CHANGE | DBG_ADODB_CHANGE` |
| `DBG_DB_TRACE`         | Shortcut for: `DBG_DOCTRINE_TRACE | DBG_ADODB_TRACE` |
| `DBG_DB_ERROR`         | Shortcut for: `DBG_DOCTRINE_ERROR | DBG_ADODB_ERROR` |
| `DBG_DB_DUMP_ON_ERROR` | Dump all database queries to the extended log stream when encountering a system error |

#### Database abstraction layer (doctrine) only

Set individual log verbosity for the doctrine database abstraction layer.

| Flag                   | Description |
|------------------------|--|
| `DBG_DOCTRINE`         | Shortcut for: `DBG_DOCTRINE_SELECT | DBG_DOCTRINE_CHANGE` |
| `DBG_DOCTRINE_SELECT`  | Log read queries (`SELECT`) of the database abstraction layer (doctrine) |
| `DBG_DOCTRINE_CHANGE`  | Log write queries (`INSERT`/`UPDATE`/`DELETE`) of the database abstraction layer (doctrine) |
| `DBG_DOCTRINE_TRACE`   | Add stack traces to the logged queries of the database abstraction layer (doctrine) |
| `DBG_DOCTRINE_ERROR`   | Log only failed queries of the database abstraction layer (doctrine) |

#### Legacy Database abstraction layer (ADOdb) only

Set individual log verbosity for the legacy database abstraction layer (ADOdb).

| Flag                   | Description |
|------------------------|--|
| `DBG_ADODB`            | Shortcut for: `DBG_ADODB_SELECT | DBG_ADODB_CHANGE` |
| `DBG_ADODB_SELECT`     | Log read queries (`SELECT`) of the legacy database abstraction layer (ADOdb) |
| `DBG_ADODB_CHANGE`     | Log write queries (`INSERT`/`UPDATE`/`DELETE`) of the legacy database abstraction layer (ADOdb) |
| `DBG_ADODB_TRACE`      | Add stack traces to the logged queries of the legacy database abstraction layer (ADOdb) |
| `DBG_ADODB_ERROR`      | Log only failed queries of the legacy database abstraction layer (ADOdb) |
