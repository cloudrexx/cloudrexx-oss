MediaSources provide abstract access to files. This allows to work with paths relative to the installation root, even if the files are in a different folder or even on a different server.

## Get a MediaSource

### Use previously registered MediaSources

Registered MediaSources can be fetched by their identifier as follows:

``` php
$cx->getMediaSourceManager()->getMediaType('<identifier>');
```

### Register a MediaSource

In order to add a folder to be available in [MediaBrowser](Mediabrowser.md), you may add a registered MediaSource. To achieve this, register an event listener for the event "mediasource:load":

``` php
// in ComponentController::registerEventListeners()
$myEventListener = new \Cx\<ComponentNS>\Model\Event\<MyEventListenerName>($this->cx);
$this->cx->getEvents()->addEventListener('mediasource.load', $myEventListener);
```

Then you can add the MediaSource in your listener:

``` php
public function mediasourceLoad(MediaSourceManager $mediaSourceManager) {
    $langData = \Env::get('init')->getComponentSpecificLanguageData($this->getName());

    $mediaSource = new \Cx\Core\MediaSource\Model\Entity\MediaSource(
        $this->getName() . '<identifier>', // name of MediaSource, unique over the whole system
        $langData['TXT_...'], // user friendly name of the MediaSource
        array(
            $this->cx->getWebsiteImagesPath() . '/...', // Path
            $this->cx->getWebsiteImagesWebPath() . '/...', // Web path
        )
    );
    $mediaSource->setAccessIds(array(13)); // access IDs that are allowed to access this MediaSource
    $mediaSourceManager->addMediaType($mediaSource);
}
```

### Create an unregistered MediaSource

If you want to work with files without adding the folder to the MediaBrowser, you need an unregistered MediaSource. The following pseude-code shows how this can be achieved. This code can simply be added where the MediaSource is needed:

``` php
$mediaSource = new MediaSource(
    '<myComponent><identifier>', // name of MediaSource, unique over the whole system
    $_ARRAYLANG['TXT_...'], // user friendly name of the MediaSource
    array(
        $this->cx->getWebsiteImagesPath() . '/...', // Path
        $this->cx->getWebsiteImagesWebPath() . '/...', // Web path
    )
);
```

## File operations

Apart from the access to files described in [DataSource](DataSource.md) the following operations can be performed:

### Read files

``` php
$recursive = true;
$dir = 'icons'; 
$files = $mediaSource->getFileSystem()->getFileList($dir, $recursive);
```

### Get lib FileSystem file object

The following code is an example to get the lib [FileSystem](FileSystem.md) file
object from a MediaSource:

```
$mediaSource = $this->cx->getMediaSourceManager()->getMediaType('<identifier>');
$mediaSourceFile = $mediaSource->getFileSystem()->getFileFromPath($filename);
if (!$mediaSourceFile) {
    // Handle error
}
$libFileSystemFile = new \Cx\Lib\FileSystem\File(
    $mediaSourceFile->getFileSystem()->getFullPath($mediaSourceFile) . $mediaSourceFile->getFullName()
);

```

### Others

See `core/MediaSource/Model/Entity/FileSystem.interface.php`
