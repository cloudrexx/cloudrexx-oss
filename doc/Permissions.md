Permissions are handled using access ids. There are two types of access ids.

-   static
    these are used to restrict the access to functions and sections (mostly backend)
-   dynamic
    these are used to restrict the access to content data (content pages, categories, documents, etc.)

``` php
// Check for a certain permission
Permission::checkAccess($accessId, $type = 'static', $return = false);
// Check for superuser
Permission::hasAllAccess();
// Redirect user to the noaccess page (frontend: section=noaccess / backend: cmd=noaccess)
Permission::noAccess($redirect);
```

## Protect user generated object

``` php
// protect a user generated object (like content)
function protect() {
    $accessId= Permission::createNewDynamicAccessId()
    if ($accessId) {
        Module::protectObjectWithAccessId($accessId);
    }
}

function checkAccess() {
    $accessId = Module::getAccessIdOfRequestedObject();
    Permission::checkAccess($accessId, 'dynamic');
}
```

### Set access to user group

``` php
// $groupId is the ID of user group you want to allow accessing the object
Permission::setAccess($accessId, $type, $groupId)
```

### Revoke access to user group

``` php
// $groupId is the ID of group to revoke the access from
Permission::removeAccess($accessId, 'dynamic', $groupId)
```
