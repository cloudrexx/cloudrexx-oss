The `\Cx\Core_Modules\Access\Model\Entity\Callback` class is a wrapper for all kinds of callbacks that can be used in Cloudrexx.

## Types of callbacks

### Inline function callback

An inline function callback is the most straight-forward case for a callback:

``` php
$callback = new \Cx\Core_Modules\Access\Model\Entity\Callback(
    function($args) {
        // some code
    }
);
```

### PHP Callable

This type of callback is any of the possibilities listed by [PHP Callables](https://www.php.net/manual/en/language.types.callable.php).

!!! example

    ``` php
    $callback = new \Cx\Core_Modules\Access\Model\Entity\Callback(
        array('MyClass', 'myCallbackMethod')
    );
    ```

### [Exposed method](Exposed methods.md) callback

Any exposed method can be used as a callback.

!!! example

    ``` php
    $callback = \Cx\Core_Modules\Access\Model\Entity\Callback::fromJsonAdapter(
        'AdapterName',
        'AdapterMethod',
        array('list', 'of' => 'arguments'),
        array('data' => 'arguments')
    );
    ```

If any arguments or data is passed they are merged with the arguments passed at call time.

## How to invoke a Callback instance

Any of the above cases can be called the same way:

``` php
$returnValue = $callback('some', 'arguments');
```

For callbacks of type [exposed methods](Exposed methods.md) the first parameter is used as arguments list and the second parameter is used as data arguments. Both need to be an array, otherwise they are ignored.

## Persistable callbacks

Callbacks of the type [exposed methods](Exposed methods.md) can be persisted to the database. This is done by persisting the Callback instance like any other entity:

``` php
$entityManager->persist($callback);
$entityManager->flush();
```

