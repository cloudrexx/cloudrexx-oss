The LinkSanitizer is used to make all relative URLs in the generated document (output) absolute. You will need the LinkSanitizer on standalone requests. Meaning that you finish the request by sending the output to the browser and abort the script execution instead of letting this the regular request handler of Contrexx do.

``` php
$ls = new LinkSanitizer(ASCMS_PATH_OFFSET.Env::get('virtualLanguageDirectory').'/', $output);
$output = $ls->replace();
```