## Server's Upload-File-Size limitation

``` php
FWSystem::getMaxUploadFileSize()
```

Returns the maximum supported file size to be uploaded through a POST interface.

## Convert byte size into literal representation

``` php
FWSystem::getLiteralSizeFormat()
```

Returns the literal representation of a byte size

``` php
// This will output "1 KB"
print FWSystem::getLiteralSizeFormat(1024);
```

## Convert literal representation into byte size

``` php
FWSystem::getBytesOfLiteralSizeFormat()
```

Returns the byte count of a literal byte size representation

``` php
// All of the following examples will output "1024"
print FWSystem::getBytesOfLiteralSizeFormat("1 KB");
print FWSystem::getBytesOfLiteralSizeFormat("1 K");
print FWSystem::getBytesOfLiteralSizeFormat("1kb");
print FWSystem::getBytesOfLiteralSizeFormat("1k");
```

