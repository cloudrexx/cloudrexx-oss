This page describes the cookie handling in Cloudrexx, specifically by the component "Privacy".

## JavaScript API

You can use a predefined privacy note, which can also be customized. More info about this feature can be found in the [end user documentation](https://cloudrexx.atlassian.net/wiki/spaces/HELP/pages/498335745). This section provides information if you want to use Cloudrexx' cookie and tracker management but create your own cookie note.

### Methods

#### `cx.privacy.cookies.getCategories()`

Returns a list of categories as an array of objects with the keys `id`, `name` and `description`.

#### `cx.privacy.cookies.getCookie(cookieId)`

Returns information about a cookie/tracker as an object. The object's properties are:

| Property              | Type      | Purpose                                                            |
|-----------------------|-----------|--------------------------------------------------------------------|
| `id`                  | `Number`  | ID of the cookie                                                   |
| `identifier`          | `String`  | Identifying name of the cookie                                     |
| `name`                | `String`  | Display name of the cookie                                         |
| `order`               | `Number`  | Number used to sort the cookies                                    |
| `description`         | `String`  | Description of the cookie                                          |
| `disableable`         | `Boolean` | Boolean which is true if the cookie can be disabled by the visitor |
| `cookieNames`         | `Array`   | Array of the names of the cookies that are set                     |
| `host`                | `String`  | Name of the host the cookie is set on                              |
| `privacyStatementUrl` | `String`  | Link to the privacy statement for this cookie                      |

#### `cx.privacy.cookies.getCookiesByCategory(categoryId)`

Returns a list of cookies (see `cx.privacy.cookies.getCookie()` for the format of a single cookie) as an object indexed by the `cookieId`.

#### `cx.privacy.cookies.isCookieActive(cookieId)`

Returns `true` if the cookie is active, `false` otherwise. The parameter `cookieId` can either be the cookies ID or it's identifier.

#### `cx.privacy.cookies.isCookieCategoryActive(categoryId)`

Returns `all`, `none` or `some` depending on the states of the cookie in the given category.

#### `cx.privacy.cookies.isCookieCategoryDisableable(categoryId)`

Returns `true` if there's at least one cookie in the given category that is disableable.

#### `cx.privacy.cookies.setCookieActive(cookieId, active)`

Sets the state of the cookie identified by `cookieId` to `active`. The parameter `cookieId` can either be the cookies ID or it's identifier.

#### `cx.privacy.cookies.setCookieCategoryActive(categoryId, active)`

Sets the states of all cookies in the given category to `active`.

#### `cx.privacy.cookies.setAllCookiesActive(active)`

Sets the states of all cookies to `active`.

#### `cx.privacy.cookies.isConsentSet()`

Tells whether the user has previously made a choice which is saved in the cookie `clxCookieNote`.

### Events

The [javascript framework event](Component User Framework.md) `cookieChanged` (of scope `privacy`) is triggered each time the state of a cookie changes. This allows to react to the user's selection. Please see [end user documentation](https://cloudrexx.atlassian.net/wiki/spaces/HELP/pages/498335745) for example usage.

## Cookies set by components

If your component sets a cookie, the privacy component should be made aware of the existence of this cookie. Currently, this is only possible by directly injecting an entry into the database table `contrexx_privacy_cookie`. Please note that this is subject of change.
pan