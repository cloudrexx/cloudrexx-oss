The Text class administrates module data for all active frontend languages. The intention is to use this class as a gateway everywhere in Contrexx for translatable texts.

## Methods

-   **function __construct($text, $lang_id, $section, $key, $id=null)**
    -   Generates a new text object for a module, with key
-   **function store()**
    -   Persists the text to the database
-   **static function replace($id, $lang_id, $section, $key, $strText)**
    -   Inserts a new text into the database, or replaces an existing one in the database
-   **static function deleteByKey($section, $key)**
    -   Deletes all texts of a module with a given key (MIND YOUR HEAD!)
-   **static function getSqlSnippets($field_id, $lang_id, $section, $keys)**
    -   Returns an array with multiple snippets of SQL, to implement translated texts into an existing query
        -   'alias': Array with a field name for each key to get the resultset
        -   'field': To add into the SELECT, does not contain a comma in the begin and end.
        -   'join': To add into the JOIN

### Examples

``` php
// Update to Shop 3.0 - start clean
Text::deleteByKey('shop', self::TEXT_NAME);

// Loop over old Products {
    $id = $objResult->fields['id'];
    $name = $objResult->fields['title'];
    if (!Text::replace($id, FRONTEND_LANG_ID, 'shop',
        self::TEXT_NAME, $name)) {
        throw new Update_DatabaseException(
            "Failed to migrate Product name '$name'");
    }
// }

[...]

// Query a Product in the current frontend language
$arrSql = Text::getSqlSnippets('`product`.`id`', FRONTEND_LANG_ID, 'shop',
    array(
        'name' => self::TEXT_NAME,
        'short' => self::TEXT_SHORT,
        'long' => self::TEXT_LONG,
        'keys' => self::TEXT_KEYS,
        'code' => self::TEXT_CODE,
        'uri' => self::TEXT_URI,
    ));
$query = "
    SELECT `product`.`id`, [...], $arrSql['field']."
      FROM `".DBPREFIX."module_shop".MODULE_INDEX."_products` AS `product`".
           $arrSql['join']."
     WHERE `product`.`id`=$id";
$objResult = $objDatabase->Execute($query);
$strName = $objResult->fields['name'];
if ($strName === null) {
    // Fallback for languages not yet edited -- use any language found
    $strName = Text::getById($id, 'shop', self::TEXT_NAME)->content();
}
[...] // Other Product properties
```

More examples are in the shop since Contrexx version 3.0.

