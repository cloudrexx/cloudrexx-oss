The [System Mode](System Modes.md) `minimal` is a special mode Cloudrexx can run in.

## Accessing the Cloudrexx framework in standalone PHP script

To access the Cloudrexx framework in a standalone PHP script, all you have to do is to initialize a minimal instance of Cloudrexx. This is done with the following code:

``` php
require_once '/<path to Cloudrexx installation>/core/Core/init.php';
$cx = init('minimal');
```

From that point on, you'll have full access to the whole Cloudrexx framework from within your standalone PHP script.

### Reading session data

Once you have established a connection to Cloudrexx (as explained above) you can also access the Cloudrexx session data of the requesting user (as anything else from the Cloudrexx framework). To do so, simply initialize the session as explained in section Initialization of article [Session handling](Session handling.md). Then you can access the session data through the superglobal **$_SESSION**.

### Checking for authenticated user

You can also check if the requester is currently signed in to the Cloudrexx website and access its user data as explained in the section Authentifizierter Besucher of article [User / Group](User Group.md).

!!! note

    Manually initializing the Cloudrexx session is not required, as checking for an authenticated user will automatically initialize the session.

!!! example

    ``` php
    require_once '/<path to Cloudrexx installation>/core/Core/init.php';
    $cx = init('minimal');

    if (\FWUser::getFWUserObject()->objUser->login()) {
        // Der aktuelle Benutzer ist authentifiziert
        // Auf die Daten des aktuell authentifizierten Benutzers kann über das
        // Objekt \FWUser::getFWUserObject()->objUser zugegriffen werden.
        print "Sie sind angemeldet als ".\FWUser::getFWUserObject()->objUser->getUsername();
    } else {
        // Der aktuelle Benutzer ist nicht authentifiziert
        print "Sie sind nicht angemeldet!";
    }
    ```

