## Migrating Third-Party Code to New Versions of Cloudrexx

The following article describes how to migrate third-party code to new versions of Cloudrexx:

- [Migrating from Contrexx 1.x.x/2.x.x to Contrexx 3.0.x](Migration Guides/Migration Guide 3.0.md)
- [Migrating from Contrexx 3.0.x to Contrexx 3.1.x](Migration Guides/Migration Guide 3.1.md)
- [Migrating from Contrexx 3.1.x to Contrexx 3.2.x](Migration Guides/Migration Guide 3.2.md)
- [Migrating from Contrexx 3.2.x to Cloudrexx 5.1.x](Migration Guides/Migration Guide 5.x.md)
- [Migrating from Cloudrexx 5.1.x to Cloudrexx 5.2.x](Migration Guides/Migration Guide 5.2.md)
