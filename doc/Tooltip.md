Ab Contrexx 3 ist im gesamten Backend jQuery Tools Tooltip im Einsatz.

## How to use

Nachfolgend ein Beispiel wie eine Tooltip-Textmeldung im Backend erstellt wird:

``` html
<span class="icon-info tooltip-trigger"></span><span class="tooltip-message">{TXT_MESSAGE}</span>
```

### Aktivierung

Das notwendige JavaScript zur Verwendung der Tooltips ist standardmässig aktiviert. In Situationen, wo eine Aktivierung trotzdem notwendig ist, kann folgendermassen vorgegangen werden:

#### Server-side (PHP)

``` php
JS::activate('cx');
```

#### Client-side (JavaScript)

``` js
cx.ui.tooltip();
```

Neue Tooltips, welche nach dem Laden der Seite mittels JavaScript hinzugefügt werden, funktionieren erst nach entsprechendem Initialisieren:

``` js
cx.ui.tooltip(jQueryElementSelector);
```

### Optionen

Sollte ein Tooltip individuell angepasst werden, so können mit dem HTML Attribut "data-tooltip-options" Optionen mittels JSON-Syntax angegeben werden:

``` html
<span class="icon-info tooltip-trigger" data-tooltip-options='{"position": {"x": "center", "y": "bottom"}, "offset": {"x": 0, "y": 10}, "predelay": 100, "relative": true}'></span>
<span class="tooltip-message">{TXT_MESSAGE}</span>
```

### Hinweise

-   Mit der CSS-Klasse "icon-info" wird das blaue Info-Icon angzeigt, welches im ganzen Contrexx Backend verwendet wird. Es gibt noch weitere Klassen wie "icon-warning" und "icon-comment".
-   Die CSS-Klasse "tooltip-trigger" muss gesetzt sein, damit ein Tooltip erstellt wird.
-   Das Element, welches unmittelbar nach dem "tooltip-trigger" folgt, wird als Tooltip verwendet. Somit kann dort ein beliebiges Element platziert werden. Wenn aber eine Textmeldung angezeigt werden soll (was dem Normalfall entspricht), so muss die CSS-Klasse "tooltip-message" gesetzt sein, damit die Textmeldungen ein einheitliches Erscheinungsbild haben.
-   Standardmässig bezieht sich die Position des Tooltips auf das unmittelbare Elternelement, was den Vorteil hat, dass übergeordnete Elemente im Layout auch mit der CSS-Eigenschaft "position: relative;" versehen werden können ohne dass dies die Position des Tooltips negativ beeinflusst. Sollte sich die Position aus einem bestimmten Grund nicht auf das Elternelement sondern auf den Browserrand beziehen, so kann beim "tooltip-trigger" die Option "relative" auf "false" gesetzt werden.

### Vorteile dieses Plugins

-   Mit nur einer CSS-Klasse ("tooltip-trigger") kann ein Tooltip erstellt werden.
-   Die Tooltips haben nun ein einheitliches Erscheinungsbild. Zudem kann der Style der Tooltips schnell angepasst werden, da nur die CSS-Definitionen der Klasse "tooltip-message" angepasst werden muss.
-   Die Icons, welche auf ein Tooltip hinweisen, können schnell ausgewechselt werden, da nur der Bildpfad der entsprechenden CSS-Klasse angepasst werden muss.

