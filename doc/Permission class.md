The Permission class is a set of requirements the current user and request needs to fulfill in order to get a permission.

## Define a permission requirements set

The requirements for a permission are passed as arguments when instantiating a `Permission` instance. All arguments are optional, however the following defaults are set:

``` php
$permission = new \Cx\Core_Modules\Access\Model\Entity\Permission(
    $allowedProtocols = ['http', 'https'],
    $allowedMethods = ['get', 'post'],
    $requiresLogin = true,
    $validUserGroups = [],
    $validAccessIds = [],
    $callback = null
);
```

Set arguments as required:

1.  `allowedProtocols`: A combination of allowed protocols (in lowercase): `http` / `https`.

    !!! info
    
        If request method is `cli` then the protocol check will be skipped/ignored.
        
2.  `allowedMethods`: A combination of allowed request methods (in lowercase): `get` / `post` / `put` / `patch` / `update` / `delete` / `options` / `head` / `cli` (Use to allow access over CLI)
3.  `requiresLogin`: `true` by default. If set to `false`, no login is required to get this permission. Note: setting `validUserGroups` or `validAccessIds` implies `requiresLogin=true`.
4.  `validUserGroups`: List of group IDs. The user needs to be in one of those groups in order to get access or the user is an administrator (flag `admin` is set). If the list is empty it is ignored. Setting `validUserGroups` implies `requiresLogin=true`.
5.  `validAccessIds`: List of [access IDS](Permissions.md). The user needs have been granted at least one of these IDs in order to get access or the user is an administrator (flag `admin` is set). If the list is empty it is ignored. Setting `validAccessIds` implies `requiresLogin=true`.
6.  `callback`: A custom callback can be specified in order to check for additional requirements. Please see [Specify a requirement using a callback](Permission class.md).

When a `Permission` is evaluated, then each requirement is validated in the above mentioned order. As soon as one requirement evaluates to `false` the validation process is immediately stopped and the `Permission` won't be granted (→ `Permission::hasAccess()` will emmit `false`).

### Specify a requirement using a callback

In order to specify a `callback` you may pass an instance of [`\Cx\Core_Modules\Access\Model\Entity\Callback`](Callback.md) or an anonymous function. If you want to persist a `Permission` instance with `callback`, then the `callback` must be a persistable object. The callback will be called with one argument of type `array`. The data and structure of the passed array depends on the element the `Permission` instance is being checked on:

<table>
<thead>
<tr class="header">
<th><p>Element</p></th>
<th><p>Passed Argument</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="Component_Definition#Backend_sub_navigation" title="wikilink">Backend Component Section</a></p></td>
<td><p>Empty array</p></td>
</tr>
<tr class="even">
<td><p><a href="Exposed_methods" title="wikilink">Exposed Method</a> called as <a href="Exposed_methods#As_a_CLI_command" title="wikilink">CLI command</a></p></td>
<td rowspan="2"><p>One dimensional array of arguments passed as <code>&lt;params&gt;</code> when calling the <a href="Exposed_methods" title="wikilink">Exposed Method</a> as <a href="Exposed_methods#As_a_CLI_command" title="wikilink">CLI command</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="Exposed_methods" title="wikilink">Exposed Method</a> called over a <a href="Exposed_methods#As_a_HTTP_endpoint" title="wikilink">HTTP endpoint</a></p></td>
</tr>
<tr class="even">
<td><p><a href="Exposed_methods" title="wikilink">Exposed Method</a> called through the <a href="Exposed_methods#Using_the_Contrexx_Javascript_Framework" title="wikilink">Javascript Framework</a></p></td>
<td><p>Two dimensional array where the first dimension consists of the following elements:</p>
<ul>
<li><code>get</code>: HTTP-<code>GET</code> arguments of method call</li>
<li><code>post</code> HTTP-<code>POST</code> arguments of method call</li>
<li><code>response</code> <a href="Response" title="wikilink"><code>Response</code></a> object of the current request</li>
</ul></td>
</tr>
<tr class="odd">
<td><p><a href="Exposed_methods" title="wikilink">Exposed Method</a> called as a <a href="Exposed_methods#As_a_DataSource" title="wikilink">DataSource</a></p></td>
<td><p>N/A</p></td>
</tr>
<tr class="even">
<td><p>Custom</p></td>
<td><p><code>$params</code> as passed to <code>Permission::hasAccess($params)</code></p></td>
</tr>
</tbody>
</table>

The callback must return `true` to grant access or `false` to deny access.

## Check requirements

In order to check if the requirements are fulfilled you can simply call

``` php
$permission->hasAccess($params);
```

$params is optional. It will get passed to the registered callback (if any).

## Virtual vs. non-virtual Permission instances

By default Permission instances are virtual. They can be manually set to be non-virtual (if you need to persist one) by calling

``` php
$permission->setVirtual(false);
```

Please note that this only works if there's no callback specified or the callback is serializable.

