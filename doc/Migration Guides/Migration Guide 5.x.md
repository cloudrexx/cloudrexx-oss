## Migrating from Contrexx 3.2.x to Cloudrexx 5.x

The following sections list the changes made to the API of Contrexx from version 3.2 to 5.x

The full API documentation of version 5.x can be found at <https://api.contrexx.com/>.

### Module to Component conversion

-   see [Migrate Module to Component](../Migrate Module to Component.md)

### API changes

#### Core

| Old class name  | Class name in Cloudrexx 5                                                 |
|-----------------|---------------------------------------------------------------------------|
| ContentWorkflow | Cx\Core\ContentWorkflow\Controller\ContentWorkflow                        |
| Country         | Cx\Core\Country\Controller\Country                                        |
| CSRF            | Cx\Core\Csrf\Controller\Csrf                                              |
| DatabaseManager | Cx\Core\DatabaseManager\Controller\DatabaseManager                        |
| error           | Cx\Core\Error\Controller\Error                                            |
| Imagetype       | Cx\Core\ImageType\Controller\ImageType                                    |
| LanguageManager | Cx\Core\LanguageManager\Controller\LanguageManager                        |
| logmanager      | Cx\Core\SystemLog\Controller\SystemLog                                    |
| MailTemplate    | Cx\Core\MailTemplate\Controller\MailTemplate                              |
| modulemanager   | Cx\Core\ComponentManager\Controller\ComponentManager                      |
| serverSettings  | Cx\Core\SystemInfo\Controller\SystemInfo                                  |
| SettingDb       | [Cx\Core\Setting\Controller\Setting](../Core Setting.md) |
| settingsManager | Cx\Core\Config\Controller\Config                                          |
| skins           | Cx\Core\ViewManager\Controller\ViewManager                                |
| Security        | Cx\Core\Security\Controller\ComponentController                           |

##### Controller

###### Cx

The method \Cx\Core\Controller\Cx::**getRequest()** does now return an instance of **\Cx\Core\Routing\Model\Entity\Request**. The formerly returned object of **\Cx\Core\Routing\Url** can now be accessed by the method **getUrl()** of the returned object of **\Cx\Core\Routing\Model\Entity\Request**: // before Cloudrexx 5 $url = \Env::get('cx')-\>getRequest();

// since Cloudrexx 5 $url = \Env::get('cx')-\>getRequest()-\>getUrl();

##### Json

The interface **Cx\Core\Json\JsonAdapter** has been extended by the following method that must be implemented by every *JsonAdapter* that implements this interface: public function getDefaultPermissions(); For an in-depth explanation of the new *Security System* of *JsonData* refer to its [documentation](../Exposed methods.md#getdefaultpermissions).

##### Routing

The constant *MODULE_INDEX* is no longer supported and therefore can't be used any longer.  
We implemented a way to easily copy a component via the command line using the workbench. At the time of writing this article the workbench is still a tool in development and some functions might not work at all or not as desired. The copy command is one of the commands which doesn't work at all. The workbench is available through our Bitbucket-repository [<https://bitbucket.org/cloudrexx/>](https://bitbucket.org/cloudrexx/).

#### Core Modules

##### Access

| Old class name | Class name in Cloudrexx 5                       |
|----------------|-------------------------------------------------|
| AccessManager  | Cx\Core_Modules\Access\Controller\AccessManager |
| Access         | Cx\Core_Modules\Access\Controller\Access        |
| AccessLib      | Cx\Core_Modules\Access\Controller\AccessLib     |
| Access_Blocks  | Cx\Core_Modules\Access\Controller\AccessBlocks  |

##### Alias

| Old class name | Class name in Cloudrexx 5                     |
|----------------|-----------------------------------------------|
| AliasAdmin     | Cx\Core_Modules\Alias\Controller\AliasManager |
| aliasLib       | Cx\Core_Modules\Alias\Controller\AliasLib     |

##### Cache

| Old class name                                                 | Class name in Cloudrexx 5                                             |
|----------------------------------------------------------------|-----------------------------------------------------------------------|
| CacheManager                                                   | Cx\Core_Modules\Cache\Controller\CacheManager                         |
| Cache                                                          | Cx\Core_Modules\Cache\Controller\Cache                                |
| cacheLib                                                       | Cx\Core_Modules\Cache\Controller\CacheLib                             |
| Cx\Core_Modules\cache\lib\Doctrine\CacheDriver\Cache           | Cx\Core_Modules\Cache\Controller\Doctrine\CacheDriver\Cache           |
| Cx\Core_Modules\cache\lib\Doctrine\CacheDriver\CacheProvider   | Cx\Core_Modules\Cache\Controller\Doctrine\CacheDriver\CacheProvider   |
| Cx\Core_Modules\cache\lib\Doctrine\CacheDriver\FileCache       | Cx\Core_Modules\Cache\Controller\Doctrine\CacheDriver\FileCache       |
| Cx\Core_Modules\cache\lib\Doctrine\CacheDriver\FilesystemCache | Cx\Core_Modules\Cache\Controller\Doctrine\CacheDriver\FilesystemCache |
| Cx\Core_Modules\Cache\lib\Doctrine\CacheDriver\MemcachedCache  | Cx\Core_Modules\Cache\Controller\Doctrine\CacheDriver\MemcachedCache  |

##### Contact

| Old class name | Class name in Cloudrexx 5                         |
|----------------|---------------------------------------------------|
| ContactManager | Cx\Core_Modules\Contact\Controller\ContactManager |
| Contact        | Cx\Core_Modules\Contact\Controller\Contact        |
| ContactLib     | Cx\Core_Modules\Contact\Controller\ContactLib     |

##### FileBrowser

| Old class name | Class name in Cloudrexx 5                          |
|----------------|----------------------------------------------------|
| FileBrowser    | Cx\Core_Modules\FileBrowser\Controller\FileBrowser |

The FileBrowser will be replaced by the new MediaBrowser

[Development_Mediabrowser](../Mediabrowser.md)

##### Uploader

The `Upload` component has been replaced by the more modern upload handler [`Uploader`](../Uploader.md) in version 5.0.0.
To migrate, all calls to the old uploader have to be removed:

``` php
/**
 * Name of the upload instance
 */
$uploaderInstanceName = 'exposed_combo_uploader';
$uploaderWidgetName = 'uploadWidget';
```

``` php
/**
 * jQuery selector of the HTML-element where the upload folder-widget shall be put in
 */
$uploaderFolderWidgetContainer = '#uploadFormField_uploadWidget';
```

``` php
$uploader = \Cx\Core_Modules\Upload\Controller\UploadFactory::getInstance()->newUploader('exposedCombo');
```

Then the upload instead has to be replaced by the new uploader:

``` php
$uploader = new \Cx\Core_Modules\Uploader\Model\Entity\Uploader();
```

Finally, the server-side callback has to be migrated to the new uploader. The name of the callback-method used by the new uploader is the same as for the old one. So if your new uploader instance is created with the same name ($uploader) you can leave it as it is.

``` php
$uploader->setFinishedCallback(array(
    '<file>',
    '<class>',
    '<method>'
));
```

!!! example

    ``` php
    $uploader = new \Cx\Core_Modules\Uploader\Model\Entity\Uploader();
    $uploader->setFinishedCallback(
        array(
            '/Modules/Gallery/Controller/GalleryManager.class.php',
            '\Cx\Modules\Gallery\Controller\GalleryManager',
            'uploadFinished'
        )
    );
    ```

    ``` php
    class GalleryManager {
        function uploadFinished(
            $tempPath, $tempWebPath, $data, $uploadId, $fileInfos
        ) {
            return array(
                $this->cx->getWebsiteImagesContentPath(),
                $this->cx->getWebsiteImagesContentWebPath()
            );
        }
    }
    ```

If you also have a folder widget displaying the files which the user has uploaded you have to migrate this too.

Replace the following code with the new folder widget instance.

``` php
$folderWidget = $f->newFolderWidget($tup[0].'/'.$tup[2]);
```

New code for folder widget instance.

``` php
$folderWidget = new \Cx\Core_Modules\MediaBrowser\Model\Entity\FolderWidget($pathToFolder);
```

To place the folder widget html in the template you can use the getXHtml method.

``` php
$template->setVariable(array(
    'FOLDER_WIDGET_CODE' => $folderWidget->getXHtml(),
));
```

If you need to refresh the folder widget after you've uploaded a new file you can use the following code:

``` php
$uploader->setCallback('JsCallback');
$javascript = "
<script type=\"text/javascript\">
function JsCallback(callback) {
        angular.element('#mediaBrowserfolderWidget_{$folderWidget->getId()}').scope().refreshBrowser();
}
</script>";
```

##### Home

| Old class name | Class name in Cloudrexx 5            |
|----------------|--------------------------------------|
| myAdmin        | Cx\Core_Modules\Home\Controller\Home |

##### NetTools

| Old class name  | Class name in Cloudrexx 5                           |
|-----------------|-----------------------------------------------------|
| NetTools        | Cx\Core_Modules\NetTools\Controller\NetTools        |
| netToolsManager | Cx\Core_Modules\NetTools\Controller\NetToolsManager |

##### News

| Old class name     | Class name in Cloudrexx 5                          |
|--------------------|----------------------------------------------------|
| newsManager        | Cx\Core_Modules\News\Controller\NewsManager        |
| news               | Cx\Core_Modules\News\Controller\News               |
| newsHeadlines      | Cx\Core_Modules\News\Controller\NewsHeadlines      |
| newsLibrary        | Cx\Core_Modules\News\Controller\NewsLibrary        |
| newsRecentComments | Cx\Core_Modules\News\Controller\NewsRecentComments |
| Teasers            | Cx\Core_Modules\News\Controller\Teasers            |
| newsTop            | Cx\Core_Modules\News\Controller\NewsTop            |

##### Search

| Old class name | Class name in Cloudrexx 5                       |
|----------------|-------------------------------------------------|
| SearchManager  | Cx\Core_Modules\Search\Controller\SearchManager |
| Search         | Cx\Core_Modules\Search\Controller\Search        |

See also section [Search Integration](#search-integration) on how to integrate a *Component*'s *content* into *Cloudrexx's fulltext-search* module.

##### Sitemap

| Old class name | Class name in Cloudrexx 5                  |
|----------------|--------------------------------------------|
| sitemap        | Cx\Core_Modules\Sitemap\Controller\Sitemap |

##### Stats

| Old class name | Class name in Cloudrexx 5                     |
|----------------|-----------------------------------------------|
| stats          | Cx\Core_Modules\Stats\Controller\Stats        |
| statsLibrary   | Cx\Core_Modules\Stats\Controller\StatsLibrary |
| makeGraph      | Cx\Core_Modules\Stats\Controller\MakeGraph    |
| counter        | Cx\Core_Modules\Stats\Controller\Counter      |

#### Modules

##### Block

| Old class name | Class name in Cloudrexx 5                |
|----------------|------------------------------------------|
| blockManager   | Cx\Modules\Block\Controller\BlockManager |
| block          | Cx\Modules\Block\Controller\Block        |
| blockLibrary   | Cx\Modules\Block\Controller\BlockLibrary |

##### Blog

| Old class name  | Class name in Cloudrexx 5                  |
|-----------------|--------------------------------------------|
| Blog            | Cx\Modules\Blog\Controller\Blog            |
| BlogAdmin       | Cx\Modules\Blog\Controller\BlogManager     |
| BlogHomeContent | Cx\Modules\Blog\Controller\BlogHomeContent |
| BlogLibrary     | Cx\Modules\Blog\Controller\BlogLibrary     |

##### Calendar

| Old class name              | Class name in Cloudrexx 5                                  |
|-----------------------------|------------------------------------------------------------|
| CalendarManager             | Cx\Modules\Calendar\Controller\CalendarManager             |
| Calendar                    | Cx\Modules\Calendar\Controller\Calendar                    |
| CalendarCategory            | Cx\Modules\Calendar\Controller\CalendarCategory            |
| CalendarCategoryManager     | Cx\Modules\Calendar\Controller\CalendarCategoryManager     |
| CalendarEvent               | Cx\Modules\Calendar\Controller\CalendarEvent               |
| CalendarEventManager        | Cx\Modules\Calendar\Controller\CalendarEventManager        |
| CalendarFeed                | Cx\Modules\Calendar\Controller\CalendarFeed                |
| CalendarForm                | Cx\Modules\Calendar\Controller\CalendarForm                |
| CalendarFormManager         | Cx\Modules\Calendar\Controller\CalendarFormManager         |
| CalendarHeadlines           | Cx\Modules\Calendar\Controller\CalendarHeadlines           |
| CalendarHost                | Cx\Modules\Calendar\Controller\CalendarHost                |
| CalendarHostManager         | Cx\Modules\Calendar\Controller\CalendarHostManager         |
| CalendarLibrary             | Cx\Modules\Calendar\Controller\CalendarLibrary             |
| CalendarMail                | Cx\Modules\Calendar\Controller\CalendarMail                |
| CalendarMailManager         | Cx\Modules\Calendar\Controller\CalendarMailManager         |
| CalendarPayment             | Cx\Modules\Calendar\Controller\CalendarPayment             |
| CalendarRegistration        | Cx\Modules\Calendar\Controller\CalendarRegistration        |
| CalendarRegistrationManager | Cx\Modules\Calendar\Controller\CalendarRegistrationManager |
| CalendarSettings            | Cx\Modules\Calendar\Controller\CalendarSettings            |
| CalendarWebserviceClient    | Cx\Modules\Calendar\Controller\CalendarWebserviceClient    |
| CalendarWebserviceServer    | Cx\Modules\Calendar\Controller\CalendarWebserviceServer    |

##### Checkout

| Old class name    | Class name in Cloudrexx 5                        |
|-------------------|--------------------------------------------------|
| CheckoutManager   | Cx\Modules\Checkout\Controller\CheckoutManager   |
| Checkout          | Cx\Modules\Checkout\Controller\Checkout          |
| CheckoutLibrary   | Cx\Modules\Checkout\Controller\CheckoutLibrary   |
| Countries         | Cx\Modules\Checkout\Controller\Countries         |
| SettingsGeneral   | Cx\Modules\Checkout\Controller\SettingsGeneral   |
| SettingsMails     | Cx\Modules\Checkout\Controller\SettingsMails     |
| SettingsYellowpay | Cx\Modules\Checkout\Controller\SettingsYellowpay |
| Transaction       | Cx\Modules\Checkout\Controller\Transaction       |

##### Crm

| Old class name                         | Class name in Cloudrexx 5                          |
|----------------------------------------|----------------------------------------------------|
| CrmManager                             | Cx\Modules\Crm\Controller\CrmManager               |
| Settings                               | Cx\Modules\Crm\Controller\CrmSettings              |
| Cx\Modules\crm\lib\controllers\JsonCrm | Cx\Modules\Crm\Controller\JsonCrm                  |
| crmInterface                           | Cx\Modules\Crm\Controller\CrmInterface             |
| crmTask                                | Cx\Modules\Crm\Controller\CrmTask                  |
| Event                                  | Cx\Modules\Crm\Model\Entity\CrmEvent               |
| EventDispatcher                        | Cx\Modules\Crm\Controller\CrmEventDispatcher       |
| DefaultEventHandler                    | Cx\Modules\Crm\Model\Events\CrmDefaultEventHandler |
| EventHandler                           | Cx\Modules\Crm\Model\Events\CrmEventHandler        |
| crmContact                             | Cx\Modules\Crm\Model\Entity\CrmContact             |
| IndustryType                           | Cx\Modules\Crm\Model\Entity\IndustryType           |
| membership                             | Cx\Modules\Crm\Model\Entity\Membership             |
| vcard                                  | Cx\Modules\Crm\Controller\CrmVcard                 |
| CrmLibrary                             | Cx\Modules\Crm\Controller\CrmLibrary               |
| Csv_bv                                 | Cx\Modules\Crm\Controller\CrmCsv                   |
| Javascript                             | Cx\Modules\Crm\Controller\CrmJavascript            |
| Sorter                                 | Cx\Modules\Crm\Controller\Sorter                   |

##### Data

| Old class name  | Class name in Cloudrexx 5                  |
|-----------------|--------------------------------------------|
| Data            | Cx\Modules\Data\Controller\Data            |
| DataAdmin       | Cx\Modules\Data\Controller\DataAdmin       |
| dataBlocks      | Cx\Modules\Data\Controller\DataBlocks      |
| DataHomeContent | Cx\Modules\Data\Controller\DataHomeContent |
| DataLibrary     | Cx\Modules\Data\Controller\DataLibrary     |

##### Directory

| Old class name                | Class name in Cloudrexx 5                        |
|-------------------------------|--------------------------------------------------|
| rssDirectory                  | Cx\Modules\Directory\Controller\Directory        |
| rssDirectory(admin.class.php) | Cx\Modules\Directory\Controller\DirectoryManager |
| dirHomeContent                | Cx\Modules\Directory\Controller\DirHomeContent   |
| directoryLibrary              | Cx\Modules\Directory\Controller\DirectoryLibrary |
| rssFeed                       | Cx\Modules\Directory\Controller\RssFeed          |

##### DocSys

| Old class name | Class name in Cloudrexx 5                  |
|----------------|--------------------------------------------|
| docSys         | Cx\Modules\DocSys\Controller\DocSys        |
| docSysManager  | Cx\Modules\DocSys\Controller\DocSysManager |
| docSysLibrary  | Cx\Modules\DocSys\Controller\DocSysLibrary |
| rssFeed        | Cx\Modules\DocSys\Controller\RssFeed       |

##### Downloads

| Old class name             | Class name in Cloudrexx 5                        |
|----------------------------|--------------------------------------------------|
| Category                   | Cx\Modules\Downloads\Controller\Category         |
| downloads                  | Cx\Modules\Downloads\Controller\Downloads        |
| downloads(admin.class.php) | Cx\Modules\Downloads\Controller\DownloadsManager |
| Download                   | Cx\Modules\Downloads\Controller\Download         |
| DownloadsLibrary           | Cx\Modules\Downloads\Controller\DownloadsLibrary |
| Group                      | Cx\Modules\Downloads\Controller\Group            |

##### Ecard

| Old class name         | Class name in Cloudrexx 5                |
|------------------------|------------------------------------------|
| ecard                  | Cx\Modules\Ecard\Controller\Ecard        |
| ecard(admin.class.php) | Cx\Modules\Ecard\Controller\EcardManager |

##### Egov

| Old class name        | Class name in Cloudrexx 5              |
|-----------------------|----------------------------------------|
| eGov                  | Cx\Modules\Egov\Controller\Egov        |
| eGov(admin.class.php) | Cx\Modules\Egov\Controller\EgovManager |
| eGovLibrary           | Cx\Modules\Egov\Controller\EgovLibrary |
| paypal_class          | Cx\Modules\Egov\Controller\Paypal      |

##### Feed

| Old class name | Class name in Cloudrexx 5              |
|----------------|----------------------------------------|
| feed           | Cx\Modules\Feed\Controller\Feed        |
| feedManager    | Cx\Modules\Feed\Controller\FeedManager |
| feedLibrary    | Cx\Modules\Feed\Controller\FeedLibrary |
| NewsML         | Cx\Modules\Feed\Controller\NewsML      |

##### FileSharing

| Old class name   | Class name in Cloudrexx 5                            |
|------------------|------------------------------------------------------|
| Filesharing      | Cx\Modules\FileSharing\Controller\FileSharing        |
| FilesharingAdmin | Cx\Modules\FileSharing\Controller\FileSharingManager |
| FilesharingLib   | Cx\Modules\FileSharing\Controller\FileSharingLib     |

##### Forum

| Old class name   | Class name in Cloudrexx 5                    |
|------------------|----------------------------------------------|
| Forum            | Cx\Modules\Forum\Controller\Forum            |
| ForumAdmin       | Cx\Modules\Forum\Controller\ForumAdmin       |
| ForumHomeContent | Cx\Modules\Forum\Controller\ForumHomeContent |
| ForumLibrary     | Cx\Modules\Forum\Controller\ForumLibrary     |

##### Gallery

| Old class name     | Class name in Cloudrexx 5                        |
|--------------------|--------------------------------------------------|
| Gallery            | Cx\Modules\Gallery\Controller\Gallery            |
| galleryManager     | Cx\Modules\Gallery\Controller\GalleryManager     |
| GalleryLibrary     | Cx\Modules\Gallery\Controller\GalleryLibrary     |
| GalleryHomeContent | Cx\Modules\Gallery\Controller\GalleryHomeContent |
| GallerySql         | Cx\Modules\Gallery\Controller\GallerySql         |

##### GuestBook

| Old class name   | Class name in Cloudrexx 5                        |
|------------------|--------------------------------------------------|
| Guestbook        | Cx\Modules\GuestBook\Controller\GuestBook        |
| GuestbookManager | Cx\Modules\GuestBook\Controller\GuestBookManager |
| GuestbookLibrary | Cx\Modules\GuestBook\Controller\GuestBookLibrary |

##### Jobs

| Old class name | Class name in Cloudrexx 5              |
|----------------|----------------------------------------|
| jobs           | Cx\Modules\Jobs\Controller\Jobs        |
| jobsManager    | Cx\Modules\Jobs\Controller\JobsManager |
| jobsLibrary    | Cx\Modules\Jobs\Controller\JobsLibrary |
| rssFeed        | Cx\Modules\Jobs\Controller\RssFeed     |

##### Knowledge

| Old class name     | Class name in Cloudrexx 5                          |
|--------------------|----------------------------------------------------|
| DatabaseError      | Cx\Modules\Knowledge\Controller\DatabaseError      |
| Knowledge          | Cx\Modules\Knowledge\Controller\Knowledge          |
| KnowledgeAdmin     | Cx\Modules\Knowledge\Controller\KnowledgeAdmin     |
| KnowledgeArticles  | Cx\Modules\Knowledge\Controller\KnowledgeArticles  |
| KnowledgeCategory  | Cx\Modules\Knowledge\Controller\KnowledgeCategory  |
| KnowledgeInterface | Cx\Modules\Knowledge\Controller\KnowledgeInterface |
| KnowledgeLibrary   | Cx\Modules\Knowledge\Controller\KnowledgeLibrary   |
| KnowledgeSettings  | Cx\Modules\Knowledge\Controller\KnowledgeSettings  |
| KnowledgeTags      | Cx\Modules\Knowledge\Controller\KnowledgeTags      |
| Search             | Cx\Modules\Knowledge\Controller\Search             |
| SearchInterface    | Cx\Modules\Knowledge\Controller\SearchInterface    |
| searchKnowledge    | Cx\Modules\Knowledge\Controller\SearchKnowledge    |
| searchResponse     | Cx\Modules\Knowledge\Controller\SearchResponse     |
| TagCloud           | Cx\Modules\Knowledge\Controller\TagCloud           |

##### Livecam

| Old class name | Class name in Cloudrexx 5                    |
|----------------|----------------------------------------------|
| Livecam        | Cx\Modules\Livecam\Controller\Livecam        |
| LivecamLibrary | Cx\Modules\Livecam\Controller\LivecamLibrary |
| LivecamManager | Cx\Modules\Livecam\Controller\LivecamManager |

##### Market

| Old class name          | Class name in Cloudrexx 5                  |
|-------------------------|--------------------------------------------|
| Market                  | Cx\Modules\Market\Controller\Market        |
| Market(admin.class.php) | Cx\Modules\Market\Controller\MarketManager |
| marketLibrary           | Cx\Modules\Market\Controller\MarketLibrary |
| MarketPayPal            | Cx\Modules\Market\Controller\MarketPayPal  |

##### MediaDir

| Old class name                             | Class name in Cloudrexx 5                                                  |
|--------------------------------------------|----------------------------------------------------------------------------|
| mediaDirectory                             | Cx\Modules\MediaDir\Controller\MediaDirectory                              |
| mediaDirectoryManager                      | Cx\Modules\MediaDir\Controller\MediaDirectoryManager                       |
| mediaDirectoryPlaceholders                 | Cx\Modules\MediaDir\Controller\MediaDirectoryPlaceholders                  |
| mediaDirectoryAddStep                      | Cx\Modules\MediaDir\Controller\MediaDirectoryAddStep                       |
| mediaDirectoryCategory                     | Cx\Modules\MediaDir\Controller\MediaDirectoryCategory                      |
| mediaDirectoryComment                      | Cx\Modules\MediaDir\Controller\MediaDirectoryComment                       |
| mediaDirectoryEntry                        | Cx\Modules\MediaDir\Controller\MediaDirectoryEntry                         |
| mediaDirectoryExport                       | Cx\Modules\MediaDir\Controller\MediaDirectoryExport                        |
| mediaDirectoryForm                         | Cx\Modules\MediaDir\Controller\MediaDirectoryForm                          |
| mediaDirectoryImport                       | Cx\Modules\MediaDir\Controller\MediaDirectoryImport                        |
| mediaDirectoryInputfield                   | Cx\Modules\MediaDir\Controller\MediaDirectoryInputfield                    |
| mediaDirectoryInterfaces                   | Cx\Modules\MediaDir\Controller\MediaDirectoryInterfaces                    |
| mediaDirectoryLevel                        | Cx\Modules\MediaDir\Controller\MediaDirectoryLevel                         |
| MediaDirectoryAccessIDs                    | Cx\Modules\MediaDir\Controller\MediaDirectoryAccessIDs                     |
| mediaDirectoryLibrary                      | Cx\Modules\MediaDir\Controller\MediaDirectoryLibrary                       |
| mediaDirectoryMail                         | Cx\Modules\MediaDir\Controller\MediaDirectoryMail                          |
| mediaDirectoryPlaceholder                  | Cx\Modules\MediaDir\Controller\MediaDirectoryPlaceholder                   |
| mediaDirectorySearch                       | Cx\Modules\MediaDir\Controller\MediaDirectorySearch                        |
| mediaDirectorySettings                     | Cx\Modules\MediaDir\Controller\MediaDirectorySettings                      |
| mediaDirectoryVoting                       | Cx\Modules\MediaDir\Controller\MediaDirectoryVoting                        |
| mediaDirectoryInputfieldAccounts           | Cx\Modules\MediaDir\Model\Entity\MediaDirectoryInputfieldAccounts          |
| mediaDirectoryInputfieldAdd_step           | Cx\Modules\MediaDir\Model\Entity\MediaDirectoryInputfieldAddStep           |
| mediaDirectoryInputfieldCheckbox           | Cx\Modules\MediaDir\Model\Entity\MediaDirectoryInputfieldCheckbox          |
| mediaDirectoryInputfieldClassification     | Cx\Modules\MediaDir\Model\Entity\MediaDirectoryInputfieldClassification    |
| mediaDirectoryInputfieldCountry            | Cx\Modules\MediaDir\Model\Entity\MediaDirectoryInputfieldCountry           |
| mediaDirectoryInputfieldDownloads          | Cx\Modules\MediaDir\Model\Entity\MediaDirectoryInputfieldDownloads         |
| mediaDirectoryInputfieldDropdown           | Cx\Modules\MediaDir\Model\Entity\MediaDirectoryInputfieldDropdown          |
| mediaDirectoryInputfieldFile               | Cx\Modules\MediaDir\Model\Entity\MediaDirectoryInputfieldFile              |
| mediaDirectoryInputfieldGoogle_map         | Cx\Modules\MediaDir\Model\Entity\MediaDirectoryInputfieldGoogleMap         |
| mediaDirectoryInputfieldGoogle_weather     | Cx\Modules\MediaDir\Model\Entity\MediaDirectoryInputfieldGoogleWeather     |
| mediaDirectoryInputfieldImage              | Cx\Modules\MediaDir\Model\Entity\MediaDirectoryInputfieldImage             |
| inputfield                                 | Cx\Modules\MediaDir\Model\Entity\Inputfield                                |
| mediaDirectoryInputfieldLabel              | Cx\Modules\MediaDir\Model\Entity\MediaDirectoryInputfieldLabel             |
| mediaDirectoryInputfieldLink               | Cx\Modules\MediaDir\Model\Entity\MediaDirectoryInputfieldLink              |
| mediaDirectoryInputfieldLink_group         | Cx\Modules\MediaDir\Model\Entity\MediaDirectoryInputfieldLinkGroup         |
| mediaDirectoryInputfieldMail               | Cx\Modules\MediaDir\Model\Entity\MediaDirectoryInputfieldMail              |
| mediaDirectoryInputfieldProduct_attributes | Cx\Modules\MediaDir\Model\Entity\MediaDirectoryInputfieldProductAttributes |
| mediaDirectoryInputfieldRadio              | Cx\Modules\MediaDir\Model\Entity\MediaDirectoryInputfieldRadio             |
| mediaDirectoryInputfieldReferences         | Cx\Modules\MediaDir\Model\Entity\MediaDirectoryInputfieldReferences        |
| mediaDirectoryInputfieldRelation           | Cx\Modules\MediaDir\Model\Entity\MediaDirectoryInputfieldRelation          |
| mediaDirectoryInputfieldRelation_group     | Cx\Modules\MediaDir\Model\Entity\MediaDirectoryInputfieldRelationGroup     |
| mediaDirectoryInputfieldResponsibles       | Cx\Modules\MediaDir\Model\Entity\MediaDirectoryInputfieldResponsibles      |
| mediaDirectoryInputfieldText               | Cx\Modules\MediaDir\Model\Entity\MediaDirectoryInputfieldText              |
| mediaDirectoryInputfieldTextarea           | Cx\Modules\MediaDir\Model\Entity\MediaDirectoryInputfieldTextarea          |
| mediaDirectoryInputfieldTitle              | Cx\Modules\MediaDir\Model\Entity\MediaDirectoryInputfieldTitle             |
| mediaDirectoryInputfieldWysiwyg            | Cx\Modules\MediaDir\Model\Entity\MediaDirectoryInputfieldWysiwyg           |

##### MemberDir

| Old class name   | Class name in Cloudrexx 5                        |
|------------------|--------------------------------------------------|
| memberDir        | Cx\Modules\MemberDir\Controller\MemberDir        |
| MemberDirLibrary | Cx\Modules\MemberDir\Controller\MemberDirLibrary |
| MemberDirManager | Cx\Modules\MemberDir\Controller\MemberDirManager |

##### Newsletter

| Old class name              | Class name in Cloudrexx 5                          |
|-----------------------------|----------------------------------------------------|
| newsletter                  | Cx\Modules\Newsletter\Controller\Newsletter        |
| newsletter(admin.class.php) | Cx\Modules\Newsletter\Controller\NewsletterManager |
| NewsletterLib               | Cx\Modules\Newsletter\Controller\NewsletterLib     |

##### Podcast

| Old class name     | Class name in Cloudrexx 5                        |
|--------------------|--------------------------------------------------|
| podcast            | Cx\Modules\Podcast\Controller\Podcast            |
| podcastManager     | Cx\Modules\Podcast\Controller\PodcastManager     |
| podcastHomeContent | Cx\Modules\Podcast\Controller\PodcastHomeContent |
| podcastLib         | Cx\Modules\Podcast\Controller\PodcastLib         |

##### Recommend

| Old class name   | Class name in Cloudrexx 5                        |
|------------------|--------------------------------------------------|
| Recommend        | Cx\Modules\Recommend\Controller\Recommend        |
| RecommendLibrary | Cx\Modules\Recommend\Controller\RecommendLibrary |
| RecommendManager | Cx\Modules\Recommend\Controller\RecommendManager |

##### Shop

| Old class name    | Class name in Cloudrexx 5                    |
|-------------------|----------------------------------------------|
| Attribute         | Cx\Modules\Shop\Controller\Attribute         |
| Attributes        | Cx\Modules\Shop\Controller\Attributes        |
| Cart              | Cx\Modules\Shop\Controller\Cart              |
| Coupon            | Cx\Modules\Shop\Controller\Coupon            |
| CsvImport         | Cx\Modules\Shop\Controller\CSVimport         |
| Csv_bv            | Cx\Modules\Shop\Controller\CsvBv             |
| Currency          | Cx\Modules\Shop\Controller\Currency          |
| Customer          | Cx\Modules\Shop\Controller\Customer          |
| Customers         | Cx\Modules\Shop\Controller\Customers         |
| Discount          | Cx\Modules\Shop\Controller\Discount          |
| Distribution      | Cx\Modules\Shop\Controller\Distribution      |
| History           | Cx\Modules\Shop\Controller\History           |
| ShopMail          | Cx\Modules\Shop\Controller\Mail              |
| Manufacturer      | Cx\Modules\Shop\Controller\Manufacturer      |
| Order             | Cx\Modules\Shop\Controller\Order             |
| Orders            | Cx\Modules\Shop\Controller\Orders            |
| Payment           | Cx\Modules\Shop\Controller\Payment           |
| PaymentProcessing | Cx\Modules\Shop\Controller\PaymentProcessing |
| Pricelist         | Cx\Modules\Shop\Controller\PriceList         |
| Product           | Cx\Modules\Shop\Controller\Product           |
| Products          | Cx\Modules\Shop\Controller\Products          |
| Shipment          | Cx\Modules\Shop\Controller\Shipment          |
| Shop              | Cx\Modules\Shop\Controller\Shop              |
| ShopCategories    | Cx\Modules\Shop\Controller\ShopCategories    |
| ShopCategory      | Cx\Modules\Shop\Controller\ShopCategory      |
| ShopCountry       | Cx\Modules\Shop\Controller\ShopCountry       |
| ShopLibrary       | Cx\Modules\Shop\Controller\ShopLibrary       |
| ShopMail          | Cx\Modules\Shop\Controller\ShopMail          |
| Shopmanager       | Cx\Modules\Shop\Controller\ShopManager       |
| ShopSettings      | Cx\Modules\Shop\Controller\ShopSettings      |
| Vat               | Cx\Modules\Shop\Controller\Vat               |
| Weight            | Cx\Modules\Shop\Controller\Weight            |
| Zones             | Cx\Modules\Shop\Controller\Zones             |

##### Survey

| Old class name          | Class name in Cloudrexx 5                          |
|-------------------------|----------------------------------------------------|
| survey                  | Cx\Modules\Survey\Controller\Survey                |
| survey(admin.class.php) | Cx\Modules\Survey\Controller\SurveyManager         |
| SurveyEntry             | Cx\Modules\Survey\Controller\SurveyEntry           |
| JsonSurvey              | Cx\Modules\Survey\Controller\JsonSurvey            |
| SurveyLibrary           | Cx\Modules\Survey\Controller\SurveyLibrary         |
| SurveyQuestion          | Cx\Modules\Survey\Controller\SurveyQuestion        |
| SurveyQuestionManager   | Cx\Modules\Survey\Controller\SurveyQuestionManager |

##### U2u

| Old class name | Class name in Cloudrexx 5            |
|----------------|--------------------------------------|
| u2u            | Cx\Modules\U2u\Controller\U2u        |
| u2uAdmin       | Cx\Modules\U2u\Controller\U2uAdmin   |
| u2uLibrary     | Cx\Modules\U2u\Controller\U2uLibrary |

##### Voting

| Old class name | Class name in Cloudrexx 5                  |
|----------------|--------------------------------------------|
| Voting         | Cx\Modules\Voting\Controller\Voting        |
| votingmanager  | Cx\Modules\Voting\Controller\VotingManager |

#### Lib

##### Captcha

| Old class name | Class name in Cloudrexx 5                    |
|----------------|----------------------------------------------|
| FWCaptcha      | Cx\Core_Modules\Captcha\Controller\Captcha   |
| reCAPTCHA      | Cx\Core_Modules\Captcha\Controller\ReCaptcha |

### Application section changes

#### Core

| Old name      | New name         |
|---------------|------------------|
| settings      | Config           |
| modulemanager | ComponentManager |
| workflow      | ContentWorkflow  |
| country       | Country          |
| CSRF          | Csrf             |
| dbm           | DatabaseManager  |
| error         | Error            |
| Imagetype     | ImageType        |
| JavaScript    | JavaScript       |
| jsondata      | JsonData         |
| language      | LanguageManager  |
| MailTemplate  | MailTemplate     |
| Message       | Message          |
| Security      | Security         |
| session       | Session          |
| server        | SystemInfo       |
| log           | SystemLog        |
| skins         | ViewManager      |

#### Core Modules

| Old name    | New name    |
|-------------|-------------|
| access      | Access      |
| agb         | Agb         |
| alias       | Alias       |
| cache       | Cache       |
| captcha     | Captcha     |
| contact     | Contact     |
| fileBrowser | FileBrowser |
| home        | Home        |
| ids         | Ids         |
| imprint     | Imprint     |
| login       | Login       |
| media       | Media       |
| nettools    | NetTools    |
| news        | News        |
| privacy     | Privacy     |
| search      | Search      |
| sitemap     | Sitemap     |
| stats       | Stats       |

#### Modules

| Old name    | New name    |
|-------------|-------------|
| block       | Block       |
| blog        | Blog        |
| calendar    | Calendar    |
| checkout    | Checkout    |
| crm         | Crm         |
| data        | Data        |
| directory   | Directory   |
| docsys      | DocSys      |
| downloads   | Downloads   |
| ecard       | Ecard       |
| egov        | Egov        |
| feed        | Feed        |
| filesharing | FileSharing |
| forum       | Forum       |
| gallery     | Gallery     |
| guestbook   | GuestBook   |
| jobs        | Jobs        |
| knowledge   | Knowledge   |
| livecam     | Livecam     |
| market      | Market      |
| mediadir    | MediaDir    |
| memberdir   | MemberDir   |
| newsletter  | Newsletter  |
| podcast     | Podcast     |
| recommend   | Recommend   |
| shop        | Shop        |
| u2u         | U2u         |
| voting      | Voting      |

### Global Constants Replacements

| Deprecated constant           | Code Base Path                        | Website Path                                           |
|-------------------------------|---------------------------------------|--------------------------------------------------------|
| ASCMS_PATH                    | Cx::getCodeBasePath()                 | Cx::getWebsitePath()                                   |
| ASCMS_PATH_OFFSET             | Cx::getCodeBaseOffsetPath()           | Cx::getWebsiteOffsetPath()                             |
| ASCMS_INSTANCE_PATH           |                                       | Cx::getWebsitePath()                                   |
| ASCMS_INSTANCE_OFFSET         |                                       | Cx::getWebsiteOffsetPath()                             |
| ASCMS_DOCUMENT_ROOT           | Cx::getCodeBaseDocumentRootPath()     | Cx::getWebsiteDocumentRootPath()                       |
| ASCMS_CUSTOMIZING_PATH        |                                       | Cx::getWebsiteCustomizingPath()                        |
| ASCMS_CUSTOMIZING_WEB_PATH    |                                       | Cx::getWebsiteCustomizingWebPath()                     |
| ASCMS_ADMIN_TEMPLATE_PATH     | Cx::getCodeBaseAdminTemplatePath()    |                                                        |
| ASCMS_ADMIN_TEMPLATE_WEB_PATH | Cx::getCodeBaseAdminTemplateWebPath() |                                                        |
| ASCMS_FRAMEWORK_PATH          | Cx::getCodeBaseFrameworkPath()        |                                                        |
| ASCMS_CORE_PATH               | Cx::getCodeBaseCorePath()             |                                                        |
| ASCMS_FEED_PATH               |                                       | Cx::getWebsiteFeedPath()                               |
| ASCMS_LIBRARY_PATH            | Cx::getCodeBaseLibraryPath()          |                                                        |
| ASCMS_MODULE_PATH             | Cx::getCodeBaseModulePath()           |                                                        |
| ASCMS_MODULE_WEB_PATH         | Cx::getCodeBaseModuleWebPath()        |                                                        |
| ASCMS_CORE_MODULE_PATH        | Cx::getCodeBaseCoreModulePath()       |                                                        |
| ASCMS_CORE_MODULE_WEB_PATH    | Cx::getCodeBaseCoreModuleWebPath()    |                                                        |
| ASCMS_THEMES_WEB_PATH         |                                       | Cx::getWebsiteThemesWebPath()                          |
| ASCMS_THEMES_PATH             |                                       | Cx::getWebsiteThemesPath()                             |
| ASCMS_TEMP_PATH               |                                       | Cx::getWebsiteTempPath()                               |
| ASCMS_TEMP_WEB_PATH           |                                       | Cx::getWebsiteTempWebPath()                            |
| ASCMS_MODEL_PATH              | Cx::getCodeBaseModelPath()            |                                                        |

### Global placeholders

The integration of global placeholders (and template blocks) has been standardized in version 5. Until version 4, it was common practice to inject global placeholders (and template blocks) directly into the global `$objTemplate` object, into the resolved `$page` object or into the global `$themesPages` array. This has been done in the SystemComponentController hooks [`preContentLoad()`](../Bootstrapping.md#preContentLoad), [`load()`](../Bootstrapping.md#load) and [`postContentLoad()`](../Bootstrapping.md#postContentLoad).

!!! warning

    As of version 5, this practice is no longer supported and considered as deprecated.

#### Integration as Widgets

Version 5 introduces so-called [Widgets](../Widgets.md). A Widget is an extension of a component that can be integrated anywhere on the website. Anywhere on the website means, that a Widget can be integrated in any **Template File** (HTML-files of a webdesign template), any **Content Page** and any **Content Pane**. Examples of such Widgets are:

-   JavaScript-Cart (of Shop)
-   Headlines (of News)
-   Language Navigation (of PageTree)

A Widget is either integrated using a placeholder (i.e.: `[[HEADLINES_FILE]]`) or a template block (i.e.: `<-- BEGIN/END shopJsCart -->`). Which way (placeholder or template block) depends on the Widget.

#### Migration

Existing global placeholders (and template blocks) that are being injected in the hooks [`preContentLoad()`](../Bootstrapping.md#preContentLoad), [`load()`](../Bootstrapping.md#load) and [`postContentLoad()`](../Bootstrapping.md#postContentLoad) must now be registered as Widgets in the [`postInit()`](../Bootstrapping.md#postInit) hook.

See article [Widgets](../Widgets.md) on how to implement your previously global placeholders now as Widgets.

### GUI templates storage location

Until version 4, the HTML templates of the backend sections were located in the component's folder `template`. Starting from version 5, those have been moved into the new component's location `View/Template/Backend`. The HTML code of the frontend sections was directly stored in the content manager as part of an application page. Since version 5, that code has been moved into application templates in the component's new location `View/Template/Frontend`. Whereas the `cmd` of an application page translates to its application template name. I.e. `details` translates to `View/Template/Frontend/Details.html`

See also [Template System](../Template System.md) and [Setup template folder](../Migrate Module to Component.md#3-setup-template-folder).

### Search Integration

The integration into the global fulltext search must now be implemented within each componet itself. See [Application Integration](../Full-Text Search.md#application-integration).

