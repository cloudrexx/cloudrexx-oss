## Migrating from Contrexx 3.1.x to Contrexx 3.2.x

The following sections list the changes made to the API of Contrexx from version 3.1 to 3.2

The full API documentation of version 3.2 can be found at <https://api.contrexx.com/>.

In version 3.2 the whole session handling has changed. The [session handler](../Session handling.md) locks the variables which are currently used by another user. This means, that as of Contrexx 3.2 it is possible to access Contrexx twice at the same time without any session variable getting lost. In Contrexx 3.1 the whole session has been written in one database field, there was no merging process.

In version 3.2 there is a useful database caching for doctrine entities. As soon as a query gets built through the entity manager, database caching is activated if the system is configured like this. There are new configurations in **General configurations \> Caching** section. You have to explicitly deactivate the caching if you don't want to load cached entities.

### Session initialization

The code you wrote somewhere in Contrexx needs to be checked. As soon as your code uses $\_SESSION, you should make the following call:

    \cmsSession::getInstance();

In backend this is not necessary, because the $\_SESSION has been loaded everytime the user is in backend.

### Deactivate result cache for Doctrine

After you got a query object like this:

    $query = $entityManager->createQuery("here comes dql");

you can deactivate the result cache with the following code:

    $query->useResultCache(false);