## Migrating from Cloudrexx 5.1.x to Cloudrexx 5.2.x

The following sections list the changes made to the API of Cloudrexx from version 5.1.x to 5.2.x

### Framework

#### Country

| Old code                                                                              | Replace by                                                              | Notes                                                            |
|---------------------------------------------------------------------------------------|-------------------------------------------------------------------------|------------------------------------------------------------------|
| `Country::getArray($count, $lang_id, $limit, $offset, $order)`                        | `Country::getData($lang_id)`                                            | All arguments by `$lang_id` dropped.                             |
| `Country::searchByName($term, $lang_id = null, $active = true)`                       | `Country::searchByName($term, $lang_id = 0)`                            | Argument `$active` dropped.                                      |
| `Country::getNameArray($active=true, $lang_id=null)`                                  | `Country::getNameArray($lang_id = 0)`                                   | Argument `$active` dropped.                                      |
| `Country::getMenu($menuName='countryId', $selected=`*`, $active=true, $onchange=`*`)` | `Country::getMenu($menuName='countryId', $selected=`*`, $onchange=`*`)` | Argument `$active` dropped.                                      |
| `Country::getMenuoptions($selected=0, $active=true)`                                  | `Country::getMenuoptions($selected=0)`                                  | Argument `$active` dropped.                                      |
| `Country::getNameById($country_id)`                                                   | `Country::getNameById($country_id, $localeId = 0)`                      | New argument `$localeId` to fetch the name of a specific locale. |
| `Country::getRecordcount()`                                                           | \-                                                                      | Dropped. No alternative                                          |
| `Country::isActiveById($country_id)`                                                  | \-                                                                      | Dropped. No alternative                                          |
| `Country::reset()`                                                                    | \-                                                                      | Dropped. No alternative                                          |
| `Country::getSqlSnippets($lang_id=0)`                                                 | \-                                                                      | Dropped. No alternative                                          |
| `Country::recordExists($country_id)`                                                  | \-                                                                      | Dropped. No alternative                                          |
| `Country::store(...)`                                                                 | \-                                                                      | Dropped. No alternative                                          |
| `Country::insert(...)`                                                                | \-                                                                      | Dropped. No alternative                                          |
| `Country::update(...)`                                                                | \-                                                                      | Dropped. No alternative                                          |
| `Country::deleteById($country_id)`                                                    | \-                                                                      | Dropped. No alternative                                          |
| `Country::getArraysByZoneId($zone_id=null)`                                           | \-                                                                      | Dropped. No alternative                                          |
| `Country::activate($strCountryIds)`                                                   | \-                                                                      | Dropped. No alternative                                          |
| `Country::settings()`                                                                 | \-                                                                      | Dropped. No alternative                                          |