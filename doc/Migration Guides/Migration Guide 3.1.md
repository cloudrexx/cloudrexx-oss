## Migrating from Contrexx 3.0.x to Contrexx 3.1.x

The following sections list the changes made to the API of Contrexx from version 3.0 to 3.1

The full API documentation of version 3.1 can be found at <https://api.contrexx.com/>.

In version 3.1 the initialization of Contrexx and its modules has changed. The [component framework](../Component User Framework.md#contrexx-31) allows containment of modules (now [components](../Component Definition.md)). This means, that as of Contrexx 3.1 it is possible to write a component (formerly known as module, core_module, etc.) without changing anything outside its own directory. Since we don't want to rewrite all existing modules we can load them as a legacy component. Contrexx is now initialized using the [Cx class](https://api.contrexx.com/latest/classes/Cx.Core.Core.Controller.Cx.html) which is configured to load components by default, legacy components need to be initialized differently:

### Legacy component initialization

The code you wrote located in initFrontend.php and initBackend.php needs to be moved (since these files are no longer used). For legacy components, these code parts are moved to [LegacyComponentHandler class](https://api.contrexx.com/latest/classes/Cx.Core.Core.Controller.LegacyComponentHandler.html). This class contains an array with [anonymous functions](https://php.net/manual/en/functions.anonymous.php). This array has the following structure:

    array[
        frontend|           // contains all frontend initializations
        backend             // contains all backend initializations
    ][
        preResolve|         // executed before resolving process
        postResolve|        // executed after resolving process
        preContentLoad|     // executed before content processing
        preContentParse|    // executed before processing application content (only for application pages)
        load|               // executed to parse application content (the former switch..case)
        postContentParse|   // executed after processing application content (only for application pages)
        postContentLoad|    // executed after content processing
        preFinalize|        // executed before parsing global template
        postFinalize        // executed at the end of the request
    ][
        {legacy component name} // module name
    ] = {callable}

As you can see, the code is split over 9 hooks. Code that was located in one of the huge switch..case statements in initFrontend.php or initBackend.php goes to the *load*-hook. Code that was before or after one of the two switch..case statements goes to one of the other hooks, depending on the time it should be executed.

!!! example

    Here's a sample to load the 'demo' module in frontend:

    ``` php
    array(
        'frontend' => array(
            ...
            'load' => array(
                ...
                'demo' => function() {
                    global $cl, $_CORELANG;
                    
                    if (!$cl->loadFile(ASCMS_MODULE_PATH . '/' . $plainSection . '/index.class.php')) {
                        die($_CORELANG['TXT_THIS_MODULE_DOESNT_EXISTS']);
                    }
                    $module = new \Cx\Modules\Demo\FrontendController($pageContent, ...);
                    \Env::get('cx')->getPage()->setContent($module->getPage());
                },
                ...
            ),
            ...
        ),
        ...
    );
    ```

