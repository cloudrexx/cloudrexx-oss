Sämtliche Operationen rund um den Benutzer- und Gruppenstamm erfolgt über die Klasse *\FWUser*. Aufgrund der komplexen Architektur ist die Initialisierung ziemlich Ressourcenaufwendig. Daher verfügt die Klasse *\FWUser* über die statische Methode *getFWUserObject()*, mit welcher eine Instanz der Klasse *\FWUser* zurück gegeben werden kann. Die Methode *\FWUser::getFWUserObject()* erstellt dabei beim ersten Aufruf eine neue Instanz der Klasse *\FWUser* und gibt diese zurück. Bei jedem weiteren Aufruf der Methode *\FWUser::getFWUserObject()* wird die bestehende Instanz von *\FWUser* geklont und der interne Cache der geklonten Instanz mit der originalen Instanz verlinkt. Der Cache beinhaltet alle Informationen von Benutzer und Gruppen die während des gesamten Seitenaufrufes jemals geladen wurden. Somit kann die Anzahl Datenbankabfragen auf ein Minimum gehalten werden.

Verwendet wird die Klasse *\FWUser* daher nie durch manuelles Instantiieren, sondern immer durch Verwendung der Methode *\FWUser::getFWUserObject()*:

``` php
$objFWUser = \FWUser::getFWUserObject();
```

Die Klasse *\FWUser* verfügt über die zwei Objekte *\FWUser-&gt;objUser* und *\FWUser-&gt;objGroup* welche als zentrale Elemente zur Verwaltung der Benutzer, rsp. der Gruppen dienen.

## Benutzer

Das Objekt *\FWUser-&gt;objUser* dient zwei Funktionen. Einerseits repräsentiert es den aktuell authentifizierten Benutzer (sofern dies jemand ist) und andererseits dient es als Schnittstelle zum gesamten Benutzerstamm. Letzteres bezieht sich auf die Methoden *\FWUser-&gt;objUser-&gt;getUser()* und *\FWUser-&gt;objUser-&gt;getUsers()*.

### Authentifizierter Besucher

Mittels der Methode *\FWUser-&gt;objUser-&gt;login()* kann im Frontend (im Backend ist dies nicht notwendig, da dies ja sowie so der Fall ist) überprüft werden, ob der aktuelle Besucher authentifiziert ist:

``` php
if (\FWUser::getFWUserObject()->objUser->login()) {
    // Der aktuelle Benutzer ist authentifiziert
    // Auf die Daten des aktuell authentifizierten Benutzers kann über das
    // Objekt \FWUser::getFWUserObject()->objUser zugegriffen werden.
    print "Sie sind angemeldet als ".\FWUser::getFWUserObject()->objUser->getUsername();
} else {
    // Der aktuelle Benutzer ist nicht authentifiziert
    print "Sie sind nicht angemeldet!";
}
```

### Benutzer Informationen laden

Die Informationen eines einzelnen Benutzers kann mittels dessen ID über die Methode *\FWUser-&gt;objUser-&gt;getUser()* geladen werden:

``` php
// Benutzer mit der ID 3 laden
if ($objUser = \FWUser::getFWUserObject()->objUser->getUser($id = 3)) {
    print "Der Benutzer ".$objUser->getUsername()." wurde erfolgreich geladen!";
} else {
    print "Ein Benutzer mit der ID 3 konnte nicht gefunden oder geladen werden!";
}
```

Mittels der Methode *\FWUser-&gt;objUser-&gt;getUsers()* können mehrere Benutzer geladen werden.

``` php
// Alle Benutzer anfordern
if ($objUser = \FWUser::getFWUserObject()->objUser->getUsers()) {
    // Das Objekt $objUser repräsentiert nun den ersten der angeforderten Benutzer
    while (!$objUser->EOF) {
        print $objUser->getUsername();

        // Nächsten Benutzer im Objekt $objUser laden
        $objUser->next();
    }
} else {
    print "Beim Laden der Benutzer trat ein Fehler auf!";
}
```

Die Methode *\FWUser-&gt;objUser-&gt;getUsers()* ist sehr mächtig was die selektive Abfrage betrifft. Einige Beispiele:

``` php
// Alle aktiven Benutzer von welchen der Benutzername mit den Buchstaben 'w' anfängt
\FWUser::getFWUserObject()->objUser->getUsers($filter = array('username' => '%w', 'active' => true));
```

``` php
// Alle Benutzer, welche Mitglied der Benutzergruppe mit der ID 3 sind
\FWUser::getFWUserObject()->objUser->getUsers($filter = array('group_id' => '3'));
```

``` php
// Alle Benutzer welchen in Ihrem Vornamen das Wort 'anna' oder 'john' enthalten
\FWUser::getFWUserObject()->objUser->getUsers(null, $search = array('firstname' => array('anna', 'john'));
```

### Benutzer Informationen abfragen

Die Benutzer Informationen sind in drei Bereiche unterteilt, die Konto, Profil und Sonstige Informationen.

#### Konto Informationen

| Eigenschaft              | Methode                            | Kommentar                                                                                    |
|--------------------------|------------------------------------|----------------------------------------------------------------------------------------------|
| Konto ID                 | User-&gt;getId()                     | Die ID des Benutzerkontos                                                                    |
| Benutzername             | User-&gt;getUsername()               | Der Benutzername (Login Name) des Benutzers                                                  |
| E-Mail Adresse           | User-&gt;getEmail()                  | E-Mail Adresse des Benutzers                                                                 |
| Status                   | User-&gt;getActiveStatus()           | Definiert ob das Benutzerkonto (in-)aktiv ist                                                |
| Benutzerrolle            | User-&gt;getAdminStatus()            | Definiert ob es sich um einen Administrator handelt                                          |
| Frontend Sprache         | User-&gt;getFrontendLanguage()       | Bevorzugte Sprache (als ID) im Frontend                                                      |
| Backend Sprache          | User-&gt;getBackendLanguage()        | Bevorzugte Sprache (als ID) im Backend                                                       |
| Letzte Aktivität         | User-&gt;getLastActivityTime()       | Die Zeit (als Timestamp) an dem der Benutzer das letzte Mal einen Seitenaufruf getätigt hat. |
| Letzte Authentifizierung | User-&gt;getLastAuthenticationTime() | Die Zeit (als Timestamp) an dem sich der Benutzer das letzte Mal authentifiziert hat.        |
| Registrierungs Datum     | User-&gt;getRegistrationDate()       | Das Datum (als Timestamp) an dem sich der Benutzer registriert hat.                          |

#### Profil Informationen

Die Profile Eigenschaften werden mittels der Methode *User-&gt;getProfileAttribute()* abgerufen. Als erster Parameter wird dabei die abzufragende Eigenschaft als formale Zeichenkette übergeben. Die folgende Tabelle listet die verfügbaren Eigenschaften auf:

| Eigenschaft   | Beschreibung |
|---------------|--------------|
| picture       | Avatar       |
| gender        | Geschlecht   |
| title         | Anrede       |
| firstname     | Vorname      |
| lastname      | Nachname     |
| company       | Firma        |
| address       | Adresse      |
| city          | Ort          |
| zip           | PLZ          |
| country       | Land         |
| phone_office  | Tel. Büro    |
| phone_private | Tel. Privat  |
| phone_mobile  | Tel. Mobile  |
| phone_fax     | Fax          |
| birthday      | Geburtstag   |
| website       | Website      |
| profession    | Beruf        |
| interests     | Interessen   |
| signature     | Signatur     |

Beispiele:

``` php
// Benutzer 4 laden
if ($objUser = \FWUser::getFWUserObject()->objUser->getUser($id = 4)) {
    // Vorname und Nachname des Benutzers mit der ID 4 ausgeben
    print $objUser->getProfileAttribute('firstname');
    print $objUser->getProfileAttribute('lastname');
} else {
    print "Ein Benutzer mit der ID 4 konnte nicht gefunden oder geladen werden!";
}
```

``` php
// Infos des authentifizierten Benutzers ausgeben
if (\FWUser::getFWUserObject()->objUser->login()) {
    // Vorname und Nachname des Benutzers ausgeben
    print \FWUser::getFWUserObject()->objUser->getProfileAttribute('firstname');
    print \FWUser::getFWUserObject()->objUser->getProfileAttribute('lastname');
} else {
    print "Sie sind nicht authentifiziert!";
}
```

``` php
// Anrede des Benutzers mit ID 4
if ($objUser = \FWUser::getFWUserObject()->objUser->getUser($id = 4)) {
    // Anrede: ID und Text
    print $objUser->getProfileAttribute('title'); // Numerische ID! 
    print $objUser->objAttribute->getById('title_'.$objUser->getProfileAttribute('title'))->getName(); // Text ("Frau", "Herr", ...)
} else {
    print "Ein Benutzer mit der ID 4 konnte nicht gefunden oder geladen werden!";
}
```

##### Benutzerdefinierte Profil Eigenschaften

Eigen definierte Profil Eigenschaften werden gleich wie die normalen Profil Eigenschaften mittels der Methode *User-&gt;getProfileAttribute()* abgefragt. Dabei wird als erster Parameter die numerische ID der abzufragenden Eigenschaft übergeben.

Beispiele:

``` php
// Benutzer 4 laden
if ($objUser = \FWUser::getFWUserObject()->objUser->getUser($id = 4)) {
    // Benutzerspezifische Eigenschaften des Benutzers mit der ID 4 ausgeben
    // Die Parameter Werte 2 und 5 in diesem Beispiel referenzieren zu zwei selbst definierten Eigenschaften welche die ID's 2 und 5 aufweisen
    print $objUser->getProfileAttribute(2);
    print $objUser->getProfileAttribute(5);
} else {
    print "Ein Benutzer mit der ID 4 konnte nicht gefunden oder geladen werden!";
}
```

``` php
// Infos des authentifizierten Benutzers ausgeben
if (\FWUser::getFWUserObject()->objUser->login()) {
    // Benutzerspezifische Eigenschaften des Benutzers ausgeben
    // Die Parameter Werte 2 und 5 in diesem Beispiel referenzieren zu zwei selbst definierten Eigenschaften welche die ID's 2 und 5 aufweisen
    print \FWUser::getFWUserObject()->objUser->getProfileAttribute(2);
    print FWUser::getFWUserObject()->objUser->getProfileAttribute(5);
} else {
    print "Sie sind nicht authentifiziert!";
}
```

#### Sonstige Informationen

| Beschreibung                                                                                                                                                                                                                                                                     | Methode                                                                                                                                                                                                  |
|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Liefert ein Array gefüllt mit dynamischen Zugriffs-ID's des Benutzers                                                                                                                                                                                                            | User-&gt;getDynamicPermissionIds()                                                                                                                                                                         |
| Bestimmt den Zugriff auf die E-Mail Adresse (dessen Sichtbarkeit).<br>Dabei bestehend die folgenden Arten:<br>**everyone** Jeder darf die E-Mail Adresse einsehen<br>**members_only** Nur Mitglieder dürfen die E-Mail Adresse einsehen<br>**nobody** Niemand darf die E-Mail Adresse einsehen (ausgenommen Administratoren) | User-&gt;getEmailAccess()
| Liefert ein Datum (Timestamp) bis wann das Konto gültig ist.                                                                                                                                                                                                                     | User-&gt;getExpirationDate()                                                                                                                                                                               |
| Bestimmt den Zugriff auf das Profil (dessen Sichtbarkeit).<br>Dabei bestehend die folgenden Arten:<br>**everyone** Jeder darf das Profil einsehen<br>**members_only** Nur Mitglieder dürfen das Profil einsehen<br>**nobody** Niemand darf das Profil einsehen (ausgenommen Administratoren) | User-&gt;getProfileAccess()                                                                                                                                                                                |
|                                                                                                                                                                                                                                                                                  | User-&gt;getRestoreKey()                                                                                                                                                                                   |
|                                                                                                                                                                                                                                                                                  | User-&gt;getRestoreKeyTime()                                                                                                                                                                               |
| Liefert ein Array gefüllt mit statischen Zugriffs-ID's des Benutzers                                                                                                                                                                                                             | User-&gt;getStaticPermissionIds()                                                                                                                                                                          |
| Mittels dieser Methode kann überprüft werden, ob der betroffene Benutzer Zugriff auf das Front-/ Backend hat.                                                                                                                                                                    | User-&gt;hasModeAccess($backend = false)                                                                                                                                                                   |
| Definiert ob der Benutzer berechtigt ist den Zugriff auf seine E-Mail Adresse (dessen Sichtbarkeit) zu ändern.                                                                                                                                                                   | User-&gt;isAllowedToChangeEmailAccess()                                                                                                                                                                    |
| Definiert ob der Benutzer berechtigt ist den Zugriff auf sein Profile (dessen Sichtbarkeit) zu ändern.                                                                                                                                                                           | User-&gt;isAllowedToChangeProfileAccess()                                                                                                                                                                  |
| Definiert ob der Benutzer berechtigt ist sein Konto zu löschen.                                                                                                                                                                                                                  | User-&gt;isAllowedToDeleteAccount()                                                                                                                                                                        |

### Benutzer Informationen setzen

#### Konto Informationen

| Eigenschaft      | Methode                      | Beschreibung                                        |
|------------------|------------------------------|-----------------------------------------------------|
| Benutzername     | User-&gt;setUsername()         |                                                     |
| E-Mail Adresse   | User-&gt;setEmail()            |                                                     |
| Kennwort         | User-&gt;setPassword()         |                                                     |
| Frontend Sprache | User-&gt;setFrontendLanguage() | Bevorzugte Frontend Sprache (als ID)                |
| Backend Sprache  | User-&gt;setBackendLanguage()  | Bevorzugte Backend Sprache (als ID)                 |
| Status           | User-&gt;setActiveStatus()     | Definiert ob das Konto (in-)aktiv ist               |
| Benutzerrolle    | User-&gt;setAdminStatus()      | Definiert ob es sich um einen Administrator handelt |

#### Profil Informationen

Die Profil Informationen werden mittels der Methode *User-&gt;setProfile()* gesetzt. Dabei muss als Parameter ein zwei Dimensionales Array übergeben werden, welches als Schlüssel die Profileigenschaft und als Wert dessen Eigenschaftswert beinhaltet.

Beispiel:

``` php
// Benutzer 5 laden
if ($objUser = \FWUser::getFWUserObject()->objUser->getUser($id = 5)) {
    // Vorname und Nachname setzen
    $arrProfile = array(
        'firstname' => array('Sarah'),
        'lastname'  => array('Conner')
    );
    $objUser->setProfile($arrProfile);

    // Benutzerkonto speichern
    if ($objUser->store()) {
        print "Das Benutzerkonto wurde erfolgreich gespeichert.";
    } else {
        print implode("<br />", $objUser->getErrorMsg());
    }
} else {
    print "Ein Benutzer mit der ID 5 konnte nicht gefunden oder geladen werden!";
}
```

##### Benutzerdefinierte Profil Eigenschaften

Benutzerdefinierte Profil Eigenschaften werden gleich wie die normalen Profil Eigenschaften mittels der Methode *User-&gt;setProfile()* gesetzt. Dabei muss als Parameter ein zwei Dimensionales Array übergeben werden, welches als Schlüssel die numerische ID der Benutzerdefinierten Profileigenschaft und als Wert dessen Eigenschaftswert beinhaltet.

Beispiel:

``` php
// Benutzer 5 laden
if ($objUser = \FWUser::getFWUserObject()->objUser->getUser($id = 5)) {
    // Benutzerdefinierte Profil Eigenschaft 2 und 6 setzen
    $arrProfile = array(
        2 => array('Blue'),
        6 => array('Water')
    );
    $objUser->setProfile($arrProfile);

    // Benutzerkonto speichern
    if ($objUser->store()) {
        print "Das Benutzerkonto wurde erfolgreich gespeichert.";
    } else {
        print implode("<br />", $objUser->getErrorMsg());
    }
} else {
    print "Ein Benutzer mit der ID 5 konnte nicht gefunden oder geladen werden!";
}
```

### Benutzer Informationen anzeigen

See API reference:

-   [AccessLib](https://api.contrexx.com/latest/classes/AccessLib.html#method___construct)
-   [AccessLib::setModulePrefix()](https://api.contrexx.com/latest/classes/AccessLib.html#method_setModulePrefix)
-   [AccessLib::setAttributeNamePrefix()](https://api.contrexx.com/latest/classes/AccessLib.html#method_setAttributeNamePrefix)
-   [AccessLib::parseAttribute()](https://api.contrexx.com/latest/classes/AccessLib.html#method_parseAttribute)

``` php
// define html template of output
$template = <<<HTML
<!-- BEGIN shop_customer_profile_attribute_firstname -->
{SHOP_CUSTOMER_PROFILE_ATTRIBUTE_FIRSTNAME}
<!-- END shop_customer_profile_attribute_firstname -->
HTML;

// initiate template system
$objTemplate = new \Cx\Core\Html\Sigma();
$objTemplate->setErrorHandling(PEAR_ERROR_DIE);
$objTemplate->setTemplate($template);

// we will parse the attribute value of the currently logged in user
$objUser = \FWUser::getFWUserObject()->objUser;

// generate html output of profile attribute
$objAccessLib = new AccessLib($objTemplate);
$objAccessLib->setModulePrefix('SHOP_CUSTOMER_');
$objAccessLib->setAttributeNamePrefix('shop_customer_profile_attribute');
$objAccessLib->parseAttribute($objUser, 'firstname', 0, false, false, false, false, false);
```

### Autonome Benutzeranmeldung

| Verfügbar seit |
|----------------|
| 3.0.2          |

Mit der Methode \FWUser::loginUser() kann ein Benutzer autonom (ohne Login-Formular) am System angemeldet werden.

``` php
$objUser = \FWUser::getFWUserObject()->objUser->getUser($userId);
\FWUser::loginUser($objUser)
```

## Gruppen

Das Objekt \FWUser-&gt;objGroup dient als Schnittstelle zum gesamten Benutzerstamm. Dabei stehen die zwei Methoden \FWUser-&gt;objGroup-&gt;getGroup() und \FWUser-&gt;objGroup-&gt;getGroups() im Zentrum.

### Spezifische Gruppe auswählen

Beispiel:

``` php
// Gruppe mit der ID 3 laden
if ($objGroup = \FWUser::getFWUserObject()->objGroup->getGroup($id = 3)) {
    print $objGroup->getName();
    print $objGroup->getDescription();
} else {
    print "Eine Gruppe mit der ID 3 konnte nicht gefunden oder geladen werden!";
}
```

### Gruppen Suchen/Filtern

Beispiel:

``` php
/**
 * Alle aktiven Frontend Gruppen laden und diese aufsteigend primär nach
 * dem Namen und sekundär nach der Beschreibung sortieren.
 * Von der sortieren Listen sollen nur die Gruppen 15-25 geladen werden.
 * Zudem soll nur die ID jeder Gruppe geladen werden.
 */
if ($objGroup = \FWUser::getFWUserObject()->objGroup->getGroups(
        $filter = array('is_active' => true, 'type' => 'frontend'),
        $arrSort = array('group_name' => 'asc', 'group_description' => 'asc'),
        $arrAttributes('group_id'),
        $limit = 10,
        $offset = 15
)) {
    while (!$objGroup->EOF) {
        print $objGroup->getId();
        $objGroup->next();
    }
} else {
    print "Keine Gruppen gefunden!";
}
```

