With the new Template Editor Component you can add options to your template which the end user can edit without any knowledge of any kind of programming/structural language (HTML, CSS, Javascript). As a template developer you have now the possibility to add customizable texts, colors, images and image series in your template.

You can also add presets (Color presets like red, blue, green or theme based presets for winter, summer, spring, fall) with predefined options, which the user can customize.

The following guide will show you how you can do this.

## Locate the options.yml and Groups.yml file

To get started create or open the file `options/options.yml` in your template directory.

For example if you use the `standard_4_0` template, the `options.yml` should be located under `/themes/standard_4_0/options/options.yml`

In this file you can specify various options for your template.

If you need to create this file you can use the following template:

``` yaml
activePreset: Default
options: ~
```

The file <code>Groups.yml</code> should be located in the same directory as the <code>options.yml</code>.<br />
In the <code>Groups.yml</code> you can specify different groups for the backend view, so the template is well-arranged in the GUI.<br />
More about <code>Groups.yml</code>: [Grouping of options](#grouping-of-options)

## Add an option to your template

To add an option you have to add a new entry to the <code>options</code>[^1] array in <code>options.yml</code>. Below is a example with a first text option added.

[^1]: When starting with a new template you'd have to add the entry <code>options</code> (on the first level of the Yaml structure) to your <code>options.yml</code> file first.

``` yaml
options:
  -
    name: website_name
    specific:
      textvalue: Contrexx
      regex: '/^[a-z]+$/i'
      regexError:
        1: 'Darf nur Buchstaben enthalten.'
        2: 'Only letters are allowed'
    type: Cx\Core_Modules\TemplateEditor\Model\Entity\TextOption
    translation:
      1: 'Website Name'
      2: 'Website Name'
```

<references />

The name is a identifier to generate the placeholder, you can then use the placeholder in your template to get the values which the user entered.

If there are two options with the same name, the last one in the array will be displayed.

All the values under 'specific' are specific to this type of option, in this case the 'TextOption'. The type defines the type of the Option.

### Translations

In the translation you can add translations for the user to explain for what this option stands in their native tongue. If there isn't a translation available the name will be shown to the user instead of a translation.

The numbers stand for the id of the language in contrexx. 1 -&gt; German, 2 -&gt; English, 3 -&gt; French, 4 -&gt; Italian, 5 -&gt; Danish, 6 -&gt; Russian

Here are examples for all the available options:

### Single options

#### TextOption

``` yaml
-
  name: website_name
  specific:
    textvalue: Contrexx
    regex: '/^[a-z]+$/i'
    regexError:
      1: 'Darf nur Buchstaben enthalten.'
      2: 'Only letters are allowed'
    html: true
  type: Cx\Core_Modules\TemplateEditor\Model\Entity\TextOption
  translation:
    1: 'Website Name'
    2: 'Website Name'
```

##### textvalue

The textvalue holds the momentary value which was entered by the user.

##### regex

With the regex you can force the user to comply to a certain format by using a regular expression.

##### regexError

In the regexError array you can define an error message per language which is displayed if the user input doesn't match the regex. If you don't define on this default message will be shown to the user: 'String %s doesn't match given regex: /^[a-z]+$/i'

##### html

With the html flag you can define whether the value which was entered shouldn't be escaped. We recommend using this flag only with a regular expression set, as otherwise the user could inject a XSS attack. <https://de.wikipedia.org/wiki/Cross-Site-Scripting>

#### TextareaOption

!!! note
    This option is not yet available.

``` yaml
-
  name: website_leading_text
  specific:
    textvalue: 'Unsere Webseite hilft Ihnen\n ihr Business im Web\n zu verwirklichen'
    regex: '/\|(?![^{]*})/g'
    regexError:
      1: 'Darf keine Klammern enthalten.'
      2: 'Brackets are not allowed'
    html: true
  type: Cx\Core_Modules\TemplateEditor\Model\Entity\TextOption
  translation:
    1: 'Website Name'
    2: 'Website Name'
```

##### textvalue

The textvalue holds the momentary value which was entered by the user.

##### regex

With the regex you can force the user to comply to a certain format by using a regular expression.

##### regexError

In the regexError array you can define an error message per language which is displayed if the user input doesn't match the regex. If you don't define on this default message will be shown to the user: 'String %s doesn't match given regex: /^[a-z]+$/i'

##### html

With the html flag you can define whether the value which was entered shouldn't be escaped. We recommend using this flag only with a regular expression set, as otherwise the user could inject a XSS attack. <https://de.wikipedia.org/wiki/Cross-Site-Scripting>

#### ColorOption

``` yaml
-
  name: main_color
  specific:
    color: '#c0392b'
    choice: ['#1ABC9C', '#16A085', '#2ECC71', '#27AE60']
  type: Cx\Core_Modules\TemplateEditor\Model\Entity\ColorOption
  translation:
    1: Hauptfarbe
    2: 'Main Color'
```

##### color

The color is the normal value which will be used in the template.

##### choice

With the attribute choice you can show the user some predefined color choices to help your user customize his template.

#### AreaOption

``` yaml
-
  name: news_area
  specific:
    active: true
  type: Cx\Core_Modules\TemplateEditor\Model\Entity\AreaOption
  translation:
    1: 'News anzeigen'
    2: 'Show news area'
```

##### active

The active value specifies whether the defined area should be shown or not.

#### ImageOption

``` yaml
-
  name: logo_image
  specific:
    url: /images/content/Community.jpg
  type: Cx\Core_Modules\TemplateEditor\Model\Entity\ImageOption
  translation:
    1: Katzenfoto
    2: 'Essential cat-picture'
```

##### url

The url specifies the url to the picture. This can also be a external ressource. (eg. <https://placekitten.com/1500/300>)

#### ImageSeriesOption

``` yaml
-
  name: slider_images
  specific:
    urls: { 1: /images/content/slideshow/slide_1.jpg, 2: /images/content/slideshow/slide_2.jpg }
  type: Cx\Core_Modules\TemplateEditor\Model\Entity\ImageSeriesOption
  translation:
    1: 'Haupt Slider'
    2: 'Main Slider'
```

##### urls

The urls array holds all the added images for this option.

#### SelectOption

``` yaml
-
  name: slider_position
  specific:
    choice:
      center:
        1: zentriert
        2: center
      left:
        1: links
        2: left
    activeChoice: left
  type: Cx\Core_Modules\TemplateEditor\Model\Entity\SelectOption
  translation:
    1: 'Ausrichtung Slider'
    2: 'Slider position'
```

##### choice

The array with the different choices, where the second level of the array is the name in the different languages.

##### activeChoice

The choice which will be preselected

### Series of element

!!! note
    This option is not yet available.
    The following types are supported: TextOption, TextareaOption, SeriesOption and ColorOption.

To create a series of elements, you can use the parameter series (boolean). If it is set to true, specific must be an array with elements. For example, if you have a single TextOption:

``` yaml
-
  name: text_single
  specific:
    textvalue: 'Single Text'
  type: Cx\Core_Modules\TemplateEditor\Model\Entity\TextOption
```

A series can be created by adding the parameter series: true and by creating an elements array in specific:

``` yaml
-
  name: text_series
  specific:
    elements:
      1:
        textvalue: 'Text 1'
      2:
        textvalue: 'Text 2'
      3:
        textvalue: 'Text 3'
  type: Cx\Core_Modules\TemplateEditor\Model\Entity\TextOption
  series: true
```

### CombinedOption

!!! note
    This option is not yet available.

#### Define a CombinedOption

A CombinedOption is an option, which contains multiple single options. It can contain as many single options as needed and is available for all single options.
The options will be shown after the title of the CombinedOption in the backend. The single options do not have an own title.

``` yaml
-
name: image_with_text
specific:
  options:
    1:
      type: Cx\Core_Modules\TemplateEditor\Model\Entity\TextOption
    2:
      type: Cx\Core_Modules\TemplateEditor\Model\Entity\ImageOption
  elements:
    1:
      textvalue: 'slide 2'
    2:
      url: images/content/slideshow/slide_1.jpg
type: Cx\Core_Modules\TemplateEditor\Model\Entity\CombinedOption
group: group_content
translation:
  1: 'Startbild'
  2: 'startimage'
```

##### options

The options array is used to define which single options you want to combine to a CombinedOption. You can also add a single option twice or more (for example: if you have a image with two texts you can use the TextOption twice)

##### elements

The elements must have the same array index as the options. Inside this index you can specify the specific fields for the single option according to their definition (See Single options).

#### Define a CombinedOption as series

``` yaml
  -
    name: slider
    specific:
      options:
        1:
          type: Cx\Core_Modules\TemplateEditor\Model\Entity\TextOption
        2:
          type: Cx\Core_Modules\TemplateEditor\Model\Entity\ImageOption
      elements:
        1:
          1:
            textvalue: 'slide 2'
          2:
            url: images/content/slideshow/slide_1.jpg
        2:
          1:
            textvalue: 'slide 2'
          2:
            url: images/content/slideshow/slide_2.jpg
    type: Cx\Core_Modules\TemplateEditor\Model\Entity\CombinedOption
    group: group_content
    series: true
    translation:
      1: 'Slider'
      2: 'slider'
```

To define a CombinedOption as Series you have to set the parameter "series" to true.

##### elements

An array with an entry foreach series element.

## Add placeholders to your template

Now that you've learned how to define the options in the component.yml file we can start adding them to our template.

Here are the examples how you can use the various options in your template.

### TextOption

If you've defined your option like this:

``` yaml
options:
-
  name: website_name
  specific:
    textvalue: Cloudrexx
  type: Cx\Core_Modules\TemplateEditor\Model\Entity\TextOption
  translation:
    1: 'Website Name'
    2: 'Website Name'
```

You can use the placeholder {TEMPLATE_EDITOR_WEBSITE_NAME} in your template to show the value entered by the user. The placeholder is built by using the name in uppercase prepended by the name 'template_editor_' in uppercase.

### TextareaOption

!!! note
    This option is not yet available.

If you've defined your option like this:

``` yaml
options:
-
  name: website_slogan
  specific:
    textvalue: 'Cloudrexx is \n you business in web!'
  type: Cx\Core_Modules\TemplateEditor\Model\Entity\TextareaOption
  translation:
    1: 'Website Slogan'
    2: 'Website slogan'
```

You can use the placeholder {TEMPLATE_EDITOR_WEBSITE_SLOGAN} in your template to show the value entered by the user. The placeholder is built by using the name in uppercase prepended by the name 'template_editor_' in uppercase.

### ColorOption

``` yaml
-
  name: main_color
  specific:
    color: '#c0392b'
    choice: ['#1ABC9C']
  type: Cx\Core_Modules\TemplateEditor\Model\Entity\ColorOption
  translation:
    1: Hauptfarbe
    2: 'Main Color'
```

You can use the placeholder `{TEMPLATE_EDITOR_MAIN_COLOR}` in your template to show the value entered by the user. The placeholder is built by using the name in uppercase prepended by the name 'template_editor_' in uppercase.
Additionally, the following placeholders are available to output variations in brightness of the set color:
- `{TEMPLATE_EDITOR_MAIN_COLOR_SUBTRACT_<offset>}`: Replace `<offset>` by `1`-`255` to generate the color code with less brightness. Where `255` is completely dark (=black).
- `{TEMPLATE_EDITOR_MAIN_COLOR_ADD_<offset>}`: Replace `<offset>` by `1`-`255` to generate the color code with more brightness. Where `255` is completely bright (=white).

### AreaOption

``` yaml
-
  name: news_area
  specific:
    active: true
  type: Cx\Core_Modules\TemplateEditor\Model\Entity\AreaOption
  translation:
    1: 'News anzeigen'
    2: 'Show news area'
```

With this option you have to use the block feature of our template engine. You can define the start of you're block with this statment:

    <!-- BEGIN template_editor_news_area -->

And end it with this:

    <!-- END template_editor_news_area -->

If the user now disables this option, the defined area won't be displayed in the frontend. The blockname is created with the combination of 'template_editor_' and the name of the option in lowercase.

### ImageOption

``` yaml
-
  name: logo_image
  specific:
    url: /images/content/Community.jpg
  type: Cx\Core_Modules\TemplateEditor\Model\Entity\ImageOption
  translation:
    1: Katzenfoto
    2: 'Essential cat-picture'
```

You can use the placeholder {TEMPLATE_EDITOR_LOGO_IMAGE} in your template to show the url for the image entered by the user. The placeholder is built by using the name in uppercase prepended by the name 'template_editor_' in uppercase.

### ImageSeriesOption

``` yaml
-
  name: slider_images
  specific:
    urls: { 1: /images/content/slideshow/slide_1.jpg, 2: /images/content/slideshow/slide_2.jpg }
  type: Cx\Core_Modules\TemplateEditor\Model\Entity\ImageSeriesOption
  translation:
    1: 'Haupt Slider'
    2: 'Main Slider'
```

To render the ImageSeriesOptions you have to add a block to your template.

``` html
<!-- BEGIN template_editor_slider_images -->
    <img alt="Slider" height="500" src="{TEMPLATE_EDITOR_SLIDER_IMAGES}" width="1200" />
<!-- END template_editor_slider_images -->
```

The blockname is created with the combination of 'template_editor_' and the name of the option in lowercase. In the area of the block you can now use the {TEMPLATE_EDITOR_SLIDER_IMAGES} placeholder to get the url of the image. This placeholder is built by using the name in uppercase prepended by the name 'template_editor_' in uppercase. The rendering system will repeat the block for every image.

If the user defined 3 images the output would look like this:

``` html
<img alt="Slider" height="500" src="/images/content/penguins.jpg" width="1200" />
<img alt="Slider" height="500" src="/images/content/lions.jpg" width="1200" />
<img alt="Slider" height="500" src="/images/content/kittens.jpg" width="1200" />
```

### SelectOption

If you've defined your option like this:

``` yaml
-
  name: slider_position
  specific:
    choice:
      center:
        1: zentriert
        2: center
      left:
        1: links
        2: left
    activeChoice: left
  type: Cx\Core_Modules\TemplateEditor\Model\Entity\SelectOption
  translation:
    1: 'Ausrichtung Slider'
    2: 'Slider position'
```

You can use the placeholder {TEMPLATE_EDITOR_SLIDER_POSITION} in your template to show the value entered by the user.
The placeholder is built by using the name in uppercase prepended by the name 'template_editor_' in uppercase.

### CombinedOption

!!! note
    This option is not yet available.</b>

To add the CombinedOption to the template, you can use this placeholder-syntax:
{TEMPLATE_EDITOR_[NAME_OF_THE_COMBINED_OPTION]_[INDEX_OF_THE_SINGLE_OPTION]}
where [NAME_OF_THE_COMBINED_OPTION] is the defined name of the CombinedOption and [INDEX_OF_THE_SINGLE_OPTION] is the index of the option defined in the attribute specific -&gt; options of the CombinedOption.
For example: If you define your CombinedOption like this CombinedOption your template should contain the following snippet.

``` html
<img alt="Image with text" src="{TEMPLATE_EDITOR_IMAGE_WITH_TEXT_2}" />
<h1>{TEMPLATE_EDITOR_IMAGE_WITH_TEXT_1}</h1>
```

The same structure is also used if you have defined your CombinedOption as series.
Here you have to add the block (BEGIN and END comment) according to the following schema:

``` html
<!-- BEGIN|END template_editor_[NAME_OF_THE_COMBINED_OPTION] -->
```

For example: If you define your CombinedOption like this Define a CombinedOption as series your template should contain the following snippet.

``` html
<!-- BEGIN template_editor_slider -->
    <div class="item">
        <img alt="Slider" src="{TEMPLATE_EDITOR_SLIDER_2}" />
        <h1>{TEMPLATE_EDITOR_SLIDER_1}</h1>
    </div>
<!-- END template_editor_slider -->
```

## Grouping of options

!!! note
    Groups are not yet available.

### Specify a group

To create a group you have to edit the Groups.yml. The Groups.yml should look like this:

``` yaml
groups:
  -
    name: groupname_1
    color: '#D9D9D9'
    translation:
      1: 'Gruppe 1'
      2: 'group 2'
  -
    name: groupname_2
    color: '#ff'
    translation:
      1: 'Gruppe 2'
      2: 'group 2'
```

The name is the identifier of the group and must be unique.
The color is used as background-color in the backend view. If not set, the background will be white.
The translations for the groups works the same as the translations for options. (see Translations)

The groups will be listed in the order they are defined in the Groups.yml.

### Add an option to a group

To add an option to a group, there is the attribute group:

``` yaml
-
  name: logo_image
  specific:
    url: /images/content/Community.jpg
  type: Cx\Core_Modules\TemplateEditor\Model\Entity\ImageOption
  group: groupname_1
```

The group should contain the name of the group from the Groups.yml.
The options will be listed in the order they are defined in the options.yml inside the group.

All options, which are not linked to a group, will be added to the standard group, which is the last listed group in the backend view.

## Presets

The presets for the templateeditor are in the folder options/presets in your template. For example if you use the 'standard_4_0' template, the presets will be located under /themes/standard_4_0/options/presets. If you migrate a new template to work with the templateeditor you have to create a new Default.yml as a default preset.

!!! example

    In the presets you have to list all properties which you've defined in the options.yml and you can overwrite the individual options with your own values.

    ``` yaml
    options:
      -
        name: main_title
        specific:
          textvalue: 'Template Editor Test'
      -
        name: subtitle
        specific:
          textvalue: 'Testing the templateditor'
    ```
