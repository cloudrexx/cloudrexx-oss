The component `Routing` provides the ability for URL-rewriting and performs the resolving process of the HTTP-request.

## URL Rewriting

URL rewriting is implemented using simple *match&replace* regular expressions and can be manged under *Administration &gt; Redirections*. For further information refer to the [Knowledge Base](https://cloudrexx.atlassian.net/wiki/spaces/HELP/pages/468387968/Wie+erstelle+ich+eine+Weiterleitungsregel).

## Resolving

Resolving of the HTTP-request is done by the legacy controller `\Cx\Core\Routing\Resolver` and gets triggered right after the `preResolve`-hook of the [bootstrap process](Bootstrapping.md).

### Resolved page

After completion of the resolving process (not until the `postResolve`-hook), the resolved page (`\Cx\Core\ContentManager\Model\Entity\Page`) can be fetched as follows:

``` php
$page = $this->cx->getPage();
```

### Injection

The outcome of the resolved page can be influenced by the `resolve`-hook of the resolved component. Using this ability makes it possible to implement the so-called pretty-URLs feature within a component.

