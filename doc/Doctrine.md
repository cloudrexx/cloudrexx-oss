This article describes the integration of [Doctrine 2.4](https://www.doctrine-project.org/) in Cloudrexx.

The Doctrine Project is the home to several PHP libraries primarily focused on database storage and object mapping. The core projects are the Object Relational Mapper (ORM) and the Database Abstraction Layer (DBAL) it is built upon.

If you are not familiar with Doctrine, it is advised to first go through the official [Getting Started Guide of Doctrine](/doctrine-orm-2.4.8-docs/index.html#document-tutorials/getting-started) or at least to read the articles [Object-relational mapping](https://en.wikipedia.org/wiki/Object-relational_mapping) and [Doctrine (PHP)](https://en.wikipedia.org/wiki/Doctrine_(PHP)) in Wikipedia.

The documentation in front of you is written with the intend to dive into the implementation of the model (of your own component) right away, without having the need to go through extensive documentation in advance. However, be warned that if you encounter any issues during the implementation, resolving them might seem like an impossible task and will most likely force you to go through some of the external linked [reference documentation](#reference-documentation) anyway.

!!! warning

    To be able to follow this documentation you will need a basic understanding of [OOP](https://php.net/manual/en/oop5.intro.php) and [Namespaces](https://php.net/manual/en/language.namespaces.php) in PHP (5.3+). See [reference documentation](#reference-documentation) below for further tutorials and documentation.

### Basic Terminology

To understand the following document we have to introduce some of the used terminology by Doctrine or by the concept of an ORM in general.

Entity  

-   Every PHP object that you want to save in the database using Doctrine is called an *Entity*.
-   The term *Entity* describes objects that have an identity over many independent requests. This identity is usually achieved by assigning a unique identifier to an entity.

Entity Repository  

-   A *Entity Repository* can be seen as a container of all existing *Entities* of a specific class.

Entity Manager  

-   As said by its name, the *Entity Manager* is used to manage the *Doctrine Entities*.
-   The *EntityManager Class* is a central access point to the ORM functionality provided by Doctrine 2.
-   The *EntityManager API* is used to manage the persistence of your objects (*Entities*) and to query for persistent objects (*Entities*).
-   The *Entity Manager* uses a *UnitOfWork* to keep track of all the things that need to be done the next time *flush* is invoked.

Unit of Work  

-   The *UnitOfWork* can be seen as a repository that contains all loaded *Doctrine Entities* that are managed by the *Doctrine Entity Manager*

Entity State  

-   An entity instance can be characterized as being NEW, MANAGED, DETACHED or REMOVED. See reference documentation to [Entity States](/doctrine-orm-2.4.8-docs/index.html#entity-states).

Entity Persistence  

-   Not all attributes of a *Doctrine Entity* are automatically persisted to the database. Instead each attribute must explicitly be marked as such to get it persisted into the database.

Persist Operation  

-   Instructing the *Entity Manager* to start managing an unmanaged *Doctrine Entity* and add it to the database on the next *Flush Operation*.

Flush Operation  

-   Writing all changes made to the *Entities* managed by the *Entity Manager* (and hold by the *UnitOfWork* of the *Entity Manager*) to the database

Mapping  

-   The mapping definition between an *Entity* in PHP and its counterpart as a dataset in the database.
-   The definition to map a PHP class variable to a database field

Association  

-   The relationship between two *Entities* (One-To-One / One-To-Many / Many-To-Many / Many-To-One).
-   In a relationship, one *Entity* is always the **Owning Side** and one is the **Inverse Side**. See article [Associations](/doctrine-orm-2.4.8-docs/index.html#document-reference/unitofwork-associations) to get a full understanding of this concept. It is essential to fully understand the concept of the *Owning and Inverse Side* to be able to successfully set up an association between two *Entities*.

## Getting Started Tutorial

### Basic Mapping

[Basic Mapping](/doctrine-orm-2.4.8-docs/index.html#document-reference/basic-mapping)

#### YAML Notation

If unfamiliar with YAML, it is advised to at least go through the sections [Examples](https://en.wikipedia.org/wiki/YAML#Examples) and [Syntax](https://en.wikipedia.org/wiki/YAML#Syntax) of the [YAML article of Wikipedia](https://en.wikipedia.org/wiki/YAML) to get a basic understanding of YAML.

A comprehensive example of a *YAML mapping document* can be found in the [official documentation of Doctrine](/doctrine-orm-2.4.8-docs/index.html#example).

##### Mapping document

Entity definition (MUST)  

``` php
<fully qualified name of the class>:
    type: entity
    repositoryClass: <fully qualified name of the repository class>
    table: <database name, without table prefix>
    fields:
      <fields definition>
```

Index definition (optional)  
Field definition (MUST)  
Association definition (optional)  

### Fetching Entities

Entities are fetch through their repository which are fetched as follows:

``` php
$repo = $this->cx->getDb()->getEntityManager()->getRepository(
    'Cx\Core\ContentManager\Model\Entity\Page'
);
```

!!! warning

    For fetching a repository, you'll have to supply the FQCN of an entity like in the example above: `Cx\Core\ContentManager\Model\Entity\Page`

Then the entities can be fetched from the repository. I.e.:

``` php
$entity = $repo->find($id = 3);
```

## Advanced Topics

### Localization

Out-of-the-box localization on entities can be implemented using the [Translatable extension from Gemo](https://github.com/doctrine-extensions/DoctrineExtensions/blob/v2.4.x/doc/translatable.md).
To implement localization on an entity, it must be setup as follows:

* Entity must implement `\Gedmo\Translatable\Translatable`
* Entity must have a (non-persistable) property that is used to identify the current locale
  * That property must be specified as `gedmo\translation\locale` in the YAML mapping document
* All fields that should be localized must be marked with the property `gedmo\translatable` in the YAML maping document
* The YAML mapping document must set `gedmo\translation\entity` to [`Cx\Core\Locale\Model\Entity\Translation`](Localization.md#translation-entity)

!!! example

    YAML mapping document:
    ``` yaml
    Cx\Modules\MyComponent\Model\Entity\MyEntity:
      type: entity
      table: module_mycomponent_myentity
      gedmo:
        translation:
          locale: locale
          entity: Cx\Core\Locale\Model\Entity\Translation
      id:
        id:
          type: integer
          generator:
            strategy: AUTO
      fields:
        name:
          type: string
          gedmo:
            - translatable
    ```

    PHP entity model
    ``` php
    namespace Cx\Modules\MyComponent\Model\Entity\MyEntity;
    class MyEntity extends \Cx\Model\Base\EntityBase implements \Gedmo\Translatable\Translatable {
        /**
         * @var string
         */
        protected $locale;

        public function setTranslatableLocale($locale) {
            $this->locale = $locale;
        }
    }
    ```

### Versioning / History

Out-of-the-box versioning on entities can be implemented using the [Loggable extension from Gemo](https://github.com/doctrine-extensions/DoctrineExtensions/blob/v2.4.x/doc/loggable.md).

#### Implementation

To implement versioning on an entity, it must be setup as follows:

* Entity must implement `\Gedmo\Loggable\Loggable`
* All fields that should be versioned must be marked with the property `gedmo\versioned` in the YAML maping document
* The YAML mapping document must set `gedmo\loggable\logEntryClass` to `Cx\Core\Model\Model\Entity\LogEntry`

!!! example

    YAML mapping document:
    ``` yaml
    Cx\Modules\MyComponent\Model\Entity\MyEntity:
      type: entity
      table: module_mycomponent_myentity
      gedmo:
        loggable:
          logEntryClass: Cx\Core\Model\Model\Entity\LogEntry
      id:
        id:
          type: integer
          generator:
            strategy: AUTO
      fields:
        name:
          type: string
          gedmo:
            - versioned
    ```

#### Usage Examples

!!! example "Fetch logs of an entity"

    In this example we are fetching all logs of the entity `$entity`:
    ``` php
    $logs = $this->cx->getDb()->getEntityManager()->getRepository(
        \Cx\Core\Model\Model\Entity\LogEntry::class
    )->getLogEntries($entity);
    ```

!!! example "Fetch create logs of all entities"

    In this example we are fetching all creation logs of model `\Cx\Modules\MyComponent\Model\Entity\MyEntity`:
    ``` php
    $logs = $this->cx->getDb()->getEntityManager()->getRepository(
        \Cx\Core\Model\Model\Entity\LogEntry::class
    )->getLogsByAction(
        'create',
        \Cx\Modules\MyComponent\Model\Entity\MyEntity::class
    );
    ```

### Virtual Entities

A *Doctrine Entity* can be flagged as virtual to instruct the *Entity Manager* to not flush any changes made on the *Entity* to the database when the *Flush Operation* is performed. This is implemented by the method `Cx\Model\Base\EntityBase::setVirtual()`.

``` php
$myEntity->setVirtual(true);
```

!!! warning

    This feature is currently deprecated and should no longer be used.

### Events

During the lifecycle of an *Entity* certain events are triggered to which custom operations can be attached to. The [Event System of Doctrine](/doctrine-orm-2.4.8-docs/index.html#document-reference/events) is integrated in Cloudrexx through its own [Event System](Event Management.md).

To each of the available [Lifecycle Events](/doctrine-orm-2.4.8-docs/index.html#lifecycle-events) of an *Entity*, an [Event Listener](Event Management.md) can be registered.

!!! example

    ``` php
    $evm = $this->cx->getEvents();
    $myEntityEventListener = new \Cx\Modules\MyComponent\Model\Event\MyEntityEventListener();
    $evm->addModelListener(
        \Doctrine\ORM\Events::prePersist,
        'Cx\\Modules\\MyModule\\Model\\Entity\\MyEntity',
        $myEntityEventListener
    );
    ```

### ENUM Data Type

There are some issues with using `ENUM` type columns with Doctrine. Please refer to [Mysql Enums](/doctrine-orm-2.4.8-docs/index.html#document-cookbook/mysql-enums) for details.

Cloudrexx prefers the second solution approach presented there: [Solution 2: Defining a Type](/doctrine-orm-2.4.8-docs/index.html#solution-2-defining-a-type)

Create a type definition in the `Cx\Core\Model\Data\Enum\<component>\<class>` namespace, name the file like `CamelCasePropertyName.class.php`, and put it into the corresponding subfolder of `core/Model/Data/Enum/`.

You can find examples in that same folder.

There's an exception to that rule regarding [Inheritance](#inheritance) and discriminator columns.

### Inheritance

Please refer to [Inheritance Mapping](/doctrine-orm-2.4.8-docs/index.html#inheritance-mapping) for details.

You may use `ENUM` type discriminator columns, in which case there's no need to create a type definition as explained in [ENUM Data Type](#enum-data-type).

A working example can be found in `core/DataSource/Model/Yaml/Cx.Core.DataSource.Model.Entity.DataSource.dcm.yml`.

### Doctrine CLI

The *command line interface* of *Doctrine* is accessible through the [Workbench](Workbench.md):

    $ cx wb db doctrine

### Entity Validation

A *Doctrine Entity* can automatically be validated on the *Persist* and the *Flush Operation*. This can be achieved by mapping specific validation callables to specific attributes of an *Entity*. The validation mapping is to be set on the member variable `$validators`.

``` php
$this->validators = array(
    'integerField' => new \CxValidateInteger(),
    'textField'    => new \CxValidateString(
        array(
            'alphanumeric' => true,
            'maxlength' => 255,
        )
    ),
    'textField'    => new \CxValidateRegexp(
        array(
            'pattern' => '/^[-A-Za-z0-9_]+$/',
        )
    ),            
);
```

If you perform a flush operation while any of the currently managed entities (by the Unit-of-Work) has an invalid property set, then an exception of class `\Cx\Model\Base\ValidationException` will be thrown.

``` php
try {
    $em = $this->cx->getDb()->getEntityManager();
    $entityManager->flush();
} catch (\Cx\Model\Base\ValidationException $ex ) {
    die("validation errrors: " . $ex->getMessage());
}
```

### Debugging

#### Database

*Doctrine* is fully integrated into Cloudrexx's [Development tools](Debugging.md). When activating *database-logging* (`DBG_DB`), the effective SQL-queries made by *Doctrine* are being logged along with the regular *AdoDB* SQL-queries. To explicitly only log *Doctrine's SQL-queries* the mode `DBG_DOCTRINE` can be used:

``` php
\DBG::activate(DBG_DOCTRINE);
```

See section [Debug Levels](Debugging.md#debug-flags) for additional, *Doctrine* specific, log levels.

!!! note

    Internally, [DBG](Debugging.md) does register `Doctrine\DBAL\Logging\EchoSQLLogger()` as an `Doctrine\DBAL\Logging\SQLLogger` on Doctrine.

#### Entities

*Doctrine Entities* are complex PHP objects which do most of the time contain complex and recursive references. Therefore, PHP native's functions to dump variables like `print_r()`, `var_dump()` or `var_export()` are not ideal when using in conjunction with *Doctrine Entities*. However, Cloudrexx's own function to dump variables, which is provided by Cloudrexx's [Development tools](Debugging.md), is able to handle *Doctrine Entities*. Use `\DBG::dump()` to dump a *Doctrine Entity* PHP object:

``` php
\DBG::dump($entity);
```

### Testing

!!! warning

    This section is outdated and not accurate anylonger and must be rewritten!


The tests are located in *testing/tests/model*.

It is highly recommended to write tests for each entity and repository: If something has been changed in the Model, it is used to check whether everything still works and whether you get the expected results. So you don't have to test manually the whole module.

There is a special test case *testing/testCases/DoctrineTestCase.php*, your test case should inherit from. This test case provides the entity manager and surrounds each test with an single transaction. So there will not be any change done to the testing database.

Example in: *testing/tests/model/contentManager/PageRepositoryTest.php*

## Integration in Cloudrexx

### Packages

Currently integrated versions of Doctrine projects are as follows:

| Package                    | Namespace                       | Project                                                                                                    | Version                                                               | Location                                                                | Requires                                                                                                                                               | Used by                                                                                                                                                                                |
|----------------------------|---------------------------------|------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------|-------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `doctrine/orm`           | `Doctrine\ORM`                | [Object Relational Mapper (ORM)](/doctrine-orm-2.4.8-docs/index.html) | `2.4.8` | `lib/doctrine/Doctrine/ORM`                                           | <ul><li>`doctrine/collections~1.1`</li><li> `doctrine/dbal~2.4` </li><li> `symfony/console~2.0` </li><li> `symfony/yaml~2.1`  </li></ul>                                          | <ul><li>`\Cx\Core\Model\Entity\Db` </li><li> `Gedmo\Loggable` </li><li> `Gedmo\Tree` </li><li> `Gedmo\Translatable` </li><li> `Gedmo\Timestampable` </li><li> `Gedmo\Sluggable` </li><li> `beberlei\DoctrineExtensions` </li></ul> |
| `doctrine/dbal`          | `Doctrine\DBAL`               | [Database Abstraction Layer (DBAL)](https://www.doctrine-project.org/projects/dbal/2.12.html)              | ~~[2.4.5](https://www.doctrine-project.org/projects/dbal/2.4.html)~~  <br> [2.12.1](https://github.com/doctrine/dbal/releases/tag/2.12.1)          | `lib/doctrine/vendor/doctrine-dbal/lib/Doctrine/DBAL`                                                                                                | <ul><li> ~~`doctrine/common~2.4`~~ </li><li> `doctrine/cache^1.0` </li><li> `doctrine/event-manager^1.0` </li></ul>  | <ul><li> `doctrine/orm`  </li></ul>                                                                                             |
| `doctrine/event-manager` | `Doctrine\Common`             | [Event Manager](https://www.doctrine-project.org/projects/event-manager/latest.html)                       | [1.1.1](https://github.com/doctrine/event-manager/releases/tag/1.1.1) | `lib/doctrine/vendor/doctrine-common/lib/Doctrine/Common/`            |                                                                                                                                                        | <ul><li> `doctrine/dbal` </li></ul>                                                                                                                                                                   |
| `doctrine/common`        | `Doctrine\Common`             | [Common](https://www.doctrine-project.org/projects/common/2.4.html)                                        | [2.4.3](https://www.doctrine-project.org/projects/common/2.4.html)    | `lib/doctrine/vendor/doctrine-common/lib/Doctrine/Common`             | <ul><li> `doctrine/inflector:1.*` </li><li> `doctrine/cache:1.*` </li><li> `doctrine/collections:1.*` </li><li> `doctrine/lexer:1.*` </li><li> `doctrine/annotations:1.*` </li></ul> | <ul><li> `Gedmo\Loggable` </li><li> `Gedmo\Tree` </li><li> `Gedmo\Translatable` </li><li> `Gedmo\Timestampable` </li><li> `Gedmo\Sluggable` </li></ul>                                                                   |
| `doctrine/inflector`     | `Doctrine\Common\Inflector`   | [Inflector](https://www.doctrine-project.org/projects/inflector/1.1.html)                                  | [1.1.0](https://github.com/doctrine/inflector/releases/tag/v1.1.0)    | `lib/doctrine/vendor/doctrine-common/lib/Doctrine/Common/Inflector`   |                                                                                                                                                        | <ul><li> `doctrine/common`   </li></ul>                                                                                                                                                              |
| `doctrine/cache`         | `Doctrine\Common\Cache`       | [Cache](https://www.doctrine-project.org/projects/cache/1.4.html)                                          | [1.4.4](https://github.com/doctrine/cache/releases/tag/v1.4.4)        | `lib/doctrine/vendor/doctrine-common/lib/Doctrine/Common/Cache`       |                                                                                                                                                        | <ul><li> `doctrine/common` </li><li> `doctrine/dbal`   </li></ul>                                                                                                                                         |
| `doctrine/collections`   | `Doctrine\Common\Collections` | [Collections](https://www.doctrine-project.org/projects/collections.html)                                  | [1.3.0](https://github.com/doctrine/collections/releases/tag/v1.3.0)  | `lib/doctrine/vendor/doctrine-common/lib/Doctrine/Common/Collections` |                                                                                                                                                        | <ul><li> `doctrine/orm` </li><li> `doctrine/common`      </li></ul>                                                                                                                                       |
| `doctrine/lexer`         | `Doctrine\Common\Lexer`       | [Lexer](https://www.doctrine-project.org/projects/lexer.html)                                              | [1.2.3](https://www.doctrine-project.org/projects/lexer/1.2.html)     | `lib/doctrine/vendor/doctrine-common/lib/Doctrine/Common/Lexer`       |                                                                                                                                                        | <ul><li> `doctrine/common` </li><li> `doctrine/annotations`          </li></ul>                                                                                                                           |
| `doctrine/annotations`   | `Doctrine\Common\Annotations` | [Annotations](https://www.doctrine-project.org/projects/annotations.html)                                  | [1.2.7](https://github.com/doctrine/annotations/tree/v1.2.7)          | `lib/doctrine/vendor/doctrine-common/lib/Doctrine/Common/Annotations` | <ul><li> `doctrine/lexer:1.*`   </li></ul>                                                                                                                          | <ul><li> `doctrine/common` </li><li> OpenAPI ([`swagger-php`](https://github.com/zircote/swagger-php))   </li></ul>                                                                                       |
| `symfony/console`        | `Symfony\Component\Console`   | [Symfony Console](https://symfony.com/components/Console)                                                  | [2.0.0](https://github.com/symfony/console/tree/v2.0.0)               | `lib/doctrine/vendor/Symfony/Component/Console`                       |                                                                                                                                                        | <ul><li> `doctrine/orm` </li></ul>                                                                                                                                                                    |
| `symfony/finder`         | `Symfony\Component\Finder`    | [Symfony Finder](https://symfony.com/components/Finder)                                                    | [3.2.0 BETA1](https://github.com/symfony/finder/tree/v3.2.0-BETA1)    | `lib/doctrine/vendor/Symfony/Component/Finder`                        |                                                                                                                                                        | <ul><li> OpenAPI ([`swagger-php`](https://github.com/zircote/swagger-php)) </li></ul>                                                                                                                |
| `symfony/yaml`           | `Symfony\Component\Yaml`      | [Symfony Yaml](https://symfony.com/doc/2.6/components/yaml/introduction.html)                              | [2.6](https://github.com/symfony/yaml/tree/2.6)                       | `lib/doctrine/vendor/Symfony/Component/Yaml`                          |                                                                                                                                                        | <ul><li> `doctrine/orm` </li><li> OpenAPI ([`swagger-php`](https://github.com/zircote/swagger-php)) </li></ul>                                                                                             |


#### Extensions

The following Doctrine extensions have been integrated into Cloudrexx (in `\Cx\Core\Model\Db::getEntityManager()`):

| Extension                                                                                                    | Description                                                                                                                                      | Version                                                                       | Location                                                       | Requires                                             | Used by                                         |
|--------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------|----------------------------------------------------------------|------------------------------------------------------|-------------------------------------------------|
| [`Gedmo\Loggable`](https://github.com/Atlantic18/DoctrineExtensions/blob/v2.4.x/doc/loggable.md)           | helps tracking changes and history of objects, also supports version management                                                                  | [2.4.0](https://github.com/Atlantic18/DoctrineExtensions/releases/tag/v2.4.0) | `lib/doctrine/Gedmo/Loggable`                                | <ul><li> `doctrine/common~2.4` </li><li> `doctrine/orm^2.6.3` </li></ul> | <ul><li> `Cx\Core\Model\Entity\EntityBase` </li></ul>          |
| [`Gedmo\Tree`](https://github.com/Atlantic18/DoctrineExtensions/blob/v2.4.x/doc/tree.md)                   | this extension automates the tree handling process and adds some tree specific functions on repository.(closure, nestedset or materialized path) | [2.4.0](https://github.com/Atlantic18/DoctrineExtensions/releases/tag/v2.4.0) | `lib/doctrine/Gedmo/Tree`                                    | <ul><li> `doctrine/common~2.4` </li><li> `doctrine/orm^2.6.3` </li></ul> | <ul><li> `Cx\Core\ContentManager\Model\Entity\Node` </li></ul> |
| [`Gedmo\Translatable`](https://github.com/Atlantic18/DoctrineExtensions/blob/v2.4.x/doc/translatable.md)   | adds localization support to entities                                                                                                            | [2.4.0](https://github.com/Atlantic18/DoctrineExtensions/releases/tag/v2.4.0) | `lib/doctrine/Gedmo/Translatable`                            | <ul><li> `doctrine/common~2.4` </li><li> `doctrine/orm^2.6.3` </li></ul> | <ul><li> `Cx\Core\Model\Entity\EntityBase` </li></ul>         |
| [`Gedmo\Timestampable`](https://github.com/Atlantic18/DoctrineExtensions/blob/v2.4.x/doc/timestampable.md) | updates date fields on create, update and even property change.                                                                                  | [2.4.0](https://github.com/Atlantic18/DoctrineExtensions/releases/tag/v2.4.0) | `lib/doctrine/Gedmo/Timestampable`                           | <ul><li> `doctrine/common~2.4` </li><li> `doctrine/orm^2.6.3` </li></ul> | <ul><li> `Cx\Core\User` </li></ul>                            |
| [`Gedmo\Sluggable`](https://github.com/Atlantic18/DoctrineExtensions/blob/v2.4.x/doc/sluggable.md)         | urlizes your specified fields into single unique slug                                                                                            | [2.4.0](https://github.com/Atlantic18/DoctrineExtensions/releases/tag/v2.4.0) | `lib/doctrine/Gedmo/Sluggable`                               | <ul><li> `doctrine/common~2.4` </li><li> `doctrine/orm^2.6.3` </li></ul> | ?                                               |
| [`beberlei\DoctrineExtensions`](https://github.com/beberlei/DoctrineExtensions)                            | Adds support for MySQL/MariaDB functions like `REGEXP`, `DATE_FORMAT` etc.                                                                   | [1.2.3](https://github.com/beberlei/DoctrineExtensions/releases/tag/v1.2.3)   | `lib/doctrine/beberlei/doctrineextensions`                   | <ul><li> `doctrine/orm^2.6` </li></ul>                             | <ul><li> `Cx\Core\User` </li></ul>                            |
| `DoctrineExtension\TablePrefixListener`                                                                    | ensures Doctrine ORM uses the configured database prefix of the Cloudrexx installation.                                                          | -                                                                            | `model/extensions/DoctrineExtension/TablePrefixListener.php` |                                                      | <ul><li> `Cx\Core\Model\Db` </li></ul>                        |

### Customizings

This is a summary of the implemented customizings on the Doctrine libraries:

-   `\Cx\Core\Model\Controller\YamlDriver` (customizing of `\Doctrine\ORM\Mapping\Driver\YamlDriver`)
-   `\Cx\Core\Model\Model\Event\LoggableListener` (customizing of `\Gedmo\Loggable\LoggableListener`)
-   `\Cx\Core\Model\Controller\EntityManager` (customizing of `\Doctrine\ORM\EntityManager`)
-   `\Cx\Core\Model\Model\Event\ORM` (customizing of `\Gedmo\Loggable\Mapping\Event\Adapter\ORM`)
-   Added additional model property data types:
    -   `enum` (see ENUM Data Type)
    -   `timestamp`
    -   `password`
    -   `set`
    -   `string`

### Initialization / Configuration

Doctrine gets initialized and configured when calling the method `\Cx\Core\Model\Db::getEntityManager()` the first time. The latter is also used to fetch the *Doctrine Entity Manager*. See section Entity Manager below on how to properly fetch the *Entity Manager*.

### Entity Integration

All *Entity Models* must be located in their component's folder `Model/Entity`.

The file name and namespace of a *model* must follow the [Cloudrexx naming conventions](Naming conventions.md).

All *Doctrine Entities* must extend from the base class `Cx\Model\Base\EntityBase`. This is required to ensure that each *Entity* can be properly handled by all components of Cloudrexx.

``` php
class MyEntity extends \Cx\Model\Base\EntityBase {}
```

### Entity Repository Integration

All *Entity Repositories* must be located in their component's folder `Model/Repository`.

The file name and namespace of a *repository* must follow the [Cloudrexx naming conventions](Naming conventions.md). Additionally, the file name must contain the suffix *Repository*. I.e.: `MyEntity`*`Repository`*

All *Doctrine Entity Repositories* must extend from the base class `Doctrine\ORM\EntityRepository`.

``` php
class MyEntityRepository extends \Doctrine\ORM\EntityRepository{}
```

### Entity Manager

The *Entity Manager* can be retrieved as follows:

``` php
$em = $this->cx->getDb()->getEntityManager();
```

### Mapping Driver

Cloudrexx uses a custom YAML mapping driver (`\Cx\Core\Model\Controller\YamlDriver`) that is based on the [ORM's YAML mapping driver](/doctrine-orm-2.4.8-docs/index.html#yaml-mapping) to describe/specify the ORM metadata. Each *Doctrine* entity is described by a single *YAML mapping document*.

A *YAML mapping document* must comply to the following conventions to be valid:

-   It must describe exactly one single entity (multiple entities can't be defined in the same document)
-   The document must be located in the folder `Model/Yaml` of the related *Cloudrexx component*
-   The name of the mapping document must consist of the fully qualified name of the class, where namespace separators are replaced by dots (`.`).
-   All mapping documents must have the extension `.dcm.yml` to identify it as a Doctrine mapping file.

The location of each component's Yaml directory is automatically registered by the *System Component Repository* (`\Cx\Core\Core\Model\Repository\SystemComponentRepository`) when initializing all available Cloudrexx Components. The registration is done using the method `\Cx\Core\Model\Db::addSchemaFileDirectories()`.

Exception for Gedmo models  

The models of the **Gedmo** extensions (see section Extensions) are loaded using the [Annotation mapping driver](/doctrine-orm-2.4.8-docs/index.html#document-reference/annotations-reference). Therefore, be aware that in case you are going to extend any of the Gedmo models, you'll have to use annotations in such case. However, this does not affect the model mapping when using any of the *Gedmo* extensions. Meaning that you would still be defining the mapping data in the regular *YAML mapping document* of the *Doctrine Entity* that shall be using a specific *Gedmo* extension.

### Proxies

All entity proxies are located under `/model/proxies` in the file system and are located within the namespace `\Cx\Model\Proxies`.

Automatic proxy file generation (see [\Doctrine\ORM\Configuration::setAutoGenerateProxyClasses()](/doctrine-orm-2.4.8-docs/index.html#auto-generating-proxy-classes-optional)) is disabled in `\Cx\Core\Model\Db::getEntityManager()`.

(Re)generate proxies using the following command:

``` shell
$ cx wb db doctrine orm:generate-proxies
```

### Caching

The configured *Database cache engine* (*Administration \> Global Configuration Caching*) of the [Caching System](Caching.md) is used as the *Result-*, *Metadata-* and *Query-Cache* of Doctrine.

### Class Autoloader

All Doctrine library classes and the *Entity* model classes as well as their *Repositories* classes are loaded through the [ClassLoader](ClassLoader.md) of Cloudrexx.

## Common mistakes

`Fatal error: Uncaught exception 'ReflectionException' with message 'Class Page does not exist' in ...`

The error is: no Namespace have been added to the parameter

``` php
$this->pageRepo = $this->cx->getDb()->getEntityManager->getRepository(
    'Page'
);
```

Correct:

``` php
$this->pageRepo = $this->cx->getDb()->getEntityManager->getRepository(
    'Cx\Model\ContentManager\Page'
);
```

## Reference Documentation

ORM  

-   [Object-relational mapping (Wikipedia)](https://en.wikipedia.org/wiki/Object-relational_mapping)

Doctrine  

-   [Overview](/doctrine-orm-2.4.8-docs/index.html)
-   [Getting Started Guide](/doctrine-orm-2.4.8-docs/index.html#document-tutorials/getting-started)
-   [Basic Mapping Guide](/doctrine-orm-2.4.8-docs/index.html#basic-mapping)
-   [Association Mapping Guide](/doctrine-orm-2.4.8-docs/index.html#association-mapping)
-   [YAML Mapping Guide](/doctrine-orm-2.4.8-docs/index.html#yaml-mapping)
-   [Architecture](/doctrine-orm-2.4.8-docs/index.html#architecture)

PHP  

-   [OOP](https://php.net/manual/en/oop5.intro.php)
-   [Namespaces](https://php.net/manual/en/language.namespaces.php)
-   [How to Use PHP Namespaces](https://www.sitepoint.com/php-53-namespaces-basics/)
