## Switch to source mode

``` php
try {
    // switch content page (?section=guestbook&cmd=post) to source mode (no wysiwyg-editor)
    // but only for versions older than 3.0.1
    \Cx\Lib\UpdateUtil::setSourceModeOnContentPage(array('module' => 'guestbook', 'cmd' => 'post'), '3.0.1');
} catch (\Cx\Lib\UpdateException $e) {
    return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
} 
```

## Migrate Placeholder Changes

The method *UpdateUtil::migrateContentPage()* can be used to migrate existing module/application content pages in case a placeholder had been renamed.

``` php
* This method will replace $search with $replace in the content page(s)
* specified by the module ID $moduleId and CMD $cmd.
* If $cmd is set to NULL, the replacement will be done on every content
* page of the specified module.
* $search and $replace can either be a single string or an array of strings.
* $changeVersion specifies the Contrexx version in which the replacement
* should take place. Latter means that the replace will only be done if
* the installed Contrexx version is older than the one specified by
* $changeVersion.
UpdateUtil::migrateContentPage($module, $cmd, $search, $replace, $changeVersion)
```

!!! example

    ``` php
    try {
        \Cx\Lib\UpdateUtil::migrateContentPage(30, NULL, ' {LIVECAM_IMAGE_SHADOWBOX}', ' rel="{LIVECAM_IMAGE_SHADOWBOX}"', '2.1.3');
    } catch (\Cx\Lib\UpdateException $e) {
        return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
    } 
    ```

### Using Regexp

``` php
/**
 * Replace certain data of a content page using regexp
 *
 * This method will do a preg_replace() on pages (filtered by $criteria) data specified by $subject using $pattern as PATTERN and $replacement as REPLACEMENT.
 * Subject is either a string or an array referencing attributes of a page.
 * The behaviour of $pattern and $replacement is exactly the same as implemented by preg_replace().
 * $changeVersion specifies the Contrexx version in which the replacement
 * should take place. Latter means that the replace will only be done if
 * the installed Contrexx version is older than the one specified by
 * $changeVersion.
 *
 * @global  ContrexxUpdate     $objUpdate
 * @global  Array               $_CONFIG
 * @param   Array               $criteria         Argument list to filter page objects. Will be passed to Cx\Model\ContentManager\Repository\PageRepository->findBy()
 * @param   mixed               $pattern          The pattern to search for. It can be either a string or an array with strings.
 * @param   mixed               $replacement      The string or an array with strings (pattern) to replace
 * @param   mixed               $subject          A string or array containing the name of an attribute of the page object
 * @param   string              $changeVersion    Contrexx version of the content page
 */
migrateContentPageUsingRegex($criteria, $pattern, $replacement, $subject, $changeVersion)
```

!!! example

    ``` php
    try {
        \Cx\Lib\UpdateUtil::migrateContentPageUsingRegex(array('module'=>'access'), '/section=communiy/', 'section=access', array('content', 'target'), '3.0.0');
    } catch (\Cx\Lib\UpdateException $e) {
        return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
    } 
    ```

### Regexp Using Callback

``` php
migrateContentPageUsingRegexCallback($criteria, $pattern, $callback, $subject, $changeVersion)
```

!!! example

    ``` php
    try {
        // this adds the missing placeholders [[SELECTED_DAY]], [[SELECTED_MONTH]], [[SELECTED_YEAR]]
        $search = array(
            '/(<option[^>]+\{USERS_BIRTHDAY_(DAY|MONTH|YEAR)\}[\'"])([^>]*>)/ms',
        );
        $callback = function($matches) {
            if (strpos($matches[1].$matches[3], '{SELECTED_'.$matches[2].'}') === false) {
                return $matches[1].' {SELECTED_'.$matches[2].'} '.$matches[3];
            } else {
                return $matches[1].$matches[3];
            }
        };
        \Cx\Lib\UpdateUtil::migrateContentPageUsingRegexCallback(array('module' => 'newsletter'), $search, $callback, array('content'), '3.0.0');
    } catch (\Cx\Lib\UpdateException $e) {
        return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
    } 
    ```
