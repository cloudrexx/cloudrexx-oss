Some general rules to apply in cron job scripts:

-   *PHP & DB* error reporting must always be on
-   cron job must not output any data to _stdout_ (except for PHP or DB errors/warnings/notices/etc)
-   e-mail notification for cron job output (*MAILTO=*) must be set to developer's e-mail address

Those rules shall ensure that in case an error occurs in a cron job script, it will immediately be detected as its error reporting is sent to the developer by e-mail.

