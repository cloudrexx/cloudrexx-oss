This is the main article for everything related to JavaScript development in Contrexx.

## JavaScript Collector

The Contrexx JavaScript and CSS collector class collects all script and style tags and moves them to the [[JAVASCRIPT]] placeholder. In addition he provides a set of predefined script sets available for activation (\JS::activate(...)).
CSS is not reordered by the collector. For additional info about this class, please visit [it's API reference](https://api.contrexx.com/latest/classes/JS.html)

### Prevent collection

In order to prevent a script tag from beeing collected, simply add a space after the equals sign of the "src" attribute:

    <script type="text/javascript" src= "/path/to/source.js"></script>
    //                                 ^

Script tags without an src attribute are not collected.

## jQuery

Most JavaScript in Contrexx is based on jQuery. jQuery is loaded per default in backend mode. In frontend it is activated if a script requiring it is activated via \JS::activate(). Since some code needs a specific version of jQuery, it isn't easy to migrate to a new version. Your scripts might require a newer version of jQuery than the one loaded by Contrexx. How to do that depends if you need it in front- or backend, the principle however is the same. Do not try to replace the jQuery version used by Contrexx!

### Load a newer version of jQuery in frontend

1.  Add a script tag that loads the desired version of jQuery to the head part of you theme. As described above, add a space after the equals sign to prevent reordering of this tag.
2.  Assign jQuery to another variable using [jQuery.noConflict()](https://api.jquery.com/jQuery.noConflict/)
3.  Use the new version of jQuery via the variable you assigned it to

### Load a newer version of jQuery in backend

1.  Copy the file /cadmin/template/ascms/index.html to /customizing/cadmin/template/ascms/index.html and activate customizings
2.  Add a script tag that loads the desired version of jQuery to the head part of that file. As described above, add a space after the equals sign to prevent reordering of this tag.
3.  Assign jQuery to another variable using [jQuery.noConflict()](https://api.jquery.com/jQuery.noConflict/)
4.  Use the new version of jQuery via the variable you assigned it to

!!! example

    Here's an example (just a little sample), how an index.html file could look like with a custom jQuery version:

    ``` html
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
            "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="https://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
        <head>
            <title>My page title</title>
            <!-- meta, link and other tags -->
            
            <script type="text/javascript" src= "path to my custom jQuery version"></script>
            <script type="text/javascript">
                myjQueryVersion = $.noConflict();
            </script>
            {JAVASCRIPT}
            
        </head>
        <body>
    ...
    ```

## Contrexx JavaScript Framework

Main articles: [Contrexx Javascript Framework](Javascript Framework.md), [Contrexx Javascript Framework UI](Javascript Framework UI.md) and [Contrexx Javascript Framework I18n](Javascript Framework I18n.md)
The framework allows easy data moving between client- and server-side, and has several handy client-side functions.

## JsonData

Main article: [JsonData](Exposed methods.md)
The server side classes under /core/Json allow easy handling of JSON requests and the corresponding responses.

