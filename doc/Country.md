The `Country` component provides a database of countries according to ISO 3166.

!!! note

    There are currently two implementations.

    The recommended [model implementation](#implementation) (`\Cx\Core\Country\Model\Entity\Country`) which uses alpha-2 codes are identifiers and the [legacy database implementation](#legacy-implementation) (`\Cx\Core\Country\Controller\Country`) which uses integers as identifiers.

## Implementation

The `\Cx\Core\Country\Model\Entity\Country` model provies localized lables of all ISO 3166 countries.

!!! example

    ``` php
    $country = $this->cx->getDb()->getEntityManager()->getRepository(
        \Cx\Core\Country\Model\Entity\Country::class
    )->findOneBy(
        ['alpha2' => 'CH']
    );
    // will output: `CH`
    echo $country->getAlpha2();
    // will output: `Switzerland (CH)`
    echo $country;
    ```

## Legacy Implementation

!!! warning

    The `\Cx\Core\Country\Controller\Country` controller is marked as deprecated. Do use the [model implementation](#implementation) instead.
    

    
!!! example

    Generate a HTML-select dropdown menu:

    ``` php
    // fetch pre-selected value from POST data
    $countryId = contrexx_input2raw($_POST['country_id']);

    // output HTML-select dropdown
    print \Cx\Core\Country\Controller\Country::getMenu('country_id', $countryId);
    ```

!!! example

    Generate a custom HTML-select dropdown menu:

    ``` php
    // fetch pre-selected value from POST data
    $countryId = contrexx_input2raw($_POST['country_id']);

    // build HTML-select
    $htmlSelectDropDown  = '<select name="country_id">'
    $htmlSelectDropDown .= \Cx\Core\Country\Controller\Country::getMenuoptions($countryId);
    $htmlSelectDropDown .= '</select>'

    // output HTML-select dropdown
    print $htmlSelectDropDown
    ```
