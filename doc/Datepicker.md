In Contrexx 3, jQuery Datepicker and Datetimepicker are used for selecting a date or time. See [https://jqueryui.com/datepicker](https://jqueryui.com/datepicker) and [https://trentrichardson.com/examples/timepicker/](https://trentrichardson.com/examples/timepicker/) for further information.

## How to Use

!!! note

    * By default, the format of the date is "dd.mm.yy" and the format of the time is "hh:mm:ss".
    * The language of the picker is set dynamically due to [i18n](Javascript Framework I18n.md). If your language is not translated, you can do it yourself by adding an appropriate language file in the directory "/lib/javascript/jquery/ui/i18n".
    * The default style file is located at "/lib/javascript/jquery/ui/css/jquery-ui.css". Don't edit this file because it fits for the Contrexx backend. If you want to load your own style in the frontend, you can create a style file at "/themes/YOUR_THEME_FOLDER/jquery-ui.css". [Javascript](Javascript Framework.md) automatically loads this file if it exists.

### Activation

```php
\JS::activate('cx');
```
The above PHP code loads automatically all needed datepicker files.

!!! example

    ```html
    <script type="text/javascript">
    cx.ready(function() {
        jQuery('input[name=date]').datepicker();
    });
    </script>

    <input type="text" name="date" value="{MODULE_DATE}" />
    ```

    The above code will generate a default datepicker. The value which is set by the input attribute "value" is going to be the datepicker's default value.

!!! example

    ```html
    <script type="text/javascript">
    cx.ready(function() {
        jQuery('input[name=dateTime]').datetimepicker();
    });
    </script>

    <input type="text" name="dateTime" value="{MODULE_DATETIME}" />
    ```

    The above code will generate a default datetimepicker. The value which is set by the input attribute "value" is going to be the datetimepicker's default value.


!!! example

    ```html
    <script type="text/javascript">
    cx.ready(function() {
        jQuery('input[name=dateTimeSpecialFormat]').datetimepicker({dateFormat: 'yy-mm-dd', timeFormat: 'hh:mm', showSecond: false});
    });
    </script>

    <input type="text" name="dateTimeSpecialFormat" value="{MODULE_DATETIME_SPECIAL_FORMAT}" />
    ```
    The above code shows how to use a specific date and time format.

!!! example

    ```html
    <script type="text/javascript">
    cx.ready(function() {
        var options = {
            onSelect: function(dateText, inst) {
                // adjust start or end date to avoid an invalid date range
                var startDate = $J('input[name=dateTimeStart]').datepicker('getDate');
                var endDate = $J('input[name=dateTimeEnd]').datepicker('getDate');

                if ((startDate != null) && (endDate != null) && (startDate > endDate)) {
                    if ($J(this).attr('name') == 'dateTimeStart') {
                        $J('input[name=dateTimeEnd]').datepicker('setDate', dateText);
                    } else {
                        $J('input[name=dateTimeStart]').datepicker('setDate', dateText);
                    }
                }
            }
        };
        jQuery('input[name=dateTimeStart]').datetimepicker(options);
        jQuery('input[name=dateTimeEnd]').datetimepicker(options);
    });
    </script>

    <input type="text" name="dateTimeStart" value="{MODULE_DATETIME_START}" />
    <input type="text" name="dateTimeEnd" value="{MODULE_DATETIME_END}" />
    ```
    The above code will generate a datetimepicker for the start and end date. The option "onSelect" allows to define a function which will be executed after selecting a date or time. In this case the function will validate the selection of the user to avoid an invalid date range.
