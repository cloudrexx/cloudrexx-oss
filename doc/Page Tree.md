This class does most of the work needed. It loops through all the pages and prepares page data for displaying the pages. The following abstract methods are then used to give the entries a "view":

``` php
protected abstract function preRender($lang);

protected abstract function renderHeader($lang);

public abstract function preRenderLevel($level, $lang, $parentNode);

protected abstract function preRenderElement($level, $hasChilds, $lang, $page);

protected abstract function renderElement($title, $level, $hasChilds, $lang, $path, $current, $page);

protected abstract function postRenderElement($level, $hasChilds, $lang, $page);

public abstract function postRenderLevel($level, $lang, $parentNode);

protected abstract function renderFooter($lang);

protected abstract function postRender($lang);
```

In addition theres an abstract method init() so that inherited classes do not have to override the constructor.

## XmlSitemapPageTree

This class is used to create the XML sitemap. It is a simple PageTree. To write the XML files it defines the following additional methods:

``` php
public static function write()

protected function writeXML()

protected function getLastModificationDate($page)

protected function getChangingFrequency($page)
```

## SigmaPageTree

This class does no more than declaring the field $template containing a \Cx\Core\Html\Sigma instance and a setter for it (setTemplate($template)). All inherited classes can use the templating system easely.

### Classes that inherit from SigmaPageTree

The following classes are implentations of SigmaPageTree. For details about their behaviour see [Navigation](Navigation.md):

#### NavigationPageTree

Creates simple non-nested nested structures of all levels that are specified as a block in the template. Children are parsed only if the parent element is active.

#### NestedNavigationPageTree

Creates nested structures including all levels specified by a special placeholder in the template. Children are parsed only if the parent element is active unless the special placeholder ends with _full.

#### DropdownNavigationPageTree

Creates nested or non-nested structures including all levels specified as a block in the template. All children are parsed even if they are not in an active branch.

