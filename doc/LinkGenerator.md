The LinkGenerator is used to translate the *Node-URL-Notation* placeholders by their URLs/slug-path. More Information in [Content](Content.md).

``` html
$html = '<a href="{NODE_SHOP}">Online Shop</a>';
  
// replace all node-url placeholders found in $html by their URL-notation
// replacement is done by reference on $html
\LinkGenerator::parseTemplate($html);
  
// Assuming that the URL-notation of the application shop is /en/online-shop,
// the content of $html would then be:
  
echo $html; // <a href="/en/online-shop">Online Shop</a>
```