The component "DataAccess" allows access to [DataSources](DataSource.md) via command line or a RESTful interface.

## v1

DataAccess provides a command "v1". It allows access to [DataSources](DataSource.md) configured with permissions. In CLI the synopsis is as follows:

``` bash
v1 [<outputModule>] <dataAccessName> [<elementId>] [apikey=<apiKey>] [<options>]
```

Piping data into this command will be interpreted like a PUT request to the RESTful API.

Using the RESTful interface the syntax is as follows:

``` bash
/api/v1/<outputModule>/<dataAccessName>/[<elementId>][?apikey=<apiKey>][<options>]
```

### Arguments

#### &lt;outputModule&gt;

The name of an output module. Currently known output modules are "json" and "raw". See output modules for more info.

#### &lt;dataAccessName&gt;

The name of the [DataAccess](RESTful API.md) to interact with. The [DataSource](DataSource.md) associated with it is implicitly used.

#### &lt;elementId&gt;

If this is specified, output is limited to the element with this ID, otherwise all entries are returned. If the ID of a [DataSource](DataSource.md) has multiple fields (combined key), do join them by a slash.

#### &lt;apiKey&gt;

This allows you to specify the API key if you authenticate by API key.

#### &lt;options&gt;

Options are key/value pairs. In CLI they are separated by a space, in RESTful interface by ampersand.

The following options are currently known:

<table>
<thead>
<tr class="header">
<th><p>Key</p></th>
<th><p>Synopsis</p></th>
<th><p>Description</p></th>
<th><p>Example</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><code>order</code></p></td>
<td><p><code>[&lt;field&gt;]=&lt;direction&gt;[&amp;...]</code></p></td>
<td><p>Orders the output by <code>&lt;field&gt;</code> in direction <code>&lt;direction&gt;</code> where <code>&lt;direction&gt;</code> can be one of <code>ASC</code> or <code>DESC</code>. Multiple fields can be specified. Precedence follows occurrence in set option <code>order</code>.</p></td>
<td><p><code>order[id]=ASC&amp;order[name]=DESC</code></p></td>
</tr>
<tr class="even">
<td><p><code>filter</code></p></td>
<td><p><code>[&lt;field&gt;][&lt;operation&gt;]=&lt;value&gt;</code></p></td>
<td><p>Limits output to elements that meet all specified conditions. Expects an array. Default operation is <code>eq</code>. For an exhaustive list of available operations see <a href="DataSource" title="wikilink">DataSource</a>.</p>
<div style="background-color: rgb(255, 238, 170);padding:10px;font-style:italic;">
<p><strong>Note:</strong> Currently this is not supported via CLI.</p>
</div></td>
<td><p><code>filter[name][eq]=bla&amp;filter[done][eq]=1</code></p></td>
</tr>
<tr class="odd">
<td><p><code>limit</code></p></td>
<td><p><code>&lt;limit&gt;[,&lt;offset&gt;]</code></p></td>
<td><p>Limits the output to <limit> elements beginning at <offset>. If option is not set, then <limit> is automatically being set to <code>10</code>. A value greater than <code>50</code> is not supported and will be reset to <code>50</code>.</p>
<div style="background-color: rgb(183, 221, 232);padding:10px;font-style:italic;">
<p><strong>Note</strong>: The <code>meta</code> property of the response of a <code>GET</code>-request does always contain the following data that could be used to implement a paging algorithm on the client side:</p>
<ul>
<li><code>limit</code>: Applied limit (defaults to <code>10</code> if option <code>limit</code> has not been set)</li>
<li><code>offset</code>: Applied offset (defaults to <code>0</code> if it has not been set through option <code>limit</code>)</li>
<li><code>matched</code>: Total number of entries found (matching the set filter <code>filter</code> if set)</li>
<li><code>count</code>: Number of entries being returned
</div></li>
</ul></td>
<td><p><code>limit=30,60</code></p></td>
</tr>
<tr class="even">
<td><p><code>locale</code></p></td>
<td><p><code>&lt;locale&gt;</code></p></td>
<td><p>Sets the locale used to set or get localized fields.</p></td>
<td><p><code>locale=de_CH</code></p></td>
</tr>
<tr class="odd">
<td><p><code>version</code></p></td>
<td><p><code>&lt;version&gt;</code></p></td>
<td><p>Set to next version number on <code>PUT</code> and <code>PATCH</code> requests.</p></td>
<td><p><code>version=3</code></p></td>
</tr>
</tbody>
</table>

Here's an example using all options in CLI and RESTful interface notation:

``` bash
./cx v1 calendar-event apikey=123 order=id/ASC limit=7 locale=de_CH
```

``` bash
/api/v1/json/calendar-event/?apikey=1&order=id/ASC&filter[invitationSent][eq]=1&limit=7&locale=de_CH
```

### Input type

The input is treated according to the *content_type* header of the request. Currently there's no way to specify the input format for CLI requests. The following content types are currently supported:

| Content type                      | Treated as                                        | Remarks                           |
|-----------------------------------|---------------------------------------------------|-----------------------------------|
| application/x-www-form-urlencoded | URL encoded data                                  | This is the default               |
| application/json                  | JSON encoded data. A single "null" is not allowed | Not yet supported, in development |
| multipart/form-data               | Not yet implemented                               |                                   |

### Output modules

| Name | Description                                                                          | Remarks                                                                                      |
|------|--------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------|
| json | JSON encoded output containing metadata. See JSON output module. |                                                                                              |
| cli  | Produces formatted, human readable output; arrays are converted to tables.           | Only available in CLI mode. Do not use this for automated processing!                        |
| raw  | Outputs binary data.                                                                 | Only available for [MediaSource](MediaSource.md). You must request exactly one file. |

#### JSON

The JSON output module returns the following JSON structure:

``` json
{
    "status":<status>,
    "meta":<meta_info>,
    "messages":<messages>,
    "data":<data>
}
```

-   **status** is "ok" or "error".
-   **meta** contains non further specified data about the result.
-   **messages** contains an array with user messages.
-   **data** contains the requested data as a JSON object.

