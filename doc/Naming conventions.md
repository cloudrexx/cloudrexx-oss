This article describes the naming conventions for Cloudrexx development.

## General

-   Use of clever names makes everything easy! For example `/modules/Demo/Controller/DemoModuleController.class.php` would be a stupid filename, because everything the filename consists of is already contained in the file's path. Use something like `modules/Demo/Controller/Frontend.class.php` instead. If you work on the Demo module, you hopefully will know what module you are working on, and it should be obvious if the file is part of model, view or controllers.
-   Avoid keywords of all used languages ([PHP keywords](https://www.php.net/manual/en/reserved.keywords.php), [JS keywords](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Reserved_Words)), because they can cause trouble when used in namespaces or as a class, interface or attribute name.
-   Always use singular names. Use the singular form of the noun to label something (component name, variable, class, etc.)

All names (component names, variables, classes, methods, HTTP parameters, ...) are written in one of the following ways. **Never mix these notations!** A string can be converted between each of those, without any loss of data:

| Code | Notation                                     | Description                                                                                                                                                                                                                                                                                                                      | URL | Class loader | HTTP 1.0 |
|------|----------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------|----------------|------------|
| CC   | Camel case                                   | CC strings are written in lowercase, except for first letter per "word".                                                                                                                                                                                                                                                         | `Url` | `ClassLoader`  | `Http10`   |
| CCL  | Camel case, starting with a lowercase letter | CCl notation works like CC, with the exception that the first letter is lowercase.                                                                                                                                                                                                                                               | `url` | `classLoader`  | `http10`   |
| USL  | Underline separated, lowercase               | USl notation splits all words with an underline character (`_`). All characters are written in lowercase.                                                                                                                                                                                                                        | `url` | `class_loader` | `http_1_0` |
| USU  | Underline separated, capital letters         | USu notation is the same as USl, except that all letter are written uppercase.                                                                                                                                                                                                                                                   | `URL` | `CLASS_LOADER` | `HTTP_1_0` |
| USC  | Underline separated, camel case              | USC is a mixture of USU/USL and CC. Words are separated using camelcase, but special characters are replaced by an underline.                                                                                                                                                                                                    | `Url` | `ClassLoader`  | `Http_1_0` |
| HR   | Human readable                               | By human readable, we mean a notation that is quite similar to the English writing. Since the software does not now the English grammar, there might be some mistakes with upper and lower case.<br>HR notation starts with a capital letter, all the following letters are written in lowercase. "Words" are separated by a space. | `Url` | `Class loader` | `Http 1 0` |
| HS   | Hyphen separated                             | HS strings are written in lowercase and separated by a hyphen (`-`).                                                                                                                                                                                                                                                             | `url` | `class-loader` | `http-1-0` |

## How to write what

Here's a table showing how to write what. Things that needed further information are listed below. This table is not limited to PHP.

| What                                                                                                                      | Notation to use                                           | Reason |
|---------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------|--------|
| Namespaces                                                                                      | CC                                      | ...    |
| Events                                                                                              | Event Naming Scheme                 | ...    |
| Classes, Interfaces and Traits                                                                                            | CC                                     | ...    |
| Class attributes and variables                                                                                            | CCL                                   | ...    |
| Class constants                                                                                                           | USU                                  | ...    |
| Methods and Functions                                                                | CCL                                    | ...    |
| Components                                                                                      | CC                                     | ...    |
| DB names, tables and attributes                                                            | USL                                    | ...    |
| DB ENUM and SET values                                                                                                    | USU                                  | ...    |
| HTTP parameter names                                                                                                      | CCL                                   | ...    |
| Files and folders                                                                        | CC                                     | ...    |
| Template placeholders                                                                | USU                                    | ...    |
| Template blocks                                                                            | USL                                   | ...    |
| cmd/section                                                                                                               | CC                                     | ...    |
| [Exposed methods](Exposed methods.md), Command mode commands | CC                                     | ...    |
| HTML attributes, classes, ids, names                                                                                      | HS                                     | ...    |
| Version numbers                                                                                                           | [Version Number Format](Version Number Format.md) | 5.0.2  |

### Namespaces

Use namespaces that match the class's or interface's location in the file system. This allows the [ClassLoader](ClassLoader.md) to auto-load this file without unnecessary searching.

### Methods and Functions

Use idiomatic names for methods and functions to make the reading of your code as straightforward as possible.

### Events

Events constist of up to 3 parts following the scheme `<component>[.<entity>]:<action>`

| Placholder  | type                   | wordtype                | usage                                                          | meaning                                                      |
|-------------|------------------------|-------------------------|----------------------------------------------------------------|--------------------------------------------------------------|
| `<component>` | CC   | Noun                    | Add always                                                     | Use the name of the component which registers the event      |
| `<entity>`   | CC   | Noun                    | Add if the event belongs to an entity or part of the component | Name of the related entity or part of the component          |
| `<action>`   | CCL | Verb in imperative form | Add always                                                     | Describes what happened just before this event was triggered |

Examples:

-   `MediaBrowser.Plugin:initialize`
-   `Cache:clear`
-   `Model:prePersist`

### Components

Be sure you don't use a reserved PHP keyword (see General) or the name of an existing module, core_module, or core component.

### Database tables

The name of a database table must follow the following scheme USL notation):

    DBPREFIX[core|core_module|module]_{COMPONENT_NAME}_{ENTITY_NAME}

Example: `contrexx_core_module_news_article` (whereas `DBPREFIX` is set to `contrexx_`)

### Files and folders

Files of course end with a type specific suffix. For PHP files, the [ClassLoader](ClassLoader.md) requires the ending `.class.php` or `.interface.php` for classes/interfaces to load properly.

### Template placeholders

The general naming scheme for placeholders is as follows (USU notation):

    [[{COMPONENT_NAME}[_{SECTION}]{OBJECT_NAME}[_{OBJECT_ATTRIBUTE}]]]

A few examples:

-   `[[ACCESS_USER_ID]]`
-   `[[ACCESS_SETTINGS_MESSAGE]]`

Placeholders used for the *Interface Text* of the *GUI* do use a special naming scheme. Refer to the article [Text-Variable](Interface Text.md).

### Template blocks

The general naming scheme for placeholders is as follows (USL notation):

    <!-- (BEGIN|END) {COMPONENT_NAME}[_{SECTION}]{OBJECT_NAME}[_{OBJECT_ATTRIBUTE}] -->

!!! example

    ``` html
    <!-- (BEGIN|END) access_sociallogin_provider -->
    ```

!!! example

    ``` html
    <!-- (BEGIN|END) access_signup_confirmation_success -->
    ```

    