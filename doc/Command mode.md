The [System Mode](System Modes.md) `command` is a special mode Cloudrexx can run in. This mode allows to execute one command per request, which is useful for command-line or API-like cases.

## Trigger

The command mode is automatically triggered in the following cases:

-   Cloudrexx is executed from command-line.
-   The HTTP request starts with the offset defined in `\Cx\Core\Core\Controller\Cx::FOLDER_NAME_COMMAND_MODE` (`/api` by default).
-   An asynchronous command is being executed.

## Usage

### Command-line

Cloudrexx comes with the helper script `cx` which starts Cloudrexx in command mode. Alternatively you may use `php index.php` to start Cloudrexx in command mode.

The synopsis is as follows:

``` php
./cx <command>[ <argumentList>]
```

### HTTP

The synopsis is as follows:

``` php
/api/<command>[<argumentList]
```

### Asynchronous

A component can execute command asynchronously in command mode as follows:

``` php
$this->getComponent('Core')->execAsync(
    $command,
    $arguments
);
```

## Commands

Commands for command mode can be registered by components. Each command has a [`Permission`](Permission class.md). If the requested command is not found or not accessible, the built-in `help` command is executed.

### `help`

This command lists all available commands (while respecting permissions) and a short or detailed description.

### `v1`

This command provides a [RESTful API](RESTful API.md).

### `Data`

This command allows access to [Exposed methods](Exposed methods.md). Call as follows:

``` php
./cx Data <outputModule> <adapterName> <method>[ <arguments>]
```

### Component specific commands

Aside of the above listed commands there are several more commands available. The list of available commands depends on the installed components which do provide the commands.

### Register your own command

Own commands can be registered in the `ComponentController` of your [Component](Component Definition.md). To implement your own command, the following three steps are required:

1\. Register command  

Add the method `getCommandsForCommandMode()` to your `ComponentController` and return an array with the name(s) of the command(s) to register:

``` php
/**
 * {@inheritdoc}
 */
public function getCommandsForCommandMode() {
    return array(
        'MyCommand',
    );
}
```

The above example will register a publicly available command that is executable by anybody as it has no execution restriction set. Therefore is it highly advised to only register commands that have an execution [`Permission`](Permission class.md) set. The following example restricts the execution to the `command` mode:

``` php
/**
 * {@inheritdoc}
 */
public function getCommandsForCommandMode() {
    return array(
        'MyCommand' => new \Cx\Core_Modules\Access\Model\Entity\Permission(
            array(),
            array('cli'),
            false
        ),
    );
}
```

2\. Add `help` description  

Add the method `getCommandDescription()` to your `ComponentController` and return the command's description as string:

``` php
/**
 * {@inheritdoc}
 */
public function getCommandDescription($command, $short = false) {
    switch ($command) {
        case 'MyCommand':
            if ($short) {
                return 'Performs my command';
            }
            return $this->getCommandDescription($command, true) . '

Usage: ./cx MyCommand [-v]';
        default:
            return '';
    }
}
```

3\. Implement command execution  

Finally, add the method `executeCommand()` to your `ComponentController` as wrapper to call your command:

``` php
/**
 * {@inheritdoc}
 */
public function executeCommand($command, $arguments, $dataArguments = array()) {
    switch ($command) {
        case 'MyCommand':
            // optionally parse arguments
            // this example does parse the argument -v
            if (
                isset($arguments[0]) &&
                isset($arguments[0]) == '-v'
            ) {
                echo 'Activating verbose mode' . PHP_EOL;
            }

            // execute your command specific commands here

            // ensure to output any return message at the
            // end of the command execution
            echo 'MyCommand has been executed' . PHP_EOL;
            break;
    }
}
```

## Arguments

Command mode knows three different kinds of arguments:

-   Simple arguments: This is a string with no spaces
-   Key/value pairs: Key and value are separated by "="
-   A single data argument

### Command-line

``` bash
echo "Data" | ./cx MyCommand SimpleArgument Key=Value
```
The above example shows an example using all three kinds of arguments:

-   A simple argument named "SimpleArgument"
-   A Key/value pair named "Key" with the value "Value"
-   The data argument containing "Data"

### HTTP

``` bash
/api/MyCommand/SimpleArgument?Key=Value
```

The above example shows an example using two kinds of arguments:

-   A simple argument named "SimpleArgument"
-   A Key/value pair named "Key" with the value "Value"

The data argument is the POST data sent with the request if the request is a POST request.

