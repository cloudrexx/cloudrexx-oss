The component `NetManager` currently provides two functions:

- [a GUI to manage the mapped domains that are used for serving HTTP(S) requests](#http-domain-mapping-management)
- [sender domain verification (along with a GUI)](#sender-domain-verification)

## HTTP-Domain mapping management
The `NetManager` component provides a GUI to manage the mapped domains (by component [Net](Net.md)) that are used for serving HTTP(S) requests.

## Sender Domain Verification
Cloudrexx ensures that only mails using a verified sender domain are sent from the system.
Sender verification is implemented by `NetManager` and enforced by [MailTemplate](MailTemplate.md). `NetManager` provides an event that allows components to announce the sender domains (along with their specific usages) they will be using. `NetManager` then provides a GUI to list each announced sender domain along with their usages.

### Implementation
Implement an [EventListener](Event Management.md#register-listener) that listens to the event `NetManager:getSenderDomains` and does return a list of used sender domains as `array`.
The listener method must accept one argument `$config` of type `array`. `$config` currently contains only the key `usages` which is either `true` or `false`. If set to `true`, then the listener method is expected to also return the `usages` for each sender domain.
The returned `array` of the listener method must be of following form:
``` php
[
    '<domain>' => [
       'usages' => [
            [
                'editurl' => '<edit_url>',
                'title' => '<title>',
            ],
            ..
        ],
    ],
    ...
]
```
!!! note

    Whereas the following replacement has to be applied:
    
    - `<domain>`: by the punycode representation of the sender domain (see [Internationalized domain name conversion](Net.md#internationalized-domain-name-conversion)),
    - `<edit_url>`: by the absoute URL to the edit view of the entity that uses the sender domain
    - `<title`: by the title of the entity using the sender domain
    
    If `$config['usages']` is set to `false` then the `usages` key should be left out to keep the processing time as short as possible.

!!! example

    First register your [event listener](Event Management.md#register-listener) in your [ComponentController](Component Definition.md#write-own-componentcontroller)
    ``` php
    /**
     * @inheritDoc
     */
    public function registerEventListeners() {
        $this->cx->getEvents()->addEventListener(
            'NetManager:getSenderDomains',
            new \Cx\Modules\MyComponent\Model\Event\MyComponentEventListener($this->cx)
        );
    }
    ```
    Then implement the event in your event listener (assuming your event lister extends from `\Cx\Core\Event\Model\Entity\DefaultEventListener`):
    ``` php
        /**
         * @param array $config Config from event NetManager:getSenderDomains
         */
        public function NetManagerGetSenderDomains(array $config = []): array {
            $senderDomains = [];
            // logic to collect used sender domains from your component
            return $senderDomains;
        }
    }
    ```

When using MailTemplate's [E-Mail templates](MailTemplate.md#e-mail-templates), you can use the helper [`\Cx\Core\MailTemplate\Controller\MailTemplate::getSenderDomains()`](https://dev.cloudrexx.com/phpdoc/classes/Cx-Core-MailTemplate-Controller-MailTemplate.html#method_getSenderDomains) in your listener method to fetch the sender domain usages of the associated _Mail Templates_.

!!! Example

    ```php
    /**
     * @param array $config Config from event NetManager:getSenderDomains
     */
    public function NetManagerGetSenderDomains(array $config = []): array {
        $senderDomains = [];
        foreach(
            \Cx\Core\MailTemplate\Controller\MailTemplate::getSenderDomains(
                'MyComponent',
                \Cx\Core\Routing\Url::fromBackend(
                    'MyComponent',
                    'mailtemplate_edit',
                    0,
                    ['active_tab' => 1]
                )
            ) as $domain => $data
        ) {
            if (!isset($senderDomains[$domain])) {
                $senderDomains[$domain] = $data;
                continue;
            }
            $senderDomains[$domain]['usages'] += $data['usages'];
        }
        return $senderDomains;
    }
    ```
