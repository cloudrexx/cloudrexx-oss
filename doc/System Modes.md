Cloudrexx is run in purpose-optimized modes. Each initialization of Cloudrexx (*HTTP(S) Request, [CLI command](Command mode.md), [RESTful-API-call](RESTful API.md) and [JsonData](Exposed methods.md)-calls*) is executed/processed in one of the defined system modes. Each mode is optimized for a specific purpose and does therefore only execute a subset of the whole system components. The selection of the system mode is done automatically based on how the initialization has been triggered.

## Modes

| Mode                                 | Description                                                                                                                                     | Automatic invocation on...                                                                                                                                                | Identifier                                   | Bootstrapping                                                                                                                                                                           |
|--------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| [`Frontend`](Frontend mode.md) | By default, Cloudrexx is run in `Frontend` mode. This mode is used to process regular HTTP(S)-requests made to the website (from web browsers). | - All HTTP(S)-requests (except those to `/cadmin` and `/api`)                                                                                                            | `\Cx\Core\Core\Controller\Cx::MODE_FRONTEND` | <br>- [Stage 1](Bootstrapping.md#STAGE_1_init)<br>- [Stage 2](Bootstrapping.md#STAGE_2_config)<br>- [Stage 3 (Frontend)](Bootstrapping.md#STAGE_3_front_and_backend) |
| [`Backend`](Backend mode.md) | The management interface (a.k.a *Backend*) is run in `Backend` system mode.                                                                     | - All HTTP(S)-requests to `/cadmin`<br>- [Legacy JsonData](Exposed methods.md)-calls                                                                                  | `\Cx\Core\Core\Controller\Cx::MODE_BACKEND`  | <br>- [Stage 1](Bootstrapping.md#STAGE_1_init)<br>- [Stage 2](Bootstrapping.md#STAGE_2_config)<br>- [Stage 3 (Backend)](Bootstrapping.md#STAGE_3_front_and_backend)  |
| [`Command`](Command mode.md) | Commands are executed in `Command` system mode.                                                                                                 | <br>- All HTTP(S)-requests to `/api`<br>- [RESTful-API](RESTful API.md)-calls<br>- Invocation form [CLI](CLI script.md)<br>- [JsonData](Exposed methods.md)-calls | `\Cx\Core\Core\Controller\Cx::MODE_COMMAND`  | - [Stage 1](Bootstrapping.md#STAGE_1_init)<br>- [Stage 2](Bootstrapping.md#STAGE_2_config)<br>- [Stage 3 (Command)](Bootstrapping.md#STAGE_3_command)               |
| [`Minimal`](Minimal mode.md) | Can be used to access the Cloudrexx framework in a standalone PHP-script.                                                                       | \-                                                                                                                                                                        | `\Cx\Core\Core\Controller\Cx::MODE_MINIMAL`  | - [Stage 1](Bootstrapping.md#STAGE_1_init)<br>- [Stage 2](Bootstrapping.md#STAGE_2_config)                                                                             |

## Manual Invocation

In a standalone PHP-script, the invocation of each mode can be forced as follows:

``` php
/* set $mode to one of:
 * \Cx\Core\Core\Controller\Cx::MODE_FRONTEND
 * \Cx\Core\Core\Controller\Cx::MODE_BACKEND
 * \Cx\Core\Core\Controller\Cx::MODE_COMMAND
 * \Cx\Core\Core\Controller\Cx::MODE_MINIMAL
 */
require_once '/<path to Cloudrexx installation>/core/Core/init.php';
$cx = init($mode);
```

