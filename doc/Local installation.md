## Setup a local Cloudrexx environment

Select the operating system on which you want to perform the local installation:

- [Environment setup Linux](Environment setup/Environment setup Linux.md)
- [Environment setup macOS](Environment setup/Environment setup macOS.md)
- [Environment setup Windows](Environment setup/Environment setup Windows.md)