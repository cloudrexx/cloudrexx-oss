The [System Mode](System Modes.md) `Backend` is used as the management interface of Cloudrexx.

## Invocation

The _Backend_ mode is automatically invocated on all HTTP(S)-requests made to the endpoint `/cadmin` and the [Legacy JsonData](Exposed methods.md)-calls.
