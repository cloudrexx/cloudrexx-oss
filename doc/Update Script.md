## Features

-   File Modification Detection (md5-checksum validation)
-   Update Protocol (update/dbg.log)
-   Application Migration (CSS & Content)
-   Submission Form Migration

### Customizing Flowchart

This flowchart describes the process of one file which is updated. The old file means the file in the Contrexx installation.

![Customizing Flowchart](img/Customizing_Flowchart.png)

## API

### Database / Content Migration

-   [Database Structure Change](Update Script Database Structure Change.md)
-   [Data Manipulation](Update Script Data Manipulation.md)
-   [Placeholder Change](Update Script Placeholder Change.md)

### Flow-Control

#### script timeout

Calling the function *checkTimeoutLimit()* will check if the script timeout will be reached soon. If so, the update process will automatically be interrupted to prevent a script timeout.

``` php
foreach($arrSlowSQLQueries as $query) {
// check if script timeout will be reached soon
checkTimeoutLimit();

// execute very slow SQL query
UpdateUtil::sql($query);
}
```

#### out of memory

Calling the function *checkMemoryLimit()* will check if script memory limit will be reached soon. If so, the update process will automatically be interrupted to prevent a fatal error due to a lack of memory.

``` php
foreach($arrLargeFilePaths as $file) {
// check if we still got some memory left for the next operation
checkMemoryLimit();

// load some data into memory
$content = file_get_contents($file);
}
```

#### user interaction

By using the function *setUpdateMsg()* a message/dialog for user interaction can be displayed:

``` php
// Set title of update screen
setUpdateMsg('Titel of update message', 'title');

// Add message/dialog content
setUpdateMsg('Content of update message. Can contain HTML-code', 'msg');

// Update the ''Next''-button (this sample code should always be used as is)
setUpdateMsg('<input type="submit" value="'.$_CORELANG['TXT_CONTINUE_UPDATE'].'" name="updateNext" /><input type="hidden" name="processUpdate" id="processUpdate" />', 'button');

// IMPORTANT: Returning FALSE is very important to abort any further script execution.
// Failing to return FALSE will no properly interrupt the update script
return false;
```

## Structure

### core

Core update scripts. These scripts don't need to be modified in general, because they get automatically generated/updated with new information when creating an update package.

backendAreas.php  

-   Data gets taken from the database table contrexx_backend_areas

modules.php  

-   Data gets taken from the database table contrexx_modules

repository.php  

-   Data gets taken from the database table contrexx_module_repository

settings.php  

-   Data gets taken from the database table contrexx_settings
-   Modifications to the database table contrexx_settings aren't needed to include in a update component, due that all data of this table is automatically inserted into the update component settings.php. Well, in the case you have to do some special handlings with a variable from contrexx_settings, e.g. for user customization, you could do that before the array with the settings are processed.

version.php  

-   Data gets taken from the release configuration

### core_module

Update scripts of core modules/applications.

### module

Update scripts of modules/applications.

## Update steps

1.  Backup customized files and copy new ones into the folders
2.  UTF-8 migration
    -   If collation isn't utf-8 already, list possible utf8 collations and migrate tables and templates. Templates are only migrated if $\_DBCONFIG\['charset'\] isn't set to utf8
3.  Session-table migration
    -   Check if contrexx_session_variable matches the new structure and alter it if necessary or create the table
    -   Check if contrexx_sessions matches the new structure and alter it if necessary or create the table
4.  Version 2.2.6 and older
    -   Migrate the content
        -   Migrate nodes, pages to new Doctrine entities
        -   Group / combine the migrated pages if they exist in several languages (Person who executes the update decides which pages shall be tied together)
        -   Remove the pages which shall no longer exist (Person who executes the update decides which pages shall be removed)
        -   Migrate aliases
        -   Migrate blocks
        -   Drop the old tables:
            -   contrexx_content
            -   contrexx_content_history
            -   contrexx_content_logfile
            -   contrexx_content_navigation
            -   contrexx_content_navigation_history
            -   contrexx_content_alias_source
            -   contrexx_content_alias_target
            -   contrexx_module_block_rel_lang
    -   update/updates/VERSION/components/core/core.php
        -   Update charset of languages to UTF-8 where charset isn't set to UTF-8 already
        -   Add access-id 127 to all the groups which have access-id 5
        -   Migrate contrexx_modules table to new structure
        -   Migrate contrexx_logs table to new structure
        -   Migrate contrexx_backend_areas table structure to support the new access system
        -   Migrate contrexx_lib_country table to the new structure
        -   Insert all countries into the contrexx_lib_country table
        -   Migrate contrexx_language table structure to support mobile templates, fallback language and app device templates
        -   Migrate contrexx_language table structure to allow no fallback language
        -   Migrate contrexx_skins table structure to disallow templates with the same name. **This will drop duplicates!**
        -   Change submenu css identifier from id to class in all .css files in all available templates
        -   Add {JAVASCRIPT} placeholder right before the closing head-tag in all index.html files in all available templates
        -   Alter the contrexx_log table structure to support / fix timezones
        -   Alter the contrexx_sessions table structure to support the remember me flag
        -   Remove the old Cache-Class to prevent classloader issues
        -   Add access-id 178 to all the groups which have access-id 10 or access-id 119 assigned
        -   Check if contrexx_core_text exists and create it in case it's missing
        -   Create contrexx_component table and add Workbench, FrontendEditing and ContentManager
    -   For versions older than 3.0.0
        -   Get all the installed components and check if one (or multiple) of the potentially missed components in fact is (or are) missing
        -   Check if one (or multiple) table(s) of one (or multiple) missed component(s) has the comment 'cx3upgrade' (this means this component or rather this table is conflicted)
        -   Create a list of all the conflicted components
    -   Loop over all files in update/updates/VERSION/components/(core_module\|module) and execute either \_FileNameUpdate() if the component is installed or \_FileNameInstall() if it's not installed yet
    -   Migrate contrexx_settings
    -   update/updates/VERSION/components/core/settings.php
        -   Change googleSitemapStatus to xmlSitemapStatus
        -   Remove fileuploader settings
        -   Loop over new settings and update the existing ones and add the new ones. In case a setid is misused, a temporary id is given to the setting which misuses the setid and update both settings to the new values
        -   Update setid to 1 for all settings which use setid 0
        -   List available time zones and update the contrexx_settings value to the selected time zone and store it in $\_CONFIG\['timezone'\] so it will be written into the configuration.php file
        -   Generate the config/settings.php file
        -   Alter contrexx_settings setid column to integer(6) UNSIGNED NOT NULL auto_increment
        -   Migrate contrexx_settings_images table to the new structure
        -   For versions older than 3.0.3
            -   Set image settings for image_cut_width, image_cut_height, image_scale_width, image_scale_height and image_compression
    -   Write new configuration.php file
        -   Prepare the new configuration template and set database and ftp connection information
        -   Replace current content with the new one
    -   Force new page request to ensure that config/version.php is no longer loaded.
    -   update/updates/VERSION/components/core/modules.php
        -   Truncate existing contrexx_modules table
        -   Fill contrexx_modules with the new component information
    -   update/updates/VERSION/components/core/backendAreas.php
        -   Truncate existing contrexx_backend_areas table
        -   Fill contrexx_backend_areas with the new data
        -   If version is older than 3.1.0
            -   Add access id 170 to groups which have access id 163 assigned
        -   If version is older than 2.0.2
            -   Select group_id and access_id from contrexx_access_group_static_ids where access_id is equal to one of these: 116, 122, 123, 140 and 141
            -   Replace these access ids with their assigned values: 116 =&gt; 145, 122 =&gt; 146, 123 =&gt; 147, 140 =&gt; 148 and 141 =&gt; 149
    -   Module repository
        -   Truncate existing contrexx_module_repository table
        -   Migrate contrexx_module_repository to the new table structure
        -   Search for repository\_(\[0-9\]+).php files in updte/updates/VERSION/components/core directory
        -   Check if function \_updateModuleRepository\_\[0-9\]+() exists and execute it
    -   Themes to component
        -   Find all themes
        -   initiate ThemeRepository (\Cx\Core\View\Model\Repository\ThemeRepository)
        -   If the current Theme doesn't have a component.yml file convert the theme
            -   Check if an info.xml file is available to use for the new component.yml file
            -   If no info.xml file is available create the information for component.yml file
            -   Add default dependencies (jquery 1.6.1)
            -   Remove the info.xml if it exists
    -   Module pages
        -   Loop over all components that must have a new template in order to work properly
        -   Load the new template from contrexx_module_repository
        -   Replace the page content with the new template
    -   Update css-definitions for components
        -   Find all themes
        -   Check which kind of theme the current one is (either print, pdf, mobile or app)
        -   Load /update/updates/VERSION/data/modules.css
        -   Search for component-separator in the modules.css file
        -   Search for component name in the component separator
        -   Load the new css-definitions and the dependencies of the component if they exist
        -   Write new css definitions into new modules_VERSION.css file in the current theme
        -   Create the link tag for the index.html file
        -   Search the {STYLE_FILE} placeholder in the index.html file
        -   Add link-tag before the {STYLE_FILE} placeholder and save the index.html file
        -   Copy images for social login to the current theme
    -   Navigation files
        -   Find all themes
        -   add {LEVELS_FULL} at the beginning of all non-empty subnavbars(2\|3).html files
        -   add {LEVELS_BRANCH} at the beginning of all navbars(2\|3).html files which have template-block 'navigation' and don't have template-block 'level-1'
    -   Create .htaccess file
        -   Determine which server-software we are running on, either apache or iis
        -   IIS Webserver
            -   Load /update/lib/PEAR/File/HtAccess.php
            -   Instantiate HtAccess with ASCMS_DOCUMENT_ROOT/web.config
            -   Add contrexx rules from /update/updates/VERSION/data/iis_htaccess.tpl
        -   Apache Webserver
            -   Load /update/lib/FRAMEWORK/FWHtAccess.class.php
            -   Instantiate FWHtAccess with ASCMS_DOCUMENT_ROOT and ASCMS_PATH_OFFSET
            -   Load /.htaccess with FWHtAccess-class
            -   Load /update/updates/VERSION/data/apache_htaccess.tpl
            -   Check if a .htaccess file already exists
            -   Check for rewrite rules which determine access to folders in the existing .htaccess and remove them from the new .htaccess
            -   Replace %PATH_ROOT_OFFSET% with ASCMS_PATH_OFFSET (or / if ASMCS_PATH_OFFSET is empty)
            -   Set section of the new .htacccess file to the rules defined in the new .htaccess file
            -   Remove section 'core_modules\_\_alias' and 'core\_\_language' from the .htaccess file
            -   Write the .htaccess file
    -   cadmin/index.php file
        -   Check if /cadmin/index.php exists
        -   Check if the /cadmin/index.php file is customized and move it into the customizings folder
        -   Delete the /cadmin/index.php file
    -   Update $\_CONFIG\['coreCmsVersion'\] to the new version
    -   License
        -   Update license and execute versioncheck.php file
        -   Update contrexx_settings contrexx version to the new version
        -   Write the /config/settings.php file
5.  Update for Contrexx 3 or newer
    -   Component repository
        -   Truncate existing contrexx_module_repository table
        -   Migrate contrexx_module_repository to the new table structure
        -   Search for repository\_(\[0-9\]+).php files in updte/updates/VERSION/components/core directory
        -   Check if function \_updateModuleRepository\_\[0-9\]+() exists and execute it
        -   Update contrexx_log_entry table set the object_class to the new fully qualified class name of entity page
        -   Migrate contrexx_content_page table to the new structure
    -   Themes to component
        -   Find all themes
        -   initiate ThemeRepository (\Cx\Core\View\Model\Repository\ThemeRepository)
        -   If the current Theme doesn't have a component.yml file convert the theme
            -   Check if an info.xml file is available to use for the new component.yml file
            -   If no info.xml file is available create the information for component.yml file
            -   Add default dependencies (jquery 1.6.1)
            -   Remove the info.xml if it exists
    -   Module pages
        -   Loop over all components that must have a new template in order to work properly
        -   Load the new template from contrexx_module_repository
        -   Replace the page content with the new template
    -   Update for 3.0.0 rc1 or 3.0.0 rc2
        -   Determine the current installed version, one of the following: rc1, rc2, 3.0.0, 3.0.0.1, 3.0.1, 3.0.2, 3.0.3, 3.0.4, 3.1.0, 3.1.1
        -   Rc1 update filesharing
            -   Insert German and English mail templates into contrexx_module_filesharing_mail_template table
        -   Database table changes from rc1 to rc2
        -   Add rc1 filesharing changes, if filesharing didn't have any mail templates
        -   Database table changes from rc2 to 3.0.0
        -   Database table changes from 3.0.0 to 3.0.0.1
        -   Database table changes from 3.0.0.1 to 3.0.1
        -   Database table changes from 3.0.1 to 3.0.2
        -   Database table changes from 3.0.2 to 3.0.3
        -   Database table changes from 3.0.3 to 3.0.4
        -   Database table changes from 3.0.4 to 3.1.0
        -   Database table changes from 3.1.0 to 3.1.1
        -   Database table changes from 3.1.1 to 3.1.2
        -   Combine database table changes:
            -   rc1 to 3.1.2
            -   rc2 to 3.1.2
            -   3.0.0 to 3.1.2
            -   3.0.0.1 to 3.1.2
            -   3.0.1 to 3.1.2
            -   3.0.2 to 3.1.2
            -   3.0.3 to 3.1.2
            -   3.0.4 to 3.1.2
            -   3.1.0 to 3.1.2
        -   determine which database changes shall be executed later on
        -   For versions older than 3.1.0
            -   Install CRM and FrontendEditing
                -   Migrate contrexx_core_mail_template table to the new structure
                -   Migrate mail templates to the new structure
                -   Create new mail templates: 'crm_user_account_created', 'crm_task_assigned', 'crm_notify_staff_on_contact_added'
        -   Execute the afore mentioned database table changes
        -   For versions older than 3.1.0
            -   Add entries for CRM and FrontendEditing to contrexx_modules-table
        -   Add primary keys to contrexx_access_group_dynamic_ids and contrexx_access_group_static_ids
        -   Truncate contrexx_module_repository-table
        -   Fill contrexx_module_repository-table with the data from the data-dump-file
        -   Add missing 'remove page' log entry
            -   Get the current Version of the pages which no longer exist and don't have a log entry with the action remove
            -   Insert a new log entry with the action remove, the current date and time and the next version
        -   For version rc1
            -   Load all fallback-pages
            -   Update their assigned module, cmd and set the updated at to the current date and time.
        -   For Versions equal or higher than rc1 and lower 3.0.3
            -   Decode urls in the newsletter
            -   Set the sort_order of the contrexx_module_shop_currencies-table to 0 where sort_order is null
            -   Migrate the contrexx_module_shop_currencies-table to the new table-structure
            -   Migrate the contrexx_module_shop_payment_processor to the new table-structure
        -   For versions older than 3.1.0
            -   Load all contact-form-pages
            -   Set their content to the source-code of the contact form. Set updated at to the current date and time
        -   For versions between 3.0.0 and 3.0.3
            -   Replace shopProductRow1 tempalte block in discounts page with shopProductRow template block
            -   Add product options after {SHOP_PRODUCT_DESCRIPTION} in product-listing and discounts pages
            -   Add {SHOP_AGB} and {SHOP_CANCELLATION_TERMS_CHECKED} placeholders to shop payment page
            -   Place {NEWS_COUNT_COMMENTS} placeholder inside news_comments_count template block in news and news details pages
        -   Migrate content of access signup to the content of version 3.0.1
        -   Update constraints for contrexx_content_node and contrexx_content_page
        -   Add confirm_delete template block to filesharing page
        -   For versions older than 3.1.0 with empty or missing contrexx_module_news_categories_catid-table
            -   Migrate contrexx_modules_news_categories to the new table-strucutre
            -   Add root node to the contrexx_modules_news_categories-table
            -   Count the existing categories
            -   Set the right_id of the root node to twice the amount of existing categories
            -   Update the categories with the nested set information
            -   Migrate or create contrexx_module_news_categories_locks and contrexx_module_news_categories_catid tables
            -   Insert into contrexx_module_news_categories_catid the id of the root node
        -   For versions older than 3.1.0
            -   Change contrexx_access_user_profile interests and signature to TEXT
            -   Add access_id 8 to groups which have access_id 7
            -   Call the updatescript for the calendar
            -   Update backend areas
        -   For versions older than 3.1.1
            -   Migrate cmd for calendar content pages: eventlist =&gt; list, event =&gt; detail, sign =&gt; register
        -   For versions older than 3.1.0
            -   Migrate contrexx_module_contact_form_field to the new table structure
            -   Set all fields which are currently set to file to multi_file
            -   Add access_id 176 to groups which have access_id 172
            -   Add access_id 4 to groups which have access_id 13 or 161
            -   Add access_id 170 to groups which have access_id 163
        -   For versions older than 3.1.0.2
            -   Add access_id 143 to groups which have access_id 142
        -   Check if /cadmin/index.php-file exists and if it is customized
        -   If it is customized, copy it to /customizings/cadmin/
        -   Remove /cadmin/index.php
        -   For versions older than 3.1.1
            -   Set news_use_teaser_text to 1 in contrexx_module_news_settings
        -   For versions 3.1.1 and 3.1.2
            -   Replace images/modules/media/\_base.gif with core_modules/media/View/Media/\_base.gif in media1, media2, media3 and media4 pages
        -   For versions older than 3.2.0
            -   Load all invitation_email_template and email_template from contrexx_module_calendar_event
            -   Store them each in an array
            -   json_encode these arrays and update the contrexx_module_calendar_event-table with the json-array
            -   Update the crm
        -   Fix the tree
        -   Update coreCmsVersion, coreCmsCodeName, coreCmsReleaseDate, coreCmsName and coreCmsStatus in contrexx_settigns with the new values
        -   Define ASCMS_INSTANCE_PATH and ASMCS_INSTANCE_OFFSET if they are not defined
        -   Write the new settings-file
    -   Create .htaccess file
        -   Determine which server-software we are running on, either apache or iis
        -   IIS Webserver
            -   Load /update/lib/PEAR/File/HtAccess.php
            -   Instantiate HtAccess with ASCMS_DOCUMENT_ROOT/web.config
            -   Add contrexx rules from /update/updates/VERSION/data/iis_htaccess.tpl
        -   Apache Webserver
            -   Load /update/lib/FRAMEWORK/FWHtAccess.class.php
            -   Instantiate FWHtAccess with ASCMS_DOCUMENT_ROOT and ASCMS_PATH_OFFSET
            -   Load /.htaccess with FWHtAccess-class
            -   Load /update/updates/VERSION/data/apache_htaccess.tpl
            -   Check if a .htaccess file already exists
            -   Check for rewrite rules which determine access to folders in the existing .htaccess and remove them from the new .htaccess
            -   Replace %PATH_ROOT_OFFSET% with ASCMS_PATH_OFFSET (or / if ASMCS_PATH_OFFSET is empty)
            -   Set section of the new .htacccess file to the rules defined in the new .htaccess file
            -   Remove section 'core_modules\_\_alias' and 'core\_\_language' from the .htaccess file
            -   Write the .htaccess file
    -   Write new configuration.php file
        -   Prepare the new configuration template and set database and ftp connection information
        -   Replace current content with the new one
    -   License
        -   Update license and execute versioncheck.php file
        -   Update contrexx_settings contrexx version to the new version
        -   Write the /config/settings.php file

