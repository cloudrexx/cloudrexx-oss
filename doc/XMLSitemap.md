Die Klasse XMLSitemap erstellt/aktualisiert die XML Sitemap der Website. Erläuterung von XML Sitemaps unter [sitemaps.org](https://www.sitemaps.org) **Diese Klasse steht ab der Version 2.0 Service Pack 2 zur Verfügung.*

## Usage

``` php
// create/update XML Sitemaps
\Cx\Core\PageTree\XmlSitemapPageTree::write();
```

### Version 2

``` php
if (include_once ASCMS_CORE_PATH.'/XMLSitemap.class.php') {
// XML Sitemaps erstellen/aktualisieren
XMLSitemap::write();
}
```

Der Aufruf der statischen Methode *XMLSitemap::write()* erzeugt das Sitemap in der Datei *sitemap.xml* im Hauptverzeichnis der Contrexx Installation.

### Spezialfall: Virtuelle Sprachverzeichnisse

Beim Einsatz von virtuellen Sprachverzeichnisse (/de, /en, usw.) wird anstatt der einzelnen *sitemap.xml* Datei für jede Sprache eine eigene Sitemap erstellt. Diese werden dann wie folgt benannt: *sitemap_%SPRACH_CODE%.xml*

Bei den Sprachen Deutsch (/de) und Englisch (/en) würden die Sitemaps dann also folglich *sitemap_de.xml* und *sitemap_en.xml* benannt sein.

Damit die Sitemaps nun noch elegant über */de/sitemap.xml* und */en/sitemap.xml* aufgerufen werden können, muss der Webserver noch entsprechend konfiguriert werden. Beim Einsatz eines Apache Webservers kann dies durch die Verwendung des Moduls [Mod_Rewrite](https://httpd.apache.org/docs/2.0/mod/mod_rewrite.html) geschehen. Dazu müssen einfach die folgenden Direktiven hinzugefügt werden (üblicherweise in einer [.htaccess](https://httpd.apache.org/docs/2.0/howto/htaccess.html) Datei im Hauptverzeichnis der Contrexx Installation):

``` php
# Request language specific sitemap.xml
RewriteCond %{REQUEST_URI} ^/(de|en|fr|it|da|ru|zh)/sitemap.xml$
RewriteRule ^(de|en|fr|it|da|ru|zh)/.*$ /sitemap_$1.xml [L,NC]
```
