The DataSource component allows unified access to different kinds of data. It is used by [DataAccess](RESTful API.md) which provides security and output modules to access data as a RESTful API.

## Structure

A DataSource consists of an identifier and a type. Together they identify the underlying source to get data from. Additionally some options can be set. A DataSource basically provides the following methods:

-   get(array $elementId = array(), array $filter = array(), array $order = array(), int $limit = 0, int $offset = 0, array $fieldList = array())
-   add(array $data)
-   update(array $elementId, array $data)
-   remove(array $elementId)

Therefore all CRUD-operations are possible through fully implemented DataSources. Additionally, if supported by the DataSource type, it can filter, sort and limit.

An element is identified by a set of key/value pairs. This for example allows using composite keys for database sources.

### Filter

Filter is a two dimensional array as follows: `array(<field> => array(<operation> => <value>))`

-   &lt;field&gt; is the DataSource field to apply the filter to.
-   &lt;operation&gt; is one of the operations listed in the following table. Not all DataSources support all operations.
-   &lt;value&gt; is the value the operation compares with the field.

If multiple filters are applied, only elements that match all filters are returned.

| Operation | Name                  | Description                                                                | Supported DataSource types |
|-----------|-----------------------|----------------------------------------------------------------------------|----------------------------|
| `eq`      | Equals                | Field must be equal to &lt;value&gt;                                       | All                        |
| `lt`      | Lower than            | Field value needs to be lower than &lt;value&gt;                           | DoctrineRepository         |
| `gt`      | Greater than          | Field value needs to be greater than &lt;value&gt;                         | DoctrineRepository         |
| `lte`     | Lower than or equal   | Field value needs to be lower than or equal to &lt;value&gt;               | DoctrineRepository         |
| `gte`     | Greater than or equal | Field value needs to be greater than or equal to &lt;value&gt;             | DoctrineRepository         |
| `in`      | In                    | Field value needs to be one of the comma separated values in &lt;value&gt; | DoctrineRepository         |
| `not`     | Not                   | Field value must not be equal to &lt;value&gt;                             | DoctrineRepository         |

## DataSource types

The following types are available. Some only provide limited support for some features:

### DoctrineRepository

Allows full CRUD access to Doctrine repositories.

### LegacyDatabaseRepository

Allows read-only access to non-Doctrine (/legacy) database tables.

### JsonDataSource

Allows access to JsonData adapters. Not implemented yet.

### YmlRepository

Allows full CRUD access to YamlRepositories. Not implemented yet.

### MediaSource

Allows full CRUD access to MediaSources. This is only partially implemented: get, add, remove and update work. Chunked upload does not yet work. Parallel upload do work yet. This is currently BETA.

For more info about what this can be used for, see [MediaSource](MediaSource.md).

## Options

Options are a PHP serialized array.

### Recursion

By default no associated entities are added to the result.

If the flag "recurse" is set to "true", all relations of type "many to many" and "\* to one" are followed and added to the result.

Additionally the options "forcedRecursions" and "skippedRecursions" can be used to follow "one to many" relations or to skip individual relations. Relations need to be specified as a chain of relation field names split by a colon.

!!! example

    To allow recursions on the entity `\Cx\Core\User\Model\Entity\UserAttributeValue` but not show user's groups you need to specify the following:

    ``` php
    array(
        'recurse' => true,
        'skippedRecursions' => array('user.groups')
    );

    // this leads to the following serialized value to put in the database field <code>options</code>
    'a:2:{s:7:"recurse";b:1;s:17:"skippedRecursions";a:1:{i:0;s:11:"user.groups";}}';
    ```

The following table shows the current behavior if recursion is turned on or off without any skipped or forced recursions:

| Relation | Non-recursive read | Non-recursive write                    | Recursive read | Recursive write  |
|----------|--------------------|----------------------------------------|----------------|------------------|
| 1:1      | skipped            | set existing entry if owning side only | followed       | not yet possible |
| 1:n      | skipped            | ignored                                | skipped        | not yet possible |
| n:1      | skipped            | set existing entry only                | followed       | not yet possible |
| m:n      | skipped            | set existing entries only              | followed       | not yet possible |


**Limitation:** Recursion is currently only suppored by the *DataSource* type *DoctrineRepository*.

## Date and time format handling

Compound formats are returned as [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601). Date without time are returned in the form "YYYY-MM-DD", time without date is returned in the form "HH:MM:SS".

Currently the dates are returned in the system's default user timezone.
