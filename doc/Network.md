Cloudrexx provides the following interfaces to manage network resources:

## HTTP Interface  

The PEAR package [\HTTP_Request2](https://pear.php.net/manual/en/package.http.http-request2.php) is integrated into Cloudrexx which provides the following functionality according to the official documentation:

`Supports POST requests with data and file uploads, basic and digest authentication, cookies, managing cookies across requests, proxies, gzip and deflate encodings, redirects, monitoring the request progress with Observers...`

Use as follows:

``` php
$request = new \HTTP_Request2($url);
$response = $request->send();
```

## JSON over HTTP Interface  

The *JSON over HTTP Interface* is an extension to the *HTTP Interface (HTTP_Request2)* that simplifies the handling of remote JSON resources.

Fetch JSON data from URL:

``` php
$jsonData = new \Cx\Core\Json\JsonData();
$json = $jsonData->getJson($url);
```

## DNS Interface  

See [DNS querying ability of `Net` component](Net.md#dns-query).

### Legacy

!!! warning
    You should use the [DNS querying ability of `Net` component](Net.md#dns-query).

The integrated PEAR library [Net_DNS2_Resolver](https://netdns2.com/documentation/) provides an interface to a domain name system.

Use as follows:

``` php
$nameServer = \Cx\Core\Setting\Controller\Setting::getValue(
    'dnsServer',
    'Config'
);
try {
    $resolver = new \Net_DNS2_Resolver(array(
        'nameservers' => array($nameServer),
    ));
    $response = $resolver->query($domain, 'A');
    foreach($result->answer as $record) {
        \DBG::dump($record):
    }
} catch(\Net_DNS2_Exception $e) {
    \DBG::log($e->getMessage());
}
```

