As of Contrexx 5.0, there's a [Component](Component Definition.md) of type core_module called "SysLog". This Component allows to log component specific log messages.

## Usage

To add a new log entry, just execute the following code:

``` php
$cx->getEvents()->triggerEvent('SysLog/Add', array(
   'severity' => 'INFO',
   'message' => 'my log message',
   'data' => 'additional debugging data',
));
```

Instead of `INFO` you may use `WARNING` or `FATAL`. All log messages should be written in the scope of the component adding the log entry.