The `MailTemplate` component provides mailing capabilities to Cloudrexx.

!!! info

    All mail handling in Cloudrexx should be done using `MailTemplate`.

## Features

-   [Sending e-mails](#sending-e-mails)
-   [Sender domain validation](#sender-domain-validation)
    -   [Live input validation](#live-input-validation)
    -   [Manual validation](#manually-check-validation)
-   [DKIM signing](#dkim-signing)
-   [E-mail templating](#e-mail-templates) having features such as:
    -   Management GUI
    -   Localization support
    -   Support for static attachments and dynamically generated PDF documents by using [PDF](PDF.md) component)
    -   Multipart support (plaintext/html)
-   [Block and placeholder support](#placeholder-support)

## Sending e-mails

Either send e-mails from scratch using [`\Cx\Core\MailTemplate\Model\Entity\Mail`](https://dev.cloudrexx.com/phpdoc/classes/Cx-Core-MailTemplate-Model-Entity-Mail.html):
!!! example

    ```php
    $mail = new \Cx\Core\MailTemplate\Model\Entity\Mail();
    $mail->setFrom($senderEmail, $senderName);
    $mail->addAddress($toEmailAddress);
    $mail->Subject = $subject;
    $mail->Body = $htmlBody;
    $mail->AltBody = $plaintextBody;
    $mail->send();
    ```

or by using the [e-mail templating](#e-mail-templates) capabilities either with (set `key`) or without a template:

!!! example
    
    ``` php
    $options = [
        'to'      => 'info@example.org',
        'subject' => 'Hello World',
        'message' => 'This is a plaintext message',
    ];
    \Cx\Core\MailTemplate\Controller\MailTemplate::send($options);
    ```
    
    The full list of `$options` that can be passed to `send()` is as follows:

    | Key            | Description                                                                                                                                                         |
    |----------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------|
    | `key`          | The key of any mail template to be used                                                                                                                             |
    | `section`      | The module to initialize for (mandatory when key is set)                                                                                                            |
    | `sender`       | The sender name                                                                                                                                                     |
    | `from`         | The sender e-mail address                                                                                                                                           |
    | `to`           | The recipient e-mail address(es), comma separated                                                                                                                   |
    | `reply`        | The reply-to e-mail address                                                                                                                                         |
    | `cc`           | The carbon copy e-mail address(es), comma separated                                                                                                                 |
    | `bcc`          | The blind carbon copy e-mail address(es), comma separated                                                                                                           |
    | `subject`      | The message subject                                                                                                                                                 |
    | `message`      | The plain text message body                                                                                                                                         |
    | `message_html` | The HTML message body                                                                                                                                               |
    | `html`         | If this evaluates to `true`, turns on HTML mode                                                                                                                     |
    | `attachments`  | An array of file paths to attach. The array keys may be used for the paths, and the values for the name.<br>If the keys are numeric, the values are regarded as paths. |
    | `inline`       | An array of inline (image) file paths to attach. If this is used, HTML mode (`html`) is switched on automatically.                                                  |
    | `search`       | The array of patterns to be replaced by...                                                                                                                          |
    | `replace`      | The array of replacements for the patterns                                                                                                                          |
    | `substitution` | A more complex structure for replacing placeholders and/or complete blocks, conditionally or repeatedly.                                                            |

    If `key` has been set, it tries to load the e-mail template identified by `key` (see e-mail templates), otherwise a blank email is initialized. The generated mail will be sent to the recipients defined by `to`, `cc` and `bcc`. If case of successful e-mail dispatching (to the configured SMTP server), the method `send()` returns `true`, otherwise `false`.

## Sender domain validation
`MailTemplate` does ensure that only authorized mails are sent. Depending on the use case it ensures that the sending domain has a valid SPF-record and/or DKIM- and DMARC-Records.
Actual validation is implemented by [`NetManager`](https://dev.cloudrexx.com/NetManager/#sender-domain-verification).
This behavior can be adjusted to a specific use case using the following helpers:

-   [`\Cx\Core\MailTemplate\Model\Entity\Mail::requiresFullSenderAuthentication()`](https://dev.cloudrexx.com/phpdoc/classes/Cx-Core-MailTemplate-Model-Entity-Mail.html#method_requiresFullSenderAuthentication)
-   [`\Cx\Core\MailTemplate\Model\Entity\Mail::ignoreSenderValidation()`](https://dev.cloudrexx.com/phpdoc/classes/Cx-Core-MailTemplate-Model-Entity-Mail.html#method_ignoreSenderValidation)

!!! example "Example: Require SPF, DKIM and DMARC authentication"

    ```php
    $mail = new \Cx\Core\MailTemplate\Model\Entity\Mail();
    $mail->requiresFullSenderAuthentication();
    $mail->setFrom($senderEmail, $senderName);
    $mail->addAddress($toEmailAddress);
    $mail->Subject = $subject;
    $mail->Body = $htmlBody;
    $mail->AltBody = $plaintextBody;
    // send process will fail if domain in $senderEmail has invalid SPF or
    // DMARC record or DKIM signing fails
    if (!$mail->send()) {
        \Message::error($mail->ErrorInfo);
    }
    ```

!!! example "Example: Ignore sender domain validation"

    ```php
    $mail = new \Cx\Core\MailTemplate\Model\Entity\Mail();
    $mail->ignoreSenderValidation();
    $mail->setFrom($senderEmail, $senderName);
    $mail->addAddress($toEmailAddress);
    $mail->Subject = $subject;
    $mail->Body = $htmlBody;
    $mail->AltBody = $plaintextBody;
    // mail will be sent even if SPF, DKIM and DMARC are invalid
    $mail->send();
    ```

### Live input validation
Add the CSS class `senderAddress` to a HTML-`input`-element to add a live sender domain validation to it. If strict[^1] validation is required, do add the attribute `data-sender-validation="strict"`. Further if the validation shall be made against a specific SMTP-server, do add the attribute `data-smtp-server-id="<id>"`.
[^1]: _strict_ validation means that SPF, DKIM and DMARC must be valid. With _non-strict_ validation only one of SPF and DKIM must be valid. DMARC is not required in _non-strict_ validation. _non-strict_ validation will be removed in the future and _strict_ will become the sole validation method.

!!! example "Regular sender address validation"
    
    ```html
    <input type="text" class="senderAddress" />
    ```

!!! example "Strict sender address validation"
    
    ```html
    <input type="text" class="senderAddress" data-sender-validation="strict" />
    ```

!!! example "Regular sender address validation against specific SMTP-server"
    
    ```html
    <input type="text" class="senderAddress" data-smtp-server-id="<id>" />
    ```
    Where `<id>` must be replaced by the ID of the SMTP-server to validate against.

### Manually check validation
To check if there are any sender domains in use that are not properly authenticated, the helper [`\Cx\Core\MailTemlate\Controller\ComponentController::hasIncompliantSenderDomains()`](https://dev.cloudrexx.com/phpdoc/classes/Cx-Core-MailTemplate-Controller-ComponentController.html#method_hasIncompliantSenderDomains) can be used.

!!! example
    
    ```php
    if ($this->getComponent('MailTemplate')->hasIncompliantSenderDomains()) {
        \Message::warning('Incompliant sender domains are in use!');
    }
    ```

!!! note
    
    Sender domains being in use are collected by an [event of the `NetManager`](NetManager.md#implementation).

## DKIM signing
If DKIM has been properly set up for a sender domain, then `MailTemplate` automatically signs the email using DKIM.

!!! note
    
    When [`\Cx\Core\MailTemplate\Model\Entity\Mail::requiresFullSenderAuthentication()`](https://dev.cloudrexx.com/phpdoc/classes/Cx-Core-MailTemplate-Model-Entity-Mail.html#method_requiresFullSenderAuthentication) is set or when sending over an external SMTP-server, but DKIM singing fails (due to a misconfiguration), then the send process will be aborted.

## E-Mail templates

E-mail templates provide the ability for custom tailored e-mail notifications that can be managed by a user over a GUI.

### Management

!!! note

    The default SystemComponentBackendController automatically ads a section for e-mail template management in the backend in case there are any templates by the viewing component present. So manual integration is therefore rarely required.

The management of e-mail templates can easily be integrated into a backend section as follows:

``` php
$htmlCode = \Cx\Core\MailTemplate\Controller\MailTemplate::overview(
    $this->getComponent()->getName(),
    'config'
)->get();
```

To integrate the management GUI directly into the view of the [Setting](Core Setting.md) configuration dialogue, do see separate section.

### Sending

Sending an e-mail template is as easy as sending a non-template e-mail, with the exception that a `key` must be specified that does identify the e-mail template to be sent:

``` php
$options = [
    'key'     => 'order_confirmation',
    'section' => 'Shop',
];
\Cx\Core\MailTemplate\Controller\MailTemplate::send($options);
```

Any previously defined option by the e-mail template will be overwritten if it gets passed to `send()` as `$options`.

## Placeholder support

A `MailTemplate` e-mail (both a template and non-template e-mail) does support template blocks and placeholders in the following form:

``` html
Placeholders (case insensitive):
[place_holder], [PLACEHOLDER] or [PLACEHOLDER2]
  
Conditional or repeated blocks:
[[BLOCK] ... [PLACEHOLDER] ... [BLOCK]]
```

!!! note

    The format for placeholders and blocks differs from the common format used by the rest of Cloudrexx.

When sending an e-mail, the option `substitution` can then be used to pass the data that shall be used to parse the placeholders and blocks within an email. It is important to note, that not only the body (`message`) of an email does support placeholders and blocks, but all `$options`. Meaning that `substitution` will be applied to all set `$options` of an e-mail template.

Placeholders and blocks that are empty or not defined by `substitution` will be removed from the e-mail before it will be sent. If the latter is not intended, then instead of `substitution`, you could use `search` together with `replace` which will do a static search & replace on all `$options` of an e-mail template.

Examples  

The following examples will use the following template:

``` html
[[MY_BLOCK] ... [MY_PLACEHOLDER] ... [MY_BLOCK]]
```

<table>

<tbody><tr>
<th><code>$options['substitution']</code>
</th>
<th>Output
</th>
<th>Notes
</th></tr>
<tr>
<td><div class="mw-highlight mw-content-ltr" dir="ltr"><pre><span></span><span class="p">[</span>
    <span class="s1">'my_block'</span> <span class="o">=&gt;</span> <span class="p">[</span>
        <span class="mi">0</span> <span class="o">=&gt;</span> <span class="p">[</span>
            <span class="s1">'MY_PLACEHOLDER'</span> <span class="o">=&gt;</span> <span class="s1">'Hello World'</span>
        <span class="p">],</span>
    <span class="p">],</span>
<span class="p">]</span>
</pre></div>
</td>
<td>
<pre>... Hello World ...
</pre>
</td>
<td>If <code>my_block</code> or <code>my_placeholder</code> is missing in <code>substitution</code>, then the whole block will not be parsed, but removed from the e-mail.
</td></tr>
<tr>
<td><div class="mw-highlight mw-content-ltr" dir="ltr"><pre><span></span><span class="p">[</span>
    <span class="s1">'my_block'</span> <span class="o">=&gt;</span> <span class="p">[</span>
        <span class="mi">0</span> <span class="o">=&gt;</span> <span class="p">[</span>
            <span class="s1">'MY_PLACEHOLDER'</span> <span class="o">=&gt;</span> <span class="s1">'One'</span><span class="p">,</span>
        <span class="p">],</span>
        <span class="mi">1</span> <span class="o">=&gt;</span> <span class="p">[</span>
            <span class="s1">'MY_PLACEHOLDER'</span> <span class="o">=&gt;</span> <span class="s1">'Two'</span><span class="p">,</span>
        <span class="p">],</span>
    <span class="p">],</span>
<span class="p">]</span>
</pre></div>
</td>
<td>
<pre>... One ...
... Two ...
</pre>
</td><td></td></tr></tbody></table>

!!! warning

    The additional array level between block and placeholders (or other blocks) is necessary - even for blocks without repetition. Therefore the following is **wrong and will not work**:

    ``` php
    $options['substitution'] = [
        'my_block' => [
            'MY_PLACEHOLDER' => 'Hello World'
        ],
    ];
    ```

## SMTP Relay

`MailTemplate` will use the default SMTP server (as set in **Administration &gt; Basic Configuration &gt; E-mail Server**) for relying mail.

!!! note

    When using a `cx env` environment the SMTP server can be set through the environment variable [`CLX_SMTP_MTA_HOST`](https://bitbucket.org/cloudrexx/cloudrexx-docker-files/src/master/).
