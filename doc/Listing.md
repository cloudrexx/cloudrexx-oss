This is the technical specification for the core module named "Listing". This component will be part of Contrexx 3.1.

## Idea

Listing provides an easy to use way to perform paging, sorting and filtering. Listing is optimized to work with [Doctrine](Doctrine.md), but should be able to handle any sort of data that can be paged, sorted and filtered from the view.

## Class Diagram

![Core Module Listing](img/Listing.png)

## Usage

Listing controller can be initialized in two ways. Either provide a doctrine entity class name or a callback function for the parameter $entities.

### Providing entity class name

``` php
$lister = new \Cx\Core_Modules\Listing\Controller\ListingController('Cx\Core\ContentManager\Model\Entity\Page');
$table = new \BackendTable($lister->getData());
return $table->toHtml();
```

### Providing callback function

The callback function returns an instance of doctrine query (\Doctrine\ORM\Query) or an instance of \Cx\Core_Modules\Listing\DataSet. If a dataset is returned, you will have to make use of $offset, $count, $criteria and $order.

#### Returning doctrine query

``` php
$lister = new \Cx\Core_Modules\Listing\Controller\ListingController(function(&$offset, &$count, &$criteria, &$order) {
    return \Env::get('em')->createQuery('SELECT p FROM Cx\Core\ContentManager\Model\Entity\Page p');
});
$table = new \BackendTable($lister->getData());
return $table->toHtml();
```

#### Handle own data

``` php
$lister = new \Cx\Core_Modules\Listing\Controller\ListingController(function(&$offset, &$count, &$criteria, &$order) {
    $query = \Env::get('em')->createQuery('SELECT p FROM Cx\Core\ContentManager\Model\Entity\Page p');
    $query->setFirstResult($offset);
    $query->setMaxResults($count);
    // TODO: handle criteria and order
    $result = $query->getResult();
    if (!$result) {
        throw new \Exception('Empty result');
    }
    return new \Cx\Core_Modules\Listing\Model\DataSet($result);
});
$table = new \BackendTable($lister->getData());
return $table->toHtml();
```

## Ideas and Todos

-   Add possibility to initialize ListingController with an instance of DataSet
-   Extend DataSet with methods like getLimit($count, $offset) or filter($crit)
-   If callback function returns a DataSet, apply limit and offset to it if necessary (should never happen, but you never now)

