Contrexx uses [CKeditor](https://www.ckeditor.net/) with version 4 as WYSIWYG editor.

## since Contrexx version 3.1

### Types

There are multiple editor configurations Contrexx provides:

| Type     | Description                                                                               | Available functions               |
|----------|-------------------------------------------------------------------------------------------|-----------------------------------|
| full     | The full functionality editor for complex contents. (With possibility to edit sourcecode) | Sourcecode<br>New site<br>Templates<br>Cut / Copy / Paste<br>Spell check<br>Undo / Redo<br>Find / Replace<br>Mark all<br />Reset formatting<br />Bold / Italic / Underlined / Struck through<br />Superscript / Subscript<br />Numbered- und unnumbered list<br />More / less indention<br />Quote<br />Link / Anker<br />Image<br />Flash<br />Table<br />Horizontal line<br />Special characters<br />Text formatting<br />Text- und background colors<br />Show blocks (Divs as dashed lines and so on)<br />Fullscreen mode<br />Div assistent |
| fullpage | The full functionality editor. Used to edit a whole HTML structure including &lt;html&gt;,&lt;head&gt; and &lt;body&gt; tags. | Same functionalities like "full". |
| small    | The full functionality editor. The editor isn't as height as the full editor.             | Same functionalities like "full". |
| bbcode   | Used to let user insert data.                                                             | Sourcecode <br />Undo / Redo<br />Find / Replace<br />Mark all<br />Reset formatting<br />Bold / Italic / Underlined<br />Link<br />Special characters |


### Editors in use

#### Backend

| Module          | Function              | Field                                 | Editor type |
|-----------------|-----------------------|---------------------------------------|-------------|
| Content manager | Enter / Edit a page   | Content                               | Full        |
| Core            | Edit mail template    | HTML                                  | Fullpage    |
| Core            | SettingDB             | Configuration                         | Small       |
| User management | Edit mail template    | HTML                                  | Fullpage    |
| Contact         | Edit a form           | Text up above the form, Feedback text | Small       |
| Contact         | Edit a form           | Mail template                         | Fullpage    |
| News            | Edit teaser template  | HTML                                  | Full        |
| News            | Add / Edit entry      | News text                             | Full        |
| Blog            | Add / Edit blog entry | Content                               | Full        |
| Blog            | Edit comment          | Text                                  | BBCode      |
| Calendar        | Edit event            | Description                           | Full        |
| Calendar        | Edit mail             | HTML                                  | Full        |
| Checkout        | Edit mail             | HTML                                  | Full        |
| CRM             | Edit deal             | Payment information / Description     | Small       |
| CRM             | Edit note             | Content                               | Small       |
| CRM             | Edit task             | Task description                      | Small       |
| Data            | Add / Edit entry      | Content                               | Full        |
| Docsystem       | Add / Edit entry      | Content                               | Full        |
| Egov            | Edit product          | Description / Message                 | Full        |
| Jobs            | Add / Edit Job        | Description                           | Full        |
| Knowledge       | Edit article          | Content                               | Full        |
| Mediadir        | Entering form data    | WYSIWYG                               | Small       |
| Newsletter      | Edit mail             | Content                               | Full        |
| Newsletter      | Edit mail template    | HTML                                  | FullPage    |
| Partners        | Add / Edit partner    | Description                           | Small       |
| Popup           | Edit                  | Content                               | Full        |
| Shop            | Edit product          | Short description                     | Small       |
| Shop            | Edit product          | Long description                      | Full        |
| U2U             | Settings              | Private message                       | Full        |

#### Frontend

| Module | Function             | Field     | Editor type |
|--------|----------------------|-----------|-------------|
| News   | Add entry            | News text | BBCode      |
| Blog   | Add comment          | Text      | BBCode      |
| Forum  | Add thread / comment | Text      | BBCode      |
| U2U    | Private message      | Message   | BBCode      |

### Plugins

CKEditor has been built using the [CKEditor Builder](https://ckeditor.com/builder)

| Plugin                                  | Information                                | Integration |
|-----------------------------------------|--------------------------------------------|-------------|
| Basic Styles                            | <https://ckeditor.com/addon/basicstyles>    | Build-in    |
| BBCode output format                    | <https://ckeditor.com/addon/bbcode>         | ExtraPlugin |
| Blockquote                              | <https://ckeditor.com/addon/blockquote>     | Build-in    |
| Clipboard                               | <https://ckeditor.com/addon/clipboard>      | Build-in    |
| CodeMirror (Source) Syntax Highlighting | <https://ckeditor.com/addon/codemirror>     | ExtraPlugin |
| Color Button                            | <https://ckeditor.com/addon/colorbutton>    | Build-in    |
| Content Templates                       | <https://ckeditor.com/addon/templates>      | Build-in    |
| Context Menu                            | <https://ckeditor.com/addon/contextmenu>    | Build-in    |
| Dialog                                  | <https://ckeditor.com/addon/dialog>         | Build-in    |
| Dialog User Interface                   | <https://ckeditor.com/addon/dialogui>       | Build-in    |
| Div Container Manager                   | <https://ckeditor.com/addon/div>            | Build-in    |
| Editor Resize                           | <https://ckeditor.com/addon/resize>         | Build-in    |
| Editor Toolbar                          | <https://ckeditor.com/addon/toolbar>        | Build-in    |
| Elements Path                           | <https://ckeditor.com/addon/elementspath>   | Build-in    |
| Enter Key                               | <https://ckeditor.com/addon/enterkey>       | Build-in    |
| Escape HTML Entities                    | <https://ckeditor.com/addon/entities>       | Build-in    |
| Fake Objects                            | <https://ckeditor.com/addon/fakeobjects>    | Build-in    |
| File Browser                            | <https://ckeditor.com/addon/filebrowser>    | Build-in    |
| Find / Replace                          | <https://ckeditor.com/addon/find>           | Build-in    |
| Flash Dialog                            | <https://ckeditor.com/addon/flash>          | Build-in    |
| Floating Panel                          | <https://ckeditor.com/addon/floatpanel>     | Build-in    |
| Floating Space                          | <https://ckeditor.com/addon/floatingspace>  | Build-in    |
| Format                                  | <https://ckeditor.com/addon/format>         | Build-in    |
| Horizontal Rule                         | <https://ckeditor.com/addon/horizontalrule> | Build-in    |
| HTML Output Writer                      | <https://ckeditor.com/addon/htmlwriter>     | Build-in    |
| IFrame Dialog                           | <https://ckeditor.com/addon/iframe>         | Build-in    |
| IFrame Editing Area                     | <https://ckeditor.com/addon/wysiwygarea>    | Build-in    |
| Image                                   | <https://ckeditor.com/addon/image>          | Build-in    |
| Indent / Outdent                        | <https://ckeditor.com/addon/indent>         | Build-in    |
| Indent List                             | <https://ckeditor.com/addon/indentlist>     | Build-in    |
| Insert Smiley                           | <https://ckeditor.com/addon/smiley>         | Build-in    |
| Justify                                 | <https://ckeditor.com/addon/justify>        | Build-in    |
| Link                                    | <https://ckeditor.com/addon/link>           | Build-in    |
| List                                    | <https://ckeditor.com/addon/list>           | Build-in    |
| List Block                              | <https://ckeditor.com/addon/listblock>      | Build-in    |
| List Style                              | <https://ckeditor.com/addon/liststyle>      | Build-in    |
| Magic Line                              | <https://ckeditor.com/addon/magicline>      | Build-in    |
| Maximize                                | <https://ckeditor.com/addon/maximize>       | Build-in    |
| Menu                                    | <https://ckeditor.com/addon/menu>           | Build-in    |
| Menu Button                             | <https://ckeditor.com/addon/menubutton>     | Build-in    |
| New Page                                | <https://ckeditor.com/addon/newpage>        | Build-in    |
| Panel                                   | <https://ckeditor.com/addon/panel>          | Build-in    |
| Panel Button                            | <https://ckeditor.com/addon/panelbutton>    | Build-in    |
| Paste As Plain Text                     | <https://ckeditor.com/addon/pastetext>      | Build-in    |
| Paste from Word                         | <https://ckeditor.com/addon/pastefromword>  | Build-in    |
| Popup                                   | <https://ckeditor.com/addon/popup>          | Build-in    |
| Preview                                 | <https://ckeditor.com/addon/preview>        | Build-in    |
| Remove Format                           | <https://ckeditor.com/addon/removeformat>   | Build-in    |
| Rich Combo                              | <https://ckeditor.com/addon/richcombo>      | Build-in    |
| Select All                              | <https://ckeditor.com/addon/selectall>      | Build-in    |
| Show Blocks                             | <https://ckeditor.com/addon/showblocks>     | Build-in    |
| Show Table Borders                      | <https://ckeditor.com/addon/showborders>    | Build-in    |
| Source Editing Area                     | <https://ckeditor.com/addon/sourcearea>     | Build-in    |
| Special Characters                      | <https://ckeditor.com/addon/specialchar>    | Build-in    |
| SpellCheckAsYouType (SCAYT)             | <https://ckeditor.com/addon/scayt>          | Build-in    |
| Styles Combo                            | <https://ckeditor.com/addon/stylescombo>    | Build-in    |
| Tab Key Handling                        | <https://ckeditor.com/addon/tab>            | Build-in    |
| Table                                   | <https://ckeditor.com/addon/table>          | Build-in    |
| Table Resize                            | <https://ckeditor.com/addon/tableresize>    | Build-in    |
| Table Tools                             | <https://ckeditor.com/addon/tabletools>     | Build-in    |
| UI Button                               | <https://ckeditor.com/addon/button>         | Build-in    |
| Undo                                    | <https://ckeditor.com/addon/undo>           | Build-in    |
| WebSpellChecker                         | <https://ckeditor.com/addon/wsc>            | Build-in    |

### ExtraPlugins (not included within the package)

[BBCode Output Format](https://ckeditor.com/addon/bbcode) does allow to handle BBCode.

#### Howto install an extra plugin

Download the plugin into the folder /lib/ckeditor/plugins. After that you have to activate it in the file /core/Wysiwyg/Wysiwyg.class.php for the editor which should use this plugin.

``` php
private $types = array(
    'small' => array(
        'toolbar' => 'Small',
        'width' => '100%',
        'height' => 200,
        'fullPage' => 'false',
        'extraPlugins' => array(),
    ),`  
    'full' => array(
        'toolbar' => 'Full',
        'width' => '100%',
        'height' => 450,
        'fullPage' => 'false',
        'extraPlugins' => array(),
    ),
    'fullpage' => array(
        'toolbar' => 'Full',
        'width' => '100%',
        'height' => 450,
        'fullPage' => 'true',
        'extraPlugins' => array(),
   ),`  
    'bbcode' => array(
        'toolbar' => 'BBCode',
        'width' => '100%',
        'height' => 200,
        'fullPage' => 'false',
        'extraPlugins' => array('bbcode'),
    ),
);
```

An example does exist for the editor type *bbcode*. There the extra plugin 'bbcode' has been activated.

### \Cx\Core\Wysiwyg\Wysiwyg

The Wysiwyg class does facilitate the implementation of a CKeditor while developing modules.

``` php
public function __construct($name, $value = "", $type = "small", $langId = null, $extraPlugins = array())
```

$name (string) is the content of the HTML attribute "name".  
$value (string) is the content of the HTML attribute "value".  
$type (string) can take the values: bbcode, small, full. default is "small". (take a look at the types up above)  
$langId (integer) default is the constant FRONTEND_LANG_ID.  
$extraPlugins (array) there extra plugins can be deactivated. z.B. array('bbcode')

### Code example for WYSIWYG implementation

``` php
$objTemplate->setVariable(array(
    'MODULE_WYSIWYG_DESCRIPTION' => new \Cx\Core\Wysiwyg\Wysiwyg('description', contrexx_raw2xhtml($htmlCode), 'full'),
));
```

The inserted text can be evaluated with the following code:

``` php
$description = contrexx_input2raw($_POST['description']);
```

### Format (BBCode / HTML)

If the WYSIWYG editor is used with the type *bbcode*, the data will be BBCode formatted in the $_POST array. It is recommended to save the data as BBCode to the database.

Example code:

``` php
$datenbankValue = \Cx\Core\Wysiwyg\Wysiwyg::prepareBBCodeForDb($_POST['formField']);
```

For the output as HTML you only need to use the method prepareBBCodeForOutput():

``` php
$htmlCode = \Cx\Core\Wysiwyg\Wysiwyg::prepareBBCodeForOutput($datenbankValue);
```

### Toolbar configurator

The toolbar configurator allows the creation of individual WYSIWYG editor toolbars.

**Initiating a new Toolbar Configurator instance**

``` php
$toolbarConfigurator = new \Cx\Core\Wysiwyg\Controller\ToolbarController(\Cx\Core\Core\Controller\Cx::instanciate());
```

The parameter is mandatory and cannot be left empty.

**Using the toolbar configurator**

To add a toolbar configurator to the view, the toolbar controller provides the getToolbarConfiguratorTemplate.

``` php
$toolbarConfiguratorTemplate = $toolbarConfigurator->getToolbarConfiguratorTemplate('/core/Core/Wysiwyg', false);
```

The first parameter specifies the path to the toolbar configurator template and the second parameter wraps the toolbar configurator in a form tag

**Storing the toolbar**

The generated toolbar configurator template will send the disabled editor functions as a POST-parameter with the name *removedButtons*.  
The current implementation of the different toolbars does not support this syntax though. Therefore, the toolbar controller provides the method *getAsOldSyntax*.

By passing the removed buttons string and the type of toolbar the user customized into the method you will get the old syntax json encoded returned.

``` php
$toolbarFunctions = $toolbarConfigurator->getAsOldSyntax($_POST['removedButtons'], 'full');
```

The second parameter can be one of the following values: *small*, *full*, *bbcode*, *frontendEditingContent* or *frontendEditingTitle*. The returned value can then be stored in the core_wysiwyg_toolbar-table, which uses just the old syntax and the removed buttons as values.

**Loading the toolbar**

In the *ckeditor.config.js.php* file located under */core/Core/Wysiwyg/* are the old syntaxes for the different toolbars. Replace the old syntax with the following `call config.toolbar_full = <?php $wysiwyg->getFullToolbar() ?>;` The shown code is an example. The call may vary and if necessary an additional method can be added to the *\Cx\Core\Wysiwyg\Controller\ComponentController*.

**Additional method**

To enhance the user experience, the toolbar configurator provides a method to deselect functions while editing an existing toolbar.  
By calling *getRemovedButtons* you will receive a string containing each function that has been disabled.  
Be aware that the string is built based on the default and the user group configuration.

**Default implementation**

The toolbar configurator is currently available in the WYSIWYG-settings and in the user group management.  
For further instructions on how to use these configurators view this FAQ entry:

## till Contrexx version 3.1

An instance of an editor can be printed with the global function get_wysiwyg_editor().

### get_wysiwyg_editor

This function looks like this:

``` php
get_wysiwyg_editor($name, $value = `*`, $mode =`*`, $languageId = null, $absoluteURIs = false)
```

$name (string) contains the content of the HTML attribute "name".  
$value (string) contains the content of the HTML attribute "value".  
$langId (integer) the default is the constant FRONTEND_LANG_ID.  
$absoluteURIs (boolean) only used for the file browser. If set to TRUE, the images will be inserted with the absolute path (incl. domain and protocol)

