<span style="font-size: 48px; color: red">DRAFT</span>

Cloudrexx provides to create own indexers. Files can be indexed and searched. The index will be updated on upload, rename, delete and move.

## Examples

### Create a new Indexer

Create a new Indexer Class and extends the *\Cx\Core\MediaSource\Model\Entity\Indexer*. Overwrite the *getText()* method. Here you can convert the content of the files into text.

In this example, the Indexer only fetches the data, because it is a text indexer and the data is already in a text format.

``` php
<?php

namespace Cx\Modules\TxtIndexer\Model\Entity;

class TxtIndexer extends \Cx\Core\MediaSource\Model\Entity\Indexer
{

    /**
     * Get text from an indexed file
     *
     * @param $mediaSource \Cx\Core\MediaSource\Model\Entity\MediaSource
     * @param $filepath    string path to file
     *
     * @return string
     */
    protected function getText($filepath)
    {
        $data = '';
        try {
            $objFile = new \Cx\Lib\FileSystem\File($filepath);
            $data = $objFile->getData();
        } catch (\Cx\Lib\FileSystem\FileSystemException $e) {
            \DBG::msg($e->getMessage());
        }
        return $data;
    }
}
```

### Register Indexer

Register your indexer in the *ComponentController* in the *postInit* method of your module.

``` php
public function postInit(\Cx\Core\Core\Controller\Cx $cx) 
{
     // Create new instance of your Indexer 
     $txtindexer = new \Cx\Modules\TxtIndexer\Model\Entity\TxtIndexer();

     // Set all Extensions your Indexer can handle 
     $txtindexer->setExtensions(array('txt'));

     // Register Indexer
     $this->getComponent('MediaSource')->registerIndexer($txtindexer);
}
```

### Get Matches

Create a new MediaSource instance and call the method *getFileSystemMatches($searchterm, $path)* to get the indexed search results. Then the index search results can be appended to the normal search results.

``` php
$mediaType = new MediaSource($name, $humanName, array($directoryPath, $directoryWebPath), $accessIds);
  
$result = new \Cx\Core_Modules\Listing\Model\Entity\DataSet(
    $mediaType->getFileSystemMatches($searchterm, $path)
);

$search->appendResult($result);
```
