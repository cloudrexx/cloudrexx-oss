## What it is

-   Modal search dialog
-   Searches user accounts with ajax and autocompletion
-   You can select an existing user account or enter a custom user name
-   You have the possibility to clear the selected user
-   Available since version **3.0.1**

## Usage

### Server-side (PHP)

\FWUser::getUserLiveSearch([$arrOptions]);

    \FWUser::getUserLiveSearch();

with options:

    \FWUser::getUserLiveSearch(array(
        'minLength' => 3,
        'canCancel' => true,
        'canClear'  => true,
        'limit'     => 15,
    ));

#### Options

| Name         | Type    | Default                                   | Description                                                                                                                                                                                                                                                                                                                                                              |
|--------------|---------|-------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| minLength    | Integer | 3                                         | The minimum length of characters to start the user search.                                                                                                                                                                                                                                                                                                               |
| canCancel    | Boolean | false                                     | If true, a cancel button is displayed in the dialog.                                                                                                                                                                                                                                                                                                                     |
| canClear     | Boolean | false                                     | If true, there is the possibility to clear the user. The user id will be 0 and the user name empty.                                                                                                                                                                                                                                                                      |
| limit        | Integer | 0                                         | Limits the result to the specified amount of users. 0 means no limit.                                                                                                                                                                                                                                                                                                    |
| resultFormat | String  | %title%                                   | Changes the content of the found list items. The following placeholders can be used: See table below.                                                                                                                                                                                                                                                             |
| searchFields | String  | company,firstname,lastname,username,email | Allows to specify which fields should be searched. Field list is the same as the placeholder list of "resultFormat".                                                                                                                                                                                                                                                     |
| searchAnd    | Boolean | false                                     | If set to "true" this searches in a more complex way: If fields A and B are searched for the terms "C" and "D" the search will only return users matching both terms (either C in A and D in B or vice versa).<br>This currently only works if no more search terms than search fields are specified. In addition, this is currently limit to a maximum of 2 search fields. |

| Placeholder | Value                                                                                                                                      |
|-------------|--------------------------------------------------------------------------------------------------------------------------------------------|
| %id%        | ID of user                                                                                                                                 |
| %title%     | Parsed user title (combination of firstname, lastname, email and username).<br>This is not to be confused with the profile attribute 'title'. |
| %username%  | Username of user                                                                                                                           |
| %email%     | E-mail of user                                                                                                                             |
| %firstname% | Firstname of user                                                                                                                          |
| %lastname%  | Lastname of user                                                                                                                           |
| %company%   | Company of user                                                                                                                            |
| %address%   | Address of user                                                                                                                            |
| %city%      | City of user                                                                                                                               |
| %zip%       | ZIP of user                                                                                                                                |

### Client-side (HTML)

If you want to link the input explicitly to an active user account:

    <input class="live-search-user-id" name="..." value="..." />

If you want to have the possibility to enter a custom user name additionally:

    <input class="live-search-user-id" name="..." value="..." />
    <input class="live-search-user-name" name="..." value="..." />

#### CSS classes

The following classes are necessary for the user live search and must be used in combination with an input tag.

| Class                 | Description                                                                                |
|-----------------------|--------------------------------------------------------------------------------------------|
| live-search-user-id   | Set this class on an input tag which is used for the user id of the selected user account. |
| live-search-user-name | Set this class on an input tag which is used for the entered/custom user name.             |

#### Events

You can attach handlers for the following events.

| Use case    | Triggered event | Scope            | Usage (attach handler to event)                                   | Version     |
|-------------|-----------------|------------------|-------------------------------------------------------------------|-------------|
| Select user | userSelected    | user/live-search | cx.bind("userSelected", function(objUser) {}, "user/live-search") | since 3.1.0 |
| Clear user  | userCleared     | user/live-search | cx.bind("userCleared", function(objUser) {}, "user/live-search")  | since 3.1.0 |

If you use the "User Live Search" plugin several times then you can add an id to the input tag with the class "live-search-user-id". Then you can extend the scope "user/live-search" with the assigned id like "user/live-search/example-id". In this case you can add an event handler for the element with the id "example-id".

Code example to add a handler to the event "userSelected" explicitly for the element with the id "example-id":

    <script type="text/javascript">
    cx.bind("userSelected", function(objUser) { ...some javascript code... }, "user/live-search/example-id");
    </script>

    <input id="example-id" class="live-search-user-id" name="..." value="..." />

##### Parameter objUser (Type: Object)

| Property     | Description |
|--------------|-------------|
| objUser.id   | User id     |
| objUser.name | User name   |

!!! example

    ``` js
    cx.bind("userSelected", function(objUser) {
        alert("This is the user id: " + objUser.id);
        alert("this is the user name: " + objUser.name);
    }, "user/live-search");
    ```
