The [API documentation](https://api.contrexx.com/) of Contrexx is automatically generated based on its PHP source code using the documentation tool [phpDocumentor 2](https://phpdoc.org/). To ensure that the generation of the documentation works flawlessly, the whole source of Contrexx has to be documented properly. This is done using so-called *DocBlocks*.

Refer to the [documentation of *phpDocumentor 2*](https://docs.phpdoc.org/guide/index.html) on how to properly document the PHP source code.

See also Wikipedia article about [PHPDoc](https://en.wikipedia.org/wiki/PHPDoc).

## Guidelines

1.  First, Study the introduction [What is a DocBlock?](https://docs.phpdoc.org/guide/getting-started/what-is-a-docblock.html)
2.  Then use at least the mandatory Tags as listed below to properly document the code
3.  Add useful (see [Best practices for writing code comments](https://stackoverflow.blog/2021/07/05/best-practices-for-writing-code-comments/)) inline code documentation

!!! example

    Example *file-level-*, *exception-* & *class-DocBlock* for a class of component `Config`:

    ``` php
    <?php
    declare(strict_types=1);

    /**
     * Cloudrexx
     *
     * @link      https://www.cloudrexx.com
     * @copyright Cloudrexx AG
     *
     * This file is part of Cloudrexx.
     *
     * Cloudrexx is free software: you can redistribute it and/or modify
     * it under the terms of the GNU Affero General Public License as
     * published by the Free Software Foundation, version 3 of the License.
     *
     * Cloudrexx is distributed in the hope that it will be useful,
     * but WITHOUT ANY WARRANTY; without even the implied warranty of
     * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
     * GNU Affero General Public License for more details.
     *
     * "Cloudrexx" is a registered trademark of Cloudrexx AG.
     * The licensing of the program under the AGPLv3 does not imply a
     * trademark license. Therefore any rights, title and interest in
     * our trademarks remain entirely with us.
     */

    namespace Cx\Core\Config\Model\Entity;

    /**
     * @copyright   Cloudrexx AG
     * @author      John Doe <john.doe@example.org>
     * @package     cloudrexx
     * @subpackage  core_config
     */
    class ExampleException extends \Exception {}

    /**
     * @copyright   Cloudrexx AG
     * @author      John Doe <john.doe@example.org>
     * @package     cloudrexx
     * @subpackage  core_config
     */
    class Example {}
    ```

### Mandatory tags

File-level / namespace / class / interface / trait  

-   [`@copyright`](https://docs.phpdoc.org/guide/references/phpdoc/tags/copyright.html)
-   [`@author`](https://docs.phpdoc.org/guide/references/phpdoc/tags/author.html)
-   [`@package`](https://docs.phpdoc.org/guide/references/phpdoc/tags/package.html)
-   [`@subpackage`](https://docs.phpdoc.org/guide/references/phpdoc/tags/subpackage.html)

property / constant  

-   [`@var`](https://docs.phpdoc.org/guide/references/phpdoc/tags/var.html)

require(_once) / include(_once)  

-   [`@ignore`](https://docs.phpdoc.org/guide/references/phpdoc/tags/ignore.html) (as all classes should get loaded by the [ClassLoader](ClassLoader.md) anyway)
      
!!! note

    Add comment about the specific reason why a require(_once) / include(_once) has been used*

function (including methods)  

-   [`@param`](https://docs.phpdoc.org/guide/references/phpdoc/tags/param.html) (if any)
-   [`@global`](https://docs.phpdoc.org/guide/references/phpdoc/tags/global.html) (if any)
-   [`@return`](https://docs.phpdoc.org/guide/references/phpdoc/tags/return.html) (if not void)
-   [`@throws`](https://docs.phpdoc.org/guide/references/phpdoc/tags/throws.html) (if so)

### Recommended / useful tags

-   [`@todo`](https://docs.phpdoc.org/guide/references/phpdoc/tags/todo.html)
-   [`@deprecated`](https://docs.phpdoc.org/guide/references/phpdoc/tags/deprecated.html)
-   [`@see`](https://docs.phpdoc.org/guide/references/phpdoc/tags/see.html)
-   [`@uses`](https://docs.phpdoc.org/guide/references/phpdoc/tags/uses.html)
-   [`@since`](https://docs.phpdoc.org/guide/references/phpdoc/tags/since.html) If the structural element is public, then use this tag to document the cloudrexx release since this feature became available
-   [`@inheritDoc`](https://docs.phpdoc.org/guide/references/phpdoc/inline-tags/inheritdoc.html) See section #Inheritance below

all other tags are nice to have and should be set where useful (for example author of a function if not the same as the author of the class)

### Inheritance

phpDocumentor does automatically inherit all documentation of parent elements (see [Inheritance at phpDocumentor](https://docs.phpdoc.org/guide/guides/inheritance.html)). So in theory no additional documentation is required on inherited elements (classes, interfaces, methodes, etc.). However to indicate a local element as documented we shall do so by adding a DocBlock as follows:

``` php
/**
 * @inheritDoc
 */
```

However, if the local element has been modified and must therefore be documented, we do only have to document the changed parts of it ([`@param`](https://docs.phpdoc.org/guide/references/phpdoc/tags/param.html), [`@global`](https://docs.phpdoc.org/guide/references/phpdoc/tags/global.html), [`@return`](https://docs.phpdoc.org/guide/references/phpdoc/tags/return.html) and [`@throws`](https://docs.phpdoc.org/guide/references/phpdoc/tags/throws.html)) and if required the description itself. For the description, the special inline tag `{@inheritDoc}` can be used to import the description of the parents DocBlock. Example:

``` php
/**
 * This is the summary of the redefined method.
 *
 * Now follows the description of the overloaded element:
 * {@inheritDoc}
 *
 * @param string This argument has been added to the redefined method
 */
```

### Not used tags

-   [`@version`](https://docs.phpdoc.org/guide/references/phpdoc/tags/version.html)