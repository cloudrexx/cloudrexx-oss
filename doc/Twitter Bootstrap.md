Ab Version 3.2 wird Twitter Bootstrap ein fester Bestandteil von Contrexx sein.

## Versionen

Die installierten Twitter Bootstrap Versionen sind im Ordner /lib/javascript/twitter-bootstrap/ abgelegt:

-   2.3.2
-   3.0.0
-   3.0.1
-   3.0.2
-   3.0.3
-   3.1.0

## Verwendung

Sie können Twitter Bootstrap ohne eine Zeile Code im Template einbinden indem Sie unter **Administration &gt; Layout &gt; Designvorlagen** beim jeweiligen Design bei den "Verwendete Bibliotheken" Twitter Bootstrap aktivieren.

## Was folgt noch?

### Backend aufgebaut mit Twitter Bootstrap

Im Backend wird dann Bootstrap 3 verwendet (aktuellste Version momentan).  
Für das Backend ein spezielles Boostrap.css machen: <https://getbootstrap.com/customize/>  
Dieses wird als Customizing der original Version im /lib/bootstrap in /cadmin/lib/bootstrap o.ä. abgelegt.  
  
z.B deaktivieren. Glyphicons, Breadcrumps, Pager, Badges, Jumbotron, Thumbnails, Progress bars, Media items, Panels, Wells, JavaScript components (except Modals)

-   Tables
    -   .adminlist muss die Styles von .table-responsive (bootstrap) haben, mittels LESS Styles

``` css
.adminlist {
   .table-responsive;
}
```
[https://ruby.bvision.com/blog/please-stop-embedding-bootstrap-classes-in-your-html](https://ruby.bvision.com/blog/please-stop-embedding-bootstrap-classes-in-your-html) (ist auch empfohlen von Twitter)


-   Forms
    -   Formulare sollten einen neuen Aufbau haben. Hier könnte man sicherlich auch mit einer externen CSS-Datei (contrexx-bootstrap.css) die Labels und
        umändern, damit diese Responsive sind.
-   Paging
    -   Paging.class.php umändern, damit diese dem `<ul>` die Klasse „pagination“ gibt:
    
``` html
<ul class="pagination"> <li class="disabled"><a href="#">«</a></li> <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li> ... </ul>
```

-   Warnboxes
    -   Die Contrexx Warnungsmeldungen im css umändern, damit diese die Styles von .alert übernehmen, ähnlich wie bei Tables
-   Kategorien-Übersichten (v2)
    -   Könnte man so umsetzen: `<span class="badge pull-right">42</span>`
    -   erzeugt hinter dem Namen ein Badge mit der Anzahl Einträge (zum Beispiel)
-   jQuery-UI Dialog durch <https://getbootstrap.com/javascript/#modals> ersetzen (Responsives modales Fenster)  
    Auch im Frontend einsetzen für Gallery, falls Bootstrap aktiv ist (v2)
-   Navigationen
    -   Eventuell funktionert es schon nachdem das Grid angepasst ist. Ansonsten müsste man noch die Navigationen (Header und Subnavigation) anpassen.
-   Jede Seite muss mit dem Grid-System ausgestattet werden. (Aufwand 4+ Tage)
-   Jede fixe Breite muss aus dem base.css verschwinden.

**Die Idee und vor allem auch die Empfehlung von anderen Entwicklern im Internet ist, dass man abgesehen vom 12-Spalten-Gitter nicht die HTML-Struktur der Applikation anpasst sondern ein LESS-Stylesheet macht, welcher dann Bootstrap und den eigenen Stylesheet vereint.**

## Update von Twitter Bootstrap

Es gibt bei jeder Version von Twitter Bootstrap ein Migration-Guide, daher ist es nicht schwierig ein Template auf die neue Bootstrap Version anzupassen.  
<https://bootply.com/bootstrap-3-migration-guide> (inoffiziell, das wichtigste klar ersichtlich)  
<https://getbootstrap.com/getting-started/#migration> (offiziell, viele Infos, sicherlich hilfreich)

