## What it is

The Contrexx Javascript Framework aims to be the connection between PHP and Javascript.

It provides:

-   Variable exchange PHP -&gt; JS / JS -&gt; JS
    Use this instead of setting variables and initializing js variables via placeholder.
-   Helpers
    -   [UI](Javascript Framework UI.md)
        This should be enhanced to cover often used tasks (Shadowbox/Expose, Dialog boxes, Gallery Javascripts and so on).
    -   [I18n](Javascript Framework I18n.md)
        An approach to centralized i18n of javascript content.
-   Asynchronous loading of javascript and css files
    Speed up page loads by asynchronous ressource loading.

## Activation

The regular way to acitvate the Contrexx JavaScript Framework is to execute the following PHP code:
`\JS::activate('cx');`

!!! note

    Prior to version 3, you would call just JS::activate('cx') (without the use of the global namespace indicator **/**)

[draft] Additionally, it is also possible to activate the Contrexx JavaScript Framework without PHP. This can be done by including the jQuery library within the template or a content page:

    <script type="text/javascript" src="/lib/javascript/jquery/1.6.1/js/jquery.min.js"></script>

## cx.ready(function[, waitForQueue])

Execute something when cxjs (and thus jQuery) is loaded.
*waitForQueue* allows to wait for other functions previously scheduled before executing the function specified. This defaults to *false*.

## cx.variables

Set and get variables.

-   js 2 js
-   php 2 js

### scopes

You should always use the scope parameter. Use {component} or {component}/{subcomponent or function} (e.g. contentmanager or contentmanager/toggle). For an example, see core/contentmanager/ContentManager.class.php.

Set the variable `var` to `value` **(defaults to scope *contrexx*)**. `value` will be converted to it's JSON representation (this means you can safely use this function for complex types like arrays).

### getting

#### cx.variables.get(var,[scope])

Get the variable `var` in scope `scope`

### setting

#### Javascript

`cx.variables.set(var, value, [,scope])`
Set the variable `var` in scope `scope` to `value`.

#### PHP

`ContrexxJavascript::getInstance()-&gt;setVariable('myVar', 'test', 'test');`
Use this to pass data to javascript rather than template placeholders.

### default variables

Those variables are always available in scope *contrexx*.

###### path  
the pages' offset from the current domain's webroot including the virtuallanguage path. equivalent of *ASCMS_PATH_OFFSET.'/'.FWLanguage::getLanguageCodeById(FRONTEND_LANG_ID).'/* '.
i.e.: */contrexx/de/*

###### basePath  
the pages' offset from the current domain's webroot. equivalent of *ASCMS_PATH_OFFSET.'/* '.
i.e.: */contrexx/*

###### cadminPath  
the backend's offset from the current domain's webroot. equivalent of *ASCMS_PATH_OFFSET.ASCMS_BACKEND_PATH.'/* '.
i.e.: */contrexx/cadmin/*

###### mode  
**backend** or **frontend**
i.e.: *frontend*

###### language  
code as defined in contrexx_languages ('de', 'en', ...)
i.e.: *de*

###### frontendLocale (only in versions newer than 5.1.1)
code as defined in contrexx_languages ('de', 'en', ...)
i.e.: *de*

###### themeId (as of version 5)  
Current (if in frontend) or default theme ID
i.e.: *1*

#### Prior to version 3

###### cmsPath  
the pages' offset from the current domain's webroot. equivalent of *ASCMS_PATH_OFFSET*.

###### cadminPath  
the backend's offset from the current domain's webroot. equivalent of *ASCMS_BACKEND_PATH*.

###### mode  
'backend' or 'frontend'

###### language  
code as defined in contrexx_languages ('de', 'en', ...)

## cx.include(files, callback, lazy, initcall)

``` js
//load something
cx.include(['lib/some/dir/my.js', 'a/path/some.css'], function() {
    //here, the files above are loaded and ready for use.
    //further, everything passed to .include and .ready before was loaded/executed and is ready to use.
});
```

``` js
//load single file
cx.include('lib/some/dir/my.js', function() {
    //here, the file above is loaded and ready for use.
    //further, everything passed to .include and .ready before was loaded/executed and is ready to use.
});
```

``` js
//lazy-load: do not wait for files to be loaded
cx.include('lib/some/dir/my.js', function() {
    //here, only the file above is loaded and ready for use
}, true);
```

## cx.jQuery

This is the jQuery object.

## cx.tools.StatusMessage

**cx.tools.StatusMessage is deprected. Whenever possible, use [cx.ui.messages](Javascript Framework UI.md) instead.**

The StatusMessage object helps you to display a status message for any action in your component. This object is used in Contrexx in the ContentManager and FrontendEditing (e.g. to show when the page has been successfully saved). It can be used very simple:
`cx.jQuery.tools.StatusMessage.showMessage(message, cssClass, showTime, callbackAfterDestroy, options);`
defaultOptions:
``` js
{
   draggable: false,
   resizable: false,
   minWidth: 100,
   minHeight: 28,
   dialogClass: "cxDialog noTitle",
   position: ["center", "top"]
}
```

| Function                                                                | Description                                                                      | Parameters                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
|-------------------------------------------------------------------------|----------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| showMessage(message, cssClass, showTime, callbackAfterDestroy, options) | Adds a message to the stack and will be shown after currently displayed message. | <ul><li> message: *(string)* the message to display as status </li><li> cssClass: *(string)* the css class to add around the message (i.e. can be used for text color) </li><li> showTime: *(integer)* after which amount of seconds the message will disappear, if this parameter is null the message will not disappear </li><li> callbackAfterDestroy: *(function)* a function which is called after the dialogs have been removed </li><li> options: *(object)* overrides the default options </li></ul> |
| removeAllDialogs(onlyPermanentMessages)                                 | Hides all dialogs and deletes all messages from the stack.                       | <ul><li> onlyPermanentMessages: *(boolean)* true = remove only permanent messages which haven't got an amount of seconds, false = remove all messages </li></ul>

## cx.ajax()

CxJs provides a wrapper for AJAX requests to JsonAdapters:

`cx.ajax(adapter, method, options, locale);`

*adapter* is the name of the JsonAdapter to call  
*method* the adapters method name  
*options* are normal jQuery.ajax() options  
(This is not implemented yet!) *locale* the locale code (i.e. de)

cx.ajax() generates success and error messages for you. In order to use them, you should use the options *preSuccess* and *postSuccess* instead of *success* and *preError* and *postError* instead of *error*. These are triggered before/after the message is displayed. You may turn off this behavior by setting the option *showMessage* to *false*.

## Events

*(Contrexx 3)*

You can define own events for Contrexx components. To bind to such an event use to following scheme:

``` js
cx.bind("myEvent", function(myArgs) {
    // do something
}, "myScope");
```

The scope parameter is optional and defaults to "contrexx", see scopes for more information about using scopes.
To fire the event and trigger all binded callbacks use the following:

``` js
cx.trigger("myEvent", "myScope", myArgs);
```

This will call all callback functions registered for this event in the according scope. The scope and argument parameters are optional and default to "contrexx" and null.

## Thread safety

CxJs provides a method to call one or multiple function thread safely:

``` js
cx.callThreadSafe(callback, scope, sleep)
```

This method calls *callback* and delayes all calls to it if it is already running.
Parameters *scope* and *sleep* are optional. If you provide a scope you can lock multiple methods together. So if one is running, calls to all others in the same scope are delayed too. Sleep defines the timout between the tries in ms (default is 500).

``` js
cx.callThreadSafe(function() {
    myToBeThreadSafeFunction(with, some, parameters);
});
```

## I18n

For all i18n related data, an i18n mechanism is provided: [i18n](Javascript Framework I18n.md)

## Modules

-   [UI](Javascript Framework UI.md)

## Issues

Usage:

-   The {JAVASCRIPT} placeholder needs to be set

Code

-   Async loading bases on an immature jQuery plugin - should be changed to an alternative (one of yepnope.js / labjs / requirejs)
