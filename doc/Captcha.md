## Client Integration (Template/Content)

``` html
<form>
...
<label>[[TXT_MODULE_CAPTCHA]]</label>
[[MODULE_CAPTCHA_CODE]]
...
</form>
```

## Server Integration (PHP)

``` php
$this->objTemplate->setVariable(array(
    'TXT_MODULE_CAPTCHA'   => $_CORELANG['TXT_CORE_CAPTCHA'],
    'MODULE_CAPTCHA_CODE'  => \Cx\Core_Modules\Captcha\Controller\Captcha::getInstance()->getCode(),
));
```

## Validation

``` php
if (!\Cx\Core_Modules\Captcha\Controller\Captcha::getInstance()->check()) {
    throw new Exception('Invalid Captcha');
}
```

## Older Contrexx Versions (&lt;3.0)

Client-Side (Template/Content)  

``` html
<form>
...
<img src="[[CONTACT_CAPTCHA_URL]]" />
<input type="text" name="modulenameFormCaptcha"/>
...
</form>
```

Server-Side (PHP)  

``` php
include_once ASCMS_LIBRARY_PATH.'/spamprotection/captcha.class.php';
$objCaptcha = new Captcha();
$this->_objTpl->setVariable('CONTACT_CAPTCHA_URL', $objCaptcha->getUrl());
```

Validation  

``` php
if (!$objCaptcha->check($_POST['modulenameFormCaptcha'])) {
    throw new Exception($_ARRAYLANG['TXT_CORE_INVALID_CAPTCHA');

}
```

