!!! info

    Cloudrexx is a software as a service and does therefore not require any installation, but just a web browser (see [client requirements](#client-requirements)) to use it.

Cloudrexx runs on a classical LAMP stack. For convenience a [command line interface](CLI script.md) is provided to setup and operate a fully functional environment using [Docker containers](https://bitbucket.org/cloudrexx/cloudrexx-docker-files/). Therefore Cloudrexx should run on any modern unix-like operating system where Docker is available. See [local installation](Local installation.md) to start now.

## Server Requirements

For running a local environment, the following system requirements are set:

-   Bash 4
-   Docker Engine 1.14
-   Docker Compose 1.9

!!! warning

    Even though the [command line interface](CLI script.md) should also work on macOS and Windows, it's neither tested, nor officially supported.

!!! note

    See [legacy on-premise requirements](Versionen.md) for specific system requirements of the discontinued legacy on-premise releases.

## Client Requirements

To use and access the Cloudrexx backend, an up to date version of the following web browsers is required:

-   Chromium
-   Google Chrome
-   Mozilla Firefox
-   Microsoft Edge

!!! note

    Apple Safari might also work. However it's support is not fully tested. For best experience, usage of Mozilla Firefox or Google Chrome (Chromium) is recommended.
