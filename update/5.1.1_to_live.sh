#!/bin/bash
cx Setting add Config -group=featureFlags -engine=Yaml CLX4854_AllowDuplicateBlocks off 1 radio on:TXT_ACTIVATED,off:TXT_DEACTIVATED
cx Setting add Config -group=featureFlags -engine=Yaml CLX4862_RecursiveMediaDirParsing off 1 radio on:TXT_ACTIVATED,off:TXT_DEACTIVATED
cx Setting add Config -group=featureFlags -engine=Yaml CLX4188_IncludeIndexedFilesInGlobalSearch off 1 radio on:TXT_ACTIVATED,off:TXT_DEACTIVATED
cx Setting add Config -group=featureFlags -engine=Yaml CLX4881_UseLegacyPdfParsing off 1 radio on:TXT_ACTIVATED,off:TXT_DEACTIVATED
cx Setting add Config -group=featureFlags -engine=Yaml CLX5051_AlternatePagingTemplate off 1 radio on:TXT_ACTIVATED,off:TXT_DEACTIVATED
cx Setting add Config -group=featureFlags -engine=Yaml CLX2298_EnableThumbnailFormatCustomization off 1 radio on:TXT_ACTIVATED,off:TXT_DEACTIVATED
cx Setting add Config -group=featureFlags -engine=Yaml CLX5054_AllowPharArchives off 1 radio on:TXT_ACTIVATED,off:TXT_DEACTIVATED
cx Setting add Config -group=featureFlags -engine=Yaml CLX5054_LoadApplicationTemplateFromCurrentTheme on 1 radio on:TXT_ACTIVATED,off:TXT_DEACTIVATED
cx Setting add Config -group=featureFlags -engine=Yaml CLX3691_ShowReservedSeats off 1 radio on:TXT_ACTIVATED,off:TXT_DEACTIVATED
cx Setting add Config -group=featureFlags -engine=Yaml CLX4789_ListChildren off 1 radio on:TXT_ACTIVATED,off:TXT_DEACTIVATED
cx Setting add Config -group=featureFlags -engine=Yaml CLX4789_LinkMyselfAsCategoryOrLevel off 1 radio on:TXT_ACTIVATED,off:TXT_DEACTIVATED
cx Setting add Config -group=featureFlags -engine=Yaml CLX4447_IgnoreFieldTypeRestrictions off 1 radio on:TXT_ACTIVATED,off:TXT_DEACTIVATED
cx Setting add Config -group=featureFlags -engine=Yaml CLX4311_NeverIncludeLevelSlug off 1 radio on:TXT_ACTIVATED,off:TXT_DEACTIVATED
cx Setting add Config -group=featureFlags -engine=Yaml CLX4311_NeverIncludeCategorySlug off 1 radio on:TXT_ACTIVATED,off:TXT_DEACTIVATED
cx Setting add Config -group=featureFlags -engine=Yaml CLX4281_NoPageMetaDataOverride off 1 radio on:TXT_ACTIVATED,off:TXT_DEACTIVATED
cx Setting add Config -group=featureFlags -engine=Yaml CLX4281_NoPageMetaDataOverrideI24 off 1 radio on:TXT_ACTIVATED,off:TXT_DEACTIVATED
cx Setting add Config -group=featureFlags -engine=Yaml CLX5087_BlockHistory off 1 radio on:TXT_ACTIVATED,off:TXT_DEACTIVATED
cx Setting add Config -group=featureFlags -engine=Yaml CLX5111_ParseBlocksInWidgets off 1 radio on:TXT_ACTIVATED,off:TXT_DEACTIVATED
cx Setting add Config -group=featureFlags -engine=Yaml CLX5111_UserbasedNavigation off 1 radio on:TXT_ACTIVATED,off:TXT_DEACTIVATED
cx Setting add Config -group=core -engine=Yaml nonDbMode off 1 radio on:TXT_ACTIVATED,off:TXT_DEACTIVATED
cx Setting delete Config -engine=Yaml -group=cache cacheUserCacheMemcacheConfig
cx Setting add Config -group=security -engine=Yaml hstsIncludeSubDomains off 13 radio on:TXT_ACTIVATED,off:TXT_DEACTIVATED
cx Setting add Config -group=security -engine=Yaml hstsPreload off 14 radio on:TXT_ACTIVATED,off:TXT_DEACTIVATED
cx Setting add Config -group=security -engine=Yaml cspForScriptSrc off 15 radio on:TXT_ACTIVATED,off:TXT_DEACTIVATED
cx Setting alter Config -group=contactInformation -engine=Yaml coreAdminEmail text '{"type":"senderAddress"}'
cx Setting add NetManager -group=config -engine=Database trackedSenderDomains '' 0 text ''
cx Setting add NetManager -group=config -engine=Database ignoreDomainBlacklist off 1 radio on:TXT_ACTIVATED,off:TXT_DEACTIVATED
cx Setting add Config -group=security -engine=Yaml upgradeInsecureRequests off 16 radio on:TXT_ACTIVATED,off:TXT_DEACTIVATED
cx Setting add Net -group=env -engine=Database publicIpAddress '' 0 text ''
cx Setting set Config corePagingLimit $(value=$(cx Setting get Config corePagingLimit); int=${value//\'/}; echo $(( int < 1 ? 1 : ( int > 1000 ? 1000 : int ) )))
cx Setting add Config -group=security -engine=Yaml frontendDenyIframing $(oldValue=$(cx Setting get Config -group=security frontendAllowIframing); if [[ "$oldValue" =~ on ]]; then echo "off"; else echo "on";fi;) 11 radio on:TXT_ACTIVATED,off:TXT_DEACTIVATED
cx Setting delete Config -group=security -engine=Yaml frontendAllowIframing
cx Setting alter Config -group=component trustedReverseProxyCount text '' otherConfigurations 13
