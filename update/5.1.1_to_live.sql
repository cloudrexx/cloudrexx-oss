INSERT INTO `contrexx_core_rewrite_rule`(`regular_expression`, `continue_on_match`, `rewrite_status_code`, `order_no`) VALUES ('#(//[^/]+)/(?:([^/]+)/)?(?:index\\.php)?\\?section=Newsletter&(n=[0-9]+&l=[0-9]+&[rm]=[0-9]+)#\\1/api/Newsletter/Redirect?locale=\\2&\\3#i', 0, '301', (SELECT IFNULL(MAX(`list`.`order_no`) + 1, 0) FROM `contrexx_core_rewrite_rule` AS `list`));
INSERT INTO `contrexx_core_rewrite_rule`(`regular_expression`, `continue_on_match`, `rewrite_status_code`, `order_no`) VALUES ('#(//[^/]+)/[^/]+/?(?:index\\.php)?\\?section=Newsletter&cmd=displayInBrowser&(standalone=true&)?#\\1/api/Newsletter/View?#i', 0, '301', (SELECT IFNULL(MAX(`list`.`order_no`) + 1, 0) FROM `contrexx_core_rewrite_rule` AS `list`));
UPDATE `contrexx_core_country` SET `active` = 1 WHERE `id` > 0 AND `id` = (SELECT `value` FROM `contrexx_core_setting` WHERE `section` LIKE 'Shop' AND `name` ='country_id');
INSERT INTO `contrexx_core_setting` (`section`, `name`, `group`, `type`, `value`, `values`, `ord`) SELECT 'Shop', 'available_countries', 'delivery', 'dropdown_multiselect', GROUP_CONCAT(id SEPARATOR ','), '{src:\\Cx\\Core\\Country\\Controller\\Country::getNameArray()}', '1' FROM `contrexx_core_country` WHERE `active` = 1;
UPDATE contrexx_core_country SET `active` = 1;
DROP TABLE `contrexx_lib_country`;
DELETE FROM `contrexx_core_text` WHERE `key` = 'core_country_name' AND `section` = 'core';
ALTER TABLE contrexx_core_country DROP `ord`, DROP `active`;
ALTER TABLE contrexx_module_crm_customer_types DROP hourly_rate;
ALTER TABLE contrexx_module_crm_currency DROP hourly_rate;
DELETE FROM contrexx_module_crm_settings WHERE `setname`='allow_pm';
#ALTER TABLE `contrexx_core_module_sync_id_mapping` ADD UNIQUE `origin` (`foreign_host`, `entity_type`, `foreign_id`);
UPDATE `contrexx_core_setting` SET `ord` = 18 WHERE `section` = 'Shop' AND `name` = 'payrexx_api_secret';
INSERT INTO `contrexx_core_setting` (`section`, `name`, `group`, `type`, `value`, `values`, `ord`) VALUES ('Shop','payrexx_domain_name','config','text','','',17);
INSERT INTO `contrexx_core_setting` (`section`, `name`, `group`, `type`, `value`, `values`, `ord`) VALUES ('Shop','account_send_login','config','checkbox','1','',2);
UPDATE `contrexx_module_mediadir_inputfields` SET `type` = 1  WHERE `type` = 21;
DELETE FROM `contrexx_module_mediadir_inputfield_types` WHERE `id` = 21;
DELETE FROM `contrexx_backend_areas` WHERE `module_id` = 32;
DELETE FROM `contrexx_component` WHERE `name` = 'NetTools';
DELETE FROM `contrexx_modules` WHERE `name` = 'NetTools';
INSERT INTO `contrexx_module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (NULL, '5', 'seriesDouranceType', 'TXT_CALENDAR_DEFAULT_DOURANCE_TYPE', '0', '', '5', 'TXT_CALENDAR_SERIES_PATTERN_NO_ENDDATE,TXT_CALENDAR_SERIES_PATTERN_END_AFTER_X_OCCURRENCES,TXT_CALENDAR_SERIES_PATTERN_END_BY_DATE', '', '11');
INSERT INTO `contrexx_module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (NULL, '5', 'seriesDouranceEvents', 'TXT_CALENDAR_DEFAULT_OCCURRENCES_COUNT', '5', 'TXT_CALENDAR_DEFAULT_OCCURRENCES_COUNT_TOOLTIP', '1', '', '', '12');
INSERT INTO `contrexx_module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (NULL, '5', 'backendSortOrder', 'TXT_CALENDAR_BACKEND_SORT_ORDER', '0', '', '5', 'TXT_CALENDAR_ASC,TXT_CALENDAR_DESC', '', '13');
UPDATE contrexx_core_module_cron_job SET `expression` = '0 2 1 * *' WHERE `command` = 'Access removeUselessProfileImages';
UPDATE contrexx_core_module_cron_job SET `expression` = '0 3 * * 0' WHERE `command` = 'Model optimize';
INSERT INTO `contrexx_module_mediadir_settings` (`id`, `name`, `value`) VALUES (54,'settingsShowCategories','0');
INSERT INTO `contrexx_backend_areas` (`area_id`, `parent_area_id`, `type`, `scope`, `area_name`, `is_active`, `uri`, `target`, `module_id`, `order_id`, `access_id`) VALUES(236, 3, "navigation", "backend", "TXT_CORE_MODULE_PRIVACY", 1, "index.php?cmd=Privacy", "_self", 46, 2, 207);
CREATE TABLE `contrexx_core_module_privacy_cookie` (id INT AUTO_INCREMENT NOT NULL, active TINYINT(1) NOT NULL, ord INT NOT NULL, name VARCHAR(255) NOT NULL, identifier VARCHAR(255) NOT NULL, component VARCHAR(255) DEFAULT NULL, category_id INT NOT NULL, description VARCHAR(255) NOT NULL, cookie_names LONGTEXT DEFAULT NULL COMMENT '(DC2Type:array)', host VARCHAR(255) DEFAULT NULL, privacy_statement_url VARCHAR(255) DEFAULT NULL, UNIQUE INDEX identifier_idx (identifier), PRIMARY KEY(id)) ENGINE = InnoDB;
INSERT INTO `contrexx_core_module_privacy_cookie` (`id`, `active`, `ord`, `name`, `identifier`, `component`, `category_id`, `description`, `cookie_names`, `host`, `privacy_statement_url`) VALUES (1, '1', '1', 'Besuchersitzung', 'session', 'Session', '1', 'Login, Warenkorb. Wird nur gesetzt, wenn ein entsprechendes Feature verwendet wird.', 'a:2:{i:0;s:9:\"PHPSESSID\";i:1;s:6:\"clxsid\";}', NULL, NULL);
INSERT INTO `contrexx_core_module_privacy_cookie` (`id`, `active`, `ord`, `name`, `identifier`, `component`, `category_id`, `description`, `cookie_names`, `host`, `privacy_statement_url`) VALUES (2, '1', '2', 'Frontend Sprache', 'langId', 'Locale', '1', 'Die gewählte Sprache im Frontend.', 'a:1:{i:0;s:6:\"langId\";}', NULL, NULL);
INSERT INTO `contrexx_core_module_privacy_cookie` (`id`, `active`, `ord`, `name`, `identifier`, `component`, `category_id`, `description`, `cookie_names`, `host`, `privacy_statement_url`) VALUES (3, '1', '3', 'Datenschutzhinweis', 'clxCookieNote', 'Locale', '1', 'Einstellungen des Datenschutzhinweises merken.', 'a:1:{i:0;s:13:\"ClxCookieNote\";}', NULL, NULL);
CREATE TABLE `contrexx_translations` (`id` int AUTO_INCREMENT, `locale` varchar(8) NOT NULL, `object_class` varchar(255) NOT NULL, `field` varchar(32) NOT NULL, `foreign_key` varchar(64) NOT NULL, `content` text DEFAULT NULL, PRIMARY KEY (`id`), UNIQUE KEY `lookup_unique_idx` (`locale`,`object_class`,`foreign_key`,`field`), INDEX `content_lookup_idx` (`content`(255), `object_class`, `field`), INDEX translations_lookup_idx (`locale`,`object_class`,`foreign_key`)) ENGINE=InnoDB;
INSERT IGNORE INTO `contrexx_translations` (`locale`, `object_class`, `field`, `foreign_key`, `content`) SELECT DISTINCT `iso_1` AS `locale`, "Cx\\Core_Modules\\Privacy\\Model\\Entity\\Cookie", "name", 1, "Besuchersitzung" FROM `contrexx_core_locale_locale` WHERE `source_language` = "de";
INSERT IGNORE INTO `contrexx_translations` (`locale`, `object_class`, `field`, `foreign_key`, `content`) SELECT DISTINCT `iso_1` AS `locale`, "Cx\\Core_Modules\\Privacy\\Model\\Entity\\Cookie", "description", 1, "Login, Warenkorb. Wird nur gesetzt, wenn ein entsprechendes Feature verwendet wird." FROM `contrexx_core_locale_locale` WHERE `source_language` = "de";
INSERT IGNORE INTO `contrexx_translations` (`locale`, `object_class`, `field`, `foreign_key`, `content`) SELECT DISTINCT `iso_1` AS `locale`, "Cx\\Core_Modules\\Privacy\\Model\\Entity\\Cookie", "name", 1, "Visitor session" FROM `contrexx_core_locale_locale` WHERE `source_language` = "en";
INSERT IGNORE INTO `contrexx_translations` (`locale`, `object_class`, `field`, `foreign_key`, `content`) SELECT DISTINCT `iso_1` AS `locale`, "Cx\\Core_Modules\\Privacy\\Model\\Entity\\Cookie", "description", 1, "Login, cart. This cookie is only set if a feature requiring it is used." FROM `contrexx_core_locale_locale` WHERE `source_language` = "en";
INSERT IGNORE INTO `contrexx_translations` (`locale`, `object_class`, `field`, `foreign_key`, `content`) SELECT DISTINCT `iso_1` AS `locale`, "Cx\\Core_Modules\\Privacy\\Model\\Entity\\Cookie", "name", 2, "Frontend Sprache" FROM `contrexx_core_locale_locale` WHERE `source_language` = "de";
INSERT IGNORE INTO `contrexx_translations` (`locale`, `object_class`, `field`, `foreign_key`, `content`) SELECT DISTINCT `iso_1` AS `locale`, "Cx\\Core_Modules\\Privacy\\Model\\Entity\\Cookie", "description", 2, "Die gewählte Sprache im Frontend." FROM `contrexx_core_locale_locale` WHERE `source_language` = "de";
INSERT IGNORE INTO `contrexx_translations` (`locale`, `object_class`, `field`, `foreign_key`, `content`) SELECT DISTINCT `iso_1` AS `locale`, "Cx\\Core_Modules\\Privacy\\Model\\Entity\\Cookie", "name", 2, "Frontend language" FROM `contrexx_core_locale_locale` WHERE `source_language` = "en";
INSERT IGNORE INTO `contrexx_translations` (`locale`, `object_class`, `field`, `foreign_key`, `content`) SELECT DISTINCT `iso_1` AS `locale`, "Cx\\Core_Modules\\Privacy\\Model\\Entity\\Cookie", "description", 2, "The selected frontentend language." FROM `contrexx_core_locale_locale` WHERE `source_language` = "en";
INSERT IGNORE INTO `contrexx_translations` (`locale`, `object_class`, `field`, `foreign_key`, `content`) SELECT DISTINCT `iso_1` AS `locale`, "Cx\\Core_Modules\\Privacy\\Model\\Entity\\Cookie", "name", 3, "Datenschutzhinweis" FROM `contrexx_core_locale_locale` WHERE `source_language` = "de";
INSERT IGNORE INTO `contrexx_translations` (`locale`, `object_class`, `field`, `foreign_key`, `content`) SELECT DISTINCT `iso_1` AS `locale`, "Cx\\Core_Modules\\Privacy\\Model\\Entity\\Cookie", "description", 3, "Einstellungen des Datenschutzhinweises merken." FROM `contrexx_core_locale_locale` WHERE `source_language` = "de";
INSERT IGNORE INTO `contrexx_translations` (`locale`, `object_class`, `field`, `foreign_key`, `content`) SELECT DISTINCT `iso_1` AS `locale`, "Cx\\Core_Modules\\Privacy\\Model\\Entity\\Cookie", "name", 3, "Privacy note" FROM `contrexx_core_locale_locale` WHERE `source_language` = "en";
INSERT IGNORE INTO `contrexx_translations` (`locale`, `object_class`, `field`, `foreign_key`, `content`) SELECT DISTINCT `iso_1` AS `locale`, "Cx\\Core_Modules\\Privacy\\Model\\Entity\\Cookie", "description", 3, "Save settings of the privacy note." FROM `contrexx_core_locale_locale` WHERE `source_language` = "en";
INSERT INTO `contrexx_core_setting` (`section`, `name`, `group`, `type`, `value`, `values`, `ord`) VALUES ('Shop','image_parse_behaviour','config','dropdown','filling','0:filling,1:fixed',0);
INSERT INTO `contrexx_core_setting` (`section`, `name`, `group`, `type`, `value`, `values`, `ord`) VALUES ('Shop','image_use_no_pictrue','config','checkbox','1','',0);
ALTER TABLE `contrexx_module_crm_contacts` DROP `email_delivery`;

ALTER TABLE `contrexx_content_page` ADD FULLTEXT KEY `fulltext` (`title`, `content`);
ALTER TABLE `contrexx_module_calendar_category_name` ADD PRIMARY KEY (`cat_id`, `lang_id`);
ALTER TABLE `contrexx_module_calendar_event_field` ADD PRIMARY KEY (`event_id`, `lang_id`);
ALTER TABLE `contrexx_module_mediadir_inputfield_types` DROP `dynamic`;
ALTER TABLE `contrexx_sessions` ADD `init_token` varchar(12) NOT NULL AFTER `client_hash`;

CREATE TABLE `contrexx_core_module_sync` ( `id` int(11) NOT NULL AUTO_INCREMENT, `data_access_id` int(11) DEFAULT NULL, `to_uri` varchar(255) NOT NULL, `api_key` varchar(32) NOT NULL, `active` tinyint(1) NOT NULL, PRIMARY KEY (`id`), KEY `data_access_id` (`data_access_id`), CONSTRAINT `contrexx_core_module_sync_ibfk_1` FOREIGN KEY (`data_access_id`) REFERENCES `contrexx_core_module_data_access` (`id`)) ENGINE=InnoDB;
CREATE TABLE `contrexx_core_module_sync_change` ( `id` int(11) NOT NULL AUTO_INCREMENT, `sync_id` int(11) DEFAULT NULL, `host_id` int(11) DEFAULT NULL, `status` tinyint(1) NOT NULL DEFAULT '0', `event_type` char(6) NOT NULL, `entity_index_data` text, `entity_identifier` char(255) NOT NULL, `contents` longtext, PRIMARY KEY (`id`)) ENGINE=InnoDB;
CREATE TABLE `contrexx_core_module_sync_host` ( `id` int(11) NOT NULL AUTO_INCREMENT, `host` varchar(255) NOT NULL, `active` tinyint(1) NOT NULL, `api_key` varchar(32) NOT NULL, `api_version` int(11) NOT NULL, `url_template` varchar(255) NOT NULL, `state` int(1) NOT NULL, `last_update` timestamp NOT NULL, PRIMARY KEY (`id`), UNIQUE KEY `host_UNIQUE` (`host`)) ENGINE=InnoDB;
CREATE TABLE `contrexx_core_module_sync_host_entity` ( `sync_id` int(11) NOT NULL, `host_id` int(11) NOT NULL, `entity_id` varchar(255) NOT NULL, PRIMARY KEY (`sync_id`,`host_id`,`entity_id`), KEY `host_id` (`host_id`), CONSTRAINT `contrexx_core_module_sync_host_entity_ibfk_1` FOREIGN KEY (`sync_id`) REFERENCES `contrexx_core_module_sync` (`id`), CONSTRAINT `contrexx_core_module_sync_host_entity_ibfk_2` FOREIGN KEY (`host_id`) REFERENCES `contrexx_core_module_sync_host` (`id`)) ENGINE=InnoDB;
CREATE TABLE `contrexx_core_module_sync_id_mapping` ( `id` int(11) NOT NULL AUTO_INCREMENT, `foreign_host` varchar(255) NOT NULL, `entity_type` varchar(255) NOT NULL, `foreign_id` varchar(255) NOT NULL, `local_id` varchar(255) NOT NULL, PRIMARY KEY (`id`), UNIQUE KEY `origin` (`foreign_host`,`entity_type`,`foreign_id`)) ENGINE=InnoDB;
CREATE TABLE `contrexx_core_module_sync_relation` ( `id` int(11) NOT NULL AUTO_INCREMENT, `parent_id` int(11) DEFAULT NULL, `related_sync_id` int(11) NOT NULL, `foreign_data_access_id` int(11) NOT NULL, `lft` int(11) NOT NULL, `rgt` int(11) NOT NULL, `lvl` int(11) NOT NULL, `local_field_name` varchar(50) NOT NULL, `do_sync` tinyint(1) NOT NULL, `default_entity_id` int(11) NOT NULL, PRIMARY KEY (`id`), KEY `parent_id` (`parent_id`), KEY `related_sync_id` (`related_sync_id`), KEY `foreign_data_access_id` (`foreign_data_access_id`), CONSTRAINT `contrexx_core_module_sync_relation_ibfk_1` FOREIGN KEY (`foreign_data_access_id`) REFERENCES `contrexx_core_module_data_access` (`id`), CONSTRAINT `contrexx_core_module_sync_relation_ibfk_2` FOREIGN KEY (`parent_id`) REFERENCES `contrexx_core_module_sync_relation` (`id`), CONSTRAINT `contrexx_core_module_sync_relation_ibfk_3` FOREIGN KEY (`related_sync_id`) REFERENCES `contrexx_core_module_sync` (`id`)) ENGINE=InnoDB;
ALTER TABLE `contrexx_module_mediadir_entries` CHANGE `create_date` `create_date` INT UNSIGNED NOT NULL;
ALTER TABLE `contrexx_module_mediadir_entries` CHANGE `update_date` `update_date` INT UNSIGNED NOT NULL;
ALTER TABLE `contrexx_module_mediadir_entries` CHANGE `validate_date` `validate_date` INT UNSIGNED NOT NULL;
ALTER TABLE `contrexx_module_mediadir_entries` CHANGE `popular_date` `popular_date` INT UNSIGNED NOT NULL;
ALTER TABLE `contrexx_module_mediadir_entries` CHANGE `last_ip` `last_ip` CHAR(32) NOT NULL;
ALTER TABLE `contrexx_module_mediadir_entries` CHANGE `duration_start` `duration_start` INT UNSIGNED NOT NULL;
ALTER TABLE `contrexx_module_mediadir_entries` CHANGE `duration_end` `duration_end` INT UNSIGNED NOT NULL;
ALTER TABLE `contrexx_module_mediadir_entries` CHANGE `translation_status` `translation_status` VARCHAR(60) NOT NULL;
UPDATE `contrexx_module_shop_payment_processors` SET `picture` = 'logo_payrexx.png' WHERE `contrexx_module_shop_payment_processors`.`name` = 'payrexx';
ALTER TABLE `contrexx_module_gallery_pictures` ADD INDEX `published` (`id`, `catid`, `validated`, `status`, `sorting`);
SELECT CONCAT('MediaDir inputfield with ID ', `field`.`id`, ' of type "', `type`.`name`, '" has set the incompatible context_type "', `field`.`context_type`, '". This must be fixed. See CLX-4447') AS 'Error' FROM `contrexx_module_mediadir_inputfields` AS `field` INNER JOIN `contrexx_module_mediadir_inputfield_types` AS `type` ON `type`.`id` = `field`.`type` WHERE (`context_type` = 'title' and `type` IN (SELECT `id` FROM `contrexx_module_mediadir_inputfield_types` WHERE `multi_lang` = 0)) OR (`context_type` = 'slug' AND `type` != 1) OR (`context_type` = 'keywords' AND `type` NOT IN (1,2,3,4,5));
INSERT IGNORE INTO `contrexx_module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) SELECT `entry`.`id`, `locale`.`id`, `entry`.`form_id`, `field`.`id`, "" FROM `contrexx_module_mediadir_entries` AS `entry` INNER JOIN `contrexx_module_mediadir_inputfields` AS `field` ON `field`.`form` = `entry`.`form_id` INNER JOIN `contrexx_module_mediadir_inputfield_types` AS `type` ON `type`.`id` = `field`.`type` AND `type`.`multi_lang` = 1 INNER JOIN `contrexx_core_locale_locale` AS `locale` INNER JOIN `contrexx_module_mediadir_settings` AS `settings` ON FIND_IN_SET(`locale`.`id`, `settings`.`value`) > 0 WHERE `settings`.`name` = "settingsActiveLanguages";
DROP TABLE `contrexx_module_ecard_ecards`;
DROP TABLE `contrexx_module_ecard_settings`;
DELETE FROM `contrexx_component` WHERE `name` = 'Ecard';
DELETE FROM `contrexx_modules` WHERE `name` = 'Ecard';
DELETE FROM `contrexx_module_repository` WHERE `moduleid` = 49;
DROP TABLE `contrexx_module_market`;
DROP TABLE `contrexx_module_market_categories`;
DROP TABLE `contrexx_module_market_mail`;
DROP TABLE `contrexx_module_market_paypal`;
DROP TABLE `contrexx_module_market_settings`;
DROP TABLE `contrexx_module_market_spez_fields`;
DELETE FROM `contrexx_component` WHERE `name` = 'Market';
DELETE FROM `contrexx_modules` WHERE `name` = 'Market';
DELETE FROM `contrexx_backend_areas` WHERE `module_id` = 33;
DELETE FROM `contrexx_module_repository` WHERE `moduleid` = 33;
DELETE FROM `contrexx_access_group_static_ids` WHERE `access_id` IN (99,100,101);
DROP TABLE `contrexx_module_data_categories`;
DROP TABLE `contrexx_module_data_message_to_category`;
DROP TABLE `contrexx_module_data_messages`;
DROP TABLE `contrexx_module_data_messages_lang`;
DROP TABLE `contrexx_module_data_placeholders`;
DROP TABLE `contrexx_module_data_settings`;
DELETE FROM `contrexx_component` WHERE `name` = 'Data';
DELETE FROM `contrexx_modules` WHERE `name` = 'Data';
DELETE FROM `contrexx_module_repository` WHERE `moduleid` = 48;
DROP TABLE `contrexx_module_feed_category`;
DROP TABLE `contrexx_module_feed_news`;
DROP TABLE `contrexx_module_feed_newsml_association`;
DROP TABLE `contrexx_module_feed_newsml_categories`;
DROP TABLE `contrexx_module_feed_newsml_documents`;
DROP TABLE `contrexx_module_feed_newsml_providers`;
DELETE FROM `contrexx_component` WHERE `name` = 'Feed';
DELETE FROM `contrexx_modules` WHERE `name` = 'Feed';
DELETE FROM `contrexx_module_repository` WHERE `moduleid` = 22;
DROP TABLE `contrexx_module_podcast_category`;
DROP TABLE `contrexx_module_podcast_medium`;
DROP TABLE `contrexx_module_podcast_rel_category_lang`;
DROP TABLE `contrexx_module_podcast_rel_medium_category`;
DROP TABLE `contrexx_module_podcast_settings`;
DROP TABLE `contrexx_module_podcast_template`;
DELETE FROM `contrexx_component` WHERE `name` = 'Podcast';
DELETE FROM `contrexx_modules` WHERE `name` = 'Podcast';
DELETE FROM `contrexx_module_repository` WHERE `moduleid` = 35;
DELETE FROM `contrexx_access_group_static_ids` WHERE `access_id` = 87;
DROP TABLE `contrexx_voting_additionaldata`;
DROP TABLE `contrexx_voting_email`;
DROP TABLE `contrexx_voting_rel_email_system`;
DROP TABLE `contrexx_voting_results`;
DROP TABLE `contrexx_voting_system`;
DELETE FROM `contrexx_component` WHERE `name` = 'Voting';
DELETE FROM `contrexx_modules` WHERE `name` = 'Voting';
DELETE FROM `contrexx_module_repository` WHERE `moduleid` = 17;
DROP TABLE `contrexx_module_docsys`;
DROP TABLE `contrexx_module_docsys_categories`;
DROP TABLE `contrexx_module_docsys_entry_category`;
DELETE FROM `contrexx_component` WHERE `name` = 'DocSys';
DELETE FROM `contrexx_modules` WHERE `name` = 'DocSys';
DELETE FROM `contrexx_module_repository` WHERE `moduleid` = 19;
ALTER TABLE `contrexx_access_users` DROP `u2u_active`;
DROP TABLE `contrexx_module_u2u_address_list`;
DROP TABLE `contrexx_module_u2u_message_log`;
DROP TABLE `contrexx_module_u2u_sent_messages`;
DROP TABLE `contrexx_module_u2u_settings`;
DROP TABLE `contrexx_module_u2u_user_log`;
DELETE FROM `contrexx_backend_areas` WHERE `module_id` = 54;
DELETE FROM `contrexx_component` WHERE `name` = 'U2u';
DELETE FROM `contrexx_modules` WHERE `name` = 'U2u';
DELETE FROM `contrexx_module_repository` WHERE `moduleid` = 54;
DROP TABLE `contrexx_module_recommend`;
DELETE FROM `contrexx_backend_areas` WHERE `module_id` = 27;
DELETE FROM `contrexx_component` WHERE `name` = 'Recommend';
DELETE FROM `contrexx_modules` WHERE `name` = 'Recommend';
DELETE FROM `contrexx_module_repository` WHERE `moduleid` = 27;
DELETE `input` FROM `contrexx_module_mediadir_rel_entry_inputfields` AS `input` LEFT JOIN `contrexx_module_mediadir_inputfields` AS `field` ON `field`.`id` = `input`.`field_id` AND `input`.`form_id` = `field`.`form` WHERE ISNULL(`field`.`id`);
DROP TABLE IF EXISTS `contrexx_backups`;
ALTER TABLE `contrexx_module_calendar_rel_event_host` ADD PRIMARY KEY (`host_id`, `event_id`);
DELETE FROM `contrexx_component` WHERE `name` = 'Directory';
DELETE FROM `contrexx_modules` WHERE `name` = 'Directory';
DELETE FROM `contrexx_access_group_static_ids` WHERE `access_id` IN (59,92,94,96,97);
DELETE FROM `contrexx_backend_areas` WHERE `module_id` = 12;
DELETE FROM `contrexx_module_repository` WHERE `moduleid` = 12;
DROP TABLE `contrexx_module_directory_categories`;
DROP TABLE `contrexx_module_directory_dir`;
DROP TABLE `contrexx_module_directory_inputfields`;
DROP TABLE `contrexx_module_directory_levels`;
DROP TABLE `contrexx_module_directory_mail`;
DROP TABLE `contrexx_module_directory_rel_dir_cat`;
DROP TABLE `contrexx_module_directory_rel_dir_level`;
DROP TABLE `contrexx_module_directory_settings`;
DROP TABLE `contrexx_module_directory_vote`;
DELETE FROM `contrexx_component` WHERE `name` = 'Forum';
DELETE FROM `contrexx_modules` WHERE `name` = 'Forum';
DELETE FROM `contrexx_access_group_static_ids` WHERE `access_id` IN (106,107,108);
DELETE FROM `contrexx_backend_areas` WHERE `module_id` = 20;
DELETE FROM `contrexx_module_repository` WHERE `moduleid` = 20;
DROP TABLE `contrexx_module_forum_access`;
DROP TABLE `contrexx_module_forum_categories`;
DROP TABLE `contrexx_module_forum_categories_lang`;
DROP TABLE `contrexx_module_forum_notification`;
DROP TABLE `contrexx_module_forum_postings`;
DROP TABLE `contrexx_module_forum_rating`;
DROP TABLE `contrexx_module_forum_settings`;
DROP TABLE `contrexx_module_forum_statistics`;
DELETE FROM `contrexx_component` WHERE `name` = 'MemberDir';
DELETE FROM `contrexx_modules` WHERE `name` = 'MemberDir';
DELETE FROM `contrexx_access_group_static_ids` WHERE `access_id` IN (83);
DELETE FROM `contrexx_backend_areas` WHERE `module_id` = 31;
DELETE FROM `contrexx_module_repository` WHERE `moduleid` = 31;
DROP TABLE `contrexx_module_memberdir_directories`;
DROP TABLE `contrexx_module_memberdir_name`;
DROP TABLE `contrexx_module_memberdir_settings`;
DROP TABLE `contrexx_module_memberdir_values`;
DELETE FROM `contrexx_component` WHERE `name` = 'Egov';
DELETE FROM `contrexx_modules` WHERE `name` = 'Egov';
DELETE FROM `contrexx_access_group_static_ids` WHERE `access_id` IN (109);
DELETE FROM `contrexx_backend_areas` WHERE `module_id` = 38;
DELETE FROM `contrexx_module_repository` WHERE `moduleid` = 38;
DELETE FROM `contrexx_core_setting` WHERE `section` = 'Egov';
DROP TABLE `contrexx_module_egov_configuration`;
DROP TABLE `contrexx_module_egov_orders`;
DROP TABLE `contrexx_module_egov_product_calendar`;
DROP TABLE `contrexx_module_egov_product_fields`;
DROP TABLE `contrexx_module_egov_products`;
DELETE FROM `contrexx_component` WHERE `name` = 'Livecam';
DELETE FROM `contrexx_modules` WHERE `name` = 'Livecam';
DELETE FROM `contrexx_access_group_static_ids` WHERE `access_id` IN (82);
DELETE FROM `contrexx_backend_areas` WHERE `module_id` = 30;
DELETE FROM `contrexx_module_repository` WHERE `moduleid` = 30;
DROP TABLE `contrexx_module_livecam`;
DROP TABLE `contrexx_module_livecam_settings`;
DELETE FROM `contrexx_component` WHERE `name` = 'GuestBook';
DELETE FROM `contrexx_modules` WHERE `name` = 'GuestBook';
DELETE FROM `contrexx_access_group_static_ids` WHERE `access_id` IN (9);
DELETE FROM `contrexx_backend_areas` WHERE `module_id` = 10;
DELETE FROM `contrexx_module_repository` WHERE `moduleid` = 10;
DROP TABLE `contrexx_module_guestbook`;
DROP TABLE `contrexx_module_guestbook_settings`;
DELETE FROM `contrexx_component` WHERE `name` = 'Knowledge';
DELETE FROM `contrexx_modules` WHERE `name` = 'Knowledge';
DELETE FROM `contrexx_access_group_static_ids` WHERE `access_id` IN (129,130,131,132,133,134);
DELETE FROM `contrexx_backend_areas` WHERE `module_id` = 56;
DELETE FROM `contrexx_module_repository` WHERE `moduleid` = 56;
DROP TABLE `contrexx_module_knowledge_article_content`;
DROP TABLE `contrexx_module_knowledge_articles`;
DROP TABLE `contrexx_module_knowledge_categories`;
DROP TABLE `contrexx_module_knowledge_categories_content`;
DROP TABLE `contrexx_module_knowledge_settings`;
DROP TABLE `contrexx_module_knowledge_tags`;
DROP TABLE `contrexx_module_knowledge_tags_articles`;
DELETE FROM `contrexx_component` WHERE `name` = 'Jobs';
DELETE FROM `contrexx_modules` WHERE `name` = 'Jobs';
DELETE FROM `contrexx_access_group_static_ids` WHERE `access_id` IN (148);
DELETE FROM `contrexx_backend_areas` WHERE `module_id` = 57;
DELETE FROM `contrexx_module_repository` WHERE `moduleid` = 57;
DROP TABLE `contrexx_module_jobs_rel_flag_job`;
DROP TABLE `contrexx_module_jobs`;
DROP TABLE `contrexx_module_jobs_categories`;
DROP TABLE `contrexx_module_jobs_flag`;
DROP TABLE `contrexx_module_jobs_location`;
DROP TABLE `contrexx_module_jobs_rel_loc_jobs`;
DROP TABLE `contrexx_module_jobs_settings`;
DROP TABLE `contrexx_access_user_network`;
DELETE FROM `contrexx_access_settings` WHERE `key` LIKE "sociallogin%";
DELETE FROM `contrexx_core_setting` WHERE `section` = "Access" AND `group` = "sociallogin";
DELETE FROM `contrexx_module_repository` WHERE `cmd` = "settings_networks";
DELETE FROM `contrexx_backend_areas` WHERE `area_id` = 208 AND `parent_area_id` = 189;
UPDATE `contrexx_backend_areas` SET `area_name` = 'TXT_MEDIA_ARCHIVES' WHERE `area_id` = 7 AND `parent_area_id` = 15;
DELETE FROM `contrexx_translations` WHERE `object_class` = 'Cx\\Core_Modules\\Privacy\\Model\\Entity\\Cookie' AND `foreign_key` IN (1, 2, 3);
UPDATE `contrexx_core_module_privacy_cookie` SET `component` = "Privacy" WHERE `id` = 3;
UPDATE `contrexx_module_downloads_download` SET `image` = CONCAT('/', `image`) WHERE `image` NOT LIKE '/%' AND `image` != '';
UPDATE `contrexx_module_downloads_download` SET `image` = REPLACE(`image`, '/images/downloads/','/images/Downloads/') WHERE `image` COLLATE `utf8_bin` LIKE '/images/downloads/%';
UPDATE `contrexx_module_downloads_download_locale` SET `source` = CONCAT('/', `source`) WHERE `source` LIKE 'images/%' OR `source` LIKE 'media/%';
UPDATE `contrexx_module_downloads_download_locale` SET `source` = REPLACE(`source`, '/images/downloads/','/images/Downloads/') WHERE `source` COLLATE `utf8_bin` LIKE '/images/downloads/%';
INSERT INTO `contrexx_core_country` (`id`, `alpha2`, `alpha3`) VALUES (240,'CW','CUW');
INSERT INTO `contrexx_core_country` (`id`, `alpha2`, `alpha3`) VALUES (241,'SX','SXM');
INSERT INTO `contrexx_core_country` (`id`, `alpha2`, `alpha3`) VALUES (242,'ME','MNE');
INSERT INTO `contrexx_core_country` (`id`, `alpha2`, `alpha3`) VALUES (243,'RS','SRB');
UPDATE `contrexx_core_setting` SET `value` = REPLACE(REPLACE(`value`, '151', '240,241'), '236', '242,243') WHERE `section` = 'Shop' AND `name` = 'available_countries' AND (`value` LIKE '%151%' OR `value` LIKE '%236%');
SELECT CONCAT(
    'The shop is currently configured to Netherlands Antilles as its home country.\n',
    'Netherlands Antilles has been replaced in ISO 3166-2 by Curaçao & Sint Maarten.\n',
    'See https://en.wikipedia.org/wiki/ISO_3166-2:AN.\n',
    'Please migrate this manually by executing either one of the following:\n',
    '# Migrate to Curaçao:\n',
    '\tUPDATE `contrexx_core_setting` SET `value` = "240" WHERE `section` = "Shop" AND `name` = "country_id" AND `value` = "151";\n',
    '# Migrate to Sint Maarten\n',
    '\tUPDATE `contrexx_core_setting` SET `value` = "241" WHERE `section` = "Shop" AND `name` = "country_id" AND `value` = "151";'
) AS 'Error' FROM `contrexx_core_setting` WHERE `section` = 'Shop' AND `name` = 'country_id' AND `value` = '151';
SELECT CONCAT(
    'The shop is currently configured to Yugoslavia as its home country.\n',
    'Yugoslavia has been replaced in ISO 3166-2 by Montenegro & Serbia.\n',
    'See https://en.wikipedia.org/wiki/ISO_3166-2:CS.\n',
    'Please migrate this manually by executing either one of the following:\n',
    '# Migrate to Montenegro:\n',
    '\tUPDATE `contrexx_core_setting` SET `value` = "242" WHERE `section` = "Shop" AND `name` = "country_id" AND `value` = "236";\n',
    '# Migrate to Serbia\n',
    '\tUPDATE `contrexx_core_setting` SET `value` = "243" WHERE `section` = "Shop" AND `name` = "country_id" AND `value` = "236";'
) AS 'Error' FROM `contrexx_core_setting` WHERE `section` = 'Shop' AND `name` = 'country_id' AND `value` = '236';
SELECT CONCAT(
    'The user with ID ', `user_id`,' has currently Netherlands Antilles set as its country.\n',
    'Netherlands Antilles has been replaced in ISO 3166-2 by Curaçao & Sint Maarten.\n',
    'See https://en.wikipedia.org/wiki/ISO_3166-2:AN.\n',
    'Please migrate this manually by executing either one of the following:\n',
    '# Migrate to Curaçao:\n',
    '\tUPDATE `contrexx_access_user_profile` SET `country` = 240 WHERE `user_id` = ', `user_id`, ';\n',
    '# Migrate to Sint Maarten\n',
    '\tUPDATE `contrexx_access_user_profile` SET `country` = 241 WHERE `user_id` = ', `user_id`, ';'
) AS 'Error' FROM `contrexx_access_user_profile` WHERE `country` = 151;
SELECT CONCAT(
    'The user with ID ', `user_id`,' has currently Yugoslavia set as its country.\n',
    'Yugoslavia has been replaced in ISO 3166-2 by Montenegro & Serbia.\n',
    'See https://en.wikipedia.org/wiki/ISO_3166-2:CS.\n',
    'Please migrate this manually by executing either one of the following:\n',
    '# Migrate to Montenegro:\n',
    '\tUPDATE `contrexx_access_user_profile` SET `country` = 242 WHERE `user_id` = ', `user_id`, ';\n',
    '# Migrate to Serbia\n',
    '\tUPDATE `contrexx_access_user_profile` SET `country` = 243 WHERE `user_id` = ', `user_id`, ';'
) AS 'Error' FROM `contrexx_access_user_profile` WHERE `country` = 236;
UPDATE `contrexx_module_block_targeting_option` SET `value` = REPLACE(REPLACE(`value`, '"151"', '"240","241"'), '"236"', '"242","243"') WHERE `type` = 'country' AND (`value` LIKE '%"151"%' OR `value` LIKE '%"236"%');
SELECT CONCAT(
    'The recipient with ID ', `id`,' has currently Netherlands Antilles set as its country.\n',
    'Netherlands Antilles has been replaced in ISO 3166-2 by Curaçao & Sint Maarten.\n',
    'See https://en.wikipedia.org/wiki/ISO_3166-2:AN.\n',
    'Please migrate this manually by executing either one of the following:\n',
    '# Migrate to Curaçao:\n',
    '\tUPDATE `contrexx_module_newsletter_user` SET `country_id` = 240 WHERE `id` = ', `id`, ';\n',
    '# Migrate to Sint Maarten\n',
    '\tUPDATE `contrexx_module_newsletter_user` SET `country_id` = 241 WHERE `id` = ', `id`, ';'
) AS 'Error' FROM `contrexx_module_newsletter_user` WHERE `country_id` = 151;
SELECT CONCAT(
    'The recipient with ID ', `id`,' has currently Yugoslavia set as its country.\n',
    'Yugoslavia has been replaced in ISO 3166-2 by Montenegro & Serbia.\n',
    'See https://en.wikipedia.org/wiki/ISO_3166-2:CS.\n',
    'Please migrate this manually by executing either one of the following:\n',
    '# Migrate to Montenegro:\n',
    '\tUPDATE `contrexx_module_newsletter_user` SET `country_id` = 242 WHERE `id` = ', `id`, ';\n',
    '# Migrate to Serbia\n',
    '\tUPDATE `contrexx_module_newsletter_user` SET `country_id` = 243 WHERE `id` = ', `id`, ';'
) AS 'Error' FROM `contrexx_module_newsletter_user` WHERE `country_id` = 236;
SELECT CONCAT(
    'The order with ID ', `id`,' has currently Netherlands Antilles set as its shipping country.\n',
    'Netherlands Antilles has been replaced in ISO 3166-2 by Curaçao & Sint Maarten.\n',
    'See https://en.wikipedia.org/wiki/ISO_3166-2:AN.\n',
    'Please migrate this manually by executing either one of the following:\n',
    '# Migrate to Curaçao:\n',
    '\tUPDATE `contrexx_module_shop_orders` SET `country_id` = 240 WHERE `id` = ', `id`, ';\n',
    '# Migrate to Sint Maarten\n',
    '\tUPDATE `contrexx_module_shop_orders` SET `country_id` = 241 WHERE `id` = ', `id`, ';'
) AS 'Error' FROM `contrexx_module_shop_orders` WHERE `country_id` = 151;
SELECT CONCAT(
    'The order with ID ', `id`,' has currently Yugoslavia set as its shipping country.\n',
    'Yugoslavia has been replaced in ISO 3166-2 by Montenegro & Serbia.\n',
    'See https://en.wikipedia.org/wiki/ISO_3166-2:CS.\n',
    'Please migrate this manually by executing either one of the following:\n',
    '# Migrate to Montenegro:\n',
    '\tUPDATE `contrexx_module_shop_orders` SET `country_id` = 242 WHERE `id` = ', `id`, ';\n',
    '# Migrate to Serbia\n',
    '\tUPDATE `contrexx_module_shop_orders` SET `country_id` = 243 WHERE `id` = ', `id`, ';'
) AS 'Error' FROM `contrexx_module_shop_orders` WHERE `country_id` = 236;
SELECT CONCAT(
    'The order with ID ', `id`,' has currently Netherlands Antilles set as its billing country.\n',
    'Netherlands Antilles has been replaced in ISO 3166-2 by Curaçao & Sint Maarten.\n',
    'See https://en.wikipedia.org/wiki/ISO_3166-2:AN.\n',
    'Please migrate this manually by executing either one of the following:\n',
    '# Migrate to Curaçao:\n',
    '\tUPDATE `contrexx_module_shop_orders` SET `billing_country_id` = 240 WHERE `id` = ', `id`, ';\n',
    '# Migrate to Sint Maarten\n',
    '\tUPDATE `contrexx_module_shop_orders` SET `billing_country_id` = 241 WHERE `id` = ', `id`, ';'
) AS 'Error' FROM `contrexx_module_shop_orders` WHERE `billing_country_id` = 151;
SELECT CONCAT(
    'The order with ID ', `id`,' has currently Yugoslavia set as its billing country.\n',
    'Yugoslavia has been replaced in ISO 3166-2 by Montenegro & Serbia.\n',
    'See https://en.wikipedia.org/wiki/ISO_3166-2:CS.\n',
    'Please migrate this manually by executing either one of the following:\n',
    '# Migrate to Montenegro:\n',
    '\tUPDATE `contrexx_module_shop_orders` SET `billing_country_id` = 242 WHERE `id` = ', `id`, ';\n',
    '# Migrate to Serbia\n',
    '\tUPDATE `contrexx_module_shop_orders` SET `billing_country_id` = 243 WHERE `id` = ', `id`, ';'
) AS 'Error' FROM `contrexx_module_shop_orders` WHERE `billing_country_id` = 236;
INSERT IGNORE INTO `contrexx_module_shop_rel_countries` (`zone_id`, `country_id`) SELECT `zone_id`, 240 FROM `contrexx_module_shop_rel_countries` WHERE `country_id` = 151;
INSERT IGNORE INTO `contrexx_module_shop_rel_countries` (`zone_id`, `country_id`) SELECT `zone_id`, 241 FROM `contrexx_module_shop_rel_countries` WHERE `country_id` = 151;
INSERT IGNORE INTO `contrexx_module_shop_rel_countries` (`zone_id`, `country_id`) SELECT `zone_id`, 242 FROM `contrexx_module_shop_rel_countries` WHERE `country_id` = 236;
INSERT IGNORE INTO `contrexx_module_shop_rel_countries` (`zone_id`, `country_id`) SELECT `zone_id`, 243 FROM `contrexx_module_shop_rel_countries` WHERE `country_id` = 236;
DELETE FROM `contrexx_module_shop_rel_countries` WHERE `country_id` IN (151, 236);
UPDATE `contrexx_core_setting` SET `value` = REPLACE(REPLACE(REPLACE(`value`, '174', '999'), '74', '73'), '999', '174') WHERE `section` = 'Shop' AND `name` = 'available_countries' AND (`value` = '74' OR `value` LIKE '74,%' OR `value` LIKE '%,74' OR `value` LIKE '%,74,%');
UPDATE `contrexx_core_setting` SET `value` = '73' WHERE `section` = 'Shop' AND `name` = 'country_id' AND `value` = '74';
UPDATE `contrexx_access_user_profile` SET `country` = 73 WHERE `country` = 74;
UPDATE `contrexx_module_block_targeting_option` SET `value` = REPLACE(`value`, '"74"', '"73"') WHERE `type` = 'country' AND `value` LIKE '%"74"%';
UPDATE `contrexx_module_newsletter_user` SET `country_id` = 73 WHERE `country_id` = 74;
UPDATE `contrexx_module_shop_orders` SET `country_id` = 73 WHERE `country_id` = 74;
UPDATE `contrexx_module_shop_orders` SET `billing_country_id` = 73 WHERE `billing_country_id` = 74;
UPDATE IGNORE `contrexx_module_shop_rel_countries` SET `country_id` = 73 WHERE `country_id` = 74;
INSERT INTO `contrexx_core_country` (`id`, `alpha2`, `alpha3`) VALUES (244,'SS','SSD');
INSERT INTO `contrexx_core_country` (`id`, `alpha2`, `alpha3`) VALUES (245,'AX','ALA');
INSERT INTO `contrexx_core_country` (`id`, `alpha2`, `alpha3`) VALUES (246,'BL','BLM');
INSERT INTO `contrexx_core_country` (`id`, `alpha2`, `alpha3`) VALUES (247,'BQ','BES');
INSERT INTO `contrexx_core_country` (`id`, `alpha2`, `alpha3`) VALUES (248,'GG','GGY');
INSERT INTO `contrexx_core_country` (`id`, `alpha2`, `alpha3`) VALUES (249,'IM','IMN');
INSERT INTO `contrexx_core_country` (`id`, `alpha2`, `alpha3`) VALUES (250,'JE','JEY');
INSERT INTO `contrexx_core_country` (`id`, `alpha2`, `alpha3`) VALUES (251,'MF','MAF');
INSERT INTO `contrexx_core_country` (`id`, `alpha2`, `alpha3`) VALUES (252,'PS','PSE');
UPDATE `contrexx_core_country` SET `alpha2` = 'TL', `alpha3` = 'TLS' WHERE `alpha2` = 'TP';
UPDATE `contrexx_core_country` SET `alpha2` = 'CD', `alpha3` = 'COD' WHERE `alpha2` = 'ZR';
DELETE FROM `contrexx_core_country` WHERE `alpha2` IN ('AN', 'YU', 'FX');
INSERT INTO `contrexx_core_country_country` (`alpha2`, `alpha3`, `ord`) VALUES ('TL','TLS',0);
INSERT INTO `contrexx_core_country_country` (`alpha2`, `alpha3`, `ord`) VALUES ('CD','COD',0);
INSERT INTO `contrexx_core_country_country` (`alpha2`, `alpha3`, `ord`) VALUES ('CW','CUW',0);
INSERT INTO `contrexx_core_country_country` (`alpha2`, `alpha3`, `ord`) VALUES ('SX','SXM',0);
INSERT INTO `contrexx_core_country_country` (`alpha2`, `alpha3`, `ord`) VALUES ('ME','MNE',0);
INSERT INTO `contrexx_core_country_country` (`alpha2`, `alpha3`, `ord`) VALUES ('RS','SRB',0);
INSERT INTO `contrexx_core_country_country` (`alpha2`, `alpha3`, `ord`) VALUES ('SS','SSD',0);
INSERT INTO `contrexx_core_country_country` (`alpha2`, `alpha3`, `ord`) VALUES ('AX','ALA',0);
INSERT INTO `contrexx_core_country_country` (`alpha2`, `alpha3`, `ord`) VALUES ('BL','BLM',0);
INSERT INTO `contrexx_core_country_country` (`alpha2`, `alpha3`, `ord`) VALUES ('BQ','BES',0);
INSERT INTO `contrexx_core_country_country` (`alpha2`, `alpha3`, `ord`) VALUES ('GG','GGY',0);
INSERT INTO `contrexx_core_country_country` (`alpha2`, `alpha3`, `ord`) VALUES ('IM','IMN',0);
INSERT INTO `contrexx_core_country_country` (`alpha2`, `alpha3`, `ord`) VALUES ('JE','JEY',0);
INSERT INTO `contrexx_core_country_country` (`alpha2`, `alpha3`, `ord`) VALUES ('MF','MAF',0);
INSERT INTO `contrexx_core_country_country` (`alpha2`, `alpha3`, `ord`) VALUES ('PS','PSE',0);
UPDATE `contrexx_core_locale_locale` SET `country` = 'TL' WHERE `country` = 'TP';
UPDATE `contrexx_core_locale_locale` SET `country` = 'CD' WHERE `country` = 'ZR';
UPDATE `contrexx_core_locale_locale` SET `country` = 'FR' WHERE `country` = 'FX';
SELECT CONCAT('Locale "', `label`, '" (ID:', `id`, ') is associated to the country code "AN" (Netherlands Antilles) which has been replaced in ISO 3166-2 by "CW" (Curaçao) & "SX" (Sint Maarten).\nSee https://en.wikipedia.org/wiki/ISO_3166-2:AN.\nPlease migrate this locale manually by executing either one of the following:\nUPDATE `contrexx_core_locale_locale` SET `country` = "CW" WHERE `id` = ', `id`, ';\nUPDATE `contrexx_core_locale_locale` SET `country` = "SX" WHERE `id` = ', `id`, ';') AS 'Error' FROM `contrexx_core_locale_locale` WHERE `country` = 'AN';
SELECT CONCAT('Locale "', `label`, '" (ID:', `id`, ') is associated to the country code "YU" (Yugoslavia) which has been replaced in ISO 3166-2 by "ME" (Montenegro) & "RS" (Serbia).\nSee https://en.wikipedia.org/wiki/ISO_3166-2:CS.\nPlease migrate this locale manually by executing either one of the following:\nUPDATE `contrexx_core_locale_locale` SET `country` = "ME" WHERE `id` = ', `id`, ';\nUPDATE `contrexx_core_locale_locale` SET `country` = "RS" WHERE `id` = ', `id`, ';') AS 'Error' FROM `contrexx_core_locale_locale` WHERE `country` = 'YU';
DELETE FROM `contrexx_core_country_country` WHERE `alpha2` IN ('AN', 'YU', 'FX', 'TP', 'ZR');
INSERT INTO `contrexx_core_locale_language` (`iso_1`,`iso_3`,`source`) VALUES ('ie','ile',0);
SELECT CONCAT('The ', CASE WHEN `page`.`type` = 'alias' THEN 'alias' ELSE 'page' END, ' "', `page`.`title`, '" (ID: ', `page`.`id`, ') uses the incompatible slug "', `page`.`slug`, '". Please fix manually!') AS `Error` FROM `contrexx_content_page` AS `page` INNER JOIN `contrexx_content_node` AS `node` ON `node`.`id` = `page`.`node_id` WHERE (`lvl` = 1 OR `node`.`parent_id` = 1) AND(`page`.`slug` IN ('cadmin', 'api') OR `page`.`slug` IN (SELECT CASE WHEN ISNULL(`locale`.`country`) THEN `locale`.`iso_1` ELSE CONCAT(`locale`.`iso_1`, '-', `country`) END FROM `contrexx_core_locale_locale` AS `locale`));
INSERT INTO `contrexx_module_mediadir_settings` (`id`, `name`, `value`) VALUES (55,'globalSearchLinkCategoryAndLevel','0');
CREATE TABLE `contrexx_core_mediasource_indexer_entry` (`id` int(11) NOT NULL AUTO_INCREMENT, `path` mediumtext NOT NULL, `indexer` varchar(255) NOT NULL, `content` mediumtext NOT NULL, `last_update` datetime NOT NULL, PRIMARY KEY (`id`), UNIQUE KEY `path_UNIQUE` (`path`(255)), FULLTEXT KEY `content` (`content`)) ENGINE=InnoDB;
INSERT INTO `contrexx_component` (`id`, `name`, `type`) VALUES (123, 'Legacy','core_module');
INSERT INTO `contrexx_modules` (`id`, `name`, `distributor`, `description_variable`, `status`, `is_required`, `is_core`, `is_active`, `is_licensed`, `additional_data`) VALUES (123,'Legacy','Cloudrexx AG','TXT_CORE_MODULES_LEGACY_DESCRIPTION','n',1,1,1,1,NULL);
UPDATE `contrexx_component` SET `name` = 'Sync2' WHERE `name` = 'Sync';
UPDATE `contrexx_modules` SET `is_active` = 0 WHERE `name` = 'Sync';
SELECT CONCAT('The following pages are broken as they share the same slug and are located within the same branch of the content tree! Ensure the slugs of those pages are unique:\n', GROUP_CONCAT(CONCAT(' - ', `page`.`title`, ' (', `page`.`id`, '): /cadmin/ContentManager?cmd=ContentManager&page=', `page`.`id`, '&tab=navigation') SEPARATOR '\n')) AS 'Error' FROM `contrexx_content_page` AS `page` INNER JOIN `contrexx_content_node` AS `node` ON `node`.`id` = `page`.`node_id` INNER JOIN `contrexx_core_locale_locale` AS `locale` ON `locale`.`id` = `page`.`lang` WHERE `page`.`lang` > 0 GROUP BY `node`.`parent_id`, `page`.`slug`, `page`.`lang` HAVING COUNT(`node`.`parent_id`) > 1 ORDER BY COUNT(`node`.`parent_id`);
ALTER TABLE `contrexx_settings_thumbnail` CHANGE `size` `size` TEXT NOT NULL;
ALTER TABLE `contrexx_module_contact_form` ADD `sender_email` VARCHAR(255) NOT NULL DEFAULT '' AFTER `mails`;
ALTER TABLE `contrexx_module_contact_form` ADD `sender_name` VARCHAR(255) NOT NULL DEFAULT '' AFTER `sender_email`;
INSERT INTO `contrexx_module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (70,5,'useWaitlist','TXT_CALENDAR_USE_WAITLIST','2','TXT_CALENDAR_USE_WAITLIST_TOOLTIP','3','TXT_CALENDAR_ACTIVATE,TXT_CALENDAR_DEACTIVATE','','16');
ALTER TABLE `contrexx_module_block_blocks` CHANGE `id` `id` INT(11) unsigned AUTO_INCREMENT, CHANGE `start` `start` INT(11) NOT NULL DEFAULT 0, CHANGE `end` `end` INT(11) NOT NULL DEFAULT 0, CHANGE `cat` `cat` INT(11) unsigned DEFAULT NULL, ADD `version_targeting_option` TEXT, ADD `version_rel_lang_content` TEXT, ADD `version_rel_page_global` TEXT, ADD `version_rel_page_category` TEXT, ADD `version_rel_page_direct` TEXT, ADD KEY `module_block_blocks_ibfk_cat_idx` (`cat`);
ALTER TABLE `contrexx_module_block_categories` CHANGE `id` `id` INT(11) unsigned NOT NULL AUTO_INCREMENT, CHANGE `parent` `parent` INT(11) unsigned DEFAULT NULL, CHANGE `order` `order` INT(11) NOT NULL DEFAULT 0, ADD KEY `module_block_category_ibfk_parent_idx` (`parent`);
ALTER TABLE `contrexx_module_block_rel_lang_content` ADD `id` INT(11) unsigned NOT NULL AUTO_INCREMENT FIRST, ADD PRIMARY KEY (`id`), CHANGE `block_id` `block_id` INT(11) unsigned DEFAULT 0, CHANGE `lang_id` `lang_id` INT(11) NOT NULL DEFAULT 0, DROP KEY `id_lang`, ADD UNIQUE KEY `unique_block_page`(`block_id`, `lang_id`), ADD KEY `module_block_rel_lang_content_ibfk_lang_id_idx` (`lang_id`);
ALTER TABLE `contrexx_module_block_rel_pages` ADD `id` INT(11) unsigned NOT NULL AUTO_INCREMENT FIRST, DROP PRIMARY KEY, ADD PRIMARY KEY (`id`), CHANGE `block_id` `block_id` INT(11) unsigned DEFAULT 0, CHANGE `page_id` `page_id` INT(11) NOT NULL DEFAULT 0, ADD UNIQUE KEY `unique_block_page_placeholder`(`block_id`, `page_id`, `placeholder`), ADD KEY `module_block_rel_pages_ibfk_page_id_idx` (`page_id`);
ALTER TABLE `contrexx_module_block_targeting_option` DROP PRIMARY KEY, ADD `id` INT(11) unsigned NOT NULL AUTO_INCREMENT FIRST, ADD PRIMARY KEY (`id`), CHANGE `block_id` `block_id` INT(11) unsigned DEFAULT NULL, ADD UNIQUE KEY `unique_block_type` (`block_id`,`type`);
ALTER TABLE `contrexx_access_group_dynamic_ids` CHANGE `access_id` `access_id` INT(11) NOT NULL, CHANGE `group_id` `group_id` INT(11) NOT NULL;
ALTER TABLE `contrexx_access_group_static_ids` CHANGE `access_id` `access_id` INT(11) NOT NULL, CHANGE `group_id` `group_id` INT(11) NOT NULL;
ALTER TABLE `contrexx_access_rel_user_group` CHANGE `user_id` `user_id` INT(11) NOT NULL, CHANGE `group_id` `group_id` INT(11) NOT NULL;
ALTER TABLE `contrexx_access_user_attribute` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `parent_id` `parent_id` INT(11) NULL DEFAULT NULL, CHANGE `order_id` `order_id` INT(11) NOT NULL DEFAULT '0', CHANGE `access_id` `access_id` INT(11) NOT NULL, CHANGE `read_access_id` `read_access_id` INT(11) NOT NULL;
ALTER TABLE `contrexx_access_user_attribute` ADD KEY IF NOT EXISTS `contrexx_access_user_attribute_parent_id_ibfk` (`parent_id`);
ALTER TABLE `contrexx_access_user_attribute_name` CHANGE `attribute_id` `attribute_id` INT(11) NOT NULL DEFAULT '0', CHANGE `lang_id` `lang_id` INT(11) NOT NULL DEFAULT '0';
ALTER TABLE `contrexx_access_user_attribute_name` ADD KEY IF NOT EXISTS `contrexx_access_user_attribute_name_attribute_id_ibfk` (`attribute_id`);
ALTER TABLE `contrexx_access_user_attribute_value` CHANGE `attribute_id` `attribute_id` INT(11) NOT NULL, CHANGE `user_id` `user_id` INT(11) NOT NULL, CHANGE `history_id` `history_id` INT(11) NOT NULL DEFAULT '0';
ALTER TABLE `contrexx_access_user_attribute_value` ADD KEY IF NOT EXISTS `contrexx_access_user_attribute_value_user_id_ibfk` (`user_id`);
ALTER TABLE `contrexx_access_user_core_attribute` CHANGE `order_id` `order_id` INT(11) NOT NULL DEFAULT '0', CHANGE `access_id` `access_id` INT(11) NOT NULL, CHANGE `read_access_id` `read_access_id` INT(11) NOT NULL;
ALTER TABLE `contrexx_access_user_groups` CHANGE `group_id` `group_id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `is_active` `is_active` SMALLINT(6) NOT NULL DEFAULT '1';
ALTER TABLE `contrexx_access_user_groups` CHANGE `toolbar` `toolbar` INT(11) NOT NULL DEFAULT '0';
ALTER TABLE `contrexx_access_user_profile` CHANGE `user_id` `user_id` INT(11) NOT NULL, CHANGE `title` `title` INT(11) NULL DEFAULT NULL, CHANGE `country` `country` INT(11) NOT NULL DEFAULT '0';
ALTER TABLE `contrexx_access_user_profile` ADD KEY IF NOT EXISTS `contrexx_access_user_profile_title_ibfk` (`title`);
ALTER TABLE `contrexx_access_user_title` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `order_id` `order_id` INT(11) NOT NULL DEFAULT '0';
ALTER TABLE `contrexx_access_users` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `is_admin` `is_admin` TINYINT(1) NOT NULL DEFAULT '0', CHANGE `auth_token_timeout` `auth_token_timeout` INT(11) NOT NULL DEFAULT '0', CHANGE `regdate` `regdate` INT(11) NOT NULL DEFAULT '0', CHANGE `expiration` `expiration` INT(11) NOT NULL DEFAULT '0', CHANGE `validity` `validity` INT(11) NOT NULL DEFAULT '0', CHANGE `last_auth` `last_auth` INT(11) NOT NULL DEFAULT '0', CHANGE `last_auth_status` `last_auth_status` SMALLINT(6) NOT NULL DEFAULT '1', CHANGE `last_activity` `last_activity` INT(11) NOT NULL DEFAULT '0', CHANGE `frontend_lang_id` `frontend_lang_id` INT(11) NOT NULL DEFAULT '0', CHANGE `backend_lang_id` `backend_lang_id` INT(11) NOT NULL DEFAULT '0', CHANGE `verified` `verified` TINYINT(1) NOT NULL DEFAULT '1', CHANGE `primary_group` `primary_group` INT(11) NOT NULL DEFAULT '0', CHANGE `restore_key_time` `restore_key_time` INT(11) NOT NULL DEFAULT '0';
ALTER TABLE `contrexx_content_page` CHANGE `updatedBy` `updatedBy` VARCHAR(40) NOT NULL, CHANGE `useCustomContentForAllChannels` `useCustomContentForAllChannels` SMALLINT(6) NULL DEFAULT NULL, CHANGE `useCustomApplicationTemplateForAllChannels` `useCustomApplicationTemplateForAllChannels` SMALLINT(6) NULL DEFAULT NULL, CHANGE `useSkinForAllChannels` `useSkinForAllChannels` SMALLINT(6) NULL DEFAULT NULL;
LOCK TABLES `contrexx_content_node` WRITE, `contrexx_content_page` WRITE;
ALTER TABLE `contrexx_content_node` DROP FOREIGN KEY IF EXISTS `contrexx_content_node_ibfk_2`;
ALTER TABLE `contrexx_content_node` DROP FOREIGN KEY IF EXISTS `contrexx_content_node_ibfk_1`;
ALTER TABLE `contrexx_content_page` DROP FOREIGN KEY IF EXISTS `contrexx_content_page_ibfk_3`;
ALTER TABLE `contrexx_content_page` DROP FOREIGN KEY IF EXISTS `contrexx_content_page_ibfk_1`;
ALTER TABLE `contrexx_content_node` DROP INDEX IF EXISTS `IDX_E5A18FDD727ACA70`;
ALTER TABLE `contrexx_content_page` DROP INDEX IF EXISTS `IDX_D8E86F54460D9FD7`;
ALTER TABLE `contrexx_content_node` ADD INDEX IF NOT EXISTS `contrexx_content_node_parent_id_ibfk` (`parent_id`);
ALTER TABLE `contrexx_content_page` ADD INDEX IF NOT EXISTS `contrexx_content_page_node_id_ibfk` (`node_id`);
ALTER TABLE `contrexx_content_node` ADD CONSTRAINT `contrexx_content_node_ibfk_2` FOREIGN KEY (`parent_id`) REFERENCES `contrexx_content_node` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE `contrexx_content_page` ADD CONSTRAINT `contrexx_content_page_ibfk_3` FOREIGN KEY (`node_id`) REFERENCES `contrexx_content_node` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;
UNLOCK TABLES;
ALTER TABLE `contrexx_core_modules_access_permission` CHANGE `allowed_protocols` `allowed_protocols` longtext NOT NULL COMMENT '(DC2Type:array)', CHANGE `allowed_methods` `allowed_methods` longtext NOT NULL COMMENT '(DC2Type:array)', CHANGE `valid_user_groups` `valid_user_groups` longtext NOT NULL COMMENT '(DC2Type:array)', CHANGE `valid_access_ids` `valid_access_ids` longtext NOT NULL COMMENT '(DC2Type:array)';
ALTER TABLE `contrexx_core_module_data_access` CHANGE `field_list` `field_list` longtext NOT NULL COMMENT '(DC2Type:array)', CHANGE `access_condition` `access_condition` longtext NOT NULL COMMENT '(DC2Type:array)', CHANGE `allowed_output_methods` `allowed_output_methods` longtext NOT NULL COMMENT '(DC2Type:array)';
ALTER TABLE `contrexx_core_data_source` CHANGE `options` `options` longtext NOT NULL COMMENT '(DC2Type:array)';
ALTER TABLE `contrexx_core_data_source` ADD UNIQUE INDEX IF NOT EXISTS `identifier` (`identifier`);
ALTER TABLE `contrexx_log_entry` CHANGE `logged_at` `logged_at` TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, CHANGE `data` `data` LONGTEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '(DC2Type:array)';
ALTER TABLE `contrexx_log_entry` DROP INDEX IF EXISTS `log_class_unique_version_idx`;
ALTER TABLE `contrexx_log_entry` ADD UNIQUE INDEX IF NOT EXISTS `log_version_lookup_idx` (`version`,`object_id`,`object_class`);
ALTER TABLE `contrexx_module_calendar_category_name` CHANGE `lang_id` `lang_id` int(11) NOT NULL, CHANGE `name` `name` varchar(225) NOT NULL;
ALTER TABLE `contrexx_module_calendar_event` CHANGE `series_status` `series_status` INT(4) NOT NULL DEFAULT '0', CHANGE `independent_series` `independent_series` SMALLINT NOT NULL DEFAULT '1';
ALTER TABLE `contrexx_module_calendar_event` ADD KEY IF NOT EXISTS `contrexx_module_calendar_registration_form_ibkf` (`registration_form`);
ALTER TABLE `contrexx_module_calendar_event_field` CHANGE `event_id` `event_id` INT(11) NOT NULL, CHANGE `lang_id` `lang_id` INT(11) NOT NULL;
ALTER TABLE `contrexx_module_calendar_events_categories` CHANGE `event_id` `event_id` INT(11) NOT NULL, CHANGE `category_id` `category_id` INT(11) NOT NULL;
ALTER TABLE `contrexx_module_calendar_events_categories` DROP KEY IF EXISTS `category_id`;
ALTER TABLE `contrexx_module_calendar_events_categories` ADD KEY IF NOT EXISTS `contrexx_module_calendar_events_categories_event_id_ibfk` (`event_id`);
ALTER TABLE `contrexx_module_calendar_events_categories` ADD KEY IF NOT EXISTS `contrexx_module_calendar_events_categories_category_id_ibfk` (`category_id`);
ALTER TABLE `contrexx_module_calendar_invite` CHANGE `date` `date` INT unsigned NOT NULL;
ALTER TABLE `contrexx_module_calendar_invite` ADD KEY IF NOT EXISTS `contrexx_module_calendar_event_id_ibfk` (`event_id`);
ALTER TABLE `contrexx_module_calendar_registration` CHANGE `submission_date` `submission_date` timestamp DEFAULT '0000-00-00 00:00:00';
ALTER TABLE `contrexx_module_calendar_registration` ADD UNIQUE INDEX IF NOT EXISTS `UNIQ_7F5FE63EA417747` (`invite_id`), ADD KEY IF NOT EXISTS `contrexx_module_calendar_registration_event_id_ibfk` (`event_id`);
ALTER TABLE `contrexx_module_calendar_registration_form_field_name` ADD KEY IF NOT EXISTS `contrexx_module_calendar_registration_form_field_name_13` (`field_id`);
ALTER TABLE `contrexx_module_calendar_registration_form_field_value` ADD KEY IF NOT EXISTS `contrexx_module_calendar_registration_form_field_value_11` (`reg_id`);
ALTER TABLE `contrexx_module_calendar_registration_form_field_value` ADD KEY IF NOT EXISTS `contrexx_module_calendar_registration_form_field_value_14` (`field_id`);
ALTER TABLE `contrexx_module_calendar_registration` DROP IF EXISTS `key`;
ALTER TABLE `contrexx_module_contact_form` CHANGE `id` `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT, CHANGE `showForm` `showForm` TINYINT(1) NOT NULL DEFAULT '0', CHANGE `use_captcha` `use_captcha` TINYINT(1) NOT NULL DEFAULT '1', CHANGE `use_custom_style` `use_custom_style` TINYINT(1) NOT NULL DEFAULT '0', CHANGE `html_mail` `html_mail` TINYINT(1) NOT NULL DEFAULT '1', CHANGE `send_attachment` `send_attachment` TINYINT(1) NOT NULL DEFAULT '0', CHANGE `crm_customer_groups` `crm_customer_groups` LONGTEXT DEFAULT NULL COMMENT '(DC2Type:array)';
ALTER TABLE `contrexx_module_crm_currency` CHANGE `active` `active` TINYINT(4) NOT NULL DEFAULT '1', CHANGE `pos` `pos` INT(11) NOT NULL DEFAULT '0';
ALTER TABLE `contrexx_module_mediadir_inputfields` CHANGE `context_type` `context_type` enum('none','title','content','address','zip','city','country','image','keywords','slug') NOT NULL;
ALTER TABLE `contrexx_module_shop_order_attributes` CHANGE `option_name` `option_name` TEXT NOT NULL DEFAULT '';
ALTER TABLE `contrexx_access_rel_user_group` ADD INDEX IF NOT EXISTS `contrexx_access_rel_user_group_user_id_ibfk` (`user_id`);
ALTER TABLE `contrexx_access_rel_user_group` ADD INDEX IF NOT EXISTS `contrexx_access_rel_user_group_group_id_ibfk` (`group_id`);
ALTER TABLE `contrexx_access_user_attribute` ADD INDEX IF NOT EXISTS `tree` (`id`, `order_id`, `parent_id`);
ALTER TABLE `contrexx_access_user_attribute_name` ADD INDEX IF NOT EXISTS `id_name` (`attribute_id`, `name`);
ALTER TABLE `contrexx_access_users` CHANGE `email` `email` varchar(255) NOT NULL;
ALTER TABLE `contrexx_access_users` ADD UNIQUE INDEX IF NOT EXISTS `UNIQ_7CD32875E7927C74` (`email`);
ALTER TABLE `contrexx_content_node` ADD KEY IF NOT EXISTS `full_index` (`id`, `lft`, `rgt`, `lvl`, `parent_id`);
ALTER TABLE `contrexx_content_page` ADD KEY IF NOT EXISTS `node_type_lang` (`node_id`, `type`, `lang`);
ALTER TABLE `contrexx_content_page` ADD KEY IF NOT EXISTS `slug_idx` (`node_id`, `type`, `lang`, `slug`);
ALTER TABLE `contrexx_core_locale_locale` CHANGE `order_no` `order_no` int(11) NOT NULL DEFAULT '0';
ALTER TABLE `contrexx_module_calendar_category_name` ADD KEY IF NOT EXISTS `cat_id` (`cat_id`);
ALTER TABLE `contrexx_module_contact_form_field` CHANGE `type` `type` enum('text','label','checkbox','checkboxGroup','country','date','file','multi_file','fieldset','hidden','horizontalLine','password','radio','select','textarea','recipient','special','datetime') BINARY NOT NULL DEFAULT 'text';
ALTER TABLE `contrexx_module_crm_contacts` DROP INDEX IF EXISTS `customer_id_2`;
ALTER TABLE `contrexx_module_crm_contacts` DROP INDEX IF EXISTS `customer_id`;
ALTER TABLE `contrexx_module_crm_contacts` DROP INDEX IF EXISTS `customer_name`;
ALTER TABLE `contrexx_module_crm_contacts` DROP INDEX IF EXISTS `contact_familyname`;
ALTER TABLE `contrexx_module_crm_contacts` DROP INDEX IF EXISTS `contact_role`;
ALTER TABLE `contrexx_module_crm_contacts` CHANGE `customer_id` `customer_id` varchar(256) BINARY DEFAULT NULL;
ALTER TABLE `contrexx_module_crm_contacts` CHANGE `customer_name` `customer_name` varchar(256) BINARY DEFAULT NULL;
ALTER TABLE `contrexx_module_crm_contacts` CHANGE `customer_website` `customer_website` varchar(256) BINARY DEFAULT NULL;
ALTER TABLE `contrexx_module_crm_contacts` CHANGE `contact_familyname` `contact_familyname` varchar(256) BINARY DEFAULT NULL;
ALTER TABLE `contrexx_module_crm_contacts` CHANGE `contact_role` `contact_role` varchar(256) BINARY DEFAULT NULL;
ALTER TABLE `contrexx_module_crm_contacts` CHANGE `notes` `notes` text BINARY;
ALTER TABLE `contrexx_module_crm_contacts` CHANGE `profile_picture` `profile_picture` varchar(256) BINARY NOT NULL DEFAULT '';
ALTER TABLE `contrexx_module_crm_contacts` ADD INDEX `customer_id` (`customer_id`(255));
ALTER TABLE `contrexx_module_crm_contacts` ADD INDEX `customer_name` (`customer_name`(255));
ALTER TABLE `contrexx_module_crm_contacts` ADD INDEX `contact_familyname` (`contact_familyname`(255));
ALTER TABLE `contrexx_module_crm_contacts` ADD INDEX `contact_role` (`contact_role`(255));
ALTER TABLE `contrexx_module_crm_contacts` ADD FULLTEXT KEY IF NOT EXISTS `customer_id_2` (`customer_id`,`customer_name`,`contact_familyname`,`contact_role`,`notes`);
ALTER TABLE `contrexx_module_crm_contacts` ADD FULLTEXT KEY IF NOT EXISTS `customer_name_fulltext` (`customer_name`);
ALTER TABLE `contrexx_module_crm_contacts` ADD FULLTEXT KEY IF NOT EXISTS `contact_familyname_fulltext` (`contact_familyname`);
ALTER TABLE `contrexx_module_crm_customer_contact_address` DROP INDEX IF EXISTS `address`, ADD INDEX `address` (`address`(255));
ALTER TABLE `contrexx_module_crm_customer_contact_address` DROP INDEX IF EXISTS `city`, ADD INDEX `city` (`city`(255));
ALTER TABLE `contrexx_module_crm_customer_contact_address` DROP INDEX IF EXISTS `state`, ADD INDEX `state` (`state`(255));
ALTER TABLE `contrexx_module_crm_customer_contact_address` DROP INDEX IF EXISTS `zip`, ADD INDEX `zip` (`zip`(255));
ALTER TABLE `contrexx_module_crm_customer_contact_address` DROP INDEX IF EXISTS `country`, ADD INDEX `country` (`country`(255));
ALTER TABLE `contrexx_module_crm_customer_contact_emails` DROP INDEX IF EXISTS `email`, ADD INDEX `email` (`email`(255));
ALTER TABLE `contrexx_module_crm_customer_contact_phone` DROP INDEX IF EXISTS `phone`, ADD INDEX `phone` (`phone`(255));
ALTER TABLE `contrexx_module_crm_customer_contact_social_network` DROP INDEX IF EXISTS `url`, ADD INDEX `url` (`url`(255));
ALTER TABLE `contrexx_module_crm_customer_contact_websites` DROP INDEX IF EXISTS `url`, ADD INDEX `url` (`url`(255));
ALTER TABLE `contrexx_module_crm_customer_types` CHANGE `label` `label` varchar(250) BINARY NOT NULL;
ALTER TABLE `contrexx_module_crm_industry_type_local` CHANGE `value` `value` varchar(256) BINARY NOT NULL;
ALTER TABLE `contrexx_module_crm_industry_type_local` DROP INDEX IF EXISTS `value`, ADD INDEX `value` (`value`(255));
ALTER TABLE `contrexx_module_crm_membership_local` CHANGE `value` `value` varchar(256) BINARY NOT NULL;
ALTER TABLE `contrexx_module_crm_membership_local` DROP INDEX IF EXISTS `value`, ADD INDEX `value` (`value`(255));
ALTER TABLE `contrexx_module_crm_task` CHANGE `task_id` `task_id` varchar(10) BINARY NOT NULL;
ALTER TABLE `contrexx_module_crm_task` CHANGE `task_title` `task_title` varchar(255) BINARY NOT NULL;
ALTER TABLE `contrexx_module_crm_task` CHANGE `description` `description` text BINARY NOT NULL;
ALTER TABLE `contrexx_module_crm_task_types` DROP INDEX IF EXISTS `name`, ADD INDEX `name` (`name`(255));
ALTER TABLE `contrexx_module_downloads_download` CHANGE `version` `version` varchar(255) NOT NULL DEFAULT '';
ALTER TABLE `contrexx_core_module_sync_change` CHANGE `id` `id` int NOT NULL AUTO_INCREMENT;
ALTER TABLE `contrexx_core_module_sync_change` CHANGE `sync_id` `sync_id` int NOT NULL;
ALTER TABLE `contrexx_core_module_sync_change` ADD COLUMN IF NOT EXISTS `origin_sync_id` int NOT NULL AFTER `sync_id`;
ALTER TABLE `contrexx_core_module_sync_change` CHANGE `origin_sync_id` `origin_sync_id` int NOT NULL;
ALTER TABLE `contrexx_core_module_sync_change` CHANGE `event_type` `event_type` varchar(6) NOT NULL;
ALTER TABLE `contrexx_core_module_sync_change` CHANGE `entity_index_data` `entity_index_data` longtext NOT NULL COMMENT '(DC2Type:array)';
ALTER TABLE `contrexx_core_module_sync_change` CHANGE `contents` `contents` longtext NOT NULL COMMENT '(DC2Type:array)';
ALTER TABLE `contrexx_core_module_sync_change` DROP COLUMN IF EXISTS `host_id`;
ALTER TABLE `contrexx_core_module_sync_change` DROP COLUMN IF EXISTS `status`;
ALTER TABLE `contrexx_core_module_sync_change` ADD COLUMN IF NOT EXISTS `condition` varchar(7) NOT NULL AFTER `event_type`;
ALTER TABLE `contrexx_core_module_sync_change` CHANGE `condition` `condition` varchar(7) NOT NULL;
ALTER TABLE `contrexx_core_module_sync_change` ADD COLUMN IF NOT EXISTS `origin_entity_index_data` longtext NOT NULL COMMENT '(DC2Type:array)' AFTER `entity_index_data`;
ALTER TABLE `contrexx_core_module_sync_change` CHANGE `origin_entity_index_data` `origin_entity_index_data` longtext NOT NULL COMMENT '(DC2Type:array)';
ALTER TABLE `contrexx_core_module_sync_change` ADD INDEX IF NOT EXISTS `contrexx_core_module_sync_change_sync_id_ibfk` (`sync_id`);
ALTER TABLE `contrexx_core_module_sync_change` ADD INDEX IF NOT EXISTS `contrexx_core_module_sync_change_origin_sync_id_ibfk` (`origin_sync_id`);
ALTER TABLE `contrexx_core_module_sync_id_mapping` CHANGE `foreign_id` `foreign_id` varchar(255) NOT NULL;
ALTER TABLE `contrexx_core_module_sync_id_mapping` CHANGE `local_id` `local_id` varchar(255) NOT NULL;
ALTER TABLE `contrexx_core_module_sync_relation` DROP FOREIGN KEY IF EXISTS `contrexx_core_module_sync_relation_ibfk_foreign_data_access_id`;
ALTER TABLE `contrexx_core_module_sync_relation` DROP FOREIGN KEY IF EXISTS `contrexx_core_module_sync_relation_ibfk_1`;
ALTER TABLE `contrexx_core_module_sync_relation` DROP KEY IF EXISTS `foreign_data_access_id`;
ALTER TABLE `contrexx_core_module_sync_relation` ADD CONSTRAINT `contrexx_core_module_sync_relation_ibfk_foreign_data_access_id` FOREIGN KEY (`foreign_data_access_id`) REFERENCES `contrexx_core_module_data_access` (`id`);
ALTER TABLE `contrexx_core_module_sync_relation` ADD KEY IF NOT EXISTS `contrexx_core_module_sync_relation_ibfk_foreign_data_access_id` (`foreign_data_access_id`);
CREATE TABLE IF NOT EXISTS `contrexx_core_module_sync_change_host` (`change_id` int(11) NOT NULL, `host_id` int(11) NOT NULL, PRIMARY KEY (`change_id`,`host_id`), INDEX `contrexx_core_module_sync_change_host_change_id_ibfk` (`change_id`), INDEX `contrexx_core_module_sync_change_host_host_id_ibfk` (`host_id`)) ENGINE=InnoDB;
ALTER TABLE `contrexx_core_module_sync_change_host` ADD INDEX IF NOT EXISTS `contrexx_core_module_sync_change_host_change_id_ibfk` (`change_id`);
ALTER TABLE `contrexx_core_module_sync_change_host` ADD INDEX IF NOT EXISTS `contrexx_core_module_sync_change_host_host_id_ibfk` (`host_id`);
DELETE FROM `contrexx_core_setting` WHERE `section` = "Shop" AND `name` LIKE "paymill_%";
DELETE `pp`, `p`, `t` FROM `contrexx_module_shop_payment_processors` AS `pp` LEFT JOIN `contrexx_module_shop_payment` AS `p` ON `p`.`processor_id` = `pp`.`id` LEFT JOIN `contrexx_core_text` AS `t` ON `t`.`id` = `p`.`id` AND `t`.`section` = "Shop" AND `t`.`key` = "payment_name" WHERE `pp`.`name` LIKE "paymill_%";
DELETE FROM `contrexx_module_shop_rel_payment` WHERE `payment_id` NOT IN (SELECT `id` FROM `contrexx_module_shop_payment`);
UPDATE `contrexx_module_mediadir_inputfields` SET `context_type` = 'none' WHERE `context_type` = '';
DELETE `d`.*, `s`.* FROM `contrexx_module_contact_form_data` AS `d` LEFT JOIN `contrexx_module_contact_form_submit_data` AS `s` ON `d`.`id` = `s`.`id_entry` WHERE `d`.`id_form` = 0;
SELECT CONCAT("Entry ", `entry_id`, "/", `lang_id`, "/", `form_id`, "/", `field_id`, " in contrexx_module_mediadir_rel_entry_inputfields matches a HTML tag. After this update this tag will be displayed.") FROM `contrexx_module_mediadir_rel_entry_inputfields` AS `v` JOIN `contrexx_module_mediadir_inputfields` AS `f` ON `f`.id = `v`.`field_id` WHERE `f`.`type` = 1 AND `value` REGEXP "<[^ ]";
UPDATE `contrexx_module_calendar_event` SET `startdate` = `enddate` WHERE `startdate` = '0000-00-00 00:00:00' OR ISNULL(`startdate`);
UPDATE `contrexx_module_calendar_event` SET `enddate` = `startdate` WHERE `enddate` = '0000-00-00 00:00:00' OR ISNULL(`enddate`);
SELECT CONCAT('The event #', `id`, ' has neither a `startdate`, nor an `enddate` set. Please fix manually!') AS 'Error' FROM `contrexx_module_calendar_event` WHERE (`startdate` = '0000-00-00 00:00:00' OR ISNULL(`startdate`)) AND (`enddate` = '0000-00-00 00:00:00' OR ISNULL(`enddate`));
INSERT INTO `contrexx_backend_areas` (`area_id`, `parent_area_id`, `type`, `scope`, `area_name`, `is_active`, `uri`, `target`, `module_id`, `order_id`, `access_id`) VALUES(257, 2, "navigation", "backend", "TXT_COMPONENTMANAGER_ADD_NEW_APPLICATION", 1, "index.php?cmd=ComponentManager", "_self", 76, 1, 23);
UPDATE `contrexx_backend_areas` SET `uri` = REPLACE(`uri`, '&stat=', '&act=') WHERE `uri` LIKE '%&stat=%';
UPDATE `contrexx_backend_areas` SET `area_name` = "TXT_CRM_CUSTOMER" WHERE `area_id` = 191;
ALTER TABLE `contrexx_core_rewrite_rule` ADD `active` TINYINT(1) NOT NULL AFTER `id`;
UPDATE `contrexx_core_rewrite_rule` SET `active` = 1;
ALTER TABLE `contrexx_core_setting` ADD `user_based` TINYINT(1) NOT NULL DEFAULT '0' AFTER `group`, DROP PRIMARY KEY, ADD `id` INT(11) NOT NULL AUTO_INCREMENT FIRST, ADD PRIMARY KEY(`id`), ADD UNIQUE KEY `setting_primary_idx` (`section`,`name`,`group`);
CREATE TABLE contrexx_core_setting_user_value (id INT AUTO_INCREMENT NOT NULL, setting_id INT NOT NULL, user_id INT NOT NULL, value LONGTEXT NOT NULL, INDEX IDX_B6923490EE35BD72 (setting_id), UNIQUE INDEX setting_user_idx (user_id, setting_id), PRIMARY KEY(id)) ENGINE = InnoDB;
SELECT CONCAT('Locale "', `label`, '" (ID:', `id`, ') is associated to the system language "da" (Dansk) which has been dropped') AS 'Error' FROM `contrexx_core_locale_locale` WHERE `source_language` = 'da';
SELECT CONCAT('Backend Language "', `iso_1`, '" (ID:', `id`, ') is associated to the system language "da" (Dansk) which has been dropped') AS 'Error' FROM `contrexx_core_locale_backend` WHERE `iso_1` = 'da';
UPDATE `contrexx_core_locale_language` SET `source` = 0 WHERE `iso_1` = 'da';
SELECT CONCAT("The form with ID ", `f`.`id`, " has no translation and needs to be cleaned up!") FROM `contrexx_module_contact_form` AS `f` LEFT JOIN `contrexx_module_contact_form_lang` as `l` ON `f`.`id` = `l`.`formID` WHERE `l`.`id` IS NULL;
INSERT IGNORE INTO `contrexx_module_news_settings` (`name`, `value`) VALUES ('news_use_teaser_text','');
SELECT CONCAT('Feature Calendar RSS-Feed has been discontinued! See CLX-3865') AS 'Error' FROM `contrexx_module_calendar_settings` WHERE `name` = 'rssFeedStatus';
DELETE FROM `contrexx_module_calendar_settings` WHERE `name` = 'rssFeedStatus';
SELECT CONCAT('Feature Calendar Event Payment has been discontinued! See CLX-3864') AS 'Error' FROM `contrexx_module_calendar_settings` WHERE `name` = 'paymentStatus' AND `value` = '1';
DELETE FROM `contrexx_module_calendar_settings` WHERE `name` IN ('paymentStatus', 'paymentBillStatus', 'paymentCurrency', 'paymentVatRate', 'paymentlBillGrace', 'paymentYellowpayStatus', 'paymentYellowpayPspid', 'paymentYellowpayShaIn', 'paymentYellowpayShaOut', 'paymentYellowpayAuthorization', 'paymentTestserver', 'paymentYellowpayMethods', 'paymentVatNumber', 'paymentBank', 'paymentBankAccount', 'paymentBankIBAN', 'paymentBankCN', 'paymentBankSC');
DELETE FROM `contrexx_module_calendar_settings_section` WHERE `name`  IN ('payment', 'paymentBill', 'paymentYellowpay', 'paymentBank');
ALTER TABLE `contrexx_module_calendar_registration` DROP IF EXISTS `payment_method`, DROP IF EXISTS `paid`;
ALTER TABLE `contrexx_module_calendar_event` DROP IF EXISTS `price`, DROP IF EXISTS `ticket_sales`, DROP IF EXISTS `num_seating`;
INSERT INTO `contrexx_component` (`id`, `name`, `type`) VALUES (125, 'Usercache','core');
INSERT INTO `contrexx_modules` (`id`, `name`, `distributor`, `description_variable`, `status`, `is_required`, `is_core`, `is_active`, `is_licensed`, `additional_data`) VALUES (125,'Usercache','Cloudrexx AG','TXT_CORE_USERCACHE_DESCRIPTION','n',1,1,1,1,NULL);
UPDATE `contrexx_module_shop_vat` AS `vat` INNER JOIN `contrexx_core_text` AS `text` ON `text`.`id`=`vat`.`id` AND `text`.`Section`='Shop' AND `text`.`key`='vat_class' AND `vat`.`rate` = '7.70' AND `text`.`text` IN ('Lichtenstein', 'MWST Schweiz', 'Normal', 'Schweiz ', 'Schweiz') SET `vat`.`rate` = '8.10';
UPDATE `contrexx_module_shop_vat` AS `vat` INNER JOIN `contrexx_core_text` AS `text` ON `text`.`id`=`vat`.`id` AND `text`.`Section`='Shop' AND `text`.`key`='vat_class' AND `vat`.`rate` = '2.50' AND `text`.`text` IN ('Schweiz ermässigt 1', 'Schweiz ermässigt 2', 'Schweiz Getränke', 'Ermässigt') SET `vat`.`rate` = '2.60';
UPDATE `contrexx_module_shop_vat` AS `vat` INNER JOIN `contrexx_core_text` AS `text` ON `text`.`id`=`vat`.`id` AND `text`.`Section`='Shop' AND `text`.`key`='vat_class' AND `vat`.`rate` = '3.70' AND `text`.`text` IN ('Schweiz ermässigt 1', 'Hotellerie') SET `vat`.`rate` = '3.80';
# See line 586 CREATE TABLE contrexx_core_netmanager_domain (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, spf_record VARCHAR(255) NOT NULL, spf_record_timeout TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00', dmarc_record VARCHAR(255) NOT NULL, dmarc_record_timeout TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00', dkim_record TEXT NOT NULL, dkim_record_timeout TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00', dkim_domainkey VARCHAR(255) NOT NULL, dkim_private_key TEXT NOT NULL, dkim_public_key TEXT NOT NULL, dkim_passphrase VARCHAR(255) NOT NULL, UNIQUE INDEX unique_netmanager_domain_name (name), PRIMARY KEY(id)) ENGINE = MyISAM;
UPDATE `contrexx_modules` SET `is_required` = 0 WHERE `name` LIKE "News";
UPDATE `contrexx_module_calendar_registration_form_field_name` AS `name` INNER JOIN `contrexx_module_calendar_registration_form_field` AS `field` ON `field`.`id` = `name`.`field_id` SET `name`.`form_id` = `field`.`form` WHERE `name`.`form_id` = 0;
# See line 586 ALTER TABLE `contrexx_core_netmanager_domain` CHANGE `spf_record_timeout` `spf_record_timeout` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00';
# See line 586 ALTER TABLE `contrexx_core_netmanager_domain` CHANGE `dkim_record_timeout` `dkim_record_timeout` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00';
# See line 586 ALTER TABLE `contrexx_core_netmanager_domain` CHANGE `dmarc_record_timeout` `dmarc_record_timeout` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00';
DROP TABLE IF EXISTS `contrexx_core_netmanager_domain`;
SELECT "Check if this internal redirect redirects to internal targets:", `id`, `regular_expression` FROM `contrexx_core_rewrite_rule` WHERE `active` = 1 AND `rewrite_status_code` = 'intern';
DELETE orphan FROM `contrexx_core_text` AS orphan INNER JOIN `contrexx_core_text` AS active USING (`id`, `section`, `key`) INNER JOIN `contrexx_core_locale_locale` AS locale ON locale.`id` = active.`lang_id` WHERE orphan.`lang_id` NOT IN (SELECT `id` FROM `contrexx_core_locale_locale`);
DELETE FROM `contrexx_module_crm_customer_contact_social_network` WHERE `url_profile` IN(9, 10, 12, 13, 14);
DROP TABLE IF EXISTS `contrexx_module_crm_success_rate`;
ALTER TABLE `contrexx_module_filesharing` CHANGE `upload_id` `upload_id` char(10) DEFAULT NULL;
UPDATE `contrexx_module_filesharing` SET `upload_id` = NULL;
UPDATE `contrexx_backend_areas` SET `order_id` = 3 WHERE `area_id` = 18;
UPDATE `contrexx_backend_areas` SET `order_id` = 4 WHERE `area_id` = 21;
UPDATE `contrexx_backend_areas` SET `order_id` = 5 WHERE `area_id` = 22;
UPDATE `contrexx_backend_areas` SET `order_id` = 6 WHERE `area_id` = 23;
UPDATE `contrexx_backend_areas` SET `order_id` = 7 WHERE `area_id` = 110;
UPDATE `contrexx_backend_areas` SET `order_id` = 2 WHERE `area_id` = 224;
UPDATE `contrexx_backend_areas` SET `order_id` = 14 WHERE `area_id` = 228;
UPDATE `contrexx_backend_areas` SET `order_id` = 14 WHERE `area_id` = 229;
UPDATE `contrexx_backend_areas` SET `area_id` = 236 WHERE `area_id` IN (234,235,237,241);
UPDATE `contrexx_backend_areas` SET `type` = 'function' WHERE `area_id` = 225;
UPDATE `contrexx_backend_areas` SET `order_id` = 12 WHERE `area_id` = 223;
UPDATE `contrexx_backend_areas` SET `area_name` = 'TXT_CRM_CUSTOMER' WHERE `area_id` = 191;
UPDATE `contrexx_backend_areas` SET `uri` = 'index.php?cmd=Stats&act=requests' WHERE `area_id` = 164;
UPDATE `contrexx_backend_areas` SET `uri` = 'index.php?cmd=Stats&act=visitors' WHERE `area_id` = 166;
UPDATE `contrexx_backend_areas` SET `uri` = 'index.php?cmd=Stats&act=referer' WHERE `area_id` = 167;
UPDATE `contrexx_backend_areas` SET `uri` = 'index.php?cmd=Stats&act=spiders' WHERE `area_id` = 168;
UPDATE `contrexx_backend_areas` SET `uri` = 'index.php?cmd=Stats&act=search' WHERE `area_id` = 169;
UPDATE `contrexx_backend_areas` SET `uri` = 'index.php?cmd=Stats&act=settings' WHERE `area_id` = 170;
ALTER TABLE `contrexx_backend_areas` AUTO_INCREMENT = 900;
DELETE FROM `contrexx_module_contact_settings` WHERE `setname` = 'fileUploadDepositionPath';
UPDATE `contrexx_backend_areas` SET `parent_area_id` = 2, `type` = 'navigation' WHERE `area_id` = 203;
UPDATE `contrexx_backend_areas` SET `type` = 'function' WHERE `parent_area_id` = 203;
UPDATE `contrexx_backend_areas` SET `parent_area_id` = 2, `type` = 'navigation' WHERE `area_id` = 8;
UPDATE `contrexx_backend_areas` SET `type` = 'function' WHERE `parent_area_id` = 8;
UPDATE `contrexx_backend_areas` SET `parent_area_id` = 2, `type` = 'navigation' WHERE `area_id` = 29;
UPDATE `contrexx_backend_areas` SET `type` = 'function' WHERE `parent_area_id` = 29;
UPDATE `contrexx_backend_areas` SET `parent_area_id` = 2, `type` = 'navigation' WHERE `area_id` = 189;
UPDATE `contrexx_backend_areas` SET `type` = 'function' WHERE `access_id` != 0 AND `parent_area_id` = 189;
UPDATE `contrexx_backend_areas` SET `parent_area_id` = 0, `type` = 'group', `order_id` = 3 WHERE `area_id` = 90;
UPDATE `contrexx_backend_areas` SET `type` = 'navigation' WHERE `parent_area_id` = 90;
UPDATE `contrexx_backend_areas` SET `order_id` = 6 WHERE `area_id` = 3;
UPDATE `contrexx_backend_areas` SET `order_id` = 5 WHERE `area_id` = 28;
DELETE FROM `contrexx_backend_areas` WHERE `area_id` = 51;
DELETE FROM `contrexx_backend_areas` WHERE `area_id` = 257;
DELETE FROM `contrexx_backend_areas` WHERE `area_id` = 8;
UPDATE `contrexx_backend_areas` SET `parent_area_id` = 2, `type` = 'navigation', `area_name` = 'TXT_CORE_ECOMMERCE' WHERE `area_id` = 13;
UPDATE `contrexx_backend_areas` SET `module_id` = 69 WHERE `area_id` = 189;
DELETE FROM `contrexx_backend_areas` WHERE `area_id` = 191;
UPDATE `contrexx_backend_areas` SET `parent_area_id` = 2, `type` = 'navigation', `area_name` = 'TXT_NEWS_MANAGER' WHERE `area_id` = 10;
DELETE FROM `contrexx_backend_areas` WHERE `area_id` = 203;
UPDATE `contrexx_backend_areas` SET `order_id` = 0, `scope` = 'backend' WHERE `area_id` = 153;
UPDATE `contrexx_backend_areas` SET `order_id` = 1 WHERE `area_id` = 154;
UPDATE `contrexx_backend_areas` SET `order_id` = 2 WHERE `area_id` = 155;
UPDATE `contrexx_backend_areas` SET `order_id` = 3, `scope` = 'backend' WHERE `area_id` = 156;
UPDATE `contrexx_backend_areas` SET `order_id` = 4, `scope` = 'backend' WHERE `area_id` = 157;
UPDATE `contrexx_backend_areas` SET `order_id` = 5, `scope` = 'backend' WHERE `area_id` = 158;
UPDATE `contrexx_backend_areas` SET `order_id` = 6, `scope` = 'backend' WHERE `area_id` = 160;
UPDATE `contrexx_backend_areas` SET `module_id` = 4 WHERE `area_id` = 29;
ALTER TABLE `contrexx_backend_areas` CHANGE `scope` `scope` ENUM('global','frontend','backend','none') NOT NULL DEFAULT 'global';
UPDATE `contrexx_backend_areas` SET `scope` = 'none' WHERE `area_id` = 2;
DELETE FROM `contrexx_access_group_static_ids` WHERE `access_id` = 192;
UPDATE `contrexx_backend_areas` SET `access_id` = 192 WHERE `area_id` = 225;
INSERT INTO `contrexx_access_group_static_ids` SELECT 192, `group_id` FROM `contrexx_access_group_static_ids` WHERE `access_id` = 193;
DELETE FROM `contrexx_access_group_static_ids` WHERE `access_id` = 189;
INSERT INTO `contrexx_backend_areas` (`area_id`, `parent_area_id`, `type`, `scope`, `area_name`, `is_active`, `uri`, `target`, `module_id`, `order_id`, `access_id`) VALUES (222,17,'function','backend','TXT_SETTINGS_MENU_SYSTEM',1,'index.php?cmd=Config&act=System','_self',80,1,189);
INSERT INTO `contrexx_access_group_static_ids` SELECT 189, `group_id` FROM `contrexx_access_group_static_ids` WHERE `access_id` = 17;
DELETE FROM `contrexx_access_group_static_ids` WHERE `access_id` = 190;
INSERT INTO `contrexx_backend_areas` (`area_id`, `parent_area_id`, `type`, `scope`, `area_name`, `is_active`, `uri`, `target`, `module_id`, `order_id`, `access_id`) VALUES (226,17,'function','backend','TXT_SETTINGS_MENU_CACHE',1,'index.php?cmd=Config&act=cache','_self',69,2,190);
INSERT INTO `contrexx_access_group_static_ids` SELECT 190, `group_id` FROM `contrexx_access_group_static_ids` WHERE `access_id` = 17;
DELETE FROM `contrexx_access_group_static_ids` WHERE `access_id` = 191;
INSERT INTO `contrexx_backend_areas` (`area_id`, `parent_area_id`, `type`, `scope`, `area_name`, `is_active`, `uri`, `target`, `module_id`, `order_id`, `access_id`) VALUES (227,17,'function','backend','TXT_EMAIL_SERVER',1,'index.php?cmd=Config&act=smtp','_self',124,3,191);
INSERT INTO `contrexx_access_group_static_ids` SELECT 191, `group_id` FROM `contrexx_access_group_static_ids` WHERE `access_id` = 17;
UPDATE `contrexx_backend_areas` SET `uri` = 'index.php?cmd=Config&act=Wysiwyg', `order_id` = 4 WHERE `area_id` = 225;
UPDATE `contrexx_backend_areas` SET `uri` = 'index.php?cmd=Config&act=Pdf', `order_id` = 5 WHERE `area_id` = 233;
INSERT INTO `contrexx_core_setting` (`section`, `name`, `group`, `type`, `value`, `values`, `ord`) VALUES ('Shop','gender_required','config','checkbox','1','',23);
UPDATE `contrexx_module_crm_contacts` SET `customer_currency` = (SELECT `id` FROM `contrexx_module_crm_currency` WHERE `default_currency` = 1 LIMIT 1) WHERE `customer_currency` NOT IN (SELECT `id` FROM `contrexx_module_crm_currency`);
UPDATE `contrexx_modules` SET `is_required` = 0 WHERE `name` = 'Crm';
UPDATE `contrexx_modules` SET `status` = 'y' WHERE `name` = 'Crm' AND (SELECT CASE WHEN (SELECT COUNT(*) FROM `contrexx_module_crm_contacts`) = 0 THEN 'do not install' WHEN (SELECT COUNT(*) FROM `contrexx_module_crm_contacts`) = 1 AND (SELECT 1 FROM `contrexx_module_crm_contacts` WHERE (`customer_name`="Cloudrexx AG" OR `customer_name`="Comvation AG Switzerland") AND `contact_type`=1) THEN 'do not install' WHEN (SELECT COUNT(*) FROM `contrexx_module_crm_contacts`) = 1 AND (SELECT 1 FROM `contrexx_module_crm_contacts` WHERE (`customer_name`="Hans" OR `customer_name`="Alexandra") AND `contact_familyname`="Muster" AND `contact_type`=2) THEN 'do not install' WHEN (SELECT COUNT(*) FROM `contrexx_module_crm_contacts`) = 2 AND ((SELECT 1 FROM `contrexx_module_crm_contacts` WHERE `customer_name`="Cloudrexx AG" AND `contact_type`=1) AND (SELECT 1 FROM `contrexx_module_crm_contacts` WHERE `customer_name`="Hans" AND `contact_familyname`="Muster" AND `contact_type`=2)) OR ((SELECT 1 FROM `contrexx_module_crm_contacts` WHERE `customer_name`="Comvation AG Switzerland" AND `contact_type`=1) AND (SELECT 1 FROM `contrexx_module_crm_contacts` WHERE `customer_name`="Alexandra" AND `contact_familyname`="Muster" AND `contact_type`=2)) THEN 'do not install' ELSE 'do install' END AS result) = 'do install';
UPDATE `contrexx_backend_areas` SET `area_name` = 'TXT_CRM' WHERE `area_id` = 189;
