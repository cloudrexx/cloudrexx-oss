<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * This is the crmTask class file for handling the all functionalities under task menu.
 *
 * @category   CrmTask
 * @package    cloudrexx
 * @subpackage module_crm
 * @author     ss4ugroup <ss4ugroup@softsolutions4u.com>
 * @version    1.0.0
 * @link       www.cloudrexx.com
 */

namespace Cx\Modules\Crm\Controller;

/**
 * This is the crmTask class file for handling the all functionalities under task menu.
 *
 * @category   CrmTask
 * @package    cloudrexx
 * @subpackage module_crm
 * @author     ss4ugroup <ss4ugroup@softsolutions4u.com>
 * @version    1.0.0
 * @link       www.cloudrexx.com
 */

class CrmTask extends CrmLibrary
{
    /**
     * Template object
     *
     * @param object
     */
    public $_objTpl;

    /**
     * sort fields
     *
     * @access protected
     * @var array
     */
    protected $_sortFields = array(
        array(
          'name'   => 'TXT_CRM_TASK_RECENTLY_ADDED',
          'column' => 'taskId'
        ),
        array(
          'name'   => 'TXT_CRM_TASK_TITLE',
          'column' => 'task_title'
        ),
        array(
          'name'   => 'TXT_CRM_TASK_DUE_DATE',
          'column' => 'due_date'
        ),
        array(
          'name'   => 'TXT_CRM_CUSTOMER_NAME',
          'column' => 'customer_name'
        ),
        array(
          'name'   => 'TXT_CRM_TASK_RESPONSIBLE',
          'column' => 'assigned_to'
        ),
    );

    /**
     * php 5.3 contructor
     *
     * @param object $objTpl template object
     */
    function __construct($objTpl, $name)
    {
        $this->_objTpl = $objTpl;
        parent::__construct($name);
    }

    /**
     * get task overview page
     *
     * @global array $_ARRAYLANG
     * @return true
     */
    public function overview()
    {
        global $_ARRAYLANG;

        $db = \Cx\Core\Core\Controller\Cx::instanciate()->getDb()->getAdoDb();
        \JS::registerJS('lib/javascript/jquery.tmpl.min.js');

        $objtpl = $this->_objTpl;
        $_SESSION['pageTitle'] = $_ARRAYLANG['TXT_CRM_TASK_OVERVIEW'];
        $objtpl->loadTemplateFile("module_{$this->moduleNameLC}_tasks_overview.html");
        $objtpl->setGlobalVariable("MODULE_NAME", $this->moduleName);

        $msg = isset($_GET['mes']) ? base64_decode($_GET['mes']) : '';
        if ($msg) {
            switch ($msg) {
            case 'taskDeleted':
                $_SESSION['strOkMessage'] = $_ARRAYLANG['TXT_CRM_TASK_DELETE_MESSAGE'];
                break;
            }
        }

        $filterTaskType  = isset($_GET['searchType'])? intval($_GET['searchType']) : 0 ;
        $filterTaskStatus= !empty($_GET['status']) && is_array($_GET['status']) ? contrexx_input2int($_GET['status']) : array(0);
        $filterTaskTitle = isset($_GET['searchTitle'])? contrexx_input2raw($_GET['searchTitle']) : '';

        $objType = $db->Execute("SELECT id,name FROM ".DBPREFIX."module_{$this->moduleNameLC}_task_types ORDER BY sorting");
        if ($objType) {
            while (!$objType->EOF) {
                $selected = ($objType->fields['id'] == $filterTaskType) ? 'selected="selected"' : '';
                $objtpl->setVariable(array(
                    'CRM_TASK_ID'       => (int) $objType->fields['id'],
                    'TXT_TASK_NAME'     => contrexx_raw2xhtml($objType->fields['name']),
                    'TXT_TASK_SELECTED' => $selected,
                ));
                $objtpl->parse('tastType');
                $objType->MoveNext();
            }
        }
        $objtpl->setGlobalVariable(array(
                'TXT_SEARCH_VALUE'              => contrexx_raw2xhtml($filterTaskTitle),
                'CRM_TASK_STATUS_OPEN'          => !empty($filterTaskStatus) && in_array(0, $filterTaskStatus) ? 'checked="checked"' : '',
                'CRM_TASK_STATUS_COMPLETED'     => !empty($filterTaskStatus) && in_array(1, $filterTaskStatus) ? 'checked="checked"' : '',
                'TXT_CRM_OVERVIEW'              => $_ARRAYLANG['TXT_CRM_OVERVIEW'],
                'TXT_CRM_ADD_TASK'              => $_ARRAYLANG['TXT_CRM_ADD_TASK'],
                'TXT_CRM_DELETE_CONFIRM'        => $_ARRAYLANG['TXT_CRM_DELETE_CONFIRM'],
                'TXT_CRM_SEARCH'                => $_ARRAYLANG['TXT_CRM_SEARCH'],
                'TXT_CRM_ENTER_SEARCH_TERM'     => $_ARRAYLANG['TXT_CRM_ENTER_SEARCH_TERM'],
                'TXT_CRM_FILTER_TASK_TYPE'      => $_ARRAYLANG['TXT_CRM_FILTER_TASK_TYPE'],
                'TXT_CRM_FILTER_STATUS'         => $_ARRAYLANG['TXT_CRM_FILTER_STATUS'],
                'TXT_CRM_TASK_STATUS_OPEN'      => $_ARRAYLANG['TXT_CRM_TASK_STATUS_OPEN'],
                'TXT_CRM_TASK_STATUS_COMPLETED' => $_ARRAYLANG['TXT_CRM_TASK_STATUS_COMPLETED'],
        ));

        $filter     = array();
        $filterLink = [];
        if (!empty($filterTaskType)) {
            $filter[]    = " t.task_type_id = '$filterTaskType'";
            $filterLink['searchType'] = $filterTaskType;
        }
        if (!empty($filterTaskTitle)) {
            $filter[]    = " t.task_title LIKE '%$filterTaskTitle%' OR c.customer_name LIKE '%$filterTaskTitle%'";
            $filterLink['searchTitle'] = $filterTaskTitle;
        }
        if (!empty($filterTaskStatus)) {
            $filter[]    = ' t.task_status IN ('. implode(" ,", $filterTaskStatus) .')';
            $filterLink['status'] = $filterTaskStatus;
        }
        $filterCondition = !empty($filter) ? " WHERE ". implode(" AND", $filter) : '';
        $sortField       = isset($_GET['sort_by']) && array_key_exists($_GET['sort_by'], $this->_sortFields) ? (int) $_GET['sort_by'] : 2;
        $sortOrder       = (isset($_GET['sort_order']) && $_GET['sort_order'] == 1)  ? 1 : 0 ;
        $sorto = $sortOrder ? 'DESC' : 'ASC';
        $query = "
            SELECT
                t.task_status,
                t.task_title,
                c.customer_name,
                t.assigned_to,
                tt.icon,
                t.id AS taskId,
                t.added_by,
                c.contact_familyname,
                t.due_date,
                c.id AS customer_id
            FROM ".DBPREFIX."module_{$this->moduleNameLC}_task as t
            LEFT JOIN ".DBPREFIX."module_{$this->moduleNameLC}_task_types as tt
            ON (t.task_type_id=tt.id)
            LEFT JOIN ".DBPREFIX."module_{$this->moduleNameLC}_contacts as c
            ON (t.customer_id = c.id) $filterCondition
            ORDER BY {$this->_sortFields[$sortField]['column']} $sorto
        ";
        $objResult = $db->Execute($query);
        $data = new \Cx\Core_Modules\Listing\Model\Entity\DataSet();
        if ($objResult) {
            while (!$objResult->EOF) {
                $data->add(
                    $objResult->fields['taskId'],
                    $objResult->fields
                );
                $objResult->MoveNext();
            }
        }
        $vg = new \Cx\Core\Html\Controller\ViewGenerator(
            $data,
            $this->getTaskVgOptions($filterLink)
        );
        $objtpl->setVariable('TASKS', $vg->render());
    }

    /**
     * @return array    ViewGenerator options
     */
    protected function getTaskVgOptions(array $filterLink): array {
        global $_ARRAYLANG;

        return array(
            'Cx\Core_Modules\Listing\Model\Entity\DataSet' => array(
                '' => [
                    'header' => $_ARRAYLANG['TXT_CRM_TASK'],
                    'rowAttributes' => function($rowData, $attributes) {
                        if (
                            !isset($rowData['task_status'])
                            || $rowData['task_status'] == 1
                            || strtotime($rowData['due_date']) > strtotime('now')
                        ) {
                            return $attributes;
                        }
                        $attributes['class'] = 'task_expired';
                        return $attributes;
                    },
                    'functions' => array(
                        'additionalOverallFunctions' => function() {
                            return array(
                                'add' => array(
                                    'icon' => 'fa-solid fa-plus',
                                    'label' => 'TXT_ADD',
                                    'link' => '/cadmin/Crm/task?tpl=modify',
                                ),
                            );
                        },
                        'actions' => function($rowData, $editId) use ($filterLink) {
                            global $_ARRAYLANG;
                            $edit = '';
                            $delete = '';
                            list($task_edit_permission, $task_delete_permission, $task_status_update_permission) = $this->getTaskPermission((int) $rowData['added_by'], (int) $rowData['assigned_to']);
                            if ($task_edit_permission) {
                                $filterLink['pos'] = $_GET['pos'] ?? '';
                                $edit = new \Cx\Core\Html\Model\Entity\HtmlElement('a');
                                $edit->setAttributes([
                                    'href' => \Cx\Core\Routing\Url::fromBackend(
                                    $this->moduleName,
                                    'task',
                                    0,
                                    [
                                        'tpl' => 'modify',
                                        'id' => $rowData['taskId'],
                                        'redirect' => base64_encode(\Cx\Core\Routing\Url::fromBackend($this->moduleName, 'task', 0, $filterLink)),
                                    ]
                                    ),
                                    'title' => $_ARRAYLANG['TXT_CRM_IMAGE_EDIT'],
                                ]);
                                $icon = new \Cx\Core\Html\Model\Entity\HtmlElement('i');
                                $icon->setAttribute('class', 'fa-regular fa-pen-to-square');
                                $edit->addChild($icon);
                            }
                            if ($task_delete_permission) {
                                $delete = new \Cx\Core\Html\Model\Entity\HtmlElement('a');
                                $delete->setAttributes([
                                    'onclick' => 'return confirmSubmit();',
                                    'href' => \Cx\Core\Routing\Url::fromBackend(
                                        $this->moduleName,
                                        'task',
                                        0,
                                        [
                                            'tpl' => 'deleteTask',
                                            'id' => $rowData['taskId'],
                                        ]
                                    ),
                                    'title' => $_ARRAYLANG['TXT_CRM_IMAGE_DELETE'],
                                ]);
                                $icon = new \Cx\Core\Html\Model\Entity\HtmlElement('i');
                                $icon->setAttribute('class', 'fa-regular fa-trash-can');
                                $delete->addChild($icon);
                            }
                            return $edit . $delete;
                        },
                        'sorting' => true,
                        'paging' => true,
                        'order' => array(
                            'taskType' => SORT_ASC,
                        ),
                    ),
                    'fields' => [
                        'task_status' => [
                            'header' => 'TXT_CRM_TASK_STATUS',
                            'table' => [
                                'parse' => function($value, $rowData, $fieldOptions, $viewGeneratorId) {
                                    list($task_edit_permission, $task_delete_permission, $task_status_update_permission) = $this->getTaskPermission((int) $rowData['added_by'], (int) $rowData['assigned_to']);
                                    $icon = $rowData['task_status'] == 1 ? 'led_green.gif' : 'led_red.gif';
                                    $img = new \Cx\Core\Html\Model\Entity\HtmlElement('img');
                                    $img->setAttributes([
                                        'src' => '/core/Core/View/Media/icons/' . $icon,
                                        'alt' => 'status',
                                    ]);
                                    if (!$task_status_update_permission) {
                                        return $img;
                                    }
                                    $switch = new \Cx\Core\Html\Model\Entity\HtmlElement('span');
                                    $switch->setAttributes([
                                        'style' => 'cursor: pointer;',
                                        'rel' => 'task_' . $rowData['taskId'],
                                        'task_status' => $rowData['task_status'],
                                        'onclick' => 'showTaskStatusPanel(this);',
                                    ]);
                                    $switch->addChild($img);
                                    return $switch;
                                },
                            ],
                        ],
                        'task_title' => [
                            'header' => 'TXT_CRM_TASK_TITLE',
                            'table' => [
                                'parse' => function($value, $rowData, $fieldOptions, $viewGeneratorId) {
                                    if (!empty($rowData['icon'])) {
                                        $icon = \Cx\Core\Core\Controller\Cx::instanciate()->getWebsiteImagesCrmWebPath() . '/' . contrexx_raw2xhtml($rowData['icon']) . '_24X24.thumb';
                                    } else {
                                        $icon = '/modules/Crm/View/Media/task_default.png';
                                    }
                                    return '<img src="' . $icon . '" alt="' . $icon . '">
                                        <div>' . contrexx_raw2xhtml($value) . '</div>
                                        <div>' . contrexx_raw2xhtml(date('h:i A Y-m-d', strtotime($rowData['due_date']))) . '</div>';
                                },
                            ],

                        ],
                        'customer_name' => [
                            'header' => 'TXT_CRM_CUSTOMER_NAME',
                            'table' => [
                                'parse' => function($value, $rowData, $fieldOptions, $viewGeneratorId) {
                                    $name = contrexx_raw2xhtml(
                                        $rowData['customer_name'] . ' ' . $rowData['contact_familyname']
                                    );
                                    $anchor = new \Cx\Core\Html\Model\Entity\HtmlElement('a');
                                    $anchor->setAttributes([
                                        'href' => \Cx\Core\Routing\Url::fromBackend(
                                            $this->moduleName,
                                            'customers',
                                            0,
                                            ['tpl' => 'showcustdetail', 'id' => $rowData['customer_id'],]
                                        ),
                                        'title' => $name,
                                    ]);
                                    $anchor->addChild(
                                        new \Cx\Core\Html\Model\Entity\TextElement($name)
                                    );
                                    return $anchor;
                                },
                            ],
                        ],
                        'assigned_to' => [
                            'header' => 'TXT_CRM_TASK_RESPONSIBLE',
                            'table' => [
                                'parse' => function($value, $rowData, $fieldOptions, $viewGeneratorId) {
                                    return contrexx_raw2xhtml($this->getUserName($value));
                                },
                            ],
                        ],
                        'icon' => ['showOverview' => false],
                        'taskId' => ['showOverview' => false],
                        'added_by' => ['showOverview' => false],
                        'contact_familyname' => ['showOverview' => false],
                        'due_date' => ['showOverview' => false],
                        'customer_id' => ['showOverview' => false],
                    ],
                ],
            ),
        );
    }

    /**
     * add /edit task
     *
     * @global array $_ARRAYLANG
     * @global object $objDatabase
     * @return true
     */
    public function _modifyTask()
    {
        global $_ARRAYLANG,$objDatabase,$objJs,$objFWUser;

        \JS::registerCSS("modules/Crm/View/Style/contact.css");
        if ( gettype($objFWUser) === 'NULL') {
            $objFWUser = \FWUser::getFWUserObject();
        }
        $objtpl = $this->_objTpl;
        $_SESSION['pageTitle'] = empty($_GET['id']) ? $_ARRAYLANG['TXT_CRM_ADDTASK'] : $_ARRAYLANG['TXT_CRM_EDITTASK'];

        $this->_objTpl->loadTemplateFile('module_'.$this->moduleNameLC.'_addtasks.html');
        $objtpl->setGlobalVariable("MODULE_NAME", $this->moduleName);

        $settings    = $this->getSettings();

        $id          = isset($_REQUEST['id'])? (int) $_REQUEST['id']:'';
        $date        = date('Y-m-d H:i:s');
        $title       = isset($_POST['taskTitle']) ? contrexx_input2raw($_POST['taskTitle']) : '';
        $type        = isset($_POST['taskType']) ? (int) $_POST['taskType'] : 0;
        $customer    = isset($_REQUEST['customerId']) ? (int) $_REQUEST['customerId'] : '';
        $duedate     = isset($_POST['date']) ? $_POST['date'] : $date;
        $assignedto  = isset($_POST['assignedto']) ? intval($_POST['assignedto']) : 0;
        $description = isset($_POST['description']) ? contrexx_input2raw($_POST['description']) : '';
        $notify      = isset($_POST['notify']);

        $taskId      = isset($_REQUEST['searchType'])? intval($_REQUEST['searchType']) : 0 ;
        $taskTitle   = isset($_REQUEST['searchTitle'])? contrexx_input2raw($_REQUEST['searchTitle']) : '';

        $redirect     = isset($_REQUEST['redirect']) ? $_REQUEST['redirect'] : base64_encode(\Cx\Core\Routing\Url::fromBackend($this->moduleName, 'task'));

        // check permission
        if (!empty($id)) {
            $objResult = $objDatabase->Execute("SELECT `added_by`,
                                                       `assigned_to`
                                                    FROM `".DBPREFIX."module_{$this->moduleNameLC}_task`
                                                 WHERE `id` = '$id'
                                               ");
            $added_user    = (int) $objResult->fields['added_by'];
            $assigned_user = (int) $objResult->fields['assigned_to'];
            if ($objResult) {
                list($task_edit_permission) = $this->getTaskPermission($added_user, $assigned_user);
                if (!$task_edit_permission) {
                    \Permission::noAccess();
                }
            }
        }

        if (isset($_POST['addtask'])) {
            if (!empty($id)) {
                if (
                    $objFWUser->objUser->getAdminStatus() ||
                    $added_user == $objFWUser->objUser->getId() ||
                    $assigned_user == $assignedto
                ) {
                        $fields    = array(
                            'task_title'        => $title,
                            'task_type_id'      => $type,
                            'customer_id'       => $customer,
                            'due_date'          => $duedate,
                            'assigned_to'       => $assignedto,
                            'description'       => $description
                        );
                        $query = \SQL::update("module_{$this->moduleNameLC}_task", $fields, array('escape' => true))." WHERE `id` = {$id}";
                        $_SESSION['strOkMessage'] = $_ARRAYLANG['TXT_CRM_TASK_UPDATE_MESSAGE'];
                } else {
                        $_SESSION['strErrMessage'] = $_ARRAYLANG['TXT_CRM_TASK_RESPONSIBLE_ERR'];
                    }
            } else {
                $addedDate = date('Y-m-d H:i:s');
                $fields    = array(
                    'task_title'        => $title,
                    'task_type_id'      => $type,
                    'customer_id'       => $customer,
                    'due_date'          => $duedate,
                    'assigned_to'       => $assignedto,
                    'added_by'          => $objFWUser->objUser->getId(),
                    'added_date_time'   => $addedDate,
                    'task_status'       => '0',
                    'description'       => $description
                );
                $query = \SQL::insert("module_{$this->moduleNameLC}_task", $fields, array('escape' => true));
                $_SESSION['strOkMessage'] = $_ARRAYLANG['TXT_CRM_TASK_OK_MESSAGE'];
            }
            $db = $objDatabase->Execute($query);
            if ($db) {
                if ($notify) {
                    $cx = \Cx\Core\Core\Controller\Cx::instanciate();
                    $id = (!empty($id)) ? $id : $objDatabase->INSERT_ID();
                    $info['substitution'] = array(
                            'CRM_ASSIGNED_USER_NAME'            => contrexx_raw2xhtml(\FWUser::getParsedUserTitle($assignedto)),
                            'CRM_ASSIGNED_USER_EMAIL'           => $objFWUser->objUser->getUser($assignedto)->getEmail(),
                            'CRM_DOMAIN'                        => ASCMS_PROTOCOL."://{$_SERVER['HTTP_HOST']}". $cx->getCodeBaseOffsetPath(),
                            'CRM_TASK_NAME'                     => $title,
                            'CRM_TASK_LINK'                     => "<a href='". ASCMS_PROTOCOL."://{$_SERVER['HTTP_HOST']}". $cx->getCodeBaseOffsetPath(). $cx->getBackendFolderName() ."/index.php?cmd=".$this->moduleName."&act=task&tpl=modify&id=$id'>$title</a>",
                            'CRM_TASK_URL'                      => ASCMS_PROTOCOL."://{$_SERVER['HTTP_HOST']}". $cx->getCodeBaseOffsetPath(). $cx->getBackendFolderName() ."/index.php?cmd=".$this->moduleName."&act=task&tpl=modify&id=$id",
                            'CRM_TASK_DUE_DATE'                 => $duedate,
                            'CRM_TASK_CREATED_USER'             => contrexx_raw2xhtml(\FWUser::getParsedUserTitle($objFWUser->objUser->getId())),
                            'CRM_TASK_DESCRIPTION_TEXT_VERSION' => contrexx_html2plaintext($description),
                            'CRM_TASK_DESCRIPTION_HTML_VERSION' => $description
                    );
                    //setting email template lang id
                    $availableMailTempLangAry = $this->getActiveEmailTemLangId('Crm', CRM_EVENT_ON_TASK_CREATED);
                    $availableLangId          = $this->getEmailTempLang($availableMailTempLangAry, $objFWUser->objUser->getUser($assignedto)->getEmail());
                    $info['lang_id']          = $availableLangId;

                    $dispatcher = CrmEventDispatcher::getInstance();
                    $dispatcher->triggerEvent(CRM_EVENT_ON_TASK_CREATED, null, $info);
                }

                \Cx\Core\Csrf\Controller\Csrf::redirect(\Cx\Core\Routing\Url::fromMagic(base64_decode($redirect)));
            }
        } elseif (!empty($id)) {
            $objValue       = $objDatabase->Execute("SELECT task_id,
                                                            task_title,
                                                            task_type_id,
                                                            due_date,
                                                            assigned_to,
                                                            description,
                                                            c.id,
                                                            c.customer_name,
                                                            c.contact_familyname
                                                       FROM `".DBPREFIX."module_{$this->moduleNameLC}_task` AS t
                                                       LEFT JOIN `".DBPREFIX."module_{$this->moduleNameLC}_contacts` AS c
                                                            ON t.customer_id = c.id
                                                       WHERE t.id='$id'");

            $title      = $objValue->fields['task_title'];
            $type       = $objValue->fields['task_type_id'];
            $customer   = $objValue->fields['id'];
            $customerName = !empty($objValue->fields['customer_name']) ? $objValue->fields['customer_name']." ".$objValue->fields['contact_familyname'] : '';
            $duedate    = $objValue->fields['due_date'];
            $assignedto = $objValue->fields['assigned_to'];
            $description= $objValue->fields['description'];
            $taskAutoId = $objValue->fields['task_id'];
        }
        $this->_getResourceDropDown('Members', $assignedto, $settings['emp_default_user_group']);
        $this->taskTypeDropDown($objtpl, $type);

        if (!empty($customer)) {
            // Get customer Name
            $objCustomer = $objDatabase->Execute("SELECT customer_name, contact_familyname  FROM `".DBPREFIX."module_crm_contacts` WHERE id = {$customer}");
            $customerName = $objCustomer->fields['customer_name']." ".$objCustomer->fields['contact_familyname'];
        }
        $objtpl->setVariable(array(
                'CRM_LOGGED_USER_ID'    => $objFWUser->objUser->getId(),
                'CRM_TASK_AUTOID'       => !empty($taskAutoId) ? contrexx_raw2xhtml($taskAutoId) : '',
                'CRM_TASK_ID'           => (int) $id,
                'CRM_TASKTITLE'         => contrexx_raw2xhtml($title),
                'CRM_DUE_DATE'          => contrexx_raw2xhtml($duedate),
                'CRM_CUSTOMER_ID'       => intval($customer),
                'CRM_CUSTOMER_NAME'     => !empty($customerName) ? contrexx_raw2xhtml($customerName) : '',
                'CRM_TASK_DESC'         => new \Cx\Core\Wysiwyg\Wysiwyg('description', contrexx_raw2xhtml($description)),
                'CRM_BACK_LINK'         => base64_decode($redirect),

                'TXT_CRM_ADD_TASK'             => empty($id)? $_ARRAYLANG['TXT_CRM_ADD_TASK'] : $_ARRAYLANG['TXT_CRM_EDITTASK'],
                'TXT_CRM_TASK_ID'              => $_ARRAYLANG['TXT_CRM_TASK_ID'],
                'TXT_CRM_TASK_TITLE'           => $_ARRAYLANG['TXT_CRM_TASK_TITLE'],
                'TXT_CRM_TASK_TYPE'            => $_ARRAYLANG['TXT_CRM_TASK_TYPE'],
                'TXT_CRM_SELECT_TASK_TYPE'     => $_ARRAYLANG['TXT_CRM_SELECT_TASK_TYPE'],
                'TXT_CRM_CUSTOMER_NAME'        => $_ARRAYLANG['TXT_CRM_CUSTOMER_NAME'],
                'TXT_CRM_TASK_DUE_DATE'        => $_ARRAYLANG['TXT_CRM_TASK_DUE_DATE'],
                'TXT_CRM_TASK_RESPONSIBLE'     => $_ARRAYLANG['TXT_CRM_TASK_RESPONSIBLE'],
                'TXT_CRM_SELECT_MEMBER_NAME'   => $_ARRAYLANG['TXT_CRM_SELECT_MEMBER_NAME'],
                'TXT_CRM_OVERVIEW'             => $_ARRAYLANG['TXT_CRM_OVERVIEW'],
                'TXT_CRM_TASK_DESCRIPTION'     => $_ARRAYLANG['TXT_CRM_TASK_DESCRIPTION'],
                'TXT_CRM_FIND_COMPANY_BY_NAME' => $_ARRAYLANG['TXT_CRM_FIND_COMPANY_BY_NAME'],
                'TXT_CRM_SAVE'                 => $_ARRAYLANG['TXT_CRM_SAVE'],
                'TXT_CRM_BACK'                 => $_ARRAYLANG['TXT_CRM_BACK'],
                'TXT_CRM_NOTIFY'               => $_ARRAYLANG['TXT_CRM_NOTIFY'],
                'TXT_CRM_MANDATORY_FIELDS_NOT_FILLED_OUT' => $_ARRAYLANG['TXT_CRM_MANDATORY_FIELDS_NOT_FILLED_OUT'],

        ));
    }

    /**
     * used to change task status if valid
     *
     * @return json status about the task
     */
    public function changeTaskStatus()
    {
        global $_ARRAYLANG, $objDatabase;
        $json = array();

        $id     = isset($_POST['id']) ? (int) $_POST['id'] : 0;
        $status = isset($_POST['status']) ? (int) $_POST['status'] : 0;
        $status = ($status == 1) ? 0 : 1;
        // check permission
        if (!empty($id)) {
            $objResult = $objDatabase->Execute("SELECT `added_by`,
                                                       `assigned_to`
                                                    FROM `".DBPREFIX."module_{$this->moduleNameLC}_task`
                                                 WHERE `id` = '$id'
                                               ");
            if ($objResult) {
                list($task_edit_permission, $task_delete_permission, $task_status_update_permission) = $this->getTaskPermission((int) $objResult->fields['added_by'], (int) $objResult->fields['assigned_to']);
                if (!$task_status_update_permission) {
                    $json['error'] = $_ARRAYLANG['TXT_CRM_NO_ACCESS_TO_STATUS'];
                } else {
                    $query = $objDatabase->Execute("UPDATE ".DBPREFIX."module_".$this->moduleNameLC."_task
                                                        SET task_status  = '$status'
                                                       WHERE id      = '$id'");
                    $json['success'] = 1;
                    $json['status_img'] = $status ? '/core/Core/View/Media/icons/led_green.gif' : '/core/Core/View/Media/icons/led_red.gif';
                    $json['status_msg'] = sprintf($_ARRAYLANG['TXT_CRM_TASK_STATUS_CHANGED'], ($status ? $_ARRAYLANG['TXT_CRM_TASK_COMPLETED'] : $_ARRAYLANG['TXT_CRM_TASK_OPEN']));

                    $query = "SELECT
                               task_status,
                               due_date
                            FROM ".DBPREFIX."module_{$this->moduleNameLC}_task
                            WHERE id = $id";

                    $objResult = $objDatabase->Execute($query);

                    $now = strtotime('now');
                    if ($objResult) {
                        $json['task'] = array(
                            'taskStatus'   => (int) $objResult->fields['task_status'],
                            'taskClass'    => $objResult->fields['task_status'] == 1 || strtotime($objResult->fields['due_date']) > $now ? '' : 'task_expired',
                        );
                    }
                }
            }
        }

        echo json_encode($json);
    }

    /**
     * delete the task
     *
     * @global array $_ARRAYLANG
     * @global object $objDatabase
     * @return true
     */
    function deleteTask()
    {
        global $objDatabase;

        $id       = isset($_GET['id']) ? (int) $_GET['id'] : 0;
        $redirect = isset($_REQUEST['redirect']) ? $_REQUEST['redirect'] : base64_encode(\Cx\Core\Routing\Url::fromBackend($this->moduleName, 'task'));

        if (!empty($id)) {
            $objResult = $objDatabase->Execute("SELECT `added_by`,
                                                       `assigned_to`
                                                    FROM `".DBPREFIX."module_{$this->moduleNameLC}_task`
                                                 WHERE `id` = '$id'
                                               ");
            if ($objResult) {
                list($task_edit_permission, $task_delete_permission) = $this->getTaskPermission((int) $objResult->fields['added_by'], (int) $objResult->fields['assigned_to']);
                if (!$task_delete_permission) {
                    \Permission::noAccess();
                }
            }
        }

        if (!empty($id)) {
            $objResult = $objDatabase->Execute("DELETE FROM ".DBPREFIX."module_{$this->moduleNameLC}_task WHERE id = '$id'");
            $redirect = \Cx\Core\Routing\Url::fromMagic(base64_decode($redirect));
            $redirect->setParam('mes', base64_encode('taskDeleted'));
            \Cx\Core\Csrf\Controller\Csrf::redirect($redirect);
        }
    }
}
