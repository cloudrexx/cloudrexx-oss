<?php

/**
 * Cloudrexx
 *
 * @link      https://www.cloudrexx.com
 * @copyright Cloudrexx AG
 *
 * This file is part of Cloudrexx.
 *
 * Cloudrexx is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3 of the License.
 *
 * Cloudrexx is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

$_ARRAYLANG['TXT_CRM'] = 'GRC';
$_ARRAYLANG['TXT_CRM_CUSTOMERS']            = "Gestion de la relation client";
$_ARRAYLANG['TXT_CRM_CUSTOMER']             = "Clients";
$_ARRAYLANG['TXT_CRM_CUSTOMER_MEMBERSHIP']  = "Groupes de client";
$_ARRAYLANG['TXT_CRM_TASKS']                = "Tâche";
$_ARRAYLANG['TXT_CRM_OPPORTUNITY']          = "Opportunité";
