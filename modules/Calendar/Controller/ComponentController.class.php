<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * Main controller for Calendar
 *
 * @copyright   Cloudrexx AG
 * @author      Project Team SS4U <info@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  module_calendar
 */

namespace Cx\Modules\Calendar\Controller;

/**
 * Main controller for Calendar
 *
 * @copyright   Cloudrexx AG
 * @author      Project Team SS4U <info@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  module_calendar
 */
class ComponentController extends \Cx\Core\Core\Model\Entity\SystemComponentController {

    /**
     * Returns all Controller class names for this component (except this)
     *
     * Be sure to return all your controller classes if you add your own
     * @return array List of Controller class names (without namespace)
     */
    public function getControllerClasses()
    {
        return array('EsiWidget', 'JsonCalendar');
    }

    /**
     * Returns a list of JsonAdapter class names
     *
     * The array values might be a class name without namespace. In that case
     * the namespace \Cx\{component_type}\{component_name}\Controller is used.
     * If the array value starts with a backslash, no namespace is added.
     *
     * Avoid calculation of anything, just return an array!
     * @return array List of ComponentController classes
     */
    public function getControllersAccessableByJson()
    {
        return array('EsiWidgetController', 'JsonCalendarController');
    }

    /**
     * @inheritdoc
     */
    public function adjustResponse(
        \Cx\Core\Routing\Model\Entity\Response $response
    ) {
        $headers = $response->getRequest()->getHeaders();
        if (isset($headers['Referer'])) {
            $refUrl = new \Cx\Lib\Net\Model\Entity\Url($headers['Referer']);
        } else {
            $refUrl = new \Cx\Lib\Net\Model\Entity\Url($response->getRequest()->getUrl()->toString());
        }
        try {
            $canonicalUrl = $this->getCanonicalUrl($refUrl, $response->getPage());
            $response->setHeader(
                'Link',
                '<' . $canonicalUrl->toString() . '>; rel="canonical"'
            );
        } catch (\Exception $e) {
            return;
        }
    }

    /**
     * Get the canonical URL for the current request
     *
     * @param \Cx\Lib\Model\Entity\Url $requestUrl URL of current request
     * @param \Cx\Core\ContentManager\Model\Entity\Page $page Resolved page of current request
     * @return \Cx\Core\Routing\Url Canonical URL for the current request
     */
    protected function getCanonicalUrl(
        \Cx\Lib\Net\Model\Entity\Url $requestUrl,
        \Cx\Core\ContentManager\Model\Entity\Page $page
    ): \Cx\Core\Routing\Url {
        // export-urls must not have a canonical url as they should not get indexed
        if ($requestUrl->hasParam('export')) {
            throw new \Exception('Don\'t set canonical URL for export');
        }

        $supportedParamsByCmd = [
            '' => [
                'params' => [
                    'catid',
                    'pos',
                    'search',
                    'term',
                    'from', // has precedence over year, month, day
                    'year',
                    'month',
                    'day',
                    'till', // has precedence over endDay, endMonth, endYear
                    'endDay',
                    'endMonth',
                    'endYear',
                ],
            ],
            'category' => ['alias' => ''],
            'my_events' => ['alias' => ''],
            'list' => ['alias' => ''],
            'eventlist' => ['alias' => ''],
            'archive' => ['alias' => ''],
            'detail' => [
                'params' => [
                    'date',
                    'id',
                ],
            ],
            'registrations' => ['alias' => 'detail'],
            'register' => [
                'params' => [
                    'date',
                    'id',
                    'i',
                    't',
                    'r',
                ],
            ],
            'sign' => ['alias' => 'register'],
            'boxes' => [
                'params' => [
                    'act',
                    'catid', // only if 'act=list'
                    'yearID',
                    'monthID',
                    'dayID', // optional and only if 'act=list'
                    'search',
                    'term',
                ],
            ],
            'add' => [
                'params' => [
                ],
            ],
            'edit' => [
                'params' => [
                    'id',
                ],
            ],
        ];

        $cmd = $page->getCmd();
        if (preg_match('/^(detail)(\d+)$/', $cmd, $match)) {
            $cmd = $match[1];
        } elseif (!empty($cmd) && (string)intval($cmd) == $cmd) {
            $cmd = 'category';
        }

        $supportedParams = $supportedParamsByCmd[$cmd] ?? [];
        if (isset($supportedParams['alias'])) {
            $supportedParams = $supportedParamsByCmd[$supportedParams['alias']] ?? [];
        }
        if (!isset($supportedParams['params'])) {
            return \Cx\Core\Routing\Url::fromPage($page);
        }
        // filter out all non-relevant URL arguments
        $params = array_filter(
            $requestUrl->getParamArray(),
            function($key) use ($supportedParams, $requestUrl) {
                if ($key == 'pos' && in_array($key, $supportedParams['params'])) {
                    return !empty($requestUrl->getParam($key));
                }
                return in_array($key, $supportedParams['params']);
            },
            \ARRAY_FILTER_USE_KEY
        );

        // note: as it's not good practice to index overview listings we do not
        // even try to make a best assumption of which overview section might be
        // the best to be indexed and reference all other overview sections to
        // that one to prevent duplicate content issues. Instead instruct users
        // to not enable indexing on those pages.

        if (
            !isset($params['id'])
            || !isset($params['date'])
        ) {
            return \Cx\Core\Routing\Url::fromPage($page, $params);
        }

        // always set canonical for event pages to the current event
        $id = intval($params['id']);
        $date = intval($params['date']);
        $event = new \Cx\Modules\Calendar\Controller\CalendarEvent();
        $event->get($id);
        if (empty($event->id)) {
            return \Cx\Core\Routing\Url::fromPage($page);
        }
        if (
            $event->seriesStatus
             && $event->independentSeries
        ) {
            $startDate = new \DateTime();
            $endDate = new \DateTime();
            $endDate->modify('+10 years');
            $eventManager = new CalendarEventManager($startDate, $endDate);
            $event = new \Cx\Modules\Calendar\Controller\CalendarEvent($id);
            if ($eventManager->_addToEventList($event)) {
                $eventManager->eventList[] = $event;
            }
            $additionalRecurrences = $event->seriesData['seriesAdditionalRecurrences'];
            $eventManager->generateRecurrencesOfEvent($event, $additionalRecurrences);
            $event = $eventManager->eventList[0];
        }
        if ($event) {
            $params['date'] = $event->startDate->getTimestamp();
        } else {
            unset($params['id']);
            unset($params['date']);
        }
        return \Cx\Core\Routing\Url::fromPage($page, $params);
    }

    /**
     * Load your component.
     *
     * @param \Cx\Core\ContentManager\Model\Entity\Page $page       The resolved page
     */
    public function load(\Cx\Core\ContentManager\Model\Entity\Page $page) {
        global $_CORELANG, $subMenuTitle, $objTemplate;
        switch ($this->cx->getMode()) {
            case \Cx\Core\Core\Controller\Cx::MODE_FRONTEND:
                $objCalendar = new \Cx\Modules\Calendar\Controller\Calendar($page->getContent());
                $page->setContent($objCalendar->getCalendarPage($page));
                if ($objCalendar->pageTitle) {
                    $page->setTitle($objCalendar->pageTitle);
                    $page->setContentTitle($objCalendar->pageTitle);
                    $page->setMetaTitle($objCalendar->pageTitle);
                }
                break;

            case \Cx\Core\Core\Controller\Cx::MODE_BACKEND:
                $this->cx->getTemplate()->addBlockfile('CONTENT_OUTPUT', 'content_master', 'ContentMaster.html');
                $objTemplate = $this->cx->getTemplate();
                \Permission::checkAccess(16, 'static');
                $subMenuTitle = $_CORELANG['TXT_CALENDAR'];
                $objCalendarManager = new \Cx\Modules\Calendar\Controller\CalendarManager();
                $objCalendarManager->getCalendarPage();
                break;

            default:
                break;
        }
    }

    /**
     * Do something after system initialization
     *
     * USE CAREFULLY, DO NOT DO ANYTHING COSTLY HERE!
     * CALCULATE YOUR STUFF AS LATE AS POSSIBLE.
     * This event must be registered in the postInit-Hook definition
     * file config/postInitHooks.yml.
     * @param \Cx\Core\Core\Controller\Cx   $cx The instance of \Cx\Core\Core\Controller\Cx
     */
    public function postInit(\Cx\Core\Core\Controller\Cx $cx)
    {
        global $_ARRAYLANG;

        if ($cx->getMode() == \Cx\Core\Core\Controller\Cx::MODE_COMMAND &&
            isset($_GET['lang'])
        ) {
            // hotfix: this is required so that the widgets to be parsed have the required
            //         language variables available
            $langId = \FWLanguage::getLanguageIdByCode($_GET['lang']);

            if (!isset($_ARRAYLANG)) {
                $_ARRAYLANG = array();
            }
            $_ARRAYLANG = array_merge(
                $_ARRAYLANG,
                \Env::get('init')->getComponentSpecificLanguageData($this->getName(), true, $langId)
            );
        }

        // Get Calendar Events
        $widgetController = $this->getComponent('Widget');
        foreach (CalendarLibrary::getHeadlinePlaceholders() as $widgetName) {
            $widget = new \Cx\Core_Modules\Widget\Model\Entity\EsiWidget(
                $this,
                $widgetName
            );
            $widget->setEsiVariable(
                \Cx\Core_Modules\Widget\Model\Entity\EsiWidget::ESI_VAR_ID_THEME |
                \Cx\Core_Modules\Widget\Model\Entity\EsiWidget::ESI_VAR_ID_CHANNEL
            );
            $widgetController->registerWidget(
                $widget
            );
        }

        // hotfix: We must reset the calendar settings as they have been initialized
        //         with the wrong language in frontend and backend mode.
        //         The reason is that in postInit the front or backend language
        //         has not yet been initialized
        // TODO: is this still required? the widget initialization does no longer
        //       initialize CalendarLibrary()
        if ($cx->getMode() == \Cx\Core\Core\Controller\Cx::MODE_FRONTEND) {
            CalendarLibrary::$settings = array();
        }
    }

    /**
     * Register your event listeners here
     *
     * USE CAREFULLY, DO NOT DO ANYTHING COSTLY HERE!
     * CALCULATE YOUR STUFF AS LATE AS POSSIBLE.
     * Keep in mind, that you can also register your events later.
     * Do not do anything else here than initializing your event listeners and
     * list statements like
     * $this->cx->getEvents()->addEventListener($eventName, $listener);
     */
    public function registerEventListeners() {
        $eventListener = new \Cx\Modules\Calendar\Model\Event\CalendarEventListener($this->cx);
        $this->cx->getEvents()->addEventListener('SearchFindContent', $eventListener);
        $this->cx->getEvents()->addEventListener('mediasource.load', $eventListener);

        foreach ($this->getEntityClasses() as $entityClassName) {
            $this->cx->getEvents()->addModelListener(\Doctrine\ORM\Events::onFlush, $entityClassName, $eventListener);
        }
   }
}
