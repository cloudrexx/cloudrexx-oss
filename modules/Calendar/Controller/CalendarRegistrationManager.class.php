<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * Calendar
 *
 * @package    cloudrexx
 * @subpackage module_calendar
 * @author     Cloudrexx <info@cloudrexx.com>
 * @copyright  CLOUDREXX CMS - CLOUDREXX AG
 * @version    1.00
 */

namespace Cx\Modules\Calendar\Controller;
/**
 * Calendar Class Registration manager
 *
 * @package    cloudrexx
 * @subpackage module_calendar
 * @author     Cloudrexx <info@cloudrexx.com>
 * @copyright  CLOUDREXX CMS - CLOUDREXX AG
 * @version    1.00
 */
class CalendarRegistrationManager extends CalendarLibrary
{
    /**
     * Event id
     *
     * @access private
     * @var integer
     */
    private $eventId;

    /**
     * Form id
     *
     * @access private
     * @var integer
     */
    private $formId;

    /**
     * Get Registration
     *
     * @access private
     * @var boolean
     */
    private $getRegistrations;

    /**
     * Get deregistration
     *
     * @access private
     * @var boolean
     */
    private $getDeregistrations;

    /**
     * Get waitlist
     *
     * @access private
     * @var boolean
     */
    private $getWaitlist;

    /**
     * Registration list
     *
     * @access public
     * @var array
     */
    public $registrationList = array();

    /**
     * Startdate filter
     *
     * @var integer
     */
    public $startDate;

    /**
     * Enddate filter
     *
     * @var integer
     */
    public $endDate;

    /**
     * Set true when registration manager is loaded to parse the current view
     *
     * @var boolean
     */
    public $defaultView;

    /**
     * Calendar Event instance
     *
     * @var \Cx\Modules\Calendar\Controller\CalendarEvent
     */
    public $event;

    /**
     * Registration manager constructor
     *
     * Loads the form object by loading the calendarEvent object
     *
     * @param \Cx\Modules\Calendar\Controller\CalendarEvent     $event                Event id
     * @param boolean                                           $getRegistrations     condition to check whether we need the
     *                                                                                  registrations
     * @param boolean                                           $getDeregistrations   condition to check whether we need the
     *                                                                                  deregistrations
     * @param boolean                                           $getWaitlist          condition to check whether we need the
     *                                                                                  waitlist
     * @param integer                                           $startDate            Startdate filter
     * @param integer                                           $endDate              Enddate filter
     * @param boolean                                           $isDefaultView        Is registration manager is loaded to parse the current view
     */
    function __construct(CalendarEvent $event, $getRegistrations = true, $getDeregistrations = false, $getWaitlist = false, $startDate = null, $endDate = null, $isDefaultView = false)
    {
        $this->event              = $event;
        $this->eventId            = intval($event->id);
        $this->getRegistrations   = $getRegistrations;
        $this->getDeregistrations = $getDeregistrations;
        $this->getWaitlist        = $getWaitlist;
        $this->startDate          = $startDate;
        $this->endDate            = $endDate;
        $this->defaultView        = $isDefaultView;
        $this->formId             = $this->event->registrationForm;
    }

    /**
     * Initialize the registration list
     *
     * @return null
     */
    function getRegistrationList()
    {
        global $objDatabase;

        $blnFirst = true;
        $arrWhere = array();
        if ($this->getRegistrations)   { $arrWhere[] = CalendarRegistration::REGISTRATION_TYPE_REGISTRATION; }
        if ($this->getDeregistrations) { $arrWhere[] = CalendarRegistration::REGISTRATION_TYPE_CANCELLATION; }
        if ($this->getWaitlist)        { $arrWhere[] = CalendarRegistration::REGISTRATION_TYPE_WAITLIST; }
        $strWhere = ' AND (';
        foreach ($arrWhere as $value) {
            $strWhere .=  $blnFirst ? '`type` = '.$value : ' OR `type` = '.$value;
            $blnFirst = false;
        }
        $strWhere .= ')';

        if ($this->startDate && $this->endDate) {
            $strWhere .= ' AND (date >= '. contrexx_input2int($this->startDate) .' AND date <= '. contrexx_input2int($this->endDate) .')';
        } elseif ($this->startDate) {
            $strWhere .= ' AND (date >= '. contrexx_input2int($this->startDate) .')';
        } elseif ($this->endDate) {
            $strWhere .= ' AND (date <= '. contrexx_input2int($this->startDate) .')';
        }

        $query = '
            SELECT `id`
            FROM `'.DBPREFIX.'module_'.$this->moduleTablePrefix.'_registration`
            WHERE `event_id` = '.$this->eventId.'
            '.$strWhere.'
            ORDER BY `id` DESC'
        ;
        $objResult = $objDatabase->Execute($query);

        if ($objResult !== false) {
            while (!$objResult->EOF) {
                $objRegistration = new \Cx\Modules\Calendar\Controller\CalendarRegistration($this->formId, intval($objResult->fields['id']));
                $this->registrationList[$objResult->fields['id']] = $objRegistration;
                $objResult->MoveNext();
            }
        }
    }

    /**
     * @return  array   List of array elements that represent a field of the
     *     currently loaded form ({@see static::$formId}). The element's index
     *     represent each field's ID. The structure of an element field is as follows:
     *     [
     *         'id'         => id of field
     *         'name'       => name of field
     *         'defaults'   => [
     *               default values
     *         ]
     *     ]
     */
    protected function fetchRegistrationListFields(): array {
        $query = '
            SELECT
                `formField`.`id`,
                (
                    SELECT `fieldName`.`name`
                    FROM `'.DBPREFIX.'module_'.$this->moduleTablePrefix.'_registration_form_field_name` AS `fieldName`
                    WHERE `fieldName`.`field_id` = `formField`.`id` AND `fieldName`.`form_id` = `formField`.`form`
                    ORDER BY CASE `fieldName`.`lang_id`
                                WHEN '.FRONTEND_LANG_ID.' THEN 1
                                ELSE 2
                                END
                    LIMIT 1
                ) AS `name`,
                (
                    SELECT `fieldDefault`.`default`
                    FROM `'.DBPREFIX.'module_'.$this->moduleTablePrefix.'_registration_form_field_name` AS `fieldDefault`
                    WHERE `fieldDefault`.`field_id` = `formField`.`id` AND `fieldDefault`.`form_id` = `formField`.`form`
                    ORDER BY CASE `fieldDefault`.`lang_id`
                                WHEN '.FRONTEND_LANG_ID.' THEN 1
                                ELSE 2
                                END
                    LIMIT 1
                ) AS `default`,
                `formField`.`type`
            FROM `'.DBPREFIX.'module_'.$this->moduleTablePrefix.'_registration_form_field` AS `formField`
            WHERE `formField`.`form` = '.$this->formId.'
            ORDER BY `formField`.`order`
        ';
        $cx = \Cx\Core\Core\Controller\Cx::instanciate();
        $objResult = $cx->getDb()->getAdoDb()->Execute($query);
        if ($objResult === false) {
            return [];
        }
        $fields = [];
        while (!$objResult->EOF) {
            if (
                in_array(
                    $objResult->fields['type'],
                    [
                        'agb',
                        'fieldset'
                    ]
                )
            ) {
                $objResult->MoveNext();
                continue;
            }
            $fields[$objResult->fields['id']] = [
                'id'        => $objResult->fields['id'],
                'name'      => $objResult->fields['name'],
                'defaults'  => explode(
                    ',',
                    $objResult->fields['default'] ?? ''
                ),
            ];
            $objResult->MoveNext();
        }
        return $fields;
    }

    /**
     * Parse the labels of the form (identified by {@see static::$formId}) of
     * which the registations have been parsed of.
     *
     * @param   \Cx\Core\Html\Sigma $template
     *     Template instance to be used for parsing the registration list(s) labels.
     * @param   array   $fields List of form fields for associated event.
     *     (see {@see static::fetchRegistrationListFields()})
     * @param   string  $typePrefix Prefix to be used on template blocks and
     *     placeholders for parsing the labels.
     */
    protected function parseRegistrationListLabels(\Cx\Core\Html\Sigma $template, array $fields, string $typePrefix = ''): void {
        global $_ARRAYLANG;

        $block = 'calendar_event_registration_list_' . $typePrefix . 'field_labels';
        $placeholder = $this->moduleLangVar.'_EVENT_REGISTRATION_LIST_' . strtoupper($typePrefix) . 'FIELD_LABEL';

        $cx = \Cx\Core\Core\Controller\Cx::instanciate();
        if ($cx->isBackendMode()) {
            $template->setVariable($placeholder, '#');
            $template->parse($block);

            $dateFilterTpl = new \Cx\Core\Html\Sigma(
                $cx->getCodeBaseModulePath() . '/' . $this->moduleName . '/View/Template/Backend'
            );
            $dateFilterTpl->loadTemplateFile('module_calendar_registration_date_filter.html');
            $dateFilterTpl->setVariable('TXT_CALENDAR_DATE', $_ARRAYLANG['TXT_CALENDAR_DATE']);
            $eventStats         = $this->getEventStats();
            $selectedDateFilter = $this->defaultView && isset($_GET['date']) ? contrexx_input2raw($_GET['date']) : '';
            $this->parseEventRegistrationStats($dateFilterTpl, $eventStats, $selectedDateFilter);
            $template->setVariable($placeholder, $dateFilterTpl->get());
            $template->parse($block);

            $template->setVariable($placeholder, $_ARRAYLANG['TXT_CALENDAR_EVENT_REGISTRATION_SUBMISSION']);
            $template->parse($block);
        }

        foreach ($fields as $field) {
            $template->setVariable($placeholder, contrexx_raw2xhtml($field['name']));
            $template->parse($block);
        }

        if (!$cx->isBackendMode()) {
            return;
        }

        $template->setVariable(array(
            $placeholder  => $_ARRAYLANG['TXT_CALENDAR_ACTION'],
        ));
        $template->parse($block);
    }

    /**
     * Parse the registrations of th event identified by {@see static::$eventId}
     * and optional (if set) {@see static::$startDate} and {@see static::$endDate}.
     *
     * @param \Cx\Core\Html\Sigma   $objTpl
     *     Template instance to be used for parsing the registration list(s).
     * @param string $tpl   Type of registrations to list. One of:
     *     - r: Sign-ons
     *     - d: Sign-offs
     *     - w: Waitlist
     *     Leave empty to list all types of registrations.
     */
    public function showRegistrationList(\Cx\Core\Html\Sigma $objTpl, string $tpl = ''): void {
        global $_ARRAYLANG;

        $blocks = $this->fetchTemplateBlockSetup($objTpl);
        if (!$blocks) {
            return;
        }
        $fields = $this->fetchRegistrationListFields();
        if (empty($this->registrationList)) {
            foreach ($blocks as $type => $block) {
                if (isset($block['name'])) {
                    $objTpl->hideBlock($block['name']);
                }
                if (!isset($block['empty'])) {
                    continue;
                }
                $objTpl->touchBlock($block['empty']);
            }
            return;
        }
        $objTpl->hideBlock('calendar_event_registration_list_empty');
        $data = $this->fetchRegistrationData($fields);
        $cx = \Cx\Core\Core\Controller\Cx::instanciate();

        if ($cx->isBackendMode()) {
            // parse the event date with or without the time information
            // depending on the fact if the event is an all-day event or not
            $format = $this->getDateFormat();
            if (!$this->event->all_day) {
                $format .= ' H:i';
            }
            $i = 0;
        }
        $userAttributes = new \User_Profile_Attribute();
        foreach ($this->registrationList as $objRegistration) {
            $entryBlockToParse = $this->getBlockToParseRegistrationList($objTpl, $objRegistration->type, $blocks);
            if (!$entryBlockToParse) {
                continue;
            }
            $fieldBlockToParse = $entryBlockToParse . '_field';
            $placeholder = strtoupper($fieldBlockToParse) . '_VALUE';
            if ($cx->isBackendMode()) {
                $checkbox = '<input type="checkbox" name="selectedRegistrationId[]" class="selectedRegistrationId" value="'.$objRegistration->id.'" />';
                $objTpl->setVariable($placeholder, $checkbox);
                $objTpl->parse($fieldBlockToParse);

                $objTpl->setVariable(
                    $placeholder,
                    date($format, $objRegistration->eventDate)
                );
                $objTpl->parse($fieldBlockToParse);

                $objTpl->setVariable(
                    $placeholder,
                    (($objRegistration->submissionDate instanceof \DateTime)
                        ? $this->format2userDateTime($objRegistration->submissionDate)
                        : ''
                    )
                );
                $objTpl->parse($fieldBlockToParse);
            }


            foreach ($fields as $field) {
                $suffix = '';
                if ($objTpl->blockExists($entryBlockToParse . '_field_' . $field['id'])) {
                    $suffix = '_FIELD_' . $field['id'];
                }
                $objTpl->setVariable(
                    strtoupper($fieldBlockToParse) . '_LABEL' . $suffix,
                    contrexx_raw2xhtml($field['name'])
                );
                $objTpl->setVariable(
                    $placeholder . $suffix,
                    isset($data[$objRegistration->id][$field['id']]) ? contrexx_raw2xhtml($data[$objRegistration->id][$field['id']]) : ''
                );
                $objTpl->parse($fieldBlockToParse . strtolower($suffix));
            }
            $userProfileBlock = $entryBlockToParse . '_user_';
            $user = null;
            if ($objRegistration->userId) {
                $user = \FWUser::getFWUserObject()->objUser->getUser(
                    $objRegistration->userId
                );
            }
            if ($cx->isFrontendMode()) {
                if ($user) {
                    $accessLib = new \Cx\Core_Modules\Access\Controller\AccessLib($objTpl);
                    $accessLib->setModulePrefix(strtoupper($userProfileBlock));
                    $accessLib->setAttributeNamePrefix($userProfileBlock . 'profile_attribute');
                }
                $userAttributes->first();
                while (!$userAttributes->EOF) {
                    $objAttribute = $userAttributes->getById($userAttributes->getId());
                    if ($user) {
                        $accessLib->parseAttribute($user, $objAttribute->getId(), 0, false, false, false, false, false);
                    } else {
                        $objTpl->hideBlock($userProfileBlock . 'profile_attribute_' . $objAttribute->getId());
                    }
                    $userAttributes->next();
                }
            }
            if ($cx->isBackendMode()) {
                $links = '
                    <a href="index.php?cmd='.$this->moduleName.'&amp;act=modify_registration&amp;tpl='.$tpl.'&amp;event_id='.$this->eventId.'&amp;rid='.$objRegistration->id.'" title="'.$_ARRAYLANG['TXT_CALENDAR_EDIT'].'"><i class="fa-regular fa-pen-to-square"></i></a>
                    <a class="delete_registration" href="index.php?cmd='. $this->moduleName .'&amp;act=event_registrations&amp;tpl='.$tpl.'&amp;id='.$this->eventId.'&amp;delete='.$objRegistration->id.'" title="'.$_ARRAYLANG['TXT_CALENDAR_DELETE'].'"><i class="fa-regular fa-trash-can"></i></a>
                    ';
                $objTpl->setVariable($placeholder, $links);
                $objTpl->parse($fieldBlockToParse);

                $objTpl->setVariable($this->moduleLangVar.'_REGISTRATION_ROW', $i % 2 == 0 ? 'row1' : 'row2');
                $i++;
            }
            $objTpl->setVariable(
                strtoupper($entryBlockToParse) . '_SUBMISSION_TIMESTAMP',
                (($objRegistration->submissionDate instanceof \DateTime)
                    ? $this->getUserDateTimeFromIntern($objRegistration->submissionDate)->getTimestamp()
                    : ''
                )
            );
            $objTpl->setVariable(
                strtoupper($entryBlockToParse) . '_TYPE',
                $objRegistration->type
            );
            $objTpl->setVariable(
                strtoupper($entryBlockToParse) . '_TYPE_LABEL',
                $objRegistration->getTypeLabel()
            );
            $objTpl->parse($entryBlockToParse);
        }

        $blockTouched = false;
        foreach ($blocks as $type => $block) {
            if ($block['parsed'] ?? false) {
                $this->parseRegistrationListLabels($objTpl, $fields, $block['prefix']);
                if (isset($block['empty'])) {
                    $objTpl->hideBlock($block['empty']);
                }
                $objTpl->touchBlock($block['name']);
                $blockTouched = true;
            } else {
                if (isset($block['name'])) {
                    $objTpl->hideBlock($block['name']);
                }
                if (
                    // Only touch type specific empty blocks here.
                    // The generic empty block will be touch below (see usage of
                    // $blockTouched), in case no other block has been touched.
                    $type
                    && isset($block['empty'])
                ) {
                    $objTpl->touchBlock($block['empty']);
                    $blockTouched = true;
                }
            }
        }
        if (
            !$blockTouched
            && isset($blocks['']['empty'])
        ) {
            $objTpl->touchBlock($blocks['']['empty']);
        }
    }

    /**
     * Identify the available template blocks to be used by
     * {@see static::showRegistrationList()} for parsing the registration list(s).
     *
     * @param   \Cx\Core\Html\Sigma   $template
     *     Template instance that is used by {@see static::showRegistrationList()}
     *     for parsing the registration list(s).
     * @return  array  The returned array is a two-dimensional array, where the
     *     first dimension represents the matching registration type
     *     (reg/signoff/waitlist) which may or may not be set depending on the
     *     existance of the associated template blocks in $template.
     *     The associated template blocks would have the following format:
     *     `<!-- BEGIN/END calendar_event_registration_list_<type> -->`
     *     If there is a template block without type association present
     *     (`<!-- BEGIN/END calendar_event_registration_list -->`) then
     *     that block information will be returned by index `''` (empty string).
     *     The format of the second dimension is as follows:
     *     ```
     *     [
     *         'name' => '<name-of-template-block>',
     *         'parsed'  => false,
     *         'prefix' => '<prefix>',
     *         'empty' => '<name-of-empty-template-block>',
     *     ]
     *     ```
     *     The keys have the following use:
     *     - `name` contains the name of the associated template block to be
     *         used by {@see static::showRegistrationList()} for parsing the
     *         associated registrations. I.e. `calendar_event_registration_list_reg`
     *         for type `reg`.
     *     - `parsed` is initially set to `false` and will be set to `true`
     *         by {@see static::getBlockToParseRegistrationList()} once the associated
     *         block (referenced by `name`) gets parsed. `parsed` will then be used by
     *         {@see static::showRegistrationList()} to determine if the block itself
     *         or its counterpart (-> `empty`-block) shall be touched or hidden.
     *     - `prefix` will be used by {@see static::parseRegistrationListLabels()} for
     *         parsing the type specific template blocks and placeholders. `<prefix>`
     *         is either set to an empty string (for `<!-- BEGIN/END calendar_event_registration_list -->`)
     *         or set to the elements index suffixed by an underscore. I.e. the `<prefix>`
     *         for type `reg` will be set to `reg_`.
     *     - `empty` contains the name of the associated template block to be
     *         used by {@see static::showRegistrationList()} to show if no registrations
     *         of the associated type have been parsed.
     *     Be aware that the key `empty` will only be set if the associated
     *     template block (i.e. calendar_event_registration_list_reg_empty) is
     *     present in the template.
     *     On the other hand, the keys `name`, `parsed` and `prefex` are also
     *     only set if the associated template (i.e. calendar_event_registration_list_reg)
     *     block is present in the template.
     */
    protected function fetchTemplateBlockSetup(\Cx\Core\Html\Sigma $template): array {
        $blocks = [];
        $prefix = 'calendar_event_registration_list';
        $regExp = '/^' . $prefix . '(?:|_([^_]+))(_empty)?$/';
        $listBlocks = preg_grep(
            $regExp,
            $template->getBlockList($template->getCurrentBlock())
        );
        foreach ($listBlocks as $block) {
            if (
                !preg_match(
                    $regExp,
                    $block,
                    $match
                )
            ) {
                continue;
            }
            $type = $match[1] ?? '';
            if (!isset($blocks[$type])) {
                $blocks[$type] = [];
            }
            if (isset($match[2])) {
                $blocks[$type]['empty'] = $match[0];
            } else {
                $blocks[$type]['name'] = $match[0];
                $blocks[$type]['parsed'] = false;
                $prefix = '';
                if ($type) {
                    $prefix = $type . '_';
                }
                $blocks[$type]['prefix'] = $prefix;
            }
        }
        return $blocks;
    }

    /**
     * Get the name of the template block from $template to be used to parse
     * a registration entry of type $type.
     * If a matching block is found, the associated entry in $blocks is incremented
     * by one. The latter will be used by {@see static::showRegistrationList()}
     * to determine which main blocks need to be touched or hidden.
     *
     * @param   \Cx\Core\Html\Sigma $template
     *     Template instance that is used by {@see static::showRegistrationList()} for parsing the registration list(s).
     * @param   int $type   One of:
     *     - {@see CalendarRegistration::REGISTRATION_TYPE_REGISTRATION}
     *     - {@see CalendarRegistration::REGISTRATION_TYPE_CANCELLATION}
     *     - {@see CalendarRegistration::REGISTRATION_TYPE_WAITLIST}
     * @param   array   $blocks List of available blocks to parse from $template.
     *     See {@see static::fetchTemplateBlockSetup()}.
     * @return  string  Name of template block to be used to parse the
     *     registration entry of type $type.
     */
    protected function getBlockToParseRegistrationList(\Cx\Core\Html\Sigma $template, int $type, array &$blocks): string {
        switch ($type) {
            case CalendarRegistration::REGISTRATION_TYPE_REGISTRATION:
                $key = 'reg';
                break;

            case CalendarRegistration::REGISTRATION_TYPE_CANCELLATION:
                $key = 'signoff';
                break;

            case CalendarRegistration::REGISTRATION_TYPE_WAITLIST:
                $key = 'waitlist';
                break;

            default:
                $key = '';
                break;
        }
        if (
            !isset($blocks[$key]['name'])
            && $key
        ) {
            $key = '';
        }
        if (!isset($blocks[$key]['name'])) {
            return '';
        }
        $blocks[$key]['parsed'] = true;
        return $blocks[$key]['name'] . '_entry';
    }

    /**
     * Fetch stored registration data for current event (identified
     * by {@see static::e$eventId}).
     *
     * @param   array   $fields List of form fields for associated event.
     *     (see {@see static::fetchRegistrationListFields()})
     * @return  array   List of registration data for the currently processed event
     *     (identified by {@see static::$eventId} and {@see static::$formId}.
     *     The format of the returned array is as follows:
     *     [
     *         <registration-id> => [
     *             <field-id> => '<submitted-data>',
     *             <field-id> => '<submitted-data>',
     *             ...
     *         ],
     *         ...
     *     ]
     */
    protected function fetchRegistrationData(array $fields): array {
        global $_ARRAYLANG;

        $cx = \Cx\Core\Core\Controller\Cx::instanciate();
        $where = '';
        if ($cx->isFrontendMode()) {
            $where = 'AND `r`.`date` >= '. contrexx_input2int($this->startDate) .' AND `r`.`date` <= '. contrexx_input2int($this->endDate);
        }
        $query = '
            SELECT `v`.`reg_id`, `v`.`field_id`, `v`.`value`, `f`.`type`
            FROM `'.DBPREFIX.'module_'.$this->moduleTablePrefix.'_registration_form_field_value` AS `v`
            INNER JOIN `'.DBPREFIX.'module_'.$this->moduleTablePrefix.'_registration_form_field` AS `f`
            ON `v`.`field_id` = `f`.`id`
            INNER JOIN `'.DBPREFIX.'module_'.$this->moduleTablePrefix.'_registration` AS `r`
            ON `r`.`id` = `v`.`reg_id`
            WHERE `f`.`form` = '.$this->formId.'
            AND `r`.`event_id` = ' . $this->eventId . '
            ' . $where . '
            ORDER BY `f`.`order`
        ';
        $objResult = $cx->getDb()->getAdoDb()->Execute($query);
        if ($objResult === false) {
            return [];
        }
        $arrValues = array();
        while (!$objResult->EOF) {
            if (in_array($objResult->fields['type'], array('agb', 'fieldset'))) {
                $objResult->MoveNext();
                continue;
            }

            $options = $fields[$objResult->fields['field_id']]['defaults'];
            $value   = '';

            switch ($objResult->fields['type']) {
                case 'firstname':
                case 'lastname':
                case 'inputtext':
                case 'textarea':
                case 'mail':
                // case 'selectBillingAddress':
                    $value = $objResult->fields['value'];
                    break;
                case 'salutation':
                case 'seating':
                case 'select':
                    // abort in case an invalid selection is stored.
                    // note: indices start at 1. Index of 0 is invalid
                    if (!$objResult->fields['value']) {
                        break;
                    }
                    // validate selected option
                    $optionIdx = intval($objResult->fields['value']) - 1;
                    if (isset($options[$optionIdx])) {
                        $value = $options[$optionIdx];
                    }
                    break;
                case 'radio':
                case 'checkbox':
                    $output   = array();
                    $input    = '';
                    foreach (explode(',', $objResult->fields['value']) as $value) {
                        $input = '';

                        // extract data from input field (in case the magic [[INPUT]] function was used
                        if (preg_match('/^([^\[]*)(?:\[\[([^\]]*)\]\])?$/', $value, $match)) {
                            $value = $match[1];
                            if (isset($match[2])) {
                                $input = $match[2];
                            }
                        }

                        $value = (int) $value;

                        // fetch label of selected option
                        $optionIdx = $value - 1;
                        $label = '';
                        if (isset($options[$optionIdx])) {
                            $label = current(explode('[[', $options[$optionIdx]));
                        }

                        // fetch value of selected option (based on selection and magic [[INPUT]] field)
                        if (!empty($input)) {
                            $output[]  = $label . ': ' . $input;
                        } else {
                            if ($label == '') {
                                $label = $value == 1 ? $_ARRAYLANG['TXT_CALENDAR_YES'] : $_ARRAYLANG['TXT_CALENDAR_NO'];
                            }

                            $output[] = $label;
                        }
                        $value = implode(', ', $output);
                    }
                    break;
            }

            $arrValues[$objResult->fields['reg_id']][$objResult->fields['field_id']] = $value;
            $objResult->MoveNext();
        }

        return $arrValues;
    }

    /**
     * Parse the Event registration stats dropdown to filter the registration
     *
     * @todo    This method's code is magic. Trying to document it is pointless.
     *          It makes more sense to refactor the whole method instead.
     * @param \Cx\Core\Html\Sigma   $objTpl      Template instance
     * @param array                 $eventStats  Array of event stats
     * @param string                $selected    Selected option
     * @param string                $parent      Parent level
     * @param integer               $level       Current level of parsing,
     *                                           It is used to define the
     *                                           indent of the dropdown entry
     * @param integer               $depth       Current stats level of parsing.
     *                                           It is used to determine the
     *                                           type of the stats.
     *                                           Depth 1: Year value
     *                                                 2: Month
     *                                                 3: Day
     *                                                 4: Time
     * @param string                $blockName   Block name to parse
     */
    public function parseEventRegistrationStats(
        \Cx\Core\Html\Sigma $objTpl,
        $eventStats = array(),
        $selected = '',
        $parent = '',
        $level = 1,
        $depth = 1,
        $blockName = 'calendar_registration_date',
        $labelStack = array()
    ) {
        foreach ($eventStats as $key => $value) {
            $optionValue = empty($parent) ? str_pad($key, 2, '0', STR_PAD_LEFT) : $parent . '-' . str_pad($key, 2, '0', STR_PAD_LEFT);
            if (
                count($eventStats) == 1 &&
                is_array($value)
            ) {
                array_unshift(
                    $labelStack,
                    $this->fetchStatsLabelByLevel($depth, $key, $value, true)
                );
                $this->parseEventRegistrationStats(
                    $objTpl,
                    $value,
                    $selected,
                    $optionValue,
                    $level, // level is not increased intentionally
                    $depth + 1,
                    $blockName,
                    $labelStack
                );
                return;
            }

            if (
                is_array($value) &&
                count($value) == 1
            ) {
                $stack = array_merge(
                    array($this->fetchStatsLabelByLevel($depth, $key, $value, true)),
                    $labelStack
                );
                $this->parseEventRegistrationStats(
                    $objTpl,
                    $value,
                    $selected,
                    $optionValue,
                    $level,
                    $depth + 1,
                    $blockName,
                    $stack
                );
                continue;
            }

            if (is_array($value)) {
                $stack = array_merge(
                    array($this->fetchStatsLabelByLevel($depth, $key, $value)),
                    $labelStack
                );
            } elseif ($depth == 3) {
                $stack = array_merge(
                    array($key . '.'),
                    $labelStack
                );
                $stack = array_merge(
                    $stack,
                    array(' (' . $value . ')')
                );
            } else {
                $stack = array_merge(
                    $labelStack,
                    array($this->fetchStatsLabelByLevel($depth, $key, $value))
                );
            }
            $optionName = join(' ', $stack);

            $objTpl->setVariable(array(
                $this->moduleLangVar . '_REGISTRATION_FILTER_DATE_INTENT'   => str_repeat('&nbsp;', ($level - 1) * 2),
                $this->moduleLangVar . '_REGISTRATION_FILTER_DATE_NAME'     => $optionName,
                $this->moduleLangVar . '_REGISTRATION_FILTER_DATE_KEY'      => $optionValue,
                $this->moduleLangVar . '_REGISTRATION_FILTER_DATE_SELECTED' => ($selected == $optionValue) ? 'selected="selected"' : '',
            ));
            $objTpl->parse($blockName);

            if (is_array($value)) {
                $this->parseEventRegistrationStats(
                    $objTpl,
                    $value,
                    $selected,
                    $optionValue,
                    $level + 1,
                    $depth + 1,
                    $blockName,
                    $stack
                );
            }
        }
    }

    /**
     * Fetch label for stats dropdown
     *
     * @param   integer $level  Nesting level of stats. Will be used to
     *                          determine the type of label to return.
     * @param   string  $key    The key used to identify the stats. Represents
     *                          one of year, month, day or time.
     * @param   mixed  $value   Either an array containing the stats or an
     *                          integer representing the numer of registrations
     */
    protected function fetchStatsLabelByLevel($level, $key, $value) {
        global $_CORELANG, $_ARRAYLANG;

        static $arrMonthTxts;
        if (!isset($arrMonthTxts)) {
            $arrMonthTxts = explode(',', $_CORELANG['TXT_MONTH_ARRAY']);
        }
        $optionName = '';
        switch ($level) {
            case 1: // year value
                $optionName = $key;
                break;
            case 2: // month value
                $optionName = $arrMonthTxts[$key-1];
                break;
            case 3: // day value
                $optionName = $key . '.';
                break;
            case 4: // time value
                $optionName = $key . ' ' . $_ARRAYLANG['TXT_CALENDAR_OCLOCK'];
                break;
            default:
                break;
        }

        // add count if entries
        if (!is_array($value)) {
            $optionName .= ' ('. $value .')';
        }

        return $optionName;
    }

    /**
     * Get the event registration count by given filter
     * output array format
     *  array(
     *     year => array(
     *      month => array(
     *          day1 => registration_count
     *          day2 => registration_count
     *      )
     *     )
     *  )
     *
     * @return array
     */
    protected function getEventStats()
    {
        global $objDatabase;

        $blnFirst = true;
        $arrWhere = array();
        if ($this->getRegistrations)   { $arrWhere[] = 1; }
        if ($this->getDeregistrations) { $arrWhere[] = 0; }
        if ($this->getWaitlist)        { $arrWhere[] = 2; }
        $strWhere = ' AND (';
        foreach ($arrWhere as $value) {
            $strWhere .=  $blnFirst ? '`type` = '.$value : ' OR `type` = '.$value;
            $blnFirst  = false;
        }
        $strWhere .= ')';
        $query = 'SELECT
                    COUNT(1) AS `count`,
                    `date` FROM
                  `'.DBPREFIX.'module_'.$this->moduleTablePrefix.'_registration`
                WHERE
                    `event_id` = '. $this->eventId .'
                    '. $strWhere .'
                GROUP BY `date`';
        $registration = $objDatabase->Execute($query);
        if (!$registration) {
            return array();
        }

        $stats = array();
        while (!$registration->EOF) {
            $dateTime = new \DateTime();
            $dateTime->setTimestamp($registration->fields['date']);
            $year  = $dateTime->format('Y');
            $month = $dateTime->format('n');
            $day   = $dateTime->format('d');
            if (!isset($stats[$year])) {
                $stats[$year] = array();
            }
            if (!isset($stats[$year][$month])) {
                $stats[$year][$month] = array();
            }

            // optionally group registrations by time if the event is not
            // an all-day event
            if ($this->event->all_day) {
                $stats[$year][$month][$day] = $registration->fields['count'];
            } else {
                $time = $dateTime->format('H:i');
                if (!isset($stats[$year][$month][$day])) {
                    $stats[$year][$month][$day] = array();
                }
                $stats[$year][$month][$day][$time] = $registration->fields['count'];
            }

            $registration->MoveNext();
        }

        return $stats;
    }

    /**
     * Set the registration fields placeholders to the template
     *
     * @param \Cx\Core\Html\Sigma   $objTpl Template instance
     * @param integer               $regId  Registration id
     */
    function showRegistrationInputfields(\Cx\Core\Html\Sigma $objTpl, $regId = null)
    {
        global $_ARRAYLANG;

        $i = 0;
        $objForm         = new \Cx\Modules\Calendar\Controller\CalendarForm($this->formId);
        $objRegistration = new \Cx\Modules\Calendar\Controller\CalendarRegistration($this->formId, $regId);

        // parse the registration type for the add/edit subscription
        $regType = CalendarRegistration::REGISTRATION_TYPE_REGISTRATION;
        if (isset($_POST['registrationType'])) {
             $regType = (int) $_POST['registrationType'];
        } elseif (!empty($regId)) {
            $regType = $objRegistration->type;
        }
        $regTypeField = '<select class="calendarSelect" name="registrationType">';
        $regTypes = [
            CalendarRegistration::REGISTRATION_TYPE_REGISTRATION,
            CalendarRegistration::REGISTRATION_TYPE_CANCELLATION,
            CalendarRegistration::REGISTRATION_TYPE_WAITLIST,
        ];
        foreach ($regTypes as $type) {
            $selected = '';
            if ($regType == $type) {
                $selected = 'selected="selected"';
            }
            $regTypeField .= '<option value="' . $type . '" '. $selected .' />';
            $regTypeField .= CalendarRegistration::getTypeLabelById($type);
            $regTypeField .= '</option>';
        }
        $regTypeField .= '</select>';
        $objTpl->setVariable(array(
            $this->moduleLangVar.'_ROW'                             => $i % 2 == 0 ? 'row1' : 'row2',
            $this->moduleLangVar.'_REGISTRATION_INPUTFIELD_NAME'    => $_ARRAYLANG['TXT_CALENDAR_TYPE'],
            $this->moduleLangVar.'_REGISTRATION_INPUTFIELD_VALUE'   => $regTypeField,
        ));
        $objTpl->parse('calendar_registration_inputfield');
        $i++;

        if ($this->event && $this->event->seriesStatus && $this->event->independentSeries) {
            $endDate = new \DateTime();
            $endDate->modify('+10 years');

            $eventManager = new CalendarEventManager(null, $endDate);
            $objEvent     = new \Cx\Modules\Calendar\Controller\CalendarEvent($this->event->id);
            if ($eventManager->_addToEventList($objEvent)) {
                $eventManager->eventList[] = $objEvent;
            }
            $additionalRecurrences = $objEvent->seriesData['seriesAdditionalRecurrences'];
            $eventManager->generateRecurrencesOfEvent($objEvent, $additionalRecurrences);

            // parse the event date with or without the time information
            // depending on the fact if the event is an all-day event or not
            $format = $this->getDateFormat();
            if (!$this->event->all_day) {
                $format .= ' H:i';
            }

            $regEventDateField = '<select class="calendarSelect" name="registrationEventDate">';
            foreach ($eventManager->eventList as $event) {
                $selectedDate       = $objRegistration->eventDate == $event->startDate->getTimestamp() ? 'selected="selected"' : '';
                $regEventDateField .= '<option value="' . $event->startDate->getTimestamp() . '" ' . $selectedDate . ' />' . $this->formatDateTime2user($event->startDate, $format) . '</option>';
            }
            $regEventDateField .= '</select>';

            $objTpl->setVariable(array(
                $this->moduleLangVar.'_ROW'                             => $i % 2 == 0 ? 'row1' : 'row2',
                $this->moduleLangVar.'_REGISTRATION_INPUTFIELD_NAME'    => $_ARRAYLANG['TXT_CALENDAR_DATE_OF_THE_EVENT'],
                $this->moduleLangVar.'_REGISTRATION_INPUTFIELD_VALUE'   => $regEventDateField,
            ));
            $objTpl->parse('calendar_registration_inputfield');
            $i++;
        }

        foreach ($objForm->inputfields as $arrInputfield) {
            $inputfield = '';
            $options = explode(',', $arrInputfield['default_value'][FRONTEND_LANG_ID]);
            $optionSelect = true;

            if(isset($_POST['registrationField'][$arrInputfield['id']])) {
                $value = $_POST['registrationField'][$arrInputfield['id']];
            } else {
                $value = $regId != null ? $objRegistration->fields[$arrInputfield['id']]['value'] : '';
            }

            switch ($arrInputfield['type']) {
                case 'inputtext':
                case 'mail':
                case 'firstname':
                case 'lastname':
                    $inputfield = '<input type="text" class="calendarInputText" name="registrationField['.$arrInputfield['id'].']" value="'.$value.'">';
                    break;
                case 'textarea':
                    $inputfield = '<textarea class="calendarTextarea" name="registrationField['.$arrInputfield['id'].']">'.$value.'</textarea>';
                    break ;
                case 'seating':
                    $optionSelect = false;
                case 'select':
                case 'salutation':
                    $inputfield = '<select class="calendarSelect" name="registrationField['.$arrInputfield['id'].']">';
                    $selected =  empty($_POST) ? 'selected="selected"' : '';
                    $inputfield .= $optionSelect ? '<option value="" '.$selected.'>'.$_ARRAYLANG['TXT_CALENDAR_PLEASE_CHOOSE'].'</option>' : '';
                    foreach ($options as $key => $name)  {
                        $selected =  ($key+1 == $value)  ? 'selected="selected"' : '';
                        $inputfield .= '<option value="'.intval($key+1).'" '.$selected.'>'.$name.'</option>';
                    }
                    $inputfield .= '</select>';
                    break;
                 case 'radio':
                    $arrValue = explode('[[', $value ?? '');
                    $value    = $arrValue[0];
                    $input    = str_replace(']]','', $arrValue[1] ?? '');
                    foreach ($options as $key => $name)  {
                        $checked =  ($key+1 == $value) ? 'checked="checked"' : '';
                        $textfield = '<input type="text" class="calendarInputCheckboxAdditional" name="registrationFieldAdditional['.$arrInputfield['id'].']['.$key.']" value="'. ($checked ? $input : '') .'" />';
                        $name = str_replace('[[INPUT]]', $textfield, $name);
                        $inputfield .= '<input type="radio" class="calendarInputCheckbox" name="registrationField['.$arrInputfield['id'].']" value="'.intval($key+1).'" '.$checked.'/>&nbsp;'.$name.'<br />';
                    }
                    break;
                 case 'checkbox':
                    $results = $value;
                    if (!is_array($results)) {
                        $results = explode(',', $results);
                    }
                    foreach ($results as $result) {
                        $parts = explode('[[', $result);
                        $value = $parts[0];
                        $input = '';
                        if (isset($parts[1])) {
                            $input = $parts[1];
                        }
                        $value = !empty($value) ? $value : 0;
                        $input = str_replace(']]','', $input);
                        $newResult[$value] = $input;
                    }

                    foreach ($options as $key => $name)  {
                        $checked = array_key_exists($key+1, $newResult) || (in_array($key+1, $_POST['registrationField'][$arrInputfield['id']] ?? array()))  ? 'checked="checked"' : '';
                        $textfield = '<input type="text" class="calendarInputCheckboxAdditional" name="registrationFieldAdditional['.$arrInputfield['id'].']['.$key.']" value="'. ($checked ? $newResult[$key+1] : '') .'" />';
                        $name = str_replace('[[INPUT]]', $textfield, $name);
                        $inputfield .= '<input '.$checked.' type="checkbox" class="calendarInputCheckbox" name="registrationField['.$arrInputfield['id'].'][]" value="'.intval($key+1).'" />&nbsp;'.$name.'<br />';
                    }
                    break;
                 case 'agb':
                     $checked = $value ? "checked='checked'" : '';
                     $inputfield = '<input '. $checked .' class="calendarInputCheckbox" type="checkbox" name="registrationField['.$arrInputfield['id'].'][]" value="1" />&nbsp;'.$_ARRAYLANG['TXT_CALENDAR_AGB'].'<br />';
                     break;
            }

            if ($arrInputfield['type'] != 'fieldset') {
                $objTpl->setVariable(array(
                    $this->moduleLangVar.'_ROW'                              => $i % 2 == 0 ? 'row1' : 'row2',
                    $this->moduleLangVar.'_REGISTRATION_INPUTFIELD_NAME'     => $arrInputfield['name'][FRONTEND_LANG_ID],
                    $this->moduleLangVar.'_REGISTRATION_INPUTFIELD_REQUIRED' => $arrInputfield['required'] == 1 ? '<font class="calendarRequired"> *</font>' : '',
                    $this->moduleLangVar.'_REGISTRATION_INPUTFIELD_VALUE'    => $inputfield,
                ));
                $objTpl->parse('calendar_registration_inputfield');
                $i++;
            }
        }
    }

    /**
     * Count returns the number of escort data for the event
     *
     * @return int number of escort data
     */
    function getEscortData()
    {
        global $objDatabase;

        $query = "SELECT
                    `n`.`field_id`
                  FROM
                    `".DBPREFIX."module_{$this->moduleTablePrefix}_registration_form_field_name` AS `n`
                  INNER JOIN
                    `".DBPREFIX."module_{$this->moduleTablePrefix}_registration_form_field` AS `f`
                  ON
                    `n`.`field_id` = `f`.`id`
                  WHERE
                    `n`.`form_id` = '{$this->formId}'
                  AND
                    `n`.`lang_id` = '" . FRONTEND_LANG_ID . "'
                  AND
                    `f`.`type` = 'seating'
                ";
        $seatingFieldId = $objDatabase->getOne($query);

        // whether or not the registrations are bound to a specific date
        $restrictToDate = false;

        // Limit search to date of the event. This is critical for series!
        if ($this->event->seriesStatus &&
            $this->event->independentSeries
        ) {
            $restrictToDate = true;
            $startDateBackup = $this->startDate;
            $endDateBackup = $this->endDate;
            $this->startDate = $this->event->startDate->format('U');
            $this->endDate = $this->event->endDate->format('U');
        }

        // fetch registrations (type REGISTRATION_TYPE_REGISTRATION) of event
        $this->getRegistrationList();

        if ($restrictToDate) {
            $this->startDate = $startDateBackup;
            $this->endDate = $endDateBackup;
        }

        $countSeating = 0;
        foreach ($this->registrationList as $registration) {
            // check if the registration contains any seating-info
            if (
                empty($seatingFieldId) ||
                !isset($registration->fields[$seatingFieldId])
            ) {
                // simply increment the attendees count by 1 as no seating info
                // is present
                $countSeating += 1;
                continue;
            }

            $arrOptions    = explode(',', $registration->getForm()->inputfields[$seatingFieldId]['default_value'][FRONTEND_LANG_ID]);
            $optionIdx = $registration->fields[$seatingFieldId]['value'] - 1;
            if (isset($arrOptions[$optionIdx])) {
                $countSeating += (int) $arrOptions[$optionIdx];
            } else {
                $countSeating += 1;
            }
        }

        return $countSeating;
    }
}
