<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * EventListener for Calendar
 *
 * @copyright   Cloudrexx AG
 * @author      Project Team SS4U <info@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  module_calendar
 */

namespace Cx\Modules\Calendar\Model\Event;
use Cx\Core\MediaSource\Model\Entity\MediaSourceManager;
use Cx\Core\MediaSource\Model\Entity\MediaSource;
use Cx\Core\Event\Model\Entity\DefaultEventListener;

/**
 * EventListener for Calendar
 *
 * @copyright   Cloudrexx AG
 * @author      Project Team SS4U <info@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  module_calendar
 */
class CalendarEventListener extends DefaultEventListener {

    public function SearchFindContent($search) {
        $term_db = $search->getTerm();
        $query = \Cx\Modules\Calendar\Controller\CalendarEvent::getEventSearchQuery($term_db);
        $dateTime = $this->getComponent('DateTime');
        $match = $this->cx->getDb()->getAdoDb()->Execute($query);
        if (!$match || $match->EOF) {
            return;
        }
        $data = array();
        while (!$match->EOF) {
            $associatedCatIds = $catIds = array_map(
                'intval',
                explode(',', $match->fields['categories'])
            );
            while ($catIds) {
                $page = \Cx\Modules\Calendar\Controller\CalendarEventManager::getDetailApplicationPageByCategoryIds(
                    $catIds
                );
                if (!$page) {
                    break;
                }
                if (!$search->isPageListable($page)) {
                    array_shift($catIds);
                    continue;
                }
                break;
            }
            if (!$page) {
                $match->MoveNext();
                continue;
            }
            $date = $dateTime->createDateTimeForDb($match->fields['startdate']);
            $dateTime->db2user($date);
            $timestamp = $date->getTimestamp();
            $url = \Cx\Modules\Calendar\Controller\CalendarEventManager::getDetailApplicationUrlFromEventParams(
                $match->fields['id'],
                $timestamp,
                $catIds
            );
            $content = '';
            if (isset($match->fields['content'])) {
                $content = $search->parseContentForResultDescription(
                    $match->fields['content']
                );
            }

            $result = array(
                'Score'     => $search->getPercentageFromScore(
                    $match->fields['score']
                ),
                'Title'     => $match->fields['title'],
                'Content'   => $content,
                'Image'     => $match->fields['image'],
                'Link'      => (string) $url,
                'Date'      => null,
                'Component' => 'Calendar',
                'Class'     => \Cx\Modules\Calendar\Controller\CalendarEvent::class,
                'Id'        => $match->fields['id'],
            );

            if ($search->getOptions()['includeAssociationData']) {
                $result['Categorization'] = [];
                foreach ($associatedCatIds as $id) {
                    $category = new \Cx\Modules\Calendar\Controller\CalendarCategory($id);
                    if (!$category) {
                        continue;
                    }
                    $result['Categorization'][] = [
                        'Class' => $category::class,
                        'Id'    => $id,
                        'Title' => $category->name,
                    ];
                }
            }

            $data[] = $result;
            $match->MoveNext();
        }
        $result = new \Cx\Core_Modules\Listing\Model\Entity\DataSet($data);
        $search->appendResult($result);
    }

    public function mediasourceLoad(MediaSourceManager $mediaBrowserConfiguration)
    {
        global $_ARRAYLANG;
        $mediaType = new MediaSource(
            'calendar',
            // hotfix as CLX-1045 is not yet live
            $_ARRAYLANG['TXT_CALENDAR'] ?? '',
            array(
                $this->cx->getWebsiteImagesCalendarPath(),
                $this->cx->getWebsiteImagesCalendarWebPath(),
            ),
            array(16)
        );
        $mediaBrowserConfiguration->addMediaType($mediaType);
    }

    public function onFlush($eventArgs) {
        $this->cx->getComponent('Cache')->deleteComponentFiles('Calendar');
        $this->cx->getComponent('Cache')->deleteComponentFiles('Home');
    }
}
