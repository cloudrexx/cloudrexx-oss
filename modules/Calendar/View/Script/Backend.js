/* global cx */
cx.jQuery(function() {
    cx.jQuery("#category").chosen({
        placeholder_text_multiple: "",
        display_disabled_options: false,
        display_selected_options: false
    });
});

function toggleStatus(el, type, eventId) {
    cx.ajax(
        "Calendar",
        "toggleStatus",
        {
            "data": {
                "type": type,
                "id": eventId
            },
            "showMessage": true,
            "postSuccess": function() {
                let imgSrc = el.querySelector("img").src;
                let status = "green";
                if (/led_green.gif/.exec(imgSrc)) {
                    status = "red";
                }
                imgSrc = imgSrc.replace("green.gif", "").replace("red.gif", "");
                el.querySelector("img").src = imgSrc + status + ".gif";
            }
        }
    );
}
