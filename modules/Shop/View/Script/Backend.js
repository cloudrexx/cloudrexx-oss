cx.ready(function() {
    const statusTogglers = document.querySelectorAll('form[name=product_list] a.schedule_status');
    statusTogglers.forEach((toggler) => {
        toggler.addEventListener('click', function(e) {
            cx.ajax(
                "Shop",
                "toggleProductState",
                {
                    context: this,
                    data: {
                        'id': this.dataset.id,
                        'locale': cx.variables.get('language')
                    },
                    showMessage: true,
                    postSuccess: function(result) {
                        if (result.data.active) {
                            this.context.classList.add('active');
                            this.context.classList.remove('inactive');
                        } else {
                            this.context.classList.add('inactive');
                            this.context.classList.remove('active');
                        }
                    }
                }
            );
        });
    });
});
