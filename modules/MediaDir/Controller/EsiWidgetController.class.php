<?php
declare(strict_types=1);

/**
 * Cloudrexx
 *
 * @link      https://www.cloudrexx.com
 * @copyright Cloudrexx AG
 *
 * This file is part of Cloudrexx.
 *
 * Cloudrexx is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3 of the License.
 *
 * Cloudrexx is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

namespace Cx\Modules\MediaDir\Controller;

/**
 * @copyright   Cloudrexx AG
 * @author      Thomas Wirz <thomas.wirz@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  module_mediadir
 */
class EsiWidgetController extends \Cx\Core_Modules\Widget\Controller\EsiWidgetController {
    /**
     * @inheritDoc
     */
    public function parseWidget($name, $template, $response, $params) {
        switch ($name) {
            case 'MEDIADIR_ENTRY':
                $widgetValue = $this->parseWidgetMediadirEntry($name, $params, $response);
                break;

            default:
                throw new \Exception('Invalid widget');
                break;
        }
        $template->setVariable($name, $widgetValue);
    }

    /**
     * Parse widget `func_mediadir_entry()`
     *
     * See Component.yml for widget description.
     *
     * @param string $name Name of widget
     * @param array $params Data passed as argument `$params` to
     *     {@see EsiWidgetController::parseWidget()}.
     * @return string Generated output of widget according to config set by `$params`.
     */
    protected function parseWidgetMediadirEntry(string $name, array $params, $response): string  {
        // callback arguments are numerically indexed in $params
        $realParams = array_filter($params, 'is_int', ARRAY_FILTER_USE_KEY);

        // name of block to parse entries in
        $block = 'mediadirEntryList';

        $widgetTemplateName = 'Default';
        if (!empty($realParams[0])) {
            $widgetTemplateName = preg_replace(
                '/[^a-z0-9_-]/i',
                '',
                (string) $realParams[0]
            );
        }
        $file = $params['theme']->getFilePath(
            $params['theme']->getFolderName()
            . $this->getSystemComponentController()->getDirectory(false, true)
            . '/Template/Frontend/Widget/'
            . strtolower($name) . '/'
            . $widgetTemplateName . '.html'
        );
        if (!file_exists($file)) {
            return '';
        }
        // legacy-widget hack (undo in CLX-1707):
        // - manually parse BLOCK_... as those are no widgets yet
        // - other widgets within $file are not supported. will be fixed in CLX-1707
        $content = file_get_contents($file);
        \Cx\Modules\Block\Controller\Block::setBlocks($content, $response->getPage());
        $template = new \Cx\Core\Html\Sigma();
        $template->setTemplate($content);

        // extend filter by additional config from url arguments
        // in case {MEDIADIR_CONFIG_FILTER_AUTO} is set in $template
        $categoryId = 0;
        if (isset($params['query']['cid'])) {
            $categoryId = intval($params['query']['cid']);
        }
        $levelId = 0;
        if (isset($params['query']['lid'])) {
            $levelId = intval($params['query']['lid']);
        }

        // extend filter by additional config from functional
        // placeholders from $template
        $config = MediaDirectoryLibrary::fetchMediaDirListConfigFromTemplate(
            $block,
            $template,
            null,
            $categoryId,
            $levelId
        );

        // ensure filter set by callback params have highest precedence
        $widgetConfig = [
            'level',
            'category',
            'form',
            'entry',
        ];
        $idx = 1;
        foreach ($widgetConfig as $param) {
            if (!empty($realParams[$idx])) {
                $config['filter'][$param] = (int) $realParams[$idx];
            }
            $idx++;
        }

        // finally generate the output
        $mediadir = new MediaDirectory(
            '',
            $this->getSystemComponentController()->getName()
        );
        $mediadir->parseEntries($template, $block, $config);
        return $template->get();
    }
}
