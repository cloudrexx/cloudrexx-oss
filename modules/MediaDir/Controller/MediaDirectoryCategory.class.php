<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * Media  Directory Category Class
 *
 * @copyright   CLOUDREXX CMS - CLOUDREXX AG
 * @author      Cloudrexx Development Team <info@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  module_mediadir
 * @todo        Edit PHP DocBlocks!
 */
namespace Cx\Modules\MediaDir\Controller;
/**
 * Media Directory Category Class
 *
 * @copyright   CLOUDREXX CMS - CLOUDREXX AG
 * @author      CLOUDREXX Development Team <info@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  module_mediadir
 */
class MediaDirectoryCategory extends MediaDirectoryLibrary
{
    private $intCategoryId;
    private $intParentId;
    private $bolGetChildren;
    private $intRowCount;
    private $arrExpandedCategoryIds = array();

    private $strSelectedOptions;
    private $strNotSelectedOptions;
    private $arrSelectedCategories;
    private $intCategoriesSortCounter = 0;
    private $strNavigationPlaceholder;

    /**
     * Contains all category data from the database.
     * Will be populated during object instantiation through
     * {@see static::fetchCategoriesFromDb()}.
     *
     * @var array|null
     */
    protected static $arrAllCategories = null;

    /**
     * Contains all category data specified by {@see static::$intCategoryId},
     * {@see static::$intParentId} and {@see static::$bolGetChildren}.
     * Will be populated by {@see static::loadCategories()}.
     *
     * @var array|null
     */
    public $arrCategories = null;

    /**
     * Constructor
     */
    function __construct($intCategoryId=null, $intParentId=null, $bolGetChildren=1, $name)
    {
        $this->intCategoryId = intval($intCategoryId);
        $this->intParentId = intval($intParentId);
        $this->bolGetChildren = intval($bolGetChildren);
        parent::__construct('.', $name);    
        parent::getSettings();
        parent::getFrontendLanguages();
        $this->loadCategories();
    }

    /**
     * Load categories fetched by {@see static::getCategories()}
     * into {@see static::arrCategories}
     *
     * {@see static::intCategoryId} will be passed to {@see static::arrCategories}
     * as `$intCategoryId` and {@see static::intParentId} as `$intParentId`.
     *
     * @param bool $useCache Set to false to force loading data from the database.
     */
    public function loadCategories(bool $useCache = true): void {
        $this->arrCategories = self::getCategories(
            $this->intCategoryId,
            $this->intParentId,
            $useCache
        );
    }

    /**
     * Return filtered set of categories fetched from {@see static::fetchCategoriesFromDb()}
     *
     * If $intCategoryId is set, then only that category is returned.
     * If $intParentId is set, then all child-categories of $intParentId
     * are returned.
     * If neither of $intCategoryId and $intParentId are set, then all main
     * categories (having parent_id set to 0) are returned.
     *
     * @param bool $useCache Set to `false` to force loading data from the database.
     * @return array|null Filtered set of categories.
     */
    protected function getCategories(
        ?int $intCategoryId,
        ?int $intParentId,
        bool $useCache = true
    ): ?array {
        $arrAllCategories = $this->fetchCategoriesFromDb($useCache);
        $arrParsedCategoryList = array();

        // return selected category
        if (!empty($intCategoryId)) {
            if (isset($arrAllCategories[$intCategoryId])) {
                $arrParsedCategoryList[$intCategoryId] = $arrAllCategories[$intCategoryId];

                if (!$this->bolGetChildren) {
                    $arrParsedCategoryList[$intCategoryId]['catChildren'] = array();
                }
            }
            return $arrParsedCategoryList;
        }

        // return children of specific category
        if (!empty($intParentId)) {
            $arrParsedCategoryList = $arrAllCategories[$intParentId]['catChildren'];
            return $arrParsedCategoryList;
        }

        // return all categories of first level (parentId=0)
        foreach ($arrAllCategories as $catId => $arrCategory) {
            if ($arrCategory['catParentId']) {
                continue;
            }
            $arrParsedCategoryList[$catId] = $arrCategory;
            if (!$this->bolGetChildren) {
                $arrParsedCategoryList[$catId]['catChildren'] = array();
            }
        }
        return $arrParsedCategoryList;
    }

    /**
     * Fetch all categories from database and store them in
     * {@see static::$arrAllCategories}
     *
     * If the categories have been fetched previously and been stored in
     * {@see static::$arrAllCategories}, then that will be returned directly
     * instead of re-fetching the data from the database again.
     *
     * @param bool $useCache Set to false to force loading data from the database.
     * @return array|null All categories fetched from the database.
     */
    protected function fetchCategoriesFromDb(bool $useCache = true): ?array {
        if (
            $useCache
            && is_array(static::$arrAllCategories)
        ) {
            return static::$arrAllCategories;
        }
        static::$arrAllCategories = array();

        $langId = static::getOutputLocale()->getId();

        $query = '
            SELECT
                `id`,
                `parent_id`,
                `order`,
                `show_entries`,
                `show_subcategories`,
                `picture`,
                `active`
            FROM
                `' . DBPREFIX . 'module_' . $this->moduleTablePrefix . '_categories`';
        if (!$this->cx->isBackendMode()) {
            $query .= ' WHERE `active` = 1';
		}
        $objCategories = $this->cx->getDb()->getAdoDb()->Execute($query);
        if (!$objCategories) {
            return array();
        }

        $langInit = [
            0 => '',
            $langId => '',
        ];
        $arrCategories = array();
        while (!$objCategories->EOF) {
            $id = $objCategories->fields['id'];
            $arrCategories[$id] = [
                'catNumEntries'         => 0,
                'catId'                 => $id,
                'catParentId'           => $objCategories->fields['parent_id'],
                'catOrder'              => $objCategories->fields['order'],
                'catName'               => $langInit,
                'catDescription'        => $langInit,
                'catMetaDesc'           => $langInit,
                'catPicture'            => htmlspecialchars($objCategories->fields['picture'], ENT_QUOTES, CONTREXX_CHARSET),
                'catShowEntries'        => $objCategories->fields['show_entries'],
                'catShowSubcategories'  => $objCategories->fields['show_subcategories'],
                'catActive'             => $objCategories->fields['active'],
                'catChildren'           => [],
            ];
            $objCategories->MoveNext();
        }

        $query = '
            SELECT
                `category_id` AS `id`,
                `category_name` AS `name`,
                `category_description` AS `description`,
                `category_metadesc` AS `metadesc`,
                `lang_id`
            FROM
                `' . DBPREFIX . 'module_' . $this->moduleTablePrefix . '_categories_names`';
        if (!$this->cx->isBackendMode()) {
            $query .= '
                INNER JOIN
                    `' . DBPREFIX . 'module_' . $this->moduleTablePrefix . '_categories` AS `category`
                ON `category_id` = `category`.`id`
                WHERE
                    `category`.`active` = 1';
		}
        $objCategories = $this->cx->getDb()->getAdoDb()->Execute($query);
        if (!$objCategories) {
            return array();
        }
        while (!$objCategories->EOF) {
            $id = $objCategories->fields['id'];
            if (!isset($arrCategories[$id])) {
                $objCategories->MoveNext();
                continue;
            }
            $lang = $objCategories->fields['lang_id'];
            $arrCategories[$id]['catName'][$lang] = htmlspecialchars($objCategories->fields['name'], ENT_QUOTES, CONTREXX_CHARSET);
            $arrCategories[$id]['catDescription'][$lang] = htmlspecialchars($objCategories->fields['description'], ENT_QUOTES, CONTREXX_CHARSET);
            $arrCategories[$id]['catMetaDesc'][$lang] = htmlspecialchars($objCategories->fields['metadesc'], ENT_QUOTES, CONTREXX_CHARSET);
            if ($objCategories->fields['lang_id'] == $langId) {
                $arrCategories[$id]['catName'][0] = $objCategories->fields['name'];
                $arrCategories[$id]['catDescription'][0] = $objCategories->fields['description'];
                $arrCategories[$id]['catMetaDesc'][0] = $objCategories->fields['metadesc'];
            }
            $objCategories->MoveNext();
        }

        $sortKey = 'catOrder';
        if ($this->arrSettings['settingsCategoryOrder']) {
            $sortKey = 'catName';
        }
        uasort($arrCategories, function($a, $b) use ($sortKey) {
            $termA = $a[$sortKey];
            if (is_array($termA)) {
                $termA = current($termA);
            }
            $termB = $b[$sortKey];
            if (is_array($termB)) {
                $termB = current($termB);
            }
            return strtolower($termA) <=> strtolower($termB);
        });

        if (
            $this->arrSettings['settingsCountEntries'] == 1
            || $this->cx->isBackendMode()
        ) {
            $query = '
                SELECT
                    COUNT(1) as cnt,
                    `rel_categories`.`category_id`
                FROM
                    `' . DBPREFIX . 'module_' . $this->moduleTablePrefix . '_entries` AS `entry`
                INNER JOIN
                    `' . DBPREFIX . 'module_' . $this->moduleTablePrefix . '_rel_entry_categories` AS `rel_categories`
                ON
                    `rel_categories`.`entry_id` = `entry`.`id`';
            if (!$this->cx->isBackendMode()) {
                $query .= '
                    INNER JOIN
                        `' . DBPREFIX . 'module_' . $this->moduleTablePrefix . '_categories` AS `category`
                    ON `rel_categories`.`category_id` = `category`.`id`
                    LEFT JOIN
                        `' . DBPREFIX . 'module_' . $this->moduleTablePrefix . '_rel_entry_inputfields` AS rel_inputfield
                    ON
                        rel_inputfield.`entry_id` = `entry`.`id`
                    WHERE
                        `category`.`active` = 1
                    AND
                        `entry`.`active` = 1
                    AND
                        rel_inputfield.`form_id` = entry.`form_id`
                    AND
                        rel_inputfield.`field_id` = (' . $this->getQueryToFindPrimaryInputFieldId() . ')
                    AND
                        rel_inputfield.`lang_id` = ' . static::getOutputLocale()->getId() . '
                    AND ((`entry`.`duration_type`=2 AND `entry`.`duration_start` <= ' . time() . ' AND `entry`.`duration_end` >= ' . time() . ') OR (`entry`.`duration_type`=1))';
            }
            $query .= '
                    GROUP BY
                        `rel_categories`.`category_id`';
            $result = $this->cx->getDb()->getAdoDB()->Execute($query);
            if (!$result) {
                return array();
            }
            while (!$result->EOF) {
                $id = $result->fields['category_id'];
                if (!isset($arrCategories[$id])) {
                    $result->MoveNext();
                    continue;
                }
                $arrCategories[$id]['catNumEntries'] = $result->fields['cnt'];
                $result->MoveNext();
            }
        }
        $arrParsedCatIds = [0];
        $newlyParsedCatIds = [];
        do {
            if ($newlyParsedCatIds) {
                array_push($arrParsedCatIds, ...$newlyParsedCatIds);
                $newlyParsedCatIds = [];
            }
            foreach ($arrCategories as $catId => &$category) {
                $parentId = $category['catParentId'];
                if (in_array($catId, $arrParsedCatIds)) {
                    continue;
                }
                if (!in_array($parentId, $arrParsedCatIds)) {
                    continue;
                }
                $newlyParsedCatIds[] = $catId;
                if (!$parentId) {
                    continue;
                }
                $arrCategories[$parentId]['catChildren'][$catId] = &$category;
                $parentId = $catId;
                while ($parentId = $arrCategories[$parentId]['catParentId']) {
                    $arrCategories[$parentId]['catNumEntries'] += $category['catNumEntries'];
                }
            }
        } while ($newlyParsedCatIds);

        static::$arrAllCategories = $arrCategories;
        return static::$arrAllCategories;
    }

    public function findBySlug($slug): array {
        return $this->findByName($this->getNameFromSlug($slug));
    }

    public function findByName($name): array {
        $ids = [];
        $arrCategories = $this->getCategoryData();
        foreach ($arrCategories as $arrCategory) {
            if ($arrCategory['catName'][0] == $name) {
                $ids[] = $arrCategory['catId'];
            }
        }
        return $ids;
    }

    /**
     * Returns a list with IDs of all visible categories that contain published
     * data
     *
     * Published data is identified as categories having any of the following
     * options set to activated:
     * - categoryShowEntries
     * - categoryShowSubcategories
     *
     * @return  array   List of IDs of categories
     */
    public static function getIdsWithPublishedData() {
        $cx = \Cx\Core\Core\Controller\Cx::instanciate();
        $db = $cx->getDb()->getAdoDb();
        $result = $db->Execute('
            SELECT `id`
            FROM `' . DBPREFIX . 'module_mediadir_categories`
            WHERE `active` = 1
            AND (
                `show_subcategories` = 1 OR
                `show_entries` = 1
            )
        ');
        if (!$result || $result->EOF) {
            return array();
        }

        $ids = array();
        while (!$result->EOF) {
            $ids[] = $result->fields['id'];
            $result->MoveNext();
        }

        return $ids;
    }

    function listCategories($objTpl, $intView, $intCategoryId=null, $arrParentIds = array(), $intEntryId=null, $arrExistingBlocks=null, $intStartLevel=1, $cmd = null)
    {
        global $_ARRAYLANG, $_CORELANG, $objDatabase;

        if (!is_array($arrParentIds)) {
            $arrParentIds = [];
            $arrCategories = $this->arrCategories;
        } else {
            $arrCategoryChildren = $this->arrCategories;

            foreach ($arrParentIds as $intParentId) {
                $arrCategoryChildren = $arrCategoryChildren[$intParentId]['catChildren'];
            }
            $arrCategories = $arrCategoryChildren;
        }

        $requestParams = $this->cx->getRequest()->getUrl()->getParamArray();

        switch ($intView) {
            case 1:
                //Backend View
                $exp_cat = isset($_GET['exp_cat']) ? $_GET['exp_cat'] : '';
                foreach ($arrCategories as $arrCategory) {
                    //generate space
                    $spacer = null;
                    $intSpacerSize = null;
                    $intSpacerSize = (count($arrParentIds)*21);
                    $spacer .= '<img src="/core/Core/View/Media/icons/pixel.gif" border="0" width="'.$intSpacerSize.'" height="11" alt="" />';

                    //check expanded categories
                    if($exp_cat == 'all') {
                        $bolExpandCategory = true;
                    } else {
                        $this->arrExpandedCategoryIds = array();
                        $bolExpandCategory = $this->getExpandedCategories($exp_cat, array($arrCategory));
                    }

                    if(!empty($arrCategory['catChildren'])) {
                        if((in_array($arrCategory['catId'], $this->arrExpandedCategoryIds) && $bolExpandCategory) || $exp_cat == 'all'){
                            $strCategoryIcon = '<a href="/cadmin/'.$this->moduleName.'?exp_cat='.$arrCategory['catParentId'].'&tab-default=MediaDirTab_categories" alt="{'.$this->moduleLangVar.'_CATEGORY_NAME}" title="{'.$this->moduleLangVar.'_CATEGORY_NAME}"><i class="fa-regular fa-square-minus"></i></a>';
                        } else {
                            $strCategoryIcon = '<a href="/cadmin/'.$this->moduleName.'?exp_cat='.$arrCategory['catId'].'&tab-default=MediaDirTab_categories" alt="{'.$this->moduleLangVar.'_CATEGORY_NAME}" title="{'.$this->moduleLangVar.'_CATEGORY_NAME}"><i class="fa-regular fa-square-plus"></i></a>';
                        }
                    } else {
                        $strCategoryIcon = '<img src="/core/Core/View/Media/icons/pixel.gif" border="0" width="11" height="11" alt="{'.$this->moduleLangVar.'_CATEGORY_NAME}" title="{'.$this->moduleLangVar.'_CATEGORY_NAME}" />';
                    }

                    //parse variables
                    $objTpl->setVariable(array(
                        $this->moduleLangVar.'_CATEGORY_ROW_CLASS' =>  $this->intRowCount%2==0 ? 'row1' : 'row2',
                        $this->moduleLangVar.'_CATEGORY_ID' => $arrCategory['catId'],
                        $this->moduleLangVar.'_CATEGORY_ORDER' => $arrCategory['catOrder'],
                        $this->moduleLangVar.'_CATEGORY_NAME' => contrexx_raw2xhtml($arrCategory['catName'][0]),
                        $this->moduleLangVar.'_CATEGORY_DESCRIPTION' => $arrCategory['catDescription'][0],
                        $this->moduleLangVar.'_CATEGORY_DESCRIPTION_ESCAPED' => strip_tags($arrCategory['catDescription'][0]),
                        $this->moduleLangVar.'_CATEGORY_PICTURE' => $arrCategory['catPicture'],
                        $this->moduleLangVar.'_CATEGORY_NUM_ENTRIES' => $arrCategory['catNumEntries'],
                        $this->moduleLangVar.'_CATEGORY_ICON' => $spacer.$strCategoryIcon,
                        $this->moduleLangVar.'_CATEGORY_VISIBLE_STATE_ACTION' => $arrCategory['catActive'] == 0 ? 1 : 0,
                        $this->moduleLangVar.'_CATEGORY_VISIBLE_STATE_IMG' => $arrCategory['catActive'] == 0 ? 'off' : 'on',
                    ));

                    $objTpl->parse($this->moduleNameLC.'CategoriesList');
                    $arrParentIds[] = $arrCategory['catId'];
                    $this->intRowCount++;

                    //get children
                    if(!empty($arrCategory['catChildren'])){
                        if($bolExpandCategory) {
                            self::listCategories($objTpl, 1, $intCategoryId, $arrParentIds);
                        }
                    }

                    @array_pop($arrParentIds);
                }
                break;
            case 2:
                if (!$arrCategories) {
                    break;
                }
                //Frontend View
                $intNumBlocks = count($arrExistingBlocks);
                $strIndexHeader = '';

                // in a previous version, the system did not start parsing
                // with the first row block. Instead it began to render the
                // last column.
                if(
                    $this->arrSettings['legacyBehavior'] &&
                    $this->arrSettings['settingsCategoryOrder'] == 2
                ) {
                    $i = $intNumBlocks-1;
                } else {
                    $i = 0;
                }

                //set first index header
                if($this->arrSettings['settingsCategoryOrder'] == 2) {
                    $strFirstIndexHeader = null;
                }

                $intNumCategories = count($arrCategories);

                if ($intNumBlocks && $intNumCategories%$intNumBlocks != 0) {
                    $intNumCategories = $intNumCategories + ($intNumCategories%$intNumBlocks);
                }

                $intNumPerColumn = intval($intNumCategories/$intNumBlocks);
                $x = 0;

                $levelId = null;
                if (isset($requestParams['lid'])) {
                    $levelId = intval($requestParams['lid']);
                }

                $thumbnailFormats = $this->cx->getMediaSourceManager()->getThumbnailGenerator()->getThumbnails();

                foreach ($arrCategories as $arrCategory) {
                    $strIndexHeaderTag = '';
                    switch ($this->arrSettings['settingsCategoryOrder']) {
                        case 2: // alphabetically grouped order
                            $intBlockId = $arrExistingBlocks[$i];
                            $strIndexHeader = strtoupper(substr($arrCategory['catName'][0],0,1));
                            if($strFirstIndexHeader != $strIndexHeader) {
                                if ($i < $intNumBlocks-1) {
                                    ++$i;
                                } else {
                                    $i = 0;
                                }
                                $strIndexHeaderTag = '<span class="'.$this->moduleNameLC.'LevelCategoryIndexHeader">'.$strIndexHeader.'</span><br />';
                            } else {
                                $strIndexHeaderTag = '';
                            }
                            break;

                        case 0: // customer order -> parse row-wise
                            $intBlockId = $arrExistingBlocks[$i];
                            if ($i < $intNumBlocks-1) {
                                ++$i;
                            } else {
                                $i = 0;
                            }
                            break;

                        case 1: // alphabetical order -> parse column-wise
                        default:
                            if ($x == $intNumPerColumn) {
                                ++$i;

                                if ($i == $intNumBlocks) {
                                    $i = 0;
                                }

                                $x = 1;
                            } else {
                                $x++;
                            }
                            $intBlockId = $arrExistingBlocks[$i];
                            break;
                    }

                    // parse entries
                    if (
                        $objTpl->blockExists($this->moduleNameLC.'CategoriesLevels_row_' . $intBlockId . '_entries') &&
                        $objTpl->blockExists($this->moduleNameLC.'CategoriesLevels_row_' . $intBlockId . '_entry')
                    ) {
                        $objEntry = new MediaDirectoryEntry($this->moduleName);
                        $objEntry->getEntries(null, $levelId, $arrCategory['catId'], null, false, null, true, null, 'n', null, null, $cmd);
                        if ($objEntry->countEntries()) {
                            // set mediadirCategoriesLevels_row_N_entry tempalte block to be parsed
                            $objEntry->setStrBlockName($this->moduleNameLC.'CategoriesLevels_row_'. $intBlockId . '_entry');

                            // parse related entries
                            $objEntry->listEntries($objTpl, 5, 'category_level');
                            $objTpl->parse($this->moduleNameLC.'CategoriesLevels_row_' . $intBlockId . '_entries');
                        } else {
                            $objTpl->hideBlock($this->moduleNameLC.'CategoriesLevels_row_' . $intBlockId . '_entries');
                        }
                    }

                    //parse variables
                    $objTpl->setVariable(array(
                        $this->moduleLangVar.'_CATEGORY_LEVEL_ID' => $arrCategory['catId'],
                        $this->moduleLangVar.'_CATEGORY_LEVEL_NAME' => contrexx_raw2xhtml($arrCategory['catName'][0]),
                        $this->moduleLangVar.'_CATEGORY_LEVEL_LINK' => $strIndexHeaderTag.'<a href="'.$this->getAutoSlugPath(null, $arrCategory['catId'], $levelId, true).'">'.contrexx_raw2xhtml($arrCategory['catName'][0]).'</a>',
                        $this->moduleLangVar.'_CATEGORY_LEVEL_LINK_SRC' => $this->getAutoSlugPath(null, $arrCategory['catId'], $levelId, true),
                        $this->moduleLangVar.'_CATEGORY_LEVEL_DESCRIPTION' => $arrCategory['catDescription'][0],
                        $this->moduleLangVar.'_CATEGORY_LEVEL_META_DESCRIPTION' => contrexx_raw2xhtml($arrCategory['catMetaDesc'][0]),
                        $this->moduleLangVar.'_CATEGORY_LEVEL_PICTURE' => '<img src="'.$arrCategory['catPicture'].'" border="0" alt="'.contrexx_raw2xhtml($arrCategory['catName'][0]).'" />',
                        $this->moduleLangVar.'_CATEGORY_LEVEL_PICTURE_SOURCE' => $arrCategory['catPicture'],
                        $this->moduleLangVar.'_CATEGORY_LEVEL_NUM_ENTRIES' => $arrCategory['catNumEntries'],
                    ));

                    // As {@see static::createCategorieTree()} does generate
                    // URLs and is therefore costly for a large category tree,
                    // we shall generate its output only if the placeholder is
                    // actuall in use.
                    if (
                        $objTpl->placeholderExists(
                            $this->moduleLangVar.'_CATEGORY_LEVEL_CHILDREN'
                        )
                    ) {
                        $objTpl->setVariable(
                            $this->moduleLangVar.'_CATEGORY_LEVEL_CHILDREN',
                            $this->createCategorieTree($arrCategory, $levelId)
                        );
                    }

                    // parse thumbnails
                    if (!empty($arrCategory['catPicture'])) {
                        $arrThumbnails = array();
                        $imagePath = pathinfo($arrCategory['catPicture'], PATHINFO_DIRNAME);
                        $imageFilename = pathinfo($arrCategory['catPicture'], PATHINFO_BASENAME);
                        $thumbnails = $this->cx->getMediaSourceManager()->getThumbnailGenerator()->getThumbnailsFromFile($imagePath, $imageFilename, true);
                        foreach ($thumbnailFormats as $thumbnailFormat) {
                            if (!isset($thumbnails[$thumbnailFormat['size']])) {
                                continue;
                            }
                            $format = strtoupper($thumbnailFormat['name']);
                            $thumbnail = $thumbnails[$thumbnailFormat['size']];
                            $objTpl->setVariable(
                                $this->moduleLangVar.'_CATEGORY_LEVEL_THUMBNAIL_FORMAT_' . $format, $thumbnail
                            );
                        }
                    }

                    $objTpl->parse($this->moduleNameLC.'CategoriesLevels_row_'.$intBlockId);
                    $objTpl->clearVariables();

                    $strFirstIndexHeader = $strIndexHeader;
                }
                break;
            case 3:
                //Category Dropdown Menu
				$strDropdownOptions = '';
                foreach ($arrCategories as $arrCategory) {
                    $spacer = null;
                    $intSpacerSize = null;

                    if($arrCategory['catId'] == $intCategoryId) {
                        $strSelected = 'selected="selected"';
                    } else {
                        $strSelected = '';
                    }

                    //generate space
                    $intSpacerSize = (count($arrParentIds));
                    for($i = 0; $i < $intSpacerSize; $i++) {
                        $spacer .= "----";
                    }

                    if($spacer != null) {
                    	$spacer .= "&nbsp;";
                    }

                    $strDropdownOptions .= '<option value="'.$arrCategory['catId'].'" '.$strSelected.' >'.$spacer.contrexx_raw2xhtml($arrCategory['catName'][0]).'</option>';

                    if(!empty($arrCategory['catChildren'])) {
                        $arrParentIds[] = $arrCategory['catId'];
                        $strDropdownOptions .= self::listCategories($objTpl, 3, $intCategoryId, $arrParentIds);
                        @array_pop($arrParentIds);
                    }
                }

                return $strDropdownOptions;
                break;
            case 4:
                //Category Selector (modify view)
                if(!isset($this->arrSelectedCategories) && $intEntryId!=null) {
                    $this->arrSelectedCategories = array();

                    $objCategorySelector = $objDatabase->Execute("
                        SELECT
                            `category_id`
                        FROM
                            ".DBPREFIX."module_".$this->moduleTablePrefix."_rel_entry_categories
                        WHERE
                            `entry_id` = '".$intEntryId."'
                    ");

                    if ($objCategorySelector !== false) {
                        while (!$objCategorySelector->EOF) {
                            $this->arrSelectedCategories[] = intval($objCategorySelector->fields['category_id']);
                            $objCategorySelector->MoveNext();
                        }
                    }
                }

                foreach ($arrCategories as $arrCategory) {
                    $spacer = null;
                    $intSpacerSize = null;
                    $strOptionId = $arrCategory['catId'];

                     //generate space
                    $intSpacerSize = (count($arrParentIds));
                    for($i = 0; $i < $intSpacerSize; $i++) {
                        $spacer .= "----";
                    }

                    if($spacer != null) {
                        $spacer .= "&nbsp;";
                    }

                    if (
                        $this->arrSelectedCategories &&
                        in_array($arrCategory['catId'], $this->arrSelectedCategories)
                    ) {
                      $this->strSelectedOptions .= '<option name="'.$strOptionId.'" value="'.$arrCategory['catId'].'">'.$spacer.contrexx_raw2xhtml($arrCategory['catName'][0]).'</option>';
                    } else {
                      $this->strNotSelectedOptions .= '<option name="'.$strOptionId.'" value="'.$arrCategory['catId'].'">'.$spacer.contrexx_raw2xhtml($arrCategory['catName'][0]).'</option>';
                    }

                    $this->intCategoriesSortCounter++;
                    if(!empty($arrCategory['catChildren'])) {
                        $arrParentIds[] = $arrCategory['catId'];
                        self::listCategories($objTpl, 4, $intCategoryId, $arrParentIds, $intEntryId);
                        @array_pop($arrParentIds);
                    }
                }

                $arrSelectorOptions['selected'] = $this->strSelectedOptions;
                $arrSelectorOptions['not_selected'] = $this->strNotSelectedOptions;

                return $arrSelectorOptions;
                
                break;
            case 5:
                //Frontend View Detail
                $levelId = null;
                if (isset($requestParams['lid'])) {
                    $levelId = intval($requestParams['lid']);
                }
                
                $thumbImage = $this->getThumbImage($arrCategories[$intCategoryId]['catPicture']);
                $objTpl->setVariable(array(
                    $this->moduleLangVar.'_CATEGORY_LEVEL_TYPE' => 'category',
                    $this->moduleLangVar.'_CATEGORY_LEVEL_ID' => $arrCategories[$intCategoryId]['catId'],
                    $this->moduleLangVar.'_CATEGORY_LEVEL_NAME' => contrexx_raw2xhtml($arrCategories[$intCategoryId]['catName'][0]),
                    $this->moduleLangVar.'_CATEGORY_LEVEL_LINK' => '<a href="'.$this->getAutoSlugPath(null, $intCategoryId, $levelId).'">'.contrexx_raw2xhtml($arrCategories[$intCategoryId]['catName'][0]).'</a>',
                    $this->moduleLangVar.'_CATEGORY_LEVEL_LINK_SRC' => $this->getAutoSlugPath(null, $intCategoryId, $levelId),
                    $this->moduleLangVar.'_CATEGORY_LEVEL_DESCRIPTION' => $arrCategories[$intCategoryId]['catDescription'][0],
                    $this->moduleLangVar.'_CATEGORY_LEVEL_META_DESCRIPTION' => contrexx_raw2xhtml($arrCategories[$intCategoryId]['catMetaDesc'][0]),
                    $this->moduleLangVar.'_CATEGORY_LEVEL_PICTURE' => '<img src="'. $thumbImage .'" border="0" alt="'.$arrCategories[$intCategoryId]['catName'][0].'" />',
                    $this->moduleLangVar.'_CATEGORY_LEVEL_PICTURE_SOURCE' => $arrCategories[$intCategoryId]['catPicture'],
                    $this->moduleLangVar.'_CATEGORY_LEVEL_NUM_ENTRIES' => $arrCategories[$intCategoryId]['catNumEntries'],
                ));

                // parse thumbnails
                if ($thumbImage) {
                    $thumbnailFormats = $this->cx->getMediaSourceManager()->getThumbnailGenerator()->getThumbnails();
                    $arrThumbnails = array();
                    $imagePath = pathinfo($arrCategories[$intCategoryId]['catPicture'], PATHINFO_DIRNAME);
                    $imageFilename = pathinfo($arrCategories[$intCategoryId]['catPicture'], PATHINFO_BASENAME);
                    $thumbnails = $this->cx->getMediaSourceManager()->getThumbnailGenerator()->getThumbnailsFromFile($imagePath, $imageFilename, true);
                    foreach ($thumbnailFormats as $thumbnailFormat) {
                        if (!isset($thumbnails[$thumbnailFormat['size']])) {
                            continue;
                        }
                        $format = strtoupper($thumbnailFormat['name']);
                        $thumbnail = $thumbnails[$thumbnailFormat['size']];
                        $objTpl->setVariable(
                            $this->moduleLangVar.'_CATEGORY_LEVEL_THUMBNAIL_FORMAT_' . $format, $thumbnail
                        );
                    }
                }

                // parse GoogleMap
                $this->parseGoogleMapPlaceholder($objTpl, $this->moduleLangVar.'_CATEGORY_LEVEL_GOOGLE_MAP');

                if(!empty($arrCategories[$intCategoryId]['catPicture']) && $this->arrSettings['settingsShowCategoryImage'] == 1) {
                    $objTpl->parse($this->moduleNameLC.'CategoryLevelPicture');
                } else {
                    $objTpl->hideBlock($this->moduleNameLC.'CategoryLevelPicture');
                }

                if(!empty($arrCategories[$intCategoryId]['catDescription'][0]) && $this->arrSettings['settingsShowCategoryDescription'] == 1) {
                    $objTpl->parse($this->moduleNameLC.'CategoryLevelDescription');
                } else {
                    $objTpl->hideBlock($this->moduleNameLC.'CategoryLevelDescription');
                }

                if(!empty($arrCategories)) {
                    $objTpl->parse($this->moduleNameLC.'CategoryLevelDetail');
                } else {
                    $objTpl->hideBlock($this->moduleNameLC.'CategoryLevelDetail');
                }

                break;
            case 6:
                //Frontend Tree Placeholder

                $levelId = null;
                if (isset($requestParams['lid'])) {
                    $levelId = intval($requestParams['lid']);
                }
                foreach ($arrCategories as $arrCategory) {
                	$this->arrExpandedCategoryIds = array();
                    $bolExpandCategory = $this->getExpandedCategories($intCategoryId, array($arrCategory));
                    $strLinkClass = $bolExpandCategory ? 'active' : 'inactive';
                    $strListClass = 'level_'.intval(count($arrParentIds)+$intStartLevel);
                    
                    $linkMyselfAsCategory = false;
                    if (
                        \Cx\Core\Setting\Controller\Setting::getValue(
                            'CLX4789_LinkMyselfAsCategoryOrLevel',
                            'Config'
                        ) === 'on'
                    ) {
                        $linkMyselfAsCategory = true;
                    }
                    $this->strNavigationPlaceholder .= '<li class="'.$strListClass.'"><a href="'.$this->getAutoSlugPath(null, $arrCategory['catId'], $levelId, $linkMyselfAsCategory).'" class="'.$strLinkClass.'">'.contrexx_raw2xhtml($arrCategory['catName'][0]).'</a></li>';
            
                    $arrParentIds[] = $arrCategory['catId'];

                    //get children
                    if(!empty($arrCategory['catChildren']) && $arrCategory['catShowSubcategories'] == 1){
                    	if($bolExpandCategory) {
                            self::listCategories($objTpl, 6, $intCategoryId, $arrParentIds, null, null, $intStartLevel);
                    	}                    
                    }
                    @array_pop($arrParentIds);
                }
                
                return $this->strNavigationPlaceholder;
                
                break;
        }
    }



    function getExpandedCategories($intExpand, $arrData)
    {
        foreach ($arrData as $arrCategory) {
            if ($arrCategory['catId'] != $intExpand) {
                if(!empty($arrCategory['catChildren'])) {
                    $this->arrExpandedCategoryIds[] = $arrCategory['catId'];
                    $this->getExpandedCategories($intExpand, $arrCategory['catChildren']);
                }
            } else {
                $this->arrExpandedCategoryIds[] = $arrCategory['catId'];
                $this->arrExpandedCategoryIds[] = "found";
            }
        }

        if(in_array("found", $this->arrExpandedCategoryIds)) {
            return true;
        } else {
           return false;
        }


    }



    function saveCategory($arrData, $intCategoryId=null)
    {
        global $_ARRAYLANG, $_CORELANG, $objDatabase;

        //get data
        $intId = intval($intCategoryId);
        $intParentId = intval($arrData['categoryPosition']);
        $intShowEntries = intval($arrData['categoryShowEntries']);
        $intShowCategories = isset($arrData['categoryShowSubcategories']) ? contrexx_input2int($arrData['categoryShowSubcategories']) : 0;
        $intActive = intval($arrData['categoryActive']);
        $strPicture = contrexx_addslashes(contrexx_strip_tags($arrData['categoryImage']));
        
        $arrName = $arrData['categoryName'];
        
        $arrDescription = $arrData['categoryDescription'];

        $arrMetaDesc = $arrData['categoryMetaDesc'];

        // set default values taken from output locale
        if (empty($arrName[0])) {
            $arrName[0] = '[[' . $_ARRAYLANG['TXT_MEDIADIR_NEW_CATEGORY'] . ']]';
        }
        if (
            empty($arrMetaDesc[0]) &&
            isset($arrMetaDesc[static::getOutputLocale()->getId()])
        ) {
            $arrMetaDesc[0] = $arrMetaDesc[static::getOutputLocale()->getId()];
        }
        if (empty($arrMetaDesc[0])) {
            $arrMetaDesc[0] = '';
        }
                        
        if(empty($intId)) {
            //insert new category
            $objInsertAttributes = $objDatabase->Execute("
                INSERT INTO
                    ".DBPREFIX."module_".$this->moduleTablePrefix."_categories
                SET
                    `parent_id`='".$intParentId."',
                    `order`= 0,
                    `show_entries`='".$intShowEntries."',
                    `show_subcategories`='".$intShowCategories."',
                    `picture`='".$strPicture."',
                    `active`='".$intActive."'
            ");

            if($objInsertAttributes !== false) {
                $intId = $objDatabase->Insert_ID();

                foreach ($this->arrFrontendLanguages as $arrLang) {
                    $strName = $arrName[$arrLang['id']];
                    $strDescription = $arrDescription[$arrLang['id']];
                    $metaDesc = $arrMetaDesc[$arrLang['id']];

                    if(empty($strName)) $strName = $arrName[0];
                    if(empty($metaDesc)) $metaDesc = $arrMetaDesc[0];

                    $objInsertNames = $objDatabase->Execute("
                        INSERT INTO
                            ".DBPREFIX."module_".$this->moduleTablePrefix."_categories_names
                        SET
                            `lang_id`='".intval($arrLang['id'])."',
                            `category_id`='".intval($intId)."',
                            `category_name`='".contrexx_raw2db($strName)."',
                            `category_description`='".contrexx_raw2db($strDescription)."',
                            `category_metadesc`='".contrexx_input2db($metaDesc)."'
                    ");
                }

                if($objInsertNames !== false) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            //update category
            if($intParentId == $intCategoryId) {
                $parentSql = null;
            } else {
                $parentSql = "`parent_id`='".$intParentId."',";
            }

            $objUpdateAttributes = $objDatabase->Execute("
                UPDATE
                    ".DBPREFIX."module_".$this->moduleTablePrefix."_categories
                SET
                    ".$parentSql."
                    `show_entries`='".$intShowEntries."',
                    `show_subcategories`='".$intShowCategories."',
                    `picture`='".$strPicture."',
                    `active`='".$intActive."'
                WHERE
                    `id`='".$intId."'
            ");

            if($objUpdateAttributes !== false) {
                
                $objDeleteNames = $objDatabase->Execute("DELETE FROM ".DBPREFIX."module_".$this->moduleTablePrefix."_categories_names WHERE category_id='".$intId."'");

                if($objInsertNames !== false) {
                    foreach ($this->arrFrontendLanguages as $arrLang) {
                        $strName = $arrName[$arrLang['id']];
                        $strDescription = $arrDescription[$arrLang['id']];
                        $metaDesc = $arrMetaDesc[$arrLang['id']];

                        if(empty($strName)) $strName = $arrName[0];
                        if(empty($metaDesc)) $metaDesc = $arrMetaDesc[0];

                        $objInsertNames = $objDatabase->Execute("
                            INSERT INTO
                                ".DBPREFIX."module_".$this->moduleTablePrefix."_categories_names
                            SET
                                `lang_id`='".intval($arrLang['id'])."',
                                `category_id`='".intval($intId)."',
                                `category_name`='".contrexx_raw2db(contrexx_input2raw($strName))."',
                                `category_description`='".contrexx_raw2db(contrexx_input2raw($strDescription))."',
                                `category_metadesc`='".contrexx_input2db($metaDesc)."'
                        ");
                    }

                    if($objInsertNames !== false) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    }




    function deleteCategory($intCategoryId=null)
    {
        global $objDatabase;

        $intCategoryId = intval($intCategoryId);

        $objSubCategoriesRS = $objDatabase->Execute("SELECT id FROM ".DBPREFIX."module_".$this->moduleTablePrefix."_categories WHERE parent_id='".$intCategoryId."'");
        if ($objSubCategoriesRS !== false) {
            while (!$objSubCategoriesRS->EOF) {
                $intSubCategoryId = $objSubCategoriesRS->fields['id'];
                $this->deleteCategory($intSubCategoryId);
                $objSubCategoriesRS->MoveNext();
            };
        }

        $objDeleteCategoryRS = $objDatabase->Execute("DELETE FROM ".DBPREFIX."module_".$this->moduleTablePrefix."_categories WHERE id='$intCategoryId'");
        $objDeleteCategoryRS = $objDatabase->Execute("DELETE FROM ".DBPREFIX."module_".$this->moduleTablePrefix."_categories_names WHERE category_id='$intCategoryId'");
        $objDeleteCategoryRS = $objDatabase->Execute("DELETE FROM ".DBPREFIX."module_".$this->moduleTablePrefix."_rel_entry_categories WHERE category_id='$intCategoryId'");

        if ($objDeleteCategoryRS !== false) {
            return true;
        } else {
            return false;
        }
    }

    function saveOrder($arrData) {
        global $objDatabase;

        foreach($arrData['catOrder'] as $intCatId => $intCatOrder) {
            $objRSCatOrder = $objDatabase->Execute("UPDATE ".DBPREFIX."module_".$this->moduleTablePrefix."_categories SET `order`='".intval($intCatOrder)."' WHERE `id`='".intval($intCatId)."'");

            if ($objRSCatOrder === false) {
                return false;
            }
        }
        $this->loadCategories(false);

        return true;
    }

    /**
     * Return HTML representing the Category tree with links
     * @param   array   $arrCategory
     * @param   int     $levelId
     * @return  string
     */
    public function createCategorieTree($arrCategory, $levelId) {
        $childrenString = '<ul>';
        if (!empty($arrCategory['catChildren'])) {
            foreach ($arrCategory['catChildren'] as $children) {
                $childrenString .= '<li>'
                    .'<a href="'
                    . $this->getAutoSlugPath(null, $children['catId'], $levelId)
                    . '" data-id="' . $children['catId'] . '">'
                    . $children['catName'][0]
                    . '</a>';
                if (!empty($children['catChildren'])) {
                    $childrenString .= $this->createCategorieTree($children, $levelId);
                }
                $childrenString .= '</li>';
            }
        }
        $childrenString .= '</ul>';
        return $childrenString;
    }
}
