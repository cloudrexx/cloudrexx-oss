<?php
declare(strict_types=1);

/**
 * Cloudrexx
 *
 * @link      https://www.cloudrexx.com
 * @copyright Cloudrexx AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

namespace Cx\Modules\MediaDir\Testing\UnitTest;

/**
 * @copyright   Cloudrexx AG
 * @author      Thomas Wirz <thomas.wirz@cloudrexx.com>
 */
class MediaDirectoryLibraryTest extends \Cx\Core\Test\Model\Entity\ContrexxTestCase {
    public function testMediadirConfigFromTemplateEmpty() {
        $lib = new \Cx\Modules\MediaDir\Controller\MediaDirectoryLibrary('.', 'MediaDir');
        $lib->_objTpl->setTemplate('
            <!-- BEGIN test --><!-- END test -->
        ');
        $this->assertEquals(
            [
                'list' => [],
                'filter' => [],
                'sort' => [],
                'func' => [],
            ],
            $lib->fetchMediaDirListConfigFromTemplate('test', $lib->_objTpl)
        );
    }

    public function testMediadirConfigFromTemplateList() {
        $lib = new \Cx\Modules\MediaDir\Controller\MediaDirectoryLibrary('.', 'MediaDir');
        $lib->_objTpl->setTemplate('
            <!-- BEGIN test -->
            {MEDIADIR_CONFIG_LIST_LATEST}
            {MEDIADIR_CONFIG_LIST_LIMIT_99}
            {MEDIADIR_CONFIG_LIST_OFFSET_42}
            <!-- END test -->
        ');
        $this->assertEquals(
            [
                'list' => [
                    'latest' => true,
                    'limit' => 99,
                    'offset' => 42,
                ],
                'filter' => [],
                'sort' => [],
                'func' => [],
            ],
            $lib->fetchMediaDirListConfigFromTemplate('test', $lib->_objTpl)
        );
    }

    public function testMediadirConfigFromTemplateSort() {
        $lib = new \Cx\Modules\MediaDir\Controller\MediaDirectoryLibrary('.', 'MediaDir');
        $lib->_objTpl->setTemplate('
            <!-- BEGIN test -->
            {MEDIADIR_CONFIG_SORT_POPULAR}
            {MEDIADIR_CONFIG_SORT_ALPHABETICAL}
            <!-- END test -->
        ');
        $this->assertEquals(
            [
                'list' => [],
                'filter' => [],
                'sort' => [
                    'popular' => true,
                    'alphabetical' => true,
                ],
                'func' => [],
            ],
            $lib->fetchMediaDirListConfigFromTemplate('test', $lib->_objTpl)
        );
    }

    public function testMediadirConfigFromTemplateFunctions() {
        $lib = new \Cx\Modules\MediaDir\Controller\MediaDirectoryLibrary('.', 'MediaDir');
        $lib->_objTpl->setTemplate('
            <!-- BEGIN test -->
            {MEDIADIR_CONFIG_FUNC_NON_EXISTING_OPTION1_AS_BOOL}
            {MEDIADIR_CONFIG_FUNC_NON_EXISTING_OPTION2_WITH_INT_42}
            {MEDIADIR_CONFIG_FUNC_FALLBACK_LINK_MYSELF_AS_CATEGORY}
            {MEDIADIR_CONFIG_FUNC_FALLBACK_LINK_MYSELF_AS_LEVEL}
            {MEDIADIR_CONFIG_FUNC_FIELD_5_FILTER_BY_OR}
            <!-- END test -->
        ');
        $this->assertEquals(
            [
                'list' => [],
                'filter' => [],
                'sort' => [],
                'func' => [
                    'non_existing_option1_as_bool' => true,
                    'non_existing_option2_with_int' => 42,
                    'fallback_link_myself_as_category' => true,
                    'fallback_link_myself_as_level' => true,
                    'field_5_filter_by_or' => true,
                ],
            ],
            $lib->fetchMediaDirListConfigFromTemplate('test', $lib->_objTpl)
        );
    }

    public function testMediadirConfigFromTemplateFilter() {
        $lib = new \Cx\Modules\MediaDir\Controller\MediaDirectoryLibrary('.', 'MediaDir');
        $lib->_objTpl->setTemplate('
            <!-- BEGIN test -->
            {MEDIADIR_CONFIG_FILTER_FORM_3}
            {MEDIADIR_CONFIG_FILTER_CATEGORY_4}
            {MEDIADIR_CONFIG_FILTER_LEVEL_5}
            {MEDIADIR_CONFIG_FILTER_ASSOCIATED}
            <!-- END test -->
        ');
        $this->assertEquals(
            [
                'list' => [],
                'filter' => [
                    'form' => 3,
                    'category' => 4,
                    'level' => 5,
                    'associated' => true,
                ],
                'sort' => [],
                'func' => [],
            ],
            $lib->fetchMediaDirListConfigFromTemplate('test', $lib->_objTpl)
        );
    }

    public function testMediadirConfigFromTemplateFilterUsingAuto() {
        $lib = new \Cx\Modules\MediaDir\Controller\MediaDirectoryLibrary('.', 'MediaDir');
        $lib->_objTpl->setTemplate('
            <!-- BEGIN test -->
            {MEDIADIR_CONFIG_FILTER_FORM_3}
            {MEDIADIR_CONFIG_FILTER_CATEGORY_4}
            {MEDIADIR_CONFIG_FILTER_LEVEL_5}
            {MEDIADIR_CONFIG_FILTER_ASSOCIATED}
            {MEDIADIR_CONFIG_FILTER_AUTO}
            <!-- END test -->
        ');
        $form = 6;
        $category = 7;
        $level = 8;
        $this->assertEquals(
            [
                'list' => [],
                'filter' => [
                    'form' => $form,
                    'category' => $category,
                    'level' => $level,
                    'associated' => true,
                ],
                'sort' => [],
                'func' => [],
            ],
            $lib->fetchMediaDirListConfigFromTemplate('test', $lib->_objTpl, $form, $category, $level)
        );
    }
}
