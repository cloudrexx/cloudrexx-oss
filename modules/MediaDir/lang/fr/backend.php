<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * @copyright   CLOUDREXX CMS - CLOUDREXX AG
 * @author      Cloudrexx Development Team <info@cloudrexx.com>
 * @access      public
 * @package     cloudrexx
 * @subpackage  module_mediadir
 */
$_ARRAYLANG['TXT_MEDIADIR_OVERVIEW'] = "Aperçu";
$_ARRAYLANG['TXT_MEDIADIR_EDIT'] = "Modifier";
$_ARRAYLANG['TXT_MEDIADIR_DELETE'] = "Supprimer";
$_ARRAYLANG['TXT_MEDIADIR_DETAIL'] = "Détail";
$_ARRAYLANG['TXT_MEDIADIR_ADD'] = "Ajouter";
$_ARRAYLANG['TXT_MEDIADIR_MORE'] = "élargi";
$_ARRAYLANG['TXT_MEDIADIR_PLEASE_CHECK_INPUT'] = "Merci de vérifier vos données";
$_ARRAYLANG['TXT_MEDIADIR_VOTING'] = "évaluer";
$_ARRAYLANG['TXT_MEDIADIR_VOTES'] = "Voter";
$_ARRAYLANG['TXT_MEDIADIR_VOTE'] = "évaluer";
$_ARRAYLANG['TXT_MEDIADIR_HITS'] = "Hits";
$_ARRAYLANG['TXT_MEDIADIR_AUTHOR'] = "Auteur";
$_ARRAYLANG['TXT_MEDIADIR_CONFIRM_DELETE_DATA'] = "Voulez-vous vraiment supprimer ces données?";
$_ARRAYLANG['TXT_MEDIADIR_ACTION_IS_IRREVERSIBLE'] = "Ces données vont supprimer sans recours.";
$_ARRAYLANG['TXT_MEDIADIR_CATEGORIES'] = "Catégories";
$_ARRAYLANG['TXT_MEDIADIR_CATEGORY'] = "Catégorie";
$_ARRAYLANG['TXT_MEDIADIR_LEVELS'] = "Niveau";
$_ARRAYLANG['TXT_MEDIADIR_LEVEL'] = "Niveau";
$_ARRAYLANG['TXT_MEDIADIR_LATEST_ENTRIES'] = "Les dernières entrées";
$_ARRAYLANG['TXT_MEDIADIR_MUSTFIELD'] = "Champs requis";
$_ARRAYLANG['TXT_MEDIADIR_ENTRY'] = "Entrée";
$_ARRAYLANG['TXT_MEDIADIR_IMPORT'] = "Import";
$_ARRAYLANG['TXT_MEDIADIR_TITLE'] = "Titre";
$_ARRAYLANG['TXT_MEDIADIR_GOOGLE'] = "Google carte d'ensemble";
$_ARRAYLANG['TXT_MEDIADIR_HITS_AND_LATEST'] = "Hits & les dernières entrées";
$_ARRAYLANG['TXT_MEDIADIR_EXPORT'] = "Export";
$_ARRAYLANG['TXT_MEDIADIR_INTERFACES'] = "Interface de transmission";
$_ARRAYLANG['TXT_MEDIADIR_CONFIRM'] = "confirmer";
$_ARRAYLANG['TXT_MEDIADIR_VIEW'] = "afficher";
$_ARRAYLANG['TXT_MEDIADIR_ACTIVATE'] = "activer";
$_ARRAYLANG['TXT_MEDIADIR_DEAVTIVATE'] = "désactiver";
$_ARRAYLANG['TXT_MEDIADIR_VISIBLE'] = "Visible";
$_ARRAYLANG['TXT_MEDIADIR_INVISIBLE'] = "Invisible";
$_ARRAYLANG['TXT_MEDIADIR_VISIBLE_CATEGORY_INFO'] = "Les catégories invisibles ne sont pas répertoriées dans le frontend, mais peuvent être liées. Les entrées ne peuvent être saisies que dans les catégories visibles, sauf dans le backend.";
$_ARRAYLANG['TXT_MEDIADIR_VISIBLE_LEVEL_INFO'] = "Les calques invisibles ne sont pas répertoriés dans le frontend, mais peuvent être liés. Les entrées ne peuvent être attribuées qu'à des niveaux visibles, sauf dans le backend.";
$_ARRAYLANG['TXT_MEDIADIR_SHOW_SUBLEVELS'] = "Afficher sous-niveau";
$_ARRAYLANG['TXT_MEDIADIR_SHOW_CATEGORIES'] = "Afficher Catégories";
$_ARRAYLANG['TXT_MEDIADIR_NUM'] = "Nombre";
$_ARRAYLANG['TXT_MEDIADIR_COMMUNITY_GROUP'] = "Groupe Communauté";
$_ARRAYLANG['TXT_MEDIADIR_NO_COMMUNITY_GROUPS'] = "pas de Groupe Communauté existant";
$_ARRAYLANG['TXT_MEDIADIR_MINIMIZE'] = "Simplifié";
$_ARRAYLANG['TXT_MEDIADIR_GLOBAL'] = "Général";
$_ARRAYLANG['TXT_MEDIADIR_EXP_SEARCH'] = "Recherche avancée";
$_ARRAYLANG['TXT_MEDIADIR_INPUTFIELDS'] = "Champ de saisie";
$_ARRAYLANG['TXT_MEDIADIR_PLACEHOLDER'] = "Garde-Place";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_INPUTFIELD_SYSTEM_FIELD_CANT_DELETE'] = "Il s'agit d'un champ système et ne peut pas être supprimé.";
$_ARRAYLANG['TXT_MEDIADIR_POPULAR_HITS'] = "Succès populaires";
$_ARRAYLANG['TXT_MEDIADIR_MOVE_ALL'] = "Transférer entrées sélectionnées";
$_ARRAYLANG['TXT_MEDIADIR_RESTORE_VOTING_ALL'] = "Remettre Votation";
$_ARRAYLANG['TXT_MEDIADIR_MAKE_SELECTION'] = "Veuillez sélectionner un fichier.";
$_ARRAYLANG['TXT_MEDIADIR_MAIL_TEMPLATES'] = "Modèles d'E-Mail";
$_ARRAYLANG['TXT_MEDIADIR_MAIL_TEMPLATE'] = "Modèle d'E-Mail";
$_ARRAYLANG['TXT_MEDIADIR_NEW_MAIL_TEMPLATE'] = "Nouveau modèle d'E-Mail";
$_ARRAYLANG['TXT_MEDIADIR_EDIT_MAIL_TEMPLATE'] = "Modifier modèle d'E-Mail";
$_ARRAYLANG['TXT_MEDIADIR_LINK'] = "Lien à l'entrée";
$_ARRAYLANG['TXT_MEDIADIR_NEW_CATEGORY'] = "Nouvelle catégorie";
$_ARRAYLANG['TXT_MEDIADIR_NEW_LEVEL'] = "Nouveau niveau";
$_ARRAYLANG['TXT_MEDIADIR_ADD_ENTRY'] = "Nouvelle entrée";
$_ARRAYLANG['TXT_MEDIADIR_ENTRIES'] = "Entrées";
$_ARRAYLANG['TXT_MEDIADIR_MANAGE_ENTRIES'] = "Gérer des entrées";
$_ARRAYLANG['TXT_MEDIADIR_CONFIRM_ALL'] = "Confirmer les entrées sélectionnées";
$_ARRAYLANG['TXT_MEDIADIR_ALERT_MAKE_SELECTION'] = "Veuillez sélectionner une entrée au minimum.";
$_ARRAYLANG['TXT_MEDIADIR_CONFIRM_LIST'] = "Liste de confirmation";
$_ARRAYLANG['TXT_MEDIADIR_FUNCTIONS'] = "Fonctions";
$_ARRAYLANG['TXT_MEDIADIR_SHOW_ENTRIES'] = "Afficher des entrées";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_SHOW_CATEGORY_DESC'] = "Afficher la description de la catégorie";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_SHOW_CATEGORY_IMG'] = "Afficher l'image de la catégorie";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_SHOW_LEVEL_DESC'] = "Afficher la description du niveau";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_SHOW_LEVEL_IMG'] = "Afficher l'image du niveau";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_NUM_ENTRIES_PER_GROUP'] = "Nombre maximum d'entrées par utilisateur";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_NUM_ENTRIES_PER_GROUP_INFO'] = "Le nombre maximum d'entrées pouvant être enregistrées par utilisateur est attribué aux différents groupes communautaires dans lesquels se trouvent les utilisateurs.<br /><br />n = illimité";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_SHOW_CATEGORY_DESC_INFO'] = "Si cette option est désactivée, la description d'une catégorie ne sera pas affichée.";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_SHOW_CATEGORY_IMG_INFO'] = "Si cette option est désactivée, l'image de la catégorie ne sera pas affichée.";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_SHOW_LEVEL_DESC_INFO'] = "Si cette option est désactivée, la description d'une couche ne sera pas affichée.";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_SHOW_LEVEL_IMG_INFO'] = "Si cette option est désactivée, l'image en couches ne sera pas affichée.";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_SHOW_LEVELS_INFO'] = "Si cette option est désactivée, seules les catégories seront utilisées. Si cette option est activée, les niveaux sont utilisés en plus des catégories.<br /><br />Chaque niveau contient toutes les catégories. Cependant, une catégorie ne peut contenir aucun niveau.";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_CATEGORY_ORDER'] = "Vue liste des catégories";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_LEVEL_ORDER'] = "Affichage de liste des niveaux";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_ORDER_USER'] = "dans l'ordre spécifié";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_ORDER_ABC'] = "Assorti alphabétique";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_ORDER_INDEX'] = "Groupé alphabétique";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_COUNT_ENTRIES'] = "Compter les entrées";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_COUNT_ENTRIES_INFO'] = "Si cette option est désactivée, les participations dans les catégories et niveaux ne seront plus comptabilisées.<br /><br />Pour de nombreuses participations, la vitesse de chargement peut être optimisée en les désactivant.";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_PLACEHOLDER_DESCRIPTION'] = "Ceci est le bloc et les espaces réservés disponibles pour <b>%i</b>";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_INPUTFIELDS_ADD_NEW'] = "Ajouter un champ";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_CONFIRM_NEW_ENTRIES'] = "Les inscriptions nouvellement saisies doivent être confirmées";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_CONFIRM_NEW_ENTRIES_INFO'] = "Si cette option est désactivée, les entrées nouvellement saisies doivent être confirmées par une personne autorisée.<br /><br />Les entrées à confirmer sont répertoriées sur la page d'aperçu.<br /><br />Les entrées saisies dans le backend sont automatiquement activé.";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_ALLOW_ADD_ENTRIES'] = "Autoriser les visiteurs à capturer des publications";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_ALLOW_ADD_ENTRIES_INFO'] = "Si cette option est désactivée, les visiteurs ne peuvent plus saisir d'entrées dans le frontend.";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_ADD_ONLY_COMMUNITY'] = "Seuls les utilisateurs enregistrés sont autorisés à créer des entrées";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_ADD_ONLY_COMMUNITY_INFO'] = "Si cette option est activée, seuls les visiteurs enregistrés peuvent saisir des entrées dans le frontend.<br /><br />Le module d'accès nécessite cette option.";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_ALLOW_EDIT_ENTRIES'] = "Autoriser les visiteurs à modifier leurs propres entrées";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_ALLOW_EDIT_ENTRIES_INFO'] = "Si cette option est activée, les visiteurs enregistrés peuvent modifier leurs entrées enregistrées dans le frontend.<br /><br />La condition préalable est d'activer l'option \"Seuls les utilisateurs enregistrés sont autorisés à créer des entrées\".";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_ALLOW_DEL_ENTRIES'] = "Autoriser les visiteurs à supprimer leurs propres entrées";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_ALLOW_DEL_ENTRIES_INFO'] = "Si cette option est activée, les visiteurs enregistrés peuvent supprimer leurs entrées enregistrées dans le frontend.<br /><br />La condition préalable est d'activer l'option \"Seuls les utilisateurs enregistrés sont autorisés à créer des entrées\".";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_LATEST_NUM_XML'] = "Nombre d'entrées les plus récentes dans le flux XML";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_LATEST_NUM_BACKEND'] = "Nombre d'entrées les plus récentes dans le backend";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_LATEST_NUM_FRONTEND'] = "Nombre d'entrées les plus récentes dans le frontend";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_POPULAR_NUM_FRONTEND'] = "Nombre d'entrées populaires dans le frontend";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_POPULAR_NUM_RESTORE'] = "Après combien de jours les hits populaires doivent-ils être réinitialisés.";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_GOOGLE_START_POSITION'] = "Carte d'ensemble de la position de départ";
$_ARRAYLANG['TXT_MEDIADIR_FIELD_NAME'] = "Nom du champ";
$_ARRAYLANG['TXT_MEDIADIR_FIELD_TYPE'] = "Type du champ";
$_ARRAYLANG['TXT_MEDIADIR_DEFAULTVALUE'] = "Valeur par défaut";
$_ARRAYLANG['TXT_MEDIADIR_VALUE_CHECK'] = "Validation";
$_ARRAYLANG['TXT_MEDIADIR_SHOW_BACK_N_FRONTEND'] = "Back- et Frontend";
$_ARRAYLANG['TXT_MEDIADIR_SHOW_FRONTEND'] = "juste Frontend";
$_ARRAYLANG['TXT_MEDIADIR_SHOW_BACKEND'] = "juste Backend";
$_ARRAYLANG['TXT_MEDIADIR_FIELD_SHOW_IN'] = "Afficher dans";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_CONFIRM_UPDATED_ENTRIES'] = "Les entrées modifiées doivent être confirmées à nouveau";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_CONFIRM_UPDATED_ENTRIES_INFO'] = "Si cette option est désactivée, les entrées modifiées doivent être à nouveau confirmées par une personne autorisée.<br /><br />Les entrées à confirmer sont répertoriées sur la page d'aperçu.<br /><br />Les entrées modifiées dans le backend sont automatiquement réactivé.";
$_ARRAYLANG['TXT_MEDIADIR_SHOW_SUBCATEGORIES'] = "Afficher sous-catégories";
$_ARRAYLANG['TXT_MEDIADIR_RECIPIENTS'] = "Receveurs additionnels";
$_ARRAYLANG['TXT_MEDIADIR_INPUTFIELD_TYPE_TEXT'] = "Champ texte";
$_ARRAYLANG['TXT_MEDIADIR_INPUTFIELD_TYPE_TEXTAREA'] = "Champ texte de plusieurs lignes";
$_ARRAYLANG['TXT_MEDIADIR_INPUTFIELD_TYPE_DROPDOWN'] = "Menu déroulant";
$_ARRAYLANG['TXT_MEDIADIR_INPUTFIELD_TYPE_RADIO'] = "Boutons radio";
$_ARRAYLANG['TXT_MEDIADIR_INPUTFIELD_TYPE_CHECKBOX'] = "Case à cocher";
$_ARRAYLANG['TXT_MEDIADIR_INPUTFIELD_TYPE_FILE'] = "Télécharger fichier";
$_ARRAYLANG['TXT_MEDIADIR_INPUTFIELD_TYPE_IMAGE'] = "Fichier d'image";
$_ARRAYLANG['TXT_MEDIADIR_INPUTFIELD_TYPE_RATING'] = "évaluation";
$_ARRAYLANG['TXT_MEDIADIR_INPUTFIELD_TYPE_LINK'] = "Lien";
$_ARRAYLANG['TXT_MEDIADIR_INPUTFIELD_TYPE_LINKGROUP'] = "Groupe de lien";
$_ARRAYLANG['TXT_MEDIADIR_INPUTFIELD_TYPE_GOOGLEMAP'] = "Google Map";
$_ARRAYLANG['TXT_MEDIADIR_INPUTFIELD_VERIFICATION_NORMAL'] = "égal";
$_ARRAYLANG['TXT_MEDIADIR_INPUTFIELD_VERIFICATION_E-MAIL'] = "Adresse email";
$_ARRAYLANG['TXT_MEDIADIR_INPUTFIELD_VERIFICATION_URL'] = "Adresse internet";
$_ARRAYLANG['TXT_MEDIADIR_INPUTFIELD_VERIFICATION_LETTERS'] = "Juste des lettres";
$_ARRAYLANG['TXT_MEDIADIR_INPUTFIELD_VERIFICATION_NUMBERS'] = "Juste des chiffres";
$_ARRAYLANG['TXT_MEDIADIR_INPUTFIELD_CONTEXT_NONE'] = "Rien";
$_ARRAYLANG['TXT_MEDIADIR_INPUTFIELD_CONTEXT_TITLE'] = "Titre";
$_ARRAYLANG['TXT_MEDIADIR_INPUTFIELD_CONTEXT_ADDRESS'] = "Adresse";
$_ARRAYLANG['TXT_MEDIADIR_INPUTFIELD_CONTEXT_ZIP'] = "Code postale";
$_ARRAYLANG['TXT_MEDIADIR_INPUTFIELD_CONTEXT_CITY'] = "Ville";
$_ARRAYLANG['TXT_MEDIADIR_INPUTFIELD_CONTEXT_COUNTRY'] = "Pays";
$_ARRAYLANG['TXT_MEDIADIR_INPUTFIELD_CONTEXT_IMAGE'] = "Image";
$_ARRAYLANG['TXT_MEDIADIR_SUCCESSFULLY_ADDED'] = "ajouté avec succès.";
$_ARRAYLANG['TXT_MEDIADIR_CORRUPT_ADDED'] = "N'a pu être ajouté.";
$_ARRAYLANG['TXT_MEDIADIR_SUCCESSFULLY_EDITED'] = "modifié avec succès.";
$_ARRAYLANG['TXT_MEDIADIR_CORRUPT_EDITED'] = "N'a pu être modifié.";
$_ARRAYLANG['TXT_MEDIADIR_SUCCESSFULLY_DELETED'] = "supprimé avec succès.";
$_ARRAYLANG['TXT_MEDIADIR_CORRUPT_DELETED'] = "N'a pu être supprimé.";
$_ARRAYLANG['TXT_MEDIADIR_SUCCESSFULLY_CONFIRM'] = "confirmé avec succès.";
$_ARRAYLANG['TXT_MEDIADIR_CORRUPT_CONFIRM'] = "konnte nicht bestätigt werden.";
$_ARRAYLANG['TXT_MEDIADIR_MAIL_ACTION_NEWENTRY'] = "Nouvel entrée ajouté";
$_ARRAYLANG['TXT_MEDIADIR_MAIL_ACTION_ENTRYADDED'] = "Entrée ajouté avec succès";
$_ARRAYLANG['TXT_MEDIADIR_MAIL_ACTION_ENTRYCONFIRMED'] = "Entré a été confirmé";
$_ARRAYLANG['TXT_MEDIADIR_MAIL_ACTION_ENTRYVOTED'] = "Entré a été évaluer";
$_ARRAYLANG['TXT_MEDIADIR_MAIL_ACTION_ENTRYDELETED'] = "Entré a été supprimer";
$_ARRAYLANG['TXT_MEDIADIR_MAIL_ACTION_NEWCOMMENT'] = "Nouvelle remarque a été ajouté";
$_ARRAYLANG['TXT_MEDIADIR_MAIL_ACTION_ENTRYEDITED'] = "Entrée a été modifier";
$_ARRAYLANG['TXT_MEDIADIR_PICS_AND_FILES'] = "Images & Fichiers";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_THUMB_SIZE'] = "Largeur de la vignette en pixels";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_ENCRYPT_FILENAME'] = "Chiffrer les noms des fichiers téléchargés";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_THUMB_SIZE_INFO'] = "Lors du téléchargement d'un fichier image, une vignette avec la largeur spécifiée a été automatiquement créée. La hauteur est calculée proportionnellement.";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_ENCRYPT_FILENAME_INFO'] = "Les noms de fichiers des fichiers téléchargés sont cryptés à l'aide d'une procédure.";
$_ARRAYLANG['TXT_MEDIADIR_FORMS'] = "Modèle de formulair";
$_ARRAYLANG['TXT_MEDIADIR_FORM'] = "Formulaire";
$_ARRAYLANG['TXT_MEDIADIR_FORM_DEL_INFO'] = "Toutes les entrées contenues dans ce document seront supprimées.";
$_ARRAYLANG['TXT_MEDIADIR_FORM_TEMPLATE'] = "Modèle de formulaire";
$_ARRAYLANG['TXT_MEDIADIR_NEW_FORM_TEMPLATE'] = "Nouveau modèle de formulaire";
$_ARRAYLANG['TXT_MEDIADIR_EDIT_FORM_TEMPLATE'] = "Modifier modèle de formulaire";
$_ARRAYLANG['TXT_MEDIADIR_CHOOSE_FORM'] = "Sélectionner typ d'entré";
$_ARRAYLANG['TXT_MEDIADIR_CHOOSE'] = "Veuillez sélectionner";
$_ARRAYLANG['TXT_MEDIADIR_GOOGLE_MAP_LON'] = "Longitude";
$_ARRAYLANG['TXT_MEDIADIR_GOOGLE_MAP_LAT'] = "Latitude";
$_ARRAYLANG['TXT_MEDIADIR_GOOGLE_MAP_ZOOM'] = "Zoom";
$_ARRAYLANG['TXT_MEDIADIR_GOOGLE_MAP_SEARCH_ADDRESS'] = "Chercher une adresse";
$_ARRAYLANG['TXT_MEDIADIR_GOOGLE_MAP_STREET'] = "Adresse";
$_ARRAYLANG['TXT_MEDIADIR_GOOGLE_MAP_ZIP'] = "Code Postale";
$_ARRAYLANG['TXT_MEDIADIR_GOOGLE_MAP_CITY'] = "Ville";
$_ARRAYLANG['TXT_MEDIADIR_INPUTFIELD_TYPE_WYSIWYG'] = "Wysiwyg Editor";
$_ARRAYLANG['TXT_MEDIADIR_INPUTFIELD_TYPE_CLASSIFICATION'] = "Classification";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_GLOBAL_PLACEHOLDER_INFO'] = "Bloc global pour tous les champs de saisie. Ce bloc sépare automatiquement tous les champs de saisie remplis dans l'ordre défini.";
$_ARRAYLANG['TXT_MEDIADIR_RESTORE'] = "Remettre";
$_ARRAYLANG['TXT_MEDIADIR_COMMENTS'] = "Remarques";
$_ARRAYLANG['TXT_MEDIADIR_CLASSIFICATION'] = "Classification";
$_ARRAYLANG['TXT_MEDIADIR_COMMENTS_AND_VOTING'] = "Remarques & évaluations";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_ALLOW_VOTES'] = "Autoriser les visiteurs à évaluer les publications";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_VOTE_ONLY_COMMUNITY'] = "Seuls les utilisateurs enregistrés sont autorisés à évaluer les entrées";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_DELETE_ALL_VOTES'] = "Supprimer tous les évaluations";
$_ARRAYLANG['TXT_MEDIADIR_RESTORE_COMMENTS_ALL'] = "Réinitialiser les commentaires";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_ALLOW_COMMENTS'] = "Autoriser les visiteurs à commenter les entrées";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_COMMENT_ONLY_COMMUNITY'] = "Seuls les utilisateurs enregistrés sont autorisés à poster des commentaires";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_DELETE_ALL_COMMENTS'] = "Supprimer tous les remarques";
$_ARRAYLANG['TXT_MEDIADIR_LEVELS_AND_CATEGORIES'] = "Niveaux & Catégories";
$_ARRAYLANG['TXT_MEDIADIR_ID_OR_SEARCH_TERM'] = "Chercher une entrée (ID ou terme de recherche)";
$_ARRAYLANG['TXT_MEDIADIR_ALL_LEVELS'] = "Tous les niveaux";
$_ARRAYLANG['TXT_MEDIADIR_ALL_CATEGORIES'] = "Tous les catégories";
$_ARRAYLANG['TXT_MEDIADIR_INPUTFIELD_TYPE_MAIL'] = "E-Mail";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_GOOGLE_NO_KEY'] = "Aucune clé API Google Maps n'a été trouvée.";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_GOOGLE_PLEASE_ENTER_KEY_IN_GLOBAL_SETTINGS'] = "Veuillez saisir la clé dans les <a href=\"index.php?cmd=Config\">Paramètres de base</a>.";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_NUM_CATEGORIES_PER_GROUP'] = "Nombre maximum de catégories sélectionnables";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_NUM_LEVELS_PER_GROUP'] = "Nombre maximum de niveaux sélectionnables";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_NUM_CATEGORIES_PER_GROUP_INFO'] = "Le nombre maximum de catégories pouvant être sélectionnées par entrée est attribué aux groupes communautaires individuels dans lesquels se trouvent les utilisateurs.<br /><br />n = illimité";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_NUM_LEVELS_PER_GROUP_INFO'] = "Le nombre maximum de niveaux pouvant être sélectionnés par entrée est attribué aux groupes communautaires individuels dans lesquels se trouvent les utilisateurs.<br /><br />n = illimité";
$_ARRAYLANG['TXT_MEDIADIR_MANAGE_COMMENTS'] = "Gérer les commentaires";
$_ARRAYLANG['TXT_MEDIADIR_ADDED_BY'] = "Ajouté par";
$_ARRAYLANG['TXT_MEDIADIR_IP'] = "IP Adresse";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_GOOGLE_ALLOW_KML'] = "Autoriser les fichiers d'itinéraire Google (*.kml).";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_GOOGLE_MAP_TYPE'] = "Définir le type de carte";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_GOOGLE_MAP_TYPE_MAP'] = "Carte";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_GOOGLE_MAP_TYPE_SATELLITE'] = "Satellite";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_GOOGLE_MAP_TYPE_HYBRID'] = "Hybride";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_PERMISSIONS_INFO'] = "Ci-dessous, il faut définir quels groupes d'utilisateurs sont autorisés à créer des entrées de ce type. Ces paramètres s'appliquent <b>uniquement dans le frontend</b> et si <b>plusieurs formulaires sont activés</b>.";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_ALLOW_GHROUP_ADD_ENTRIES'] = "Autoriser la saisie des entrées";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_SHOW_ENTRIES_IN_ALL_LANG'] = "Afficher les entrées dans toutes les langues";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_SHOW_ENTRIES_IN_ALL_LANG_INFO'] = "Si cette option est activée, toutes les entrées dans toutes les langues seront affichées dans le frontend. Si cette option est désactivée, les entrées seront affichées uniquement dans la langue dans laquelle elles ont été enregistrées.";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_PAGING_NUM_ENTRIES'] = "Nombre d'entrées par page";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_PAGING_NUM_ENTRIES_INFO'] = "Avec cet affichage, vous pouvez définir combien d'entrées ou de résultats de recherche doivent être affichés par page.";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_CLASSIFICATION_POINTS'] = "Nombre de points pour le classement";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_CLASSIFICATION_POINTS_INFO'] = "Avec ces informations, vous pouvez définir le nombre de points/étoiles fournis pour le champ de classification.";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_CLASSIFICATION_SEARCH'] = "Comment fonctionne la recherche";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_CLASSIFICATION_SEARCH_FROM'] = "Loin";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_CLASSIFICATION_SEARCH_TO'] = "Jusqu'à";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_CLASSIFICATION_SEARCH_EXACT'] = "Exactement";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_CLASSIFICATION_SEARCH_INFO'] = "Avec cette option, vous définissez le fonctionnement de la recherche. De : Toutes les entrées sont affichées qui ont au moins cette valeur en points - À : Toutes les entrées sont affichées qui ont un maximum de cette valeur en points - Exact : Toutes les entrées sont affichées qui ont exactement cette valeur en points";
$_ARRAYLANG['TXT_MEDIADIR_SPEZ_FIELDS'] = "Données spéciales";
$_ARRAYLANG['TXT_MEDIADIR_CHOOSE_USER'] = "Sélectionner Utilisateur";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_DEFAULT_DISPLAYDURATION'] = "Durée d'affichage Standard";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_DEFAULT_DISPLAYDURATION_INFO'] = "Avec cette option, vous définissez la durée d'affichage par défaut d'une entrée. Ces informations peuvent également être définies individuellement pour chaque entrée.";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_NOTIFICATION_DISPLAYDURATION'] = "Notification à l'expiration";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_NOTIFICATION_DISPLAYDURATION_INFO'] = "Notification à l'expiration";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_NOTIFICATION_DISPLAYDURATION_DAYSBEFOR'] = "Jour(s) avant expiration";
$_ARRAYLANG['TXT_MEDIADIR_DISPLAYDURATION'] = "Durée d'affichage";
$_ARRAYLANG['TXT_MEDIADIR_DISPLAYDURATION_ALWAYS'] = "Illimité";
$_ARRAYLANG['TXT_MEDIADIR_DISPLAYDURATION_PERIOD'] = "Zeitspanne";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_DISPLAYDURATION_VALUE_TYPE_DAY'] = "Jour(s)";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_DISPLAYDURATION_VALUE_TYPE_MONTH'] = "Mois";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_DISPLAYDURATION_VALUE_TYPE_YEAR'] = "An(s)";
$_ARRAYLANG['TXT_MEDIADIR_MAIL_ACTION_NOTIFICATIONDISPLAYDURATION'] = "Notification d'expiration de la durée de visionnage";

$_ARRAYLANG['TXT_MEDIADIR_SPEZ_FIELDS'] = "Données spéciales";
$_ARRAYLANG['TXT_MEDIADIR_CHOOSE_USER'] = "Sélectionner Utilisateur";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_DEFAULT_DISPLAYDURATION'] = "Durée d'affichage Standard";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_DEFAULT_DISPLAYDURATION_INFO'] = "Avec cette option, vous définissez la durée d'affichage par défaut d'une entrée. Ces informations peuvent également être définies individuellement pour chaque entrée.";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_NOTIFICATION_DISPLAYDURATION'] = "Notification à l'expiration";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_NOTIFICATION_DISPLAYDURATION_INFO'] = "Lorsque la période d'affichage d'une participation expire, un e-mail de notification est déclenché.";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_NOTIFICATION_DISPLAYDURATION_DAYSBEFOR'] = "Jour(s) avant l'expiration";
$_ARRAYLANG['TXT_MEDIADIR_DISPLAYDURATION'] = "Durée d'affichage";
$_ARRAYLANG['TXT_MEDIADIR_DISPLAYDURATION_ALWAYS'] = "Illimitée";
$_ARRAYLANG['TXT_MEDIADIR_DISPLAYDURATION_PERIOD'] = "Période";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_DISPLAYDURATION_VALUE_TYPE_DAY'] = "Jour(s)";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_DISPLAYDURATION_VALUE_TYPE_MONTH'] = "Mois";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_DISPLAYDURATION_VALUE_TYPE_YEAR'] = "An(s)";
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_DISPLAYDURATION_VALUE_TYPE_YEAR'] = "An(s)";
$_ARRAYLANG['TXT_MEDIADIR_MAIL_ACTION_NOTIFICATIONDISPLAYDURATION'] = "Notification d'expiration de la durée de visionnage";
$_ARRAYLANG['TXT_MEDIADIR_DISPLAYDURATION_RESET_NOTIFICATION_STATUS'] = "Réinitialiser l'état des notifications?";
$_ARRAYLANG['TXT_MEDIADIR_DISPLAYNAME'] = 'Nom d`affichage';
$_ARRAYLANG['TXT_MEDIADIR_NO_ENTRIES_FOUND'] = 'pas trouver des entrées';
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_TRANSLATION_STATUS'] = 'Statuts de la traduction';
$_ARRAYLANG['TXT_MEDIADIR_TRANSLATION_STATUS'] = 'Statuts de la traduction';
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_TRANSLATION_STATUS_INFO'] = 'Si le statut de traduction est activé, vous pouvez indiquer pour chaque entrée individuelle si une langue a été entièrement traduite. Si une langue n\'a pas encore été traduite, la langue dans laquelle l\'entrée a été enregistrée sera affichée.';
$_ARRAYLANG['TXT_MEDIADIR_CMD'] = 'Parameter (cmd)';
$_ARRAYLANG['TXT_MEDIADIR_CMD_INFO'] = 'Avec ce paramètre, la sélection des entrées sur la page du module peut être réduite à ce modèle de formulaire.';
$_ARRAYLANG['TXT_MEDIADIR_USE_CATEGORY'] = 'Utiliser les catégories';
$_ARRAYLANG['TXT_MEDIADIR_USE_CATEGORY_INFO'] = 'Avec cette option, l"attribution de catégories pour ce modèle de formulaire peut être activée ou désactivée.';
$_ARRAYLANG['TXT_MEDIADIR_USE_LEVEL'] = 'Utiliser des calques';
$_ARRAYLANG['TXT_MEDIADIR_USE_LEVEL_INFO'] = 'Avec cette option, l"attribution de niveaux pour ce modèle de formulaire peut être activée ou désactivée.';
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_READY_TO_CONFIRM'] = 'Les visiteurs doivent approuver les entrées pour vérification';
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_READY_TO_CONFIRM_INFO'] = 'Si ce paramètre est activé, le visiteur doit explicitement approuver son entrée pour examen en sélectionnant une option. Sinon, l"entrée ne peut pas être vérifiée.<br /><br />Il est recommandé d"activer ce paramètre pour les formulaires plus grands afin qu"une entrée ne doive pas être saisie en une seule fois et puisse donc être temporairement enregistrée.';
$_ARRAYLANG['TXT_MEDIADIR_INPUTFIELD_TYPE_COUNTRY'] = 'Pays';
$_ARRAYLANG['TXT_MEDIADIR_ALL_FORMS'] = 'Tous les modèles de formulaire';
$_ARRAYLANG['TXT_MEDIADIR_FORM'] = 'Formulaire';
$_ARRAYLANG['TXT_MARKETPLACE_SETTINGS_IMAGE_FILESIZE'] = 'Taille d`une image';
$_ARRAYLANG['TXT_MARKETPLACE_SETTINGS_IMAGE_FILESIZE_INFO'] = 'Avec cette option, vous définissez la taille maximale du fichier pour les images pouvant être téléchargées. Veuillez entrer la taille du fichier en Ko.';
$_ARRAYLANG['TXT_MARKETPLACE_MAX_FILESIZE'] = 'max. ';
$_ARRAYLANG['TXT_MEDIADIR_LANGUAGES'] = 'Langue';
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_ACTIVE_LANGUAGES'] = 'Langue utilisé dans le module';
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_MULTILANG_FRONTEND'] = 'Saisir entrées multilingue dans le Frontend';
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_MULTILANG_FRONTEND_INFO'] = 'Si cette option est activée, les entrées dans le frontend peuvent être enregistrées dans plusieurs langues. Si cette option est désactivée, une entrée sera enregistrée uniquement dans la langue active.';
$_ARRAYLANG['TXT_MEDIADIR_SETTINGS_ENTRIEY_INDIVIDUAL_ORDER'] = "Assortir entrées individuelles";
$_ARRAYLANG['TXT_MEDIADIR_PHONE'] = 'Téléphone';
$_ARRAYLANG['TXT_MEDIADIR_FAX'] = 'Fax';
$_ARRAYLANG['TXT_MEDIADIR_FUNCTION'] = 'Function';

$_ARRAYLANG['TXT_MEDIADIR_FROM_DATABASE'] = "de base de données";
$_ARRAYLANG['TXT_MEDIADIR_FROM_FILE'] = "du fichier";
$_ARRAYLANG['TXT_MEDIADIR_SELECT_TABLE'] = "Choisir un tableau";
$_ARRAYLANG['TXT_MEDIADIR_DO_IMPORT'] = "importer données";
$_ARRAYLANG['TXT_MEDIADIR_NO_EXPORT_MASK'] = "pas utiliser une masque d'export";
$_ARRAYLANG['TXT_MEDIADIR_DO_EXPORT'] = "exporter données";
$_ARRAYLANG['TXT_MEDIADIR_EXPORT_MASKS'] = "Exporter masque";
$_ARRAYLANG['TXT_MEDIADIR_EXPORT_MASK'] = "Exporter masque";
$_ARRAYLANG['TXT_MEDIADIR_NEW_EXPORT_MASK'] = "Nouvelle masque d'export";
$_ARRAYLANG['TXT_MEDIADIR_EDIT_EXPORT_MASK'] = "Modifier Masque d'export";
$_ARRAYLANG['TXT_MEDIADIR_FORM_TEMPLATE'] = "Modèle du formulaire";
$_ARRAYLANG['TXT_MEDIADIR_EXPORT_FORMAT'] = "Exporter format";
$_ARRAYLANG['TXT_MEDIADIR_OWNER'] = "Utilisateur";
$_ARRAYLANG['TXT_MEDIADIR_VALUE_CONTEXT'] = "Context";
$_ARRAYLANG['TXT_MEDIADIR_VALUE_CONTEXT_TOOLTIP'] = "Définissez ici quel champ doit être utilisé comme partie de l'adresse pour un événement de calendrier.";
$_ARRAYLANG['TXT_MEDIADIR_BASIC_DATA'] = "Basic Data";
$_ARRAYLANG['TXT_MEDIADIR_LEVEL_DETAILS'] = "Détails du niveau";
$_ARRAYLANG['TXT_MEDIADIR_CATEGORY_DETAILS'] = "Détails de la catégorie";
$_ARRAYLANG['TXT_MEDIADIR_SUBMIT'] = "Enregistrer";
$_ARRAYLANG['TXT_MEDIADIR_ACTIVE'] = "Active";
$_ARRAYLANG['TXT_MEDIADIR_AVERAGE_SYMBOL'] = "&Oslash;";
$_ARRAYLANG['TXT_MEDIADIR_COMMENT'] = "Remarque";
