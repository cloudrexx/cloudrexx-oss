<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * Cloudrexx CMS
 * @copyright   CLOUDREXX CMS - CLOUDREXX AG
 * @package     cloudrexx
 * @subpackage  module_filesharing
 */

$_ARRAYLANG['TXT_FILESHARING_DOWNLOAD_LINK'] = "Datei teilen";
$_ARRAYLANG['TXT_FILESHARING_DOWNLOAD_LINK_INFO'] = "Alle Personen mit dem Link können die Datei herunterladen und ansehen.";
$_ARRAYLANG['TXT_FILESHARING_DELETE_LINK'] = "Datei löschen";
$_ARRAYLANG['TXT_FILESHARING_INFO'] = "Information";
$_ARRAYLANG['TXT_FILESHARING_STATUS'] = "Status";
$_ARRAYLANG['TXT_FILESHARING_SEND_MAIL'] = "Dateifreigabe per E-Mail versenden";
$_ARRAYLANG['TXT_FILESHARING_EMAIL'] = "E-Mail-Adresse";
$_ARRAYLANG['TXT_FILESHARING_SUBJECT'] = "E-Mail Betreff";
$_ARRAYLANG['TXT_FILESHARING_SUBJECT_INFO'] = "Betreff für den Empfänger";
$_ARRAYLANG['TXT_FILESHARING_MESSAGE'] = "Nachricht";
$_ARRAYLANG['TXT_FILESHARING_EMAIL_INFO'] = "E-Mail-Adresse des Empfängers";
$_ARRAYLANG['TXT_FILESHARING_MESSAGE_INFO'] = "Nachricht für den Empfänger";
$_ARRAYLANG['TXT_FILESHARING_EXPIRATION'] = "Dauer unbegrenzt";
$_ARRAYLANG['TXT_FILESHARING_NEVER'] = "Ablauf der Dateifreigabe";
$_ARRAYLANG['TXT_FILESHARING_SEND'] = "Senden";
$_ARRAYLANG['TXT_FILESHARING_SAVE'] = "Speichern";
$_ARRAYLANG['TXT_FILESHARING_SHARED'] = "Diese Datei ist freigegeben.";
$_ARRAYLANG['TXT_FILESHARING_NOT_SHARED'] = "Diese Datei ist nicht freigegeben.";
$_ARRAYLANG['TXT_FILESHARING_STOP_SHARING'] = "Freigabe stoppen?";
$_ARRAYLANG['TXT_FILESHARING_START_SHARING'] = "Freigabe starten?";
$_ARRAYLANG['TXT_FILESHARING_MAIL_TEMPLATES'] = "E-Mail-Vorlagen";
$_ARRAYLANG['TXT_FILESHARING_LINK_BACK'] = "Abbrechen";
$_ARRAYLANG['TXT_FILESHARING_SECURITY'] = "Berechtigungen";
$_ARRAYLANG['TXT_FILESHARING_SECURITY_INFO'] = "Bestimmen Sie hier, welche Benutzer über das Frontend (Website) Dateien hochladen und freigeben dürfen.";

$_ARRAYLANG['TXT_FILESHARING_SETTINGS_GENERAL_INFORMATION'] = 'Informationen';
$_ARRAYLANG['TXT_FILESHARING_SETTINGS_GENERAL_MODULE_NAME_TITLE'] = 'Anwendungsname';
$_ARRAYLANG['TXT_FILESHARING_SETTINGS_GENERAL_MODULE_DESCRIPTION_TITLE'] = 'Beschreibung';
$_ARRAYLANG['TXT_FILESHARING_SETTINGS_GENERAL_MODULE_DESCRIPTION'] = 'Unter &bdquo;Dateifreigabe&ldquo; versteht man eine Hochlademöglichkeit für einen beliebigen Website Besucher. Die hochgeladenen Dateien können mit Hilfe eines Links direkt an eine dritte Person weitergeleitet werden. Der Zugriff auf die Datei ist nur mit einem direkten Link möglich.';
$_ARRAYLANG['TXT_FILESHARING_SETTINGS_GENERAL_MODULE_MANUAL_TITLE'] = 'Kurzanleitung';
$_ARRAYLANG['TXT_FILESHARING_SETTINGS_GENERAL_MODULE_MANUAL'] = 'Bei der Installation der Anwendung wird eine standardmässige Inhaltsseite mit der section "filesharing" generiert. Diese kann als Vorlage für eine andere Seite verwendet werden. Sie können eine Inhaltsseite in einem beliebigen Bereich (cmd) erstellen.<br>Der Bereich (cmd) entspricht beim Hochladen auf der Seite dem Ziel-Ordner. Sofern der Bereich (cmd) "downloads" verwendet wird, werden die hochgeladenen Dateien in den Ordner der Anwendung Digital Asset Management verschoben und können dadurch einfacher in der Anwendung ausgewählt werden.';
$_ARRAYLANG['TXT_FILESHARING_INVALID_FILE_PATH'] = 'Die ausgewählte Datei ist ungültig.';
$_ARRAYLANG['TXT_FILESHARING_SEND_SUCCESS'] = 'Erfolgreicher Versand der Einladung an folgende Empfänger: %s';
$_ARRAYLANG['TXT_FILESHARING_SEND_FAILURES'] = 'Versand der Einladung an folgende Empfänger ist fehlgeschlagen: %s';
$_ARRAYLANG['TXT_FILESHARING_FILE_SHARED'] = 'Freigabe der Datei %s gestartet';
$_ARRAYLANG['TXT_FILESHARING_FILE_SHARE_CANCELLED'] = 'Freigabe der Datei %s beendet';
