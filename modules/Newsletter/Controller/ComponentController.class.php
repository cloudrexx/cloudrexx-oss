<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * Main controller for Newsletter
 *
 * @copyright   Cloudrexx AG
 * @author      Project Team SS4U <info@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  module_newsletter
 */

namespace Cx\Modules\Newsletter\Controller;

/**
 * Main controller for Newsletter
 *
 * @copyright   Cloudrexx AG
 * @author      Project Team SS4U <info@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  module_newsletter
 */
class ComponentController extends \Cx\Core\Core\Model\Entity\SystemComponentController {
    public function getControllerClasses() {
        // Return an empty array here to let the component handler know that there
        // does not exist a backend, nor a frontend controller of this component.
        return array();
    }

    /**
     * Returns a list of command mode commands provided by this component
     *
     * @return array List of command names
     */
    public function getCommandsForCommandMode()
    {
        // No restriction here, but the 'autoclean' subcommand is limited
        // to cli mode in executeCommand().
        return array('Newsletter');
    }

    /**
     * Returns the description for a command provided by this component
     * @param string $command The name of the command to fetch the description from
     * @param boolean $short Wheter to return short or long description
     * @return string Command description
     */
    public function getCommandDescription($command, $short = false) {
        switch ($command) {
            case 'Newsletter':
                $desc = 'Group-based newsletter system';
                if ($short) {
                    return $desc;
                }
                $desc .= PHP_EOL . PHP_EOL . 'autoclean' . "\t" . 'Cleanup unsuccessul registrations';
                return $desc;
                break;
        }
    }

    /**
     * Execute api command
     *
     * @param string $command Name of command to execute
     * @param array  $arguments List of arguments for the command
     * @param array  $dataArguments (optional) List of data arguments for the command
     */
    public function executeCommand($command, $arguments, $dataArguments = array())
    {
        $subcommand = null;
        if (!empty($arguments[0])) {
            $subcommand = $arguments[0];
        }

        // define frontend language
        if (!defined('FRONTEND_LANG_ID')) {
            define('FRONTEND_LANG_ID', 1);
        }

        switch ($command) {
            case 'Newsletter':
                switch ($subcommand) {
                    case 'autoclean':
                        if (!$this->cx->isCliCall()) {
                            return;
                        }
                        $newsletterLib = new NewsletterLib();
                        $newsletterLib->autoCleanRegisters();
                        break;

                    case 'View':
                        Newsletter::displayInBrowser();
                        break;

                    case 'Redirect':
                        Newsletter::trackLink();
                        break;

                    case 'Unsubscribe':
                        $this->unsubscribe($arguments, $dataArguments);
                        break;
                }
                break;
            default:
                break;
        }
    }

    /**
     * Unsubscribe user using one-click unsubscribe method according to
     * {@see https://www.rfc-editor.org/rfc/rfc8058.txt RFC 8058} and
     * {@see https://datatracker.ietf.org/doc/html/rfc2369 RFC 2369}
     *
     * @param array $args GET request data
     * @param array $data POST request data
     */
    protected function unsubscribe(array $args, array $data = array()): void {
        $this->handleOneClickUnsubscribeOverGet($args);
        $response = $this->cx->getResponse();
        $response->setContentType('text/plain');
        if (
            !isset($data['List-Unsubscribe'])
            || $data['List-Unsubscribe'] !== 'One-Click'
            || !isset($args['mail'])
            || !isset($args['code'])
        ) {
            $response->setCode('400');
            $response->setParsedContent('Invalid request');
            echo $response->send(false);
            return;
        }
        try {
            $id = Newsletter::getRecipientIdByCode($args['mail'], $args['code']);
            Newsletter::unsubscribe($id);
        } catch (\Exception $e) {
            \DBG::debug($e->getMessage());
            $response->setCode('401');
            $response->setParsedContent('Invalid request');
            echo $response->send(false);
            return;
        }
        $response->setCode('200');
        $response->setParsedContent($args['mail'] . ' successfully unsubscribed');
        echo $response->send(false);
    }

    /**
     * Redirect to regular unsubscribe page in case the one-click
     * unsubscribe has been requested over GET and not over POST
     * as expected.
     * This case is to handle most email clients that do not yet
     * support one-click unsubscribe according to
     * {@see https://www.rfc-editor.org/rfc/rfc8058.txt RFC 8058}.
     *
     * @param array $args GET request data
     */
    protected function handleOneClickUnsubscribeOverGet(array $args): void {
        // note: we have to use $_GET here as in Cx::MODE_COMMAND all
        // query arguments have been removed from $this->cx->getRequest()
        if (
            $this->cx->getRequest()->getHttpRequestMethod() !== 'get'
            || !isset($args['mail'])
            || !isset($args['code'])
        ) {
            return;
        }

        \Cx\Core\Csrf\Controller\Csrf::redirect(
            \Cx\Core\Routing\Url::fromModuleAndCmd(
                $this->getName(),
                'unsubscribe',
                NewsletterLib::getUsersPreferredLanguageId(
                    $args['mail'],
                    NewsletterLib::USER_TYPE_NEWSLETTER
                ),
                array(
                    'code' => $args['code'],
                    'mail' => $args['mail'],
                )
            )
        );
    }

     /**
     * Load your component.
     *
     * @param \Cx\Core\ContentManager\Model\Entity\Page $page       The resolved page
     */
    public function load(\Cx\Core\ContentManager\Model\Entity\Page $page) {
        global $_CORELANG, $objTemplate, $subMenuTitle;
        switch ($this->cx->getMode()) {
            case \Cx\Core\Core\Controller\Cx::MODE_FRONTEND:
                $newsletter = new Newsletter(\Env::get('cx')->getPage()->getContent());
                \Env::get('cx')->getPage()->setContent($newsletter->getPage());
                break;

            case \Cx\Core\Core\Controller\Cx::MODE_BACKEND:
                $this->cx->getTemplate()->addBlockfile('CONTENT_OUTPUT', 'content_master', 'ContentMaster.html');
                $objTemplate = $this->cx->getTemplate();

                $subMenuTitle = $_CORELANG['TXT_CORE_EMAIL_MARKETING'];
                $objNewsletter = new NewsletterManager();
                $objNewsletter->getPage();
                break;
        }
    }

    /**
     * Do something before content is loaded from DB
     *
     * @param \Cx\Core\ContentManager\Model\Entity\Page $page       The resolved page
     */
    public function preContentLoad(\Cx\Core\ContentManager\Model\Entity\Page $page) {
        global $newsletter, $_ARRAYLANG, $page_template, $themesPages, $objInit;
        switch ($this->cx->getMode()) {
            case \Cx\Core\Core\Controller\Cx::MODE_FRONTEND:
                // get Newsletter
                $_ARRAYLANG = array_merge($_ARRAYLANG, $objInit->loadLanguageData('Newsletter'));
                $newsletter = new Newsletter('');
                $content = \Env::get('cx')->getPage()->getContent();
                if (preg_match('/{NEWSLETTER_BLOCK}/', $content)) {
                    $newsletter->setBlock($content);
                }
                if (preg_match('/{NEWSLETTER_BLOCK}/', $page_template)) {
                    $newsletter->setBlock($page_template);
                }
                if (preg_match('/{NEWSLETTER_BLOCK}/', $themesPages['index'])) {
                    $newsletter->setBlock($themesPages['index']);
                }
                break;
        }
    }

    /**
     * @inheritDoc
     */
    public function registerEventListeners() {
        $this->cx->getEvents()->addEventListener(
            'NetManager:getSenderDomains',
            new \Cx\Modules\Newsletter\Model\Event\NewsletterEventListener($this->cx)
        );
    }
}
