<?php
declare(strict_types=1);

/**
 * Cloudrexx
 *
 * @link      https://www.cloudrexx.com
 * @copyright Cloudrexx AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

namespace Cx\Modules\Newsletter\Model\Event;

/**
 * @copyright   Cloudrexx AG
 * @author      Thomas Wirz <thomas.wirz@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  module_newsletter
 */
class NewsletterEventListener extends \Cx\Core\Event\Model\Entity\DefaultEventListener {
    /**
     * @param array $config Config from event NetManager:getSenderDomains
     */
    public function NetManagerGetSenderDomains(array $config = []): array {
        $reportUsages = $config['usages'] ?? false;
        $senderDomains = [];
        $this->fetchSenderDomainsFromSettings($senderDomains, $reportUsages);
        $this->fetchSenderDomainsFromCampaigns($senderDomains, $reportUsages);
        $this->fetchSenderDomainsFromMailTemplates($senderDomains);
        return $senderDomains;
    }

    /**
     * Fetch used sender domains from settings
     *
     * @param array $senderDomains Fetched sender domains from settings will
     *     be merged into $senderdomains.
     * @param bool $reportUsages Whether to report usages of sender domains.
     */
    protected function fetchSenderDomainsFromSettings(array &$senderDomains = [], bool $reportUsages = false): void {
        $result = $this->cx->getDb()->getAdoDb()->Execute(
            'SELECT `setvalue`
                FROM `' . DBPREFIX . 'module_newsletter_settings`
                WHERE `setname` = ?',
            'sender_mail'
        );
        if (!$result || !$result->RecordCount()) {
            return;
        }
        try {
            $domain = $this->getDomainFromEmail($result->fields['setvalue']);
        } catch (\Exception $e) {
            \DBG::debug($e->getMessage());
            return;
        }
        if (!isset($senderDomains[$domain])) {
            $senderDomains[$domain] = [
                'strict' => true,
                'usages' => [],
            ];
        }
        if (!$reportUsages) {
            return;
        }
        $langData = \Env::get('init')->getComponentSpecificLanguageData(
            'Newsletter', false
        );
        $senderDomains[$domain]['usages']['setting'] = [
            'editurl' => \Cx\Core\Routing\Url::fromBackend(
                'Newsletter',
                'Settings',
                0,
                ['tpl' => 'Dispatch']
            )->toString(),
            'title' => $langData['TXT_SENDER_EMAIL'],
        ];
    }

    /**
     * Fetch used sender domains in email campaigns
     *
     * @param array $senderDomains Fetched sender domains from email campaigns
     *     be merged into $senderdomains.
     * @param bool $reportUsages Whether to report usages of sender domains.
     */
    protected function fetchSenderDomainsFromCampaigns(array &$senderDomains = [], bool $reportUsages = false): void {
        $campaignIds = [];
        $campaignSubjects = [];
        if ($reportUsages) {
            $query = '
                SELECT
                    `sender_email`,
                    GROUP_CONCAT(`id`) AS `usages`
                FROM `' . DBPREFIX .'module_newsletter`
                WHERE `sender_email` != \'\'
                GROUP BY `sender_email`
            ';
        } else {
            $query = '
                SELECT
                    DISTINCT `sender_email`
                FROM `' . DBPREFIX .'module_newsletter`
                WHERE `sender_email` != \'\'
            ';
        }
        $result = $this->cx->getDb()->getAdoDb()->Execute($query);
        if (!$result || !$result->RecordCount()) {
            return;
        }
        while (!$result->EOF) {
            try {
                $domain = $this->getDomainFromEmail($result->fields['sender_email']);
            } catch (\Exception $e) {
                \DBG::debug($e->getMessage());
                $result->MoveNext();
                continue;
            }
            if (!isset($senderDomains[$domain])) {
                $senderDomains[$domain] = [
                    'strict' => true,
                    'usages' => [],
                ];
            } else {
                $senderDomains[$domain]['strict'] = true;
            }
            if ($reportUsages) {
                $senderDomains[$domain]['campaign_usages'] = explode(
                    ',', $result->fields['usages']
                );
                $campaignIds = array_merge(
                    $campaignIds,
                    $senderDomains[$domain]['campaign_usages']
                );
            }
            $result->MoveNext();
        }
        if (!$reportUsages) {
            return;
        }
        $campaignIds = array_unique($campaignIds);
        $result = $this->cx->getDb()->getAdoDb()->Execute('
            SELECT
                `id`,
                `subject`
            FROM `' . DBPREFIX .'module_newsletter`
            WHERE `id` IN (' . join(',', $campaignIds) . ')
        ');
        if (!$result || !$result->RecordCount()) {
            return;
        }
        while (!$result->EOF) {
            $campaignSubjects[$result->fields['id']] = $result->fields['subject'];
            $result->MoveNext();
        }
        foreach ($senderDomains as &$senderDomain) {
            if (!isset($senderDomain['campaign_usages'])) {
                continue;
            }
            foreach ($senderDomain['campaign_usages'] as $campaignId) {
                $senderDomain['usages']['campaign_' . $campaignId] = [
                    'editurl' => \Cx\Core\Routing\Url::fromBackend(
                        'Newsletter',
                        'editMail',
                        0,
                        ['id' => $campaignId]
                    )->toString(),
                    'title' => $campaignSubjects[$campaignId],
                ];
            }
            unset($senderDomain['campaign_usages']);
        }
    }

    /**
     * Fetch used sender domains in MailTemplates
     *
     * @param array $senderDomains Fetched sender domains from MailTemplates
     *     be merged into $senderdomains.
     */
    protected function fetchSenderDomainsFromMailTemplates(array &$senderDomains = []): void {
        foreach(
            \Cx\Core\MailTemplate\Controller\MailTemplate::getSenderDomains(
                'Newsletter',
                \Cx\Core\Routing\Url::fromBackend(
                    'Newsletter',
                    'mailtemplate_edit',
                    0,
                    ['tpl' => 'EmailTemplates']
                )
            ) as $domain => $data
        ) {
            if (!isset($senderDomains[$domain])) {
                $senderDomains[$domain] = $data;
                continue;
            }
            $senderDomains[$domain]['usages'] += $data['usages'];
        }
    }

    /**
     * Get the domain of email address $email
     *
     * @param string $email Email to fetch the domain from
     * @throws \Exception If $email not a valid email address
     * @return string Domain part of email
     */
    protected function getDomainFromEmail(string $email): string {
        if (!\FWValidator::isEmail($email)) {
            throw new \Exception('Invalid email: ' . $email);
        }
        $emailParts = explode('@', $email);
        $domain = array_pop($emailParts);
        return \Cx\Core\Net\Controller\ComponentController::convertIdnToAsciiFormat(
            $domain
        );
    }
}
