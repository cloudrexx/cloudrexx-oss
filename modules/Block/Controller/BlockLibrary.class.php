<?php

/**
 * Cloudrexx
 *
 * @link      https://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2023
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * Block
 * @copyright   CLOUDREXX CMS - CLOUDREXX AG
 * @author      Cloudrexx Development Team <info@cloudrexx.com>
 * @version     1.0.0
 * @package     cloudrexx
 * @subpackage  module_block
 */

namespace Cx\Modules\Block\Controller;

/**
 * Cx\Modules\Block\Controller\BlockLibraryException
 *
 * @copyright   CLOUDREXX CMS - CLOUDREXX AG
 * @author      Manuel Schenk <manuel.schenk@comvation.com>
 * @version     1.0.0
 * @package     cloudrexx
 * @subpackage  module_block
 */
class BlockLibraryException extends \Exception {}

/**
 * Cx\Modules\Block\Controller\NoCategoryFoundException
 *
 * @copyright   CLOUDREXX CMS - CLOUDREXX AG
 * @author      Manuel Schenk <manuel.schenk@comvation.com>
 * @version     1.0.0
 * @package     cloudrexx
 * @subpackage  module_block
 */
class NoCategoryFoundException extends BlockLibraryException {}

/**
 * Block
 *
 * Block library class
 * @copyright   CLOUDREXX CMS - CLOUDREXX AG
 * @author      Cloudrexx Development Team <info@cloudrexx.com>
 * @access      private
 * @version     1.0.0
 * @package     cloudrexx
 * @subpackage  module_block
 */
class BlockLibrary
{
    /**
     * Block name prefix
     *
     * @access public
     * @var string
     */
    var $blockNamePrefix = 'BLOCK_';

    /**
     * Block ids
     *
     * @access private
     * @var array
     */
    var $_arrBlocks;

    /**
     * Array of categories
     *
     * @var array
     */
    var $_categories = array();

    /**
     * holds the category dropdown select options
     *
     * @var array of strings: HTML <options>
     */
    var $_categoryOptions = array();

    /**
     * array containing the category names
     *
     * @var array catId => name
     */
    var $_categoryNames = array();

    protected $availableTargeting = array(
        'country',
    );

    /**
     * Constructor
     */
    function __construct()
    {
        if (\Cx\Core\Core\Controller\Cx::instanciate()->getMode() != \Cx\Core\Core\Controller\Cx::MODE_COMMAND) {
            return;
        }
    }


    /**
     * Get blocks
     *
     * Get all blocks
     *
     * @access private
     * @see array blockLibrary::_arrBlocks
     * @return array Array with block ids
     */
    public function getBlocks($catId = 0)
    {
        global $objDatabase;

        if (
            \Cx\Core\Setting\Controller\Setting::getValue(
                'CLX5087_BlockHistory',
                'Config'
            ) === 'on'
        ) {
            if (!is_array($this->_arrBlocks)) {
                $em = \Cx\Core\Core\Controller\Cx::instanciate()->getDb()->getEntityManager();
                $blockRepo = $em->getRepository('\Cx\Modules\Block\Model\Entity\Block');

                if ($catId != 0) {
                    $blocks = $blockRepo->findBy(array('category' => $catId));
                } else {
                    $blocks = $blockRepo->findAll();
                }

                $this->_arrBlocks = array();

                foreach ($blocks as $block) {
                    $langArr = array();
                    $contents = $block->getRelLangContents();
                    foreach ($contents as $content) {
                        if ($content->getActive() == 1) {
                            array_push($langArr, $content->getLocale()->getId());
                        }
                    }

                    $catId = 0;
                    $cat = $block->getCategory();
                    if ($cat) {
                        $catId = $block->getCategory()->getId();
                    }

                    $this->_arrBlocks[$block->getId()] = array(
                        'cat' => $catId,
                        'start' => $block->getStart(),
                        'end' => $block->getEnd(),
                        'order' => $block->getOrder(),
                        'random' => $block->getRandom(),
                        'random2' => $block->getRandom2(),
                        'random3' => $block->getRandom3(),
                        'random4' => $block->getRandom4(),
                        'global' => $block->getShowInGlobal(),
                        'active' => $block->getActive(),
                        'direct' => $block->getShowInDirect(),
                        'name' => $block->getName(),
                        'lang' => array_unique($langArr),
                    );
                }
            }
        } else {
            $catId = intval($catId);
            $where = '';

            if ($catId > 0) {
                $where = 'WHERE `cat` = '.$catId;
            }

            if (!is_array($this->_arrBlocks)) {
                $query = 'SELECT    `id`,
                                    `cat`,
                                    `name`,
                                    `start`,
                                    `end`,
                                    `order`,
                                    `random`,
                                    `random_2`,
                                    `random_3`,
                                    `random_4`,
                                    `global`,
                                    `active`,
                                    `direct`,
                                    `category`
                            FROM `%1$s`
                            # WHERE
                            %2$s
                            ORDER BY `order`';

                $objResult = $objDatabase->Execute(sprintf($query, DBPREFIX.'module_block_blocks',
                                                                   $where));
                if ($objResult !== false) {
                    $this->_arrBlocks = array();

                    while (!$objResult->EOF) {
                        $langArr          = array();
                        $objBlockLang = $objDatabase->Execute("SELECT lang_id FROM ".DBPREFIX."module_block_rel_lang_content WHERE block_id=".$objResult->fields['id']." AND `active` = 1 ORDER BY lang_id ASC");

                        if ($objBlockLang) {
                            while (!$objBlockLang->EOF) {
                                $langArr[] = $objBlockLang->fields['lang_id'];
                                $objBlockLang->MoveNext();

                            }
                        }
                        $this->_arrBlocks[$objResult->fields['id']] = array(
                            'cat'       => $objResult->fields['cat'],
                            'start'     => $objResult->fields['start'],
                            'end'       => $objResult->fields['end'],
                            'order'     => $objResult->fields['order'],
                            'random'    => $objResult->fields['random'],
                            'random2'   => $objResult->fields['random_2'],
                            'random3'   => $objResult->fields['random_3'],
                            'random4'   => $objResult->fields['random_4'],
                            'global'    => $objResult->fields['global'],
                            'active'    => $objResult->fields['active'],
                            'direct'    => $objResult->fields['direct'],
                            'category'    => $objResult->fields['category'],
                            'name'      => $objResult->fields['name'],
                            'lang'      => array_unique($langArr),
                        );
                        $objResult->MoveNext();
                    }
                }
            }
        }

        return $this->_arrBlocks;
    }

    /**
     * Add a new block to database
     *
     * @param int $cat
     * @param array $arrContent
     * @param string $name
     * @param int $start
     * @param int $end
     * @param int $blockRandom
     * @param int $blockRandom2
     * @param int $blockRandom3
     * @param int $blockRandom4
     * @param int $blockWysiwygEditor
     * @param array $arrLangActive
     * @return bool|int the block's id
     */
    public function _addBlock($cat, $arrContent, $name, $start, $end, $blockRandom, $blockRandom2, $blockRandom3, $blockRandom4, $blockWysiwygEditor, $arrLangActive)
    {
        global $objDatabase;

        $query = "INSERT INTO `".DBPREFIX."module_block_blocks` (
                    `order`,
                    `name`,
                    `cat`,
                    `start`,
                    `end`,
                    `random`,
                    `random_2`,
                    `random_3`,
                    `random_4`,
                    `wysiwyg_editor`,
                    `active`
                  ) SELECT MAX(`order`) + 1,
                      '".contrexx_raw2db($name)."',
                      ".intval($cat).",
                      ".intval($start).",
                      ".intval($end).",
                      ".intval($blockRandom).",
                      ".intval($blockRandom2).",
                      ".intval($blockRandom3).",
                      ".intval($blockRandom4).",
                      ".intval($blockWysiwygEditor).",
                       1
                  FROM `".DBPREFIX."module_block_blocks`";
        if ($objDatabase->Execute($query) === false) {
            return false;
        }
        $id = $objDatabase->Insert_ID();

        // clear page cache to refresh global and category placeholders
        \Cx\Core\Core\Controller\Cx::instanciate()->getComponent('Cache')->clearCache();
        $this->storeBlockContent($id, $arrContent, $arrLangActive);

        return $id;
    }


    /**
     * Update an existing block
     *
     * @param int $id
     * @param int $cat
     * @param array $arrContent
     * @param string $name
     * @param int $start
     * @param int $end
     * @param int $blockRandom
     * @param int $blockRandom2
     * @param int $blockRandom3
     * @param int $blockRandom4
     * @param int $blockWysiwygEditor
     * @param array $arrLangActive
     * @return bool|int the id of the block
     */
    public function _updateBlock($id, $cat, $arrContent, $name, $start, $end, $blockRandom, $blockRandom2, $blockRandom3, $blockRandom4, $blockWysiwygEditor, $arrLangActive)
    {
        global $objDatabase;

        $query = "UPDATE `".DBPREFIX."module_block_blocks`
                    SET `name`              = '".contrexx_raw2db($name)."',
                        `cat`               = ".intval($cat).",
                        `start`             = ".intval($start).",
                        `end`               = ".intval($end).",
                        `random`            = ".intval($blockRandom).",
                        `random_2`          = ".intval($blockRandom2).",
                        `random_3`          = ".intval($blockRandom3).",
                        `random_4`          = ".intval($blockRandom4).",
                        `wysiwyg_editor`    = ".intval($blockWysiwygEditor)."
                  WHERE `id` = ".intval($id);
        if ($objDatabase->Execute($query) === false) {
            return false;
        }

        $this->storeBlockContent($id, $arrContent, $arrLangActive);

        return $id;
    }

    /**
     * Store the placeholder settings for a block
     *
     * @param int $blockId
     * @param int $global
     * @param int $direct
     * @param int $category
     * @param array $globalAssociatedPages
     * @param array $directAssociatedPages
     * @param array $categoryAssociatedPages
     * @return bool it was successfully saved
     */
    protected function storePlaceholderSettings($blockId, $global, $direct, $category, $globalAssociatedPages, $directAssociatedPages, $categoryAssociatedPages) {
        global $objDatabase;

        if (
            \Cx\Core\Setting\Controller\Setting::getValue(
                'CLX5087_BlockHistory',
                'Config'
            ) === 'on'
        ) {
            $block = $blockId;
            $relPagesWithoutBlock = array();
            if ($global == 2) {
                $relPagesWithoutBlock = array_merge($relPagesWithoutBlock, $this->storePageAssociations($block, $globalAssociatedPages, 'global'));
            }
            if ($direct == 1) {
                $relPagesWithoutBlock = array_merge($relPagesWithoutBlock, $this->storePageAssociations($block, $directAssociatedPages, 'direct'));
            }
            if ($category == 1) {
                $relPagesWithoutBlock = array_merge($relPagesWithoutBlock, $this->storePageAssociations($block, $categoryAssociatedPages, 'category'));
            }
            return $relPagesWithoutBlock;
        } else {
            $objDatabase->Execute("UPDATE `" . DBPREFIX . "module_block_blocks`
                                    SET `global` = ?,
                                        `direct` = ?,
                                        `category` = ?
                                    WHERE `id` = ?", array($global, $direct, $category, $blockId));

            $objDatabase->Execute("DELETE FROM `" . DBPREFIX . "module_block_rel_pages` WHERE `block_id` = '" . intval($blockId) . "'");
            if ($global == 2) {
                $this->storePageAssociations($blockId, $globalAssociatedPages, 'global');
            }
            if ($direct == 1) {
                $this->storePageAssociations($blockId, $directAssociatedPages, 'direct');
            }
            if ($category == 1) {
                $this->storePageAssociations($blockId, $categoryAssociatedPages, 'category');
            }
            return true;
        }
    }

    /**
     * Store the page associations
     *
     * @param int $blockId the block id
     * @param array $blockAssociatedPageIds the page ids
     * @param string $placeholder the placeholder
     */
    private function storePageAssociations($blockId, $blockAssociatedPageIds, $placeholder)
    {
        global $objDatabase;

        if (
            \Cx\Core\Setting\Controller\Setting::getValue(
                'CLX5087_BlockHistory',
                'Config'
            ) === 'on'
        ) {
            $block = $blockId;
            $em = \Cx\Core\Core\Controller\Cx::instanciate()->getDb()->getEntityManager();
            $pageRepo = $em->getRepository('\Cx\Core\ContentManager\Model\Entity\Page');
            $relPageRepo = $em->getRepository('\Cx\Modules\Block\Model\Entity\RelPage');

            $relPagesWithoutBlock = array();
            foreach ($blockAssociatedPageIds as $pageId) {
                if (!$pageId > 0) {
                    continue;
                }

                $page = $pageRepo->findOneBy(array('id' => $pageId));
                if (!$page) {
                    continue;
                }

                if ($block) {
                    $relPage = $relPageRepo->findOneBy(
                        array(
                            'block' => $block,
                            'page' => $page,
                            'placeholder' => $placeholder,
                        )
                    );
                    if ($relPage) {
                        continue;
                    }
                }

                $relPage = new \Cx\Modules\Block\Model\Entity\RelPage();
                $relPage->setPage($page);
                $relPage->setPlaceholder($placeholder);
                if ($block) {
                    $relPage->setBlock($block);
                } else {
                    array_push($relPagesWithoutBlock, $relPage);
                }

                $em->persist($relPage);
            }

            if ($block) {
                $relPages = $relPageRepo->findBy(
                    array(
                        'block' => $block,
                        'placeholder' => $placeholder,
                    )
                );
                if ($relPages) {
                    foreach ($relPages as $relPage) {
                        if (!in_array($relPage->getPage()->getId(), $blockAssociatedPageIds)) {
                            $em->remove($relPage);
                        }
                    }
                }
            }

            return $relPagesWithoutBlock;
        } else {
            foreach ($blockAssociatedPageIds as $pageId) {
                $objDatabase->Execute("INSERT INTO `" . DBPREFIX . "module_block_rel_pages` (`block_id`, `page_id`, `placeholder`)
                                        VALUES (?, ?, ?)", array($blockId, $pageId, $placeholder));
            }
        }
    }

    protected function storeBlockContent($blockId, $arrContent, $arrLangActive)
    {
        global $objDatabase;

        if (
            \Cx\Core\Setting\Controller\Setting::getValue(
                'CLX5087_BlockHistory',
                'Config'
            ) === 'on'
        ) {
            $block = $blockId;
            $em = \Cx\Core\Core\Controller\Cx::instanciate()->getDb()->getEntityManager();
            $relLangContentRepo = $em->getRepository('\Cx\Modules\Block\Model\Entity\RelLangContent');
            $localeRepo = $em->getRepository('\Cx\Core\Locale\Model\Entity\Locale');

            $arrPresentLang = array();
            if ($block) {
                $relLangContents = $block->getRelLangContents();

                foreach ($relLangContents as $relLangContent) {
                    $arrPresentLang[] = $relLangContent->getLocale()->getId();
                }
            }

            $relLangContentsWithoutBlock = array();
            foreach ($arrContent as $langId => $content) {
                if (empty($content)) {
                    continue;
                }
                $content = preg_replace('/\[\[([A-Z0-9_-]+)\]\]/', '{\\1}', $content);
                $locale = $localeRepo->findOneBy(array('id' => $langId));

                if (in_array($langId, $arrPresentLang)) {
                    $relLangContent = $relLangContentRepo->findOneBy(array('block' => $block, 'locale' => $locale));

                    $relLangContent->setContent($content);
                    $relLangContent->setActive(isset($arrLangActive[$langId]) ? $arrLangActive[$langId] : 0);
                } else {
                    $relLangContent = new \Cx\Modules\Block\Model\Entity\RelLangContent();

                    $relLangContent->setContent($content);
                    $relLangContent->setActive(isset($arrLangActive[$langId]) ? $arrLangActive[$langId] : 0);
                    $relLangContent->setLocale($locale);
                    if ($block) {
                        $relLangContent->setBlock($block);
                    } else {
                        array_push($relLangContentsWithoutBlock, $relLangContent);
                    }

                    $em->persist($relLangContent);
                }
            }

            if ($block) {
                \Cx\Core\Core\Controller\Cx::instanciate()->getComponent('Cache')->clearSsiCachePage(
                    'Block',
                    'getBlockContent',
                    array(
                        'block' => $block->getId(),
                    )
                );

                $relLangContents = $block->getRelLangContents();
                if ($relLangContents) {
                    foreach ($relLangContents as $relLangContent) {
                        if (!$arrLangActive[$relLangContent->getLocale()->getId()]) {
                            $em->remove($relLangContent);
                        }
                    }
                }
            }

            return $relLangContentsWithoutBlock;
        } else {
            $arrPresentLang = array();
            $objResult = $objDatabase->Execute('SELECT lang_id FROM '.DBPREFIX.'module_block_rel_lang_content WHERE block_id='.$blockId);
            if ($objResult) {
                while (!$objResult->EOF) {
                    $arrPresentLang[] = $objResult->fields['lang_id'];
                    $objResult->MoveNext();
                }
            }

            foreach ($arrContent as $langId => $content) {
                if (in_array($langId, $arrPresentLang)) {
                    $query = 'UPDATE `%1$s` SET %2$s WHERE `block_id` = %3$s AND `lang_id`='.intval($langId);
                } else {
                    $query = 'INSERT INTO `%1$s` SET %2$s, `block_id` = %3$s';
                }

                $content = preg_replace('/\[\[([A-Z0-9_-]+)\]\]/', '{\\1}', $content);
                $objDatabase->Execute(sprintf($query, DBPREFIX.'module_block_rel_lang_content',
                                                      "lang_id='".intval($langId)."',
                                                       content='".contrexx_raw2db($content)."',
                                                       active='".intval((isset($arrLangActive[$langId]) ? $arrLangActive[$langId] : 0))."'",
                                                      $blockId));
            }
            \Cx\Core\Core\Controller\Cx::instanciate()->getComponent('Cache')->clearSsiCachePage(
                'Block',
                'getBlockContent',
                array(
                    'block' => $blockId,
                )
            );
            /*
             * TODO: Once the Block placeholders have been properly migrated to
                     widgets, do replace the call to clearSsiCachePage() by the
                     following event trigger:

                $this->cx->getEvents()->triggerEvent(
                    'clearEsiCache',
                    array(
                        'Widget',
                        array(
                            'BLOCK_RANDOMIZER',
                            'BLOCK_RANDOMIZER_2',
                            'BLOCK_RANDOMIZER_3',
                            'BLOCK_RANDOMIZER_4',
                        ),
                    )
                );
            */
            for ($i = 1; $i <= 4; $i++) {
                \Cx\Core\Core\Controller\Cx::instanciate()->getComponent('Cache')->clearSsiCachePage(
                    'Block',
                    'getRandomBlockContent',
                    array(
                        'randomizer' => $i,
                    )
                );
            }

            $objDatabase->Execute("DELETE FROM ".DBPREFIX."module_block_rel_lang_content WHERE block_id=".$blockId." AND lang_id NOT IN (".join(',', array_map('intval', array_keys($arrLangActive))).")");
        }
    }

    /**
     * Get GeoIp component controller
     *
     * @return \Cx\Core_Modules\GeoIp\Controller\ComponentController
     */
    public function getGeoIpComponent()
    {
        $componentRepo = \Cx\Core\Core\Controller\Cx::instanciate()
            ->getDb()
            ->getEntityManager()
            ->getRepository('Cx\Core\Core\Model\Entity\SystemComponent');
        $geoIpComponent = $componentRepo->findOneBy(array('name' => 'GeoIp'));
        if (!$geoIpComponent) {
            return null;
        }
        $geoIpComponentController = $geoIpComponent->getSystemComponentController();
        if (!$geoIpComponentController) {
            return null;
        }

        return $geoIpComponentController;
    }

    /**
     * Verify targeting options for the given block Id
     *
     * @param integer $blockId Block id
     *
     * @return boolean True when all targeting options vaild, false otherwise
     */
    public function checkTargetingOptions($blockId)
    {
        if (
            \Cx\Core\Setting\Controller\Setting::getValue(
                'CLX5087_BlockHistory',
                'Config'
            ) === 'on'
        ) {
            $em = \Cx\Core\Core\Controller\Cx::instanciate()->getDb()->getEntityManager();
            $blockRepo = $em->getRepository('\Cx\Modules\Block\Model\Entity\Block');
            $block = $blockRepo->findOneBy(array('id' => $blockId));
            $targetingOptions = $block->getTargetingOptions();

            if (empty($targetingOptions)) {
                return true;
            }

            foreach ($targetingOptions as $targetingOption) {
                switch ($targetingOption->getType()) {
                    case 'country':
                        if (!$this->checkTargetingCountry($targetingOption->getFilter(), $targetingOption->getValue())) {
                            return false;
                        }
                        break;
                    default:
                        break;
                }
            }
            return true;
        } else {
            $targeting = $this->loadTargetingSettings($blockId);

            if (empty($targeting)) {
                return true;
            }

            foreach ($targeting as $targetingType => $targetingSetting) {
                switch ($targetingType) {
                    case 'country':
                        if (!$this->checkTargetingCountry($targetingSetting['filter'], $targetingSetting['value'])) {
                            return false;
                        }
                        break;
                    default:
                        break;
                }
            }
            return true;
        }
    }

    /**
     * Check Country targeting option
     *
     * @param string $filter include => client country should exists in given country ids
     *                            exclude => client country should not exists in given country ids
     * @param array $countryIds Country ids to match
     *
     * @return boolean True when targeting country option matching to client country
     *                 False otherwise
     */
    public function checkTargetingCountry($filter, $countryIds)
    {
        // getClient country using GeoIp component
        $geoIpComponentController = $this->getGeoIpComponent();
        if (!$geoIpComponentController) {
            return false;
        }

        $clientRecord = $geoIpComponentController->getClientRecord();
        if (!$clientRecord) {
            return false;
        }
        $clientCountryAlpha2 = $clientRecord->country->isoCode;
        $clientCountryId = \Cx\Core\Country\Controller\Country::getIdByAlpha2($clientCountryAlpha2);

        $isCountryExists = in_array($clientCountryId, $countryIds);
        if ($filter == 'include') {
            return $isCountryExists;
        } else {
            return !$isCountryExists;
        }
    }

    /**
     * Load Targeting settings
     *
     * @param integer $blockId Content block id
     *
     * @return array Settings array
     */
    public function loadTargetingSettings($blockId)
    {
        global $objDatabase;

        $query = 'SELECT
                    `filter`,
                    `type`,
                    `value`
                  FROM
                    `'. DBPREFIX .'module_block_targeting_option`
                  WHERE
                    `block_id` = "'. contrexx_raw2db($blockId) .'"
                 ';
        $targeting = $objDatabase->Execute($query);
        if (!$targeting) {
            return array();
        }

        $targetingArr = array();
        while (!$targeting->EOF) {
            $targetingArr[$targeting->fields['type']] = array(
                'filter' => $targeting->fields['filter'],
                'value'  => json_decode($targeting->fields['value'])
            );
            $targeting->MoveNext();
        }

        return $targetingArr;
    }

    /**
    * Get block
    *
    * Return a block
    *
    * @access private
    * @param integer $id
    * @global ADONewConnection
    * @return mixed content on success, false on failure
    */
    function _getBlock($id)
    {
        global $objDatabase;

        if (
            \Cx\Core\Setting\Controller\Setting::getValue(
                'CLX5087_BlockHistory',
                'Config'
            ) === 'on'
        ) {
            $em = \Cx\Core\Core\Controller\Cx::instanciate()->getDb()->getEntityManager();
            $blockRepo = $em->getRepository('\Cx\Modules\Block\Model\Entity\Block');
            $block = $blockRepo->findOneBy(array('id' => $id));

            if ($block) {
                $arrContent = array();
                $arrActive = array();

                $relLangContentRepo = $em->getRepository('\Cx\Modules\Block\Model\Entity\RelLangContent');
                $relLangContents = $relLangContentRepo->findBy(array('block' => $block));
                foreach ($relLangContents as $relLangContent) {
                    $arrContent[$relLangContent->getLocale()->getId()] = $relLangContent->getContent();
                    $arrActive[$relLangContent->getLocale()->getId()] = $relLangContent->getActive();
                }

                $catId = 0;
                $cat = $block->getCategory();
                if ($cat) {
                    $catId = $cat->getId();
                }

                return array(
                    'cat' => $catId,
                    'start' => $block->getStart(),
                    'end' => $block->getEnd(),
                    'random' => $block->getRandom(),
                    'random2' => $block->getRandom2(),
                    'random3' => $block->getRandom3(),
                    'random4' => $block->getRandom4(),
                    'global' => $block->getShowInGlobal(),
                    'direct' => $block->getShowInDirect(),
                    'category' => $block->getShowInCategory(),
                    'active' => $block->getActive(),
                    'name' => $block->getName(),
                    'wysiwyg_editor' => $block->getWysiwygEditor(),
                    'content' => $arrContent,
                    'lang_active' => $arrActive,
                );
            }
        } else {
            $objBlock = $objDatabase->SelectLimit("SELECT name, cat, start, end, random, random_2, random_3, random_4, global, direct, category, active, wysiwyg_editor FROM ".DBPREFIX."module_block_blocks WHERE id=".$id, 1);


            if ($objBlock !== false && $objBlock->RecordCount() == 1) {
                $arrContent = array();
                $arrActive = array();

                $objBlockContent = $objDatabase->Execute("SELECT lang_id, content, active FROM ".DBPREFIX."module_block_rel_lang_content WHERE block_id=".$id);
                if ($objBlockContent !== false) {
                    while (!$objBlockContent->EOF) {
                        $arrContent[$objBlockContent->fields['lang_id']] = $objBlockContent->fields['content'];
                        $arrActive[$objBlockContent->fields['lang_id']]  = $objBlockContent->fields['active'];
                        $objBlockContent->MoveNext();
                    }
                }

                return array(
                    'cat'               => $objBlock->fields['cat'],
                    'start'             => $objBlock->fields['start'],
                    'end'               => $objBlock->fields['end'],
                    'random'            => $objBlock->fields['random'],
                    'random2'           => $objBlock->fields['random_2'],
                    'random3'           => $objBlock->fields['random_3'],
                    'random4'           => $objBlock->fields['random_4'],
                    'global'            => $objBlock->fields['global'],
                    'direct'            => $objBlock->fields['direct'],
                    'category'          => $objBlock->fields['category'],
                    'active'            => $objBlock->fields['active'],
                    'name'              => $objBlock->fields['name'],
                    'wysiwyg_editor'    => $objBlock->fields['wysiwyg_editor'],
                    'content'           => $arrContent,
                    'lang_active'       => $arrActive,
                );
            }
        }

        return false;
    }

    /**
     * Get the associated pages for a placeholder
     *
     * @param int $blockId block id
     * @param string $placeholder
     * @return array
     */
    function _getAssociatedPageIds($blockId, $placeholder)
    {
        global $objDatabase;

        if (
            \Cx\Core\Setting\Controller\Setting::getValue(
                'CLX5087_BlockHistory',
                'Config'
            ) === 'on'
        ) {
            $em = \Cx\Core\Core\Controller\Cx::instanciate()->getDb()->getEntityManager();
            $relPageRepo = $em->getRepository('\Cx\Modules\Block\Model\Entity\RelPage');
            $blockRepo = $em->getRepository('\Cx\Modules\Block\Model\Entity\Block');

            $block = $blockRepo->findOneBy(array('id' => $blockId));
            $relPages = $relPageRepo->findBy(array(
                'block' => $block,
                'placeholder' => $placeholder,
            ));

            $arrPageIds = array();
            foreach ($relPages as $relPage) {
                array_push($arrPageIds, $relPage->getPage()->getId());
            }
        } else {
            $arrPageIds = array();
            $objResult = $objDatabase->Execute("SELECT page_id FROM ".DBPREFIX."module_block_rel_pages WHERE block_id = '" . intval($blockId) . "' AND placeholder = '" . contrexx_raw2db($placeholder) . "'");
            if ($objResult !== false) {
                while (!$objResult->EOF) {
                    array_push($arrPageIds, $objResult->fields['page_id']);
                    $objResult->MoveNext();
                }
            }
        }
        return $arrPageIds;
    }

    function _getBlocksForPage($page)
    {
        global $objDatabase;

        $arrBlocks = array(
            'global' => array(),
            'direct' => array(),
            'category' => array(),
        );
        if (
            \Cx\Core\Setting\Controller\Setting::getValue(
                'CLX5087_BlockHistory',
                'Config'
            ) === 'on'
        ) {
            $em = \Cx\Core\Core\Controller\Cx::instanciate()->getDb()->getEntityManager();

            $qb = $em->createQueryBuilder();
            $blocks = $qb->select('
                    b.id, rp.placeholder
                ')
                ->from('\Cx\Modules\Block\Model\Entity\Block', 'b')
                ->innerJoin('\Cx\Modules\Block\Model\Entity\RelPage', 'rp', 'WITH', 'b.id = rp.block')
                ->where('rp.page = :page')
                ->andWhere($qb->expr()->orX(
                    $qb->expr()->gt('b.showInGlobal', 1),
                    $qb->expr()->gt('b.showInDirect', 0),
                    $qb->expr()->gt('b.showInCategory', 0)
                ))
                ->orderBy('b.order')
                ->setParameter('page', $page)
                ->getQuery()
                ->getResult();

            foreach ($blocks as $block) {
                $arrBlocks[$block['placeholder']][$block['id']] = $block['id'];
            }
            $qb = $em->createQueryBuilder();
            $blocks = $qb->select('
                    b.id, b.showInGlobal, b.showInDirect, b.showInCategory
                ')
                ->from('\Cx\Modules\Block\Model\Entity\Block', 'b')
                ->andWhere($qb->expr()->orX(
                    $qb->expr()->eq('b.showInGlobal', 1),
                    $qb->expr()->eq('b.showInDirect', 0),
                    $qb->expr()->eq('b.showInCategory', 0)
                ))
                ->orderBy('b.order')
                ->getQuery()
                ->getResult();

            foreach ($blocks as $block) {
                if ($block['showInGlobal'] == 1) {
                    $arrBlocks['global'][$block['id']] = $block['id'];
                }
                if ($block['showInDirect'] == 0) {
                    $arrBlocks['direct'][$block['id']] = $block['id'];
                }
                if ($block['showInCategory'] == 0) {
                    $arrBlocks['category'][$block['id']] = $block['id'];
                }
            }
        } else {
            $pageId = $page->getId();
            $objResult = $objDatabase->Execute('
                SELECT
                    `b`.`id`,
                    `p`.`placeholder`
                FROM
                    `'.DBPREFIX.'module_block_blocks` AS `b`
                INNER JOIN
                    `'.DBPREFIX.'module_block_rel_pages` AS `p`
                ON
                    `b`.`id` = `p`.`block_id`
                WHERE
                    `p`.`page_id` = \'' . $pageId . '\'
                    AND (
                        `b`.`global` > 1
                        OR `b`.`direct` > 0
                        OR `b`.`category` > 0
                    )
                ORDER BY
                    `b`.`order`
            ');
            while ($objResult && !$objResult->EOF) {
                $arrBlocks[$objResult->fields['placeholder']][$objResult->fields['id']] = $objResult->fields['id'];
                $objResult->MoveNext();
            }
            $objResult = $objDatabase->Execute('
                SELECT
                    `b`.`id`,
                    `b`.`global`,
                    `b`.`direct`,
                    `b`.`category`
                FROM
                    `'.DBPREFIX.'module_block_blocks` AS `b`
                WHERE
                    `b`.`global` = 1
                    OR `b`.`direct` = 0
                    OR `b`.`category` = 0
                ORDER BY
                    `b`.`order`
            ');
            if ($objResult === false) {
                return $arrBlocks;
            }
            while (!$objResult->EOF) {
                if ($objResult->fields['global'] == 1) {
                    $arrBlocks['global'][$objResult->fields['id']] = $objResult->fields['id'];
                }
                if ($objResult->fields['direct'] == 0) {
                    $arrBlocks['direct'][$objResult->fields['id']] = $objResult->fields['id'];
                }
                if ($objResult->fields['category'] == 0) {
                    $arrBlocks['category'][$objResult->fields['id']] = $objResult->fields['id'];
                }
                $objResult->MoveNext();
            }
        }
        return $arrBlocks;
    }

    function _setBlocksForPage($page, $blockIds) {
        global $objDatabase;

        if (
            \Cx\Core\Setting\Controller\Setting::getValue(
                'CLX5087_BlockHistory',
                'Config'
            ) === 'on'
        ) {
            $em = \Cx\Core\Core\Controller\Cx::instanciate()->getDb()->getEntityManager();
            $blockRepo = $em->getRepository('\Cx\Modules\Block\Model\Entity\Block');
            $relPageRepo = $em->getRepository('\Cx\Modules\Block\Model\Entity\RelPage');

            $relPages = $relPageRepo->findBy(array(
                'page' => $page,
                'placeholder' => 'global',
            ));
            foreach ($relPages as $relPage) {
                $em->remove($relPage);
            }
            $em->flush();

            $blocks = array();
            foreach ($blockIds as $blockId) {
                $block = $blockRepo->findOneBy(array('id' => $blockId));
                array_push($blocks, $block);

                // block is global and will be shown on all pages, don't need to save the relation
                if ($block->getShowInGlobal() == 1) {
                    continue;
                }

                $relPage = new \Cx\Modules\Block\Model\Entity\RelPage();
                $relPage->setBlock($block);
                $relPage->setPage($page);
                $relPage->setPlaceholder('global');
                $em->persist($relPage);

                // if the block was not global till now, make it global
                if ($block->getShowInGlobal() == 0) {
                    $block->setShowInGlobal(1);
                }
            }

            $qb = $em->createQueryBuilder();
            $qb->delete('\Cx\Modules\Block\Model\Entity\RelPage', 'rp')
                ->where('rp.placeholder = :placeholder')
                ->andWhere('rp.page = :page');
            if (count($blocks)) {
                $qb->andWhere($qb->expr()->notIn('rp.block', $blocks));
            }
            $qb->setParameters(array(
                'placeholder' => 'global',
                'page' => $page,
            ))->getQuery()->getResult();

            foreach ($relPages as $relPage) {
                $em->remove($relPage);
            }
            $em->flush();
        } else {
            $pageId = $page->getId();

            $result = $objDatabase->Execute("SELECT `block_id` FROM `".DBPREFIX."module_block_rel_pages` WHERE `page_id` = " . intval($pageId) . " AND `placeholder` = 'global'");
            $oldBlockIds = array();
            if ($result) {
                while (!$result->EOF) {
                    $oldBlockIds[] = $result->fields['block_id'];
                    $result->MoveNext();
                }
            }
            $objDatabase->Execute("DELETE FROM ".DBPREFIX."module_block_rel_pages WHERE page_id = " . intval($pageId) . " AND placeholder = 'global'");

            $values = array();
            foreach ($blockIds as $blockId) {
                $block = $this->_getBlock($blockId);
                // block is global and will be shown on all pages, don't need to save the relation
                if ($block['global'] == 1) {
                    continue;
                }
                // if the block was not global till now, make it global
                if ($block['global'] == 0) {
                    $objDatabase->Execute("UPDATE `" . DBPREFIX . "module_block_blocks` SET `global` = 2 WHERE `id` = " . intval($blockId));
                }
                $values[] = '
                    (
                        \'' . intval($blockId) . '\',
                        \'' . intval($pageId) . '\',
                        \'global\'
                    )';
            }
            if (!empty($values)) {
                $query = 'INSERT INTO
                        `' . DBPREFIX . 'module_block_rel_pages`
                        (
                            `block_id`,
                            `page_id`,
                            `placeholder`
                        )
                    VALUES' . implode(', ', $values);
                $objDatabase->Execute($query);
            }
            $objDatabase->Execute('
                DELETE FROM
                    `' . DBPREFIX . 'module_block_rel_pages`
                WHERE
                    `page_id` = \'' . $pageId . '\' AND
                    `block_id` NOT IN
                        (
                            \'' . implode('\',\'', array_map('intval', $blockIds)).'\'
                        ) AND
                    `placeholder` = \'global\'
            ');

            if (
                !empty($values) ||
                count($blockIds) != count($oldBlockIds) ||
                !empty(array_diff($blockIds, $oldBlockIds))
            ) {
                // clear page cache to refresh global placeholders
                \Cx\Core\Core\Controller\Cx::instanciate()->getComponent('Cache')->clearCache();
            }
        }
    }

    /**
     * Set block
     *
     * Parse the block with the id $id
     *
     * @access private
     * @param integer $id Block ID
     * @param string &$code
     * @param int $pageId
     * @global ADONewConnection
     * @global integer
     */
    function _setBlock($id, &$code, $pageId = 0)
    {
        $now = time();

        if (
            \Cx\Core\Setting\Controller\Setting::getValue(
                'CLX5087_BlockHistory',
                'Config'
            ) === 'on'
        ) {
            $block = $id;
            $page = $pageId;
            $em = \Cx\Core\Core\Controller\Cx::instanciate()->getDb()->getEntityManager();
            $localeRepo = $em->getRepository('\Cx\Core\Locale\Model\Entity\Locale');
            $locale = $localeRepo->findOneBy(array('id' => FRONTEND_LANG_ID));

            $qb = $em->createQueryBuilder();
            $orX = $qb->expr()->orX();
            $qb2 = $em->createQueryBuilder();
            $allBlocks = $qb->select('b')
                ->from('\Cx\Modules\Block\Model\Entity\Block', 'b')
                ->from('\Cx\Modules\Block\Model\Entity\RelLangContent', 'rlc')
                ->where('b = :block')
                /*->andWhere(
                    $orX->addMultiple(array(
                        'b.showInDirect = 0',
                        $qb2->select('count(rp)')
                            ->from('\Cx\Modules\Block\Model\Entity\Block', 'b')
                            ->from('\Cx\Modules\Block\Model\Entity\RelPage', 'rp')
                            ->where('rp.page = :page')
                            ->andWhere('rp.block = b')
                            ->andWhere('rp.placeholder = \'direct\'')
                            ->setParameter('page', $page)
                            ->getQuery()
                            ->getSingleResult()[1] . ' > 0'
                    ))
                )*/
                ->andWhere('rlc.block = b')
                ->andWhere('(rlc.locale = :locale AND rlc.active = 1)')
                ->andWhere('(b.start <= :now OR b.start = 0)')
                ->andWhere('(b.end >= :now OR b.end = 0)')
                ->andWhere('b.active = 1')
                ->setParameters(array(
                    'block' => $block,
                    'locale' => $locale,
                    'now' => $now,
                ))
                ->getQuery()
                ->getResult();

            // filter blocks by assigned content page
            $blocks = array();
            foreach ($allBlocks as $block) {
                if ($block->getShowInDirect()) {
                    foreach ($block->getRelPages() as $relPage) {
                        if ($relPage->getPlaceholder() != 'direct') {
                            continue;
                        }
                        if ($relPage->getPage()->getId() == $page->getId()) {
                            $blocks[] = array('id' => $block->getId());
                            continue;
                        }
                    }
                    continue;
                }
                $blocks[] = array('id' => $block->getId());
            }

            $this->replaceBlocks(
                $this->blockNamePrefix . $block->getId(),
                $blocks,
                $page,
                $code
            );
        } else {
            $activeFilter = '
                AND (
                    tblBlock.`start` <= ' . $now . '
                    OR tblBlock.`start` = 0
                )
                AND (
                    tblBlock.`end` >= ' . $now . '
                    OR tblBlock.end = 0
                )
                AND
                    tblBlock.active = 1
            ';
            // Note: This is a workaround as content panes are no real widgets yet.
            //
            // In case the frontend editing is not in use
            // then we can always load the content pane.
            // The JsonData adapter will then decide if the content pane is active or not.
            // The check for frontend editing is required, as otherwise the frontend editing
            // would inject an empty DIV element in  case the content pane is inactive.
            $cx = \Cx\Core\Core\Controller\Cx::instanciate();
            if (!$cx->getComponent('FrontendEditing')->frontendEditingIsActive(false, false)) {
                $activeFilter = '';
            }
            // End of workaround

            $this->replaceBlocks(
                $this->blockNamePrefix . $id,
                '
                    SELECT
                        tblBlock.id
                    FROM
                        ' . DBPREFIX . 'module_block_blocks AS tblBlock,
                        ' . DBPREFIX . 'module_block_rel_lang_content AS tblContent
                    WHERE
                        tblBlock.id = ' . intval($id) . '
                        AND (
                            tblBlock.`direct` = 0
                            OR (
                                SELECT
                                    count(1)
                                FROM
                                    `' . DBPREFIX . 'module_block_rel_pages` AS tblRel
                                WHERE
                                    tblRel.`page_id` = ' . intval($pageId) . '
                                    AND tblRel.`block_id` = tblBlock.`id`
                                    AND tblRel.`placeholder` = "direct") > 0
                            )
                        AND tblContent.block_id = tblBlock.id
                        AND (
                            tblContent.lang_id = ' . FRONTEND_LANG_ID . '
                            AND tblContent.active = 1
                        )' . $activeFilter,
                $pageId,
                $code
            );
        }
    }

    /**
     * Set category block
     *
     * Parse the category block with the id $id
     *
     * @access private
     * @param integer $id Category ID
     * @param string &$code
     * @param int $pageId
     * @global ADONewConnection
     * @global integer
     */
    function _setCategoryBlock($id, &$code, $pageId = 0)
    {
        if (
            \Cx\Core\Setting\Controller\Setting::getValue(
                'CLX5087_BlockHistory',
                'Config'
            ) === 'on'
        ) {
            $category = $id;
            $page = $pageId;
            $now = time();

            $em = \Cx\Core\Core\Controller\Cx::instanciate()->getDb()->getEntityManager();
            $localeRepo = $em->getRepository('\Cx\Core\Locale\Model\Entity\Locale');
            $locale = $localeRepo->findOneBy(array('id' => FRONTEND_LANG_ID));

            $qb = $em->createQueryBuilder();
            $orX = $qb->expr()->orX();
            $qb2 = $em->createQueryBuilder();
            $allBlocks = $qb->select('b')
                ->from('\Cx\Modules\Block\Model\Entity\Block', 'b')
                ->innerJoin('\Cx\Modules\Block\Model\Entity\RelLangContent', 'rlc', 'WITH', 'rlc.block = b')
                ->where('b.category = :category')
                /*->andWhere(
                    $orX->addMultiple(array(
                        'b.showInCategory = 0',
                        $qb2->select('count(rp)')
                            ->from('\Cx\Modules\Block\Model\Entity\RelPage', 'rp')
                            ->where('rp.block = b')
                            ->andWhere('rp.page = :page')
                            ->andWhere('rp.placeholder = \'category\'')
                            ->setParameter('page', $page)
                            ->getDQL()
                            ->getSingleResult()[1] . ' > 0'
                    ))
                )*/
                ->andWhere('b.active = 1')
                ->andWhere('(b.start <= :now OR b.start = 0)')
                ->andWhere('(b.end >= :now OR b.end = 0)')
                ->andWhere('(rlc.locale = :locale AND rlc.active = 1)')
                ->orderBy('b.order')
                ->setParameters(array(
                    'category' => $category,
                    'now' => $now,
                    'locale' => $locale,
                ))
                ->getQuery()
                ->getResult();

            // filter blocks by assigned content page
            $blocks = array();
            foreach ($allBlocks as $block) {
                if ($block->getShowInCategory()) {
                    foreach ($block->getRelPages() as $relPage) {
                        if ($relPage->getPlaceholder() != 'category') {
                            continue;
                        }
                        if ($relPage->getPage()->getId() == $page->getId()) {
                            $blocks[] = array('id' => $block->getId());
                            continue;
                        }
                    }
                    continue;
                }
                $blocks[] = array('id' => $block->getId());
            }

            $this->replaceBlocks(
                $this->blockNamePrefix . 'CAT_' . $category->getId(),
                $blocks,
                $page,
                $code,
                $category->getSeperator()
            );
        } else {
            $category = $this->_getCategory($id);
            $separator = '';
            if ($category) {
                $separator = $category['seperator'];
            }

            $now = time();

            $activeFilter = '
                AND tblBlock.`active` = 1
                AND (tblBlock.`start` <= ' . $now . ' OR tblBlock.`start` = 0)
                AND (tblBlock.`end` >= ' . $now . ' OR tblBlock.`end` = 0)
            ';
            // Note: This is a workaround as content panes are no real widgets yet.
            //
            // In case the frontend editing is not in use and there
            // is no content pane separator for the category defined,
            // then we can load all content panes into the template at once.
            // The JsonData adapter will then decide which content panes to display and
            // which not.
            // The check for frontend editing is required, as otherwise the frontend editing
            // would inject empty DIV elements for non-active content panes.
            // The check for the content pane separator is required, as otherwise
            // the system would output the separator for each non-active content pane.
            $cx = \Cx\Core\Core\Controller\Cx::instanciate();
            if (
                !$cx->getComponent('FrontendEditing')->frontendEditingIsActive(false, false) &&
                $separator == ''
            ) {
                $activeFilter = '';
            }
            // End of workaround

            $this->replaceBlocks(
                $this->blockNamePrefix . 'CAT_' . $id,
                '
                    SELECT
                        tblBlock.id
                    FROM
                        `' . DBPREFIX . 'module_block_blocks` AS tblBlock
                    INNER JOIN
                        `' . DBPREFIX . 'module_block_rel_lang_content` AS tblContent
                    ON
                        tblBlock.id = tblContent.block_id
                    WHERE
                        tblBlock.`cat` = ' . $id . '
                        AND (
                            tblBlock.`category` = 0
                            OR (
                                SELECT
                                    count(1)
                                FROM
                                    `' . DBPREFIX . 'module_block_rel_pages` AS tblRel
                                WHERE
                                    tblRel.`page_id` = ' . intval($pageId) . '
                                    AND tblRel.`block_id` = tblBlock.`id`
                                    AND tblRel.`placeholder` = "category") > 0
                            )
                        ' . $activeFilter . '
                        AND (tblContent.lang_id = ' . FRONTEND_LANG_ID . ' AND tblContent.active = 1)
                    ORDER BY
                        tblBlock.`order`
                ',
                $pageId,
                $code,
                $separator
            );
        }
    }

    /**
     * Set block Global
     *
     * Parse the block with the id $id
     *
     * @access private
     * @param integer $id
     * @param string &$code
     * @global ADONewConnection
     */
    function _setBlockGlobal(&$code, $pageId = 0)
    {
        global $objDatabase;

        if (
            \Cx\Core\Setting\Controller\Setting::getValue(
                'CLX5087_BlockHistory',
                'Config'
            ) === 'on'
        ) {
            $page = $pageId;
            $em = \Cx\Core\Core\Controller\Cx::instanciate()->getDb()->getEntityManager();

            // fetch separator
            \Cx\Core\Setting\Controller\Setting::init('Block', 'setting');
            $separator = \Cx\Core\Setting\Controller\Setting::getValue('blockGlobalSeperator', 'Block');

            $now = time();

            $localeRepo = $em->getRepository('\Cx\Core\Locale\Model\Entity\Locale');
            $locale = $localeRepo->findOneBy(array('id' => FRONTEND_LANG_ID));

            $qb1 = $em->createQueryBuilder();
            $result1 = $qb1->select('
                    b.id AS id,
                    rlc.content AS content,
                    b.order
                ')
                ->from('\Cx\Modules\Block\Model\Entity\Block', 'b')
                ->innerJoin('\Cx\Modules\Block\Model\Entity\RelLangContent', 'rlc', 'WITH', 'rlc.block = b')
                ->innerJoin('\Cx\Modules\Block\Model\Entity\RelPage', 'rp', 'WITH', 'rp.block = b')
                ->where('b.showInGlobal = 2')
                ->andWhere('rp.page = :page')
                ->andWhere('rlc.locale = :locale')
                ->andWhere('rlc.active = 1')
                ->andWhere('b.active = 1')
                ->andWhere('rp.placeholder = \'global\'')
                ->andWhere('(b.start <= :now OR b.start = 0)')
                ->andWhere('(b.end >= :now OR b.end = 0)')
                ->orderBy('b.order')
                ->setParameters(array(
                    'locale' => $locale,
                    'page' => $page,
                    'now' => $now,
                ))
                ->getQuery()
                ->getResult();

            $qb2 = $em->createQueryBuilder();
            $result2 = $qb2->select('
                    b.id AS id,
                    rlc.content AS content,
                    b.order
                ')
                ->from('\Cx\Modules\Block\Model\Entity\Block', 'b')
                ->innerJoin('\Cx\Modules\Block\Model\Entity\RelLangContent', 'rlc', 'WITH', 'rlc.block = b')
                ->where('b.showInGlobal = 1')
                ->andWhere('rlc.locale = :locale')
                ->andWhere('rlc.active = 1')
                ->andWhere('b.active = 1')
                ->andWhere('(b.start <= :now OR b.start = 0)')
                ->andWhere('(b.end >= :now OR b.end = 0)')
                ->orderBy('b.order')
                ->setParameters(array(
                    'locale' => $locale,
                    'now' => $now,
                ))
                ->getQuery()
                ->getResult();

            $blocks = array_unique(array_merge($result1, $result2));
            usort($blocks, array($this, 'cmpByOrder'));

            $this->replaceBlocks(
                $this->blockNamePrefix . 'GLOBAL',
                $blocks,
                $page,
                $code,
                $separator
            );
        } else {
            // fetch separator
            $separator = '';
            $objResult = $objDatabase->Execute(
                '
                    SELECT
                        `value`
                    FROM
                        `' . DBPREFIX . 'module_block_settings`
                    WHERE
                        `name` = "blockGlobalSeperator"
                    LIMIT
                        1
                '
            );
            if ($objResult !== false) {
                $separator = $objResult->fields['value'];
            }

            $now = time();

            $activeFilter = '
                AND tblBlock.active = 1
                AND (
                    tblBlock.`start` <= ' . $now . '
                    OR tblBlock.`start` = 0
                )
                AND (
                    tblBlock.`end` >= ' . $now . '
                    OR tblBlock.end = 0
                )
            ';
            // Note: This is a workaround as content panes are no real widgets yet.
            //
            // In case the frontend editing is not in use and there
            // is no content pane separator for the global content pane defined,
            // then we can load all content panes into the template at once.
            // The JsonData adapter will then decide which content panes to display and
            // which not.
            // The check for frontend editing is required, as otherwise the frontend editing
            // would inject empty DIV elements for non-active content panes.
            // The check for the content pane separator is required, as otherwise
            // the system would output the separator for each non-active content pane.
            $cx = \Cx\Core\Core\Controller\Cx::instanciate();
            if (
                !$cx->getComponent('FrontendEditing')->frontendEditingIsActive(false, false) &&
                $separator == ''
            ) {
                $activeFilter = '';
            }
            // End of workaround

            $this->replaceBlocks(
                $this->blockNamePrefix . 'GLOBAL',
                '
                    SELECT
                        `tblBlock`.`id` AS `id`,
                        `tblContent`.`content` AS `content`,
                        `tblBlock`.`order`
                    FROM
                        ' . DBPREFIX . 'module_block_blocks AS tblBlock
                    INNER JOIN
                        ' . DBPREFIX . 'module_block_rel_lang_content AS tblContent
                    ON
                        tblContent.`block_id` = tblBlock.`id` 
                    INNER JOIN
                        ' . DBPREFIX . 'module_block_rel_pages AS tblPage
                    ON
                        tblPage.`block_id` = tblBlock.`id`
                    WHERE
                        tblBlock.`global` = 2
                        AND tblPage.page_id = ' . intval($pageId) . '
                        AND tblContent.`lang_id` = ' . FRONTEND_LANG_ID . '
                        AND tblContent.`active` = 1
                        AND tblPage.placeholder = "global"
                        ' . $activeFilter . '
                    UNION DISTINCT
                        SELECT
                            tblBlock.`id` AS `id`,
                            tblContent.`content` AS `content`,
                            tblBlock.`order`
                        FROM
                            ' . DBPREFIX . 'module_block_blocks AS tblBlock
                        INNER JOIN
                            ' . DBPREFIX . 'module_block_rel_lang_content AS tblContent
                        ON
                            tblContent.`block_id` = tblBlock.`id` 
                        WHERE
                            tblBlock.`global` = 1
                            AND tblContent.`lang_id` = ' . FRONTEND_LANG_ID . '
                            AND tblContent.`active` = 1
                            ' . $activeFilter . '
                    ORDER BY
                        `order`
                ',
                $pageId,
                $code,
                $separator
            );
        }
    }

    /**
     * Compares two arrays by order attribute
     *
     * @param array $a
     * @param array $b
     * @return int relative position
     */
    function cmpByOrder($a, $b)
    {
        if (
            \Cx\Core\Setting\Controller\Setting::getValue(
                'CLX5087_BlockHistory',
                'Config'
            ) !== 'on'
        ) {
            throw new \Exception(
                'Method ' . __METHOD__ . ' called but feature flag CLX5087_BlockHistory not active'
            );
        }
        if ($a['order'] == $b['order']) {
            return 0;
        }
        return ($a['order'] < $b['order']) ? -1 : 1;
    }

    /**
     * Generate ESI content for random block widget
     *
     * @todo    This is legacy code and must be replaced by proper EsiWidget
     *          implementation through a EsiWidgetController.
     * @deprecated
     */
    protected function parseBlockRandom(&$code, $id, $pageId = 0)
    {
        switch ($id) {
            case '1':
                $blockNr        = "";
                break;
            case '2':
                $blockNr        = "_2";
                break;
            case '3':
                $blockNr        = "_3";
                break;
            case '4':
                $blockNr        = "_4";
                break;
        }
        $placeholderName = $this->blockNamePrefix . 'RANDOMIZER' . $blockNr;
        $content = \Cx\Core\Core\Controller\Cx::instanciate()->getComponent('Cache')->getEsiContent(
            'Block',
            'getRandomBlockContent',
            array(
                'randomizer' => $id,
                'locale' => \FWLanguage::getLanguageCodeById(FRONTEND_LANG_ID),
                'page' => $pageId,
            )
        );
        $code = str_replace('{' . $placeholderName . '}', $content, $code);
    }

    /**
     * Set block Random
     *
     * Parse the block with the id $id
     *
     * @todo    This is legacy code and must be replaced by proper EsiWidget
     *          implementation through a EsiWidgetController.
     * @deprecated
     * @access private
     * @param integer $id
     * @param string &$code
     * @global ADONewConnection
     */
    function _setBlockRandom(&$code, $id, $pageId = 0)
    {
        global $objDatabase;

        if (
            \Cx\Core\Setting\Controller\Setting::getValue(
                'CLX5087_BlockHistory',
                'Config'
            ) === 'on'
        ) {
            $page = $pageId;
            $now = time();

            $em = \Cx\Core\Core\Controller\Cx::instanciate()->getDb()->getEntityManager();
            $localeRepo = $em->getRepository('\Cx\Core\Locale\Model\Entity\Locale');
            $locale = $localeRepo->findOneBy(array('id' => FRONTEND_LANG_ID));

            $qb = $em->createQueryBuilder();
            $qb->select('b.id')
                ->from('\Cx\Modules\Block\Model\Entity\Block', 'b')
                ->from('\Cx\Modules\Block\Model\Entity\RelLangContent', 'rlc')
                ->where('rlc.block = b')
                ->andWhere('(rlc.locale = :locale AND rp.active = 1)')
                ->andWhere('(b.start <= :now OR b.start = 0)')
                ->andWhere('(b.end >= :now OR b.end = 0)')
                ->andWhere('b.active = 1');

            // Get Block Name and Status
            $blockNr = '';
            switch ($id) {
                case '1':
                    $qb->andWhere('b.random = 1');
                    break;
                case '2':
                    $qb->andWhere('b.random2 = 1');
                    $blockNr = '_2';
                    break;
                case '3':
                    $qb->andWhere('b.random3 = 1');
                    $blockNr = '_3';
                    break;
                case '4':
                    $qb->andWhere('b.random4 = 1');
                    $blockNr = '_4';
                    break;
            }

            $blocks = $qb->setParameters(array(
                'locale' => $locale,
                'now' => $now,
            ))->getQuery()->getResult();

            if (count($blocks) <= 0) {
                return;
            }

            foreach ($blocks as $block) {
                $arrActiveBlocks[] = $block['id'];
            }

            $this->replaceBlocks(
                $this->blockNamePrefix . 'RANDOMIZER' . $blockNr,
                $blocks,
                $page,
                $code,
                '',
                true
            );
        } else {
            $now = time();
            $query = "  SELECT
                            tblBlock.id
                        FROM
                            ".DBPREFIX."module_block_blocks AS tblBlock,
                            ".DBPREFIX."module_block_rel_lang_content AS tblContent
                        WHERE
                            tblContent.block_id = tblBlock.id
                        AND
                            (tblContent.lang_id = ".FRONTEND_LANG_ID." AND tblContent.active = 1)
                        AND (tblBlock.`start` <= $now OR tblBlock.`start` = 0)
                        AND (tblBlock.`end` >= $now OR tblBlock.end = 0)
                        AND
                            tblBlock.active = 1 ";

            // Get Block Name and Status
            switch ($id) {
                case '1':
                    $query .= 'AND tblBlock.random=1';
                    $blockNr = '';
                    break;
                case '2':
                    $query .= 'AND tblBlock.random_2=1';
                    $blockNr = '_2';
                    break;
                case '3':
                    $query .= 'AND tblBlock.random_3=1';
                    $blockNr = '_3';
                    break;
                case '4':
                    $query .= 'AND tblBlock.random_4=1';
                    $blockNr = '_4';
                    break;
            }

            $this->replaceBlocks(
                $this->blockNamePrefix . 'RANDOMIZER' . $blockNr,
                $query,
                $pageId,
                $code,
                '',
                true
            );
        }
    }

    /**
     * Replaces a placeholder with block content
     * @param string $placeholderName Name of placeholder to replace
     * @param string $query SQL query used to fetch blocks
     * @param int $pageId ID of the current page, 0 if no page available
     * @param string $code (by reference) Code to replace placeholder in
     * @param string $separator (optional) Separator used to separate the blocks
     * @param boolean $randomize (optional) Wheter to randomize the blocks or not, default false
     */
    protected function replaceBlocks($placeholderName, $query, $pageId, &$code, $separator = '', $randomize = false) {
        global $objDatabase;

        if (
            \Cx\Core\Setting\Controller\Setting::getValue(
                'CLX5087_BlockHistory',
                'Config'
            ) === 'on'
        ) {
            $blocks = $query;
            $page = $pageId;
            // find all block IDs to parse
            if (count($blocks) <= 0) {
                // drop empty placeholders
                $code = str_replace('{' . $placeholderName . '}', '', $code);
                return;
            }
            $blockIds = array();
            foreach ($blocks as $block) {
                if (!$this->checkTargetingOptions($block['id'])) {
                    continue;
                }
                $blockIds[] = $block['id'];
            }

            $pageId = 0;
            if ($page) {
                $pageId = $page->getId();
            }

            // parse
            $cx = \Cx\Core\Core\Controller\Cx::instanciate();
            $em = $cx->getDb()->getEntityManager();
            $systemComponentRepo = $em->getRepository('Cx\Core\Core\Model\Entity\SystemComponent');
            $frontendEditingComponent = $systemComponentRepo->findOneBy(array('name' => 'FrontendEditing'));

            if ($randomize) {
                $esiBlockInfos = array();
                foreach ($blockIds as $blockId) {
                    $esiBlockInfos[] = array(
                        'Block',
                        'getBlockContent',
                        array(
                            'block' => $blockId,
                            'lang' => \FWLanguage::getLanguageCodeById(FRONTEND_LANG_ID),
                            'page' => $pageId,
                        )
                    );
                }
                $blockContent = $cx->getComponent('Cache')->getRandomizedEsiContent(
                    $esiBlockInfos
                );
                $frontendEditingComponent->prepareBlock(
                    $blockId,
                    $blockContent
                );
                $content = $blockContent;
            } else {
                $contentList = array();
                foreach ($blockIds as $blockId) {
                    $blockContent = $cx->getComponent('Cache')->getEsiContent(
                        'Block',
                        'getBlockContent',
                        array(
                            'block' => $blockId,
                            'lang' => \FWLanguage::getLanguageCodeById(FRONTEND_LANG_ID),
                            'page' => $pageId,
                        )
                    );
                    $frontendEditingComponent->prepareBlock(
                        $blockId,
                        $blockContent
                    );
                    $contentList[] = $blockContent;
                }
                $content = implode($separator, $contentList);
            }

            \Cx\Core\Setting\Controller\Setting::init('Block', 'setting');
            $markParsedBlock = \Cx\Core\Setting\Controller\Setting::getValue('markParsedBlock', 'Block');
            if (!empty($markParsedBlock)) {
                $content = "<!-- start $placeholderName -->$content<!-- end $placeholderName -->";
            }
        } else {
            // find all block IDs to parse
            $objResult = $objDatabase->Execute($query);
            $blockIds = array();
            if ($objResult === false || $objResult->RecordCount() <= 0) {
                // drop empty placeholders
                $code = str_replace('{' . $placeholderName . '}', '', $code);
                return;
            }
            while (!$objResult->EOF) {
                if (!$this->checkTargetingOptions($objResult->fields['id'])) {
                    $objResult->MoveNext();
                    continue;
                }
                $blockIds[] = $objResult->fields['id'];
                $objResult->MoveNext();
            }

            // parse
            $cx = \Cx\Core\Core\Controller\Cx::instanciate();
            $em = $cx->getDb()->getEntityManager();
            $systemComponentRepo = $em->getRepository('Cx\Core\Core\Model\Entity\SystemComponent');
            $frontendEditingComponent = $systemComponentRepo->findOneBy(array('name' => 'FrontendEditing'));
            $settings = $this->getSettings();

            if ($randomize) {
                $esiBlockInfos = array();
                foreach ($blockIds as $blockId) {
                    $esiBlockInfos[] = array(
                        'Block',
                        'getBlockContent',
                        array(
                            'block' => $blockId,
                            'lang' => \FWLanguage::getLanguageCodeById(FRONTEND_LANG_ID),
                            'page' => $pageId,
                        )
                    );
                }
                $blockContent = $cx->getComponent('Cache')->getRandomizedEsiContent(
                    $esiBlockInfos
                );
                $frontendEditingComponent->prepareBlock(
                    $blockId,
                    $blockContent
                );
                $content = $blockContent;
            } else {
                $contentList = array();
                foreach ($blockIds as $blockId) {
                    $blockContent = $cx->getComponent('Cache')->getEsiContent(
                        'Block',
                        'getBlockContent',
                        array(
                            'block' => $blockId,
                            'lang' => \FWLanguage::getLanguageCodeById(FRONTEND_LANG_ID),
                            'page' => $pageId,
                        )
                    );
                    $frontendEditingComponent->prepareBlock(
                        $blockId,
                        $blockContent
                    );
                    $contentList[] = $blockContent;
                }
                $content = implode($separator, $contentList);
            }

            if (!empty($settings['markParsedBlock'])) {
                $content = "<!-- start $placeholderName -->$content<!-- end $placeholderName -->";
            }
        }

        $code = str_replace('{' . $placeholderName . '}', $content, $code);
    }

    /**
     * Get the settings from database
     *
     * @staticvar array $settings settings array
     *
     * @return array settings array
     */
    public function getSettings()
    {

        static $settings = array();
        if (!empty($settings)) {
            return $settings;
        }

        $query = '
            SELECT
                `name`,
                `value`
            FROM
                `'. DBPREFIX .'module_block_settings`';
        $setting = \Cx\Core\Core\Controller\Cx::instanciate()
                    ->getDb()
                    ->getAdoDb()
                    ->Execute($query);
        if ($setting === false) {
            return array();
        }
        while (!$setting->EOF) {
            $settings[$setting->fields['name']] = $setting->fields['value'];
            $setting->MoveNext();
        }

        return $settings;
    }

    /**
     * Save the settings associated to the block system
     *
     * @access    private
     * @param    array $arrSettings
     */
    function _saveSettings($arrSettings)
    {
        \Cx\Core\Setting\Controller\Setting::init('Config', 'component', 'Yaml');
        if (isset($arrSettings['blockStatus'])) {
            if (!\Cx\Core\Setting\Controller\Setting::isDefined('blockStatus')) {
                \Cx\Core\Setting\Controller\Setting::add('blockStatus', $arrSettings['blockStatus'], 1,
                    \Cx\Core\Setting\Controller\Setting::TYPE_RADIO, '1:TXT_ACTIVATED,0:TXT_DEACTIVATED', 'component');
            } else {
                \Cx\Core\Setting\Controller\Setting::set('blockStatus', $arrSettings['blockStatus']);
                \Cx\Core\Setting\Controller\Setting::update('blockStatus');
            }
        }
        if (isset($arrSettings['blockRandom'])) {
            if (!\Cx\Core\Setting\Controller\Setting::isDefined('blockRandom')) {
                \Cx\Core\Setting\Controller\Setting::add('blockRandom', $arrSettings['blockRandom'], 1,
                    \Cx\Core\Setting\Controller\Setting::TYPE_RADIO, '1:TXT_ACTIVATED,0:TXT_DEACTIVATED', 'component');
            } else {
                \Cx\Core\Setting\Controller\Setting::set('blockRandom', $arrSettings['blockRandom']);
                \Cx\Core\Setting\Controller\Setting::update('blockRandom');
            }
        }

        // clear page cache to refresh all block placeholders
        \Cx\Core\Core\Controller\Cx::instanciate()->getComponent('Cache')->clearCache();
    }

    /**
     * create the categories dropdown
     *
     * @param array $arrCategories
     * @param array $arrOptions
     * @param integer $level
     * @return string categories as HTML options
     */
    function _getCategoriesDropdown($parent = 0, $catId = 0, $arrCategories = array(), $arrOptions = array(), $level = 0)
    {
        global $objDatbase;

        $first = false;
        if (count($arrCategories) == 0) {
            $first = true;
            $level = 0;
            $this->_getCategories();
            $arrCategories = $this->_categories[0]; //first array contains all root categories (parent id 0)
        }

        foreach ($arrCategories as $arrCategory) {
            $this->_categoryOptions[] =
                '<option value="' . $arrCategory['id'] . '" '
                . (
                $parent > 0 && $parent == $arrCategory['id']  //selected if parent specified and id is parent
                    ? 'selected="selected"'
                    : ''
                )
                . (
                ($catId > 0 && in_array($arrCategory['id'], $this->_getChildCategories($catId))) || $catId == $arrCategory['id'] //disable children and self
                    ? 'disabled="disabled"'
                    : ''
                )
                . ' >' // <option>
                . str_repeat('&nbsp;', $level * 4)
                . htmlentities($arrCategory['name'], ENT_QUOTES, CONTREXX_CHARSET)
                . '</option>';

            if (!empty($this->_categories[$arrCategory['id']])) {
                $this->_getCategoriesDropdown($parent, $catId, $this->_categories[$arrCategory['id']], $arrOptions, $level + 1);
            }
        }
        if ($first) {
            return implode("\n", $this->_categoryOptions);
        }
    }

    /**
     * Save a block category
     * @param   string      $name
     * @param   string      $seperator
     * @param   int         $id
     * @param   int         $parent
     * @param   int         $order
     * @param   int         $status
     * @return  int|bool                Inserted ID, or false on failure
     */
    function _saveCategory(
        $name,
        $seperator,
        $id = 0,
        $parent = 0,
        $order = 1,
        $status = 1
    ) {
        global $objDatabase;

        if (
            \Cx\Core\Setting\Controller\Setting::getValue(
                'CLX5087_BlockHistory',
                'Config'
            ) === 'on'
        ) {
            // check for necessary arguments
            if (empty($name)) {
                throw new NotEnoughArgumentsException('not enough arguments');
            }

            if ($id > 0 && $id == $parent) { //don't allow category to attach to itself
                return false;
            }

            if ($id == 0) { //if new record then set to NULL for auto increment
                $id = 'NULL';
            } else {
                $arrChildren = $this->_getChildCategories($id);
                if (in_array($parent, $arrChildren)) { //don't allow category to be attached to one of it's own children
                    return false;
                }
            }

            $em = \Cx\Core\Core\Controller\Cx::instanciate()->getDb()->getEntityManager();
            $categoryRepo = $em->getRepository('\Cx\Modules\Block\Model\Entity\Category');
            $category = $categoryRepo->findOneBy(array('id' => $id));

            $new = false;
            if (!$category) {
                if ($id > 0) {
                    throw new NoCategoryFoundException('no category found');
                }
                $category = new \Cx\Modules\Block\Model\Entity\Category();
                $new = true;
            }

            $parent = $categoryRepo->findOneBy(array('id' => $parent));
            $category->setParent($parent);
            $category->setName($name);
            $category->setSeperator($seperator);
            $category->setOrder($order);
            $category->setStatus($status);

            if ($new) {
                $em->persist($category);
                $em->flush();
                $em->refresh($category);
            } else {
                $em->flush();
            }

            return $category->getId();
        } else {
            $id = intval($id);
            if ($id > 0 && $id == $parent) { //don't allow category to attach to itself
                return false;
            }

            if ($id == 0) { //if new record then set to NULL for auto increment
                $id = 'NULL';
            } else {
                $arrChildren = $this->_getChildCategories($id);
                if (in_array($parent, $arrChildren)) { //don't allow category to be attached to one of it's own children
                    return false;
                }
            }
            if($objDatabase->Execute('
                INSERT INTO `'.DBPREFIX."module_block_categories`
                (`id`, `parent`, `name`, `seperator`, `order`, `status`)
                VALUES
                ($id, $parent, '$name', '$seperator', $order, $status )
                ON DUPLICATE KEY UPDATE
                `id`       = $id,
                `parent`   = $parent,
                `name`     = '$name',
                `seperator`= '$seperator',
                `order`    = $order,
                `status`   = $status"))
            {
                // clear page cache to refresh category placeholders
                \Cx\Core\Core\Controller\Cx::instanciate()->getComponent('Cache')->clearCache();
                return $id == 'NULL' ? $objDatabase->Insert_ID() : $id;
            } else {
                return false;
            }
        }
    }

    /**
     * return all child caegories of a cateogory
     *
     * @param integer ID of category to get list of children from
     * @param array cumulates the child arrays, internal use
     * @return array IDs of children
     */
    function _getChildCategories($id, &$_arrChildCategories = array())
    {
        if (empty($this->_categories)) {
            $this->_getCategories();
        }
        if (!isset($this->_categories[$id])) {
            return array();
        }
        foreach ($this->_categories[$id] as $cat) {
            if (!empty($this->_categories[$cat['parent']])) {
                $_arrChildCategories[] = $cat['id'];
                $this->_getChildCategories($cat['id'], $_arrChildCategories);
            }

        }
        return $_arrChildCategories;
    }

    /**
     * delete a category by id
     *
     * @param integer $id category id
     * @return bool success
     */
    function _deleteCategory($id = 0)
    {
        global $objDatabase;

        if (
            \Cx\Core\Setting\Controller\Setting::getValue(
                'CLX5087_BlockHistory',
                'Config'
            ) === 'on'
        ) {
            $em = \Cx\Core\Core\Controller\Cx::instanciate()->getDb()->getEntityManager();
            $categoryRepo = $em->getRepository('\Cx\Modules\Block\Model\Entity\Category');
            $blockRepo = $em->getRepository('\Cx\Modules\Block\Model\Entity\Block');

            if ($id < 1) {
                throw new NotEnoughArgumentsException('not enough arguments');
            }

            $category = $categoryRepo->findOneBy(array('id' => $id));
            if (!$category) {
                throw new NoCategoryFoundException('no category found');
            }

            $categoryParent = $categoryRepo->findOneBy(array('parent' => $id));
            if ($categoryParent) {
                $categoryParent->setParent(null);
            }

            $blocks = $blockRepo->findBy(array('category' => $category));
            foreach ($blocks as $block) {
                $block->setCategory(null);
            }
            $em->remove($category);

            return true;
        } else {
            $id = intval($id);
            if ($id < 1) {
                return false;
            }
            return $objDatabase->Execute('DELETE FROM `'.DBPREFIX.'module_block_categories` WHERE `id`='.$id)
                && $objDatabase->Execute('UPDATE `'.DBPREFIX.'module_block_categories` SET `parent` = 0 WHERE `parent`=' . $id)
                && $objDatabase->Execute('UPDATE `'.DBPREFIX.'module_block_blocks` SET `cat` = 0 WHERE `cat`=' . $id);
        }
    }

    /**
     * fill and/or return the categories array
     *
     * category arrays are put in the array as first dimension elements, with their parent as key, as follows:
     * $this->_categories[$objRS->fields['parent']][] = $objRS->fields;
     *
     * just to make this clear:
     * note that $somearray['somekey'][] = $foo adds $foo to $somearray['somekey'] rather than overwriting it.
     *
     * @param bool force refresh from DB
     * @see blockManager::_parseCategories for parse example
     * @see blockLibrary::_getCategoriesDropdown for parse example
     * @global ADONewConnection
     * @global array
     * @return array all available categories
     */
    function _getCategories($refresh = false)
    {
        global $objDatabase, $_ARRAYLANG;

        if (!empty($this->_categories) && !$refresh) {
            return $this->_categories;
        }

        $this->_categories = array(0 => array());
        $this->_categoryNames[0] = $_ARRAYLANG['TXT_BLOCK_NONE'];

        if (
            \Cx\Core\Setting\Controller\Setting::getValue(
                'CLX5087_BlockHistory',
                'Config'
            ) === 'on'
        ) {
            $em = \Cx\Core\Core\Controller\Cx::instanciate()->getDb()->getEntityManager();
            $categoryRepo = $em->getRepository('Cx\Modules\Block\Model\Entity\Category');
            $categories = $categoryRepo->findBy(array(), array('order' => 'ASC', 'id' => 'ASC'));

            foreach ($categories as $category) {
                $parentId = 0;
                $parent = $category->getParent();
                if ($parent) {
                    $parentId = $parent->getId();
                }
                $this->_categories[$parentId][] = array(
                    'id' => $category->getId(),
                    'parent' => $parentId,
                    'name' => $category->getName(),
                    'order' => $category->getOrder(),
                    'status' => $category->getStatus(),
                    'seperator' => $category->getSeperator(),
                );
                $this->_categoryNames[$category->getId()] = $category->getName();
            }
        } else {
            $objRS = $objDatabase->Execute('
               SELECT `id`,`parent`,`name`,`order`,`status`,`seperator`
               FROM `'.DBPREFIX.'module_block_categories`
               ORDER BY `order` ASC, `id` ASC
            ');
            if ($objRS !== false && $objRS->RecordCount() > 0) {
                while(!$objRS->EOF){
                    $this->_categories[$objRS->fields['parent']][] = $objRS->fields;
                    $this->_categoryNames[$objRS->fields['id']] = $objRS->fields['name'];
                    $objRS->MoveNext();
                }
            }
        }
        return $this->_categories;
    }

    /**
     * return the category specified by ID
     *
     * @param integer $id
     * @return array category information
     */
    function _getCategory($id = 0)
    {
        global $objDatabase;

        $id = intval($id);
        if ($id == 0) {
            return false;
        }

        $objRS = $objDatabase->Execute('
           SELECT `id`,`parent`,`name`,`seperator`,`order`,`status`
           FROM `'.DBPREFIX.'module_block_categories`
           WHERE `id`= '.$id
        );
        if (!$objRS) {
            return false;
        }
        return $objRS->fields;
    }

    /**
     * Replace the block placeholders by their associated content
     * within a supplied string
     *
     * @param   string  &$content    The content to replace all block placeholders by their content in
     */
    static function replacePlaceholdersInContent(&$content) {
        // functionality is only available in frontend -> abort if we're not in frontend mode
        if (\Cx\Core\Core\Controller\Cx::instanciate()->getMode() != \Cx\Core\Core\Controller\Cx::MODE_FRONTEND) {
            return;
        }

        // only proceed if the block system is active
        $config = \Env::get('config');
        if (!$config['blockStatus']) {
            return;
        }

        // the block assignements are based on content pages, therefore it's
        // necessarily that a content page has been requested and loaded to proceed
        if (!\Cx\Core\Core\Controller\Cx::instanciate()->getPage()) {
            return;
        }

        // finally, replace the placeholders by their associated blocks
        \Cx\Modules\Block\Controller\Block::setBlocks($content, \Cx\Core\Core\Controller\Cx::instanciate()->getPage());
    }
}
