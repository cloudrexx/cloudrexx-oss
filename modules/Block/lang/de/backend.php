<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * @copyright   CLOUDREXX CMS - CLOUDREXX AG
 * @author      Cloudrexx Development Team <info@cloudrexx.com>
 * @access      public
 * @package     cloudrexx
 * @subpackage  module_block
 */
$_ARRAYLANG['TXT_BLOCK_MODIFY_BLOCK'] = "Inhaltscontainer %s bearbeiten";
$_ARRAYLANG['TXT_BLOCK_ADD_BLOCK'] = "Inhaltscontainer hinzufügen";
$_ARRAYLANG['TXT_BLOCK_BLOCKS'] = "Inhaltscontainer";
$_ARRAYLANG['TXT_BLOCK_NAME'] = "Name";
$_ARRAYLANG['TXT_BLOCK_PLACEHOLDER'] = "Platzhalter";
$_ARRAYLANG['TXT_BLOCK_FUNCTIONS'] = "Funktionen";
$_ARRAYLANG['TXT_BLOCK_CONTENT'] = "Inhalt";
$_ARRAYLANG['TXT_BLOCK_SAVE'] = "Speichern";
$_ARRAYLANG['TXT_BLOCK_BLOCK_UPDATED_SUCCESSFULLY'] = "Der Inhaltscontainer %s wurde erfolgreich aktualisiert.";
$_ARRAYLANG['TXT_BLOCK_BLOCK_COULD_NOT_BE_UPDATED'] = "Der Inhaltscontainer konnte nicht aktualisiert werden!";
$_ARRAYLANG['TXT_BLOCK_SETTINGS'] = "Einstellungen";
$_ARRAYLANG['TXT_BLOCK_USE_BLOCK_SYSTEM'] = "Inhaltscontainer verwenden";
$_ARRAYLANG['TXT_BLOCK_OVERVIEW'] = "Übersicht";
$_ARRAYLANG['TXT_BLOCK_BASIC_DATA'] = "Grunddaten";
$_ARRAYLANG['TXT_BLOCK_ADDITIONAL_OPTIONS'] = "Erweiterte Optionen";
$_ARRAYLANG['TXT_BLOCK_SUBMIT_SELECT'] = "Aktion auswählen";
$_ARRAYLANG['TXT_BLOCK_SUBMIT_DELETE'] = "Markierte löschen";
$_ARRAYLANG['TXT_BLOCK_SUBMIT_ACTIVATE'] = "Markierte aktivieren";
$_ARRAYLANG['TXT_BLOCK_SUBMIT_DEACTIVATE'] = "Markierte deaktivieren";
$_ARRAYLANG['TXT_BLOCK_SELECT_ALL'] = "Alle auswählen";
$_ARRAYLANG['TXT_BLOCK_DESELECT_ALL'] = "Auswahl entfernen";
$_ARRAYLANG['TXT_BLOCK_UNSELECT_ALL'] = "Auswahl entfernen";
$_ARRAYLANG['TXT_BLOCK_RANDOM'] = "Anzeige in den Zufallsplatzhaltern";
$_ARRAYLANG['TXT_BLOCK_DELETE_SELECTED_BLOCKS'] = "Möchten Sie die ausgewählten Blöcke wirklich löschen?";
$_ARRAYLANG['TXT_BLOCK_NO_NAME'] = "[kein Name]";
$_ARRAYLANG['TXT_BLOCK_USE_BLOCK_RANDOM'] = "Zufallsgenerator verwenden";
$_ARRAYLANG['TXT_BLOCK_USE_BLOCK_RANDOM_PLACEHOLDER'] = "Platzhalter für Zufallsgenerator <code>[[BLOCK_RANDOMIZER]]</code>";
$_ARRAYLANG['TXT_BLOCK_ACTIVE'] = "Aktiv";
$_ARRAYLANG['TXT_BLOCK_INACTIVE'] = "Inaktiv";
$_ARRAYLANG['TXT_BLOCK_FRONTEND_LANGUAGES'] = "Seitenauswahl für den globalen Containerplatzhalter";
$_ARRAYLANG['TXT_BLOCK_BLOCK_ADDED_SUCCESSFULLY'] = "Der Inhaltscontainer %s wurde erfolgreich hinzugefügt.";
$_ARRAYLANG['TXT_BLOCK_BLOCK_COULD_NOT_BE_ADDED'] = "Beim Hinzufügen des Inhaltscontainers trat ein Fehler auf!";
$_ARRAYLANG['TXT_BLOCK_COULD_NOT_DELETE_BLOCK'] = "Beim Löschen des Inhaltscontainers %s trat ein Fehler auf!";
$_ARRAYLANG['TXT_BLOCK_FAILED_TO_DELETE_BLOCKS'] = "Beim Löschen der folgenden Inhaltscontainer trat ein Fehler auf: %s";
$_ARRAYLANG['TXT_BLOCK_SUCCESSFULLY_DELETED'] = "Der Inhaltscontainer %s wurde erfolgreich gelöscht.";
$_ARRAYLANG['TXT_BLOCK_BLOCKS_SUCCESSFULLY_DELETED'] = "Die Inhaltscontainer wurden erfolgreich gelöscht.";
$_ARRAYLANG['TXT_BLOCK_OPERATION_IRREVERSIBLE'] = "Dieser Vorgang kann nicht rückgängig gemacht werden!";
$_ARRAYLANG['TXT_BLOCK_CONFIRM_DELETE_BLOCK'] = "Möchten Sie den Inhaltscontainer %s wirklich löschen?";
$_ARRAYLANG['TXT_BLOCK_COPY_BLOCK'] = "Inhaltscontainer %s kopieren";
$_ARRAYLANG['TXT_BLOCK_DELETE_BLOCK'] = "Inhaltscontainer %s löschen";
$_ARRAYLANG['TXT_BLOCK_BLOCK_RANDOM'] = "Zufallsgenerator";
$_ARRAYLANG['TXT_BLOCK_BLOCK_GLOBAL'] = "Globaler Inhaltscontainer";
$_ARRAYLANG['TXT_BLOCK_GLOBAL_SEPERATOR'] = "Trennzeichen im globalen Inhaltscontainer";
$_ARRAYLANG['TXT_BLOCK_GLOBAL_SEPERATOR_INFO'] = "Trennzeichen zwischen den Blöcken im globalen Containerplatzhalter";
$_ARRAYLANG['TXT_BLOCK_SUBMIT_GLOBAL'] = "Markierte zum globalen Inhaltscontainer hinzufügen";
$_ARRAYLANG['TXT_BLOCK_SUBMIT_GLOBAL_OFF'] = "Markierte vom globalen Inhaltscontainer entfernen";
$_ARRAYLANG['TXT_BLOCK_SHOW_IN_GLOBAL'] = "im globalen Inhaltscontainer anzeigen";
$_ARRAYLANG['TXT_BLOCK_STATUS'] = "Status";
$_ARRAYLANG['TXT_BLOCK_BLOCK'] = "Platzhalter";
$_ARRAYLANG['TXT_BLOCK_ACTIVATE'] = "aktivieren";
$_ARRAYLANG['TXT_BLOCK_SHOW_BLOCK_ON_ALL_'] = "Inhaltscontainer auf jeder Seite dieser Sprache anzeigen";
$_ARRAYLANG['TXT_BLOCK_CONTENT_PAGES'] = "Frontend-Seiten";
$_ARRAYLANG['TXT_BLOCK_SHOW_BLOCK_IN_THIS_LANGUAGE'] = "Inhaltscontainer in dieser Sprache anzeigen";
$_ARRAYLANG['TXT_BLOCK_DEACTIVATE'] = "deaktivieren";
$_ARRAYLANG['TXT_DONT_SHOW_ON_PAGES'] = "Auf keiner Seite";
$_ARRAYLANG['TXT_SHOW_ON_ALL_PAGES'] = "Auf jeder Seite";
$_ARRAYLANG['TXT_SHOW_ON_SELECTED_PAGES'] = "Seiten auswählen";
$_ARRAYLANG['TXT_BLOCK_SHOW_ON_ALL_PAGES'] = "Inhaltscontainer auf jeder Seite dieser Sprache anzeigen";
$_ARRAYLANG['TXT_BLOCK_FRONTEND_PAGES'] = "Frontend-Seiten";
$_ARRAYLANG['TXT_BLOCK_LANG_SHOW'] = "Inhaltscontainer in dieser Sprache anzeigen";
$_ARRAYLANG['TXT_BLOCK_BLOCK_NO'] = "Inhaltscontainer #%s";
$_ARRAYLANG['TXT_BLOCK_BLOCK_ALL'] = "alle";
$_ARRAYLANG['TXT_BLOCK_BLOCK_NONE'] = "-";
$_ARRAYLANG['TXT_BLOCK_CATEGORIES'] = "Kategorien";
$_ARRAYLANG['TXT_BLOCK_CATEGORIES_MANAGE'] = "Kategorie verwalten";
$_ARRAYLANG['TXT_BLOCK_CATEGORIES_ADD'] = "Kategorie hinzufügen";
$_ARRAYLANG['TXT_BLOCK_CATEGORIES_ADD_ERROR'] = "Beim Aktualisieren/Hinzufügen der Kategorien ist ein Fehler aufgetreten.";
$_ARRAYLANG['TXT_BLOCK_CATEGORIES_ADD_OK'] = "Die Kategorie wurde aktualisiert/hinzugefügt.";
$_ARRAYLANG['TXT_BLOCK_PARENT'] = "Oberkategorie";
$_ARRAYLANG['TXT_BLOCK_NONE'] = "Keine";
$_ARRAYLANG['TXT_BLOCK_CATEGORIES_ALL'] = "Alle Kategorien";
$_ARRAYLANG['TXT_BLOCK_NO_CATEGORIES_FOUND'] = "Keine Einträge vorhanden";
$_ARRAYLANG['TXT_BLOCK_CATEGORIES_DELETE_ERROR'] = "Beim Löschen einer Kategorie trat ein Fehler auf.";
$_ARRAYLANG['TXT_BLOCK_CATEGORIES_DELETE_OK'] = "Kategorie(n) erfolgreich gelöscht.";
$_ARRAYLANG['TXT_BLOCK_CATEGORIES_EDIT'] = "Kategorie bearbeiten";
$_ARRAYLANG['TXT_BLOCK_CATEGORIES_DELETE_INFO_HEAD'] = "Informationen zum Löschen von Kategorien";
$_ARRAYLANG['TXT_BLOCK_CATEGORIES_DELETE_INFO'] = "Es können nur Kategorien gelöscht werden, welche keine Unterkategorien enthalten.<br>Falls eine Kategorie gelöscht wird, welche noch bei Blöcken zugewiesen ist, wird diese Zuweisung vorher entfernt.";
$_ARRAYLANG['TXT_BLOCK_CATEGORY'] = "Kategorie";
$_ARRAYLANG['TXT_BLOCK_SHOW_FROM'] = "Von";
$_ARRAYLANG['TXT_BLOCK_SHOW_UNTIL'] = "Bis";
$_ARRAYLANG['TXT_BLOCK_AVAILABLE_PAGES'] = "Verfügbare Inhaltsseiten";
$_ARRAYLANG['TXT_BLOCK_SELECTED_PAGES'] = "Ausgewählte Inhaltsseiten";
$_ARRAYLANG['TXT_BLOCK_SHOW_ALWAYS'] = "Immer anzeigen";
$_ARRAYLANG['TXT_BLOCK_SHOW_TIMED'] = "Zeitgesteuertes Anzeigen";
$_ARRAYLANG['TXT_BLOCK_LANGUAGE'] = "Sprache";
$_ARRAYLANG['TXT_BLOCK_GLOBAL_PLACEHOLDERS'] = "Inhaltscontainer im globalen Platzhalter anzeigen";
$_ARRAYLANG['TXT_BLOCK_GLOBAL_PLACEHOLDERS_INFO'] = "Hier können Sie die Seiten auswählen, auf welchen dieser Inhaltscontainer im globalen Platzhalter <code>[[BLOCK_GLOBAL]]</code> angezeigt werden soll.";
$_ARRAYLANG['TXT_BLOCK_DIRECT_PLACEHOLDERS'] = "Inhaltscontainer im direkten Platzhalter anzeigen";
$_ARRAYLANG['TXT_BLOCK_DIRECT_PLACEHOLDERS_INFO'] = "Hier können Sie die Seiten auswählen, auf welchen der direkte Platzhalter <code>[[BLOCK_XY]]</code> angezeigt werden soll.";
$_ARRAYLANG['TXT_BLOCK_CATEGORY_PLACEHOLDERS'] = "Inhaltscontainer im Kategorien-Platzhalter anzeigen";
$_ARRAYLANG['TXT_BLOCK_CATEGORY_PLACEHOLDERS_INFO'] = "Hier können Sie die Seiten auswählen, auf welchen der Inhaltscontainer im Kategorie-Platzhalter <code>[[BLOCK_CAT_XY]]</code> angezeigt werden soll.";
$_ARRAYLANG['TXT_BLOCK_CATEGORY_SEPERATOR'] = "Trennzeichen zwischen den Inhaltscontainern für Kategorie-Platzhalter";
$_ARRAYLANG['TXT_BLOCK_DISPLAY_TIME'] = "Anzeigedauer";
$_ARRAYLANG['TXT_BLOCK_ORDER']   = "Reihenfolge";
$_ARRAYLANG['TXT_BLOCK_DISPLAY_SELECTED_PAGE'] = "Dieser Inhaltscontainer wird auf den folgenden Seiten im globalen Platzhalter <code>[[BLOCK_GLOBAL]]</code> angezeigt:";
$_ARRAYLANG['TXT_BLOCK_DISPLAY_ALL_PAGE'] = "Dieser Inhaltscontainer wird auf jeder Seite im globalen Platzhalter <code>[[BLOCK_GLOBAL]]</code> angezeigt";
$_ARRAYLANG['TXT_BLOCK_USE_WYSIWYG_EDITOR'] = "WYSIWYG-Editor verwenden";
$_ARRAYLANG['TXT_BLOCK_RANDOM_INFO_INCLUDED'] = "Dieser Inhaltscontainer wird im Zufallsplatzhalter <code>%s</code> angezeigt";
$_ARRAYLANG['TXT_BLOCK_RANDOM_INFO_EXCLUDED'] = "Dieser Inhaltscontainer wird nicht im Zufallsplatzhalter <code>%s</code> angezeigt";
$_ARRAYLANG['TXT_BLOCK_SEPERATOR'] = "Trennzeichen";
$_ARRAYLANG['TXT_BLOCK_TARGETING'] = 'Targeting';
$_ARRAYLANG['TXT_BLOCK_TARGETING_SHOW_PANE'] = 'Wer soll diesen Inhaltscontainer sehen?';
$_ARRAYLANG['TXT_BLOCK_TARGETING_ALL_USERS'] = 'Alle Besucher';
$_ARRAYLANG['TXT_BLOCK_TARGETING_VISITOR_CONDITION_BELOW'] = 'Besucher welche die folgenden Kriterien erfüllen';
$_ARRAYLANG['TXT_BLOCK_TARGETING_INCLUDE'] = 'Einschliesslich';
$_ARRAYLANG['TXT_BLOCK_TARGETING_EXCLUDE'] = 'Ausschliesslich';
$_ARRAYLANG['TXT_BLOCK_TARGETING_TYPE_LOCATION'] = 'Land eingeben';
$_ARRAYLANG['TXT_BLOCK_TARGETING_GEOIP_DISABLED_WARNING'] = 'Die standortbezogenen Dienste sind aktuell deaktiviert. Sofern diese nicht aktiviert werden, führt die Verwendung dieser Funktion dazu, dass dieser Inhaltscontainer auf der Website nicht mehr angezeigt wird. <a target="_blank" title="Standortbezogene Dienste jetzt aktivieren" href="index.php?cmd=GeoIp">Standortbezogene Dienste jetzt aktivieren</a>';
$_ARRAYLANG['TXT_BLOCK_INCLUSION'] = 'Einbindung';
$_ARRAYLANG['TXT_BLOCK_LOCATION_BASED_DISPLAY_INFO'] = 'Dieser Inhaltscontainer wird allen Besuchern angezeigt';
$_ARRAYLANG['TXT_BLOCK_TARGETING_INFO_INCLUDE'] = 'Dieser Inhaltscontainer wird nur Besuchern der folgenden Länder angezeigt:';
$_ARRAYLANG['TXT_BLOCK_TARGETING_INFO_EXCLUDE'] = 'Dieser Inhaltscontainer wird allen Besuchern ausser jene der folgenden Ländern angezeigt:';
$_ARRAYLANG['TXT_BLOCK_DISPLAY_GLOBAL_INACTIVE'] = 'Dieser Inhaltscontainer wird im globalen Platzhalter <code>[[BLOCK_GLOBAL]]</code> nicht angezeigt';
$_ARRAYLANG['TXT_BLOCK_DIRECT_INFO_SHOW_ALL_PAGES'] = 'Dieser Inhaltscontainer wird auf jeder Seite im eigenen Platzhalter <code>%s</code> angezeigt';
$_ARRAYLANG['TXT_BLOCK_DIRECT_INFO_SHOW_SELECTED_PAGES'] = 'Dieser Inhaltscontainer wird auf den folgenden Seiten im eigenen Platzhalter <code>%s</code> angezeigt:';
$_ARRAYLANG['TXT_BLOCK_MARK_PARSED_BLOCK'] = 'Inhaltscontainer mit HTML-Kommentar umschließen';
$_ARRAYLANG['TXT_BLOCK_MARK_PARSED_BLOCK_INFO'] = 'Aktivieren Sie diese Option, um die Inhaltscontainer im Frontend mit einem HTML-Kommentar zu versehen. Ist diese Option aktiv, werden alle Inhaltscontainer mit einem HTML-Kommentar wie folgt umschlossen: <code>&lt;!-- start BLOCK_1 --&gt;</code>Inhalt des Containers<code>&lt;!-- end BLOCK_1 --&gt;</code>';
$_ARRAYLANG['TXT_BLOCK_HISTORY'] = 'Verlauf';
$_ARRAYLANG['TXT_BLOCK_HISTORY_DATE'] = 'Datum';
$_ARRAYLANG['TXT_BLOCK_HISTORY_USER'] = 'Benutzer';
$_ARRAYLANG['TXT_BLOCK_HISTORY_FUNCTION'] = 'Funktion';
$_ARRAYLANG['TXT_BLOCK_HISTORY_VERSION_LOAD'] = 'Laden';
$_ARRAYLANG['TXT_BLOCK_HISTORY_NO_ENTRIES'] = 'Es existieren keine Einträge.';
