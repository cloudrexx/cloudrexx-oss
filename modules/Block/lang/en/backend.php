<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * @copyright   CLOUDREXX CMS - CLOUDREXX AG
 * @author      Cloudrexx Development Team <info@cloudrexx.com>
 * @access      public
 * @package     cloudrexx
 * @subpackage  module_block
 */
$_ARRAYLANG['TXT_BLOCK_MODIFY_BLOCK'] = "Modify content pane %s";
$_ARRAYLANG['TXT_BLOCK_ADD_BLOCK'] = "Add";
$_ARRAYLANG['TXT_BLOCK_BLOCKS'] = "Content panes";
$_ARRAYLANG['TXT_BLOCK_NAME'] = "Name";
$_ARRAYLANG['TXT_BLOCK_PLACEHOLDER'] = "Placeholder";
$_ARRAYLANG['TXT_BLOCK_FUNCTIONS'] = "Functions";
$_ARRAYLANG['TXT_BLOCK_CONTENT'] = "Content";
$_ARRAYLANG['TXT_BLOCK_SAVE'] = "Store";
$_ARRAYLANG['TXT_BLOCK_BLOCK_UPDATED_SUCCESSFULLY'] = "The content pane has successfully been updated.";
$_ARRAYLANG['TXT_BLOCK_BLOCK_COULD_NOT_BE_UPDATED'] = "The content pane couldn't be updated!";
$_ARRAYLANG['TXT_BLOCK_SETTINGS'] = "Settings";
$_ARRAYLANG['TXT_BLOCK_USE_BLOCK_SYSTEM'] = "Use content panes";
$_ARRAYLANG['TXT_BLOCK_OVERVIEW'] = "Overview";
$_ARRAYLANG['TXT_BLOCK_BASIC_DATA'] = "Basic Data";
$_ARRAYLANG['TXT_BLOCK_ADDITIONAL_OPTIONS'] = "Additional Options";
$_ARRAYLANG['TXT_BLOCK_SUBMIT_SELECT'] = "Select action";
$_ARRAYLANG['TXT_BLOCK_SUBMIT_DELETE'] = "Delete selected";
$_ARRAYLANG['TXT_BLOCK_SUBMIT_ACTIVATE'] = "Activate selected";
$_ARRAYLANG['TXT_BLOCK_SUBMIT_DEACTIVATE'] = "Deactivate selected";
$_ARRAYLANG['TXT_BLOCK_SELECT_ALL'] = "Select all";
$_ARRAYLANG['TXT_BLOCK_DESELECT_ALL'] = "Delete selected";
$_ARRAYLANG['TXT_BLOCK_RANDOM'] = "Included In random generator";
$_ARRAYLANG['TXT_BLOCK_DELETE_SELECTED_BLOCKS'] = "Do you really want to delete the selected content panes?";
$_ARRAYLANG['TXT_BLOCK_NO_NAME'] = "[No name]";
$_ARRAYLANG['TXT_BLOCK_USE_BLOCK_RANDOM'] = "Use random generator";
$_ARRAYLANG['TXT_BLOCK_USE_BLOCK_RANDOM_PLACEHOLDER'] = "[[BLOCK_RANDOMIZER]] Placeholder for the random generator";
$_ARRAYLANG['TXT_BLOCK_ACTIVE'] = "Active";
$_ARRAYLANG['TXT_BLOCK_INACTIVE'] = "Inactive";
$_ARRAYLANG['TXT_BLOCK_FRONTEND_LANGUAGES'] = "Frontend languages";
$_ARRAYLANG['TXT_BLOCK_BLOCK_ADDED_SUCCESSFULLY'] = "The content pane %s has been added successfully.";
$_ARRAYLANG['TXT_BLOCK_BLOCK_COULD_NOT_BE_ADDED'] = "An error occurred while adding the content pane!";
$_ARRAYLANG['TXT_BLOCK_COULD_NOT_DELETE_BLOCK'] = "An error occurred while deleting the content pane %s!";
$_ARRAYLANG['TXT_BLOCK_FAILED_TO_DELETE_BLOCKS'] = "An error occurred while deleting the following content panes: %s";
$_ARRAYLANG['TXT_BLOCK_SUCCESSFULLY_DELETED'] = "The content pane %s has been deleted successfully.";
$_ARRAYLANG['TXT_BLOCK_BLOCKS_SUCCESSFULLY_DELETED'] = "The content panes have been deleted successfully.";
$_ARRAYLANG['TXT_BLOCK_OPERATION_IRREVERSIBLE'] = "This operation is irreversible!";
$_ARRAYLANG['TXT_BLOCK_CONFIRM_DELETE_BLOCK'] = "Do you really want to delete the content pane %s?";
$_ARRAYLANG['TXT_BLOCK_COPY_BLOCK'] = "Copy content pane %s";
$_ARRAYLANG['TXT_BLOCK_DELETE_BLOCK'] = "Delete content pane %s";
$_ARRAYLANG['TXT_BLOCK_BLOCK_RANDOM'] = "Placeholder for random generator";
$_ARRAYLANG['TXT_BLOCK_BLOCK_GLOBAL'] = "Placeholder for all content panes. Within this placeholder, all page and language assignments will be considered.";
$_ARRAYLANG['TXT_BLOCK_GLOBAL_SEPERATOR'] = "Delimiter within global content pane";
$_ARRAYLANG['TXT_BLOCK_GLOBAL_SEPERATOR_INFO'] = "Delimiter between individual content panes within the global pane. You can use HTML.";
$_ARRAYLANG['TXT_BLOCK_SUBMIT_GLOBAL'] = "Add selected to the global content pane";
$_ARRAYLANG['TXT_BLOCK_SUBMIT_GLOBAL_OFF'] = "Delete selected from global content pane";
$_ARRAYLANG['TXT_BLOCK_SHOW_IN_GLOBAL'] = "Display in global pane";
$_ARRAYLANG['TXT_BLOCK_STATUS'] = "Status";
$_ARRAYLANG['TXT_BLOCK_BLOCK'] = "Content pane";
$_ARRAYLANG['TXT_BLOCK_ACTIVATE'] = "Activate";
$_ARRAYLANG['TXT_BLOCK_SHOW_BLOCK_ON_ALL_'] = "Show content pane on all pages in this language";
$_ARRAYLANG['TXT_BLOCK_CONTENT_PAGES'] = "Frontend pages";
$_ARRAYLANG['TXT_BLOCK_SHOW_BLOCK_IN_THIS_LANGUAGE'] = "Display content pane in this language";
$_ARRAYLANG['TXT_BLOCK_DEACTIVATE'] = "deactivate";
$_ARRAYLANG['TXT_DONT_SHOW_ON_PAGES'] = "On none pages";
$_ARRAYLANG['TXT_SHOW_ON_ALL_PAGES'] = "On all pages";
$_ARRAYLANG['TXT_SHOW_ON_SELECTED_PAGES'] = "Select pages";
$_ARRAYLANG['TXT_BLOCK_SHOW_ON_ALL_PAGES'] = "Show content pane on all pages in this language";
$_ARRAYLANG['TXT_BLOCK_FRONTEND_PAGES'] = "Frontend pages";
$_ARRAYLANG['TXT_BLOCK_LANG_SHOW'] = "Display content pane in this language";
$_ARRAYLANG['TXT_BLOCK_BLOCK_NO'] = "Content pane #%s";
$_ARRAYLANG['TXT_BLOCK_BLOCK_ALL'] = "all";
$_ARRAYLANG['TXT_BLOCK_BLOCK_NONE'] = "-";
$_ARRAYLANG['TXT_BLOCK_CATEGORIES'] = "Categories";
$_ARRAYLANG['TXT_BLOCK_CATEGORIES_MANAGE'] = "Manage categories";
$_ARRAYLANG['TXT_BLOCK_CATEGORIES_ADD'] = "Add";
$_ARRAYLANG['TXT_BLOCK_CATEGORIES_ADD_ERROR'] = "An error occurred while updating/adding the categories.";
$_ARRAYLANG['TXT_BLOCK_CATEGORIES_ADD_OK'] = "The section has been updated/added.";
$_ARRAYLANG['TXT_BLOCK_PARENT'] = "Parent category";
$_ARRAYLANG['TXT_BLOCK_NONE'] = "None";
$_ARRAYLANG['TXT_BLOCK_CATEGORIES_ALL'] = "All categories";
$_ARRAYLANG['TXT_BLOCK_NO_CATEGORIES_FOUND'] = "No categories found";
$_ARRAYLANG['TXT_BLOCK_CATEGORIES_DELETE_ERROR'] = "An error occurred when deleting a category.";
$_ARRAYLANG['TXT_BLOCK_CATEGORIES_DELETE_OK'] = "Category(s) successfully deleted.";
$_ARRAYLANG['TXT_BLOCK_CATEGORIES_EDIT'] = "Modify category";
$_ARRAYLANG['TXT_BLOCK_CATEGORIES_DELETE_INFO_HEAD'] = "About deleting categories";
$_ARRAYLANG['TXT_BLOCK_CATEGORIES_DELETE_INFO'] = "Only categories that do not contain subcategories can be deleted.<br />If a category is deleted that is still assigned to blocks, this assignment will be removed beforehand.";
$_ARRAYLANG['TXT_BLOCK_CATEGORY'] = "Category";
$_ARRAYLANG['TXT_BLOCK_SHOW_FROM'] = "From";
$_ARRAYLANG['TXT_BLOCK_SHOW_UNTIL'] = "Till";
$_ARRAYLANG['TXT_BLOCK_AVAILABLE_PAGES'] = "Available pages";
$_ARRAYLANG['TXT_BLOCK_SELECTED_PAGES'] = "Selected pages";
$_ARRAYLANG['TXT_BLOCK_SELECT_ALL'] = "Select All";
$_ARRAYLANG['TXT_BLOCK_UNSELECT_ALL'] = "Unselect all";
$_ARRAYLANG['TXT_BLOCK_SHOW_ALWAYS'] = "Always";
$_ARRAYLANG['TXT_BLOCK_SHOW_TIMED'] = "Scheduled publication";
$_ARRAYLANG['TXT_BLOCK_LANGUAGE'] = "Language";
$_ARRAYLANG['TXT_BLOCK_GLOBAL_PLACEHOLDERS'] = "Include content pane in global placeholder";
$_ARRAYLANG['TXT_BLOCK_GLOBAL_PLACEHOLDERS_INFO'] = "Select the pages on which this content pane shall be included in the global placeholder [[BLOCK_GLOBAL]].";
$_ARRAYLANG['TXT_BLOCK_DIRECT_PLACEHOLDERS'] = "Activate content pane's own placeholder";
$_ARRAYLANG['TXT_BLOCK_DIRECT_PLACEHOLDERS_INFO'] = "Select the pages on which this content pane's own placeholder [[BLOCK_XY]] shall be displayed.";
$_ARRAYLANG['TXT_BLOCK_CATEGORY_PLACEHOLDERS'] = "Include content pane in category placeholder";
$_ARRAYLANG['TXT_BLOCK_CATEGORY_PLACEHOLDERS_INFO'] = "Select the pages on which this content pane shall be included in its associated category's placeholder [[BLOCK_CAT_XY]].";
$_ARRAYLANG['TXT_BLOCK_CATEGORY_SEPERATOR'] = "Separator for category placeholder";
$_ARRAYLANG['TXT_BLOCK_DISPLAY_TIME'] = "Publication";
$_ARRAYLANG['TXT_BLOCK_CONTENT'] = "Content";
$_ARRAYLANG['TXT_BLOCK_ORDER']   = "Order";
$_ARRAYLANG['TXT_BLOCK_DISPLAY_SELECTED_PAGE'] = "This content pane gets displayed on the following pages in the global placeholder [[BLOCK_GLOBAL]]:";
$_ARRAYLANG['TXT_BLOCK_DISPLAY_ALL_PAGE'] = "This content pane gets displayed on all pages in the global placeholder [[BLOCK_GLOBAL]]";
$_ARRAYLANG['TXT_BLOCK_USE_WYSIWYG_EDITOR'] = "Use WYSIWYG editor";
$_ARRAYLANG['TXT_BLOCK_RANDOM_INFO_INCLUDED'] = "This content pane gets displayed in the random generator %s";
$_ARRAYLANG['TXT_BLOCK_RANDOM_INFO_EXCLUDED'] = "This content pane is not part of the random generator %s";
$_ARRAYLANG['TXT_BLOCK_SEPERATOR'] = "Separator between blocks";
$_ARRAYLANG['TXT_BLOCK_TARGETING'] = 'Targeting';
$_ARRAYLANG['TXT_BLOCK_TARGETING_SHOW_PANE'] = 'Who should see this content pane?';
$_ARRAYLANG['TXT_BLOCK_TARGETING_ALL_USERS'] = 'All visitors';
$_ARRAYLANG['TXT_BLOCK_TARGETING_VISITOR_CONDITION_BELOW'] = 'Visitor who meet the conditions below';
$_ARRAYLANG['TXT_BLOCK_TARGETING_INCLUDE'] = 'Include';
$_ARRAYLANG['TXT_BLOCK_TARGETING_EXCLUDE'] = 'Exclude';
$_ARRAYLANG['TXT_BLOCK_TARGETING_TYPE_LOCATION'] = 'Type country';
$_ARRAYLANG['TXT_BLOCK_TARGETING_GEOIP_DISABLED_WARNING'] = 'Warning: The location-based services are currently deactivated. Unless activated, this option will cause the content pane to be no longer visible on the website. <a target="_blank" title="Activate location-based services" href="index.php?cmd=GeoIp">Activate location-based services now</a>';
$_ARRAYLANG['TXT_BLOCK_INCLUSION'] = 'Inclusion';
$_ARRAYLANG['TXT_BLOCK_LOCATION_BASED_DISPLAY_INFO'] = 'This content pane will be shown to all visitors';
$_ARRAYLANG['TXT_BLOCK_TARGETING_INFO_INCLUDE'] = 'This content pane will be shown to visitors from the following countries <strong>only</strong>:';
$_ARRAYLANG['TXT_BLOCK_TARGETING_INFO_EXCLUDE'] = 'This content pane will be shown to all visitors <strong>except</strong> those from the following countries:';
$_ARRAYLANG['TXT_BLOCK_DISPLAY_GLOBAL_INACTIVE'] = 'This content pane is not included in the global placeholder [[BLOCK_GLOBAL]]';
$_ARRAYLANG['TXT_BLOCK_DIRECT_INFO_SHOW_ALL_PAGES'] = 'This content pane gets displayed on all pages in its own placeholder %s';
$_ARRAYLANG['TXT_BLOCK_DIRECT_INFO_SHOW_SELECTED_PAGES'] = 'This content pane gets displayed on the following pages in its own placeholder %s:';
$_ARRAYLANG['TXT_BLOCK_MARK_PARSED_BLOCK'] = 'Enclose content panes by HTML comment';
$_ARRAYLANG['TXT_BLOCK_MARK_PARSED_BLOCK_INFO'] = 'By activating this option, all content panes will be enclosed by a HTML comment in the website output (frontend) like the following: <br />
&lt;!-- start BLOCK_1 --&gt;Content of pane&lt;!-- end BLOCK_1 --&gt;';
$_ARRAYLANG['TXT_BLOCK_HISTORY'] = 'History';
$_ARRAYLANG['TXT_BLOCK_HISTORY_DATE'] = 'Date';
$_ARRAYLANG['TXT_BLOCK_HISTORY_USER'] = 'User';
$_ARRAYLANG['TXT_BLOCK_HISTORY_FUNCTION'] = 'Function';
$_ARRAYLANG['TXT_BLOCK_HISTORY_VERSION_LOAD'] = 'Load';
$_ARRAYLANG['TXT_BLOCK_HISTORY_NO_ENTRIES'] = 'There are no entries.';
