$J(document).ready(function () {
    // disables actions of latest version (always first in table)
    var availableVersion = cx.jQuery('.tab.data_history tr.history-version:first').data('version');
    disableActions(availableVersion);

    cx.jQuery( 'input[name=targeting_status]' ).change(function(){
        toggleTargetingFilterBlock( cx.jQuery( this ).val() == '1' );
    });
    toggleTargetingFilterBlock( cx.jQuery('input[name=targeting_status]:checked').val() == '1' );
});

/**
 * Disables the actions on the provided version and enables all others
 *
 * @param version integer version to disable
 * @return void
 */
function disableActions(version) {
    cx.jQuery('.data_history tr.history-version .actions a').css('display', 'inline-block');
    if (version) {
        cx.jQuery('.data_history tr.history-version[data-version="' + version + '"] .actions a').css('display', 'none');
    }
}

function toggleTargetingFilterBlock( show ) {
    if ( show ) {
        cx.jQuery( '#targeting_filter_block' ).show();
    } else {
        cx.jQuery( '#targeting_filter_block' ).hide();
    }
}

/**
 * Gets provided history version
 *
 * @param id integer wanted block id
 * @param version integer wanted block version
 */
function loadHistoryVersion(id, version) {
    var lang = cx.variables.get('language');
    cx.ready(function () {
        cx.ajax(
            'Block',
            'getBlock',
            {
                data: {
                    id: id,
                    version: version,
                    lang: lang
                },
                success: function (data) {
                    // applies data to form
                    applyHistoryVersion(data.data);
                    // switches to first tab
                    cx.trigger("selectTab", "cx.ui.tabs", {
                        "tablist": "default",
                        "tabname": ""
                    });
                    // disables actions of current version
                    disableActions(version)
                }
            }
        );
    });
}

/**
 * Applies given data to the block modify form
 *
 * @param data object returned data from AJAX call
 */
const selectorMappings = {
    "start": "input[name=inputStartDate]",
    "end": "input[name=inputEndDate]",
    "name": "input[name=blockName]",
    "random": "input[name=blockRandom]",
    "random2": "input[name=blockRandom2]",
    "random3": "input[name=blockRandom3]",
    "random4": "input[name=blockRandom4]",
    "wysiwygEditor": "input[name=wysiwyg_editor]",
};
function applyHistoryVersion(data) {
    // sets data on elements
    for (const key in data) {
        // skip loop if the property is from prototype or property not wanted for this action
        if (!data.hasOwnProperty(key)) {
            continue;
        }
        switch (key) {
            case "id":
            case "active":
            case "order":
                continue;
            case "start":
            case "end":
            case "name":
                _setBySelector(selectorMappings[key], data[key]);
                break;
            case "random":
            case "random2":
            case "random3":
            case "random4":
                // Note: setAttribute("checked") does not work as expected for checkboxes
                document.querySelector(selectorMappings[key]).checked = data[key] == 1;
                document.querySelector(selectorMappings[key]).dispatchEvent(new Event("change"));
                break;
            case "wysiwygEditor":
                // Note: setAttribute("checked") does not work as expected for checkboxes
                document.querySelector(selectorMappings[key]).checked = data[key] === 1;
                // Note: we must not trigger a change here as this would trigger a WYSIWYG update
                //          and CKeditor disallows fast paced updates. See below (after the for loop).
                break;
            case "showInGlobal":
                data[key] = data[key] - 1;
                //break; Intentionally no break
            case "showInCategory":
            case "showInDirect":
                for (const el of document.querySelectorAll("form[name=blockFormModifyBlock] [name=block" + key.substr(6) + "]")) {
                    el.removeAttribute("checked");
                }
                data[key] = data[key] + 1;
                _setBySelector("#block" + key.substr(6) + data[key], true, "checked");
                break;
            case "category":
                _setBySelector("select[name=blockCat] option[value=\"" + data[key] + "\"]", true, "selected");
                break;
            case "relPageGlobal":
            case "relPageCategory":
            case "relPageDirect":
                _setRelPage(key.substr(7).toLowerCase(), data[key]);
                break;
            default:
                window['set' + key.charAt(0).toUpperCase() + key.slice(1)](data[key]);
                break;
        }
    }
    document.querySelector(selectorMappings["wysiwygEditor"]).dispatchEvent(new Event("change"));
}

function _setBySelector(selector, value, prop) {
    if (prop == undefined) {
        prop = "value";
    }
    const element = document.querySelector("form[name=blockFormModifyBlock] " + selector);
    element.setAttribute(prop, value);
    element.dispatchEvent(new Event("change"));
}

/**
 * Sets targeting option on modify form
 *
 * @param targetingOption array
 */
function setTargetingOption(targetingOption) {
    // gets form
    var form = cx.jQuery('form[name=blockFormModifyBlock]');
    // sets new data on element
    if (Object.prototype.toString.call(targetingOption[0].value) === '[object Array]') {
        // hides targeting filter block
        toggleTargetingFilterBlock(false);
        form.find('input[name=targeting_status][value=0]').prop('checked', true);
    } else {
        // shows targeting filter block
        form.find('input[name=targeting_status][value=1]').prop('checked', true);
        toggleTargetingFilterBlock(true);
        // gets element
        var element = form.find('#targeting_filter_block select:first-child');
        // iterates through targeting options
        targetingOption.forEach(function (option) {
            if (option.type === 'country') {
                if (option.filter === 'include') {
                    // selects include option in select
                    element.find('option[value=include]').prop('selected', true);
                } else {
                    // selects exclude option in select
                    element.find('option[value=exclude]').prop('selected', true);
                }

                // removes selected countries
                form.find('#selectedCountries').tagit('removeAll');
                Object.keys(option.value).forEach(function (key) {
                    // adds countries from log
                    cx.jQuery('#selectedCountries').tagit('createTag', option.value[key], null, true);
                    // adds id as data-attribute
                    form.find('#selectedCountries li.tagit-choice:last input.tagit-hidden-field').attr('data-id', key);
                });
            }
        });
    }
}

/**
 * Sets content by languages
 *
 * @param relLangContent array
 * @param wysiwygEditor integer
 */
function setRelLangContent(relLangContent) {
    // gets form
    var form = cx.jQuery('form[name=blockFormModifyBlock]');
    // sets empty string on content element
    form.find('input[id^=blockFormText_]').attr('value', '');
    // sets new data and activates languages
    var activatedCheckboxes = [];
    for (var i = 0; i < relLangContent.length; i++) {
        // sets new data on element
        form.find('input#blockFormText_' + relLangContent[i].locale).attr('value', relLangContent[i].content);
        // processes checkboxes
        activatedCheckboxes.push(relLangContent[i].locale);
        var checkbox = form.find('input#languagebar_' + relLangContent[i].locale)[0];
        if (!checkbox.checked) {
            checkbox.click();
        }
    }
    // deactivates languages
    var checkboxes = form.find('input[id^=languagebar_]');
    for (var i = 0; i < checkboxes.length; i++) {
        if (!checkboxes[i].checked) {
            continue;
        }
        var index = activatedCheckboxes.indexOf(parseInt(checkboxes[i].id.slice(-1)));
        if (index < 0) {
            checkboxes[i].click();
        }
    }
    // We do not update WYSIWYG or textarea here. This will be done indirectly.
    // For this we reset oldActiveLangId (as when loading the view initially).
    // The triggered change on WYSIWYG checkbox will initialize the wysiwyg or
    // textarea correctly.
    // If we do it directly here we might run into trouble as WYSIWYG has a
    // cooldown time after setting content.
    oldActiveLangId = 0;
}

function _setRelPage(placeholder, relations) {
    const form = cx.jQuery("form[name=blockFormModifyBlock]");
    const languages = form.find("span[id^=" + placeholder + "ChangePageTitlesTo_]");
    const element = form.find("#" + placeholder + "PageSelector");

    for (const language in languages.toArray()) {
        // set language
        cx.jQuery(language).find("> input[type=radio]").prop("checked", true);
        // clear existing selection
        element.find("#" + placeholder + "SelectedPages option").prop("selected", true);
        moveSelectedPages(
            document.getElementById(placeholder + "SelectedPages"),
            document.getElementById(placeholder + "NotSelectedPages"),
            document.getElementsByName("addpages")[0],
            document.getElementsByName("removepages")[0]
        );
        // sets new data on element
        for (var i = 0; i < relations.length; i++) {
            var option = element.find("#" + placeholder + "NotSelectedPages option[value=" + relations[i].page + "]");
            if (option) {
                option.prop("selected", true);
            }
        }
        // apply new selection
        moveSelectedPages(
            document.getElementById(placeholder + "NotSelectedPages"),
            document.getElementById(placeholder + "SelectedPages"),
            document.getElementsByName("addpages")[0],
            document.getElementsByName("removepages")[0]
        );
    }
    // selects first language by default
    cx.jQuery(languages[0]).find("> input[type=radio]").prop("checked", true);
}
