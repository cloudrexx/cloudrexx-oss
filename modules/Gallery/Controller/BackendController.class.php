<?php declare(strict_types=1);
/**
 * Cloudrexx
 *
 * @link      https://www.cloudrexx.com
 * @copyright Cloudrexx AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

namespace Cx\Modules\Gallery\Controller;

/**
 * @copyright   Cloudrexx AG
 * @author      Michael Ritter <michael.ritter@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  modules_gallery
 */
class BackendController extends \Cx\Core\Core\Model\Entity\SystemComponentBackendController {

    /**
     * @inheritDoc
     */
    public function getParsedCommands(): array {
        $cmds = array(
            '' => array(
                'permission' => new \Cx\Core_Modules\Access\Model\Entity\Permission(
                    ['http', 'https'],
                    ['get', 'post'],
                    true,
                    [],
                    [12]
                ),
                'translatable' => false,
                'children' => array(),
                'langKey' => 'TXT_GALLERY_MENU_OVERVIEW',
            ),
        );
        if ($this->hasPicturesToValidate()) {
            $cmds['validate_form'] = array(
                'permission' => new \Cx\Core_Modules\Access\Model\Entity\Permission(
                    ['http', 'https'],
                    ['get', 'post'],
                    true,
                    [],
                    [69]
                ),
                'translatable' => false,
                'children' => array(),
                'langKey' => 'TXT_GALLERY_MENU_VALIDATE',
            );
        }
        $cmds['settings'] = array(
            'permission' => new \Cx\Core_Modules\Access\Model\Entity\Permission(
                ['http', 'https'],
                ['get', 'post'],
                true,
                [],
                [70]
            ),
            'translatable' => false,
            'children' => array(),
            'langKey' => 'TXT_GALLERY_MENU_SETTINGS',
        );
        return $cmds;
    }

    /**
     * Tells whether there are pictures to be validated
     *
     * @return boolean True if there are pictures to validate, false otherwise
     */
    public function hasPicturesToValidate(): bool {
        $objResult = $this->cx->getDb()->getAdoDb()->Execute('
            SELECT
                COUNT(`id`) AS `count`
            FROM
                `' . DBPREFIX . 'module_gallery_pictures`
            WHERE
                `validated` = "0"
        ');
        return $objResult->fields['count'] > 0;
    }
}
