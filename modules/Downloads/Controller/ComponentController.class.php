<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * Main controller for Downloads
 *
 * @copyright   Cloudrexx AG
 * @author      Project Team SS4U <info@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  module_downloads
 */

namespace Cx\Modules\Downloads\Controller;

/**
 * Main controller for Downloads
 *
 * @copyright   Cloudrexx AG
 * @author      Thomas Wirz <thomas.wirz@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  module_downloads
 */
class DownloadsInternalException extends \Exception {}

/**
 * Main controller for Downloads
 *
 * @copyright   Cloudrexx AG
 * @author      Project Team SS4U <info@cloudrexx.com>
 * @author      Thomas Wirz <thomas.wirz@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  module_downloads
 */
class ComponentController extends \Cx\Core\Core\Model\Entity\SystemComponentController {

    /**
     * {@inheritdoc}
     */
    public function getControllerClasses() {
        return array('Backend', 'EsiWidget', 'JsonDownloads');
    }

    /**
     * {@inheritdoc}
     */
    public function getControllersAccessableByJson()
    {
        return array('EsiWidgetController', 'JsonDownloadsController');
    }

    /**
     * @inheritdoc
     */
    public function adjustResponse(
        \Cx\Core\Routing\Model\Entity\Response $response
    ) {
        $headers = $response->getRequest()->getHeaders();
        if (isset($headers['Referer'])) {
            $refUrl = new \Cx\Lib\Net\Model\Entity\Url($headers['Referer']);
        } else {
            $refUrl = new \Cx\Lib\Net\Model\Entity\Url($response->getRequest()->getUrl()->toString());
        }
        try {
            $canonicalUrl = $this->getCanonicalUrl($refUrl, $response->getPage());
            $response->setHeader(
                'Link',
                '<' . $canonicalUrl->toString() . '>; rel="canonical"'
            );
        } catch (\Exception $e) {
            return;
        }
    }

    /**
     * Get the canonical URL for the current request
     *
     * @param \Cx\Lib\Model\Entity\Url $requestUrl URL of current request
     * @param \Cx\Core\ContentManager\Model\Entity\Page $page Resolved page of current request
     * @return \Cx\Core\Routing\Url Canonical URL for the current request
     */
    protected function getCanonicalUrl(
        \Cx\Lib\Net\Model\Entity\Url $requestUrl,
        \Cx\Core\ContentManager\Model\Entity\Page $page
    ): \Cx\Core\Routing\Url {
        // download-urls must not have a canonical url as they should not get indexed
        if ($requestUrl->hasParam('download')) {
            throw new \Exception('Don\'t set canonical URL for downloads');
        }

        $canonicalUrl = \Cx\Core\Routing\Url::fromPage($page);
        $categoryId = 0;
        if ($requestUrl->hasParam('category')) {
            $categoryId = intval($requestUrl->getParam('category'));
        }
        if (empty($categoryId)) {
            $categoryId = intval($page->getCmd());
        }
        if (empty($categoryId)) {
            return $canonicalUrl;
        }

        // set canonical url for assets
        if ($requestUrl->hasParam('id')) {
            $download= new Download();
            $download->load($requestUrl->getParam('id'));
            if ($download->EOF) {
                return $canonicalUrl;
            }
            try {
                $canonicalUrl = DownloadsLibrary::getApplicationUrl(
                    $download->getAssociatedCategoryIds()
                );
                $canonicalUrl->setParam('id', $download->getId());
            } catch (DownloadsLibraryException $e) {}
            return $canonicalUrl;
        }

        // set canonical url for categories
        try {
            $canonicalUrl = DownloadsLibrary::getApplicationUrl([$categoryId]);
            if (
                $requestUrl->hasParam('pos')
                && !empty($requestUrl->getParam('pos'))
            ) {
                $canonicalUrl->setParams(['pos' => $requestUrl->getParam('pos')]);
            }
        } catch (DownloadsLibraryException $e) {}
        return $canonicalUrl;
    }

    /**
     * Load your component.
     *
     * @param \Cx\Core\ContentManager\Model\Entity\Page $page       The resolved page
     */
    public function load(\Cx\Core\ContentManager\Model\Entity\Page $page) {
        switch ($this->cx->getMode()) {
            case \Cx\Core\Core\Controller\Cx::MODE_FRONTEND:
                $objDownloadsModule = new Downloads(\Env::get('cx')->getPage()->getContent());
                \Env::get('cx')->getPage()->setContent($objDownloadsModule->getPage());
                $downloads_pagetitle = $objDownloadsModule->getPageTitle();
                if ($downloads_pagetitle) {
                    \Env::get('cx')->getPage()->setTitle($downloads_pagetitle);
                    \Env::get('cx')->getPage()->setContentTitle($downloads_pagetitle);
                    \Env::get('cx')->getPage()->setMetaTitle($downloads_pagetitle);
                }

                break;

            case \Cx\Core\Core\Controller\Cx::MODE_BACKEND:
                parent::load($page);
                break;

            default:
                break;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function postInit(\Cx\Core\Core\Controller\Cx $cx)
    {
        // downloads group
        $groups            = Group::getGroups();
        $groupsPlaceholders = $groups->getGroupsPlaceholders();
        $this->registerDownloadsWidgets($groupsPlaceholders, \Cx\Core_Modules\Widget\Model\Entity\EsiWidget::TYPE_PLACEHOLDER);

        // downloads category list
        $categoriesBlocks = Category::getCategoryWidgetNames();
        $this->registerDownloadsWidgets($categoriesBlocks, \Cx\Core_Modules\Widget\Model\Entity\EsiWidget::TYPE_BLOCK);
    }

    /**
     * Register the downloads widgets
     *
     * @param array   $widgets widgets array
     * @param string  $type Widget type
     *
     * @return null
     */
    protected function registerDownloadsWidgets($widgets, $type) {

        $pos = 0;
        if (isset($_GET['pos'])) {
            $pos = intval($_GET['pos']);
        }
        $widgetController = $this->getComponent('Widget');
        foreach ($widgets as $widgetName) {
            $widget = new \Cx\Core_Modules\Widget\Model\Entity\EsiWidget(
                $this,
                $widgetName,
                $type,
                '',
                '',
                array('pos' => $pos)
            );
            $widget->setEsiVariable(
                \Cx\Core_Modules\Widget\Model\Entity\EsiWidget::ESI_VAR_ID_USER |
                \Cx\Core_Modules\Widget\Model\Entity\EsiWidget::ESI_VAR_ID_THEME |
                \Cx\Core_Modules\Widget\Model\Entity\EsiWidget::ESI_VAR_ID_CHANNEL
            );
            $widgetController->registerWidget($widget);
        }
    }

    /**
     * Register your event listeners here
     *
     * USE CAREFULLY, DO NOT DO ANYTHING COSTLY HERE!
     * CALCULATE YOUR STUFF AS LATE AS POSSIBLE.
     * Keep in mind, that you can also register your events later.
     * Do not do anything else here than initializing your event listeners and
     * list statements like
     * $this->cx->getEvents()->addEventListener($eventName, $listener);
     */
    public function registerEventListeners() {
        $evm = $this->cx->getEvents();
        $eventListener = new \Cx\Modules\Downloads\Model\Event\DownloadsEventListener($this->cx);
        $evm->addEventListener('SearchFindContent', $eventListener);
        $evm->addEventListener('mediasource.load', $eventListener);
        $evm->addEventListener('NetManager:getSenderDomains', $eventListener);

        // locale event listener
        $localeLocaleEventListener = new \Cx\Modules\Downloads\Model\Event\LocaleLocaleEventListener($this->cx);
        $evm->addModelListener('postPersist', 'Cx\\Core\\Locale\\Model\\Entity\\Locale', $localeLocaleEventListener);
        $evm->addModelListener('preRemove', 'Cx\\Core\\Locale\\Model\\Entity\\Locale', $localeLocaleEventListener);
    }

    /**
     * Find Downloads by keyword and return them in a
     * two-dimensional array compatible to be used by Search component.
     *
     * @param \Cx\Core_Modules\Search\Controller\Search The search instance
     *                                                  that triggered the
     *                                                  search event
     * @return  array   Two-dimensional array of Downloads found by keyword.
     *                  If integration into search component is disabled or
     *                  no Download matched the giving keyword, then an
     *                  empty array is retured.
     */
    public function getDownloadsForSearchComponent(
        \Cx\Core_Modules\Search\Controller\Search $search
    ) {
        $data = array();
        $downloadLibrary = new DownloadsLibrary();
        $config = $downloadLibrary->getSettings();
        $download = new Download($config);

        // abort in case downloads shall not be included into the global
        // fulltext search component
        if (!$config['integrate_into_search_component']) {
            return array();
        }

        // check for valid published application page
        $filter = null;
        try {
            $arrCategoryIds = $this->getCategoryFilterForSearchComponent(
                $search
            );

            // set category filter if we have to restrict search by
            // any category IDs
            if ($arrCategoryIds) {
                $filter = array('category_id' => $arrCategoryIds);
            }
        } catch (DownloadsInternalException $e) {
            return array();
        }

        // lookup downloads by given keyword
        if (!$download->loadDownloads(
            $filter,
            $search->getTerm(),
            null,
            null,
            null,
            null,
            true,
            $config['list_downloads_current_lang']
        )) {
            return array();
        }

        /**
         * @ignore
         */
        \Env::get('ClassLoader')->loadFile(ASCMS_LIBRARY_PATH . '/PEAR/Download.php');

        $langId = DownloadsLibrary::getOutputLocale()->getId();

        if ($search->getOptions()['includeAssociationData']) {
            $langData = \Env::get('init')->getComponentSpecificLanguageData(
                $this->getName(),
                true,
                FRONTEND_LANG_ID
            );
        }

        while (!$download->EOF) {
            try {
                $catIds = $download->getAssociatedCategoryIds();
                $url = DownloadsLibrary::getApplicationUrl(
                    $catIds
                );
            } catch (DownloadsLibraryException $e) {
                $download->next();
                continue;
            }

            // determine link-behaviour
            switch ($config['global_search_linking']) {
                case HTTP_DOWNLOAD_INLINE:
                case HTTP_DOWNLOAD_ATTACHMENT:
                    DownloadsLibrary::enhanceUrlForDownload(
                        $url,
                        $download,
                        $config['global_search_linking']
                    );
                    break;

                case 'detail':
                default:
                    $url->setParam('id', $download->getId());
                    break;
            }

            $image = '';
            $imageSrc = $download->getImage();
            if (
                !empty($imageSrc)
                && file_exists(
                    $this->cx->getWebsiteDocumentRootPath() . '/' . $imageSrc
                )
            ) {
                $image = contrexx_raw2encodedUrl($imageSrc);
            }

            $result = array(
                'Score'     => 100,
                'Title'     => $download->getName($langId),
                'Content'   => $download->getTrimmedDescription($langId),
                'Image'     => $image,
                'Link'      => (string) $url,
                'Component' => $this->getName(),
                'Class'     => Download::class,
                'Id'        => $download->getId(),
            );

            if ($search->getOptions()['includeAssociationData']) {
                $type = $download->getMimeType();
                // note: we should set $result['Mimetype'] instead of $result['AdditionalData']['Type']
                $result['AdditionalData'] = [
                    'Type' => [
                        $type => $langData[$download::$arrMimeTypes[$type]['description']]
                    ]
                ];
                $result['Categorization'] = [];
                foreach ($catIds as $id) {
                    $category = Category::getCategory($id);
                    if ($category->EOF) {
                        continue;
                    }
                    $result['Categorization'][] = [
                        'Class' => Category::class,
                        'Id'    => $id,
                        'Title'  => $category->getName(),
                    ];
                }
            }
            $data[] = $result;
            $download->next();
        }

        return $data;
    }

    /**
     * Get published category IDs (as application pages)
     *
     *
     * @param \Cx\Core_Modules\Search\Controller\Search The search instance
     *                                                  that triggered the
     *                                                  search event
     * @return  array   List of published category IDs.
     *                  An empty array is retured, in case an application 
     *                  page is published that has no category restriction set
     *                  through its CMD.
     * @throws  DownloadsInternalException In case no application page of this
     *                                  component is published
     */
    protected function getCategoryFilterForSearchComponent(
        \Cx\Core_Modules\Search\Controller\Search $search
    ) {
        // fetch data about existing application pages of this component
        $cmds = array();
        $em = $this->cx->getDb()->getEntityManager();
        $pageRepo = $em->getRepository('Cx\Core\ContentManager\Model\Entity\Page');
        $pages = $pageRepo->getAllFromModuleCmdByLang($this->getName());
        foreach ($pages as $pagesOfLang) {
            foreach ($pagesOfLang as $page) {
                $cmds[] = $page->getCmd();
            }
        }

        // check if an application page is published
        $cmds = array_unique($cmds);
        $arrCategoryIds = array();
        foreach ($cmds as $cmd) {
            // fetch application page with specific CMD from current locale
            $page = $pageRepo->findOneByModuleCmdLang($this->getName(), $cmd, FRONTEND_LANG_ID);
            if (!$page) {
                continue;
            }

            // skip pages that are not eligible to be listed in search results
            if (!$search->isPageListable($page)) {
                continue;
            }

            // in case the CMD is an integer, then
            // the integer does represent an ID of category which has to be
            // applied to the search filter
            if (preg_match('/^\d+$/', $cmd)) {
                $arrCategoryIds[] = $cmd;
                continue;
            }

            // in case an application exists that has not set a category-ID as
            // its CMD, then we do not have to restrict the search by one or
            // more specific categories
            return array();
        }

        // if we reached this point and no category-IDs have been fetched 
        // then this means that no application is published
        if (empty($arrCategoryIds)) {
            throw new DownloadsInternalException('Application is not published');
        }

        return $arrCategoryIds;
    }
}
