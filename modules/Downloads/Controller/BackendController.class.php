<?php declare(strict_types=1);

/**
 * Cloudrexx
 *
 * @link      https://www.cloudrexx.com
 * @copyright Cloudrexx AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

namespace Cx\Modules\Downloads\Controller;

/**
 * @copyright   Cloudrexx AG
 * @author      Michael Ritter <michael.ritter@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  modules_downloads
 */
class BackendController extends \Cx\Core\Core\Model\Entity\SystemComponentBackendController {

    public function getCommands() {
        return array(
            '',
            'downloads',
            'categories',
            'groups' => array(
                'permission' => new \Cx\Core_Modules\Access\Model\Entity\Permission(
                    array('http', 'https'),
                    array('get', 'post'),
                    true,
                    array(),
                    array(142),
                ),
            ),
            'settings' => array(
                'permission' => new \Cx\Core_Modules\Access\Model\Entity\Permission(
                    array('http', 'https'),
                    array('get', 'post'),
                    true,
                    array(),
                    array(142),
                ),
                'children' => array(
                    '',
                    'display',
                    'search',
                    'statistics',
                    'attributes',
                    'automation',
                    'mailtemplates',
                ),
            ),
        );
    }

    public function getPage($page) {
        global $_CORELANG, $subMenuTitle, $objTemplate;
        $cmd = array('');
        if (isset($_GET['act'])) {
            $cmd = explode('/', contrexx_input2raw($_GET['act']));
        }
        $this->cx->getTemplate()->addBlockfile('CONTENT_OUTPUT', 'content_master', 'ContentMaster.html');
        $this->cx->getTemplate()->setVariable('CONTENT_NAVIGATION', $this->parseNavigation($cmd)->get());
        $objTemplate = $this->cx->getTemplate();
        $subMenuTitle = $_CORELANG['TXT_DOWNLOADS'];
        $objDownloadsModule = new DownloadsManager();
        $objDownloadsModule->getPage($cmd);
    }

    public function parseNavigation(&$cmd = array()) {
        $mappings = array(
            '' => array(
                'overview',
            ),
            'downloads' => array(
                'download',
                'delete_download',
                'switch_download_status',
                'add_new_download_to_category',
            ),
            'categories' => array(
                'category',
                'delete_category',
                'switch_category_status',
                'add_downloads_to_category',
                'categories_delete_download',
                'unlink_download',
            ),
            'groups' => array(
                'group',
                'delete_group',
                'switch_group_status',
            ),
            'settings' => array(
                'mailtemplate_overview',
                'mailtemplate_edit',
            ),
        );
        $currentCmd = current($cmd);
        $replacement = '';
        if (!isset($mappings[$currentCmd])) {
            foreach ($mappings as $navCmd=>$aliases) {
                if (!in_array($currentCmd, $aliases)) {
                    continue;
                }
                $replacement = $cmd[0];
                $cmd[0] = $navCmd;
                break;
            }
        }
        $ret = parent::parseNavigation($cmd);
        if (!empty($replacement)) {
            $cmd[0] = $replacement;
        }
        return $ret;
    }
}

